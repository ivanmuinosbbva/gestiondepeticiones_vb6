/*
*********************************************************************************************************************************
ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR
*********************************************************************************************************************************

-001- a. FJS 06.07.2007 - Se modifica el SP para soportar el filtro por el campo cod_clase de peticiones (Copia de sp_rptHsTrabAreaEjec)
-002- a. FJS 29.05.2008 - Se agrega el valor de sin clase.
-003- a. FJS 29.10.2008 - Se agrega una nueva columna para identificar de mejor manera las horas trabajadas en Tareas.
-004- a. FJS 29.06.2011 - Se corrige un bug que generaba informaci�n basura (no filtraba correctamente por la clase solicitada).
-005- a. FJS 04.05.2016 - Nuevo: se agrega filtro por proyecto y por agrupamientos.
*/

use GesPet
go

print 'Actualizando procedimiento almacenado: sp_rptHsTrabAreaEjec2'
go

if exists (select * from sysobjects where name = 'sp_rptHsTrabAreaEjec2' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptHsTrabAreaEjec2
go

create procedure dbo.sp_rptHsTrabAreaEjec2
	@fdesde				varchar(8)='NULL',
	@fhasta				varchar(8)='NULL',
	@nivel				varchar(4)='NULL',
	@area				varchar(8)='NULL',
	@recurso			varchar(10)='NULL',
	@detalle			varchar(1)='0',
	@cod_clase			char(40)='NULL',
	@incluir			char(3)='ALL',			-- add -003- a.
	--{ add -005- a.
	@projid				int=null,
	@projsubid			int=null,
	@projsubsid			int=null,
	@agr_nrointerno		int=null,
	@tipoproj			char(1)='S'
	--}
as
	declare @cod_direccion		varchar (10)
	declare @cod_gerencia		varchar (10)
	declare @cod_sector			varchar (10)
	declare @cod_grupo			varchar (10)
	declare @cod_recurso		varchar (10)
	declare @cod_tarea			varchar (10)
	declare @nom_tarea			varchar (50)
	declare @pet_nrointerno		int
	declare @pet_nroasignado	int
	declare @PetClass			char(4)			-- add -001-
	declare @fe_desde			smalldatetime
	declare @fe_hasta			smalldatetime
	declare @horas				int
	declare @trabsinasignar		char (1)
	declare @sol_desde			smalldatetime
	declare @sol_hasta			smalldatetime
	declare @ret_dias			int
	declare @ret_horas			int
	declare @horas_si			numeric(10,2)
	declare @horas_no			numeric(10,2)
	declare @xhoras_si			numeric(10,2)
	declare @xhoras_no			numeric(10,2)
	declare @ret_desde			smalldatetime
	declare @ret_hasta			smalldatetime
	declare @tip_asig			varchar(3)
	declare @cod_asig			varchar(10)
	declare @cod_real			varchar(10)
	declare @nom_asig			varchar(50)

	set dateformat ymd

	--{ add -005- a.
	declare @secuencia int  
	declare @ret_status     int  
	 
	create table #AgrupXPetic (
		pet_nrointerno		int,
		agr_nrointerno		int)  
	  
	-- Esto es si se pidio agrupamiento
	if @agr_nrointerno<>0  
		begin  
			create table #AgrupArbol(  
				secuencia		int,  
				nivel			int,  
				agr_nrointerno  int)  
			
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_nrointerno, 0, @secuencia OUTPUT  
			if (@ret_status <> 0)
				begin  
					if (@ret_status = 30003)  
					begin  
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError  
						return (@ret_status)  
					end  
				end  
			insert into #AgrupXPetic (pet_nrointerno, agr_nrointerno)  
				select AP.pet_nrointerno, AP.agr_nrointerno  
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where 
					AA.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_vigente='S'
		end
	--}


	if RTRIM(@fdesde)='NULL'
		begin
			select @sol_desde=dateadd(mm,-1,getdate())
			select @fdesde=convert(char(8),@sol_desde,112)
		end
	else
		select @sol_desde=@fdesde

	if RTRIM(@fhasta)='NULL'
		begin
			select @sol_hasta=dateadd(mm,1,getdate())
			select @fhasta=convert(char(8),@sol_hasta,112)
		end
	else
		select @sol_hasta=@fhasta

	CREATE TABLE #TmpSalida(
			cod_direccion	char(10) null,
			cod_gerencia	char(10) null,
			cod_sector		char(10) null,
			cod_grupo		char(10) null,
			cod_recurso		char(10) null,
			nom_direccion	char(80) null,
			nom_gerencia	char(80) null,
			nom_sector		char(80) null,
			nom_grupo		char(80) null,
			nom_recurso		char(50) null,
			tip_asig		char(3),			-- TAR/PET
			cod_asig		char(10) null,		-- C�digo de tarea
			cod_real		char(10) null,		-- Nro. de petici�n asignado
			cod_clase		char(4)	 null,		-- add -001- a. Clase de petici�n
			entre_desde		smalldatetime,
			entre_hasta		smalldatetime,
			horas_si		numeric(10,2),
			horas_no		numeric(10,2),
			horas_tarea		numeric(10,2)		-- add -003- a.
	)

	CREATE TABLE #PETICIONES_CLASES(
			classcod        char(4)		null,
			classnom        varchar(60)	null)
	
	insert into #PETICIONES_CLASES (classcod, classnom) values ('NUEV', 'Nuevo desarrollo')
	insert into #PETICIONES_CLASES (classcod, classnom) values ('EVOL', 'Mantenimiento evolutivo')
	insert into #PETICIONES_CLASES (classcod, classnom) values ('ATEN', 'Atenci�n a usuario')
	insert into #PETICIONES_CLASES (classcod, classnom) values ('OPTI', 'Optimizaci�n')
	insert into #PETICIONES_CLASES (classcod, classnom) values ('CORR', 'Correctivo')
	insert into #PETICIONES_CLASES (classcod, classnom) values ('SPUF', 'SPUFI')
	insert into #PETICIONES_CLASES (classcod, classnom) values ('SINC', 'Sin clase')
	--insert into #PETICIONES_CLASES (classcod, classnom) values ('TAR.', 'Tareas')		-- add -003- a.

	DECLARE CursHoras CURSOR FOR
		select
			Re.cod_direccion,
			Re.cod_gerencia,
			Re.cod_sector,
			Re.cod_grupo,
			Ht.cod_recurso,
			Ht.cod_tarea,
			Ta.nom_tarea,
			Ht.pet_nrointerno,
			Pt.pet_nroasignado,
			Pt.cod_clase,					-- add -001-
			Ht.trabsinasignar,
			Ht.fe_desde,
			Ht.fe_hasta,
			Ht.horas
		from
			GesPet..HorasTrabajadas Ht,
			GesPet..Recurso Re,
			GesPet..Tarea Ta,
			GesPet..Peticion Pt
		where
			((@incluir = 'ALL') or (@incluir = 'TAR' and Ht.pet_nrointerno = 0) or (@incluir = 'PET' and Ht.pet_nrointerno <> 0)) and
			(RTRIM(@fdesde)='NULL' or convert(char(8),Ht.fe_hasta,112) >= @fdesde) and
			(RTRIM(@fhasta)='NULL' or convert(char(8),Ht.fe_desde,112) <= @fhasta) and
			(RTRIM(@recurso)='NULL' OR RTRIM(Ht.cod_recurso)=RTRIM(@recurso)) and
			(RTRIM(Ht.cod_recurso) = RTRIM(Re.cod_recurso)) and
			--(RTRIM(@cod_clase)='NULL' or RTRIM(Pt.cod_clase)=RTRIM(@cod_clase)) and							-- add -001- -- del -005- a.
			--(RTRIM(@cod_clase)='NULL' or charindex(RTRIM(Pt.cod_clase), LTRIM(RTRIM(@cod_clase)))>0) AND		-- add -005- a.
			((@nivel is null or RTRIM(@nivel)='NULL') or
			 (@nivel='DIRE' and RTRIM(Re.cod_direccion)=RTRIM(@area)) or
			 (@nivel='GERE' and RTRIM(Re.cod_gerencia)=RTRIM(@area)) or
			 (@nivel='SECT' and RTRIM(Re.cod_sector)=RTRIM(@area)) or
			 (@nivel='GRUP' and RTRIM(Re.cod_grupo)=RTRIM(@area))) and
			(RTRIM(Ht.cod_tarea) *= RTRIM(Ta.cod_tarea)) and
			--(Ht.pet_nrointerno *= Pt.pet_nrointerno AND (RTRIM(@cod_clase)='NULL' or charindex(RTRIM(Pt.cod_clase), LTRIM(RTRIM(@cod_clase)))>0)) 		-- add -005- a.)			-- del -004- a.
			--(Ht.pet_nrointerno = Pt.pet_nrointerno)			-- add -004- a.
			(Ht.pet_nrointerno *= Pt.pet_nrointerno)			-- add -004- a. 
			--{ add -007- a.
			AND
			((@agr_nrointerno=0 OR @agr_nrointerno IS NULL) or 
				(@incluir='ALL' AND (Ht.pet_nrointerno = 0 OR Ht.pet_nrointerno IN (select AXP.pet_nrointerno from #AgrupXPetic AXP))) OR 
				(@incluir='PET' AND Ht.pet_nrointerno IN (select AXP.pet_nrointerno from #AgrupXPetic AXP)) OR 
				(@incluir='TAR' AND Ht.pet_nrointerno = 0)) 
			AND 
			((@projid is null or @projid = 0) OR 
				(@incluir='TAR' AND Ht.pet_nrointerno = 0) OR
				(@incluir='PET' AND Ht.pet_nrointerno IN ( 
					select p.pet_nrointerno
					from Peticion p
					where 
						(@tipoproj = 'S' AND p.pet_projid = @projid and
							p.pet_projsubid = @projsubid and 
							p.pet_projsubsid = @projsubsid) OR 
						(@tipoproj <> 'S' AND p.pet_projid = @projid and
							((@projsubid is null OR @projsubid = 0) OR p.pet_projsubid = @projsubid) AND
							((@projsubsid is null OR @projsubsid = 0) OR p.pet_projsubsid = @projsubsid)))
				) OR 
				(@incluir='ALL' AND (Ht.pet_nrointerno = 0 OR Ht.pet_nrointerno IN ( 
					select p.pet_nrointerno
					from Peticion p
					where 
						(@tipoproj = 'S' AND p.pet_projid = @projid and
							p.pet_projsubid = @projsubid and 
							p.pet_projsubsid = @projsubsid) OR 
						(@tipoproj <> 'S' AND p.pet_projid = @projid and
							((@projsubid is null OR @projsubid = 0) OR p.pet_projsubid = @projsubid) AND
							((@projsubsid is null OR @projsubsid = 0) OR p.pet_projsubsid = @projsubsid))))
				)
			)
			--}
		union --all
		select
			Fa.cod_direccion,
			Fa.cod_gerencia,
			Fa.cod_sector,
			Fa.cod_grupo,
			Fa.cod_fab as cod_recurso,
			Ht.cod_tarea,
			Ta.nom_tarea,
			Ht.pet_nrointerno,
			Pt.pet_nroasignado,
			Pt.cod_clase,   -- add -001-
			Ht.trabsinasignar,
			Ht.fe_desde,
			Ht.fe_hasta,
			Ht.horas
		from
			GesPet..HorasTrabajadas Ht,
			GesPet..Fabrica Fa,
			GesPet..Tarea Ta,
			GesPet..Peticion Pt
		where
			((@incluir = 'ALL') or (@incluir = 'TAR' and Ht.pet_nrointerno = 0) or (@incluir = 'PET' and Ht.pet_nrointerno <> 0)) and
			(RTRIM(@fdesde)='NULL' or convert(char(8),Ht.fe_hasta,112) >= @fdesde) and
			(RTRIM(@fhasta)='NULL' or convert(char(8),Ht.fe_desde,112) <= @fhasta) and
			(RTRIM(@recurso)='NULL' OR RTRIM(Ht.cod_recurso)=RTRIM(@recurso)) and
			(RTRIM(Ht.cod_recurso) = RTRIM(Fa.cod_fab)) and
			--(RTRIM(@cod_clase)='NULL' or RTRIM(Pt.cod_clase)=RTRIM(@cod_clase)) and							-- add -001- -- del -005- a.
			--(RTRIM(@cod_clase)='NULL' or CHARINDEX(RTRIM(Pt.cod_clase), LTRIM(RTRIM(@cod_clase)))>0) AND		-- add -005- a.
			((@nivel is null or RTRIM(@nivel)='NULL') or
			 (@nivel='DIRE' and RTRIM(Fa.cod_direccion)=RTRIM(@area)) or
			 (@nivel='GERE' and RTRIM(Fa.cod_gerencia)=RTRIM(@area)) or
			 (@nivel='SECT' and RTRIM(Fa.cod_sector)=RTRIM(@area)) or
			 (@nivel='GRUP' and RTRIM(Fa.cod_grupo)=RTRIM(@area))) and
			(RTRIM(Ht.cod_tarea) *= RTRIM(Ta.cod_tarea)) and
			(Ht.pet_nrointerno *= Pt.pet_nrointerno)
			--(Ht.pet_nrointerno *= Pt.pet_nrointerno AND (RTRIM(@cod_clase)='NULL' or charindex(RTRIM(Pt.cod_clase), LTRIM(RTRIM(@cod_clase)))>0)) 		-- add -005- a.)			-- del -004- a.
			--(Ht.pet_nrointerno = Pt.pet_nrointerno)
			--{ add -007- a.
			AND
			((@agr_nrointerno=0 OR @agr_nrointerno IS NULL) or 
				(@incluir='ALL' AND (Ht.pet_nrointerno = 0 OR Ht.pet_nrointerno IN (select AXP.pet_nrointerno from #AgrupXPetic AXP))) OR 
				(@incluir='PET' AND Ht.pet_nrointerno IN (select AXP.pet_nrointerno from #AgrupXPetic AXP)) OR 
				(@incluir='TAR' AND Ht.pet_nrointerno = 0)) 
			AND 
			((@projid is null or @projid = 0) OR 
				(@incluir='TAR' AND Ht.pet_nrointerno = 0) OR
				(@incluir='PET' AND Ht.pet_nrointerno IN ( 
					select p.pet_nrointerno
					from Peticion p
					where 
						(@tipoproj = 'S' AND p.pet_projid = @projid and
							p.pet_projsubid = @projsubid and 
							p.pet_projsubsid = @projsubsid) OR 
						(@tipoproj <> 'S' AND p.pet_projid = @projid and
							((@projsubid is null OR @projsubid = 0) OR p.pet_projsubid = @projsubid) AND
							((@projsubsid is null OR @projsubsid = 0) OR p.pet_projsubsid = @projsubsid)))
				) OR 
				(@incluir='ALL' AND (Ht.pet_nrointerno = 0 OR Ht.pet_nrointerno IN ( 
					select p.pet_nrointerno
					from Peticion p
					where 
						(@tipoproj = 'S' AND p.pet_projid = @projid and
							p.pet_projsubid = @projsubid and 
							p.pet_projsubsid = @projsubsid) OR 
						(@tipoproj <> 'S' AND p.pet_projid = @projid and
							((@projsubid is null OR @projsubid = 0) OR p.pet_projsubid = @projsubid) AND
							((@projsubsid is null OR @projsubsid = 0) OR p.pet_projsubsid = @projsubsid))))
				)
			)
			--}
		for read only

		OPEN CursHoras
		FETCH CursHoras INTO
				@cod_direccion,
				@cod_gerencia,
				@cod_sector,
				@cod_grupo,
				@cod_recurso,
				@cod_tarea,
				@nom_tarea,
				@pet_nrointerno,
				@pet_nroasignado,
				@PetClass,					-- add -001-
				@trabsinasignar,
				@fe_desde,
				@fe_hasta,
				@horas

		WHILE (@@sqlstatus = 0)
		BEGIN
			execute sp_GetHorasPeriodo 
				@sol_desde,
				@sol_hasta,
				@fe_desde,
				@fe_hasta,
				@horas,
				@ret_desde OUTPUT,
				@ret_hasta OUTPUT,
				@ret_dias  OUTPUT,
				@ret_horas OUTPUT

			if RTRIM(@trabsinasignar)='S'
				begin
					select @horas_si = 0
					select @horas_no = @ret_horas
				end
			else
				begin
					select @horas_si = @ret_horas
					select @horas_no = 0
				end

			if @cod_tarea is null or RTRIM(@cod_tarea)=''
			--if @cod_tarea is null or RTRIM(@cod_tarea) is null or RTRIM(@cod_tarea)=''
			--if @PetClass is null or RTRIM(@PetClass) is null or RTRIM(@PetClass)=''
			--if @pet_nrointerno is null or @pet_nrointerno = 0
				begin
					select @PetClass = 'TAR.'		-- add -003- a.
					select @tip_asig = 'TAR'
					select @cod_asig=@cod_tarea
					select @cod_real=@cod_tarea
				end
			else
				select @tip_asig='PET'
			
			/*
				begin
					--select @tip_asig='PET'
					--select @cod_asig=convert(varchar(10),@pet_nroasignado)
					--select @cod_real=convert(varchar(10),@pet_nrointerno)
				end
			else
				begin
					--select @tip_asig='TAR'
					--select @cod_asig=@cod_tarea
					--select @cod_real=@cod_tarea
					select @PetClass = 'TAR.'		-- add -003- a.
				end
			*/
			
			--{ add -003- a.
			if @tip_asig = 'TAR'
				insert #TmpSalida
				   (cod_direccion,
					cod_gerencia,
					cod_sector,
					cod_grupo,
					cod_recurso,
					tip_asig,
					cod_asig,
					cod_real,
					cod_clase,
					entre_desde,
					entre_hasta,
					horas_si,
					horas_no,
					horas_tarea)
				values
				   (@cod_direccion,
					@cod_gerencia,
					@cod_sector,
					@cod_grupo,
					@cod_recurso,
					@tip_asig,
					@cod_asig,
					@cod_real,
					@PetClass,
					@ret_desde,
					@ret_hasta,
					0,
					0,
					@horas_si + @horas_no)
			else
			--}
				insert #TmpSalida
				   (cod_direccion,
					cod_gerencia,
					cod_sector,
					cod_grupo,
					cod_recurso,
					tip_asig,
					cod_asig,
					cod_real,
					cod_clase,          -- add -001-
					entre_desde,
					entre_hasta,
					horas_si,
					horas_no,
					horas_tarea)		-- add -003- a.
				values
				   (@cod_direccion,
					@cod_gerencia,
					@cod_sector,
					@cod_grupo,
					@cod_recurso,
					@tip_asig,
					@cod_asig,
					@cod_real,
					@PetClass,          -- add -001-
					@ret_desde,
					@ret_hasta,
					@horas_si,
					@horas_no,
					0)

			FETCH CursHoras INTO
				@cod_direccion,
				@cod_gerencia,
				@cod_sector,
				@cod_grupo,
				@cod_recurso,
				@cod_tarea,
				@nom_tarea,
				@pet_nrointerno,
				@pet_nroasignado,
				@PetClass,          -- add -001-
				@trabsinasignar,
				@fe_desde,
				@fe_hasta,
				@horas
		END
		CLOSE CursHoras
		DEALLOCATE CURSOR CursHoras

	if @detalle='0'         -- Nivel de agrupamiento: Sin agrupamiento
		select
			TS.cod_direccion,
			nom_direccion = (select nom_direccion from Direccion where cod_direccion=TS.cod_direccion),
			TS.cod_gerencia,
			nom_gerencia = (select nom_gerencia from Gerencia where cod_gerencia=TS.cod_gerencia),
			TS.cod_sector,
			nom_sector = (select nom_sector from Sector where cod_sector=TS.cod_sector),
			TS.cod_grupo,
			nom_grupo = (select nom_grupo from Grupo where cod_grupo=TS.cod_grupo),
			TS.cod_recurso,
			--nom_recurso = (select nom_recurso from Recurso where cod_recurso=TS.cod_recurso),
			nom_recurso = case
				when (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = TS.cod_recurso) is null then (select x.nom_fab from GesPet..Fabrica x where x.cod_fab = TS.cod_recurso)
				else (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = TS.cod_recurso)
			end,
			--{ add -001-
			TS.tip_asig,	-- add -003- a.
			TS.cod_clase,
			classnom = (select classnom from #PETICIONES_CLASES where classcod = TS.cod_clase),
			--}
			convert(numeric(10,2),TS.horas_si/6000) as asignadas_si,
			convert(numeric(10,2),TS.horas_no/6000) as asignadas_no,
			convert(numeric(10,2),TS.horas_tarea/6000) as hs_tareas		-- add -003- a.
		from 
			#TmpSalida  TS
		order by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.cod_sector,
			TS.cod_grupo,
			TS.cod_recurso, 
			TS.tip_asig, 
			TS.cod_clase
	if @detalle='1'         -- Nivel de agrupamiento: Recurso
		select distinct
			TS.cod_direccion,
			nom_direccion = (select nom_direccion from Direccion where cod_direccion=TS.cod_direccion),
			TS.cod_gerencia,
			nom_gerencia = (select nom_gerencia from Gerencia where cod_gerencia=TS.cod_gerencia),
			TS.cod_sector,
			nom_sector = (select nom_sector from Sector where cod_sector=TS.cod_sector),
			TS.cod_grupo,
			nom_grupo = (select nom_grupo from Grupo where cod_grupo=TS.cod_grupo),
			TS.cod_recurso,
			--nom_recurso = (select nom_recurso from Recurso where cod_recurso=TS.cod_recurso),
			nom_recurso = case
				when (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = TS.cod_recurso) is null then (select x.nom_fab from GesPet..Fabrica x where x.cod_fab = TS.cod_recurso)
				else (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = TS.cod_recurso)
			end,
			--{ add -001-
			TS.tip_asig,	-- add -003- a.
			TS.cod_clase,
			classnom = (select classnom from #PETICIONES_CLASES where classcod = TS.cod_clase),
			--}
			convert(numeric(10,2),sum(TS.horas_si/6000)) as asignadas_si,
			convert(numeric(10,2),sum(TS.horas_no/6000)) as asignadas_no
			,convert(numeric(10,2),sum(TS.horas_tarea/6000)) as hs_tareas	-- add -003- a.
		from #TmpSalida  TS
		group by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.cod_sector,
			TS.cod_grupo,
			TS.cod_recurso, 
			TS.tip_asig, 
			TS.cod_clase
		order by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.cod_sector,
			TS.cod_grupo,
			TS.cod_recurso, 
			--TS.tip_asig, 
			TS.cod_clase
	if @detalle='2'         -- Nivel de agrupamiento: Grupo
		select distinct
			TS.cod_direccion,
			nom_direccion = (select nom_direccion from Direccion where cod_direccion=TS.cod_direccion),
			TS.cod_gerencia,
			nom_gerencia = (select nom_gerencia from Gerencia where cod_gerencia=TS.cod_gerencia),
			TS.cod_sector,
			nom_sector = (select nom_sector from Sector where cod_sector=TS.cod_sector),
			TS.cod_grupo,
			nom_grupo = (select nom_grupo from Grupo where cod_grupo=TS.cod_grupo),
			--'' as cod_recurso,
			--'' as nom_recurso,
			NULL as cod_recurso,
			NULL as nom_recurso,
			--{ add -001-
			TS.tip_asig,	-- add -003- a.
			TS.cod_clase,
			classnom = (select classnom from #PETICIONES_CLASES where classcod = TS.cod_clase),
			--}
			convert(numeric(10,2),sum(TS.horas_si/6000)) as asignadas_si,
			convert(numeric(10,2),sum(TS.horas_no/6000)) as asignadas_no
			,convert(numeric(10,2),sum(TS.horas_tarea/6000)) as hs_tareas	-- add -003- a.
		from 
			#TmpSalida  TS
		group by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.cod_sector,
			TS.cod_grupo, 
			TS.tip_asig, 
			TS.cod_clase
		order by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.cod_sector,
			TS.cod_grupo, 
			TS.tip_asig, 
			TS.cod_clase
	if @detalle='3'         -- Nivel de agrupamiento: Sector
		select  distinct
			TS.cod_direccion,
			nom_direccion = (select nom_direccion from Direccion where cod_direccion=TS.cod_direccion),
			TS.cod_gerencia,
			nom_gerencia = (select nom_gerencia from Gerencia where cod_gerencia=TS.cod_gerencia),
			TS.cod_sector,
			nom_sector = (select nom_sector from Sector where cod_sector=TS.cod_sector),
			'' as cod_grupo,
			'' as nom_grupo,
			'' as cod_recurso,
			'' as nom_recurso,
			--{ add -001-
			TS.tip_asig,	-- add -003- a.
			TS.cod_clase,
			classnom = (select classnom from #PETICIONES_CLASES where classcod = TS.cod_clase),
			--}
			convert(numeric(10,2),sum(TS.horas_si/6000)) as asignadas_si,
			convert(numeric(10,2),sum(TS.horas_no/6000)) as asignadas_no
			,convert(numeric(10,2),sum(TS.horas_tarea/6000)) as hs_tareas	-- add -003- a.
		from #TmpSalida  TS
		group by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.cod_sector, 
			TS.tip_asig, 
			TS.cod_clase
		order by 
			TS.cod_direccion,
			TS.cod_gerencia,
			TS.cod_sector, 
			--TS.tip_asig, 
			TS.cod_clase
	if @detalle='4'         -- Nivel de agrupamiento: Gerencia
		select  distinct
			TS.cod_direccion,
			nom_direccion = (select nom_direccion from Direccion where cod_direccion=TS.cod_direccion),
			TS.cod_gerencia,
			nom_gerencia = (select nom_gerencia from Gerencia where cod_gerencia=TS.cod_gerencia),
			'' as cod_sector,
			'' as nom_sector,
			'' as cod_grupo,
			'' as nom_grupo,
			'' as cod_recurso,
			'' as nom_recurso,
			--{ add -001-
			TS.tip_asig,	-- add -003- a.
			TS.cod_clase,
			classnom = (select classnom from #PETICIONES_CLASES where classcod = TS.cod_clase),
			--}
			convert(numeric(10,2),sum(TS.horas_si/6000)) as asignadas_si,
			convert(numeric(10,2),sum(TS.horas_no/6000)) as asignadas_no
			,convert(numeric(10,2),sum(TS.horas_tarea/6000)) as hs_tareas	-- add -003- a.
		from 
			#TmpSalida  TS
		group by 
			TS.cod_direccion,
			TS.cod_gerencia, 
			TS.tip_asig, 
			TS.cod_clase
		order by 
			TS.cod_direccion,
			TS.cod_gerencia, 
			--TS.tip_asig, 
			TS.cod_clase
	if @detalle='5'         -- Nivel de agrupamiento: Direcci�n
		select  distinct
			TS.cod_direccion,
			nom_direccion = (select nom_direccion from Direccion where cod_direccion=TS.cod_direccion),
			'' as cod_gerencia,
			'' as nom_gerencia,
			'' as cod_sector,
			'' as nom_sector,
			'' as cod_grupo,
			'' as nom_grupo,
			'' as cod_recurso,
			'' as nom_recurso,
			--{ add -001-
			TS.tip_asig,	-- add -003- a.
			TS.cod_clase,
			classnom = (select classnom from #PETICIONES_CLASES where classcod = TS.cod_clase),
			--}
			convert(numeric(10,2),sum(TS.horas_si/6000)) as asignadas_si,
			convert(numeric(10,2),sum(TS.horas_no/6000)) as asignadas_no,
			convert(numeric(10,2),sum(TS.horas_tarea/6000)) as hs_tareas			-- add -003- a.
		from 
			#TmpSalida  TS
		group by 
			TS.cod_direccion, 
			TS.cod_clase
		order by 
			TS.cod_direccion, 
			TS.cod_clase

	return(0)
go

grant execute on dbo.sp_rptHsTrabAreaEjec2 to GesPetUsr
go

print 'Actualizaci�n finalizada.'
go