/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. FJS 28.04.2011 - Planificación: cuando una petición es anexada a otra, dejan de existir sus grupos. Por ende, debe ser eliminada de las tablas de planificación.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_AnexarPeticion'
go

if exists (select * from sysobjects where name = 'sp_AnexarPeticion' and sysstat & 7 = 4)
	drop procedure dbo.sp_AnexarPeticion
go

create procedure dbo.sp_AnexarPeticion   
	@ori_nrointerno    int,
    @des_nrointerno    int
as
	declare @cod_estado			varchar(255) 
	declare @aux_nrointerno		int

	select @cod_estado = 'CANCEL|RECHAZ|RECHTE|TERMIN' 
 
	if not exists (select pet_nrointerno from GesPet..Peticion where pet_nrointerno = @des_nrointerno)
		begin   
			select 30010 as ErrCode ,'No existe la Peticion destino' as ErrDesc   
			raiserror 30010 'No existe la Peticion destino'   
			return (30010)   
		end
 
   if  exists (select pet_nrointerno from GesPet..Peticion where pet_nrointerno = @des_nrointerno and pet_nroanexada <> 0)
		begin
			select 30011 as ErrCode ,'La Peticion destino ya se encuentra anexada' as ErrDesc   
			raiserror 30011 'La Peticion destino ya se encuentra anexada'   
			return (30011)   
		end
 
   if  exists (select pet_nroanexada from GesPet..Peticion where pet_nroanexada = @ori_nrointerno)
		begin   
			select 30012 as ErrCode ,'La Peticion posee peticiones anexadas' as ErrDesc
			raiserror 30012 'La Peticion posee peticiones anexadas'
			return (30012)
		end   
    
   if exists (select cod_sector from GesPet..PeticionSector where pet_nrointerno = @ori_nrointerno and 
        cod_sector not in (select cod_sector from GesPet..PeticionSector where pet_nrointerno = @des_nrointerno))   
		begin
			select 30013 as ErrCode ,'La peticion origen posee sectores que no existen en la Peticion destino' as ErrDesc   
			raiserror 30013 'La peticion origen posee sectores que no existen en la Peticion destino'   
			return (30013)   
		end
 
   if exists (select cod_grupo from GesPet..PeticionGrupo where pet_nrointerno = @ori_nrointerno and 
        cod_grupo not in (select cod_grupo from GesPet..PeticionGrupo where pet_nrointerno = @des_nrointerno))
		begin   
			select 30014 as ErrCode ,'La peticion origen posee grupos que no existen en la Peticion destino' as ErrDesc
			raiserror 30014 'La peticion origen posee grupos que no existen en la Peticion destino'
			return (30014)
		end
      
	BEGIN TRAN 
		update GesPet..Peticion
		set pet_nroanexada = @des_nrointerno, 
			cod_estado = 'ANEXAD', 
			cod_situacion = '', 
			ult_accion  = '', 
			fe_estado = getdate(), 
			audit_user = SUSER_NAME(),   
			audit_date = getdate()   
		where  pet_nrointerno = @ori_nrointerno   
		 
		execute sp_DeleteMensaje 
			@pet_nrointerno=@ori_nrointerno, 
			@msg_nrointerno=0 
		 
		execute sp_DoMensaje 
			@evt_alcance='PET', 
			@cod_accion='PCHGEST', 
			@pet_nrointerno=@ori_nrointerno, 
			@cod_hijo='', 
			@dsc_estado='ANEXAD', 
			@txt_txtmsg='', 
			@txt_txtprx='' 
		 
		/* anexa las horas */ 
		execute sp_AnexarHoras 
			@ori_nrointerno=@ori_nrointerno, 
			@des_nrointerno=@des_nrointerno 
		 
		/* ahora elimina los sectores originales  */ 
		delete from GesPet..PeticionSector 
		where pet_nrointerno = @ori_nrointerno 
		 
		delete from GesPet..PeticionGrupo 
		where  pet_nrointerno = @ori_nrointerno 

	COMMIT TRAN 

	return(0)   
go

grant execute on dbo.sp_AnexarPeticion to GesPetUsr 
go

print 'Actualización realizada.'
go