/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 11.08.2014 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateEstadoSolicitud'
go

if exists (select * from sysobjects where name = 'sp_UpdateEstadoSolicitud' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateEstadoSolicitud
go

create procedure dbo.sp_UpdateEstadoSolicitud
	@cod_estado	char(1),
	@nom_estado	char(50)
as
	update 
		GesPet.dbo.EstadoSolicitud
	set
		nom_estado = @nom_estado
	where 
		cod_estado = @cod_estado
go

grant execute on dbo.sp_UpdateEstadoSolicitud to GesPetUsr
go

print 'Actualización realizada.'
go
