/*
-000- a. FJS 29.05.2008 - Nuevo SP para utilizar en el informe 'Peticiones con fecha prevista de pasaje a Producci�n y la futura publicaci�n en intranet de estas peticiones.
-001- a. FJS 24.06.2008 - Se agregan a la temporal #TpoPet los tipos para Auditor�a.
-002- a. FJS 08.08.2008 - Se dejan de visualizar grupos en estados terminales
-003- a. FJS 02.12.2008 - Se agrega al condicionamiento, que no se visualicen grupos en estado 'Suspendido temporalmente' (SUSPEN).
-003- b. FJS 02.12.2008 - Se agregan dos nuevas columnas para mostrar las horas por responsable y recurso para un grupo en particular.
-004- a. FJS 15.01.2009 - Se modifica para soportar parametr�a respecto de la cantidad de d�as a contabilizar.
-005- a. FJS 05.06.2009 - Se agregan par�metros para especificar un rango de fechas.
-006- a. FJS 08.07.2009 - BUG: reportado por Barbarito. Las horas por responsable y por recurso, no estan discriminando por grupos (estan trayendo los totales).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionesPublicacion'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionesPublicacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionesPublicacion
go

create procedure dbo.sp_GetPeticionesPublicacion
	--{ add -005- a.
	@fecha_desde	varchar(8)='NULL',
	@fecha_hasta	varchar(8)='NULL'
	--}
as 
	--{ add -004- a.
	declare @dias	int
	
	select @dias = (select var_numero from GesPet..Varios where var_codigo = 'RPT8')
	if @dias = 0 or @dias is null
		begin
			select @dias = 15			-- Inicializaci�n por defecto: 15 d�as
		end
	--}

	-- Temporal para descripci�n de Tipo de Petici�n
	create table #TpoPet
	(
		cod_tipo_peticion	char(3)		null,
		nom_tipo_peticion	char(30)	null
	)
	insert into #TpoPet (cod_tipo_peticion, nom_tipo_peticion) values ('NOR', 'Normal')
	insert into #TpoPet (cod_tipo_peticion, nom_tipo_peticion) values ('PRJ', 'Proyecto')
	insert into #TpoPet (cod_tipo_peticion, nom_tipo_peticion) values ('ESP', 'Especial')
	insert into #TpoPet (cod_tipo_peticion, nom_tipo_peticion) values ('PRO', 'Propia')
	insert into #TpoPet (cod_tipo_peticion, nom_tipo_peticion) values ('AUX', 'Audit. Externa')
	insert into #TpoPet (cod_tipo_peticion, nom_tipo_peticion) values ('AUI', 'Audit. Interna')

	-- Temporal para descripci�n de Clase de Petici�n
	create table #ClasePet
	(
		cod_clase	char(4)		null,
		nom_clase	char(30)	null
	)
	insert into #ClasePet (cod_clase, nom_clase) values ('SINC', 'Sin clase')
	insert into #ClasePet (cod_clase, nom_clase) values ('CORR', 'Mant. Correctivo')
	insert into #ClasePet (cod_clase, nom_clase) values ('ATEN', 'Atenci�n a usuario')
	insert into #ClasePet (cod_clase, nom_clase) values ('OPTI', 'Optimizaci�n')
	insert into #ClasePet (cod_clase, nom_clase) values ('EVOL', 'Mant. evolutivo')
	insert into #ClasePet (cod_clase, nom_clase) values ('NUEV', 'Nuevo desarrollo')
	insert into #ClasePet (cod_clase, nom_clase) values ('SPUF', 'SPUFI')

	-- Temporal para descripci�n de Homologaci�n
	create table #Homologacion
	(
		cod_homo	char(1)		null,
		nom_homo	char(30)	null
	)
	insert into #Homologacion (cod_homo, nom_homo) values ('N', 'No')
	insert into #Homologacion (cod_homo, nom_homo) values ('S', 'Si')
	insert into #Homologacion (cod_homo, nom_homo) values ('H', 'Homologaci�n')

    select
        PET.pet_nroasignado,
		PET.titulo,
		nom_tipo_peticion = (select nom_tipo_peticion from #TpoPet where cod_tipo_peticion = PET.cod_tipo_peticion),
		nom_clase = (select nom_clase from #ClasePet where cod_clase = PET.cod_clase),
        PGR.cod_sector,
		nom_sector = (select nom_sector from GesPet..Sector where cod_sector = PGR.cod_sector),
        PGR.cod_grupo,
		nom_grupo = (select nom_grupo from GesPet..Grupo where cod_grupo = PGR.cod_grupo),
        PGR.horaspresup,
        PGR.cod_estado,
		nom_estado = (select nom_estado from GesPet..Estados where cod_estado = PGR.cod_estado),
        PGR.fe_estado,
        PGR.fe_produccion,
		homologacion = (select nom_homo from #Homologacion where GRP.grupo_homologacion = cod_homo),
		--{ add -002- b.
		horas_resp = (
			select 
				sum(cast(HRS.horas as float(1)))/60 
			from 
				GesPet..HorasTrabajadas HRS left join 
				GesPet..Recurso R on HRS.cod_recurso = R.cod_recurso	-- upd -006- a. Join
			where 
				HRS.pet_nrointerno = PGR.pet_nrointerno and 
				R.cod_grupo = PGR.cod_grupo and		-- add -006- a.
				HRS.cod_recurso in (select PRF.cod_recurso from GesPet..RecursoPerfil PRF where PRF.cod_recurso = HRS.cod_recurso and PRF.cod_perfil in ('CSEC','CGRU','BPAR','CDIR','CGER'))),
		horas_recu = (
			select 
				sum(cast(HRS.horas as float(1)))/60 
			from 
				GesPet..HorasTrabajadas HRS left join 
				GesPet..Recurso R on HRS.cod_recurso = R.cod_recurso	-- upd -006- a. Join
			where 
				HRS.pet_nrointerno = PGR.pet_nrointerno and 
				R.cod_grupo = PGR.cod_grupo and		-- add -006- a.
				HRS.cod_recurso not in (select PRF.cod_recurso from GesPet..RecursoPerfil PRF where PRF.cod_recurso = HRS.cod_recurso and PRF.cod_perfil in ('CSEC','CGRU','BPAR','CDIR','CGER'))),
		horas_total = (
			select 
				sum(cast(HRS.horas as float(1)))/60 
			from 
				GesPet..HorasTrabajadas HRS left join 
				GesPet..Recurso R on HRS.cod_recurso = R.cod_recurso	-- upd -006- a. Join
			where 
				HRS.pet_nrointerno = PGR.pet_nrointerno and 
				R.cod_grupo = PGR.cod_grupo)		-- add -006- a.
		--}
    from
        GesPet.dbo.PeticionGrupo PGR inner join 
		GesPet.dbo.Grupo GRP on (PGR.cod_grupo = GRP.cod_grupo) inner join
		GesPet.dbo.Peticion PET on (PGR.pet_nrointerno = PET.pet_nrointerno)
    where
		PGR.cod_gerencia = 'DESA' and
		--{ add -002- a.
		charindex(PGR.cod_estado,'ANULAD|CANCEL|TERMIN|RECHAZ|RECHTE|ANEXAD|SUSPEN|') = 0 and		-- upd -003- a. Se agreg� [|ANEXAD|SUSPEN]
		--}
		--convert(varchar,PGR.fe_produccion,112) >= convert(varchar,getdate(),112) and		-- *** RESTAURAR!!! ***
        PGR.fe_produccion is not null 
        --and datediff(day, getdate(), PGR.fe_produccion) <= @dias		-- upd -004- a. Se cambia la constante num�rica 15 por la variable	-- del -005- a.
		--{ add -005- a.
		and
			(
				(rtrim(@fecha_desde) = 'NULL' or convert(char(8),PGR.fe_produccion,112) >= @fecha_desde) and
				(rtrim(@fecha_hasta) = 'NULL' or convert(char(8),PGR.fe_produccion,112) <= @fecha_hasta)
			)
		--}
	order by 
		PGR.fe_produccion
return(0) 
go

grant execute on dbo.sp_GetPeticionesPublicacion to GesPetUsr
go

print 'Actualizaci�n realizada.'
