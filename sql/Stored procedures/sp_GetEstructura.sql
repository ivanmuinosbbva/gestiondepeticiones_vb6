/*
-001- a. FJS 15.03.2010 - Nuevo SP para obtener la estructura de un �rea con los responsable de cada sub-�rea.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetEstructura'
go

if exists (select * from sysobjects where name = 'sp_GetEstructura' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetEstructura
go

create procedure dbo.sp_GetEstructura
	@cod_direccion	char(8)='NULL',
	@cod_gerencia	char(8)='NULL',
	@cod_sector		char(8)='NULL',
	@cod_grupo		char(8)='NULL',
	@flg_habil		char(4)='NULL'
as
	select
		a.cod_grupo,
		a.nom_grupo,
		grupo_resp = (
			select 
				x.nom_recurso 
			from GesPet..Recurso x 
			where 
				x.cod_direccion = d.cod_direccion and 
				x.cod_gerencia = c.cod_gerencia and 
				x.cod_sector = b.cod_sector and 
				x.cod_grupo = a.cod_grupo and 
				x.estado_recurso = 'A' and
				x.flg_cargoarea = 'S'),
		b.cod_sector,
		b.nom_sector,
		sector_resp = (
			select 
				max(x.nom_recurso)
			from GesPet..Recurso x 
			where 
				x.cod_direccion = d.cod_direccion and 
				x.cod_gerencia = c.cod_gerencia and 
				x.cod_sector = b.cod_sector and 
				x.cod_grupo = '' and
				x.estado_recurso = 'A' and
				x.flg_cargoarea = 'S'),
		c.cod_gerencia,
		c.nom_gerencia,
		gerencia_resp = (
			select 
				max(x.nom_recurso)
			from GesPet..Recurso x 
			where 
				x.cod_direccion = d.cod_direccion and 
				x.cod_gerencia = c.cod_gerencia and 
				x.cod_sector = '' and
				x.estado_recurso = 'A' and
				x.flg_cargoarea = 'S'),
		d.cod_direccion,
		d.nom_direccion,
		direccion_resp = (
			select 
				max(x.nom_recurso)
			from GesPet..Recurso x 
			where 
				x.cod_direccion = d.cod_direccion and 
				x.cod_gerencia = '' and
				x.estado_recurso = 'A' and
				x.flg_cargoarea = 'S')
	from
		Direccion d left join
		Gerencia c on (c.cod_direccion = d.cod_direccion) left join
		Sector b on (b.cod_gerencia = c.cod_gerencia) left join
		Grupo a on (a.cod_sector = b.cod_sector)
		/*
		GesPet..Grupo a inner join 
		GesPet..Sector b on (a.cod_sector = b.cod_sector) inner join
		GesPet..Gerencia c on (b.cod_gerencia = c.cod_gerencia) inner join
		GesPet..Direccion d on (c.cod_direccion = d.cod_direccion)
		*/
	where
		(rtrim(ltrim(@cod_direccion)) = 'NULL' or @cod_direccion = d.cod_direccion) and
		(rtrim(ltrim(@cod_gerencia)) = 'NULL' or @cod_gerencia = c.cod_gerencia) and
		(rtrim(ltrim(@cod_sector)) = 'NULL' or @cod_sector = b.cod_sector) and
		(rtrim(ltrim(@cod_grupo)) = 'NULL' or @cod_grupo = a.cod_grupo) and
		(rtrim(ltrim(@flg_habil)) = 'NULL' or @flg_habil = a.flg_habil) and
		(rtrim(ltrim(@flg_habil)) = 'NULL' or @flg_habil = b.flg_habil) and
		(rtrim(ltrim(@flg_habil)) = 'NULL' or @flg_habil = c.flg_habil) and
		(rtrim(ltrim(@flg_habil)) = 'NULL' or @flg_habil = d.flg_habil)
go

grant execute on dbo.sp_GetEstructura to GesPetUsr
go

print 'Actualizaci�n realizada.'
go