/*
-000- a. FJS 11.12.2008 - Nuevo SP para corregir problemas en campos de la tabla del historial.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateHistorialField'
go

if exists (select * from sysobjects where name = 'sp_UpdateHistorialField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateHistorialField
go

create procedure dbo.sp_UpdateHistorialField
    @hst_nrointerno		int,
	@cod_campo			char(10),
	@valor_numero		int,
	@valor_texto		char(10),
	@valor_fecha		smalldatetime
as 
	if ltrim(rtrim(@cod_campo)) = 'hst_fecha'
		begin
			update  
				GesPet..Historial
			set  
				hst_fecha = @valor_fecha
			where 
				hst_nrointerno = @hst_nrointerno
		end
	if ltrim(rtrim(@cod_campo)) = 'cod_evento'
		begin
			update  
				GesPet..Historial
			set  
				cod_evento = @valor_texto
			where 
				hst_nrointerno = @hst_nrointerno
		end
	if ltrim(rtrim(@cod_campo)) = 'pet_estado'
		begin
			update  
				GesPet..Historial
			set  
				pet_estado = @valor_texto
			where 
				hst_nrointerno = @hst_nrointerno
		end
	if ltrim(rtrim(@cod_campo)) = 'cod_sector'
		begin
			update  
				GesPet..Historial
			set  
				cod_sector = @valor_texto
			where 
				hst_nrointerno = @hst_nrointerno
		end
	if ltrim(rtrim(@cod_campo)) = 'sec_estado'
		begin
			update  
				GesPet..Historial
			set  
				sec_estado = @valor_texto
			where 
				hst_nrointerno = @hst_nrointerno
		end
	if ltrim(rtrim(@cod_campo)) = 'cod_grupo'
		begin
			update  
				GesPet..Historial
			set  
				cod_grupo = @valor_texto
			where 
				hst_nrointerno = @hst_nrointerno
		end
	if ltrim(rtrim(@cod_campo)) = 'gru_estado'
		begin
			update  
				GesPet..Historial
			set  
				gru_estado = @valor_texto
			where 
				hst_nrointerno = @hst_nrointerno
		end
	if ltrim(rtrim(@cod_campo)) = 'replc_user'
		begin
			update  
				GesPet..Historial
			set  
				replc_user = @valor_texto
			where 
				hst_nrointerno = @hst_nrointerno
		end
	if ltrim(rtrim(@cod_campo)) = 'audit_user'
		begin
			update  
				GesPet..Historial
			set  
				audit_user = @valor_texto
			where 
				hst_nrointerno = @hst_nrointerno
		end
return(0)
go

grant execute on dbo.sp_UpdateHistorialField to GesPetUsr
go

print 'Actualización realizada.'