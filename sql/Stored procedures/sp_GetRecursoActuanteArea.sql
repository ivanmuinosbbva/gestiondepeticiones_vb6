use GesPet
go
print 'Actualizando: sp_GetRecursoActuanteArea'
go
if exists (select * from sysobjects where name = 'sp_GetRecursoActuanteArea' and sysstat & 7 = 4)
drop procedure dbo.sp_GetRecursoActuanteArea
go

create procedure dbo.sp_GetRecursoActuanteArea 
    @cod_direccion      varchar(8)=NULL,  
    @cod_gerencia       varchar(8)=NULL,  
    @cod_sector         varchar(8)=NULL,  
    @cod_grupo          varchar(8)=NULL,  
    @perfil				varchar(8)=NULL  
as  

select
        a.cod_recurso,
        a.nom_recurso,
        a.cod_direccion,
        nom_direccion = (select nom_direccion from GesPet..Direccion where cod_direccion = a.cod_direccion),
        a.cod_gerencia,
        nom_gerencia = (select nom_gerencia from GesPet..Gerencia where cod_gerencia = a.cod_gerencia),
        a.cod_sector,
        nom_sector = (select nom_sector from GesPet..Sector where cod_sector = a.cod_sector),
        a.cod_grupo,
        nom_grupo = (select nom_grupo from GesPet..Grupo where cod_grupo = a.cod_grupo),
        b.cod_perfil,
        b.cod_nivel,
        b.cod_area
    from
        GesPet..Recurso a left join GesPet..RecursoPerfil b on (a.cod_recurso = b.cod_recurso)
    where
        a.estado_recurso = 'A' and
        b.cod_perfil     = @perfil and
        ((a.cod_direccion = '' or a.cod_direccion = @cod_direccion) or
        (
            (b.cod_nivel = 'DIRE' and b.cod_area = @cod_direccion) or
            (b.cod_nivel = 'GERE' and b.cod_area = @cod_gerencia) or
            (b.cod_nivel = 'SECT' and b.cod_area = @cod_sector) or
            (b.cod_nivel = 'GRUP' and b.cod_area = @cod_grupo)
        )) and
        ((a.cod_gerencia = '' or a.cod_gerencia = @cod_gerencia) or
        (
            (b.cod_nivel = 'DIRE' and b.cod_area = @cod_direccion) or
            (b.cod_nivel = 'GERE' and b.cod_area = @cod_gerencia) or
            (b.cod_nivel = 'SECT' and b.cod_area = @cod_sector) or
            (b.cod_nivel = 'GRUP' and b.cod_area = @cod_grupo)
        )) and
        ((a.cod_sector = '' or a.cod_sector = @cod_sector) or
        (
            (b.cod_nivel = 'DIRE' and b.cod_area = @cod_direccion) or
            (b.cod_nivel = 'GERE' and b.cod_area = @cod_gerencia) or
            (b.cod_nivel = 'SECT' and b.cod_area = @cod_sector) or
            (b.cod_nivel = 'GRUP' and b.cod_area = @cod_grupo)
        )) and
        ((a.cod_grupo = '' or a.cod_grupo = @cod_grupo) or
        (
            (b.cod_nivel = 'DIRE' and b.cod_area = @cod_direccion) or
            (b.cod_nivel = 'GERE' and b.cod_area = @cod_gerencia) or
            (b.cod_nivel = 'SECT' and b.cod_area = @cod_sector) or
            (b.cod_nivel = 'GRUP' and b.cod_area = @cod_grupo)
        ))
	order by
		a.cod_direccion, 
		a.cod_gerencia, 
		a.cod_sector, 
		a.cod_grupo

return(0)  

go

grant execute on dbo.sp_GetRecursoActuanteArea to GesPetUsr 
go

print 'Actualización finalizada.'