/*

-001- FJS 22.08.2007 - Se agrega el �ndice para utilizar correctamente la estructura de la tabla

*/

use GesPet
go
print 'sp_GetVarios'
go
if exists (select * from sysobjects where name = 'sp_GetVarios' and sysstat & 7 = 4)
drop procedure dbo.sp_GetVarios
go
create procedure dbo.sp_GetVarios 
             @var_codigo        char(8)=null 
as 
if @var_codigo is null 
begin 
    select var_codigo, 
        var_numero, 
        var_texto, 
        var_fecha 
    from GesPet..Varios 
    order by var_codigo, var_numero	-- add -001- 
end 
else 
begin 
    if exists (select var_codigo  from GesPet..Varios where RTRIM(var_codigo) = RTRIM(@var_codigo))   
    begin 
        select var_codigo, 
            var_numero, 
            var_texto, 
            var_fecha 
        from GesPet..Varios 
        where RTRIM(var_codigo) = RTRIM(@var_codigo) 
	order by var_codigo, var_numero	-- add -001- 
    end 
    else 
    begin 
        select var_codigo=@var_codigo, 
            var_numero=0, 
            var_texto='', 
            var_fecha=null 
    end 
end 
return(0) 
go

grant execute on dbo.sp_GetVarios to GesPetUsr 
go


