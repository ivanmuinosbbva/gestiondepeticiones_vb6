use GesPet
go
print 'sp_UpdateProyectoBalanceCab'
go
if exists (select * from sysobjects where name = 'sp_UpdateProyectoBalanceCab' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateProyectoBalanceCab
go
create procedure dbo.sp_UpdateProyectoBalanceCab
	@prj_nrointerno		int,
	@cod_PerfilCosto	char(8),
	@cod_PerfilBenef	char(8),
	@txt_Beneficio		char(250)
as
if not exists (select cod_PerfilCostBenef from PerfilCostBenef where cod_PerfilCostBenef=@cod_PerfilCosto)
	begin 
		raiserror 30011 'Error codigo perfil Costo'   
		select 30011 as ErrCode , 'Error codigo perfil Costo' as ErrDesc   
		return (30011)   
	end 
if not exists (select cod_PerfilCostBenef from PerfilCostBenef where cod_PerfilCostBenef=@cod_PerfilBenef)
	begin 
		raiserror 30011 'Error codigo perfil Beneficio'   
		select 30011 as ErrCode , 'Error codigo perfil Beneficio' as ErrDesc   
		return (30011)   
	end 

if exists (select prj_nrointerno from GesPet..ProyectoBalanceCab where prj_nrointerno = @prj_nrointerno)
	begin
		update ProyectoBalanceCab set
			cod_PerfilCosto = @cod_PerfilCosto,
			cod_PerfilBenef = @cod_PerfilBenef,
			txt_Beneficio   = @txt_Beneficio,
			audit_user      = SUSER_NAME(),
			audit_date      = getdate()
		where	prj_nrointerno  = @prj_nrointerno
	end
else
	begin
	  insert into GesPet..ProyectoBalanceCab 
		    (prj_nrointerno,
			cod_PerfilCosto,
			cod_PerfilBenef,
			txt_Beneficio,
			audit_user,
			audit_date)
			values
			(@prj_nrointerno,
			@cod_PerfilCosto,
			@cod_PerfilBenef,
			@txt_Beneficio,
			SUSER_NAME(),
			getdate())
end


return(0)
go

grant execute on dbo.sp_UpdateProyectoBalanceCab to GesPetUsr
go
