use GesPet
go
print 'sp_InsertTareaSector'
go
if exists (select * from sysobjects where name = 'sp_InsertTareaSector' and sysstat & 7 = 4)
drop procedure dbo.sp_InsertTareaSector
go
create procedure dbo.sp_InsertTareaSector 
    @cod_tarea          char(8), 
    @cod_sector     char(8) 
as 
 
if not exists (select cod_sector from GesPet..TareaSector where RTRIM(cod_sector) = RTRIM(@cod_sector) and RTRIM(cod_tarea) = RTRIM(@cod_tarea)) 
begin 
      insert into GesPet..TareaSector 
             (cod_sector, cod_tarea) 
      values 
             (@cod_sector, @cod_tarea) 
end 
return(0) 
go



grant execute on dbo.sp_InsertTareaSector to GesPetUsr 
go


