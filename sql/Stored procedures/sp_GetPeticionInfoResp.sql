/*  C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 28.08.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionInfoResp'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionInfoResp' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionInfoResp
go

create procedure dbo.sp_GetPeticionInfoResp
	@pet_nrointerno	int
as
	-- Responsables de ejecuci�n + referente de sistema de la petici�n
	SELECT 
		r.cod_recurso, 
		r.nom_recurso,
		'GRUP'		as nivel,
		r.cod_grupo	as area
	FROM Recurso r
	WHERE 
		r.cod_grupo in (
			select pg.cod_grupo
			from PeticionGrupo pg
			where 
				pg.pet_nrointerno = @pet_nrointerno and
				charindex(pg.cod_estado,'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0) and
		r.flg_cargoarea= 'S' and r.estado_recurso = 'A'
	UNION
	SELECT 
		r.cod_recurso, 
		r.nom_recurso,
		'BBVA'		as nivel,
		'BBVA'		as area
	FROM 
		Recurso r inner join
		Peticion p on (p.cod_bpar = r.cod_recurso)
	WHERE
		p.pet_nrointerno = @pet_nrointerno and 
		r.cod_recurso = p.cod_bpar

	/*
	drop table #tempRecu

	create table #tempRecu (
		cod_recurso	char(10)	not null,
		nom_recurso	char(100)	null,
		cod_perfil	char(4)		null,
		cod_nivel	char(4)		null,
		cod_area	char(8)		null)
	
	-- Solicitante
	INSERT INTO  #tempRecu
		SELECT r.cod_recurso, r.nom_recurso, 'SOLI', 'SECT', p.cod_sector
		FROM
			Peticion p inner join
			Recurso r on (r.cod_direccion = p.cod_direccion and r.cod_gerencia = p.cod_gerencia and r.cod_sector = p.cod_sector and r.cod_recurso = p.cod_solicitante)
		WHERE
			p.pet_nrointerno = @pet_nrointerno

	-- Referentes de sistema (todos)
	INSERT INTO  #tempRecu
		SELECT r.cod_recurso, r.nom_recurso, 'BPAR', 'BBVA', 'BBVA'
		FROM Recurso r 
		WHERE r.cod_recurso IN (
			select rp.cod_recurso
			from RecursoPerfil rp
			where rp.cod_perfil = 'BPAR')

	-- Responsables de sector (todos los de la petici�n)
	INSERT INTO  #tempRecu
		SELECT r.cod_recurso, r.nom_recurso, 'CSEC', 'SECT', ps.cod_sector
		FROM
			PeticionSector ps inner join
			Recurso r on (r.cod_direccion = ps.cod_direccion and r.cod_gerencia = ps.cod_gerencia and r.cod_sector = ps.cod_sector)
		WHERE
			ps.pet_nrointerno = @pet_nrointerno and
			r.flg_cargoarea = 'S' and
			r.cod_grupo = ''

	-- Responsables de ejecuci�n (todos los de la petici�n)
	INSERT INTO  #tempRecu
		SELECT r.cod_recurso, r.nom_recurso, 'CGRU', 'GRUP', pg.cod_grupo
		FROM 
			PeticionGrupo pg inner join
			Recurso r on (r.cod_direccion = pg.cod_direccion and r.cod_gerencia = pg.cod_gerencia and r.cod_sector = pg.cod_sector and r.cod_grupo = pg.cod_grupo)
		WHERE
			pg.pet_nrointerno = @pet_nrointerno and
			r.flg_cargoarea = 'S'
	
	SELECT
		tp.cod_recurso,
		tp.nom_recurso,
		tp.cod_perfil,
		tp.cod_nivel,
		tp.cod_area
	from #tempRecu tp
	order by 3
	*/
go

grant execute on dbo.sp_GetPeticionInfoResp to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
