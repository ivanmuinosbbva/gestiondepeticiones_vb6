/*
-000- a. FJS dd.mm.yyyy - Nuevo SP para
*/

use GesPet
go

print 'Creando/actualizando SP: sp_CambioPassword'
go

if exists (select * from sysobjects where name = 'sp_CambioPassword' and sysstat & 7 = 4)
	drop procedure dbo.sp_CambioPassword
go

create procedure dbo.sp_CambioPassword
	@recurso	char(10),
	@password	varchar(30),
	@componente	varchar(30)
as 
	declare @nueva	varchar(60)

	select @nueva = @password + @componente
	--use master
	--
	alter login @recurso modify password immediately @nueva
go

grant execute on dbo.sp_CambioPassword to GesPetUsr
go

print 'Actualización realizada.'
go
