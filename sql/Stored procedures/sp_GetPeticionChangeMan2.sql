/*
-001- a. FJS 04.06.2008 - Se agrega la cl�usula ORDER BY.
-002- a. FJS 23.02.2010 - Se agrega el estado de la petici�n.
-003- a. FJS 24.02.2010 - Se agrega el modo: 1 para peticiones v�lidas para pasaje a Producci�n a fecha y 2 para las hist�ricas (que ya finalizaron y vencieron)
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionChangeMan2'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionChangeMan2' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionChangeMan2
go

create procedure dbo.sp_GetPeticionChangeMan2 (
	@modo				char(1),		-- add -003- a.
	@pet_nrointerno		int,    
    @pet_sector			char(8),    
    @pet_grupo			char(8))
as     
	--{ add -003- a.
	declare @FechaHoy	varchar(8)  
	declare @Dias		int 

	set @Dias = (select var_numero from GesPet..Varios where var_codigo = 'CGMPRM1')

	if @modo = '1' or @modo is null
		begin
	--}
			select  
				Pet.pet_nroasignado,
				Pet.titulo,
				Pet.cod_tipo_peticion,
				Pet.cod_clase,
				Pet.cod_estado,		-- add -002- a.
				(select nom_estado from GesPet..Estados where cod_estado = Pet.cod_estado) as nom_estado,
				Pet.fe_estado,
				Chg.pet_nrointerno, 
				Chg.pet_sector, 
				Chg.pet_grupo, 
				Chg.pet_date, 
				Chg.pet_record 
			from  
				GesPet..PeticionChangeMan Chg inner join 
				GesPet..Peticion Pet on Chg.pet_nrointerno = Pet.pet_nrointerno
			where 
				(Chg.pet_nrointerno = @pet_nrointerno OR @pet_nrointerno IS NULL) and  
				(Chg.pet_sector = @pet_sector OR @pet_sector IS NULL) and  
				(Chg.pet_grupo = @pet_grupo OR @pet_grupo IS NULL) 
			order by
				Pet.pet_nroasignado,
				Chg.pet_date
	--{ add -003- a.
		end
	else
		begin
			select  
				Pet.pet_nroasignado,
				Pet.titulo,
				Pet.cod_tipo_peticion,
				Pet.cod_clase,
				Pet.cod_estado,
				(select nom_estado from GesPet..Estados where cod_estado = Pet.cod_estado) as nom_estado,
				Pet.fe_estado,
				Env.pet_nrointerno, 
				Env.pet_sector, 
				Env.pet_grupo, 
				Env.pet_date, 
				Env.pet_record, 
				Env.pet_usrid,
				(select  nom_recurso from GesPet..Recurso where cod_recurso = Env.pet_usrid) as nom_recurso, 
				Env.pet_done 
			from  
				GesPet..PeticionEnviadas Env left Join 
				GesPet..Peticion Pet on Env.pet_nrointerno = Pet.pet_nrointerno
			where 
				(Env.pet_nrointerno = @pet_nrointerno OR @pet_nrointerno IS NULL) and  
				(Env.pet_sector = @pet_sector OR @pet_sector IS NULL) and  
				(Env.pet_grupo = @pet_grupo OR @pet_grupo IS NULL) and 
				Env.pet_nrointerno not in (select x.pet_nrointerno from GesPet..PeticionChangeMan x)
				/*
				Pet.cod_estado in ('RECHAZ','CANCEL','RECHTE','TERMIN','ANEXAD','ANULAD') and
				datediff(day, Pet.fe_estado, getdate()) > @Dias
				*/
			order by
				Pet.pet_nroasignado,
				Env.pet_date
		end
	--}
	return(0) 
go

grant Execute  on dbo.sp_GetPeticionChangeMan2 to GesPetUsr 
go

print 'Actualizaci�n realizada.'