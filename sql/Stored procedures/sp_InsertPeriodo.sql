/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 15.09.2010 - 
-001- a. FJS 09.12.2010 - Se agrega la abreviatura del período.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeriodo'
go

if exists (select * from sysobjects where name = 'sp_InsertPeriodo' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeriodo
go

create procedure dbo.sp_InsertPeriodo
	@per_nrointerno	int,
	@per_nombre		char(50),
	@per_tipo		char(1),
	@fe_desde		smalldatetime,
	@fe_hasta		smalldatetime,
	@per_estado		char(6),
	@per_abrev		char(15)

as
	insert into GesPet.dbo.Periodo (
		per_nrointerno,
		per_nombre,
		per_tipo,
		fe_desde,
		fe_hasta,
		per_estado,
		per_ultfchest,
		per_ultusrid,
		per_abrev)
	values (
		@per_nrointerno,
		@per_nombre,
		@per_tipo,
		@fe_desde,
		@fe_hasta,
		@per_estado,
		null,
		null,
		@per_abrev)
	return(0)
go

grant execute on dbo.sp_InsertPeriodo to GesPetUsr
go

print 'Actualización realizada.'
go