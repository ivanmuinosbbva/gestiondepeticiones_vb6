use GesPet
go

print 'Creando/actualizando SP: sp_GetRecursosPorAreaDyd'
go

if exists (select * from sysobjects where name = 'sp_GetRecursosPorAreaDyd' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRecursosPorAreaDyd
go

create procedure dbo.sp_GetRecursosPorAreaDyd
	@cod_perfil		char(4)
as
	select
		a.cod_recurso,
		a.nom_recurso,
		a.cod_direccion,
		nom_direccion = (select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = a.cod_direccion),
		a.cod_gerencia,
		nom_gerencia = (select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = a.cod_gerencia),
		a.cod_sector,
		nom_sector = (select x.nom_sector from GesPet..Sector x where x.cod_sector = a.cod_sector),
		a.cod_grupo,
		nom_grupo = (select x.nom_grupo from GesPet..Grupo x where x.cod_grupo = a.cod_grupo)
	from 
		GesPet..Recurso a inner join 
		GesPet..RecursoPerfil b on a.cod_recurso = b.cod_recurso
	where 
		a.cod_gerencia = 'DESA' and
		b.cod_perfil = @cod_perfil and 
		a.flg_cargoarea = 'S'
	order by
		a.nom_recurso

	return(0)
go

grant execute on dbo.sp_GetRecursosPorAreaDyd to GesPetUsr
go

print 'Actualización realizada.'
go
