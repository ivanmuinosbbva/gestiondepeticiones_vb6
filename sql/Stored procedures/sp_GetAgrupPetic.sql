use GesPet
go
print 'sp_GetAgrupPetic'
go
if exists (select * from sysobjects where name = 'sp_GetAgrupPetic' and sysstat & 7 = 4)
drop procedure dbo.sp_GetAgrupPetic
go
create procedure dbo.sp_GetAgrupPetic 
             @agr_nrointerno   int=0,
             @pet_nrointerno   int=0
as 
begin 
    select  
	AP.agr_nrointerno,
	AP.pet_nrointerno,
	AG.agr_titulo,
	AG.agr_vigente,
	AG.cod_direccion,
	AG.cod_gerencia,
	AG.cod_sector,
	AG.cod_grupo,
	AG.cod_usualta,
	nom_usualta = (select R.nom_recurso from Recurso R where R.cod_recurso=AG.cod_usualta),
	AG.agr_visibilidad,
	AG.agr_actualiza,
	pet_titulo=(select P.titulo from Peticion P where P.pet_nrointerno=AP.pet_nrointerno),
	pet_nroasignado=(select P.pet_nroasignado from Peticion P where P.pet_nrointerno=AP.pet_nrointerno)
    from 
        AgrupPetic AP, Agrup AG
    where   
        (@agr_nrointerno = 0 or AP.agr_nrointerno = @agr_nrointerno) and
        (@pet_nrointerno = 0 or AP.pet_nrointerno = @pet_nrointerno) and
	(AG.agr_nrointerno=AP.agr_nrointerno)
    order by AP.agr_nrointerno,AP.pet_nrointerno ASC  
end 
return(0) 
go



grant execute on dbo.sp_GetAgrupPetic to GesPetUsr 
go
