/*
-000- a. FJS 16.04.2015 - Nuevo: crea de manera autom�tica el encabezado y detalle de un informe de homologaci�n.
-001- a. FJS 05.08.2015 - Nuevo: se modifica para que el alta manual no contemple ninguna automatizaci�n.
-002- a. FJS 16.09.2015 - Se agrega que copie las observaciones del informe anterior cuando se genera uno nuevo.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionInformeManual'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionInformeManual' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionInformeManual
go

create procedure dbo.sp_InsertPeticionInformeManual
	@pet_nrointerno		int,
	@cod_recurso		char(10),
	@infoprot_id		int
as
	-- Variables
	declare @copia_info_id		int
	declare @clase				char(4)
	declare @texto				varchar(255)
	declare @hst_nrointerno		int
	declare @evento				char(8)
	declare @pet_estado			char(6)
	declare @tipo_proceso		char(8)
	declare @nombre_informe		varchar(255)

	-- Obtengo el pr�xmo nro. para le informe a generar.
	declare @proxnumero		int 
	select @proxnumero = 0 
	execute sp_ProximoNumero 'ULPETHOM', @proxnumero OUTPUT 

	select 
		@clase		= p.cod_clase,
		@pet_estado = p.cod_estado
	from Peticion p
	where p.pet_nrointerno = @pet_nrointerno
	
	-- Carga en @copia_info_id el id de informe anterior para la petici�n que est� en confecci�n o verificado.
	select @copia_info_id = max(pc.info_id)
	from PeticionInfohc pc
	where pc.pet_nrointerno = @pet_nrointerno and pc.info_estado in ('CONFEC','VERIFI')

	-- Se determina la leyenda para el historial
	select 
		@tipo_proceso = case
			when ic.infoprot_tipo = 'C' then 'Completo'
			when ic.infoprot_tipo = 'R' then 'Reducido'
		end,
		@nombre_informe = ic.infoprot_nom
	from Infoproc ic
	where ic.infoprot_id = @infoprot_id

	select @texto = 'Se crea un ' + LTRIM(RTRIM(@nombre_informe)) + ' (' + LTRIM(RTRIM(LOWER(@tipo_proceso))) + ')'

	-- Si existe un informe anterior, tomo el m�s reciente
	if @copia_info_id > 0
		begin
			-- Genera la cabecera del informe para una petici�n a partir del id de una plantilla (@infoprot_id).
			INSERT INTO PeticionInfohc (pet_nrointerno, info_id, info_idver, info_tipo, info_punto, info_fecha, info_recurso, info_estado, infoprot_id) 
				SELECT @pet_nrointerno, @proxnumero, 0, ic.infoprot_tipo, convert(int,ic.infoprot_punto), getdate(), @cod_recurso, 'CONFEC', @infoprot_id
				FROM Infoproc ic
				WHERE ic.infoprot_id = @infoprot_id and ic.infoprot_estado = 'S'
			
			-- Genera el detalle del informe solicitado tomando la base de los items del informe inmediato anterior.
			INSERT INTO PeticionInfohd (pet_nrointerno, info_id, info_idver, info_item, info_estado, info_valor, info_orden) 
				select
					@pet_nrointerno, @proxnumero, 0, 
					id.infoprot_item,
					info_estado = (select pd.info_estado from PeticionInfohd pd where pd.pet_nrointerno = @pet_nrointerno and pd.info_id = @copia_info_id and pd.info_item = id.infoprot_item),
					info_valor = (select pd.info_valor from PeticionInfohd pd where pd.pet_nrointerno = @pet_nrointerno and pd.info_id = @copia_info_id and pd.info_item = id.infoprot_item),
					id.infoprot_orden
				from Infoprod id 
				where id.infoprot_id = @infoprot_id
				
				/*
				select
					@pet_nrointerno, @proxnumero, 0, 
					id.infoprot_item,
					pd.info_estado,
					pd.info_valor,
					id.infoprot_orden
				from
					PeticionInfohc pc inner join
					PeticionInfohd pd on (pd.pet_nrointerno = pc.pet_nrointerno and pd.info_id = pc.info_id) right join
					Infoprod id on (id.infoprot_id = pc.infoprot_id and id.infoprot_item = pd.info_item)
				where
					pd.pet_nrointerno = @pet_nrointerno and 
					id.infoprot_id = @infoprot_id
				*/
				

			-- Genera el detalle de responsables si ya existieran observaciones para el informe anterior.
			INSERT INTO PeticionInfohr (pet_nrointerno, info_id, info_idver, info_item, cod_recurso, cod_perfil, cod_nivel, cod_area, info_valor, info_comen) 
				SELECT @pet_nrointerno, @proxnumero, pr.info_idver, pr.info_item, pr.cod_recurso, pr.cod_perfil, pr.cod_nivel, pr.cod_area, pr.info_valor, pr.info_comen
				FROM PeticionInfohr pr
				WHERE pr.pet_nrointerno = @pet_nrointerno and pr.info_id = @copia_info_id and pr.info_idver = 0

			-- Si existen observaciones generales en el informe, se copian al nuevo.
			INSERT INTO PeticionInfoht (pet_nrointerno, info_id, info_idver, info_renglon, info_renglontexto)
				SELECT @pet_nrointerno, @proxnumero, pt.info_idver, pt.info_renglon, pt.info_renglontexto
				FROM PeticionInfoht pt
				WHERE pt.pet_nrointerno = @pet_nrointerno and pt.info_id = @copia_info_id and pt.info_idver = 0
		end
	else
		begin
			-- Genera la cabecera del informe para una petici�n a partir del id de una plantilla (@infoprot_id).
			INSERT INTO PeticionInfohc (pet_nrointerno, info_id, info_idver, info_tipo, info_punto, info_fecha, info_recurso, info_estado, infoprot_id) 
				SELECT @pet_nrointerno, @proxnumero, 0, ic.infoprot_tipo, convert(int,ic.infoprot_punto), getdate(), @cod_recurso, 'CONFEC', @infoprot_id
				FROM Infoproc ic
				WHERE ic.infoprot_id = @infoprot_id and ic.infoprot_estado = 'S'
			
			-- Genera el detalle del informe solicitado.
			INSERT INTO PeticionInfohd (pet_nrointerno, info_id, info_idver, info_item, info_estado, info_valor, info_orden) 
				SELECT @pet_nrointerno, @proxnumero, 0, id.infoprot_item, '', null, id.infoprot_orden
				FROM Infoprod id
				WHERE id.infoprot_id = @infoprot_id
		end

	-- Agregamos el evento en el historial
	select @evento = LTRIM(RTRIM('INFOHP'))
	select @cod_recurso = SUSER_NAME()
	--select @texto = 'Se crea nuevo informe preliminar de homologaci�n.'
	exec sp_AddHistorial @hst_nrointerno OUTPUT, @pet_nrointerno, @evento, @pet_estado, null, null, null, null, @cod_recurso
	exec sp_AddHistorialMemo @hst_nrointerno, 0, @texto

	/* -- AUTOMATIZACION --
	-- Obtengo el siguiente nro. disponible (PK)
	declare @proxnumero			int 
	declare @clase				char(4)
	declare @tipo_proceso		char(1)		-- Tipo de proceso: (C)ompleto o (R)educido.
	declare @infoprot_id		int
	declare @texto				varchar(255)
	declare @hst_nrointerno		int
	declare @evento				char(8)
	declare @pet_estado			char(6)
	declare @info_id			int
	declare @punto_control		char(1)
	
	select @proxnumero = 0 
	execute sp_ProximoNumero 'ULPETHOM', @proxnumero OUTPUT 

	select 
		@clase = p.cod_clase,
		@pet_estado = p.cod_estado
	from Peticion p
	where p.pet_nrointerno = @pet_nrointerno

	IF (@modo = 'A') 
		BEGIN
			IF charindex(@clase,'EVOL|NUEV|OPTI|') >0
				begin
					select @tipo_proceso = 'C'
					select @punto_control = '3'
				end
			ELSE
				begin
					select @tipo_proceso = 'R'
					select @punto_control = '1'
				end
		END
	ELSE 
		BEGIN
			IF charindex(@clase,'EVOL|NUEV|OPTI|') >0
				begin
					select @tipo_proceso = 'C'
					select @punto_control = '1'		-- Esto esta bien as�????
				end
			ELSE
				begin
					select @tipo_proceso = 'R'
					select @punto_control = '1'
				end
		END

	if exists (
		select pc.pet_nrointerno
		from PeticionInfohc pc
		where pc.pet_nrointerno = @pet_nrointerno and pc.info_punto = convert(int,@punto_control))
		begin
			-- Encabezado
			SELECT @infoprot_id = ic.infoprot_id
			FROM Infoproc ic
			WHERE ic.infoprot_tipo = @tipo_proceso and ic.infoprot_punto = @punto_control and ic.infoprot_estado = 'S'

			SELECT @info_id = max(pc.info_id)
			FROM PeticionInfohc pc
			WHERE pc.pet_nrointerno = @pet_nrointerno and pc.info_punto = convert(int,@punto_control)
			GROUP BY info_id

			INSERT INTO PeticionInfohc (pet_nrointerno, info_id, info_idver, info_tipo, info_punto, info_fecha, info_recurso, info_estado, info_obs, infoprot_id) 
				SELECT @pet_nrointerno, @proxnumero, 0, @tipo_proceso, convert(int,@punto_control), getdate(), @cod_recurso, 'CONFEC', null, @infoprot_id
				FROM Infoproc ic
				WHERE ic.infoprot_id = @infoprot_id and ic.infoprot_punto = @punto_control and ic.infoprot_estado = 'S'		

			INSERT INTO PeticionInfohd (pet_nrointerno, info_id, info_idver, info_item, info_estado, info_valor, info_orden, info_comen) 
				select
					@pet_nrointerno, @proxnumero, 0, 
					id.infoprot_item,
					pd.info_estado,
					pd.info_valor,
					id.infoprot_orden, null
				from 
					PeticionInfohd pd inner join
					PeticionInfohc pc on (pd.pet_nrointerno = pc.pet_nrointerno and pd.info_id = pc.info_id) inner join
					Infoprod id on (id.infoprot_id = pc.infoprot_id and id.infoprot_item = pd.info_item)
				where
					pd.pet_nrointerno = @pet_nrointerno and 
					pd.info_id = @info_id and 
					pc.info_punto = convert(int,@punto_control) 
			
			-- Agregamos el evento en el historial
			select @evento = LTRIM(RTRIM('INFOHP'))
			select @cod_recurso = SUSER_NAME()
			select @texto = 'Se crea nuevo informe preliminar de homologaci�n.'
			exec sp_AddHistorial @hst_nrointerno OUTPUT, @pet_nrointerno, @evento, @pet_estado, null, null, null, null, @cod_recurso
			exec sp_AddHistorialMemo @hst_nrointerno, 0, @texto
		end
	*/
	return(0)
go

grant execute on dbo.sp_InsertPeticionInformeManual to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
