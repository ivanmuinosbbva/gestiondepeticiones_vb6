/*
-000- a. FJS 26.06.2009 - Nuevo SP para el manejo de solicitudes a Carga de M�quina
-001- a. FJS 08.10.2009 - Cambio en la definici�n: si la solicitud es CON enmascaramiento de datos, no necesita aprobaci�n de ninguna clase. En cambio si solicita datos
						  SIN enmascarar, necesita solo aprobaci�n directa de un supervisor y luego es cursada a Carga de M�quina.
-002- a. FJS 28.12.2009 - Nuevo tipo de solicitud: restore de archivo a fecha. 
-002- b. FJS 20.01.2010 - Cambio: la opci�n 5 debe quedar como pendiente de liberaci�n, como la 6 si es con enmascaramiento. Si es sin enmascarar,
						  debe quedar pendiente de autorizaci�n.
-003- a. FJS 20.01.2010 - Nuevo: Solicitudes de ejecuci�n en diferido.
-004- a. FJS 27.06.2014 - Nuevo: se agrega un campo para identificar las solicitudes por EMErgencia.
-005- a. FJS 16.07.2014 - Nuevo: Nuevo nivel de aprobaci�n: para las solicitudes sin enmascaramiento normales. Si el solicitante es un lider, 
						  la solicitud ya sale con la pre-aprobaci�n.
-006- a. FJS 22.08.2014 - Nuevo: se agregan 3 nuevos campos: sol_file_inter, sol_bckuptabla, sol_bckupjob. Adem�s, se cambia el tama�o de los 
						  siguientes variables: @sol_file_prod, @sol_file_desa, @sol_file_out, @sol_lib_Sysin, @sol_mem_Sysin (pasar de char 
						  de 255 a char de 50, acorde a los cambios en los campos de la tabla).

Referencias:
************

CM: Carga de M�quina

*/

/*
A: Pendiente de aprobaci�n N4
B: Pendiente de aprobaci�n N3
C: Aprobado
D: Aprobado y en proceso de envio
E: Enviado y finalizado
F: En espera de Restore
G: En espera (ejecuci�n diferida)		Aquellas que nacen como de ejecuci�n diferida, luego pasan a estado "C"
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertSolicitud'
go

if exists (select * from sysobjects where name = 'sp_InsertSolicitud' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertSolicitud
go

create procedure dbo.sp_InsertSolicitud
	@sol_diferida		char(1)=null,
	@sol_tipo			char(1)=null,
	@sol_mask			char(1)=null,
	@sol_recurso		char(10)=null,
	@pet_nrointerno		int=null,
	@jus_codigo			int=null,
	@sol_texto			char(255)=null,
	@sol_fe_auto1		smalldatetime=null,
	@sol_fe_auto2		smalldatetime=null,
	@sol_fe_auto3		smalldatetime=null,
	-- { add -006- a.
	@sol_file_prod		char(50)=null,
	@sol_file_desa		char(50)=null,
	@sol_file_out		char(50)=null,
	@sol_file_inter		char(50)=null,
	@sol_bckuptabla		char(50)=null,
	@sol_bckupjob		char(50)=null,
	@sol_lib_Sysin		char(50)=null,
	@sol_mem_Sysin		char(50)=null,
	-- }
	@sol_join			char(1)=null,
	@sol_nrotabla		char(4)=null,
	@sol_prod_cpybbl	char(50)=null,
	@sol_prod_cpynom	char(50)=null,
	@sol_dsn_id			char(8)=null,
	@sol_eme			char(1)=null,		-- add -004- a.
	@ret_nroasignado	int output
as
	declare @sol_nroasignado	int
	declare @fe_formato			char(27)
	declare @fe_componente		char(2)
	declare @today				smalldatetime
	declare @estado				char(1)
	declare @ds					char(1)		-- Datos sensibles
	
	declare @cod_sector			char(8)		-- Sector del recurso solicitante
	declare @cod_grupo			char(8)		-- Grupo del recurso solicitante
	declare @sol_resp_ejec		char(10)	-- Responsable de ejecuci�n
	declare @sol_resp_sect		char(10)	-- Responsable de sector
	
	
	-- Determino el �rea actuante del recurso solicitante
	select 
		@cod_sector = r.cod_sector,
		@cod_grupo = r.cod_grupo
	from Recurso r
	where r.cod_recurso = @sol_recurso

	-- Determino el responsable de sector y el responsable de ejecuci�n del recurso actuante
	-- Responsable de sector
	select @sol_resp_sect = b.cod_recurso
	from Recurso b
	where 
		LTRIM(RTRIM(b.cod_sector)) = LTRIM(RTRIM(@cod_sector)) and b.flg_cargoarea = 'S' and 
		(b.cod_grupo = '' or b.cod_grupo = '' or b.cod_grupo = space(col_length('GesPet..Recurso', 'b.cod_grupo'))) 
	
	-- Responsable de ejecuci�n
	select @sol_resp_ejec = b.cod_recurso
	from Recurso b
	where LTRIM(RTRIM(b.cod_grupo)) = LTRIM(RTRIM(@cod_grupo)) and b.flg_cargoarea = 'S'

	/* 
	==========================================================================================================================================================
	INICIO: determino el estado inicial de la solicitud
	==========================================================================================================================================================
	*/
	
	-- Determinamos si el archivo DSN tiene datos sensibles
	select @ds = dsn_enmas
	from GesPet..HEDT001 A
	where LTRIM(RTRIM(A.dsn_id)) = LTRIM(RTRIM(@sol_dsn_id))

	if @sol_diferida = 'S'										-- *** DIFERIDA ***
		begin
			if @ds = 'S'										-- DSN con datos sensibles
				select @estado = 'G'							-- En espera (ejecuci�n diferida)
			else
				if @sol_tipo in ('5','6','7')					-- *** RESTORE ***
					select @estado = 'E'						-- Enviado y finalizado
				else
					select @estado = 'C'						-- Aprobado
		end
	else
		--{ add -004- a.
		if @sol_eme = 'S'
			if (LTRIM(RTRIM(@sol_recurso)) = LTRIM(RTRIM(@sol_resp_sect)))		-- Si solicita el Supervisor, pasa por mail directamente a CM
				begin
					select @estado = 'E'
					select @sol_resp_ejec = ''			-- Si solicita el Supervisor, no tiene l�der.
					select @sol_fe_auto2 = getdate()	-- Ya queda con la fecha de aprobaci�n de nivel N3.
				end
			else
				select @estado = 'B'					-- Las EMErgencias se envian directamente por correo electr�nico autom�ticamente, quedan en estado "Pendiente aprobaci�n nivel 3 (ex post)".
		else
			begin
		--}
				-- Opci�n: con RESTORE
				if @sol_tipo = '5' or @sol_tipo = '6' or @sol_tipo = '7'
					begin
						if @sol_mask = 'S'				-- El pedido es con enmascaramiento
							select @estado = 'E'		-- Enviado y finalizado
						else
							--{ add -005- a.
							if (LTRIM(RTRIM(@sol_recurso)) = LTRIM(RTRIM(@sol_resp_ejec)))
								select @estado = 'B'	-- Si solicita el L�der, queda pendiente de aprobaci�n del Supervisor
							else if (LTRIM(RTRIM(@sol_recurso)) = LTRIM(RTRIM(@sol_resp_sect)))
								begin
									select @estado = 'C'				-- Si solicita el Supervisor, pasa por archivo a CM
									select @sol_resp_ejec = ''			-- Si solicita el Supervisor, no tiene l�der.
									select @sol_fe_auto2 = getdate()	-- Ya queda con la fecha de aprobaci�n de nivel N3.
								end	
							else
								select @estado = 'A'	-- Si solicita un no L�der, queda pendiente de aprobaci�n del L�der
							--}
					end
				else
					if @sol_mask = 'S'
						select @estado = 'C'		-- add -001- a. Si es CON enmascaramiento, pasa por archivo a CM.
					else
						--select @estado = 'A'		-- add -001- a.		-- del -005- a.
						--{ add -005- a.
						if (LTRIM(RTRIM(@sol_recurso)) = LTRIM(RTRIM(@sol_resp_ejec)))
							select @estado = 'B'	-- Si solicita el L�der, queda pendiente de aprobaci�n del Supervisor
						else if (LTRIM(RTRIM(@sol_recurso)) = LTRIM(RTRIM(@sol_resp_sect)))	
							begin
								select @estado = 'C'				-- Si solicita el Supervisor, pasa por archivo a CM
								select @sol_resp_ejec = ''			-- Si solicita el Supervisor, no tiene l�der.
								select @sol_fe_auto2 = getdate()	-- Ya queda con la fecha de aprobaci�n de nivel N3.
							end
						else
							select @estado = 'A'	-- Si solicita un no L�der, queda pendiente de aprobaci�n del L�der
						--}
			end										-- add -004- a.
	/* 
	==========================================================================================================================================================
	INICIO: determino el nro. asignado de la solicitud y formateo de la fecha y la hora
	==========================================================================================================================================================
	*/

	select @sol_nroasignado = max(sol_nroasignado) + 1
	from GesPet..Solicitudes

	if @sol_nroasignado is null or @sol_nroasignado = 0
		select @sol_nroasignado = 1

	select @today = getdate()
	select @fe_formato = '- '

	-- D�a
	select @fe_componente = convert(char(2), datepart(dd, @today))

	if abs(convert(int, @fe_componente)) < 10
		select @fe_componente = '0' + rtrim(@fe_componente)
		select @fe_formato = rtrim(@fe_formato) + ' ' + @fe_componente + ' / '

	-- Mes
	select @fe_componente = convert(char(2), datepart(mm, @today))

	if abs(convert(int, @fe_componente)) < 10
		select @fe_componente = '0' + rtrim(@fe_componente)
		select @fe_formato = rtrim(@fe_formato) + ' ' + @fe_componente + ' / '

	-- A�o
	select @fe_formato = rtrim(@fe_formato) + ' ' + substring(convert(char(4), datepart(yy, @today)), 3, 2) + ' - '

	-- Horas
	select @fe_componente = convert(char(2), datepart(hh, @today))

	if abs(convert(int, @fe_componente)) < 10
		if abs(convert(int, @fe_componente)) = 0
			select @fe_componente = '00'
		else
			select @fe_componente = '0' + rtrim(@fe_componente)
		select @fe_formato = rtrim(@fe_formato) + ' ' + @fe_componente + ':'

	-- Minutos
	select @fe_componente = convert(char(2), datepart(mi, @today))

	if abs(convert(int, @fe_componente)) < 10
		if abs(convert(int, @fe_componente)) = 0
			select @fe_componente = '00'
		else
			select @fe_componente = '0' + rtrim(@fe_componente)
		select @fe_formato = rtrim(@fe_formato) + @fe_componente + ':'

	-- Segundos
	select @fe_componente = convert(char(2), datepart(ss, @today))

	if abs(convert(int, @fe_componente)) < 10
		if abs(convert(int, @fe_componente)) = 0
			select @fe_componente = '00'
		else
			select @fe_componente = '0' + rtrim(@fe_componente)
		select @fe_formato = rtrim(@fe_formato) + @fe_componente + ' -'

	/* 
	==========================================================================================================================================================
	FIN: formateo de la fecha y la hora
	==========================================================================================================================================================
	*/
		insert into GesPet.dbo.Solicitudes (
			sol_nroasignado,
			sol_tipo,
			sol_mask,
			sol_fecha,
			sol_recurso,
			pet_nrointerno,
			sol_resp_ejec,
			sol_resp_sect,
			sol_seguridad,
			jus_codigo,
			sol_texto,
			sol_fe_auto1,
			sol_fe_auto2,
			sol_fe_auto3,
			sol_file_prod,
			sol_file_desa,
			sol_file_out,
			sol_lib_Sysin,
			sol_mem_Sysin,
			sol_join,
			sol_nrotabla,
			fe_formato,
			sol_estado,
			sol_trnm_fe,
			sol_trnm_usr,
			pet_nroasignado,
			sol_prod_cpybbl,
			sol_prod_cpynom,
			dsn_id,
			--sol_eme)				-- add -004- a.		-- del -006- a.
			-- { add -006- a.
			sol_eme,
			sol_file_inter,
			sol_bckuptabla,
			sol_bckupjob,
			sol_diferida)
			-- }
		values (
			@sol_nroasignado,
			@sol_tipo,
			@sol_mask,
			@today,
			@sol_recurso,
			@pet_nrointerno,
			@sol_resp_ejec,
			@sol_resp_sect,
			NULL,
			@jus_codigo,
			@sol_texto,
			@sol_fe_auto1,
			@sol_fe_auto2,
			@sol_fe_auto3,
			@sol_file_prod,
			@sol_file_desa,
			@sol_file_out,
			@sol_lib_Sysin,
			@sol_mem_Sysin,
			@sol_join,
			@sol_nrotabla,
			@fe_formato,
			@estado,
			null,
			null,
			null,
			@sol_prod_cpybbl,
			@sol_prod_cpynom,
			@sol_dsn_id,
			--@sol_eme)		-- add -004- a.		-- del -006- a.
			-- { add -006- a.
			@sol_eme,
			@sol_file_inter,
			@sol_bckuptabla,
			@sol_bckupjob,
			@sol_diferida)
			-- }

			select @ret_nroasignado	= @sol_nroasignado
go

grant execute on dbo.sp_InsertSolicitud to GesPetUsr
go

print 'Actualizaci�n realizada.'
go