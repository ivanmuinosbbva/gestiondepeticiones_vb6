use GesPet
go
/* sp_DeleteProyectoAvance  3,'200704' */
print 'sp_DeleteProyectoAvance'
go
if exists (select * from sysobjects where name = 'sp_DeleteProyectoAvance' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteProyectoAvance
go
create procedure dbo.sp_DeleteProyectoAvance
	@prj_nrointerno		int,
	@pav_aamm	char(6)

as

if not exists (select prj_nrointerno from ProyectoAvanceCab where prj_nrointerno=@prj_nrointerno and pav_aamm=@pav_aamm)
	begin 
		raiserror 30011 'Planilla de avance inexistente'   
		select 30011 as ErrCode , 'Planilla de avance inexistente' as ErrDesc   
		return (30011)   
	end 

begin tran 
	delete from ProyectoAvanceMem
        where prj_nrointerno = @prj_nrointerno and RTRIM(pav_aamm) = RTRIM(@pav_aamm)
	delete from ProyectoAvanceDet
	where  prj_nrointerno = @prj_nrointerno  and pav_aamm=@pav_aamm
	delete from ProyectoAvanceCab
	where  prj_nrointerno = @prj_nrointerno  and pav_aamm=@pav_aamm
commit tran 


return(0)
go

grant execute on dbo.sp_DeleteProyectoAvance to GesPetUsr
go
