use GesPet
go
print 'sp_DeleteAvanceCoef'
go
if exists (select * from sysobjects where name = 'sp_DeleteAvanceCoef' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteAvanceCoef
go
create procedure dbo.sp_DeleteAvanceCoef 
           @cod_AvanceCoef    char(8) 
as 
 
if exists (select cod_AvanceCoef from GesPet..AvanceCoef where RTRIM(cod_AvanceCoef) = RTRIM(@cod_AvanceCoef)) 
begin 
    delete GesPet..AvanceCoef 
      where RTRIM(cod_AvanceCoef) = RTRIM(@cod_AvanceCoef) 
end 
else 
begin 
    raiserror 30001 'Avance Coeficiente Inexistente'   
    select 30001 as ErrCode , 'Avance Coeficiente Inexistente' as ErrDesc   
    return (30001)   
end 
return(0) 
go



grant execute on dbo.sp_DeleteAvanceCoef to GesPetUsr 
go
