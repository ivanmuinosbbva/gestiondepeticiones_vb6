/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteEstadosDesarrollo'
go

if exists (select * from sysobjects where name = 'sp_DeleteEstadosDesarrollo' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteEstadosDesarrollo
go

create procedure dbo.sp_DeleteEstadosDesarrollo
	@cod_estado_desa	int=null
as
	delete from
		GesPet.dbo.EstadosDesarrollo
	where
		(cod_estado_desa = @cod_estado_desa or @cod_estado_desa is null)
	return(0)
go

grant execute on dbo.sp_DeleteEstadosDesarrollo to GesPetUsr
go

print 'Actualización realizada.'
