/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 07.10.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionPlantxt'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionPlantxt' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionPlantxt
go

create procedure dbo.sp_InsertPeticionPlantxt
	@per_nrointerno	int=null,
	@pet_nrointerno	int=null,
	@cod_grupo	char(8)=null,
	@mem_campo	char(10)=null,
	@mem_secuencia	smallint=null,
	@mem_texto	char(255)=null
as
	insert into GesPet.dbo.PeticionPlantxt (
		per_nrointerno,
		pet_nrointerno,
		cod_grupo,
		mem_campo,
		mem_secuencia,
		mem_texto)
	values (
		@per_nrointerno,
		@pet_nrointerno,
		@cod_grupo,
		@mem_campo,
		@mem_secuencia,
		@mem_texto)
	return(0)
go

grant execute on dbo.sp_InsertPeticionPlantxt to GesPetUsr
go

print 'Actualización realizada.'
