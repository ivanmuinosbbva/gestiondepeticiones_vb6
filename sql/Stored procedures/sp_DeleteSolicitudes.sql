/*
-000- a. FJS 26.06.2009 - Nuevo SP para el manejo de solicitudes a Carga de M�quina
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteSolicitudes'
go

if exists (select * from sysobjects where name = 'sp_DeleteSolicitudes' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteSolicitudes
go

create procedure dbo.sp_DeleteSolicitudes
	@sol_nroasignado	int
as
	delete from
		GesPet..Solicitudes
	where
		sol_nroasignado = @sol_nroasignado
go

grant execute on dbo.sp_DeleteSolicitudes to GesPetUsr
go

print 'Actualizaci�n realizada.'
