/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.07.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertSolicitudesSSDD'
go

if exists (select * from sysobjects where name = 'sp_InsertSolicitudesSSDD' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertSolicitudesSSDD
go

create procedure dbo.sp_InsertSolicitudesSSDD
	@solicitante		char(10),
	@sol_mask			char(1)='S',
	@jus_codigo			int=null,
	@sol_texto			varchar(255)=null,
	@pet_nroasignado	int=null,
	@tablaId			int=null,
	@sol_archivo		varchar(80)=null,
	@sol_eme			char(1)='N',
	@sol_diferida		char(1)='N'
as
	declare @sol_nroasignado	int
	declare @pet_nrointerno		int
	declare @estado_inicial		char(1)

	if @sol_mask = 'N'					-- SIN ENMASCARAMIENTO
		select @estado_inicial = 'B'	-- Pendiente de autorizar
	else
		select @estado_inicial = 'P'	-- Pendiente de proceso

	select @sol_nroasignado = max(sol_nroasignado) + 1
	from GesPet..SolicitudesSSDD

	if @sol_nroasignado is null or @sol_nroasignado = 0
		select @sol_nroasignado = 1

	select @pet_nrointerno = p.pet_nrointerno
	from Peticion p
	where p.pet_nroasignado = @pet_nroasignado

	insert into GesPet.dbo.SolicitudesSSDD (
		sol_nroasignado,
		sol_mask,
		sol_fecha,
		sol_recurso,
		jus_codigo,
		sol_texto,
		sol_trnm_fe,
		sol_trnm_usr,
		pet_nroasignado,
		pet_nrointerno,
		tablaId,
		sol_archivo,
		sol_eme,
		sol_diferida,
		sol_estado,
		fechaAprobacion,
		aprobador)
	values (
		@sol_nroasignado,
		@sol_mask,
		GETDATE(),
		@solicitante,
		@jus_codigo,
		@sol_texto,
		null,
		null,
		@pet_nroasignado,
		@pet_nrointerno,
		@tablaId,
		@sol_archivo,
		@sol_eme,
		@sol_diferida,
		@estado_inicial,
		null,
		null)
go

grant execute on dbo.sp_InsertSolicitudesSSDD to GesPetUsr
go

grant execute on dbo.sp_InsertSolicitudesSSDD to TrnCGMEnmascara
go

sp_procxmode 'dbo.sp_InsertSolicitudesSSDD','anymode'
go

print 'Actualización realizada.'
go
