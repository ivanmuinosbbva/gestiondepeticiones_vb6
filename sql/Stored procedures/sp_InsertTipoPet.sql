/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.10.2014 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertTipoPet'
go

if exists (select * from sysobjects where name = 'sp_InsertTipoPet' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertTipoPet
go

create procedure dbo.sp_InsertTipoPet
	@cod_tipopet	char(3),
	@nom_tipopet	char(50)
as
	insert into GesPet.dbo.TipoPet (
		cod_tipopet,
		nom_tipopet)
	values (
		@cod_tipopet,
		@nom_tipopet)
go

grant execute on dbo.sp_InsertTipoPet to GesPetUsr
go

print 'Actualización realizada.'
go
