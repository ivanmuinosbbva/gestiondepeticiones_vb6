/*
-000- a. FJS 03.09.2010 - Nuevo SP eliminar equivalencia de estados entre IGM y CGM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_ABM_DelEstado'
go

if exists (select * from sysobjects where name = 'sp_IGM_ABM_DelEstado' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_ABM_DelEstado
go

create procedure dbo.sp_IGM_ABM_DelEstado
	@IGM_estado		char(6), 
	@CGM_estado		char(6)
as 
	delete from GesPet..IGM_Estados
	where 
		cod_IGM_estado = @IGM_estado and
		cod_CGM_estado = @CGM_estado
	return(0)
go

grant execute on dbo.sp_IGM_ABM_DelEstado to GesPetUsr
go

grant execute on dbo.sp_IGM_ABM_DelEstado to GrpTrnIGM
go

print 'Actualización realizada.'
go