/* REVISAR PARA CONSOLIDAR */

/*
-000- a. FJS 07.05.2009 - Agrega de manera automática las peticiones que se encuentren al momento de ejecutar el script en estado "En ejecución"
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InitPeticionEnviadas2'
go

if exists (select * from sysobjects where name = 'sp_InitPeticionEnviadas2' and sysstat & 7 = 4)
	drop procedure dbo.sp_InitPeticionEnviadas2
go

create procedure dbo.sp_InitPeticionEnviadas2
as 
	declare @pet_nrointerno	int
	declare @pet_record	char(80)
	declare	@pet_sector	char(8)
	declare @pet_grupo	char(8)
	
	select @pet_nrointerno = 0
	select @pet_record	= ''
	select @pet_sector	= ''
	select @pet_grupo	= ''

	declare curPeticiones cursor for 
		select
			a.pet_nrointerno
		from
			GesPet..Peticion a,
			GesPet..PeticionSector b,
			GesPet..PeticionGrupo c
		where
			a.pet_nrointerno *= b.pet_nrointerno and
			b.pet_nrointerno *= c.pet_nrointerno and
			b.cod_sector *= c.cod_sector and
			a.cod_estado = 'EJECUC'
		group by
			a.pet_nrointerno
		for read only 

		open curPeticiones
		fetch curPeticiones into @pet_nrointerno 
		while @@sqlstatus = 0 
			begin 
				if not exists (select pet_nrointerno 
							   from GesPet..PeticionEnviadas2 
							   where pet_nrointerno = @pet_nrointerno and pet_sector = @pet_sector and pet_grupo = @pet_grupo) 
					begin 
						if @pet_sector is null 
							select @pet_sector = '' 
						if @pet_grupo is null 
							select @pet_grupo = '' 
				 
						execute sp_FormatEnviadas @pet_nrointerno, 'SECTOR', 'GRUPO', @pet_record output 
						 
						if datalength(@pet_record) < 80 
							select @pet_record = @pet_record + space(datalength(@pet_record) - 80) 
						insert into GesPet.dbo.PeticionEnviadas2 ( 
							pet_nrointerno, 
							pet_sector, 
							pet_grupo, 
							pet_record, 
							pet_usrid, 
							pet_date, 
							pet_done) 
						values ( 
							@pet_nrointerno, 
							@pet_sector, 
							@pet_grupo, 
							@pet_record, 
							SUSER_NAME(), 
							getdate(), 
							null) 
					end 
				else 
					begin 
						execute sp_FormatEnviadas @pet_nrointerno, 'SECTOR', 'GRUPO', @pet_record output 
				 
						update    
							GesPet..PeticionEnviadas2 
						set    
							pet_record = @pet_record, 
							pet_usrid  = SUSER_NAME(),    
							pet_date   = getdate() 
						where   
							pet_nrointerno = @pet_nrointerno and    
							pet_sector     = @pet_sector and    
							pet_grupo      = @pet_grupo  
					end 
				fetch curPeticiones into @pet_nrointerno
			end 
		close curPeticiones
	deallocate cursor curPeticiones 
go

grant execute on dbo.sp_InitPeticionEnviadas2 to GesPetUsr
go

print 'Actualización realizada.'