/*  C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.01.2013 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertProyectoIDMHoras'
go

if exists (select * from sysobjects where name = 'sp_InsertProyectoIDMHoras' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertProyectoIDMHoras
go

create procedure dbo.sp_InsertProyectoIDMHoras
	@ProjId		int,
	@ProjSubId	int,
	@ProjSubSId	int,
	@cod_nivel	char(3),
	@cod_area	char(8),
	@cant_horas	int
as
	if exists (select ProjId from GesPet..ProyectoIDMHoras where ProjId = @ProjId and 
		ProjSubId = @ProjSubId and 
		ProjSubSId = @ProjSubSId and 
		cod_nivel = LTRIM(RTRIM(@cod_nivel)) and 
		cod_area = LTRIM(RTRIM(@cod_area)))
		begin 
			raiserror 30010 'Las horas para este nivel y �rea ya est�n declaradas'
			select 30010 as ErrCode, 'Las horas para este nivel y �rea ya est�n declaradas' as ErrDesc 
			return (30010) 
		end
	else
		begin
			insert into GesPet.dbo.ProyectoIDMHoras (
				ProjId,
				ProjSubId,
				ProjSubSId,
				cod_nivel,
				cod_area,
				cant_horas)
			values (
				@ProjId,
				@ProjSubId,
				@ProjSubSId,
				@cod_nivel,
				@cod_area,
				@cant_horas)
			
			if @cod_nivel = 'GER'
				update GesPet..ProyectoIDM
				set totalhs_gerencia = totalhs_gerencia + @cant_horas
				where 
					ProjId = @ProjId and
					ProjSubId = @ProjSubId and
					ProjSubSId = @ProjSubSId
			else
				update GesPet..ProyectoIDM
				set totalhs_sector = totalhs_sector + @cant_horas
				where 
					ProjId = @ProjId and
					ProjSubId = @ProjSubId and
					ProjSubSId = @ProjSubSId
		end
		return(0)
go

grant execute on dbo.sp_InsertProyectoIDMHoras to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
