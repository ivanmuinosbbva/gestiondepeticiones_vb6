/*
-000- a. FJS 22.07.2008 - Nuevo SP para devolver el conjunto de resultados a partir del campo var_texto
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetVariosTexto'
go

if exists (select * from sysobjects where name = 'sp_GetVariosTexto' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetVariosTexto
go

create procedure dbo.sp_GetVariosTexto
	@var_texto		varchar(30)=null 
as 
	select
		var_codigo,
		var_numero,
		var_texto,
		var_fecha
	from
		GesPet..Varios
	where
		@var_texto is null or 
		rtrim(var_texto) = rtrim(@var_texto)

go

grant execute on dbo.sp_GetVariosTexto to GesPetUsr
go

print 'Actualización realizada.'
