use GesPet
go
print 'sp_UpdateProyectoAvanceMemo'
go
if exists (select * from sysobjects where name = 'sp_UpdateProyectoAvanceMemo' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateProyectoAvanceMemo
go
create procedure dbo.sp_UpdateProyectoAvanceMemo   
    @borra_last       char(1)='N',   
    @prj_nrointerno int,   
    @pav_aamm   char(6),
    @mem_campo      char(10),   
    @mem_secuencia  smallint,   
    @mem_texto      char(255)   
as   
   
declare @textoaux varchar (255)   
declare @textomemo varchar (255)   
select @textomemo = ''   
   
if exists (select prj_nrointerno from GesPet..ProyectoAvanceMem   
        where prj_nrointerno = @prj_nrointerno and   
  	    RTRIM(pav_aamm) = RTRIM(@pav_aamm) and
            RTRIM(mem_campo) = RTRIM(@mem_campo) and   
            mem_secuencia = @mem_secuencia)   
    begin   
        update GesPet..ProyectoAvanceMem   
        set mem_texto = @mem_texto   
        where prj_nrointerno = @prj_nrointerno and   
	    RTRIM(pav_aamm) = RTRIM(@pav_aamm) and
            RTRIM(mem_campo) = RTRIM(@mem_campo) and   
            mem_secuencia = @mem_secuencia   
    end   
    else   
    begin   
       insert into GesPet..ProyectoAvanceMem   
           (prj_nrointerno,   
	    pav_aamm,
	    mem_campo,   
            mem_secuencia,   
            mem_texto)   
       values   
           (@prj_nrointerno,   
	    @pav_aamm,
            @mem_campo,   
            @mem_secuencia,   
            @mem_texto)   
    end   
   
if @borra_last = 'S'   
    begin   
        delete from GesPet..ProyectoAvanceMem   
        where prj_nrointerno = @prj_nrointerno and   
	    RTRIM(pav_aamm) = RTRIM(@pav_aamm) and
            RTRIM(mem_campo) = RTRIM(@mem_campo) and   
            mem_secuencia > @mem_secuencia   
    end   
   
return(0)   
go



grant execute on dbo.sp_UpdateProyectoAvanceMemo to GesPetUsr 
go


