/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoIDMAlertas'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoIDMAlertas' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoIDMAlertas
go

create procedure dbo.sp_UpdateProyectoIDMAlertas
	@ProjId				int,
	@ProjSubId			int,
	@ProjSubSId			int,
	@alert_itm			int,
	@alert_tipo			char(1),
	@alert_desc			varchar(180),
	@accion_id			varchar(180),
	@alert_fe_ini		smalldatetime,
	@alert_fe_fin		smalldatetime,
	@responsables		varchar(180),
	@observaciones		varchar(180),
	@status				char(1),
	@fecha_resolucion	smalldatetime
as
	update 
		GesPet.dbo.ProyectoIDMAlertas
	set
		alert_tipo		= @alert_tipo,
		alert_desc		= @alert_desc,
		accion_id		= @accion_id,
		alert_fe_ini	= @alert_fe_ini,
		alert_fe_fin	= @alert_fe_fin,
		responsables	= @responsables,
		observaciones	= @observaciones,
		alert_status	= @status,
		alert_fecres	= @fecha_resolucion,
		alert_fecha		= getdate(),
		alert_usuario	= SUSER_NAME()
	where 
		ProjId		= @ProjId and
		ProjSubId	= @ProjSubId and
		ProjSubSId	= @ProjSubSId and
		alert_itm	= @alert_itm
	return(0)
go

grant execute on dbo.sp_UpdateProyectoIDMAlertas to GesPetUsr
go

print 'Actualización realizada.'
go
