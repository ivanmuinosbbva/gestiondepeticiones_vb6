/*
-000- a. FJS 20.08.2010 - Nuevo SP para administrar el campo IGM_hab.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_ABM_Direccion'
go

if exists (select * from sysobjects where name = 'sp_IGM_ABM_Direccion' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_ABM_Direccion
go

create procedure dbo.sp_IGM_ABM_Direccion
	@cod_direccion	char(8),
	@hab			char(1)
as 
	if exists (select cod_direccion from GesPet..Direccion where cod_direccion=@cod_direccion)
		begin
			update GesPet..Direccion
			set IGM_hab = @hab
			where cod_direccion = @cod_direccion
			return(0)
		end
	else
		begin
			select 30010 as ErrCode, 'Error: No existe el c�digo de direcci�n especificado.' as ErrDesc 
			raiserror 30010 'Error: No existe el c�digo de direcci�n especificado.'
			return (30010) 
		end
go

grant execute on dbo.sp_IGM_ABM_Direccion to GesPetUsr
go

grant execute on dbo.sp_IGM_ABM_Direccion to GrpTrnIGM
go

print 'Actualizaci�n realizada.'
go