-- ESTE SP DEBE SER convertIDO PARA CONSOLIDAR
/*
-000- a. FJS 24.06.2010 - Nuevo SP para obtener las horas trabajas solo en peticiones por �rea en un per�odo dado y para un proyecto y/o petici�n.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_HorasTrabajadasXArea'
go

if exists (select * from sysobjects where name = 'sp_HorasTrabajadasXArea' and sysstat & 7 = 4)
	drop procedure dbo.sp_HorasTrabajadasXArea
go

create procedure dbo.sp_HorasTrabajadasXArea
	@fdesde				varchar(8)='NULL',
	@fhasta				varchar(8)='NULL',
	@nivel				varchar(4)='NULL',
	@area				varchar(8)='NULL',
	@ProjId				int=null,
	@ProjSubId			int=null,
	@ProjSubSId			int=null,
	@p_pet_nrointerno	int=null,
	@horas_tot			int output
as 
	declare @fe_desde			smalldatetime
	declare @fe_hasta			smalldatetime
	declare @horas				int

	declare @cod_direccion		varchar (10)
	declare @cod_gerencia		varchar (10)
	declare @cod_sector			varchar (10)
	declare @cod_grupo			varchar (10)
	declare @cod_recurso		varchar (10)
	declare @pet_nrointerno		int
	declare @sol_desde			smalldatetime
	declare @sol_hasta			smalldatetime
	declare @ret_dias			int
	declare @ret_horas			int
	declare @horas_trab			numeric(10,2)
	declare @xhoras_trab		numeric(10,2)
	declare @ret_desde			smalldatetime
	declare @ret_hasta			smalldatetime

	set dateformat ymd
	
	if RTRIM(@fdesde)='NULL'
		begin
			select @sol_desde=dateadd(yy,-50,getdate())
			select @fdesde=convert(char(8),@sol_desde,112)
		end
	else
		select @sol_desde=@fdesde

	if RTRIM(@fhasta)='NULL'
		begin
			select @sol_hasta=dateadd(yy,50,getdate())
			select @fhasta=convert(char(8),@sol_hasta,112)
		end
	else
		select @sol_hasta=@fhasta

	CREATE TABLE #TmpSalida(
		cod_direccion	char(10)		null,
		cod_gerencia	char(10)		null,
		cod_sector		char(10)		null,
		cod_grupo		char(10)		null,
		cod_recurso		char(10)		null,
		pet_nrointerno	int				null,
		entre_desde		smalldatetime	null,
		entre_hasta		smalldatetime	null,
		horas			int				null)

	DECLARE CursHoras CURSOR FOR
		select
			Re.cod_direccion,
			Re.cod_gerencia,
			Re.cod_sector,
			Re.cod_grupo,
			Ht.cod_recurso,
			Ht.pet_nrointerno,
			Ht.fe_desde,
			Ht.fe_hasta,
			Ht.horas
		from
			GesPet..HorasTrabajadas Ht inner join
			GesPet..Recurso Re on (Ht.cod_recurso = Re.cod_recurso) inner join
			GesPet..Peticion Pt on (Ht.pet_nrointerno = Pt.pet_nrointerno)
		where
			(Ht.cod_tarea = '' or Ht.cod_tarea is null) and 
			(RTRIM(@fdesde)='NULL' or convert(char(8),Ht.fe_hasta,112) >= @fdesde) and
			(RTRIM(@fhasta)='NULL' or convert(char(8),Ht.fe_desde,112) <= @fhasta) and
			((@nivel is null or RTRIM(@nivel)='NULL') or
			(@nivel='DIRE' and RTRIM(Re.cod_direccion)=RTRIM(@area)) or
			(@nivel='GERE' and RTRIM(Re.cod_gerencia)=RTRIM(@area)) or
			(@nivel='SECT' and RTRIM(Re.cod_sector)=RTRIM(@area)) or
			(@nivel='GRUP' and RTRIM(Re.cod_grupo)=RTRIM(@area))) and
			(@ProjId is null or Pt.pet_projid = @ProjId) and 
			(@ProjSubId is null or Pt.pet_projsubid = @ProjSubId) and 
			(@ProjSubSId is null or Pt.pet_projsubsid = @ProjSubSId) and
			(@p_pet_nrointerno is null or Ht.pet_nrointerno = @p_pet_nrointerno)
		for read only

		OPEN CursHoras 
		FETCH CursHoras INTO
			@cod_direccion,
			@cod_gerencia,
			@cod_sector,
			@cod_grupo,
			@cod_recurso,
			@pet_nrointerno,
			@fe_desde,
			@fe_hasta,
			@horas

		WHILE (@@sqlstatus = 0)
			BEGIN
				execute sp_GetHorasPeriodo @sol_desde, @sol_hasta, @fe_desde, @fe_hasta, @horas,
					@ret_desde OUTPUT,
					@ret_hasta OUTPUT,
					@ret_dias OUTPUT,
					@ret_horas OUTPUT

				select @horas_trab = @ret_horas/100
				
				insert #TmpSalida (
					cod_direccion,
					cod_gerencia,
					cod_sector,
					cod_grupo,
					cod_recurso,
					pet_nrointerno,
					entre_desde,
					entre_hasta,
					horas)
				values (
					@cod_direccion,
					@cod_gerencia,
					@cod_sector,
					@cod_grupo,
					@cod_recurso,
					@pet_nrointerno,
					@ret_desde,
					@ret_hasta,
					@horas_trab)

				FETCH CursHoras INTO
					@cod_direccion,
					@cod_gerencia,
					@cod_sector,
					@cod_grupo,
					@cod_recurso,
					@pet_nrointerno,
					@fe_desde,
					@fe_hasta,
					@horas
			END
		CLOSE CursHoras
		DEALLOCATE CURSOR CursHoras
	
	-- Resultado
	--select @horas_tot = sum(TS.horas)
	select *
	from #TmpSalida TS

	drop table #TmpSalida
go

grant execute on dbo.sp_HorasTrabajadasXArea to GesPetUsr
go

print 'Actualizaci�n realizada.'
