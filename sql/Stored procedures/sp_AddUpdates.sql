use GesPet
go
print 'sp_AddUpdates'
go
if exists (select * from sysobjects where name = 'sp_AddUpdates' and sysstat & 7 = 4)
drop procedure dbo.sp_AddUpdates
go

create procedure dbo.sp_AddUpdates 

as

set nocount off

declare @ocurrencias        int
declare @pet_nrointerno     int
declare @cod_estado         char(6)
declare @cod_solicitante    char(10)
declare @cod_responsable    char(10)
declare @cod_sector         char(4)
declare @hst_nrointerno     int
declare @nrointerno         int


declare cur_peticiones cursor for
    select
        a.pet_nrointerno,
        a.cod_estado,
        a.cod_solicitante,
        a.cod_sector
    from
        GesPet..Peticion a left join GesPet..PeticionConf b on a.pet_nrointerno = b.pet_nrointerno
    where
        a.cod_clase = 'SPUF' and a.cod_estado = 'TERMIN' and 
        a.pet_sox001 = 1 and (
            (b.pet_nrointerno is null) or
            (b.pet_nrointerno is not null and b.ok_tipo not in ('TEST','TESP') and not exists(select * from GesPet..PeticionConf Conf where Conf.pet_nrointerno = a.pet_nrointerno and Conf.ok_tipo in ('TEST','TESP')))
            )
    group by a.pet_nrointerno
for read only

begin tran

open cur_peticiones
    fetch cur_peticiones into
        @pet_nrointerno, @cod_estado, @cod_solicitante, @cod_sector
        while (@@SQLSTATUS = 0)
	begin
		-- Busco al responsable de Sector
		select @ocurrencias = (select count(*) from GesPet..RecursoPerfil a where (a.cod_nivel = 'SECT') and (a.cod_area = @cod_sector) and (a.cod_perfil = 'CSEC') )
		if not @ocurrencias > 1 or @ocurrencias = 0
		select @cod_responsable = (select a.cod_recurso from GesPet..RecursoPerfil a where (a.cod_nivel = 'SECT') and (a.cod_area = @cod_sector) and (a.cod_perfil = 'CSEC'))
		else
		select @cod_responsable = 'N/A'
		-- Guardo la operación en el historial
		execute GesPet..sp_AddHistorial2 @hst_nrointerno output, @pet_nrointerno, 'OKTEST', @cod_estado, NULL, NULL, NULL, NULL, @cod_responsable, @cod_solicitante
		select @nrointerno = @hst_nrointerno
		execute GesPet..sp_AddHistorialMemo @nrointerno, 0, 'Sin observaciones'
		fetch cur_peticiones into @pet_nrointerno, @cod_estado, @cod_solicitante, @cod_sector
	end
close cur_peticiones

-- Inserción masiva de conformes a peticiones de tipo Spufi
insert into GesPet..PeticionConf
	select
	a.pet_nrointerno, 'TEST', 'REQ', 'Conforme otorgado', a.cod_solicitante, getdate(), 1
	from
	GesPet..Peticion a left join GesPet..PeticionConf b on a.pet_nrointerno = b.pet_nrointerno
	where
	a.cod_clase = 'SPUF' and a.cod_estado = 'TERMIN' and 
	a.pet_sox001 = 1 and (
	(b.pet_nrointerno is null) or
	(b.pet_nrointerno is not null and b.ok_tipo not in ('TEST','TESP') and not exists(select * from GesPet..PeticionConf Conf where Conf.pet_nrointerno = a.pet_nrointerno and Conf.ok_tipo in ('TEST','TESP'))))
	group by a.pet_nrointerno
commit tran

deallocate cursor cur_peticiones
set nocount on

return(0)
go

grant execute on dbo.sp_AddUpdates to GesPetUsr 
go