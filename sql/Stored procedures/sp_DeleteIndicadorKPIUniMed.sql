/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteIndicadorKPIUniMed'
go

if exists (select * from sysobjects where name = 'sp_DeleteIndicadorKPIUniMed' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteIndicadorKPIUniMed
go

create procedure dbo.sp_DeleteIndicadorKPIUniMed
	@kpiid	int=0,
	@unimedId	char(10)=''
as
	delete from
		GesPet.dbo.IndicadorKPIUniMed
	where
		kpiid = @kpiid and
		unimedId = @unimedId
go

grant execute on dbo.sp_DeleteIndicadorKPIUniMed to GesPetUsr
go

print 'Actualización realizada.'
go
