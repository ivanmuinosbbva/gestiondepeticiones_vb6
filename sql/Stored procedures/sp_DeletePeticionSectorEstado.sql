use GesPet
go

print 'sp_DeletePeticionSectorEstado'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionSectorEstado' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionSectorEstado
go

create procedure dbo.sp_DeletePeticionSectorEstado 
    @pet_nrointerno     int, 
    @cod_estado         varchar(50)='' 
as 
	declare @flg_herdlt1    char(1) 
	declare @xcod_estado    char(8) 
	 
	select @xcod_estado = cod_estado from Peticion where pet_nrointerno=@pet_nrointerno 
	select @flg_herdlt1 = flg_herdlt1 from Estados where cod_estado=@xcod_estado 
	 
	if @flg_herdlt1='S' 
		begin 
			BEGIN TRANSACTION 
				delete GesPet..PeticionGrupo 
				  where @pet_nrointerno=pet_nrointerno and 
					(charindex(RTRIM(cod_estado),RTRIM(@cod_estado))>0) 
				
				delete GesPet..PeticionGrupo 
				  where @pet_nrointerno=pet_nrointerno and 
					cod_sector in (select cod_sector from GesPet..PeticionSector where  @pet_nrointerno=pet_nrointerno and charindex(RTRIM(cod_estado),RTRIM(@cod_estado))>0) 
				
				delete GesPet..PeticionSector 
				  where @pet_nrointerno=pet_nrointerno and 
					(charindex(RTRIM(cod_estado),RTRIM(@cod_estado))>0) 
			COMMIT TRANSACTION 
		end 
	return(0) 
go

grant execute on dbo.sp_DeletePeticionSectorEstado to GesPetUsr 
go
