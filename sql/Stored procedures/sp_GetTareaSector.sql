use GesPet
go
print 'sp_GetTareaSector'
go
if exists (select * from sysobjects where name = 'sp_GetTareaSector' and sysstat & 7 = 4)
drop procedure dbo.sp_GetTareaSector
go
create procedure dbo.sp_GetTareaSector 
         @cod_tarea     char(8)=null, 
         @cod_sector        char(8)=null 
as 
begin 
    select  
        cod_sector, 
        cod_tarea 
    from 
        GesPet..TareaSector 
    where   
        (@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(cod_sector) = RTRIM(@cod_sector)) and  
        (@cod_tarea is null or RTRIM(@cod_tarea) is null or RTRIM(@cod_tarea)='' or RTRIM(cod_tarea) = RTRIM(@cod_tarea)) 
    order by cod_sector ASC  
end 
return(0) 
go



grant execute on dbo.sp_GetTareaSector to GesPetUsr 
go


