/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.16 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 04.11.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoClase'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoClase' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoClase
go

create procedure dbo.sp_UpdateProyectoClase
	@ProjCatId		int,
	@ProjClaseId	int,
	@ProjClaseNom	char(100)
as
	update 
		GesPet.dbo.ProyectoClase
	set
		ProjCatId = @ProjCatId,
		ProjClaseNom = @ProjClaseNom
	where 
		ProjClaseId = @ProjClaseId
	return(0)
go

grant execute on dbo.sp_UpdateProyectoClase to GesPetUsr
go

print 'Actualización realizada.'
go
