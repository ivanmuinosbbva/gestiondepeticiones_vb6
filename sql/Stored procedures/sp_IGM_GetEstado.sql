/*
-000- a. FJS 23.07.2010 - Nuevo SP para devolver la correspondencia de estados entre CGM e IGM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_GetEstado'
go

if exists (select * from sysobjects where name = 'sp_IGM_GetEstado' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_GetEstado
go

create procedure dbo.sp_IGM_GetEstado
	@cod_estado		char(6)=null
as 
	select
		a.cod_IGM_estado,
		a.cod_CGM_estado,
		b.nom_estado as 'nom_CGM_estado'
	from 
		GesPet..IGM_Estados a left join
		GesPet..Estados b on (a.cod_CGM_estado = b.cod_estado)
	where
		(@cod_estado is null or a.cod_CGM_estado = @cod_estado)
	return(0)
go

grant execute on dbo.sp_IGM_GetEstado to GesPetUsr
go

grant execute on dbo.sp_IGM_GetEstado to GrpTrnIGM
go

print 'Actualización realizada.'
go