use GesPet
go
print 'sp_GetMensajesEvento'
go
if exists (select * from sysobjects where name = 'sp_GetMensajesEvento' and sysstat & 7 = 4)
drop procedure dbo.sp_GetMensajesEvento
go
create procedure dbo.sp_GetMensajesEvento 
    @evt_alcance char(3)=null, 
    @cod_accion char(8)=null, 
    @cod_estado char(6)=null, 
    @cod_perfil char(4)=null 
as 
    select  evt_alcance, 
        cod_accion, 
        cod_estado, 
        cod_perfil, 
        cod_txtmsg, 
        est_recept, 
        est_codigo, 
        txt_extend, 
        txt_pzofin, 
        flg_activo 
    from    GesPet..MensajesEvento 
    where   (RTRIM(@evt_alcance) is null or RTRIM(@evt_alcance)='' or RTRIM(evt_alcance)=RTRIM(@evt_alcance)) and 
        (RTRIM(@cod_accion) is null or RTRIM(@cod_accion)='' or RTRIM(cod_accion)=RTRIM(@cod_accion)) and 
        (RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or RTRIM(cod_estado)=RTRIM(@cod_estado)) and 
        (RTRIM(@cod_perfil) is null or RTRIM(@cod_perfil)='' or RTRIM(cod_perfil)=RTRIM(@cod_perfil)) 
 
return(0) 
go



grant execute on dbo.sp_GetMensajesEvento to GesPetUsr 
go


