/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteValoracion'
go

if exists (select * from sysobjects where name = 'sp_DeleteValoracion' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteValoracion
go

create procedure dbo.sp_DeleteValoracion
	@valorId	int=0
as
	delete from
		GesPet.dbo.Valoracion
	where
		valorId = @valorId
go

grant execute on dbo.sp_DeleteValoracion to GesPetUsr
go

print 'Actualización realizada.'
go
