/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.09.2012 - Para la exportación de SI.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHEDT003a'
go

if exists (select * from sysobjects where name = 'sp_GetHEDT003a' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHEDT003a
go

create procedure dbo.sp_GetHEDT003a
	@dsn_id	char(8)
as
	select 
		a.dsn_id,
		a.cpy_id,
		a.cpo_id,
		a.cpo_nrobyte,
		a.cpo_canbyte,
		cpo_type = (case
			when a.cpo_type = 'A' then 'ALFABETIC'
			when a.cpo_type = 'X' then 'ALFANUMERIC'
			when a.cpo_type = '9' then 'DISPLAY'
			when a.cpo_type = 'D' then 'DECIMAL'
			when a.cpo_type = 'S' then 'SMALLINT'
			when a.cpo_type = 'I' then 'INTEGER'
			when a.cpo_type = 'O' then 'DOUBLE'
			when a.cpo_type = 'F' then 'FLOAT'
			when a.cpo_type = 'C' then 'COMP'
			when a.cpo_type = '2' then 'COMP-2'
			when a.cpo_type = '3' then 'COMP-3'
			when a.cpo_type = 'B' then 'BINNARY'
			else 'N/A'
		end),
		a.cpo_decimals,
		a.cpo_dsc,
		tipo = (select y.tipo_nom from GesPet..HEDT012 x inner join GesPet..HEDT011 y on (x.tipo_id = y.tipo_id) where x.rutina = a.cpo_nomrut),
		subtipo = (select x.subtipo_nom from GesPet..HEDT012 x where x.rutina = a.cpo_nomrut),
		a.cpo_nomrut
	from 
		GesPet.dbo.HEDT003 a
	where
		a.dsn_id = @dsn_id or @dsn_id is null
	return(0)
go

grant execute on dbo.sp_GetHEDT003a to GesPetUsr
go

print 'Actualización realizada.'
go
