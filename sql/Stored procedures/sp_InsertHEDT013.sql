/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 12.11.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertHEDT013'
go

if exists (select * from sysobjects where name = 'sp_InsertHEDT013' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertHEDT013
go

create procedure dbo.sp_InsertHEDT013
	@tipo_id	char(1)=null,
	@subtipo_id	char(1)=null,
	@item	int=null,
	@item_nom	char(50)=null
as
	insert into GesPet.dbo.HEDT013 (
		tipo_id,
		subtipo_id,
		item,
		item_nom)
	values (
		@tipo_id,
		@subtipo_id,
		@item,
		@item_nom)
go

grant execute on dbo.sp_InsertHEDT013 to GesPetUsr
go

print 'Actualización realizada.'
