/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 08.05.2013 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteFabrica'
go

if exists (select * from sysobjects where name = 'sp_DeleteFabrica' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteFabrica
go

create procedure dbo.sp_DeleteFabrica
	@cod_fab	char(10)=''
as
	delete from
		GesPet.dbo.Fabrica
	where
		cod_fab = @cod_fab
go

grant execute on dbo.sp_DeleteFabrica to GesPetUsr
go

print 'Actualización realizada.'
go
