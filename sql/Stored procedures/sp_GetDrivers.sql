/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 22.04.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetDrivers'
go

if exists (select * from sysobjects where name = 'sp_GetDrivers' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetDrivers
go

create procedure dbo.sp_GetDrivers
	@driverId		int=null,
	@driverHab		char(1)=null,
	@indicadorId	int=null,
	@empresa		int=1
as
	select 
		ag.driverId,
		ag.driverNom,
		ag.driverAyuda,
		ag.driverValor,
		ag.driverResp,
		ag.driverHab,
		ag.indicadorId,
		i.indicadorNom	as indicadorNombre,
		ag.driverReq
	from 
		GesPet..Drivers ag inner join
		GesPet..Indicador i on (ag.indicadorId = i.indicadorId)
	where
		(@driverId is null or ag.driverId = @driverId) and
		(@driverHab is null or ag.driverHab = @driverHab) and
		(@indicadorId is null or ag.indicadorId = @indicadorId) and 
		(@empresa is null or i.indicadorEmp = @empresa)
	order by
		ag.indicadorId,
		ag.driverId
	return(0)
go

grant execute on dbo.sp_GetDrivers to GesPetUsr
go

print 'Actualización realizada.'
go
