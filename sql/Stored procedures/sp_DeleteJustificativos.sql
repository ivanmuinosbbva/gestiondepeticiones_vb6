/*
-000- a. FJS 04.08.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteJustificativos'
go

if exists (select * from sysobjects where name = 'sp_DeleteJustificativos' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteJustificativos
go

create procedure dbo.sp_DeleteJustificativos
	@jus_codigo		int
as
	delete from
		GesPet.dbo.Justificativos
	where
		jus_codigo = @jus_codigo
go

grant execute on dbo.sp_DeleteJustificativos to GesPetUsr
go

print 'Actualización realizada.'
