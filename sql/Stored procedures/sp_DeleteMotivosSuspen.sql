/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteMotivosSuspen'
go

if exists (select * from sysobjects where name = 'sp_DeleteMotivosSuspen' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteMotivosSuspen
go

create procedure dbo.sp_DeleteMotivosSuspen
	@cod_motivo_suspen	int
as
	delete from
		GesPet.dbo.MotivosSuspen
	where
		(cod_motivo_suspen = @cod_motivo_suspen or @cod_motivo_suspen is null)
	return(0)
go

grant execute on dbo.sp_DeleteMotivosSuspen to GesPetUsr
go

print 'Actualización realizada.'
