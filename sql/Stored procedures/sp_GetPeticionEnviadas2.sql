/*
-001- a. FJS 04.06.2008 - Se agrega la clausula ORDER BY
-002- a. FJS 22.02.2010 - Se corrige la UNION entre las tablas.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionEnviadas2'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionEnviadas2' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionEnviadas2
go

create procedure dbo.sp_GetPeticionEnviadas2
    (@pet_nrointerno    int,    
     @pet_sector        char(8),    
     @pet_grupo         char(8))   
as     
   
select  
    Pet.pet_nroasignado,
    Pet.titulo,
    Pet.cod_tipo_peticion,
    Pet.cod_clase,
    (select nom_estado from GesPet..Estados where cod_estado = Pet.cod_estado) as nom_estado,
    Pet.fe_estado,
    Env.pet_nrointerno, 
    Env.pet_sector, 
    Env.pet_grupo, 
    Env.pet_record, 
    Env.pet_date, 
    Env.pet_usrid,
    (select  nom_recurso from GesPet..Recurso where cod_recurso = Env.pet_usrid) as nom_recurso, 
    Env.pet_done 
from  
    GesPet..PeticionEnviadas Env left Join 
	GesPet..Peticion Pet on Env.pet_nrointerno = Pet.pet_nrointerno		-- upd -002- a. Antes INNER JOIN, ahora LEFT JOIN
where 
	(Env.pet_nrointerno = @pet_nrointerno OR @pet_nrointerno IS NULL) and  
	(Env.pet_sector = @pet_sector OR @pet_sector IS NULL) and  
	(Env.pet_grupo = @pet_grupo OR @pet_grupo IS NULL) 
order by
    Pet.pet_nroasignado,
    Env.pet_date
 
return(0) 
 
go

grant Execute  on dbo.sp_GetPeticionEnviadas2 to GesPetUsr 
go

print 'Actualización realizada.'