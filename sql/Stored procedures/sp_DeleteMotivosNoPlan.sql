/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteMotivosNoPlan'
go

if exists (select * from sysobjects where name = 'sp_DeleteMotivosNoPlan' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteMotivosNoPlan
go

create procedure dbo.sp_DeleteMotivosNoPlan
	@cod_motivo_noplan	int
as
	delete from
		GesPet.dbo.MotivosNoPlan
	where
		(cod_motivo_noplan = @cod_motivo_noplan or @cod_motivo_noplan is null)
	return(0)
go

grant execute on dbo.sp_DeleteMotivosNoPlan to GesPetUsr
go

print 'Actualización realizada.'
