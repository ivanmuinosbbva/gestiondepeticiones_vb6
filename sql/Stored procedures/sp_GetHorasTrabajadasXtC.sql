/*
-001- a. FJS 12.07.2016 - Nuevo:
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHorasTrabajadasXtC'
go

if exists (select * from sysobjects where name = 'sp_GetHorasTrabajadasXtC' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHorasTrabajadasXtC
go

create procedure dbo.sp_GetHorasTrabajadasXtC
	@opcion			char(1)='0',			-- Opci�n: 0:Vinculado / 1:No vinculado
	@cod_recurso	char(10),
	@fechadesde		smalldatetime=null,
	@fechahasta		smalldatetime=null
as
	declare @secuencia		int
	declare @ret_status     int

	create table #AgrupArbol(
		secuencia		int,
		nivel 			int,
		agr_nrointerno 	int)

	create table #tablaCriterio (
		pet_nrointerno		int,
		categoria			char(6))

	create table #AgrupXPeticSDA (
		pet_nrointerno		int,
		agr_nrointerno		int) 
	
	declare	@agr_SDA	int
	
	SELECT @agr_SDA = var_numero
	FROM Varios
	WHERE var_codigo = 'AGRP_SDA'

	if @agr_SDA<>0
		begin  
			select @secuencia=0
			execute @ret_status = sp_GetAgrupArbol @agr_SDA, 0, @secuencia OUTPUT  
			if (@ret_status <> 0)
				begin  
					if (@ret_status = 30003)  
					begin
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
						return (@ret_status)
					end  
				end
			insert into #AgrupXPeticSDA (pet_nrointerno, agr_nrointerno)
				select AP.pet_nrointerno, AP.agr_nrointerno  
				from #AgrupArbol AA, AgrupPetic AP, Agrup AG
				where 
					AA.agr_nrointerno=AP.agr_nrointerno and  
					AG.agr_nrointerno=AP.agr_nrointerno and 
					AG.agr_vigente='S'
			
			INSERT INTO #tablaCriterio
			select 
				p.pet_nrointerno,
				categoria = case
					when p.cod_tipo_peticion in ('AUI','AUX') OR p.pet_regulatorio = 'S' then '1. REG'
					when exists (select x.pet_nrointerno from #AgrupXPeticSDA x where x.pet_nrointerno = p.pet_nrointerno) then '2. SDA'
					when exists (select count(1) from ProyectoIDM px where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 7 having count(1) > 0) then '3. PES'
					when exists (select count(1) from ProyectoIDM px where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId = 8 having count(1) > 0) then '4. ENG'
					when exists (
						 select count(1) 
						 from ProyectoIDM px 
						 where p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId and px.ProjCatId NOT IN (7,8) 
						 having count(1) > 0) OR (p.valor_impacto <> 0 OR p.valor_facilidad <>0) then '5. PRI'
					when p.cod_clase in ('OPTI','CORR','SPUF','ATEN') then '6. CSI'
					when p.cod_clase = 'SINC' then '---'
					else '7. SPR'
				end
			from Peticion p 
		end
	
	IF (@opcion = '0') 
		BEGIN
			select
				p.pet_nrointerno															as pet_nrointerno,
				p.pet_nroasignado															as pet_nroasignado,
				p.cod_clase																	as cod_clase,
				c.categoria																	as categoria,
				p.titulo																	as titulo,
				pg.horaspresup																as horas_presup_grupo,
				/*
				-- Otras horas de otros recursos del grupo asignados a la petici�n 
				(select SUM(convert(REAL,ht1.horas) / convert(REAL,60))
				from 
					HorasTrabajadas ht1 inner join
					PeticionRecurso pr1 on (pr1.pet_nrointerno = ht1.pet_nrointerno) inner join
					PeticionGrupo pg1 on (
						pg1.pet_nrointerno = pr1.pet_nrointerno and pg1.cod_grupo = pr1.cod_grupo and 
						pg1.cod_sector = pr1.cod_sector and pg1.cod_gerencia = pr1.cod_gerencia and 
						pg1.cod_direccion = pr1.cod_direccion)
				where
					ht1.pet_nrointerno = ht.pet_nrointerno and 
					pg1.cod_grupo = pr.cod_grupo and 
					pg1.cod_sector = pr.cod_sector and 
					pg1.cod_gerencia = pr.cod_gerencia and 
					pg1.cod_direccion = pr.cod_direccion and 
					(@fechahasta is null or ht1.fe_desde <= @fechahasta) and
					(@fechadesde is null or ht1.fe_hasta >= @fechadesde) and
					ht1.cod_recurso <> @cod_recurso)										as horas_cargadas_otros,
				*/
				isnull(SUM(convert(REAL,ht.horas) / convert(REAL,60)),0)					as horas_cargadas_recurso,
				isnull( 
					((SUM(convert(REAL,ht.horas) / convert(REAL,60))) / 
					case 
						when pg.horaspresup is null or pg.horaspresup = 0 then 1
						else pg.horaspresup
				end) * 100,0)	as porcj_carga
			from 
				PeticionRecurso pr inner join
				PeticionGrupo pg on (
						pg.pet_nrointerno = pr.pet_nrointerno and 
						pg.cod_grupo = pr.cod_grupo and 
						pg.cod_sector = pr.cod_sector and 
						pg.cod_gerencia = pr.cod_gerencia and 
						pg.cod_direccion = pr.cod_direccion) inner join 
				Peticion p on (p.pet_nrointerno = pr.pet_nrointerno) inner join
				#tablaCriterio c on (p.pet_nrointerno = c.pet_nrointerno) left join
				HorasTrabajadas ht on (c.pet_nrointerno = ht.pet_nrointerno and ht.cod_recurso = @cod_recurso and (@fechadesde is null or ht.fe_desde >= @fechadesde) and (@fechahasta is null or ht.fe_hasta <= @fechahasta))
			where
				/*
				(@fechahasta is null or ht.fe_desde <= @fechahasta) and
				(@fechadesde is null or ht.fe_hasta >= @fechadesde) and
				*/
				/* ESTE
				(@fechadesde is null or ht.fe_desde >= @fechadesde) and
				(@fechahasta is null or ht.fe_hasta <= @fechahasta) and
				*/
				pr.cod_recurso = @cod_recurso and 
				charindex(p.cod_estado,'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')=0
			group by
				p.pet_nrointerno,
				p.pet_nroasignado,
				p.cod_clase,
				c.categoria,
				p.titulo,
				pg.horaspresup
			order by 
				c.categoria
		END
	ELSE BEGIN
		select
			p.pet_nrointerno															as pet_nrointerno,
			p.pet_nroasignado															as pet_nroasignado,
			p.cod_clase																	as cod_clase,
			c.categoria																	as categoria,
			p.titulo																	as titulo,
			pg.horaspresup																as horas_presup_grupo,
			isnull(SUM(convert(REAL,ht.horas) / convert(REAL,60)),0)					as horas_cargadas_recurso
		from 
			HorasTrabajadas ht inner join
			Peticion p on (p.pet_nrointerno = ht.pet_nrointerno) inner join
			#tablaCriterio c on (p.pet_nrointerno = c.pet_nrointerno) LEFT join
			PeticionRecurso pr on (pr.pet_nrointerno = c.pet_nrointerno) LEFT join
			PeticionGrupo pg on (
					pg.pet_nrointerno = pr.pet_nrointerno and 
					pg.cod_grupo = pr.cod_grupo and 
					pg.cod_sector = pr.cod_sector and 
					pg.cod_gerencia = pr.cod_gerencia and 
					pg.cod_direccion = pr.cod_direccion)
		where
			ht.cod_recurso = @cod_recurso and 
			charindex(p.cod_estado,'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')=0 and 
			(@fechadesde is null or ht.fe_desde >= @fechadesde) and
			(@fechahasta is null or ht.fe_hasta <= @fechahasta)
		group by
			p.pet_nrointerno,
			p.pet_nroasignado,
			p.cod_clase,
			c.categoria,
			p.titulo,
			pg.horaspresup
		UNION
		select
			p.pet_nrointerno															as pet_nrointerno,
			p.pet_nroasignado															as pet_nroasignado,
			p.cod_clase																	as cod_clase,
			c.categoria																	as categoria,
			p.titulo																	as titulo,
			pg.horaspresup																as horas_presup_grupo,
			isnull(SUM(convert(REAL,ht.horas) / convert(REAL,60)),0)					as horas_cargadas_recurso
		from 
			PeticionRecurso pr inner join
			PeticionGrupo pg on (
					pg.pet_nrointerno = pr.pet_nrointerno and 
					pg.cod_grupo = pr.cod_grupo and 
					pg.cod_sector = pr.cod_sector and 
					pg.cod_gerencia = pr.cod_gerencia and 
					pg.cod_direccion = pr.cod_direccion) inner join 
			Peticion p on (p.pet_nrointerno = pr.pet_nrointerno) inner join
			#tablaCriterio c on (p.pet_nrointerno = c.pet_nrointerno) LEFT join
			HorasTrabajadas ht on (c.pet_nrointerno = ht.pet_nrointerno and ht.cod_recurso = pr.cod_recurso and (@fechadesde is null or ht.fe_desde >= @fechadesde) and (@fechahasta is null or ht.fe_hasta <= @fechahasta))
		where
			pr.cod_recurso = @cod_recurso and 
			charindex(p.cod_estado,'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')=0 
		group by
			p.pet_nrointerno,
			p.pet_nroasignado,
			p.cod_clase,
			c.categoria,
			p.titulo,
			pg.horaspresup
		order by 
			c.categoria
	END
	return(0)
go

grant execute on dbo.sp_GetHorasTrabajadasXtC to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
