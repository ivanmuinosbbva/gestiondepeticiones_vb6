/*
-001- a. - FJS 28.05.2008 - Se cambia la manera de guardar la fecha para poder incluir la hora.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_AddHistorial'
go

if exists (select * from sysobjects where name = 'sp_AddHistorial' and sysstat & 7 = 4)
	drop procedure dbo.sp_AddHistorial
go

create procedure dbo.sp_AddHistorial 
    @hst_nrointerno		int OUTPUT, 
    @pet_nrointerno		int, 
    @cod_evento			varchar(8)	= null, 
    @pet_estado			varchar(6)  = null, 
    @cod_sector			char(8)		= null, 
    @sec_estado			varchar(6)	= null, 
    @cod_grupo			char(8)		= null, 
    @gru_estado			varchar(6)  = null, 
    @replc_user			varchar(10) = null,
	@cod_perfil			char(4)		= null
as

	declare @proxnumero int 
	select @proxnumero = 0 
	execute sp_HistorialProximo @proxnumero OUTPUT 
	 
	select @hst_nrointerno = @proxnumero 
	 
	insert into GesPet..Historial (
		pet_nrointerno, 
		hst_nrointerno, 
		cod_evento, 
		pet_estado, 
		cod_sector, 
		sec_estado, 
		cod_grupo, 
		gru_estado, 
		replc_user, 
		audit_user, 
		hst_fecha,
		cod_perfil) 
	values (
		@pet_nrointerno, 
		@hst_nrointerno, 
		@cod_evento, 
		@pet_estado, 
		@cod_sector, 
		@sec_estado, 
		@cod_grupo, 
		@gru_estado, 
		@replc_user, 
		SUSER_NAME(),
		getdate(),
		@cod_perfil)
	return(0)
go

grant execute on dbo.sp_AddHistorial to GesPetUsr 
go

grant execute on sp_AddHistorial to RolGesIncConex
go

sp_procxmode 'sp_AddHistorial', anymode
go

print 'Actualización realizada.'
go

