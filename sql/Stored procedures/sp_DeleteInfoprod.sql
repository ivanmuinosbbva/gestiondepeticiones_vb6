/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteInfoprod'
go

if exists (select * from sysobjects where name = 'sp_DeleteInfoprod' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteInfoprod
go

create procedure dbo.sp_DeleteInfoprod
	@infoprot_id	int=0,
	@infoprot_item	int=0
as
	delete from
		GesPet.dbo.Infoprod
	where
		(infoprot_id = @infoprot_id or @infoprot_id is null) and
		(infoprot_item = @infoprot_item or @infoprot_item is null)
go

grant execute on dbo.sp_DeleteInfoprod to GesPetUsr
go

print 'Actualización realizada.'
go
