/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 02.10.2009 - 
-001- a. FJS 03.11.2009 - Se agrega el campo de actualización de estado por proceso de transmisión.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHEDT003'
go

if exists (select * from sysobjects where name = 'sp_GetHEDT003' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHEDT003
go

create procedure dbo.sp_GetHEDT003
	@dsn_id	char(8)=null,
	@cpy_id	char(8)=null,
	@cpo_id	char(18)=null
as
	select 
		a.dsn_id,
		a.cpy_id,
		a.cpo_id,
		a.cpo_nrobyte,
		a.cpo_canbyte,
		a.cpo_type,
		a.cpo_decimals,
		a.cpo_dsc,
		a.cpo_nomrut,
		a.cpo_feult,
		a.cpo_userid,
		b.estado,
		a.fe_hst_estado,			-- add -001- a.
		a11.tipo_nom,
		a12.subtipo_nom
	from 
		GesPet..HEDT003 a inner join 
		GesPet..HEDT001 b on (a.dsn_id = b.dsn_id) left join
		GesPet..HEDT012 a12 on (a12.rutina = a.cpo_nomrut) left join
		GesPet..HEDT011 a11 on (a11.tipo_id = a12.tipo_id)
	where
		(a.dsn_id = @dsn_id or @dsn_id is null) and
		(a.cpy_id = @cpy_id or @cpy_id is null) and
		(a.cpo_id = @cpo_id or @cpo_id is null)
go

grant execute on dbo.sp_GetHEDT003 to GesPetUsr
go

print 'Actualización realizada.'
