/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetMotivosNoPlan'
go

if exists (select * from sysobjects where name = 'sp_GetMotivosNoPlan' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetMotivosNoPlan
go

create procedure dbo.sp_GetMotivosNoPlan
	@cod_motivo_noplan	int=null,
	@estado_hab			char(1)=null
as
	select 
		a.cod_motivo_noplan,
		a.nom_motivo_noplan,
		a.estado_hab
	from 
		GesPet.dbo.MotivosNoPlan a
	where
		(a.cod_motivo_noplan = @cod_motivo_noplan or @cod_motivo_noplan is null) and
		(a.estado_hab = @estado_hab or @estado_hab is null)
	return(0)
go

grant execute on dbo.sp_GetMotivosNoPlan to GesPetUsr
go

print 'Actualización realizada.'
