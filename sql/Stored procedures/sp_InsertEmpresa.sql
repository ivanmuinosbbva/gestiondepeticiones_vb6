/*
-000- a. FJS 11.01.2016 - SPs para el mantenimiento de la tabla Empresa.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertEmpresa'
go

if exists (select * from sysobjects where name = 'sp_InsertEmpresa' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertEmpresa
go

create procedure dbo.sp_InsertEmpresa
	@empid		int,
	@empnom		varchar(50)='',
	@emphab		char(1),
	@empabrev	varchar(30)=''
as
	insert into GesPet.dbo.Empresa (
		empid,
		empnom,
		emphab,
		empabrev)
	values (
		@empid,
		@empnom,
		@emphab,
		@empabrev)
	return(0)
go

grant execute on dbo.sp_InsertEmpresa to GesPetUsr
go

print 'Actualización realizada.'
go