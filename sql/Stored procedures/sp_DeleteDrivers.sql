/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteDrivers'
go

if exists (select * from sysobjects where name = 'sp_DeleteDrivers' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteDrivers
go

create procedure dbo.sp_DeleteDrivers
	@driverId	int=0
as
	delete from
		GesPet.dbo.Drivers
	where
		driverId = @driverId
go

grant execute on dbo.sp_DeleteDrivers to GesPetUsr
go

print 'Actualización realizada.'
go
