/*
-000- a. FJS 04.03.2009 - Nuevo SP: devuelve la estructura jerárquica de un recurso en perticular
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetRecursoEstructura'
go

if exists (select * from sysobjects where name = 'sp_GetRecursoEstructura' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRecursoEstructura
go

create procedure dbo.sp_GetRecursoEstructura
	@cod_recurso		char(10)=''
as 
	select
		r.cod_recurso,
		r.nom_recurso,
		r.cod_direccion,
		nom_direccion = (select nom_direccion from GesPet..Direccion where cod_direccion = r.cod_direccion),
		r.cod_gerencia,
		nom_gerencia = (select nom_gerencia from GesPet..Gerencia where cod_gerencia = r.cod_gerencia),
		r.cod_sector,
		nom_sector = (select nom_sector from GesPet..Sector where cod_sector = r.cod_sector),
		r.cod_grupo,
		nom_grupo = (select nom_grupo from GesPet..Grupo where cod_grupo = r.cod_grupo),
		r.flg_cargoarea
	from
		GesPet..Recurso r
	where
		@cod_recurso is null or r.cod_recurso = @cod_recurso
	order by
		r.cod_recurso
go

grant execute on dbo.sp_GetRecursoEstructura to GesPetUsr
go

print 'Actualización realizada.'
