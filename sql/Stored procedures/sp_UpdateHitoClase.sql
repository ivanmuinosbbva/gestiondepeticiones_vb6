/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.22 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 16.03.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateHitoClase'
go

if exists (select * from sysobjects where name = 'sp_UpdateHitoClase' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateHitoClase
go

create procedure dbo.sp_UpdateHitoClase
	@hitoid	int,
	@hitodesc	char(60),
	@hitoexpos	char(1)
as
	update 
		GesPet.dbo.HitoClase
	set
		hitodesc = @hitodesc,
		hitoexpos = @hitoexpos
	where 
		(hitoid = @hitoid or @hitoid is null)
go

grant execute on dbo.sp_UpdateHitoClase to GesPetUsr
go

print 'Actualización realizada.'
go
