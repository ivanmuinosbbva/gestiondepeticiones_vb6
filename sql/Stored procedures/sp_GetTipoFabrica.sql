/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 24.06.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetTipoFabrica'
go

if exists (select * from sysobjects where name = 'sp_GetTipoFabrica' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetTipoFabrica
go

create procedure dbo.sp_GetTipoFabrica
	@TipoFabId	int=null
as
	select 
		p.TipoFabId,
		p.TipoFabNom
	from 
		GesPet.dbo.TipoFabrica p
	where
		(p.TipoFabId = @TipoFabId or @TipoFabId is null)
	return(0)
go

grant execute on dbo.sp_GetTipoFabrica to GesPetUsr
go

print 'Actualización realizada.'
go
