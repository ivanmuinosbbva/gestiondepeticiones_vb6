/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. FJS 06.07.2009 - Se corrige porque esta calculando mal.
-002- a. FJS 21.07.2009 - Se corrige porque esta calculando mal.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_rptHorasTrabajadasRecurso2'
go

if exists (select * from sysobjects where name = 'sp_rptHorasTrabajadasRecurso2' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptHorasTrabajadasRecurso2
go

create procedure dbo.sp_rptHorasTrabajadasRecurso2 
    @cod_recurso    varchar(10), 
    @fec_desde		smalldatetime, 
    @fec_hasta		smalldatetime 
as 
 
declare @pet_desde  smalldatetime 
declare @pet_hasta  smalldatetime 
declare @pet_horas  numeric(10,2) 
 
declare @aux_fecha  smalldatetime 
declare @aux_horas  numeric(10,2) 
 
declare @dias_totales int 
declare @dias_habiles int 
declare @diasferiados int 

declare @horasxdia		numeric(10,2)
declare @horasxdiafer	numeric(10,2)	-- add -002- a.

declare @texto	char(100)
 
 
CREATE TABLE #TmpSalida( 
        fe_infor	smalldatetime, 
        horas_trab	numeric(10,2), 
        horas_asig	numeric(10,2),
		horas_feri	numeric(10,2)
) 


-- Primero gerenar todos los dias del período 
select @aux_fecha = @fec_desde 

select @horasxdiafer = 0	-- add -002- a.

WHILE (convert(char(8),@aux_fecha,112) <= convert(char(8),@fec_hasta,112)) 
	BEGIN 
		BEGIN 
			insert into #TmpSalida (
				fe_infor, 
				horas_trab, 
				horas_asig,
				horas_feri)
			values (
				@aux_fecha, 
				0, 
				0,
				0)  
		END 
		select @aux_fecha = dateadd(day, 1, @aux_fecha) 
	END 
 
DECLARE CursHoras CURSOR FOR 
	select  
		fe_desde, 
		fe_hasta, 
		horas 
	from    
		GesPet..HorasTrabajadas 
	where   
		RTRIM(cod_recurso) = RTRIM(@cod_recurso) and 
		convert(char(8),fe_desde,112) <= convert(char(8),@fec_hasta,112) and 
		convert(char(8),fe_hasta,112) >= convert(char(8),@fec_desde,112)
	--{ add -002- a.
	order by
		cod_recurso, fe_desde, fe_hasta
	--}
	for read only 
 
 
OPEN CursHoras 
FETCH CursHoras INTO  
    @pet_desde, 
    @pet_hasta, 
    @pet_horas 
WHILE (@@sqlstatus = 0) 
	BEGIN 
		-- dias habiles del periodo para la peticion/tarea
		select @dias_totales = datediff(day, @pet_desde, @pet_hasta) + 1 

		select @dias_habiles = @dias_totales 

		select 
			@diasferiados = count(1) 
		from 
			GesPet..Feriado 
		where 
			convert(char(8),fecha,112) >= convert(char(8),@pet_desde,112) and 
			convert(char(8),fecha,112) <= convert(char(8),@pet_hasta,112) 

		if @diasferiados < @dias_totales 
			select @dias_habiles = @dias_habiles - @diasferiados 
			select @horasxdia = convert(numeric(10,2), @pet_horas / @dias_habiles)
			select @aux_fecha = @pet_desde 
			
		/* con cada dia del periodo */ 
		WHILE (convert(char(8),@aux_fecha,112) <= convert(char(8),@pet_hasta,112)) 
			BEGIN 
				/* si esta en el periodo solicitado */ 
				if convert(char(8),@aux_fecha,112) <= convert(char(8),@fec_hasta,112) and convert(char(8),@aux_fecha,112) >= convert(char(8),@fec_desde,112) 
				BEGIN 
					-- si no es feriado
					if not exists (select 1 from GesPet..Feriado where convert(char(8),fecha,112) = convert(char(8),@aux_fecha,112))
						BEGIN 
							if exists (select 1 from #TmpSalida where convert(char(8),fe_infor,112) = convert(char(8),@aux_fecha,112)) 
								BEGIN 
									update #TmpSalida 
									set 
										horas_trab = convert(numeric(10,2), horas_trab + @horasxdia)	-- add -001- a.
									--set horas_trab = horas_trab + convert(numeric(10,2), @pet_horas) / (datediff(day, @pet_desde, @pet_hasta) + 1)	-- del -001- a.
									where convert(char(8),fe_infor,112) = convert(char(8),@aux_fecha,112)  
								END 
							/*
							else
								BEGIN 
									insert into #TmpSalida (
										fe_infor,
										horas_trab,
										horas_asig)     
									values (
										@aux_fecha,
										convert(numeric(10,2), @horasxdia),
										convert(numeric(10,2), 0))
								END 
							*/
						END 
					-- es un feriado
					else
						/* del -002- a.
						update #TmpSalida 
						set horas_feri = convert(numeric(10,2), horas_feri + @horasxdia)
						where convert(char(8),fe_infor,112) = convert(char(8),@aux_fecha,112)
						*/

						--{ add -001- a.
						if exists (select 1 from GesPet..HorasTrabajadas where 
									RTRIM(cod_recurso) = RTRIM(@cod_recurso) and 
									convert(char(8),fe_desde,112) = convert(char(8),@aux_fecha,112) and 
									convert(char(8),fe_hasta,112) = convert(char(8),@aux_fecha,112) and
									horas = @horasxdia
								  )
							begin
								update #TmpSalida 
								set horas_trab = convert(numeric(10,2), horas_trab + @horasxdia)
								where convert(char(8),fe_infor,112) = convert(char(8),@aux_fecha,112)
							end
						--}
						/* del -001- a.
						update #TmpSalida 
							set 
								horas_trab = convert(numeric(10,2), horas_trab + @horasxdia)
							--set horas_trab = horas_trab + convert(numeric(10,2), @pet_horas) / (datediff(day, @pet_desde, @pet_hasta) + 1)
							where convert(char(8),fe_infor,112) = convert(char(8),@aux_fecha,112)
						*/
				END 
				select @aux_fecha = dateadd(day,1,@aux_fecha) 
			END 
		FETCH CursHoras INTO  
			@pet_desde, 
			@pet_hasta, 
			@pet_horas 
	END 
CLOSE CursHoras 
DEALLOCATE CURSOR CursHoras 
 
select 
	fe_infor, 
    convert(int,horas_trab) as horas_trab,
	@horasxdiafer as horas_feri		-- add -002- a.
from 
	#TmpSalida  
order by 
	fe_infor 
 
return(0)   
go

grant execute on dbo.sp_rptHorasTrabajadasRecurso2 to GesPetUsr 
go

print 'Actualización realizada.'
go
