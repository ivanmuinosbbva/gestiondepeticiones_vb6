/*
-000- a. FJS 14.05.2009 - Nuevo SP para devolver al responsable a cargo de un �rea espec�fica.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetResponsableAreaFinal'
go

if exists (select * from sysobjects where name = 'sp_GetResponsableAreaFinal' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetResponsableAreaFinal
go

create procedure dbo.sp_GetResponsableAreaFinal
	@nivel	char(4),
	@area	char(8)
as 
	select 
		b.cod_recurso,
		b.nom_recurso
	from 
		GesPet..Recurso b 
	where 
		-- Nivel: Direcci�n
		(
			@nivel = 'DIRE' and b.cod_direccion = @area and
			(b.cod_gerencia = '' or b.cod_gerencia = '' or b.cod_gerencia = space(col_length('GesPet..Recurso', 'b.cod_gerencia'))) and
			(b.cod_sector = '' or b.cod_sector = '' or b.cod_sector = space(col_length('GesPet..Recurso', 'b.cod_sector'))) and
			(b.cod_grupo = '' or b.cod_grupo = '' or b.cod_grupo = space(col_length('GesPet..Recurso', 'b.cod_grupo'))) and
			b.flg_cargoarea = 'S'
		) or 
		-- Nivel: Gerencia
		(
			@nivel = 'GERE' and b.cod_gerencia = @area and
			(b.cod_sector = '' or b.cod_sector = '' or b.cod_sector = space(col_length('GesPet..Recurso', 'b.cod_sector'))) and
			(b.cod_grupo = '' or b.cod_grupo = '' or b.cod_grupo = space(col_length('GesPet..Recurso', 'b.cod_grupo'))) and
			b.flg_cargoarea = 'S'
		) or 
		-- Nivel: Sectores
		(
			@nivel = 'SECT' and b.cod_sector = @area and
			(b.cod_grupo = '' or b.cod_grupo = '' or b.cod_grupo = space(col_length('GesPet..Recurso', 'b.cod_grupo'))) and
			b.flg_cargoarea = 'S'
		) or 
		-- Nivel: Grupos
		(
			@nivel = 'GRUP' and b.cod_grupo = @area and
			b.flg_cargoarea = 'S'
		)
	return(0)
go

grant execute on dbo.sp_GetResponsableAreaFinal to GesPetUsr
go

print 'Actualizaci�n realizada.'
