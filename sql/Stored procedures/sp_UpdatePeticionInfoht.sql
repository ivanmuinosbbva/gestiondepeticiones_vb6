use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionInfoht'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionInfoht' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionInfoht
go

create procedure dbo.sp_UpdatePeticionInfoht
    @borra_last			char(1)='N',
    @pet_nrointerno		int,
	@info_id			int,
	@info_idver			int,
    @mem_secuencia		smallint,
    @mem_texto			varchar(255)
as
	if exists (
		select pet_nrointerno from GesPet..PeticionInfoht
		where 
			pet_nrointerno = @pet_nrointerno and
			info_id = @info_id and
			info_idver = @info_idver and 
			info_renglon = @mem_secuencia)
		begin
			update GesPet..PeticionInfoht
			set info_renglontexto = @mem_texto
			where 
				pet_nrointerno = @pet_nrointerno and
				info_id = @info_id and
				info_idver = @info_idver and 
				info_renglon = @mem_secuencia
		end
	else
		begin
		   insert into GesPet..PeticionInfoht
			   (pet_nrointerno,
				info_id,
				info_idver,
				info_renglon,
				info_renglontexto)
		   values
			   (@pet_nrointerno,
			    @info_id,
				@info_idver,
				@mem_secuencia,
				@mem_texto)
		end

	if @borra_last = 'S'
		begin
			delete from GesPet..PeticionInfoht
			where 
				pet_nrointerno = @pet_nrointerno and
				info_id = @info_id and
				info_idver = @info_idver and 
				info_renglon > @mem_secuencia
		end
	return(0)
go

grant execute on dbo.sp_UpdatePeticionInfoht to GesPetUsr
go

print 'Actualización realizada.'
go
