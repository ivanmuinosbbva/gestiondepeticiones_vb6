use GesPet
go
print 'sp_UpdateBalanceRubro'
go
if exists (select * from sysobjects where name = 'sp_UpdateBalanceRubro' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateBalanceRubro
go
create procedure dbo.sp_UpdateBalanceRubro
	@cod_BalanceRubro	char(8),
	@nom_BalanceRubro	char(30),
	@flg_habil		char(1)=null
as



if not exists (select cod_BalanceRubro from GesPet..BalanceRubro where RTRIM(cod_BalanceRubro) = RTRIM(@cod_BalanceRubro))
	begin 
		raiserror 30011 'Balance-Rubro Inexistente'   
		select 30011 as ErrCode , 'Balance-Rubro Inexistente' as ErrDesc   
		return (30011)   
	end 

BEGIN TRANSACTION
	if RTRIM(@flg_habil)='N'
	begin
		update GesPet..BalanceSubRubro
		set flg_habil = 'N'
		from	GesPet..BalanceSubRubro Bsr
		where   (RTRIM(Bsr.cod_BalanceRubro) = RTRIM(@cod_BalanceRubro))
	end
	update GesPet..BalanceRubro
	set nom_BalanceRubro = @nom_BalanceRubro,
		flg_habil = @flg_habil
	where RTRIM(cod_BalanceRubro) = RTRIM(@cod_BalanceRubro)
COMMIT

return(0)
go

grant execute on dbo.sp_UpdateBalanceRubro to GesPetUsr
go
