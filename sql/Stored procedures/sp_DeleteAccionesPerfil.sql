use GesPet
go
print 'sp_DeleteAccionesPerfil'
go
if exists (select * from sysobjects where name = 'sp_DeleteAccionesPerfil' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteAccionesPerfil
go
create procedure dbo.sp_DeleteAccionesPerfil 
            @cod_accion  char(8)=null, 
            @cod_perfil  char(4)=null, 
            @cod_estado  char(6)=null, 
            @cod_estnew  char(6)=null 
as 
 
    delete GesPet..AccionesPerfil  
    where   (@cod_accion is null or RTRIM(@cod_accion) is null or RTRIM(@cod_accion)='' or RTRIM(cod_accion) = RTRIM(@cod_accion)) and  
        (@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or RTRIM(cod_estado) = RTRIM(@cod_estado)) and  
        (@cod_perfil is null or RTRIM(@cod_perfil) is null or RTRIM(@cod_perfil)='' or RTRIM(cod_perfil) = RTRIM(@cod_perfil)) and 
        (@cod_estnew is null or RTRIM(@cod_estnew) is null or RTRIM(@cod_estnew)='' or RTRIM(cod_estnew) = RTRIM(@cod_estnew)) 
return(0) 
go



grant execute on dbo.sp_DeleteAccionesPerfil to GesPetUsr 
go


