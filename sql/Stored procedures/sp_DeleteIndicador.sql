/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteIndicador'
go

if exists (select * from sysobjects where name = 'sp_DeleteIndicador' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteIndicador
go

create procedure dbo.sp_DeleteIndicador
	@indicadorId	int=0
as
	delete from
		GesPet.dbo.Indicador
	where
		indicadorId = @indicadorId
go

grant execute on dbo.sp_DeleteIndicador to GesPetUsr
go

print 'Actualización realizada.'
go
