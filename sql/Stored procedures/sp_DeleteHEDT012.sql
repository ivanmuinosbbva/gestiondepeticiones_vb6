/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 20.10.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteHEDT012'
go

if exists (select * from sysobjects where name = 'sp_DeleteHEDT012' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteHEDT012
go

create procedure dbo.sp_DeleteHEDT012
	@tipo_id	char(1)=null,
	@subtipo_id	char(1)=null,
	@subtipo_nom	char(50)=null
as
	delete from
		GesPet.dbo.HEDT012
	where
		(tipo_id = @tipo_id or @tipo_id is null) and
		(subtipo_id = @subtipo_id or @subtipo_id is null) and
		(subtipo_nom = @subtipo_nom or @subtipo_nom is null)
go

grant execute on dbo.sp_DeleteHEDT012 to GesPetUsr
go

print 'Actualización realizada.'
