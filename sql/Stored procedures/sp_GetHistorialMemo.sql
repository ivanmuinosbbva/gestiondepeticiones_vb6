/*
-001- a. FJS 05.03.2010 - Se modifica el condicionamiento porque esta demorando mucho tiempo.
*/

use GesPet
go

print 'sp_GetHistorialMemo'
go

if exists (select * from sysobjects where name = 'sp_GetHistorialMemo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHistorialMemo
go

create procedure dbo.sp_GetHistorialMemo 
	@hst_nrointerno   int=null   
as   
    select 
		hst_nrointerno,
		hst_secuencia,
		mem_texto   
    from 
		GesPet..HistorialMemo   
    where 
		hst_nrointerno = @hst_nrointerno								-- add -001- a.
		--(@hst_nrointerno is null or hst_nrointerno=@hst_nrointerno)	-- del -001- a.
    order by 
		hst_nrointerno,
		hst_secuencia
	
	return(0)
go

grant execute on dbo.sp_GetHistorialMemo to GesPetUsr 
go


