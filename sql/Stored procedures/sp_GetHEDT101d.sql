/*
-000- a. FJS 04.09.2012 - Nuevo SP para devolver los archivos DSN válidos.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHEDT101d'
go

if exists (select * from sysobjects where name = 'sp_GetHEDT101d' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHEDT101d
go

create procedure dbo.sp_GetHEDT101d
	@dsn_nomprod	char(50)
as 
	select 
		a.dsn_id,
		a.dsn_nom,
		a.dsn_feult
	from 
		GesPet..HEDT001 a
	where 
		a.dsn_nomprod = @dsn_nomprod and (
		(a.dsn_enmas = 'N' and (a.dsn_nomprod is not null and ltrim(rtrim(a.dsn_nomprod)) <> '')) or
		(((select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = a.dsn_id) > 0 and
			(select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id) > 0 and a.dsn_id not in
			(select x.dsn_id from GesPet..HEDT003 x where x.dsn_id = a.dsn_id and x.cpo_nomrut = '' or x.cpo_nomrut is null)) 
			and a.dsn_enmas = 'S' and (a.dsn_nomprod is not null and ltrim(rtrim(a.dsn_nomprod)) <> '')))
	order by 
		a.dsn_nom, a.dsn_feult desc
	return(0)
go

grant execute on dbo.sp_GetHEDT101d to GesPetUsr
go

print 'Actualización realizada.'
go



--upper(a.dsn_nomprod) like '%' + upper(LTRIM(RTRIM(@dsn_nomprod))) + '%' and (