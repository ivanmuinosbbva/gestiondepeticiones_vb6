/*
-000- a. FJS 17.01.2017 - Nuevo SP para alta autom�tica de procesos de planificaci�n/priorizaci�n.

PeticionPlancab
***************
per_nrointerno	int					-- Nro. de per�odo de planificaci�n.
pet_nrointerno	int					-- Nro. interno de la petici�n.
prioridad		int					-- Nro. entero positivo para designar el orden de ejecuci�n de la petici�n.
cod_bpe			char(10)			-- Referente de RGyP que asigna la prioridad.
plan_origen		int					-- Origen de la planificaci�n: proceso (1) o manual (2). 
plan_actfch		smalldatetime		-- Fecha en que la petici�n fu� inclu�da en este periodo de planificaci�n.
plan_actusr		char(10)			-- Usuario que realizo la inclusi�n.

*/

use GesPet
go

print 'Creando/actualizando SP: sp_GeneracionPlanificacion1'
go

if exists (select * from sysobjects where name = 'sp_GeneracionPlanificacion1' and sysstat & 7 = 4)
	drop procedure dbo.sp_GeneracionPlanificacion1
go

create procedure dbo.sp_GeneracionPlanificacion1
as 
	declare @hoy					smalldatetime
	declare @per_nrointerno_nuevo	int
	declare @per_nombre				varchar(100)
	declare @per_abrev				varchar(15)
	declare @mes_nombre				varchar(60)
	declare @per_estado				char(6)
	declare @inicio					smalldatetime
	declare @per_fedesde			smalldatetime
	declare @per_fehasta			smalldatetime
	declare @frecuencia				char(1)
	declare @proj_estategico		int
	declare @proj_engineering		int
	
	declare @cierre_periodo			int
	declare @hst_nrointerno			int
	declare @c_pet_nrointerno		int
	declare @c_pet_nroasignado		int
	declare @c_cod_estado			char(6)
	declare @mensaje				varchar(255)

	declare @inicio_proceso_aprobacion	smalldatetime
	
	select @hoy = getdate()

	/*
		******************************************************************************************************************
		Veo si existe dado de alta un per�odo de planificaci�n que est� vigente.
		******************************************************************************************************************
	*/

	if not exists (
		select p.per_nrointerno 
		from Periodo p 
		where CONVERT(CHAR(8),@hoy,112) >= CONVERT(CHAR(8),p.fe_desde,112) and CONVERT(CHAR(8),@hoy,112) <= CONVERT(CHAR(8),p.fe_hasta,112))
		-- No existe, debe ser dado de alta
		
		BEGIN
			create table #mesNombre (
				mes_nro		int,
				mes_nombre	varchar(60))

			INSERT INTO #mesNombre VALUES (1, 'Enero')
			INSERT INTO #mesNombre VALUES (2, 'Febrero')
			INSERT INTO #mesNombre VALUES (3, 'Marzo')
			INSERT INTO #mesNombre VALUES (4, 'Abril')
			INSERT INTO #mesNombre VALUES (5, 'Mayo')
			INSERT INTO #mesNombre VALUES (6, 'Junio')
			INSERT INTO #mesNombre VALUES (7, 'Julio')
			INSERT INTO #mesNombre VALUES (8, 'Agosto')
			INSERT INTO #mesNombre VALUES (9, 'Septiembre')
			INSERT INTO #mesNombre VALUES (10, 'Octubre')
			INSERT INTO #mesNombre VALUES (11, 'Noviembre')
			INSERT INTO #mesNombre VALUES (12, 'Diciembre')

			/*
				Obtengo el �ltimo per�odo de planificaci�n para cerrarlo. Esto evita que se puedan 
				agregar nuevas peticiones al per�odo cerrado.
			*/
			select @cierre_periodo = p.per_nrointerno
			from Periodo p
			where p.fe_hasta IN (
				select MAX(fe_hasta)
				from Periodo)

			update Periodo
			set per_estado = 'PLAN40'
			where per_nrointerno = @cierre_periodo

			/*
				PLN001: Fecha a partir de la cual se tomar�n en cuenta peticiones para planificar.
				PLN002: Frecuencia de generaci�n de planificaci�n.
				PLN003: Fecha de inicio del pr�ximo per�odo de planificaci�n.
			*/

			select @per_estado = 'INI'
			
			select @frecuencia = v.var_texto
			from Varios v
			where var_codigo = 'PLN002'
			
			-- Inicio forzado en fecha
			select @inicio = v.var_fecha
			from Varios v
			where v.var_codigo = 'PLN003'

			-- Busco la �ltima fecha de planificaci�n
			if @inicio is null
				select @inicio = dateadd(dd, 1, MAX(p.fe_hasta))
				from Periodo p

			select @per_nrointerno_nuevo = MAX(p.per_nrointerno) + 1
			from Periodo p

			select @per_fedesde = @inicio
			select @inicio_proceso_aprobacion = @inicio

			select @per_fehasta = case
				when @frecuencia = 'Z' then dateadd(dd, 7, @inicio)
				when @frecuencia = 'Q' then dateadd(dd, 15, @inicio)
				when @frecuencia = 'M' then dateadd(mm, 1, @inicio)
				when @frecuencia = 'B' then dateadd(mm, 2, @inicio)
				when @frecuencia = 'T' then dateadd(mm, 3, @inicio)
				when @frecuencia = 'C' then dateadd(mm, 4, @inicio)
				when @frecuencia = 'S' then dateadd(mm, 6, @inicio)
				when @frecuencia = 'A' then dateadd(yy, 1, @inicio)
				--when @frecuencia = 'X' then dateadd(dd, 7, @inicio)			-- TODO: revisar esto.
			end

			select @per_fehasta = dateadd(dd, -1, @per_fehasta)
			select @mes_nombre = #mesNombre.mes_nombre
			from #mesNombre where mes_nro = MONTH(@per_fehasta)

			select @per_nombre = case
				when @frecuencia = 'Z' then 'Plan. semanal - Semana: ' + convert(char(2),datepart(wk, @per_fehasta))
				when @frecuencia = 'Q' then 'Plan. quincenal '
				when @frecuencia = 'M' then 'Plan. mensual - ' + convert(char(4),YEAR(@per_fehasta)) + ', ' + LTRIM(RTRIM(@mes_nombre))	--datename(month, @per_fehasta)
				when @frecuencia = 'B' then 'Plan. bimestral '
				when @frecuencia = 'T' then 'Plan. trimestral '
				when @frecuencia = 'C' then 'Plan. cuatrimestral '
				when @frecuencia = 'S' then 'Plan. semestral '
				when @frecuencia = 'A' then 'Plan. anual ' + convert(char(4),YEAR(@per_fehasta))
				when @frecuencia = 'X' then 'Plan. definido por el usuario'
			end

			select @per_abrev = case
				when @frecuencia = 'Z' then CONVERT(CHAR(4),YEAR(@per_fedesde))
				when @frecuencia = 'Q' then CONVERT(CHAR(4),YEAR(@per_fedesde))
				when @frecuencia = 'M' then CONVERT(CHAR(4),YEAR(@per_fedesde)) + '-' + CONVERT(CHAR(2), (
				case 
					when MONTH(@per_fedesde) < 10 then '0' + CONVERT(CHAR(1),MONTH(@per_fedesde)) 
					else CONVERT(CHAR(2),MONTH(@per_fedesde)) 
				END))
				when @frecuencia = 'B' then CONVERT(CHAR(4),YEAR(@per_fedesde))
				when @frecuencia = 'T' then CONVERT(CHAR(4),YEAR(@per_fedesde))
				when @frecuencia = 'C' then CONVERT(CHAR(4),YEAR(@per_fedesde))
				when @frecuencia = 'S' then CONVERT(CHAR(4),YEAR(@per_fedesde))
				when @frecuencia = 'A' then CONVERT(CHAR(4),YEAR(@per_fedesde))
				when @frecuencia = 'X' then CONVERT(CHAR(4),YEAR(@per_fedesde))
			end
			
			INSERT INTO Periodo  
			VALUES (@per_nrointerno_nuevo, @per_nombre, @frecuencia, @per_fedesde, @per_fehasta, 'PLAN10', getdate(), suser_name(), @per_abrev)

			-- Actualizo la fecha del siguiente inicio de per�odo de planificaci�n
			UPDATE Varios
			SET var_fecha = dateadd(dd, 1, @per_fehasta)
			WHERE var_codigo = 'PLN003' 

			select @per_estado = 'OK'

			drop table #mesNombre
		END
	else
		select 
			@per_nrointerno_nuevo = p.per_nrointerno,
			@per_estado = 'OK'
		from Periodo p where @hoy >= p.fe_desde and @hoy <= p.fe_hasta
	
	/*
		******************************************************************************************************************
		Agrega al per�odo en cuesti�n las peticiones que se encuentran en condiciones de ser planificadas
		******************************************************************************************************************
	*/
	if @per_estado = 'OK'
		/*
			Primero, quito todas las peticiones de periodos anteriores que no fueron analizadas ni comprometidas (horas y ambas fechas).
			Estas peticiones, pasan autom�ticamente al nuevo per�odo con los datos que ten�an, para ser priorizadas nuevamente.
			
			Segundo, busco todas las peticiones que cumplan con el criterio establecido por BPE para priorizar.

			Criterio:
				- Peticiones de tipo Normales, Especiales y Proyectos
				- En estado "Al referente de RGyP" o "Al referente de Sistema".
				- De clase EVOLutivos y NUEVos desarrollos
				- Apalancados
				- No regulatorios
				- Asociadas a proyectos de Engineering o Estrat�gicos o
				- Asociadas a otros proyectos
				- No vinculadas a periodos de planificaci�n anterior.
		*/
		begin 
			select @per_abrev = p.per_abrev
			from Periodo p
			where p.per_nrointerno = @per_nrointerno_nuevo

			-- Identifico los agrupamientos SDA
			declare @secuencia		int
			declare @ret_status     int
			declare	@agr_SDA		int

			create table #AgrupXPetic(
				pet_nrointerno 	int)

			create table #AgrupArbol(
				secuencia		int,
				nivel 			int,
				agr_nrointerno 	int)
			
			create table #AgrupXPeticSDA (
				pet_nrointerno		int,
				agr_nrointerno		int) 
			
			SELECT @agr_SDA = var_numero
			FROM Varios
			WHERE var_codigo = 'AGRP_SDA'

			if @agr_SDA<>0
				begin  
					select @secuencia=0
					execute @ret_status = sp_GetAgrupArbol @agr_SDA, 0, @secuencia OUTPUT  
					if (@ret_status <> 0)
						begin  
							if (@ret_status = 30003)  
							begin
								select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
								return (@ret_status)
							end  
						end
					insert into #AgrupXPeticSDA (pet_nrointerno, agr_nrointerno)
						select AP.pet_nrointerno, AP.agr_nrointerno  
						from #AgrupArbol AA, AgrupPetic AP, Agrup AG
						where 
							AA.agr_nrointerno=AP.agr_nrointerno and  
							AG.agr_nrointerno=AP.agr_nrointerno and 
							AG.agr_vigente='S'
				end
			
			/*  Borramos aquellas que no han evolucionado para que sean incorporadas en los periodos subsiguientes.

				La condici�n es la siguiente:

				Aquellas peticiones que no tienen ni horas estimadas (analisis) ni fechas planificadas (compromiso), deben volver a competir,
				en el nuevo periodo dado de alta, con el resto de las peticiones. Para esto, se buscan aquellas peticiones que no tienen analisis
				ni compromiso, y se eliminan de la planificaci�n, volviendo al estado "Al referente de Sistemas". Los grupos que hubieran sido 
				vinculados, independientemente de sus datos (horas y fechas), conservan su estado y datos.
			*/
			declare CursPetplan cursor for
				select 
					pp.pet_nrointerno,
					p.pet_nroasignado,
					p.cod_estado
				from 
					PeticionPlancab pp inner join
					Peticion p on (p.pet_nrointerno = pp.pet_nrointerno)
				where 
					pp.per_nrointerno <> @per_nrointerno_nuevo and 
					pp.pet_nrointerno IN (
						select p.pet_nrointerno 
						from Peticion p
						where 
							charindex(p.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')=0 and
							(p.horaspresup = 0 OR (p.fe_ini_plan is null OR p.fe_fin_plan is null)))
			for read only

			open CursPetplan fetch CursPetplan into @c_pet_nrointerno, @c_pet_nroasignado, @c_cod_estado
				while (@@sqlstatus = 0) 
					begin
						select @mensaje = 'La petici�n ' + LTRIM(RTRIM(CONVERT(VARCHAR(12),@c_pet_nroasignado))) + 
										' ha sido quitada de la planificaci�n del per�odo ' + LTRIM(RTRIM(@per_abrev)) + 
										' por no haber sido analizada y/o comprometida. Pasa a disposici�n del per�odo siguiente.'
						exec sp_AddHistorial @hst_nrointerno OUTPUT, @c_pet_nrointerno, 'PDELPRI0', @c_cod_estado, null,null,null,null,null,null
						exec sp_AddHistorialMemo @hst_nrointerno, 0, @mensaje

						fetch CursPetplan into @c_pet_nrointerno, @c_pet_nroasignado, @c_cod_estado
					end
			close CursPetplan
			deallocate cursor CursPetplan

			INSERT INTO PeticionPlancab (per_nrointerno, pet_nrointerno, prioridad, cod_bpe, plan_actfch, plan_actusr, plan_origen) 
			select @per_nrointerno_nuevo, pet_nrointerno, null, null, getdate(), suser_name(), 3
			from PeticionPlancab
			where 
				per_nrointerno <> @per_nrointerno_nuevo and 
				pet_nrointerno IN (
					select p.pet_nrointerno 
					from Peticion p
					where 
						charindex(p.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')=0 and
						(p.horaspresup = 0 OR (p.fe_ini_plan is null OR p.fe_fin_plan is null)))

			DELETE
			FROM PeticionPlancab 
			WHERE 
				per_nrointerno <> @per_nrointerno_nuevo and
				pet_nrointerno IN (
					select p.pet_nrointerno 
					from Peticion p
					where 
						charindex(p.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')=0 and 			-- No contemplar peticiones en estado terminal 
						(p.horaspresup = 0 OR 																	-- Peticiones con horas estimadas
						(p.fe_ini_plan is null OR p.fe_fin_plan is null)))										-- Peticiones con fechas planificadas (ambas)
			
			-- Peticiones dadas de alta en el nuevo periodo a contemplar
			INSERT INTO PeticionPlancab (per_nrointerno, pet_nrointerno, prioridad, cod_bpe, plan_actfch, plan_actusr, plan_origen)  
			select @per_nrointerno_nuevo, p.pet_nrointerno, null, null, getdate(), suser_name(), 1
			from Peticion p
			where 
				p.cod_tipo_peticion in ('NOR','ESP','PRJ') and 
				p.cod_clase in ('NUEV','EVOL') and 
				p.pet_regulatorio = 'N' and 
				p.cod_estado IN ('REFBPE','COMITE') and															-- Se incluyen en la planificaci�n aquellas peticiones que han sido completadas por los referentes de RGyP y designado Ref. de sistemas.
				(p.cod_bpar is not null and LTRIM(RTRIM(p.cod_bpar)) <> '') and 
				((p.valor_impacto <> 0 OR p.valor_facilidad <> 0) OR 
				 (p.pet_projid IN (
					select px.ProjId
					from ProyectoIDM px 
					where 
						p.pet_projid = px.ProjId and p.pet_projsubid = px.ProjSubId and p.pet_projsubsid = px.ProjSubSId)))
				-- Esto evita que una misma petici�n coexista en diferentes per�odos de planificaci�n
				and p.pet_nrointerno NOT IN (
					select x.pet_nrointerno
					from PeticionPlancab x) 
				-- Evita que peticiones categorizadas como SDA sean consideradas
				and p.pet_nrointerno NOT IN (
					select sda.pet_nrointerno
					from #AgrupXPeticSDA sda 
					group by sda.pet_nrointerno)
			
			declare CursPetplan cursor for
				select 
					pp.pet_nrointerno,
					p.pet_nroasignado,
					p.cod_estado
				from 
					PeticionPlancab pp inner join
					Peticion p on (p.pet_nrointerno = pp.pet_nrointerno)
				where
					pp.per_nrointerno = @per_nrointerno_nuevo and 
					pp.pet_nrointerno NOT IN (
						select h.pet_nrointerno
						from Historial h 
						where h.cod_evento = 'PCHGPRI0'
						group by h.pet_nrointerno)
			for read only

			open CursPetplan fetch CursPetplan into @c_pet_nrointerno, @c_pet_nroasignado, @c_cod_estado
				while (@@sqlstatus = 0) 
					begin
						select @mensaje = 'La petici�n ' + LTRIM(RTRIM(CONVERT(VARCHAR(12),@c_pet_nroasignado))) + 
										' ha sido incluida autom�ticamente en el per�odo de planificaci�n ' + LTRIM(RTRIM(@per_abrev)) + '.'

						exec sp_AddHistorial @hst_nrointerno OUTPUT, @c_pet_nrointerno, 'PCHGPRI0', @c_cod_estado, null,null,null,null,null,null
						exec sp_AddHistorialMemo @hst_nrointerno, 0, @mensaje

						update PeticionPlancab set hst_nrointerno = @hst_nrointerno where pet_nrointerno = @c_pet_nrointerno

						fetch CursPetplan into @c_pet_nrointerno, @c_pet_nroasignado, @c_cod_estado
					end
			close CursPetplan
			deallocate cursor CursPetplan

			return(0)
		end
go

grant execute on dbo.sp_GeneracionPlanificacion1 to GesPetUsr
go

print 'Actualizaci�n realizada.'
go

/*
-- APROBACION AUTOMATICA
-- Este proceso se debe ejecutar luego de una cantidad N de d�as pasados el alta del per�odo.
-- Se "aprueban" autom�ticamente todas las peticiones con n�mero de prioridad, pasando del estado
-- "Al ref. de RGyP" al estado "Al ref. de sistema".

declare @dias_aprobacion	int

select @dias_aprobacion = v.var_numero
from Varios v
where var_codigo = 'PLN004'

WHILE (exists (select fecha from Feriado where fecha = @inicio_proceso_aprobacion)) 
	BEGIN
		select @inicio_proceso_aprobacion = dateadd(dd, 1, @inicio_proceso_aprobacion)
	END

-- Ahora, busco todas las peticiones del per�odo actual que tengan definida una 
-- prioridad para cambiar el estado "Al Ref. de sistemas".
declare CursPetplan cursor for
	select 
		pp.pet_nrointerno,
		p.pet_nroasignado,
		p.cod_estado
	from 
		PeticionPlancab pp inner join
		Peticion p on (p.pet_nrointerno = pp.pet_nrointerno)
	where
		pp.per_nrointerno = @per_nrointerno_nuevo and
		(pp.prioridad is not null and pp.prioridad > 0)
for read only

open CursPetplan fetch CursPetplan into @c_pet_nrointerno, @c_pet_nroasignado, @c_cod_estado
	while (@@sqlstatus = 0) 
		begin
			-- Cambio de estado "Al ref. de RGyP" a "Al ref. de sistemas"
			UPDATE Peticion
			SET cod_estado = 'COMITE', fe_estado = getdate()
			WHERE pet_nrointerno = @c_pet_nrointerno

			select @mensaje = 'La petici�n ' + LTRIM(RTRIM(CONVERT(VARCHAR(12),@c_pet_nroasignado))) + ' fu� pasada al Referente de Sistemas autom�ticamente por el proceso de planificaci�n ' + LTRIM(RTRIM(@per_abrev)) + '.'
			exec sp_AddHistorial @hst_nrointerno OUTPUT, @c_pet_nrointerno, 'PCHGEST', 'COMITE', null,null,null,null,null,null
			exec sp_AddHistorialMemo @hst_nrointerno, 0, @mensaje

			exec sp_DoMensaje 'PET','PCHGEST',@c_pet_nrointerno, 'COMITE', 'NULL', null, null

			fetch CursPetplan into @c_pet_nrointerno, @c_pet_nroasignado, @c_cod_estado
		end
close CursPetplan
deallocate cursor CursPetplan
*/