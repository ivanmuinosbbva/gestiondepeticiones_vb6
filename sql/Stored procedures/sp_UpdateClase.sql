/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.10.2014 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateClase'
go

if exists (select * from sysobjects where name = 'sp_UpdateClase' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateClase
go

create procedure dbo.sp_UpdateClase
	@cod_clase	char(4),
	@nom_clase	char(40)
as
	update 
		GesPet.dbo.Clase
	set
		nom_clase = @nom_clase
	where 
		cod_clase = @cod_clase
go

grant execute on dbo.sp_UpdateClase to GesPetUsr
go

print 'Actualización realizada.'
go
