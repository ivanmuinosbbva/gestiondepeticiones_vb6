/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 11.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMMemo'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMMemo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMMemo
go

create procedure dbo.sp_GetProyectoIDMMemo
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int,
	@mem_campo		char(10)=null,
	@mem_secuencia	smallint=null
as
	select 
		m.ProjId,
		m.ProjSubId,
		m.ProjSubSId,
		m.mem_campo,
		m.mem_secuencia,
		m.mem_texto
	from 
		GesPet.dbo.ProyectoIDMMemo m
	where
		m.ProjId = @ProjId and
		m.ProjSubId = @ProjSubId and
		m.ProjSubSId = @ProjSubSId and
		(@mem_campo is null or m.mem_campo = @mem_campo) and
		(@mem_secuencia is null or m.mem_secuencia = @mem_secuencia)
	order by
		m.ProjId,
		m.ProjSubId,
		m.ProjSubSId,
		m.mem_campo,
		m.mem_secuencia
	return(0)
go

grant execute on dbo.sp_GetProyectoIDMMemo to GesPetUsr
go

print 'Actualización realizada.'
go