use GesPet
go
print 'sp_InsEscalamiento'
go
if exists (select * from sysobjects where name = 'sp_InsEscalamiento' and sysstat & 7 = 4)
drop procedure dbo.sp_InsEscalamiento
go
create procedure dbo.sp_InsEscalamiento 
    @evt_alcance char(3), 
    @cod_estado char(6), 
    @pzo_desde  int, 
    @pzo_hasta  int, 
    @cod_accion char(8), 
    @fec_activo smalldatetime 
as 
 
if exists (select 1 from GesPet..Escalamientos where 
        RTRIM(evt_alcance)=RTRIM(@evt_alcance) and 
        RTRIM(cod_estado)=RTRIM(@cod_estado) and 
        pzo_desde=@pzo_desde and 
        pzo_hasta=@pzo_hasta and 
        RTRIM(cod_accion)=RTRIM(@cod_accion))   
begin   
     select 30012 as ErrCode ,'Evento/Mensaje Existente' as ErrDesc   
    raiserror 30012 'Evento/Mensaje Existente'   
     return (30012)   
end 
else 
begin 
    insert into GesPet..Escalamientos  
        (evt_alcance, 
        cod_estado, 
        pzo_desde, 
        pzo_hasta, 
        cod_accion, 
        fec_activo) 
    values 
        (@evt_alcance, 
        @cod_estado, 
        @pzo_desde, 
        @pzo_hasta, 
        @cod_accion, 
        @fec_activo) 
end 
return(0) 
go



grant execute on dbo.sp_InsEscalamiento to GesPetUsr 
go


