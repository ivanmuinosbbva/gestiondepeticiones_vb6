/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetInfoproc'
go 

if exists (select * from sysobjects where name = 'sp_GetInfoproc' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetInfoproc
go

create procedure dbo.sp_GetInfoproc
	@infoprot_id		int=null,
	@infoprot_estado	char(1)=null
as
	select 
		p.infoprot_id,
		p.infoprot_nom,
		p.infoprot_tipo,
		tipo_dsc = (case
			when p.infoprot_tipo = 'INI' then 'Inicial'
			when p.infoprot_tipo = 'SEG' then 'Seguimiento'
			when p.infoprot_tipo = 'PPF' then 'Preliminar/Parcial/Final'
		end),
		p.infoprot_punto,
		p.infoprot_estado,
		p.infoprot_vermaj, 
		p.infoprot_vermin
	from 
		GesPet.dbo.Infoproc p
	where
		(p.infoprot_id = @infoprot_id or @infoprot_id is null) and
		(p.infoprot_estado = @infoprot_estado or @infoprot_estado is null)
go

grant execute on dbo.sp_GetInfoproc to GesPetUsr
go

print 'Actualización realizada.'
go
