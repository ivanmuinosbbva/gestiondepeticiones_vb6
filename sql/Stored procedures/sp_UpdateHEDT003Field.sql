/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 02.10.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateHEDT003Field'
go

if exists (select * from sysobjects where name = 'sp_UpdateHEDT003Field' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateHEDT003Field
go

create procedure dbo.sp_UpdateHEDT003Field
	@dsn_id	char(8),
	@cpy_id	char(8),
	@cpo_id	char(8),
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as

if rtrim(@campo)='DSN_ID'
	update 
		GesPet.dbo.HEDT003
	set
		dsn_id = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null) and
		(cpo_id = @cpo_id or @cpo_id is null)

if rtrim(@campo)='CPY_ID'
	update 
		GesPet.dbo.HEDT003
	set
		cpy_id = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null) and
		(cpo_id = @cpo_id or @cpo_id is null)

if rtrim(@campo)='CPO_ID'
	update 
		GesPet.dbo.HEDT003
	set
		cpo_id = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null) and
		(cpo_id = @cpo_id or @cpo_id is null)

if rtrim(@campo)='CPO_NROBYTE'
	update 
		GesPet.dbo.HEDT003
	set
		cpo_nrobyte = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null) and
		(cpo_id = @cpo_id or @cpo_id is null)

if rtrim(@campo)='CPO_CANBYTE'
	update 
		GesPet.dbo.HEDT003
	set
		cpo_canbyte = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null) and
		(cpo_id = @cpo_id or @cpo_id is null)

if rtrim(@campo)='CPO_TYPE'
	update 
		GesPet.dbo.HEDT003
	set
		cpo_type = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null) and
		(cpo_id = @cpo_id or @cpo_id is null)

if rtrim(@campo)='CPO_DECIMALS'
	update 
		GesPet.dbo.HEDT003
	set
		cpo_decimals = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null) and
		(cpo_id = @cpo_id or @cpo_id is null)

if rtrim(@campo)='CPO_DSC'
	update 
		GesPet.dbo.HEDT003
	set
		cpo_dsc = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null) and
		(cpo_id = @cpo_id or @cpo_id is null)

if rtrim(@campo)='CPO_NOMRUT'
	update 
		GesPet.dbo.HEDT003
	set
		cpo_nomrut = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null) and
		(cpo_id = @cpo_id or @cpo_id is null)

if rtrim(@campo)='CPO_FEULT'
	update 
		GesPet.dbo.HEDT003
	set
		cpo_feult = @valordate
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null) and
		(cpo_id = @cpo_id or @cpo_id is null)

if rtrim(@campo)='CPO_USERID'
	update 
		GesPet.dbo.HEDT003
	set
		cpo_userid = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null) and
		(cpo_id = @cpo_id or @cpo_id is null)
if rtrim(@campo)='ESTADO'
	update 
		GesPet.dbo.HEDT003
	set
		estado = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null) and
		(cpo_id = @cpo_id or @cpo_id is null)
go

grant execute on dbo.sp_UpdateHEDT003Field to GesPetUsr
go

print 'Actualización realizada.'
