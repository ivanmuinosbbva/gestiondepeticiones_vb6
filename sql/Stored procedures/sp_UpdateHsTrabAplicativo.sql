/*
-000- a. FJS 19.05.2009 - Nuevo SP para manejo de carga de horas desglosado por Aplicativo
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateHsTrabAplicativo'
go

if exists (select * from sysobjects where name = 'sp_UpdateHsTrabAplicativo' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateHsTrabAplicativo
go

create procedure dbo.sp_UpdateHsTrabAplicativo
	@cod_recurso	char(10)=null,
	@cod_tarea		char(8)=null,
	@pet_nrointerno	int=null,
	@fe_desde		smalldatetime,
	@fe_hasta		smalldatetime,
	@horas			smallint=null,
	@app_id			char(30)=null,
	@app_horas		smallint=null,
	@hsapp_texto	varchar(255)
as
	update 
		GesPet..HorasTrabajadasAplicativo
	set
		app_horas		= @app_horas,
		hsapp_texto		= @hsapp_texto
	where 
		cod_recurso = @cod_recurso and
		cod_tarea = @cod_tarea and
		pet_nrointerno = @pet_nrointerno and
		fe_desde = @fe_desde and
		fe_hasta = @fe_hasta and 
		horas = @horas and
		app_id = @app_id
go

grant execute on dbo.sp_UpdateHsTrabAplicativo to GesPetUsr
go

print 'Actualización realizada.'
