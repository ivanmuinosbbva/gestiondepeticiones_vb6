/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteIndicadorKPI'
go

if exists (select * from sysobjects where name = 'sp_DeleteIndicadorKPI' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteIndicadorKPI
go

create procedure dbo.sp_DeleteIndicadorKPI
	@kpiid	int=0
as
	delete from
		GesPet.dbo.IndicadorKPI
	where
		kpiid = @kpiid
go

grant execute on dbo.sp_DeleteIndicadorKPI to GesPetUsr
go

print 'Actualización realizada.'
go
