/*
-000- a. FJS 26.06.2009 - Nuevo SP para el manejo de autorizaciones a solicitudes a Carga de M�quina
-001- a. FJS 12.01.2010 - Se modifica para permitir los restore de backup.
-002- a. FJS 20.01.2010 - Nuevo: Solicitudes de ejecuci�n en diferido.
-003- a. FJS 06.07.2010 - Se actualiza el nombre descriptivo del estado E porque trae confusi�n al usuario.
-004- a. FJS 13.11.2013 - Nuevo: se filtran las solicitudes con enmascaramiento para evitar listarselas al supervisor (ya que no tiene sentido que las vea).
-005- a. FJS 08.07.2014 - Nuevo: ahora, las solicitudes sin enmascaramiento deben ser preaprobadas por el l�der, luego por el supervisor.
-005- b. FJS 14.07.2014 - Nuevo: solicitudes de EMErgencia.
-006- a. FJS 29.07.2014 - Nuevo: se agrega la tabla con las descripciones de los tipos de solicitud.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetSolicitudAuto'
go

if exists (select * from sysobjects where name = 'sp_GetSolicitudAuto' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetSolicitudAuto
go

create procedure dbo.sp_GetSolicitudAuto
	@cod_recurso	char(10),
	@cod_perfil		char(4)=null,
	@fe_desde		smalldatetime=null,
	--@propias		char(1)='S',
	@pendientes		char(1)='P'
as
	declare @cod_sector		char(8)
	declare @cod_grupo		char(8)

	-- Determino la estructura del recurso actuante (para saber que tengo que buscar para autorizar)
	select 
		@cod_sector = x.cod_sector,
		@cod_grupo = x.cod_grupo
	from
		GesPet..Recurso x
	where
		x.cod_recurso = rtrim(@cod_recurso)
	
	-- Resultado de la consulta
	select 
		a.sol_nroasignado,
		a.sol_eme,									-- add -005- b.
		a.sol_tipo as sol_tipocod,																		-- upd -006- a.
		sol_tipo = (select x.nom_tipo from GesPet..TipoSolicitud x where x.cod_tipo = a.sol_tipo),		-- add -006- a.
		/* { del -006- a.
		sol_tipo = 
		case
			when a.sol_tipo = '1' then 'XCOM'
			when a.sol_tipo = '2' then 'UNLO'
			when a.sol_tipo = '3' then 'TCOR'
			when a.sol_tipo = '4' then 'SORT'
			when a.sol_tipo = '5' then 'XCOM*'
			when a.sol_tipo = '6' then 'SORT*'
		end,
		} */
		a.sol_mask,
		a.sol_fecha,
		a.sol_recurso,
		nom_recurso = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_recurso),
		a.pet_nrointerno,
		pet_nroasignado = (select x.pet_nroasignado from GesPet..Peticion x where x.pet_nrointerno = a.pet_nrointerno),
		pet_titulo = (select x.titulo from GesPet..Peticion x where x.pet_nrointerno = a.pet_nrointerno),
		a.sol_resp_ejec,
		nom_resp_ejec = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_resp_ejec),
		--{ add -005- b.
		a.sol_resp_sect,
		nom_resp_sect = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_resp_sect),
		--}
		a.sol_texto,
		jus_desc = (select x.jus_descripcion from GesPet..Justificativos x where x.jus_codigo = a.jus_codigo),
		file_prod = case
			--when a.sol_tipo in ('1','5','4','6') then a.sol_file_prod 
			when a.sol_tipo = '2' then '-'
			when a.sol_tipo = '3' then a.sol_nrotabla
			else
				a.sol_file_prod
		end,
		a.sol_estado, 
		nom_estado = (select x.nom_estado from GesPet..EstadoSolicitud x where x.cod_estado = a.sol_estado),
		/* { del -005- a.
		nom_estado = 
		case
			when a.sol_estado = 'A' then 'Pendiente de aprobaci�n'
			when a.sol_estado = 'C' then 'Aprobado por l�der'
			when a.sol_estado = 'D' then 'Aprobado y en proceso de envio'
			when a.sol_estado = 'E' then 'Enviado'								-- add -003- a.
			when a.sol_estado = 'F' then 'En espera de Restore'					-- add -001- a.
			when a.sol_estado = 'G' then 'En espera (ejecuci�n diferida)'		-- add -002- a.
		end,
		} */
		--{ add -005- a.
		a.sol_fe_auto1,
		a.sol_fe_auto2,
		a.sol_fe_auto3,
		--}
		b.cod_grupo,
		b.cod_sector
	from 
		GesPet..Solicitudes a left join 
		GesPet..Recurso b on a.sol_recurso = b.cod_recurso
	where
		a.sol_mask = 'N' and 
		(@fe_desde is null or convert(char(10),a.sol_fecha,112) >= convert(char(10),@fe_desde,112)) and (
			(@cod_perfil = 'CGRU' and @cod_recurso <> a.sol_recurso and 
			(a.sol_eme = 'N' and (@pendientes = 'T' and @cod_sector = b.cod_sector and @cod_grupo = b.cod_grupo) or (@cod_sector = b.cod_sector and @cod_grupo = b.cod_grupo and a.sol_estado = 'A'))) 
			or
			(@cod_perfil = 'CSEC' and @cod_recurso <> a.sol_recurso and 
			((@pendientes = 'P' and a.sol_estado='B' and @cod_sector = b.cod_sector) or (@pendientes = 'Q' and charindex(a.sol_estado,'A|B|')>0 and @cod_sector = b.cod_sector) or @pendientes = 'T')
			) 
		)

go

grant execute on dbo.sp_GetSolicitudAuto to GesPetUsr
go

print 'Actualizaci�n realizada.'
go


/*
a.sol_mask = 'N' and 
		(@fe_desde is null or convert(char(10),a.sol_fecha,112) >= convert(char(10),@fe_desde,112)) and (
			(@cod_perfil = 'CGRU' and @cod_recurso <> a.sol_recurso and 
				((@propias = 'S' and  @cod_grupo = b.cod_grupo) or @propias = 'N') and 
				((@pendientes = 'S' and a.sol_estado = 'A') or @pendientes = 'N')
			) 
			or
			(@cod_perfil = 'CSEC' and @cod_recurso <> a.sol_recurso and 
				((@propias = 'S' and @cod_sector = b.cod_sector) or @propias = 'N') and 
				((@pendientes = 'P' and a.sol_estado='B') or (@pendientes = 'Q' and charindex(a.sol_estado,'A|B|')>0) or @pendientes = 'T')
			) 
		)
*/



	/*
	or
		(@cod_perfil = 'ASEG' and ((a.sol_estado = 'B' and @pendientes = 'S') or @pendientes = 'N'))
	*/
		/*
		--(rtrim(@cod_perfil) = 'CGRU' and ((rtrim(@cod_grupo) = b.cod_grupo and @propias = 'S') or @propias = 'N') and ((sol_fe_auto1 is null and @pendientes = 'S') or (@pendientes = 'N'))) or
		(rtrim(@cod_perfil) = 'CGRU' and ((rtrim(@cod_grupo) = b.cod_grupo and @propias = 'S') or @propias = 'N') and ((a.sol_estado = 'A' and @pendientes = 'S') or (@pendientes = 'N'))) or
		--(rtrim(@cod_perfil) = 'CSEC' and ((rtrim(@cod_sector) = b.cod_sector and @propias = 'S') or @propias = 'N') and ((sol_fe_auto2 is null and @pendientes = 'S') or (@pendientes = 'N')))
		(rtrim(@cod_perfil) = 'CSEC' and ((rtrim(@cod_sector) = b.cod_sector and @propias = 'S') or @propias = 'N') and ((a.sol_estado is null and @pendientes = 'S') or (@pendientes = 'N')))
		*/


