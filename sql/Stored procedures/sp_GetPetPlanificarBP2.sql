/*
-000- a. FJS 12.07.2010 - Nuevo SP para...
-001- a. FJS 12.10.2010 - Corrección: se cambia el criterio de cálculo de la columna [alca] (se cambia TEST por ALCA).
-002- a. FJS 15.11.2010 - Planificación DYD (2° etapa): se identifica las peticiones agregadas por DYD.
-003- a. FJS 06.12.2010 - Mejora: se agrega la descripción abreviada del período de planificación.
-004- a. FJS 04.03.2011 - Cambios: desde la bandeja de trabajo de planificación para BP, estos deben poder ver también lo planificado por los otros BPs (a pedido de Icaro).
-005- a. FJS 04.04.2011 - Cambios: se agrega el orden puesto por Icaro.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPetPlanificarBP2'
go

if exists (select * from sysobjects where name = 'sp_GetPetPlanificarBP2' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPetPlanificarBP2
go

create procedure dbo.sp_GetPetPlanificarBP2
	@cod_bpar	char(8)=null,
	@cod_grupo	char(8)=null,
	@per_nrointerno	int=null,
	@prioridad	char(1)=null
as 
	select 
		1 as 'orden',
		isnull(p.per_nrointerno,0) as per_nrointerno,
		a.pet_nroasignado,
		a.pet_nrointerno,
		a.cod_tipo_peticion,
		a.cod_clase,
		a.pet_regulatorio,
		isnull(a.pet_projid,0) as pet_projid,
		isnull(a.pet_projsubid,0) as pet_projsubid,
		isnull(a.pet_projsubsid,0) as pet_projsubsid,
		a.titulo,
		pc.prioridad,
		a.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..Estados x where x.cod_estado = a.cod_estado),
		a.cod_bpar,
		nom_bpar = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.cod_bpar),
		a.cod_sector,
		nom_sector    = (select RTRIM(x.nom_sector) from GesPet..Sector x where x.cod_sector = a.cod_sector),
		nom_gerencia  = (select RTRIM(x.nom_gerencia) from GesPet..Gerencia x where x.cod_gerencia = a.cod_gerencia),
		nom_direccion = (select RTRIM(x.nom_direccion) from GesPet..Direccion x where x.cod_direccion = a.cod_direccion),
		'alca' = (
		case
			when (select count(*) from GesPet..PeticionAdjunto y where y.pet_nrointerno = a.pet_nrointerno and y.adj_tipo in ('C100','P950')) > 0 and 
				 (select count(*) from GesPet..PeticionConf x where x.pet_nrointerno = a.pet_nrointerno and x.ok_tipo = 'ALCA') > 0 then 'U'
			when (select count(*) from GesPet..PeticionAdjunto y where y.pet_nrointerno = a.pet_nrointerno and y.adj_tipo in ('C100','P950')) > 0 and 
				 (select count(*) from GesPet..PeticionConf x where x.pet_nrointerno = a.pet_nrointerno and x.ok_tipo = 'ALCA') = 0 then 'S'
			else 'N'
		end),
		b.cod_grupo,
		g.nom_grupo,
		resp_grupo = (select x.nom_recurso from GesPet..Recurso x where x.cod_grupo = b.cod_grupo and x.cod_sector = b.cod_sector and x.flg_cargoarea = 'S'),
		b.cod_estado as 'grp_cod_estado',
		'grp_nom_estado' = (select x.nom_estado from GesPet..Estados x where x.cod_estado = b.cod_estado),
		b.horaspresup,
		p.plan_orden,
		p.plan_ordenfin,	-- add -005- a.
		p.plan_ori,
		nom_origen = (
			case
				when p.plan_ori = 1 then 'Planificación inicial'
				when p.plan_ori = 2 then 'Agregada'
				--{ add -002- a.
				when p.plan_ori = 3 then 'Planificación inicial (DYD)'
				when p.plan_ori = 4 then 'Agregada (DYD)'
				--}
				else '***'
			end),	-- Origen de planificación
		estado_orden = (
		case
			when (p.plan_orden is null and pc.prioridad is null) then 'Pendiente priorización'
			when (p.plan_orden is null and pc.prioridad is not null and pc.prioridad = '1') then 'Pendiente de orden'
			else '-'
		end),
		dyd_default_prio = case 
			when (
				select count(*)
				from
					GesPet..Peticion x inner join
					GesPet..PeticionGrupo y on (x.pet_nrointerno = y.pet_nrointerno) inner join
					GesPet..Grupo z on (y.cod_grupo = z.cod_grupo)
				where 
					x.pet_nrointerno = a.pet_nrointerno and
					y.cod_gerencia = 'DESA' and
					charindex(y.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
					(y.cod_estado = 'EJECUC' or (y.cod_estado = 'SUSPEN' and y.cod_motivo in (1,2,7,8))) and
					(z.grupo_homologacion = 'S')
			) >= 1 and pc.prioridad is null then '1'
			else ''
		end,
		dyd_default_orden = case 
			when (
				select count(*)
				from
					GesPet..Peticion x inner join
					GesPet..PeticionGrupo y on (x.pet_nrointerno = y.pet_nrointerno) inner join
					GesPet..Grupo z on (y.cod_grupo = z.cod_grupo)
				where 
					x.pet_nrointerno = a.pet_nrointerno and
					y.cod_gerencia = 'DESA' and
					charindex(y.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
					(y.cod_estado = 'EJECUC' or (y.cod_estado = 'SUSPEN' and y.cod_motivo in (1,2,7,8))) and
					(z.grupo_homologacion = 'S')
			) = 1 and p.plan_orden is null and pc.prioridad = '1' then '0'
			when (
				select count(*)
				from
					GesPet..Peticion x inner join
					GesPet..PeticionGrupo y on (x.pet_nrointerno = y.pet_nrointerno) inner join
					GesPet..Grupo z on (y.cod_grupo = z.cod_grupo)
				where 
					x.pet_nrointerno = a.pet_nrointerno and
					y.cod_gerencia = 'DESA' and
					charindex(y.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
					(y.cod_estado = 'EJECUC' or (y.cod_estado = 'SUSPEN' and y.cod_motivo in (1,2,7,8))) and
					(z.grupo_homologacion = 'S')
			) > 1 and p.plan_orden is null and pc.prioridad = '1' and (b.cod_estado = 'EJECUC' or (b.cod_estado = 'SUSPEN' and b.cod_motivo in (1,2,7,8))) then '0'
			/* when (
				select count(*)
				from
					GesPet..Peticion x inner join
					GesPet..PeticionGrupo y on (x.pet_nrointerno = y.pet_nrointerno) inner join
					GesPet..Grupo z on (y.cod_grupo = z.cod_grupo)
				where 
					x.pet_nrointerno = a.pet_nrointerno and
					y.cod_gerencia = 'DESA' and
					charindex(y.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
					(y.cod_estado = 'EJECUC' or (y.cod_estado = 'SUSPEN' and y.cod_motivo in (1,2,7,8))) and
					(z.grupo_homologacion = 'S')
			) > 1 and p.plan_orden is null and pc.prioridad = '1' and not (b.cod_estado = 'EJECUC' or (b.cod_estado = 'SUSPEN' and b.cod_motivo in (1,2,7,8))) then '*' */
			else ''
		end,
		'default_per_nrointerno' = 
		case
			when (p.per_nrointerno is null or p.per_nrointerno = 0) then pc.per_nrointerno
			else pc.per_nrointerno
		end,
		p.plan_estado,
		'plan_situacion' = '',
		'plan_situacion_dsc' = case
			when (p.plan_estado is null or p.plan_estado = '') then 'Pendiente'
			when p.plan_estado = 'PLANIF' then 'Ordenado'
			when p.plan_estado in ('PLANOK','APROBA') then 'Confirmado'
			else ''
		end,
		p.plan_actfch,
		p.plan_actusr,
		'plan_actusrnom' = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = p.plan_actusr),
		p.plan_act2fch,
		p.plan_act2usr,
		'plan_act2usrnom' = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = p.plan_act2usr),
		p.plan_estadoini,		-- Inicial: Estado de planificación
		nom_estadoini = (select x.nom_estado_plan from GesPet..EstadosPlanificacion x where x.cod_estado_plan = p.plan_estadoini),
		p.plan_motnoplan,		-- Inicial: Motivo de no planificación (no se puede planificar)
		nom_motnoplan = (select x.nom_motivo_noplan from GesPet..MotivosNoPlan x where x.cod_motivo_noplan = p.plan_motnoplan),
		p.plan_fealcadef,		-- Inicial: Fecha de definición de alcance
		p.plan_fefunctst,		-- Inicial: Fecha de fin de pruebas funcionales
		p.plan_horasper,		-- Inicial: Cantidad de horas planificadas para el presente período
		p.plan_desaestado,		-- Seguimiento: Estado actual del desarrollo
		nom_desaestado = (select x.nom_estado_desa from GesPet..EstadosDesarrollo x where x.cod_estado_desa = p.plan_desaestado),
		p.plan_motivsusp,		-- Seguimiento: Motivo de Susp./Desplan./Canc./Rech.
		nom_motivsusp = (select x.nom_motivo_suspen from GesPet..MotivosSuspen x where x.cod_motivo_suspen = p.plan_motivsusp),
		p.plan_motivrepl,		-- Replanificar: Motivo de replanificación
		nom_motivrepl = (select x.nom_motivo_replan from GesPet..MotivosReplan x where x.cod_motivo_replan = p.plan_motivrepl),
		p.plan_feplanori,		-- Replanificar: Fecha original de planificación
		p.plan_feplannue,		-- Replanificar: Nueva fecha de planificación
		p.plan_cantreplan,		-- Replanificar: Cantidad de replanificaciones (contador)
		p.plan_horasreplan,		-- Replanificar: Cantidad de horas totales replanificadas
		nom_motivo_replan = (select x.nom_motivo_replan from GesPet..MotivosReplan x where x.cod_motivo_replan = p.plan_motivrepl)
		,per_abrev = isnull((select x.per_abrev from GesPet..Periodo x where x.per_nrointerno = p.per_nrointerno),'-')	-- add -003- a.
	from 
		GesPet..Peticion a left join
		GesPet..PeticionGrupo b on (a.pet_nrointerno = b.pet_nrointerno) left join
		GesPet..Grupo g on (b.cod_grupo = g.cod_grupo) left join
		GesPet..PeticionPlancab pc on (pc.per_nrointerno = @per_nrointerno and pc.pet_nrointerno = a.pet_nrointerno) left join
		GesPet..PeticionPlandet p on (p.per_nrointerno = @per_nrointerno and p.cod_grupo = b.cod_grupo and p.pet_nrointerno = a.pet_nrointerno)
	where 
		(@cod_bpar is null or a.cod_bpar = @cod_bpar) and 
		(@cod_grupo is null or b.cod_grupo = @cod_grupo) and
		(@prioridad is null or pc.prioridad = @prioridad) and 
		(@per_nrointerno is null or p.per_nrointerno is null or p.per_nrointerno = @per_nrointerno) and		-- upd -003- a.
		--a.cod_clase in ('NUEV','EVOL') and 
		--{ add -002- a.
		(charindex(a.cod_clase, 'NUEV|EVOL|') > 0 or
		(charindex(a.cod_clase,'OPTI|')>0 and p.pet_nrointerno is not null)) and
		--}
		charindex(a.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
		(b.cod_gerencia = 'DESA') and
		(g.grupo_homologacion = 'S' and charindex(b.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0) and
		(charindex(b.cod_estado, 'ESTIOK|ESTIRK|ESTIMA|PLANOK|PLANIF|PLANRK|EJECRK|EJECUC|SUSPEN|') > 0) 
	union 
	select 
		2 as 'orden',
		isnull(p.per_nrointerno,0) as per_nrointerno,
		a.pet_nroasignado,
		a.pet_nrointerno,
		a.cod_tipo_peticion,
		a.cod_clase,
		a.pet_regulatorio,
		isnull(a.pet_projid,0) as pet_projid,
		isnull(a.pet_projsubid,0) as pet_projsubid,
		isnull(a.pet_projsubsid,0) as pet_projsubsid,
		a.titulo,
		p.prioridad,
		a.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..Estados x where x.cod_estado = a.cod_estado),
		a.cod_bpar,
		nom_bpar = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.cod_bpar),
		a.cod_sector,
		nom_sector    = (select RTRIM(x.nom_sector) from GesPet..Sector x where x.cod_sector = a.cod_sector),
		nom_gerencia  = (select RTRIM(x.nom_gerencia) from GesPet..Gerencia x where x.cod_gerencia = a.cod_gerencia),
		nom_direccion = (select RTRIM(x.nom_direccion) from GesPet..Direccion x where x.cod_direccion = a.cod_direccion),
		'alca' = (
		case
			when (select count(*) from GesPet..PeticionAdjunto y where y.pet_nrointerno = a.pet_nrointerno and y.adj_tipo in ('C100','P950')) > 0 and 
				 (select count(*) from GesPet..PeticionConf x where x.pet_nrointerno = a.pet_nrointerno and x.ok_tipo = 'TEST') > 0 then 'U'
			when (select count(*) from GesPet..PeticionAdjunto y where y.pet_nrointerno = a.pet_nrointerno and y.adj_tipo in ('C100','P950')) > 0 and 
				 (select count(*) from GesPet..PeticionConf x where x.pet_nrointerno = a.pet_nrointerno and x.ok_tipo = 'TEST') = 0 then 'S'
			else 'N'
		end),
		'' as 'cod_grupo',
		'' as 'nom_grupo',
		'' as 'resp_grupo',
		'' as 'grp_cod_estado',
		'' as 'grp_nom_estado',
		0 as 'horaspresup',
		0 as 'plan_orden',
		0 as 'plan_ordenfin',		-- add -005- a.
		0 as 'plan_ori',
		'***' as 'nom_origen',
		'Pendiente asig. DYD' as 'estado_orden',
		'' as 'dyd_default_prio',
		'' as 'dyd_default_orden',
		null as 'default_per_nrointerno',
		'',
		'plan_situacion' = '',
		'plan_situacion_dsc' = 'Sin grupo DYD activo',
		p.plan_actfch,
		p.plan_actusr,
		'plan_actusrnom'	= (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = p.plan_actusr),
		p.plan_act2fch,
		p.plan_act2usr,
		'plan_act2usrnom'	= (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = p.plan_act2usr),
		plan_estadoini		= null,		-- Inicial: Estado de planificación
		nom_estadoini		= null,
		plan_motnoplan		= null,		-- Inicial: Motivo de no planificación (no se puede planificar)
		nom_motnoplan		= null,
		plan_fealcadef		= null,		-- Inicial: Fecha de definición de alcance
		plan_fefunctst		= null,		-- Inicial: Fecha de fin de pruebas funcionales
		plan_horasper		= null,		-- Inicial: Cantidad de horas planificadas para el presente período
		plan_desaestado		= null,		-- Seguimiento: Estado actual del desarrollo
		nom_desaestado		= null,
		plan_motivsusp		= null,		-- Seguimiento: Motivo de Susp./Desplan./Canc./Rech.
		nom_motivsusp		= null,
		plan_motivrepl		= null,		-- Replanificar: Motivo de replanificación
		nom_motivrepl		= null,
		plan_feplanori		= null,		-- Replanificar: Fecha original de planificación
		plan_feplannue		= null,		-- Replanificar: Nueva fecha de planificación
		plan_cantreplan		= null,		-- Replanificar: Cantidad de replanificaciones (contador)
		plan_horasreplan	= null,		-- Replanificar: Cantidad de horas totales replanificadas
		nom_motivo_replan	= null
		,per_abrev = isnull((select x.per_abrev from GesPet..Periodo x where x.per_nrointerno = p.per_nrointerno),'-')	-- add -003- a.
	from 
		GesPet..Peticion a left join
		GesPet..PeticionPlancab p on (p.per_nrointerno = @per_nrointerno and p.pet_nrointerno = a.pet_nrointerno)
	where 
		@cod_grupo is null and (
		(@cod_bpar is null or a.cod_bpar = @cod_bpar) and 
		(@per_nrointerno is null or p.per_nrointerno is null or p.per_nrointerno = @per_nrointerno) and
		(@prioridad is null or p.prioridad = @prioridad) and 
		--a.cod_clase in ('NUEV','EVOL') and											-- del -002- a.
		--(charindex(a.cod_clase,'NUEV|EVOL|')>0 and p.pet_nrointerno is not null)) and	-- add -002- a.
		charindex(a.cod_clase,'NUEV|EVOL|')>0 and
		charindex(a.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
		a.pet_nrointerno not in (
			select a.pet_nrointerno
			from 
				GesPet..Peticion a left join
				GesPet..PeticionGrupo b on (a.pet_nrointerno = b.pet_nrointerno) left join
				GesPet..Grupo g on (b.cod_grupo = g.cod_grupo) left join
				GesPet..PeticionPlancab pc on (pc.per_nrointerno = @per_nrointerno and pc.pet_nrointerno = a.pet_nrointerno) left join
				GesPet..PeticionPlandet p on (p.per_nrointerno = @per_nrointerno and p.cod_grupo = b.cod_grupo and p.pet_nrointerno = a.pet_nrointerno)
			where 
				(@cod_bpar is null or a.cod_bpar = @cod_bpar) and 
				(@cod_grupo is null or b.cod_grupo = @cod_grupo) and
				(@prioridad is null or pc.prioridad = @prioridad) and 
				(@per_nrointerno is null or p.per_nrointerno is null or p.per_nrointerno = @per_nrointerno) and		-- upd -003- a.
				(charindex(a.cod_clase, 'NUEV|EVOL|') > 0 or
				(charindex(a.cod_clase,'OPTI|')>0 and p.pet_nrointerno is not null)) and
				charindex(a.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
				(b.cod_gerencia = 'DESA') and
				(g.grupo_homologacion = 'S' and charindex(b.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0) and
				(charindex(b.cod_estado, 'ESTIOK|ESTIRK|ESTIMA|PLANOK|PLANIF|PLANRK|EJECRK|EJECUC|SUSPEN|') > 0)
		))
	order by
		1,
		g.nom_grupo,
		pc.prioridad,
		a.pet_nroasignado
	return (0)
go

grant execute on dbo.sp_GetPetPlanificarBP2 to GesPetUsr
go

print 'Actualización realizada.'
go