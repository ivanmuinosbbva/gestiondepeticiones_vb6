/*
-000- a. FJS 25.06.2008 - Nuevo SP para generar exportaci�n completa de Peticiones abierto por Grupo
						  para la gente de Organizaci�n.
-001- a. FJS 11.07.2008 - Se actualiza el criterio de reemplazo de caracteres no compatibles para el proceso 
						  de exportaci�n a archivo plano y se agrega la posibilidad de ejecutar solo una petici�n 
						  para la baja al archivo.
-001- b. FJS 24.07.2008 - Se cambia el tipo de datos para los campos de fecha y se guarda mediante una funci�n.
-002- a. FJS 30.07.2008 - Se agrega el algoritmo de normalizaci�n de cadenas aplicado al t�tulo de la petici�n.
-002- b. FJS 30.07.2008 - Se cambia el orden de los campos de la tabla.
-003- a. FJS 23.10.2008 - Se agregan los siguientes datos a la exportaci�n: Regulatorio, Proyectos IDM, Fecha deseada, Beneficios
						  esperados y Relaci�n con otros requerimientos.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionesOrganizacion'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionesOrganizacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionesOrganizacion
go

create procedure dbo.sp_GetPeticionesOrganizacion
	@flt_pet_nrointerno		int			-- add -001- a.
as 

-- Variables para el armado de las descripciones por petici�n
declare	@pet_nrointerno		int
declare @cod_sector			char(8)
declare @cod_grupo			char(8)
declare	@horasResp			int
declare	@horasRecu			int
declare	@cont				int
declare @ultimapos			int
declare @texto				char(255)
declare @titulo				char(50)	-- add -002- a.

	create table #Peticiones
	(
		pet_empresa			int				null,
		pet_nrointerno		int				null,
		pet_nroasignado		int				null,
		pet_tipo			char(3)			null,
		pet_clase			char(4)			null,
		pet_imptec			char(1)			null,
		pet_titulo			char(50)		null,
		pet_descripcion		char(255)		null,
		pet_prioridad		char(1)			null,
		pet_fe_comite		char(10)		null,	-- upd -001- b. Se pasa de smalldatetime a char
		pet_cod_estado		char(30)		null,	-- 50
		pet_fe_estado		char(10)		null,	-- upd -001- b. Se pasa de smalldatetime a char
		pet_cod_sector		char(98)		null,	-- 120
		pet_cod_bpar		char(50)		null,	-- 80
		pet_cod_solicitante	char(50)		null,	-- 80
		pet_cod_referente	char(50)		null,	-- 80
		pet_cod_autorizante	char(50)		null,	-- 80
		pet_horaspresup		int				null,
		pet_hs_resp			int				null,
		pet_hs_rec			int				null,
		pet_hs_resp_no		int				null,
		pet_hs_rec_no		int				null,
		pet_fe_ini_plan		char(10)		null,	-- upd -001- b. Se pasa de smalldatetime a char
		pet_fe_fin_plan		char(10)		null,	-- upd -001- b. Se pasa de smalldatetime a char
		pet_fe_ini_real		char(10)		null,	-- upd -001- b. Se pasa de smalldatetime a char
		pet_fe_fin_real		char(10)		null,	-- upd -001- b. Se pasa de smalldatetime a char
		pet_fe_ini_orig		char(10)		null,	-- upd -001- b. Se pasa de smalldatetime a char
		pet_fe_fin_orig		char(10)		null,	-- upd -001- b. Se pasa de smalldatetime a char
		pet_cant_planif		int				null,
		pet_corp_local		char(1)			null,
		pet_visibilidad		char(30)		null,	-- 100
		pet_cod_orientacion	char(2)			null,
		grp_cod_sector		char(8)			null,
		grp_cod_grupo		char(8)			null,
		pet_cod_grupo		char(64)		null,	-- 100
		grp_cod_estado		char(30)		null,	-- 100
		grp_horaspresup		int				null,
		grp_fe_ini_plan		char(10)		null,	-- upd -001- b. Se pasa de smalldatetime a char
		grp_fe_fin_plan		char(10)		null,	-- upd -001- b. Se pasa de smalldatetime a char
		grp_fe_ini_real		char(10)		null,	-- upd -001- b. Se pasa de smalldatetime a char
		grp_fe_fin_real		char(10)		null,	-- upd -001- b. Se pasa de smalldatetime a char
		grp_fe_ini_orig		char(10)		null,	-- upd -001- b. Se pasa de smalldatetime a char
		grp_fe_fin_orig		char(10)		null,	-- upd -001- b. Se pasa de smalldatetime a char
		grp_prio_ejecuc		int				null,
		grp_hs_resp			int				null,
		grp_hs_rec			int				null
		--{ add -003- a.
	   ,regulatorio			char(1)			null
	   ,prjid				int				null
	   ,prjsubid			int				null
	   ,prjsubsid			int				null
	   ,prjnom				varchar(255)	null	-- 100
	   ,fec_deseada			char(10)		null
	   ,beneficios			varchar(255)	null	-- 100
	   ,relacion_otros		varchar(255)	null	-- 100
		--}
	)
/*
	Aqu� agrego el total de peticiones y sus datos principales
*/
insert into #Peticiones
	select
		1,
		PET.pet_nrointerno,
		PET.pet_nroasignado,
		PET.cod_tipo_peticion,
		PET.cod_clase,
		PET.pet_imptech,
		PET.titulo,
		null, 
		PET.prioridad,
		convert(char(10), PET.fe_comite, 103),		-- upd -001- b. Se pasa de smalldatetime a char
		cod_estado = 
			(select 
				EST.nom_estado
			 from
				GesPet..Estados EST
			 where
				EST.cod_estado = PET.cod_estado
			),
		convert(char(10), PET.fe_estado, 103),		-- upd -001- b. Se pasa de smalldatetime a char
		cod_sector = 
			(select
				RTRIM(DIR.nom_direccion) + ' >> ' + RTRIM(GER.nom_gerencia) + ' >> ' + RTRIM(SEC.nom_sector)
			 from
				GesPet..Sector SEC 
					inner join GesPet..Gerencia GER on (SEC.cod_gerencia = GER.cod_gerencia)
					inner join GesPet..Direccion DIR on (GER.cod_direccion = DIR.cod_direccion)
			 where
				SEC.cod_sector = PET.cod_sector
			 ),
		cod_bpar = (select REC.nom_recurso from GesPet..Recurso REC where REC.cod_recurso = PET.cod_bpar),
		cod_solicitante = (select REC.nom_recurso from GesPet..Recurso REC where REC.cod_recurso = PET.cod_solicitante),
		cod_referente = (select REC.nom_recurso from GesPet..Recurso REC where REC.cod_recurso = PET.cod_referente),
		cod_autorizante = (select REC.nom_recurso from GesPet..Recurso REC where REC.cod_recurso = PET.cod_director),
		PET.horaspresup,
		pet_Hs_Resp = 
			( 
			select 
				sum(cast(HRS.horas as float(1)))/60 
			from 
				GesPet..HorasTrabajadas HRS inner join 
				GesPet..Recurso REC on REC.cod_recurso = HRS.cod_recurso 
			where 
				HRS.pet_nrointerno = PET.pet_nrointerno and 
				HRS.cod_recurso in (select PRF.cod_recurso from GesPet..RecursoPerfil PRF where PRF.cod_recurso = HRS.cod_recurso and PRF.cod_perfil in ('CSEC','CGRU','BPAR','CDIR','CGER')) and 
				HRS.pet_nrointerno in (select GRU.pet_nrointerno from GesPet..PeticionGrupo GRU where GRU.pet_nrointerno = HRS.pet_nrointerno and  GRU.cod_sector = REC.cod_sector and GRU.cod_grupo = REC.cod_grupo) 
			), 
		pet_Hs_Rec = 
			( 
			select 
				sum(cast(HRS.horas as float(1)))/60 
			from 
				GesPet..HorasTrabajadas HRS inner join 
				GesPet..Recurso REC on REC.cod_recurso = HRS.cod_recurso 
			where 
				HRS.pet_nrointerno = PET.pet_nrointerno and 
				HRS.cod_recurso not in (select PRF.cod_recurso from GesPet..RecursoPerfil PRF where PRF.cod_recurso = HRS.cod_recurso and PRF.cod_perfil in ('CSEC','CGRU','BPAR','CDIR','CGER')) and 
				HRS.pet_nrointerno in (select GRU.pet_nrointerno from GesPet..PeticionGrupo GRU where GRU.pet_nrointerno = HRS.pet_nrointerno and  GRU.cod_sector = REC.cod_sector and GRU.cod_grupo = REC.cod_grupo) 
			), 
		pet_Hs_Resp_NoAsignadas = 
			( 
			select 
				sum(cast(HRS.horas as float(1)))/60 
			from 
				GesPet..HorasTrabajadas HRS inner join 
				GesPet..Recurso REC on REC.cod_recurso = HRS.cod_recurso 
			where 
				HRS.pet_nrointerno = PET.pet_nrointerno and 
				HRS.cod_recurso in (select PRF.cod_recurso from GesPet..RecursoPerfil PRF where PRF.cod_recurso = HRS.cod_recurso and PRF.cod_perfil in ('CSEC','CGRU','BPAR','CDIR','CGER')) and 
				HRS.pet_nrointerno not in (select GRU.pet_nrointerno from GesPet..PeticionGrupo GRU where GRU.pet_nrointerno = HRS.pet_nrointerno and  GRU.cod_sector = REC.cod_sector and GRU.cod_grupo = REC.cod_grupo) 
			), 
		pet_Hs_Rec_NoAsignadas = 
			( 
			select 
				sum(cast(HRS.horas as float(1)))/60 
			from 
				GesPet..HorasTrabajadas HRS inner join 
				GesPet..Recurso REC on REC.cod_recurso = HRS.cod_recurso 
			where 
				HRS.pet_nrointerno = PET.pet_nrointerno and 
				HRS.cod_recurso not in (select PRF.cod_recurso from GesPet..RecursoPerfil PRF where PRF.cod_recurso = HRS.cod_recurso and PRF.cod_perfil in ('CSEC','CGRU','BPAR','CDIR','CGER')) and 
				HRS.pet_nrointerno not in (select GRU.pet_nrointerno from GesPet..PeticionGrupo GRU where GRU.pet_nrointerno = HRS.pet_nrointerno and  GRU.cod_sector = REC.cod_sector and GRU.cod_grupo = REC.cod_grupo) 
			),
		convert(char(10), PET.fe_ini_plan, 103),	-- upd -001- b. Se pasa de smalldatetime a char
		convert(char(10), PET.fe_fin_plan, 103),	-- upd -001- b. Se pasa de smalldatetime a char
		convert(char(10), PET.fe_ini_real, 103),	-- upd -001- b. Se pasa de smalldatetime a char
		convert(char(10), PET.fe_fin_real, 103),	-- upd -001- b. Se pasa de smalldatetime a char
		convert(char(10), PET.fe_ini_orig, 103),	-- upd -001- b. Se pasa de smalldatetime a char
		convert(char(10), PET.fe_fin_orig, 103),	-- upd -001- b. Se pasa de smalldatetime a char
		PET.cant_planif,
		PET.corp_local,
		Visibilidad = 
			(
			select 
				IMP.nom_importancia
			from
				GesPet..Importancia IMP
			where
				IMP.cod_importancia = PET.importancia_cod
			),
		PET.cod_orientacion,
		GRP.cod_sector as grp_cod_sector,
		GRP.cod_grupo,
		grp_cod_grupo = 
			(select
				RTRIM(SEC.nom_sector) + ' >> ' + RTRIM(GRU.nom_grupo)
			 from
				GesPet..Grupo GRU inner join GesPet..Sector SEC on (GRU.cod_sector = SEC.cod_sector)
			 where
				GRU.cod_grupo = GRP.cod_grupo and SEC.cod_sector = GRP.cod_sector
			 ),
		grp_cod_estado = (select EST.nom_estado from GesPet..Estados EST where EST.cod_estado = GRP.cod_estado),
		GRP.horaspresup,
		convert(char(10), GRP.fe_ini_plan, 103),	-- upd -001- b. Se pasa de smalldatetime a char
		convert(char(10), GRP.fe_fin_plan, 103),	-- upd -001- b. Se pasa de smalldatetime a char
		convert(char(10), GRP.fe_ini_real, 103),	-- upd -001- b. Se pasa de smalldatetime a char
		convert(char(10), GRP.fe_fin_real, 103),	-- upd -001- b. Se pasa de smalldatetime a char
		convert(char(10), GRP.fe_ini_orig, 103),	-- upd -001- b. Se pasa de smalldatetime a char
		convert(char(10), GRP.fe_fin_orig, 103),	-- upd -001- b. Se pasa de smalldatetime a char
		GRP.prio_ejecuc,
		--{ add -001- a.
		0,
		0
		--}
		--{ add -003- a.
	   ,PET.pet_regulatorio
	   ,PET.pet_projid
	   ,PET.pet_projsubid
	   ,PET.pet_projsubsid
	   ,pet_projnom = (select PRJ.ProjNom from GesPet..ProyectoIDM PRJ where PRJ.ProjId = PET.pet_projid and PRJ.ProjSubId = PET.pet_projsubid and PRJ.ProjSubSId = PET.pet_projsubsid)
	   ,convert(char(10), PET.fe_requerida, 103)
	   ,null
	   ,null
	   --}
	from
		GesPet..Peticion PET left join GesPet..PeticionGrupo GRP on (PET.pet_nrointerno = GRP.pet_nrointerno)
	-- { add -001- a.
	where
		PET.pet_nrointerno = @flt_pet_nrointerno or @flt_pet_nrointerno is null
	-- }
	
	-- Aqu� arma las descripci�nes por cada petici�n (char(13))
	select @cont = 0
	declare curNumeros cursor for 
		select pet_nrointerno 
			from #Peticiones 
			group by pet_nrointerno
		for read only 

		open curNumeros
		fetch curNumeros into @pet_nrointerno 
		while @@sqlstatus = 0 
			begin 
				select @texto = (select mem_texto from GesPet..PeticionMemo where pet_nrointerno = @pet_nrointerno and mem_campo = 'DESCRIPCIO' and mem_secuencia = 0)
				if char_length(rtrim(@texto)) > 0
					begin
						while charindex(char(13), @texto) > 0
							select @texto = stuff(@texto, charindex(char(13), @texto), 1, space(1))
						while charindex(char(10), @texto) > 0
							select @texto = stuff(@texto, charindex(char(10), @texto), 1, space(1))
						while charindex(char(9), @texto) > 0
							select @texto = stuff(@texto, charindex(char(9), @texto), 1, space(1))
						while charindex(char(7), @texto) > 0
							select @texto = stuff(@texto, charindex(char(7), @texto), 1, space(1))
						-- Actualiza la tabla temporal con las descripciones
						update #Peticiones 
						set pet_descripcion = RTRIM(@texto) where pet_nrointerno = @pet_nrointerno
						-- Vuelve el contador a 0
						select @cont = 0
					end
				fetch curNumeros into @pet_nrointerno
			end 
		close curNumeros
	deallocate cursor curNumeros 
	
	--{ add -003- a.
	-- Aqu� arma los campos 'Beneficios esperados' por cada petici�n
	select @cont = 0
	declare curNumeros cursor for 
		select pet_nrointerno 
			from #Peticiones 
			group by pet_nrointerno
		for read only 

		open curNumeros
		fetch curNumeros into @pet_nrointerno 
		while @@sqlstatus = 0 
			begin 
				select @texto = (select mem_texto from GesPet..PeticionMemo where pet_nrointerno = @pet_nrointerno and mem_campo = 'BENEFICIOS' and mem_secuencia = 0)
				if char_length(rtrim(@texto)) > 0
					begin
						while charindex(char(13), @texto) > 0
							select @texto = stuff(@texto, charindex(char(13), @texto), 1, space(1))
						while charindex(char(10), @texto) > 0
							select @texto = stuff(@texto, charindex(char(10), @texto), 1, space(1))
						while charindex(char(9), @texto) > 0
							select @texto = stuff(@texto, charindex(char(9), @texto), 1, space(1))
						while charindex(char(7), @texto) > 0
							select @texto = stuff(@texto, charindex(char(7), @texto), 1, space(1))
						-- Actualiza la tabla temporal con las descripciones
						update #Peticiones 
						set beneficios = RTRIM(@texto) where pet_nrointerno = @pet_nrointerno
						-- Vuelve el contador a 0
						select @cont = 0
					end
				fetch curNumeros into @pet_nrointerno
			end 
		close curNumeros
	deallocate cursor curNumeros 

	-- Aqu� arma los campos 'Relaci�n con otros requerimientos' por cada petici�n
	select @cont = 0
	declare curNumeros cursor for 
		select pet_nrointerno 
			from #Peticiones 
			group by pet_nrointerno
		for read only 

		open curNumeros
		fetch curNumeros into @pet_nrointerno 
		while @@sqlstatus = 0 
			begin 
				select @texto = (select mem_texto from GesPet..PeticionMemo where pet_nrointerno = @pet_nrointerno and mem_campo = 'RELACIONES' and mem_secuencia = 0)
				if char_length(rtrim(@texto)) > 0
					begin
						while charindex(char(13), @texto) > 0
							select @texto = stuff(@texto, charindex(char(13), @texto), 1, space(1))
						while charindex(char(10), @texto) > 0
							select @texto = stuff(@texto, charindex(char(10), @texto), 1, space(1))
						while charindex(char(9), @texto) > 0
							select @texto = stuff(@texto, charindex(char(9), @texto), 1, space(1))
						while charindex(char(7), @texto) > 0
							select @texto = stuff(@texto, charindex(char(7), @texto), 1, space(1))
						-- Actualiza la tabla temporal con las descripciones
						update #Peticiones 
						set relacion_otros = RTRIM(@texto) where pet_nrointerno = @pet_nrointerno
						-- Vuelve el contador a 0
						select @cont = 0
					end
				fetch curNumeros into @pet_nrointerno
			end 
		close curNumeros
	deallocate cursor curNumeros
	--}
	
	--{ add -002- a.
	-- Normalizaci�n de la cadena del t�tulo de la petici�n
	declare curNumeros cursor for 
		select pet_nrointerno 
			from #Peticiones 
			group by pet_nrointerno
		for read only 

		open curNumeros
		fetch curNumeros into @pet_nrointerno 
		while @@sqlstatus = 0 
			begin 
				select @titulo = (select pet_titulo from #Peticiones where pet_nrointerno = @pet_nrointerno group by pet_nrointerno, pet_titulo)
				if char_length(rtrim(@titulo)) > 0
					begin
						while charindex(char(13), @titulo) > 0
							select @titulo = stuff(@titulo, charindex(char(13), @titulo), 1, space(1))
						while charindex(char(10), @titulo) > 0
							select @titulo = stuff(@titulo, charindex(char(10), @titulo), 1, space(1))
						while charindex(char(9), @titulo) > 0
							select @titulo = stuff(@titulo, charindex(char(9), @titulo), 1, space(1))
						while charindex(char(7), @titulo) > 0
							select @titulo = stuff(@titulo, charindex(char(7), @titulo), 1, space(1))
						-- Actualiza la tabla temporal con las descripciones
						update #Peticiones 
						set pet_titulo = RTRIM(@titulo) where pet_nrointerno = @pet_nrointerno
					end
				fetch curNumeros into @pet_nrointerno
			end 
		close curNumeros
	deallocate cursor curNumeros 
	--}

	-- Calculo de horas por cada petici�n, sector y grupo
	declare curNumeros cursor for 
		--select int_nrointerno, cod_sector, cod_grupo from #Numeros
		select pet_nrointerno, grp_cod_sector, grp_cod_grupo from #Peticiones group by pet_nrointerno, grp_cod_sector, grp_cod_grupo
		for read only 

		open curNumeros
		fetch curNumeros into @pet_nrointerno, @cod_sector, @cod_grupo 
		while @@sqlstatus = 0 
			begin 
				select @horasResp = null
				select @horasRecu = null
				exec sp_GetHorasRealesResp @pet_nrointerno, @cod_sector, @cod_grupo, @horasResp  output
				exec sp_GetHorasRealesRecu @pet_nrointerno, @cod_sector, @cod_grupo, @horasRecu  output
				-- Actualiza la tabla temporal con las horas calculadas
				update 
					#Peticiones 
				set 
					grp_hs_resp    = @horasResp,
					grp_hs_rec     = @horasRecu
				where 
					pet_nrointerno = @pet_nrointerno and
					RTRIM(grp_cod_sector) = RTRIM(@cod_sector) and
					RTRIM(grp_cod_grupo) = RTRIM(@cod_grupo)

				fetch curNumeros into @pet_nrointerno, @cod_sector, @cod_grupo
			end 
		close curNumeros
	deallocate cursor curNumeros

	-- Borro los datos anteriores de la temporal
	truncate table GesPet..tmpPetOrg

	-- Grabo la temporal
	insert into GesPet..tmpPetOrg 
		select 
			pet_nroasignado,
			pet_tipo,
			pet_clase,
			pet_imptec,
			pet_titulo,
			pet_descripcion,
			pet_prioridad,
			pet_fe_comite,
			pet_cod_estado,
			pet_fe_estado,
			pet_cod_sector,
			pet_cod_bpar,
			pet_cod_solicitante,
			pet_cod_referente,
			pet_cod_autorizante,
			pet_horaspresup,
			pet_hs_resp,
			pet_hs_rec,
			pet_hs_resp_no,
			pet_hs_rec_no,
			pet_fe_ini_plan,
			pet_fe_fin_plan,
			pet_fe_ini_real,
			pet_fe_fin_real,
			pet_fe_ini_orig,
			pet_fe_fin_orig,
			pet_cant_planif,
			pet_corp_local,
			pet_visibilidad,
			pet_cod_orientacion,
			pet_cod_grupo,
			grp_cod_estado,
			grp_horaspresup,
			grp_hs_resp,
			grp_hs_rec,
			grp_fe_ini_plan,
			grp_fe_fin_plan,
			grp_fe_ini_real,
			grp_fe_fin_real,
			grp_fe_ini_orig,
			grp_fe_fin_orig,
			pet_empresa
			--{ add -003- a.
		   ,regulatorio
		   ,prjid
		   ,prjsubid
		   ,prjsubsid
		   ,prjnom
		   ,fec_deseada
		   ,beneficios
		   ,relacion_otros
			--}
		from #Peticiones
	
return(0)
go

grant execute on dbo.sp_GetPeticionesOrganizacion to GesPetUsr
go

print 'Actualizaci�n realizada.'