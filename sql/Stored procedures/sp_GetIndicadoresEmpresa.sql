/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 09.06.2017 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetIndicadoresEmpresa'
go

if exists (select * from sysobjects where name = 'sp_GetIndicadoresEmpresa' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetIndicadoresEmpresa
go

create procedure dbo.sp_GetIndicadoresEmpresa
	@empid_peticion		int=null,
	@empid_indicador	int=null
as
	select 
		ape.empid_peticion,
		ape.empid_indicador
	from 
		GesPet.dbo.IndicadoresEmpresa ape
	where
		(ape.empid_peticion = @empid_peticion or @empid_peticion is null) and
		(ape.empid_indicador = @empid_indicador or @empid_indicador is null)
go

grant execute on dbo.sp_GetIndicadoresEmpresa to GesPetUsr
go

print 'Actualización realizada.'
go
