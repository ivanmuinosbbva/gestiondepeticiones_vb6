/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
print 'sp_ChangeGerenciaDireccion'
go
if exists (select * from sysobjects where name = 'sp_ChangeGerenciaDireccion' and sysstat & 7 = 4)
drop procedure dbo.sp_ChangeGerenciaDireccion
go
create procedure dbo.sp_ChangeGerenciaDireccion
    @cod_gerencia	 char(8),
    @new_direccion	char(8)
as

declare @cod_recurso char(10)
declare @old_direccion char(8)

select  @old_direccion=Ge.cod_direccion
from Gerencia Ge
where	RTRIM(Ge.cod_gerencia) = RTRIM(@cod_gerencia)

declare CursRecursos Cursor
	for select cod_recurso
	from Recurso
	where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)
	for Read Only


BEGIN TRANSACTION
	/* perfiles */
	open CursRecursos
	fetch CursRecursos into @cod_recurso
	if @@sqlstatus = 0
	begin
		While @@sqlstatus = 0
		begin
			update RecursoPerfil
			set cod_area = @new_direccion
			where RTRIM(cod_recurso) = @cod_recurso and
				RTRIM(cod_nivel) = 'DIRE' and
				RTRIM(cod_area) = @old_direccion

			fetch CursRecursos into @cod_recurso
		end
	end
	close CursRecursos

	/* recursos */
	update GesPet..Recurso
	set cod_direccion = @new_direccion
	where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)

	/* pet-solicitante */
	update GesPet..Peticion
	set cod_direccion = @new_direccion
	where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)

	/* petsector */
	update GesPet..PeticionSector
	set cod_direccion = @new_direccion
	where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)

	/* petgrupo */
	update GesPet..PeticionGrupo
	set cod_direccion = @new_direccion
	where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)

	/* los agrupamientos */
	update GesPet..Agrup
	set	cod_direccion=@new_direccion
	where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)

	/* los PeticionRecurso */
	update GesPet..PeticionRecurso
	set	cod_direccion=@new_direccion
	where RTRIM(cod_gerencia) = RTRIM(@cod_gerencia)

COMMIT TRANSACTION
DEALLOCATE CURSOR CursRecursos
return(0)
go



grant execute on dbo.sp_ChangeGerenciaDireccion to GesPetUsr
go


