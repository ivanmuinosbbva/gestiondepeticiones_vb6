/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. FJS 26.05.2008 - Se agregan comentarios al c�digo
-002- a. FJS 19.09.2008 - Se corrige el uso de la variable @dsc_estado por esta otra @pcod_estado porque da un error en runtime.
-003- a. FJS 29.08.2011 - Se agrega la posibilidad de enviar mensajes a pooles (SI).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DoMensaje'
go

if exists (select * from sysobjects where name = 'sp_DoMensaje' and sysstat & 7 = 4)
	drop procedure dbo.sp_DoMensaje
go

create procedure dbo.sp_DoMensaje 
	@evt_alcance		varchar(3), 
	@cod_accion			varchar(8)='NULL', 
	@pet_nrointerno		int=0, 
	@cod_hijo			varchar(8)='', 
	@dsc_estado			varchar(6)='NULL', 
	@txt_txtmsg			varchar(250)=null, 
	@txt_txtprx			varchar(250)=null 
as 
	/*
	Par�metros de entrada:

	@evt_alcance		: C�digo de evento (Sector, Grupo o Petici�n) Creo que corresponde al "�mbito de acci�n"
	@cod_accion			: C�digo de acci�n (Nuevo grupo, nuevo sector, cambio de estado de grupo, etc.)
	@pet_nrointerno		: Nro. interno de la petici�n
	@cod_hijo			: Area (Grupo, Sector, etc.)
	@dsc_estado			: Estado (es el estado al que acaba de pasar la petici�n)
	@txt_txtmsg			: Mensaje 1
	@txt_txtprx			: Mensaje 2
	*/

	 -- Declaraci�n de variables
	declare @cod_tipo_peticion  char(3)
	declare @pcod_sector		char(8) 
	declare @pcod_estado		char(6) 
	declare @pcod_situacion		char(6) 
	declare @cod_sector			char(8) 
	declare @cod_estado			char(6) 
	declare @cod_situacion		char(6) 
	declare @cod_perfil			char(4) 
	declare @cod_txtmsg			int 
	declare @est_recept			varchar(3) 
	declare @est_codigo			varchar(255) 
	declare @txt_extend			varchar(255) 
	declare @txt_pzofin			char(1) 
	declare @texto				varchar(255) 
	declare @cod_bpar			char(10) 
	declare @cod_bpe			char(10)
	declare @proxnumero			int

	-- Inicializaci�n de variables
	select @proxnumero = 0   

	-- Obtengo los datos de la petici�n: Sector solicitante, estado actual, situaci�n actual, tipo de petici�n y BP 
	select  
		@pcod_sector       = cod_sector, 
		@pcod_estado       = cod_estado, 
		@pcod_situacion    = cod_situacion,
		@cod_tipo_peticion = cod_tipo_peticion,
		@cod_bpar		   = cod_bpar,
		@cod_bpe		   = cod_BPE		-- add 
	from 
		GesPet..Peticion 
	where 
		pet_nrointerno = @pet_nrointerno

	IF (@pcod_situacion) is null
		select @pcod_situacion = ''
	 
	/*
	Declaro un cursor para obtener la parametrizaci�n de los eventos activos que estan declarados
	en la tabla MensajesEvento. All� obtengo la manera en que ser�n enviados los mensajes.
	*/
	declare CurxEventos cursor for
		select  
			cod_perfil,		-- Perfiles a quien se les envia el mensaje
			cod_txtmsg,		-- C�digo de mensaje prefijado por el sistema
			est_recept,		-- "Receptor"
			est_codigo,		-- C�digo de estado
			txt_extend,		-- Texto adicional 
			txt_pzofin		-- Incluye fecha de vencimiento
		from 
			GesPet..MensajesEvento 
		where 
			(flg_activo='S') and 
			(RTRIM(@evt_alcance)=RTRIM(evt_alcance)) and 
			(RTRIM(@cod_accion)=RTRIM(cod_accion)) and 
			(RTRIM(@dsc_estado)='NULL' OR RTRIM(@dsc_estado)=RTRIM(cod_estado)) 
		for read only 
	 
	-- Si el evento es a nivel de la petici�n entonces...
	if @evt_alcance='PET' 
		begin 
			OPEN CurxEventos FETCH CurxEventos INTO @cod_perfil, @cod_txtmsg, @est_recept, @est_codigo, @txt_extend, @txt_pzofin 
			WHILE (@@sqlstatus = 0) 
			BEGIN 
				/*
				Si encluye fecha de vencimiento, entonces la agrega con su descripci�n
				correspondiente al mensaje adicional o extendido.
				*/
				if RTRIM(@txt_pzofin)='S' 
					begin 
						select @texto = LTRIM(@txt_extend + ' ' + @txt_txtprx + ' ' + @txt_txtmsg)
					end 
				else 
					begin 
						select @texto = LTRIM(@txt_extend + ' ' + @txt_txtmsg)
					end 
				/*
				Si el receptor del mensaje es el Business Partner, lo agrega en 
				la tabla de mensajes a enviar (la tabla Mensajes)
				*/
				if @est_recept='BPA' 
				--if charindex(@est_recept,'BPA|BPE|')>0 
					begin 
						execute sp_ProximoNumero 'ULMSGSYS', @proxnumero OUTPUT 
						insert 
							Mensajes 
						values 
							(@proxnumero, 
							 @pet_nrointerno, 
							 getdate(), 
							 @cod_perfil, 
							 'BBVA', 
							 @cod_bpar, 
							 @cod_txtmsg, 
							 @pcod_estado, 
							 @pcod_situacion, 
							 @texto)
					end 
				/*
				Si el receptor del mensaje es a nivel de la petici�n, entonces si la petici�n no es Propia o Proyecto
				y el perfil del receptor del mensaje es de la rama Solicitante (perfil de Solicitante, Referente, 
				Supervisor o Autorizante), lo que es lo mismo que decir que la petici�n es Normal, Especial o de Auditor�a
				y de la rama Solicitante:
				se agrega el mensaje en la tabla de Mensajes para el nivel de Sector
				*/
				if @est_recept='PET' 
					begin 
						--{ add 
						if @cod_perfil = 'GBPE'
							begin 
								execute sp_ProximoNumero 'ULMSGSYS', @proxnumero OUTPUT 
								insert 
									Mensajes 
								values 
									(@proxnumero, 
									 @pet_nrointerno, 
									 getdate() , 
									 @cod_perfil, 
									 'BBVA',
									 @cod_bpe,
									 @cod_txtmsg, 
									 @pcod_estado,		-- El estado de la petici�n
									 @pcod_situacion, 
									 @texto) 
							end
						--}

						if not ((@cod_tipo_peticion='PRO' or @cod_tipo_peticion='PRJ') and (@cod_perfil='SOLI' or @cod_perfil='REFE' or @cod_perfil='SUPE' or @cod_perfil='AUTO'))
							begin 
								execute sp_ProximoNumero 'ULMSGSYS', @proxnumero OUTPUT 
								insert 
									Mensajes 
								values 
									(@proxnumero, 
									 @pet_nrointerno, 
									 getdate() , 
									 @cod_perfil, 
									 'SECT',
									 @pcod_sector,		-- Es el sector solicitante de la petici�n
									 @cod_txtmsg, 
									 @pcod_estado,		-- El estado de la petici�n
									 @pcod_situacion, 
									 @texto) 
							end
					end 
				/*
				Si el receptor del mensaje es a nivel del Sector, entonces guarda en un cursor todos los sectores
				que se encuentran en el estado configurado en la parametrizaci�n de mensajes para esa petici�n y 
				envia un mensaje para cada uno de los responsables de Sector involucrados en la petici�n.
				*/
				if @est_recept='SEC' 
					begin 
						declare CurxSector cursor for 
							select
								cod_sector, 
								cod_estado, 
								cod_situacion 
							from	
								GesPet..PeticionSector
							where   
								pet_nrointerno = @pet_nrointerno and 
								charindex(RTRIM(cod_estado),RTRIM(@est_codigo)) > 0 
						for read only

						open CurxSector 
						fetch CurxSector into
							@cod_sector, 
							@cod_estado, 
							@cod_situacion 
						while (@@sqlstatus = 0) 
						begin
							execute sp_ProximoNumero 'ULMSGSYS', @proxnumero OUTPUT   
							insert 
								Mensajes  
							values 
								(@proxnumero, 
								 @pet_nrointerno, 
								 getdate() , 
								 @cod_perfil, 
								 'SECT', 
								 @cod_sector,		-- Es el sector ejecutor de la petici�n
								 @cod_txtmsg, 
								 --@cod_estado,		-- del -002- a.
								 @pcod_estado,		-- add -002- a.
								 @cod_situacion, 
								 @texto) 
							fetch CurxSector into
								@cod_sector, 
								@cod_estado, 
								@cod_situacion 
						end
						close CurxSector 
						deallocate cursor CurxSector 
					end 
				/*
				Si el receptor del mensaje es a nivel del Grupo, entonces guarda en un cursor todos los grupos
				que se encuentran en el estado configurado en la parametrizaci�n de mensajes para esa petici�n y 
				envia un mensaje para cada uno de los responsables de Grupo involucrados en la petici�n.
				*/
				if @est_recept='GRU' 
					begin 
						declare CurxGrupo cursor for
							select  
								cod_grupo, 
								cod_estado, 
								cod_situacion 
							from 
								GesPet..PeticionGrupo 
							where   
								pet_nrointerno = @pet_nrointerno and 
								charindex(RTRIM(cod_estado),RTRIM(@est_codigo)) > 0 
						for read only 
						
						open CurxGrupo 
						fetch CurxGrupo into
							@cod_sector, 
							@cod_estado, 
							@cod_situacion 
						while (@@sqlstatus = 0) 
						begin
							execute sp_ProximoNumero 'ULMSGSYS', @proxnumero OUTPUT   
							insert 
								Mensajes  
							values  
								(@proxnumero, 
								 @pet_nrointerno, 
								 getdate() , 
								 @cod_perfil, 
								 'GRUP', 
								 @cod_sector, 
								 @cod_txtmsg, 
								 @cod_estado, 
								 @cod_situacion, 
								 @texto) 
							fetch CurxGrupo into
								@cod_sector, 
								@cod_estado, 
								@cod_situacion 
						end
						close CurxGrupo
						deallocate cursor CurxGrupo 
					end
				--{ add -003- a.
				if ltrim(rtrim(@est_recept))='PSI'	-- Esto esta fijo para SI
					begin 
						execute sp_ProximoNumero 'ULMSGSYS', @proxnumero OUTPUT
						insert Mensajes values (@proxnumero, @pet_nrointerno, getdate(), @cod_perfil,'GRUP', @cod_hijo, @cod_txtmsg, @pcod_estado, '', @texto) 
					end
				--}
		 
				fetch CurxEventos into
					@cod_perfil, 
					@cod_txtmsg, 
					@est_recept, 
					@est_codigo, 
					@txt_extend, 
					@txt_pzofin 
		 
			end
			close CurxEventos 
			deallocate cursor CurxEventos 
		end
	 
	-- ******************************************************************************************************************************************************************************************
	if @evt_alcance='SEC' 
		begin
			-- Obtiene el estado y la situaci�n del sector en cuesti�n de la petici�n
			select  
				@cod_estado    = cod_estado, 
				@cod_situacion = cod_situacion 
			from 
				GesPet..PeticionSector
			where 
				pet_nrointerno = @pet_nrointerno and 
				RTRIM(cod_sector) = RTRIM(@cod_hijo) 
			
			--{ add -002- a.
			if @cod_estado is null or RTRIM(@cod_estado) = ''
				select @cod_estado = @pcod_estado
			if @cod_situacion is null or RTRIM(@cod_situacion) = ''
				select @cod_situacion = @pcod_situacion
			--}
		 
			-- Obtiene la parametrizaci�n de mensajes a enviar y la guarda en un cursor
			open CurxEventos 
			fetch CurxEventos into 
				@cod_perfil, 
				@cod_txtmsg, 
				@est_recept, 
				@est_codigo, 
				@txt_extend, 
				@txt_pzofin 
			while (@@sqlstatus = 0) 

			begin
				/*
				Si encluye fecha de vencimiento, entonces la agrega con su descripci�n
				correspondiente al mensaje adicional o extendido.
				*/
				if RTRIM(@txt_pzofin)='S' 
					begin 
						select @texto = @txt_extend + @txt_txtprx + @txt_txtmsg 
					end 
				else 
					begin 
						select @texto = @txt_extend + @txt_txtmsg 
					end 
				execute sp_ProximoNumero 'ULMSGSYS', @proxnumero OUTPUT   
				insert 
					Mensajes  
				values 
					(@proxnumero, 
					 @pet_nrointerno, 
					 getdate() , 
					 @cod_perfil, 
					 'SECT', 
					 @cod_hijo, 
					 @cod_txtmsg, 
					 @cod_estado, 
					 @cod_situacion, 
					 @texto) 
				fetch CurxEventos into 
					@cod_perfil, 
					@cod_txtmsg, 
					@est_recept, 
					@est_codigo, 
					@txt_extend, 
					@txt_pzofin 
			end
			close CurxEventos 
			deallocate cursor CurxEventos 
		end

	-- ******************************************************************************************************************************************************************************************
	if @evt_alcance='GRU' 
		begin 
			select  
				@cod_estado	   = cod_estado, 
				@cod_situacion = cod_situacion 
			from 
				GesPet..PeticionGrupo 
			where 
				pet_nrointerno = @pet_nrointerno and 
				RTRIM(cod_grupo) = RTRIM(@cod_hijo)

			--{ add -002- a.
			if @cod_estado is null or RTRIM(@cod_estado) = ''
				select @cod_estado = @pcod_estado
			if @cod_situacion is null or RTRIM(@cod_situacion) = ''
				select @cod_situacion = @pcod_situacion
			--}
		 
			open CurxEventos 
			fetch CurxEventos into
				@cod_perfil, 
				@cod_txtmsg, 
				@est_recept, 
				@est_codigo, 
				@txt_extend, 
				@txt_pzofin 
			while (@@sqlstatus = 0) 
			begin 
				/*
				Si encluye fecha de vencimiento, entonces la agrega con su descripci�n
				correspondiente al mensaje adicional o extendido.
				*/
				if RTRIM(@txt_pzofin)='S' 
					begin 
						select @texto = @txt_extend + @txt_txtprx + @txt_txtmsg 
					end 
				else 
					begin 
						select @texto = @txt_extend + @txt_txtmsg 
					end 
				execute sp_ProximoNumero 'ULMSGSYS', @proxnumero OUTPUT 

				insert 
					Mensajes 
				values 
					(@proxnumero, 
					 @pet_nrointerno, 
					 getdate(), 
					 @cod_perfil, 
					 'GRUP', 
					 @cod_hijo, 
					 @cod_txtmsg, 
					 @cod_estado, 
					 @cod_situacion, 
					 @texto) 
				fetch CurxEventos into
					@cod_perfil, 
					@cod_txtmsg, 
					@est_recept, 
					@est_codigo, 
					@txt_extend, 
					@txt_pzofin 
			end
			close CurxEventos 
			deallocate cursor CurxEventos 
		end
	return(0) 
go

sp_procxmode 'sp_DoMensaje', anymode
go

grant execute on dbo.sp_DoMensaje to GesPetUsr 
go

grant execute on dbo.sp_DoMensaje to RolGesIncConex
go

print 'Actualizaci�n realizada.'
go
