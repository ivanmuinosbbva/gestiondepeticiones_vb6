/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 09.11.2010 - Manejo de documentos adjuntos del sistema
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteDocumento'
go

if exists (select * from sysobjects where name = 'sp_DeleteDocumento' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteDocumento
go

create procedure dbo.sp_DeleteDocumento
	@cod_doc	char(6)=null
as
	delete from
		GesPet.dbo.Documento
	where
		(cod_doc = @cod_doc or @cod_doc is null)
go

grant execute on dbo.sp_DeleteDocumento to GesPetUsr
go

print 'Actualización realizada.'
go
