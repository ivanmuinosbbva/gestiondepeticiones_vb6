/*
-000- a. FJS 26.07.2010 - Nuevo SP para...
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPrioridad'
go

if exists (select * from sysobjects where name = 'sp_GetPrioridad' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPrioridad
go

create procedure dbo.sp_GetPrioridad
	@prior_id		char(1)=null,
	@prior_hab		char(1)=null
as 
	select
		a.prior_id,
		a.prior_dsc,
		a.prior_ord,
		a.prior_hab
	from
		GesPet..Prioridad a
	where
		(@prior_id is null or a.prior_id = @prior_id) and
		(@prior_hab is null or a.prior_hab = @prior_hab)
	order by
		a.prior_ord
	return(0)
go

grant execute on dbo.sp_GetPrioridad to GesPetUsr
go

print 'Actualización realizada.'
go