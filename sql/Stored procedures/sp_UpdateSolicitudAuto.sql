/*
-000- a. FJS 26.06.2009 - Nuevo SP para el manejo de solicitudes a Carga de M�quina
-001- a. FJS 31.08.2009 - Mejora: en vez de utilizar la variable, utilizamos la funci�n GetDate para tomar tambi�n la hora.
-002- a. FJS 08.10.2009 - Cambio en la definici�n: si la solicitud es CON enmascaramiento de datos, no necesita aprobaci�n de ninguna clase. En cambio si solicita datos
						  SIN enmascarar, necesita solo aprobaci�n directa de un supervisor y luego es cursada via correo electr�nico a Carga de M�quina.
-003- a. FJS 29.12.2009 - Aquellas solicitudes con restore previo (XCOM*|SORT*) se dejan en Aprobado (C) para que sean tomadas por la siguiente corrida.
-003- b. FJS 21.01.2010 - Aquellas solicitudes con restore previo (XCOM*|SORT*) que solicitaron NO enmascarar los datos, una vez restoreado el archivo,
						  y que el usuario "liber�" la solicitud, entonces queda pendiente de aprobaci�n.
-004- a. FJS 17.07.2014 - Nuevo: nuevo nivel de aprobaci�n.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateSolicitudAuto'
go

if exists (select * from sysobjects where name = 'sp_UpdateSolicitudAuto' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateSolicitudAuto
go

create procedure dbo.sp_UpdateSolicitudAuto
	@sol_nroasignado	int,
	@cod_recurso		char(10),
	@cod_perfil			char(4),
	@fecha				smalldatetime
as
	declare	@sol_masked		char(1)
	declare @sol_estado		char(1)
	declare @sol_fe_auto2	smalldatetime
	declare @masked			char(1)				-- add -003- b.
	--declare @new_estado		char(1)				-- add -004- a.
	
	--{ add -003- b.
	select @masked = x.sol_mask
	from GesPet..Solicitudes x
	where x.sol_nroasignado = @sol_nroasignado
	--)

	-- { add -003- a.
	if @cod_perfil = 'REST'
		if @masked = 'S'	-- add -003- b.
			begin
				update GesPet..Solicitudes
				set	sol_estado = 'C'
				where sol_nroasignado = @sol_nroasignado
				return(0)
			end
		--{ add -003- b.
		else
			begin
				update GesPet..Solicitudes
				set	sol_estado = 'A'
				where sol_nroasignado = @sol_nroasignado
				return(0)
			end
		--}
	else
		begin
			--select @sol_estado = 'E'	-- add -002- a. Solo el Resp. de Sector (un supervisor con autorizaci�n de nivel 3) dej� como "Enviado y finalizado."	-- del -004- a.
			--{ add -004- a.
			if @cod_perfil = 'CSEC'
				update GesPet..Solicitudes
				set 
					sol_resp_sect = @cod_recurso,			-- Se deja as�, para reflejar autorizaciones de otros supervisores
					sol_fe_auto2  = getdate(),
					sol_estado    = 'E'
				where sol_nroasignado = @sol_nroasignado

			else
				update GesPet..Solicitudes
				set 
					sol_fe_auto1  = getdate(),
					sol_estado    = 'B'
				where sol_nroasignado = @sol_nroasignado
			--}
		end
	return(0)
go

grant execute on dbo.sp_UpdateSolicitudAuto to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
