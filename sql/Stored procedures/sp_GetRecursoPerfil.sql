use GesPet
go

print 'Creando/actualizando SP: sp_GetRecursoPerfil'
go

if exists (select * from sysobjects where name = 'sp_GetRecursoPerfil' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRecursoPerfil
go

create procedure dbo.sp_GetRecursoPerfil
	@cod_recurso	char(10)=null,
	@cod_perfil		char(4)=null
as
	begin
		select	
			Rp.cod_recurso,
			nom_recurso = (select Re.nom_recurso from Recurso Re where RTRIM(Re.cod_recurso)=RTRIM(Rp.cod_recurso)),
			Rp.cod_perfil,
			nom_perfil = (select Pe.nom_perfil from Perfil Pe where RTRIM(Pe.cod_perfil) = RTRIM(Rp.cod_perfil)),
			Rp.cod_nivel,
			Rp.cod_area,
			Su.nom_grupo as nom_area,
			Su.flg_habil as flg_area
		from GesPet..RecursoPerfil Rp, Grupo Su
		where
			(@cod_recurso is null or RTRIM(@cod_recurso) is null or RTRIM(@cod_recurso)='' or RTRIM(Rp.cod_recurso) = RTRIM(@cod_recurso)) and
			(@cod_perfil is null or RTRIM(@cod_perfil) is null or RTRIM(@cod_perfil)='' or RTRIM(Rp.cod_perfil) = RTRIM(@cod_perfil)) and
			(RTRIM(Rp.cod_nivel) = 'GRUP') and
			(RTRIM(Rp.cod_area) *= RTRIM(Su.cod_grupo))
		UNION ALL
		select	
			Rp.cod_recurso,
			nom_recurso = (select Re.nom_recurso from Recurso Re where RTRIM(Re.cod_recurso)=RTRIM(Rp.cod_recurso)),
			Rp.cod_perfil,
			nom_perfil = (select Pe.nom_perfil from Perfil Pe where RTRIM(Pe.cod_perfil) = RTRIM(Rp.cod_perfil)),
			Rp.cod_nivel,
			Rp.cod_area,
			Gr.nom_sector as nom_area,
			Gr.flg_habil as flg_area
		from GesPet..RecursoPerfil Rp, Sector Gr
		where
			(@cod_recurso is null or RTRIM(@cod_recurso) is null or RTRIM(@cod_recurso)='' or RTRIM(Rp.cod_recurso) = RTRIM(@cod_recurso)) and
			(@cod_perfil is null or RTRIM(@cod_perfil) is null or RTRIM(@cod_perfil)='' or RTRIM(Rp.cod_perfil) = RTRIM(@cod_perfil)) and
			(RTRIM(Rp.cod_nivel) = 'SECT') and
			(RTRIM(Rp.cod_area) *= RTRIM(Gr.cod_sector))
		UNION ALL
		select	
			Rp.cod_recurso,
			nom_recurso = (select Re.nom_recurso from Recurso Re where RTRIM(Re.cod_recurso)=RTRIM(Rp.cod_recurso)),
			Rp.cod_perfil,
			nom_perfil = (select Pe.nom_perfil from Perfil Pe where RTRIM(Pe.cod_perfil) = RTRIM(Rp.cod_perfil)),
			Rp.cod_nivel,
			Rp.cod_area,
			Ge.nom_gerencia as nom_area,
			Ge.flg_habil as flg_area
		from GesPet..RecursoPerfil Rp, Gerencia Ge
		where
			(@cod_recurso is null or RTRIM(@cod_recurso) is null or RTRIM(@cod_recurso)='' or RTRIM(Rp.cod_recurso) = RTRIM(@cod_recurso)) and
			(@cod_perfil is null or RTRIM(@cod_perfil) is null or RTRIM(@cod_perfil)='' or RTRIM(Rp.cod_perfil) = RTRIM(@cod_perfil)) and
			(RTRIM(Rp.cod_nivel) = 'GERE') and
			(RTRIM(Rp.cod_area) *= RTRIM(Ge.cod_gerencia))
		UNION ALL
		select	
			Rp.cod_recurso,
			nom_recurso = (select Re.nom_recurso from Recurso Re where RTRIM(Re.cod_recurso)=RTRIM(Rp.cod_recurso)),
			Rp.cod_perfil,
			nom_perfil = (select Pe.nom_perfil from Perfil Pe where RTRIM(Pe.cod_perfil) = RTRIM(Rp.cod_perfil)),
			Rp.cod_nivel,
			Rp.cod_area,
			Di.nom_direccion as nom_area,
			Di.flg_habil as flg_area
		from GesPet..RecursoPerfil Rp, Direccion Di
		where
			(@cod_recurso is null or RTRIM(@cod_recurso) is null or RTRIM(@cod_recurso)='' or RTRIM(Rp.cod_recurso) = RTRIM(@cod_recurso)) and
			(@cod_perfil is null or RTRIM(@cod_perfil) is null or RTRIM(@cod_perfil)='' or RTRIM(Rp.cod_perfil) = RTRIM(@cod_perfil)) and
			(RTRIM(Rp.cod_nivel) = 'DIRE') and
			(RTRIM(Rp.cod_area) *= RTRIM(Di.cod_direccion))
		UNION ALL
		select	
			Rp.cod_recurso,
			nom_recurso = (select Re.nom_recurso from Recurso Re where RTRIM(Re.cod_recurso)=RTRIM(Rp.cod_recurso)),
			Rp.cod_perfil,
			nom_perfil = (select Pe.nom_perfil from Perfil Pe where RTRIM(Pe.cod_perfil) = RTRIM(Rp.cod_perfil)),
			Rp.cod_nivel,
			Rp.cod_area,
			'Todo el BBVA' as nom_area,
			'S' as flg_area
		from GesPet..RecursoPerfil Rp
		where
			(@cod_recurso is null or RTRIM(@cod_recurso) is null or RTRIM(@cod_recurso)='' or RTRIM(Rp.cod_recurso) = RTRIM(@cod_recurso)) and
			(@cod_perfil is null or RTRIM(@cod_perfil) is null or RTRIM(@cod_perfil)='' or RTRIM(Rp.cod_perfil) = RTRIM(@cod_perfil)) and
			(RTRIM(Rp.cod_nivel) = 'BBVA')
		order by 
			nom_recurso, nom_perfil
	end
	return(0)
go

grant execute on dbo.sp_GetRecursoPerfil to GesPetUsr
go

print 'Actualización realizada.'
go