/*
-000- a. FJS 22.05.2008 - Nuevo SP para obtener todos los datos de una petición a nivel de grupos que son homologables.
*/
use GesPet
go

print 'Creando/actualizando SP: sp_GetPetGrupoHomologable'
go

if exists (select * from sysobjects where name = 'sp_GetPetGrupoHomologable' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPetGrupoHomologable
go

create procedure dbo.sp_GetPetGrupoHomologable 
    @pet_nrointerno		int = null
as
    select 
        PG.pet_nrointerno,
        PG.cod_grupo,
        PG.cod_sector,
        PG.cod_gerencia,
        PG.cod_direccion,
        PG.fe_ini_plan,
        PG.fe_fin_plan,
        PG.fe_ini_real,
        PG.fe_fin_real,
        PG.horaspresup,
        PG.cod_estado,
        PG.fe_estado,
        PG.cod_situacion,
        PG.ult_accion,
        PG.hst_nrointerno_sol,
        PG.hst_nrointerno_rsp,
        PG.audit_user,
        PG.audit_date,
        PG.fe_fec_plan,
        PG.fe_ini_orig,
        PG.fe_fin_orig,
        PG.fe_fec_orig,
        PG.cant_planif,
        PG.prio_ejecuc,
        PG.info_adicio
    from
        GesPet.dbo.PeticionGrupo PG inner join GesPet.dbo.Grupo G on PG.cod_grupo = G.cod_grupo
    where
        PG.pet_nrointerno = @pet_nrointerno and 
        G.grupo_homologacion = 'S'

return(0) 
go

grant execute on dbo.sp_GetPetGrupoHomologable to GesPetUsr
go

print 'Actualización realizada.'
