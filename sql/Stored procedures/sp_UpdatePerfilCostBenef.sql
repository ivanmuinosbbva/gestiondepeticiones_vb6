use GesPet
go
print 'sp_UpdatePerfilCostBenef'
go
if exists (select * from sysobjects where name = 'sp_UpdatePerfilCostBenef' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdatePerfilCostBenef
go
create procedure dbo.sp_UpdatePerfilCostBenef
	@cod_PerfilCostBenef	char(8),
	@nom_PerfilCostBenef	char(30),
	@flg_habil		char(1)=null
as



if not exists (select cod_PerfilCostBenef from GesPet..PerfilCostBenef where RTRIM(cod_PerfilCostBenef) = RTRIM(@cod_PerfilCostBenef))
	begin 
		raiserror 30011 'Balance Rubro Inexistente'   
		select 30011 as ErrCode , 'Balance Rubro Inexistente' as ErrDesc   
		return (30011)   
	end 

BEGIN TRANSACTION
	update GesPet..PerfilCostBenef
	set nom_PerfilCostBenef = @nom_PerfilCostBenef,
		flg_habil = @flg_habil
	where RTRIM(cod_PerfilCostBenef) = RTRIM(@cod_PerfilCostBenef)
COMMIT

return(0)
go


grant execute on dbo.sp_UpdatePerfilCostBenef to GesPetUsr
go
