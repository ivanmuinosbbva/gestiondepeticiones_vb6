/*
-001- a. FJS 27.12.2016 - Se agregan dos funciones para determinar si debe considerarse el sector para el c�lculo del estado de la petici�n.
						  La primer funci�n determina si existen grupos ejecutores distintos de homologaci�n, seguridad inform�tica, arquitectura, etc.
						  Si no existen, devuelve 0. Si existen, devuelve la cantidad de grupos descontando los grupos especiales.
						  La segunda funci�n, devuelve 0 si el sector no tiene ninguno de los grupos especiales definidos, y > 0 si lo tiene.
*/

use GesPet
go

print 'Actualizando procedimiento almacenado: sp_GetPeticionSectorXt'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionSectorXt' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionSectorXt
go

create procedure dbo.sp_GetPeticionSectorXt
	@pet_nrointerno		int=0,
	@cod_sector		char(8)=null,
	@cod_estado		varchar(250)=null
as
	begin
		select
			Ps.pet_nrointerno,
			Ps.cod_direccion,
			nom_direccion = (select Di.nom_direccion from Direccion Di where RTRIM(Di.cod_direccion) = RTRIM(Ps.cod_direccion)),
			Ps.cod_gerencia,
			nom_gerencia = (select Ge.nom_gerencia from Gerencia Ge where RTRIM(Ge.cod_gerencia) = RTRIM(Ps.cod_gerencia)),
			Ps.cod_sector,
			Gr.nom_sector,
			Gr.cod_abrev,
			Pt.titulo,
			Pt.pet_nroasignado,
			Ps.fe_ini_plan,
			Ps.fe_fin_plan,
			Ps.fe_ini_real,
			Ps.fe_fin_real,
			Ps.horaspresup,
			Ps.cod_estado,
			Ps.fe_estado,
			Ps.cod_situacion,
			nom_situacion=(select nom_situacion from GesPet..Situaciones where RTRIM(Ps.cod_situacion)=RTRIM(cod_situacion)),
			Ps.hst_nrointerno_sol,
			Ps.hst_nrointerno_rsp,
			EST.nom_estado,
			EST.flg_rnkup,
			Ps.fe_ini_orig,
			Ps.fe_fin_orig,
			--{ add -001- a.
			considerar = (
				select count(1) 
				from PeticionGrupo Pg inner join Grupo g on (Pg.cod_grupo = g.cod_grupo)
				where 
					Pg.pet_nrointerno = Ps.pet_nrointerno and 
					Pg.cod_sector = Ps.cod_sector and 
					charindex(g.grupo_homologacion,'H|1|2|')=0),
			especial = (
				select count(1)
				from PeticionGrupo Pg inner join Grupo g on (Pg.cod_grupo = g.cod_grupo)
				where 
					Pg.pet_nrointerno = Ps.pet_nrointerno and 
					Pg.cod_sector = Ps.cod_sector and 
					charindex(g.grupo_homologacion,'H|1|2|')>0)
			--}
		from
			PeticionSector Ps inner join
			Peticion Pt on (Ps.pet_nrointerno = Pt.pet_nrointerno) inner join
			Sector Gr on (Ps.cod_sector = Gr.cod_sector) inner join 
			Estados EST on (EST.cod_estado = Ps.cod_estado)
			/*
			GesPet..PeticionSector Ps, GesPet..Estados EST,
			GesPet..Sector Gr, GesPet..Peticion Pt
			*/
		where
			(@pet_nrointerno = 0 or @pet_nrointerno = Ps.pet_nrointerno) and
			(@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(Ps.cod_sector) = RTRIM(@cod_sector)) and
			(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(Ps.cod_estado),RTRIM(@cod_estado))>0) 
			/*
			and
			(Ps.pet_nrointerno *= Pt.pet_nrointerno) and
			(RTRIM(Ps.cod_sector) *= RTRIM(Gr.cod_sector)) and
			RTRIM(Ps.cod_estado) *= RTRIM(EST.cod_estado)
			*/
		order by 
			Ps.cod_sector ASC
	end
	return(0)
go

grant execute on dbo.sp_GetPeticionSectorXt to GesPetUsr
go

print 'Actualizaci�n finalizada.'
go


