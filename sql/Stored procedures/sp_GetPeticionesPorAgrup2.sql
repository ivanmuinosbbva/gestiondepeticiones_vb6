/*
-000- a. FJS 15.03.2010 - Nuevo SP para generar exportación parcial de peticiones por Agrupamiento para la gente de Organización (Walter Salvi).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionesPorAgrup2'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionesPorAgrup2' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionesPorAgrup2
go

create procedure dbo.sp_GetPeticionesPorAgrup2
	@agr_nrointerno		int=null
as 

	-- Variables para el armado de las descripciones por petición
	declare	@pet_nrointerno		int
	declare @cod_sector			char(8)
	declare @cod_grupo			char(8)
	declare	@horasResp			int
	declare	@horasRecu			int
	declare	@cont				int
	declare @ultimapos			int
	declare @texto				varchar(8192)
	--declare @texto_completo		varchar(16384)
	declare @titulo				char(50)

	create table #Peticiones (
		pet_empresa			int				null,
		pet_nrointerno		int				null,
		pet_nroasignado		int				null,
		pet_tipo			char(3)			null,
		pet_titulo			char(50)		null,
		pet_nom_estado		char(30)		null,
		pet_fe_ini_plan		char(10)		null,
		pet_fe_fin_plan		char(10)		null,
		pet_fe_ini_real		char(10)		null,
		pet_fe_fin_real		char(10)		null,
		pet_cod_agrup		char(30)		null)
	/*
		Aquí agrego el total de peticiones y sus datos principales
	*/
	insert into #Peticiones
		select
			1,
			PET.pet_nrointerno,
			PET.pet_nroasignado,
			PET.cod_tipo_peticion,
			PET.titulo,
			nom_estado = (select EST.nom_estado from GesPet..Estados EST where EST.cod_estado = PET.cod_estado),
			convert(char(10), PET.fe_ini_plan, 103),
			convert(char(10), PET.fe_fin_plan, 103),
			convert(char(10), PET.fe_ini_real, 103),
			convert(char(10), PET.fe_fin_real, 103),
			AG1.agr_titulo
		from
			GesPet..Peticion PET left join 
			GesPet..AgrupPetic AG2 on (PET.pet_nrointerno = AG2.pet_nrointerno) inner join
			GesPet..Agrup AG1 on (AG2.agr_nrointerno = AG1.agr_nrointerno)
		where
			(AG1.agr_nrointerno = @agr_nrointerno or @agr_nrointerno is null)
		
		-- Muestro el resultado
		select
			pet_nrointerno,
			pet_nroasignado,
			pet_tipo,
			pet_titulo,
			pet_nom_estado,
			pet_fe_ini_plan,
			pet_fe_fin_plan,
			pet_fe_ini_real,
			pet_fe_fin_real,
			pet_cod_agrup
		from
			#Peticiones
	
	return(0)
go

grant execute on dbo.sp_GetPeticionesPorAgrup2 to GesPetUsr
go

print 'Actualización realizada.'