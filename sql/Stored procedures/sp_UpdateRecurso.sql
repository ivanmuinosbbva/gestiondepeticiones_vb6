/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. FJS 13.10.2009 - Se agrega la parte que actualiza los datos de los perfiles asociados al recurso. Se rompe la relaci�n existente entre Petici�n-Recurso cuando
						  el estado del grupo no es alguno de los estados terminales.
-002- a. FJS 13.05.2016 - Correcci�n: se agrega la parte de la actualizaci�n cuando los datos en perfiles estan desactualizados (aunque no hubo cambio de �rea en Recurso).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateRecurso'
go

if exists (select * from sysobjects where name = 'sp_UpdateRecurso' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateRecurso
go

create procedure dbo.sp_UpdateRecurso 
    @cod_recurso		char(10), 
    @nom_recurso		char(50), 
    @vinculo			char(1)=' ', 
    @cod_direccion		char(8)=' ', 
    @cod_gerencia		char(8)=' ', 
    @cod_sector			char(8)=' ', 
    @cod_grupo			char(8)=' ', 
    @flg_cargoarea		char(1)=' ', 
    @estado_recurso		char(1)=' ', 
    @horasdiarias		smallint=8, 
    @observaciones		varchar(255)=null, 
    @dlg_recurso		char(10)=' ', 
    @dlg_desde			smalldatetime=null, 
    @dlg_hasta			smalldatetime=null,
    @email				varchar(60)='',
    @euser				char(1)=' '
as 
	--{ add -001- a.
	/*
	declare @new_direccion	char(8)
	declare @new_gerencia	char(8)
	declare @new_sector		char(8)
	declare @new_grupo		char(8)
	*/

	declare @old_direccion	char(8)
	declare @old_gerencia	char(8)
	declare @old_sector		char(8)
	declare @old_grupo		char(8)
	declare @cambios		char(1)

	-- 1. Estructura actual
	select  
		@old_direccion	= isnull(r.cod_direccion,''),
		@old_gerencia	= isnull(r.cod_gerencia,''),
		@old_sector		= isnull(r.cod_sector,''),
		@old_grupo		= isnull(r.cod_grupo,''),
		@cambios		= 'N'
	from Recurso r
	where RTRIM(r.cod_recurso) = RTRIM(@cod_recurso) 

		/* and 
		RTRIM(Gr.cod_grupo) = RTRIM(@cod_grupo) and
		RTRIM(Se.cod_sector) = RTRIM(Gr.cod_sector) and
		RTRIM(Se.cod_gerencia) = RTRIM(Ge.cod_gerencia)
		*/

	-- 2. Estructura a la que cambia
	/*
	SELECT @new_direccion =	@cod_direccion
	SELECT @new_gerencia  = @cod_gerencia
	SELECT @new_sector	  = @cod_sector
	SELECT @new_grupo	  = @cod_grupo
	*/

	/*
	select  
		@new_direccion  = isnull(Ge.cod_direccion,''),
		@new_gerencia   = isnull(Se.cod_gerencia,''),
		@new_sector		= isnull(Se.cod_sector,''),
		@new_grupo		= isnull(Gr.cod_grupo,'')
	from 
		Grupo Gr, 
		Sector Se,
		Gerencia Ge
	where 
		RTRIM(Gr.cod_grupo) = RTRIM(@cod_grupo) and
		RTRIM(Se.cod_sector) = RTRIM(@new_sector) and
		RTRIM(Se.cod_gerencia) = RTRIM(Ge.cod_gerencia)
	--}
	*/

	if ((RTRIM(@dlg_recurso) is not null and RTRIM(@dlg_recurso)<>'')) 
		begin 
			if exists (select cod_recurso from GesPet..Recurso where 
				  RTRIM(cod_recurso) = RTRIM(@dlg_recurso) and 
				  RTRIM(dlg_recurso) = RTRIM(@cod_recurso) and 
				  convert(char(8),@dlg_desde,112) <= convert(char(8),dlg_desde,112) and 
				  convert(char(8),@dlg_hasta,112) >= convert(char(8),dlg_hasta,112) 
				  ) 
				  begin 
					raiserror 30011 'Superposicion de Delegaci�n'   
					select 30011 as ErrCode , 'Superposicion de Delegaci�n' as ErrDesc   
					return (30011)   
				  end 
			if exists (select cod_recurso from GesPet..Recurso where 
				  RTRIM(cod_recurso) = RTRIM(@dlg_recurso) and 
				  RTRIM(dlg_recurso) = RTRIM(@cod_recurso) and 
				  convert(char(8),@dlg_desde,112) >= convert(char(8),dlg_desde,112) and 
				  convert(char(8),@dlg_desde,112) <= convert(char(8),dlg_hasta,112) 
				  ) 
				  begin 
					raiserror 30012 'Superposicion de Delegaci�n'   
					select 30012 as ErrCode , 'Superposicion de Delegaci�n' as ErrDesc   
					return (30012)   
				  end 
			if exists (select cod_recurso from GesPet..Recurso where 
				  RTRIM(cod_recurso) = RTRIM(@dlg_recurso) and 
				  RTRIM(dlg_recurso) = RTRIM(@cod_recurso) and 
				  convert(char(8),@dlg_hasta,112) >= convert(char(8),dlg_desde,112) and 
				  convert(char(8),@dlg_hasta,112) <= convert(char(8),dlg_hasta,112) 
				  ) 
				  begin 
					raiserror 30013 'Superposicion de Delegaci�n'   
					select 30013 as ErrCode , 'Superposicion de Delegaci�n' as ErrDesc   
					return (30013)   
				  end 
		end 
	 
	 
	if ((RTRIM(@flg_cargoarea) is not null and RTRIM(@flg_cargoarea)<>'')) 
		begin 
			if exists (select cod_recurso from GesPet..Recurso where 
				  ('X' + RTRIM(cod_direccion) = 'X' + RTRIM(@cod_direccion)) and 
				  ('X' + RTRIM(cod_gerencia) = 'X' + RTRIM(@cod_gerencia)) and 
				  ('X' + RTRIM(cod_sector) = 'X' + RTRIM(@cod_sector)) and 
				  ('X' + RTRIM(cod_grupo) = 'X' + RTRIM(@cod_grupo)) and 
				  (RTRIM(flg_cargoarea) = 'S') and 
				  (RTRIM(cod_recurso) <> RTRIM(@cod_recurso)) ) 
				begin 
					raiserror 30012 'Ya existe un recurso a cargo del Area'   
					select 30012 as ErrCode , 'Ya existe un recurso a cargo del Area' as ErrDesc   
					return (30012)   
				end 
			if (RTRIM(@estado_recurso)<>'A') 
				begin 
					raiserror 30013 'No puede estar a cargo de Area un recurso no Activo'   
					select 30013 as ErrCode , 'No puede estar a cargo de Area un recurso no Activo' as ErrDesc   
					return (30013)   
				end 
		end 
	
	-- Actualizo los perfiles para el recurso
	-- Reemplazo el grupo anterior con el nuevo c�digo de grupo para los perfiles que tuviera el recurso
	--if RTRIM(@new_grupo) is not null or @new_grupo <> ''
		update GesPet..RecursoPerfil
		set cod_area = isnull(@cod_grupo, '')
		where 
			RTRIM(cod_recurso)	= RTRIM(@cod_recurso) and
			RTRIM(cod_nivel)	= 'GRUP' and
			RTRIM(cod_area)		<> RTRIM(@cod_grupo)										-- add -002- a.
			--{ del -002- a.
			--RTRIM(cod_area)		= RTRIM(@old_grupo)
			--and (RTRIM(@cod_grupo) <> rtrim(@old_grupo) and RTRIM(@cod_grupo) <> '')
			--}

	-- Reemplazo el sector anterior con el nuevo c�digo de sector para los perfiles que tuviera el recurso
	--if @new_sector is not null
		update GesPet..RecursoPerfil
		set cod_area = @cod_sector
		where 
			RTRIM(cod_recurso)	= RTRIM(@cod_recurso) and
			RTRIM(cod_nivel)	= 'SECT' and
			RTRIM(cod_area)		<> RTRIM(@cod_sector)										-- add -002- a.
			--{ del -002- a.
			--RTRIM(cod_area)		= RTRIM(@old_sector)									
			--and (rtrim(@cod_sector) <> rtrim(@old_sector) and rtrim(@cod_sector) <> '')
			--}

	-- Reemplazo la gerencia anterior con el nuevo c�digo de gerencia del sector nuevo
	--if @new_gerencia is not null
		update GesPet..RecursoPerfil
		set cod_area = @cod_gerencia
		where 
			RTRIM(cod_recurso)	= RTRIM(@cod_recurso) and
			RTRIM(cod_nivel)	= 'GERE' and
			RTRIM(cod_area)		<> RTRIM(@cod_gerencia)										-- add -002- a.
			--{ del -002- a.
			--RTRIM(cod_area) = RTRIM(@old_gerencia) 
			--and (rtrim(@cod_gerencia) <> rtrim(@old_gerencia) and rtrim(@cod_gerencia) <> '')	
			--}

	-- Reemplazo la direcci�n anterior con el nuevo c�digo de direcci�n del sector nuevo
	--if @new_direccion is not null
		update GesPet..RecursoPerfil
		set cod_area = @cod_direccion
		where 
			RTRIM(cod_recurso)	= RTRIM(@cod_recurso) and
			RTRIM(cod_nivel)	= 'DIRE' and
			RTRIM(cod_area)		<> RTRIM(@cod_direccion)									-- add -002- a.
			--{ del -002- a.
			--RTRIM(cod_area) = RTRIM(@old_direccion) 
			--and (rtrim(@cod_direccion) <> rtrim(@old_direccion) and rtrim(@cod_direccion) <> '')
			--}

	-- AGRUPAMIENTOS del recurso
	update 
		GesPet..Agrup
	set	
		cod_direccion = isnull(@cod_direccion,''),
		cod_gerencia  = isnull(@cod_gerencia, ''),
		cod_sector    = isnull(@cod_sector, ''),
		cod_grupo     = isnull(@cod_grupo,'')
	where 
		RTRIM(cod_usualta)   = RTRIM(@cod_recurso) and
		RTRIM(cod_direccion) = RTRIM(@old_direccion) and
		RTRIM(cod_gerencia)  = RTRIM(@old_gerencia) and
		RTRIM(cod_sector)    = RTRIM(@old_sector) and
		RTRIM(cod_grupo)     = RTRIM(@old_grupo) and
		(
			(rtrim(@cod_grupo) <> '' and rtrim(@cod_grupo) <> rtrim(@old_grupo)) or
			(rtrim(@cod_sector) <> '' and rtrim(@cod_sector) <> rtrim(@old_sector)) or
			(rtrim(@cod_gerencia) <> '' and rtrim(@cod_gerencia) <> rtrim(@old_gerencia)) or
			(rtrim(@cod_direccion) <> '' and rtrim(@cod_direccion) <> rtrim(@old_direccion))
		)
		
		/*
		-- PETICIONRECURSO
		-- Actualizo la estructura jer�rquica en las peticiones en las que se ve involucrado el grupo a reestructurar
		delete 
			GesPet..PeticionRecurso
		from 
			GesPet..PeticionRecurso a, 
			GesPet..PeticionGrupo b
		where 
			a.pet_nrointerno = b.pet_nrointerno and 
			a.cod_grupo = b.cod_grupo and 
			a.cod_sector = b.cod_sector and
			RTRIM(a.cod_recurso) = RTRIM(@cod_recurso) and b.cod_estado not in ('RECHAZ','CANCEL','RECHTE','TERMIN','ANEXAD','ANULAD')
		*/
	
	update 
		GesPet..Recurso 
	set 
		nom_recurso     = @nom_recurso, 
		vinculo         = @vinculo, 
		cod_direccion   = @cod_direccion, 
		cod_gerencia    = @cod_gerencia, 
		cod_sector      = @cod_sector, 
		cod_grupo		= @cod_grupo, 
		flg_cargoarea   = @flg_cargoarea, 
		estado_recurso  = @estado_recurso, 
		horasdiarias    = @horasdiarias, 
		observaciones   = @observaciones, 
		dlg_recurso     = @dlg_recurso, 
		dlg_desde       = @dlg_desde, 
		dlg_hasta       = @dlg_hasta, 
		email			= @email, 
		euser			= @euser, 
		audit_user		= SUSER_NAME(),   
		audit_date		= getdate()
	where 
		RTRIM(cod_recurso) = RTRIM(@cod_recurso)

	return(0)
go

sp_procxmode 'sp_UpdateRecurso', anymode
go

grant execute on dbo.sp_UpdateRecurso to GesPetUsr 
go

print 'Actualizaci�n realizada.'
go
