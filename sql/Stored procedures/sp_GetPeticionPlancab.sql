/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionPlancab'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionPlancab' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionPlancab
go

create procedure dbo.sp_GetPeticionPlancab
	@per_nrointerno	int=null,
	@pet_nrointerno	int=null
as
	select 
		a.per_nrointerno,
		a.pet_nrointerno,
		a.prioridad,
		a.plan_actfch,
		a.plan_actusr,
		a.cod_bpe,
		a.plan_origen
		/*
		a.cod_bpar,
		a.plan_estado,
		a.plan_act2fch,
		a.plan_act2usr,
		a.plan_stage
		*/
	from 
		GesPet.dbo.PeticionPlancab a
	where
		(a.per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(a.pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) 
	return(0)
go

grant execute on dbo.sp_GetPeticionPlancab to GesPetUsr
go

print 'Actualización realizada.'
go