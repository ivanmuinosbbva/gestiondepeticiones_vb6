/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 22.12.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionValidacion'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionValidacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionValidacion
go

create procedure dbo.sp_UpdatePeticionValidacion
	@pet_nrointerno	int,
	@valid			int,
	@valitem		int,
	@valitemvalor	char(1)
as
	update 
		GesPet.dbo.PeticionValidacion
	set
		valitemvalor	= @valitemvalor,
		audituser		= suser_name(),
		auditfecha		= getdate()
	where 
		pet_nrointerno = @pet_nrointerno and
		valid = @valid and
		valitem = @valitem
go

grant execute on dbo.sp_UpdatePeticionValidacion to GesPetUsr
go

print 'Actualización realizada.'
go
