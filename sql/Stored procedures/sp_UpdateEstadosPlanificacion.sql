/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateEstadosPlanificacion'
go

if exists (select * from sysobjects where name = 'sp_UpdateEstadosPlanificacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateEstadosPlanificacion
go

create procedure dbo.sp_UpdateEstadosPlanificacion
	@cod_estado_plan	int,
	@nom_estado_plan	char(50),
	@estado_hab			char(1)
as
	update 
		GesPet.dbo.EstadosPlanificacion
	set
		nom_estado_plan = @nom_estado_plan,
		estado_hab		= @estado_hab
	where 
		(cod_estado_plan = @cod_estado_plan or @cod_estado_plan is null)
	return(0)
go

grant execute on dbo.sp_UpdateEstadosPlanificacion to GesPetUsr
go

print 'Actualización realizada.'
