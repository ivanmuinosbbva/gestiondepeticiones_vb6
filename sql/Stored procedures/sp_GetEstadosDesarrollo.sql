/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetEstadosDesarrollo'
go

if exists (select * from sysobjects where name = 'sp_GetEstadosDesarrollo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetEstadosDesarrollo
go

create procedure dbo.sp_GetEstadosDesarrollo
	@cod_estado_desa	int=null,
	@estado_hab			char(1)=null
as
	select 
		a.cod_estado_desa,
		a.nom_estado_desa,
		a.estado_hab
	from 
		GesPet.dbo.EstadosDesarrollo a
	where
		(a.cod_estado_desa = @cod_estado_desa or @cod_estado_desa is null) and
		(a.estado_hab = @estado_hab or @estado_hab is null)
	return(0)
go

grant execute on dbo.sp_GetEstadosDesarrollo to GesPetUsr
go

print 'Actualización realizada.'
