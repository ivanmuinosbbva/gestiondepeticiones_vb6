/*
-001- a. FJS 12.07.2007 - Se hace un join a la tabla de peticiones para poder agregar al conjunto de resultados actual, la 
					      fecha de envio al BP, el indicador de impacto tecnol�gico y la clase de la petici�n. Esto es por 
					      el punto 6 del documento CGM - Controles SOX 2007 / Alcance del desarrollo previsto - v8.0
-002- a. FJS 09.05.2008 - Se agregan nuevos campos a devolver agregados por el tema de Homologaci�n.
-003- a. FJS 12.08.2008 - Se agrega una columna para indicar si el grupo es homologable o es el grupo homologador.
-004- a. FJS 08.01.2009 - Se optimiza para aprovechar el nuevo �ndice 'idx_PeticionGrupo'.
-005- a. FJS 08.01.2015 - Se agrega campo para obtener el responsable del �rea.
-006- a. FJS 04.06.2015 - Nuevo: se agrega el legajo del responsable del grupo.

*/

use GesPet
go

print 'Actualizando procedimiento almacenado: sp_GetPeticionGrupo'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionGrupo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionGrupo
go

create procedure dbo.sp_GetPeticionGrupo 
    @pet_nrointerno     int=0, 
    @cod_sector         char(8)=null, 
    @cod_grupo          char(8)=null 
as 
    select  
        PET.pet_nrointerno, 
        PET.cod_grupo, 
        PET.cod_sector, 
        PET.cod_gerencia, 
        PET.cod_direccion, 
        PET.fe_ini_plan, 
        PET.fe_fin_plan, 
        PET.fe_ini_real, 
        PET.fe_fin_real, 
        PET.horaspresup, 
        PET.cod_estado, 
        PET.fe_estado, 
        PET.cod_situacion, 
		--{ add -001-
		A.fe_comite,
		A.pet_imptech,
		A.cod_clase,
		--}
        PET.hst_nrointerno_sol, 
        PET.hst_nrointerno_rsp, 
        nom_grupo=(select nom_grupo from GesPet..Grupo where RTRIM(cod_grupo)=RTRIM(PET.cod_grupo)) , 
        nom_estado=(select nom_estado from GesPet..Estados where RTRIM(cod_estado)=RTRIM(PET.cod_estado)) , 
        nom_situacion=(select nom_situacion from GesPet..Situaciones where RTRIM(cod_situacion)=RTRIM(PET.cod_situacion))
	   ,homo = (select grupo_homologacion from GesPet..Grupo where cod_grupo = PET.cod_grupo)	-- add -003- a.
		--{ add -002- a.
	   ,PET.fe_produccion,
	    PET.fe_suspension,
		PET.cod_motivo,
		nom_motivo=(select nom_motivo from GesPet..Motivos where cod_motivo=PET.cod_motivo)
		--}
		--{ add -005- a.
		,cod_responsable = isnull((select x.cod_recurso from Recurso x where x.cod_grupo = PET.cod_grupo and x.estado_recurso = 'A' and x.flg_cargoarea = 'S'),'')										-- add -006- a.
		,nom_responsable = isnull((select x.nom_recurso from Recurso x where x.cod_grupo = PET.cod_grupo and x.estado_recurso = 'A' and x.flg_cargoarea = 'S'),'*** SIN RESPONSABLE A CARGO ***')		-- add -005- a.
		,g.grupo_homologacion
		,perfil = (case
			when g.grupo_homologacion = 'H' then 'Homologaci�n'
			when g.grupo_homologacion = 'S' then 'T�cnico'
			when g.grupo_homologacion = 'N' then 'Funcional'
			when g.grupo_homologacion = '1' then 'Seguridad inform�tica'
			when g.grupo_homologacion = '2' then 'Tecnolog�a'
			else 'Otro'
		end)
		--}
		,PET.prio_ejecuc
		,cod_estado_default = (select e.default_est from Estados e where e.cod_estado = PET.cod_estado)
	from 
        GesPet..PeticionGrupo PET inner join 
		GesPet..Grupo g on (g.cod_grupo = PET.cod_grupo) inner join 
		GesPet..Peticion A on (A.pet_nrointerno = PET.pet_nrointerno)	-- add -001- a.
    where   
        (@pet_nrointerno = 0 or @pet_nrointerno = PET.pet_nrointerno) and 
        (@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(PET.cod_sector) = RTRIM(@cod_sector)) and 
        (@cod_grupo is null or RTRIM(@cod_grupo) is null or RTRIM(@cod_grupo)='' or RTRIM(PET.cod_grupo) = RTRIM(@cod_grupo))  
    order by 
		PET.pet_nrointerno, 
		PET.cod_sector, 
		PET.cod_grupo		-- upd -004- a.
return(0) 
go

grant execute on dbo.sp_GetPeticionGrupo to GesPetUsr 
go

print 'Actualizaci�n finalizada.'
go