/*
-000- a. FJS 24.11.2008 - Nuevo SP para agregar nuevos Tips a la tabla.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertTips'
go

if exists (select * from sysobjects where name = 'sp_InsertTips' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertTips
go

create procedure dbo.sp_InsertTips
	@tips_version	char(10)=null,
	@tips_tipo		char(6)=null,
	@tips_nro		int=null,
	@tips_rng		int=null,
	@tips_txt		varchar(255)=null,
	@tips_date		smalldatetime=null,
	@tips_hab		char(1)=null
as
	insert into GesPet.dbo.Tips (
		tips_version,
		tips_tipo,
		tips_nro,
		tips_rng,
		tips_txt,
		tips_date,
		tips_hab)
	values (
		@tips_version,
		@tips_tipo,
		@tips_nro,
		@tips_rng,
		@tips_txt,
		@tips_date,
		@tips_hab)
go

grant execute on dbo.sp_InsertTips to GesPetUsr
go

print 'Actualización realizada.'
