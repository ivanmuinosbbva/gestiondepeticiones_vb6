/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 12.11.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateHEDT013Field'
go

if exists (select * from sysobjects where name = 'sp_UpdateHEDT013Field' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateHEDT013Field
go

create procedure dbo.sp_UpdateHEDT013Field
	@tipo_id	int,
	@subtipo_id	int,
	@item	int,
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as

if rtrim(@campo)='TIPO_ID'
	update 
		GesPet.dbo.HEDT013
	set
		tipo_id = @valortxt
	where 
		(tipo_id = @tipo_id or @tipo_id is null) and

		(subtipo_id = @subtipo_id or @subtipo_id is null) and

		(item = @item or @item is null)

if rtrim(@campo)='SUBTIPO_ID'
	update 
		GesPet.dbo.HEDT013
	set
		subtipo_id = @valortxt
	where 
		(tipo_id = @tipo_id or @tipo_id is null) and

		(subtipo_id = @subtipo_id or @subtipo_id is null) and

		(item = @item or @item is null)

if rtrim(@campo)='ITEM'
	update 
		GesPet.dbo.HEDT013
	set
		item = @valornum
	where 
		(tipo_id = @tipo_id or @tipo_id is null) and

		(subtipo_id = @subtipo_id or @subtipo_id is null) and

		(item = @item or @item is null)

if rtrim(@campo)='ITEM_NOM'
	update 
		GesPet.dbo.HEDT013
	set
		item_nom = @valortxt
	where 
		(tipo_id = @tipo_id or @tipo_id is null) and

		(subtipo_id = @subtipo_id or @subtipo_id is null) and

		(item = @item or @item is null)

go

grant execute on dbo.sp_UpdateHEDT013Field to GesPetUsr
go

print 'Actualización realizada.'
