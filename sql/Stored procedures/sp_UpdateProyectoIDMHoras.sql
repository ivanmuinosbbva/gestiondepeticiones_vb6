/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.01.2013 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoIDMHoras'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoIDMHoras' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoIDMHoras
go

create procedure dbo.sp_UpdateProyectoIDMHoras
	@ProjId		int,
	@ProjSubId	int,
	@ProjSubSId	int,
	@cod_nivel	char(3),
	@cod_area	char(8),
	@cant_horas	int
as
	update 
		GesPet.dbo.ProyectoIDMHoras
	set
		cant_horas	= @cant_horas
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		cod_nivel = @cod_nivel and 
		cod_area  = @cod_area
go

grant execute on dbo.sp_UpdateProyectoIDMHoras to GesPetUsr
go

print 'Actualización realizada.'
go
