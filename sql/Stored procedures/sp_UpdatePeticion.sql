use GesPet
go

print 'Actualizando procedimiento almacenado: sp_UpdatePeticion'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticion' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticion
go

create procedure dbo.sp_UpdatePeticion
    @pet_nrointerno     int,
    @pet_nroasignado    int,
    @titulo             varchar(120),
    @cod_tipo_peticion  char(3),
    @prioridad          char(1),
    @corp_local			char(1)='',
    @cod_orientacion	char(2)='',
    @importancia_cod	char(2)='',
    @importancia_prf	char(4)='',
    @importancia_usr	char(10)='',
    @pet_nroanexada     int,
    @prj_nrointerno     int,
    @fe_pedido          smalldatetime=null,
    @fe_requerida       smalldatetime=null,
    @fe_comite          smalldatetime=null,
    @fe_ini_plan        smalldatetime=null,
    @fe_fin_plan        smalldatetime=null,
    @fe_ini_real        smalldatetime=null,
    @fe_fin_real        smalldatetime=null,
    @horaspresup        smallint,
    @cod_direccion      char(8) ,
    @cod_gerencia       char(8) ,
    @cod_sector         char(8) ,
    @cod_usualta        char(10),
    @cod_solicitante    char(10),
    @cod_referente      char(10),
    @cod_supervisor     char(10),
    @cod_director       char(10),
    @cod_estado         char(6) ,
    @fe_estado			smalldatetime=null,
    @cod_situacion      char(6),
    @cod_bpar			char(10)=null,
    @cod_clase			char(4)=null,
    @pet_imptech		char(1)=null,
    @pet_sox001			tinyint,
	@pet_regulatorio	char(1)='X',
	@projid				int=null,
	@projsubid			int=null,
	@projsubsid			int=null,
	@pet_emp			int=null,
	@pet_ro				char(1),
	@cod_BPE			char(10),
	@fecha_BPE			smalldatetime,
	@driverBPE			char(8),
	@cargaBeneficios	char(1)
as
	declare @proxnumero			int
	declare @p_cod_estado		char(6)
	declare @old_estado			char(6)
	declare @tipo_proyecto		char(1)
	declare @clase				char(4)

	select  @proxnumero = 0

	if @pet_nrointerno = 0
		begin
			execute sp_ProximoNumero 'ULPETINT', @proxnumero OUTPUT
			if exists (select pet_nrointerno from GesPet..Peticion where pet_nrointerno = @proxnumero)
				begin
					select 30010 as ErrCode , 'Error al generar numero interno' as ErrDesc
					raiserror 30010 'Error al generar numero interno'
					return (30010)
				end
		end
	else
		begin
			select @proxnumero = @pet_nrointerno
		end

	if exists (select pet_nrointerno from GesPet..Peticion where pet_nrointerno = @proxnumero)
		begin
			select @clase = cod_clase
			from Peticion
			where pet_nrointerno = @proxnumero

			IF (@clase = 'SINC' and charindex(@cod_clase, 'NUEV|EVOL|')>0) 
				BEGIN
					INSERT INTO PeticionValidacion  
					SELECT @proxnumero, valid, valitem, null, null, null
					FROM Validacion
					WHERE valhab = 'S'
					ORDER BY valorden
				END

			update 
				GesPet..Peticion 
			set
				pet_nroasignado		= @pet_nroasignado,
				titulo				= @titulo,
				cod_tipo_peticion	= @cod_tipo_peticion,
				prioridad			= @prioridad,
				pet_nroanexada		= @pet_nroanexada,
				prj_nrointerno		= @prj_nrointerno,
				fe_pedido			= @fe_pedido,
				fe_requerida		= @fe_requerida,
				fe_comite			= @fe_comite,
				fe_ini_plan			= @fe_ini_plan,
				fe_fin_plan			= @fe_fin_plan,
				fe_ini_real			= @fe_ini_real,
				fe_fin_real			= @fe_fin_real,
				horaspresup			= @horaspresup,
				cod_direccion		= @cod_direccion,
				cod_gerencia		= @cod_gerencia,
				cod_sector			= @cod_sector,
				cod_usualta			= @cod_usualta,
				cod_solicitante		= @cod_solicitante,
				cod_referente		= @cod_referente,
				cod_supervisor		= @cod_supervisor,
				cod_director		= @cod_director,
				cod_estado			= @cod_estado,
				fe_estado			= @fe_estado,
				cod_situacion		= @cod_situacion,
				cod_bpar			= @cod_bpar,
				ult_accion			= '',
				corp_local			= @corp_local,
				cod_orientacion		= @cod_orientacion,
				importancia_cod		= @importancia_cod,
				importancia_prf		= @importancia_prf,
				importancia_usr		= @importancia_usr,
				audit_user			= SUSER_NAME(),
				pet_sox001			= @pet_sox001,
				audit_date			= getdate(),
				cod_clase			= @cod_clase,
				pet_imptech			= @pet_imptech,
				pet_regulatorio		= @pet_regulatorio,
				pet_projid			= @projid,
				pet_projsubid		= @projsubid,
				pet_projsubsid		= @projsubsid,
				pet_emp				= @pet_emp,
				pet_ro				= @pet_ro,
				cod_BPE				= @cod_BPE,
				fecha_BPE			= @fecha_BPE,
				pet_driver			= @driverBPE,
				cargaBeneficios		= @cargaBeneficios
			where  
				pet_nrointerno = @proxnumero
		end
	else
		begin
			insert into GesPet..Peticion (
				pet_nrointerno,
				pet_nroasignado,
				titulo,
				cod_tipo_peticion,
				prioridad,
				pet_nroanexada,
				prj_nrointerno,
				fe_pedido,
				fe_requerida,
				fe_comite,
				fe_ini_plan,
				fe_fin_plan,
				fe_ini_real,
				fe_fin_real,
				horaspresup,
				cant_planif,
				cod_direccion,
				cod_gerencia,
				cod_sector,
				cod_usualta,
				cod_solicitante,
				cod_referente,
				cod_supervisor,
				cod_director,
				cod_bpar,
				cod_estado,
				fe_estado,
				cod_situacion,
				ult_accion,
				corp_local,
				cod_orientacion,
				importancia_cod,
				importancia_prf,
				importancia_usr,
				audit_user,
				audit_date,
				pet_sox001,
				cod_clase,
				pet_imptech,
				pet_regulatorio,
				pet_projid,
				pet_projsubid,
				pet_projsubsid,
				pet_emp,
				pet_ro,
				cod_BPE,
				fecha_BPE,
				pet_driver,
				cargaBeneficios)
			 values (
				@proxnumero,
				@pet_nroasignado,
				@titulo,
				@cod_tipo_peticion,
				@prioridad,
				@pet_nroanexada,
				@prj_nrointerno,
				@fe_pedido,
				@fe_requerida,
				@fe_comite,
				@fe_ini_plan,
				@fe_fin_plan,
				@fe_ini_real,
				@fe_fin_real,
				@horaspresup,
				0,
				@cod_direccion,
				@cod_gerencia,
				@cod_sector,
				@cod_usualta,
				@cod_solicitante,
				@cod_referente,
				@cod_supervisor,
				@cod_director,
				@cod_bpar,
				@cod_estado,
				getdate(),
				@cod_situacion,
				'',
				@corp_local,
				@cod_orientacion,
				@importancia_cod,
				@importancia_prf,
				@importancia_usr,
				SUSER_NAME(),
				getdate(),
				@pet_sox001,
				@cod_clase,
				@pet_imptech,
				@pet_regulatorio,
				@projid,
				@projsubid,
				@projsubsid,
				@pet_emp,
				@pet_ro,
				@cod_BPE,
				@fecha_BPE,
				@driverBPE,
				@cargaBeneficios)
		end

		if exists(
			select ProjId
			from GesPet..ProyectoIDM where ProjId = @projid and ProjSubId = @projsubid and ProjSubSId = @projsubsid)

			select @tipo_proyecto = tipo_proyecto, @old_estado	= cod_estado
			from GesPet..ProyectoIDM where ProjId = @projid and ProjSubId = @projsubid and ProjSubSId = @projsubsid

			begin
				if charindex(@old_estado,'DESEST|CANCEL|TERMIN|')=0 and @tipo_proyecto = 'P'
					begin
						exec sp_GetEstadoProyectoIDM @projid, @projsubid, @projsubsid, @p_cod_estado output
						update 
							GesPet..ProyectoIDM
						set
							cod_estado = @p_cod_estado,
							fch_estado = getdate(),
							usr_estado = SUSER_NAME()
						from 
							GesPet..Peticion
						where
							ProjId = @projid and 
							ProjSubId = @projsubid and 
							ProjSubSId = @projsubsid
					end
			end

	select @proxnumero  as valor_clave
	return(0)
go

sp_procxmode 'sp_UpdatePeticion', anymode
go

grant execute on dbo.sp_UpdatePeticion to GesPetUsr
go

grant execute on dbo.sp_UpdatePeticion to RolGesIncConex
go

print 'Actualización finalizada.'
go