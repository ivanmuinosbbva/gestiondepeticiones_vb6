use GesPet
go
print 'sp_DeleteDireccion'
go
if exists (select * from sysobjects where name = 'sp_DeleteDireccion' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteDireccion
go
create procedure dbo.sp_DeleteDireccion 
           @cod_direccion       char(8) 
as 
 
if exists (select cod_direccion from GesPet..Direccion where RTRIM(cod_direccion) = RTRIM(@cod_direccion)) 
begin 
    if exists (select cod_direccion from GesPet..Gerencia where RTRIM(cod_direccion) = RTRIM(@cod_direccion)) 
    begin 
        select 30010 as ErrCode , 'Existen Gerencias asociadas a esta Direccion' as ErrDesc 
        raiserror  30010 'Existen Gerencias asociadas a esta Direccion'  
    return (30010) 
    end 
 
    if exists (select cod_direccion from GesPet..Recurso where RTRIM(cod_direccion) = RTRIM(@cod_direccion)) 
    begin 
        raiserror  30011 'Existen Recursos asociados a esta Direccion'  
        select 30011 as ErrCode , 'Existen Recursos asociados a esta Direccion'  as ErrDesc 
    return (30011) 
    end 
 
    if exists (select cod_recurso from GesPet..RecursoPerfil where RTRIM(cod_nivel) = 'DIRE' and RTRIM(cod_area) = RTRIM(@cod_direccion)) 
    begin 
        raiserror  30011 'Existen Perfiles asociados a esta Direccion'  
        select 30011 as ErrCode , 'Existen Perfiles asociados a esta Direccion'  as ErrDesc 
    return (30011) 
    end 
 
    if exists (select cod_direccion from GesPet..Peticion where RTRIM(cod_direccion) = RTRIM(@cod_direccion)) 
    begin 
        raiserror  30011 'Existen Peticiones asociadas a esta Direccion'  
        select 30011 as ErrCode , 'Existen Peticiones asociadas a esta Direccion'  as ErrDesc 
    return (30011) 
    end 
 
    if exists (select cod_direccion from GesPet..PeticionSector where RTRIM(cod_direccion) = RTRIM(@cod_direccion)) 
    begin 
        raiserror  30011 'Existen Peticiones asociadas a esta Direccion'  
        select 30011 as ErrCode , 'Existen Peticiones asociadas a esta Direccion'  as ErrDesc 
    return (30011) 
    end 
 
    delete GesPet..Direccion 
      where RTRIM(cod_direccion) = RTRIM(@cod_direccion) 
end 
else 
begin 
    raiserror 30001 'Direccion Inexistente'   
    select 30001 as ErrCode , 'Direccion Inexistente' as ErrDesc   
    return (30001)   
end 
return(0) 
go



grant execute on dbo.sp_DeleteDireccion to GesPetUsr 
go


