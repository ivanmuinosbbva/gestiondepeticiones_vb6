/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.07.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteCatalogoTabla'
go

if exists (select * from sysobjects where name = 'sp_DeleteCatalogoTabla' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteCatalogoTabla
go

create procedure dbo.sp_DeleteCatalogoTabla
	@id	int
as
	delete from
		GesPet.dbo.CatalogoTabla
	where
		id = @id
go

grant execute on dbo.sp_DeleteCatalogoTabla to GesPetUsr
go
grant execute on dbo.sp_DeleteCatalogoTabla to TrnCGMEnmascara
go
sp_procxmode 'dbo.sp_DeleteCatalogoTabla','anymode'
go

print 'Actualización realizada.'
go
