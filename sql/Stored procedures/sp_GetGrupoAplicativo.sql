/*
-000- a. FJS 12.05.2009 - Nuevo SP para manejo de Aplicativos por Grupo ejecutor
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetGrupoAplicativo'
go

if exists (select * from sysobjects where name = 'sp_GetGrupoAplicativo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetGrupoAplicativo
go

create procedure dbo.sp_GetGrupoAplicativo
	@cod_grupo	char(8)=null,
	@app_id		char(30)=null
as
	select 
		a.cod_grupo,
		c.nom_grupo,
		a.app_id,
		b.app_nombre,
		b.app_abrev
	from 
		GesPet..GrupoAplicativo a inner join GesPet..Aplicativo b on a.app_id = b.app_id inner join GesPet..Grupo c on a.cod_grupo = c.cod_grupo
	where
		(a.cod_grupo = @cod_grupo or @cod_grupo is null) and
		(a.app_id = @app_id or @app_id is null)
go

grant execute on dbo.sp_GetGrupoAplicativo to GesPetUsr
go

print 'Actualización realizada.'
