use GesPet
go
print 'sp_HistorialIDMProximo'
go
if exists (select * from sysobjects where name = 'sp_HistorialIDMProximo' and sysstat & 7 = 4)
drop procedure dbo.sp_HistorialIDMProximo
go
create procedure dbo.sp_HistorialIDMProximo 
             @var_numero    int OUTPUT 
as 
select @var_numero  = 0 
 
if exists ( select var_numero 
   from   GesPet..Varios 
   where  var_codigo = 'ULTHIST2') 
 begin 
  select @var_numero=var_numero 
   from   GesPet..Varios 
   where  var_codigo = 'ULTHIST2' 
  select @var_numero = @var_numero + 1 
  update GesPet..Varios 
      set    var_numero = @var_numero, 
             var_texto = '' 
      where  var_codigo = 'ULTHIST2' 
 end 
 else 
 begin 
  select @var_numero = 1 
  insert into GesPet..Varios 
         ( var_codigo, var_numero, var_texto) 
      values 
         ( 'ULTHIST2', 1, '') 
 end 
return(0) 
go



grant execute on dbo.sp_HistorialIDMProximo to GesPetUsr 
go


