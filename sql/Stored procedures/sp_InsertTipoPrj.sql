use GesPet
go
print 'sp_InsertTipoPrj'
go
if exists (select * from sysobjects where name = 'sp_InsertTipoPrj' and sysstat & 7 = 4)
drop procedure dbo.sp_InsertTipoPrj
go
create procedure dbo.sp_InsertTipoPrj
	@cod_tipoprj	char(8)=null,
	@nom_tipoprj	char(30)=null,
	@flg_habil		char(1)=null,
	@secuencia		int = 1


as

declare @old_secuencia int  
select  @old_secuencia = BSR.secuencia  
	from  TipoPrj BSR
	where 	RTRIM(cod_tipoprj)<>RTRIM(@cod_tipoprj)

if @old_secuencia = @secuencia 
	begin 
		raiserror 30011 'Ya existe esa secuencia para ese elemento'   
		select 30011 as ErrCode , 'Ya existe esa secuencia para ese elemento' as ErrDesc   
		return (30011)   
	end 


if exists (select cod_tipoprj from TipoPrj where RTRIM(cod_tipoprj)=RTRIM(@cod_tipoprj))
	begin 
		raiserror 30011 'Codigo TipoPrj Existente'   
		select 30011 as ErrCode , 'Codigo de TipoPrj Existente' as ErrDesc   
		return (30011)   
	end 

	insert into GesPet..TipoPrj
		(	cod_tipoprj,
		nom_tipoprj, 
		flg_habil,
		secuencia)
	values
		(@cod_tipoprj, 
		@nom_tipoprj,
		@flg_habil,
		@secuencia)

return(0)
go


grant execute on dbo.sp_InsertTipoPrj to GesPetUsr
go

