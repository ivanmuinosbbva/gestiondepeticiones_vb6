/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 02.10.2009 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateHEDT002Field'
go

if exists (select * from sysobjects where name = 'sp_UpdateHEDT002Field' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateHEDT002Field
go

create procedure dbo.sp_UpdateHEDT002Field
	@dsn_id	char(8),
	@cpy_id	char(8),
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as

if rtrim(@campo)='DSN_ID'
	update 
		GesPet.dbo.HEDT002
	set
		dsn_id = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null)

if rtrim(@campo)='CPY_ID'
	update 
		GesPet.dbo.HEDT002
	set
		cpy_id = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null)

if rtrim(@campo)='CPY_DSC'
	update 
		GesPet.dbo.HEDT002
	set
		cpy_dsc = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null)

if rtrim(@campo)='CPY_NOMRUT'
	update 
		GesPet.dbo.HEDT002
	set
		cpy_nomrut = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null)

if rtrim(@campo)='CPY_FEULT'
	update 
		GesPet.dbo.HEDT002
	set
		cpy_feult = @valordate
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null)

if rtrim(@campo)='CPY_USERID'
	update 
		GesPet.dbo.HEDT002
	set
		cpy_userid = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null)
if rtrim(@campo)='ESTADO'
	update 
		GesPet.dbo.HEDT002
	set
		estado = @valortxt
	where 
		(dsn_id = @dsn_id or @dsn_id is null) and
		(cpy_id = @cpy_id or @cpy_id is null)
go

grant execute on dbo.sp_UpdateHEDT002Field to GesPetUsr
go

print 'Actualización realizada.'
