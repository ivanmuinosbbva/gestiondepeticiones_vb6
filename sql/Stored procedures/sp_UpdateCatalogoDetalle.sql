/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.07.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateCatalogoDetalle'
go

if exists (select * from sysobjects where name = 'sp_UpdateCatalogoDetalle' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateCatalogoDetalle
go

create procedure dbo.sp_UpdateCatalogoDetalle
	@id	int=null,
	@campo		varchar(50)=null,
	@pos_desde	int=null,
	@longitud	int=null,
	@tipo_id	char(1)=null,
	@subtipo_id	char(1)=null,
	@rutina		char(8)=null
as
	update 
		GesPet.dbo.CatalogoDetalle
	set
		pos_desde	= @pos_desde,
		longitud	= @longitud,
		tipo_id		= @tipo_id,
		subtipo_id	= @subtipo_id,
		rutina		= @rutina
	where 
		id = @id and
		campo = LTRIM(RTRIM(@campo))
go

grant execute on dbo.sp_UpdateCatalogoDetalle to GesPetUsr
go

print 'Actualización realizada.'
go
