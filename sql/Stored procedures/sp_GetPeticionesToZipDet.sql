/*
-000- a. FJS 14.07.2011 - Nuevo SP para devolver únicamente los objetos incrustados (BLOB) de una petición
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionesToZipDet'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionesToZipDet' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionesToZipDet
go

create procedure dbo.sp_GetPeticionesToZipDet
	@pet_nrointerno	int,
	@adj_tipo		char(8),
	@adj_file		char(100)
as 
	set chained off
	select 
		pa.pet_nrointerno,
		pa.adj_tipo,
		pa.adj_file,
		pa.adj_objeto
	from 
		GesPet..PeticionAdjunto pa 
	where
		pa.pet_nrointerno = @pet_nrointerno and
		pa.adj_tipo = @adj_tipo and
		pa.adj_file = @adj_file
	return(0)
go

grant execute on dbo.sp_GetPeticionesToZipDet to GesPetUsr
go

exec sp_procxmode 'sp_GetPeticionesToZipDet', anymode
go

print 'Actualización realizada.'
go
