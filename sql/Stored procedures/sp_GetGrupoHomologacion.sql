/*
-000- a. FJS 27.02.2008 - Nuevo SP para obtener el grupo Homologador.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetGrupoHomologacion'
go

if exists (select * from sysobjects where name = 'sp_GetGrupoHomologacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetGrupoHomologacion
go

create procedure dbo.sp_GetGrupoHomologacion
	@tipo	char(1)=null
as 
    select
        g.cod_grupo,
        g.nom_grupo,
        g.cod_sector,
		s.nom_sector,
        g.flg_habil,
        g.es_ejecutor,
        g.cod_bpar,
        g.grupo_homologacion,
		nom_responsable = ISNULL((
			select r.nom_recurso 
			from Recurso r 
			where 
				r.flg_cargoarea = 'S' and
				r.cod_sector = g.cod_sector and 
				r.cod_grupo = g.cod_grupo), '-')
    from
        Grupo g inner join
		Sector s on (g.cod_sector = s.cod_sector)
    where
        --G.grupo_homologacion = 'H'
		((@tipo is null and g.grupo_homologacion = 'H') or g.grupo_homologacion = @tipo)
return(0) 
go

grant execute on dbo.sp_GetGrupoHomologacion to GesPetUsr
go

print 'Actualización realizada.'
go

