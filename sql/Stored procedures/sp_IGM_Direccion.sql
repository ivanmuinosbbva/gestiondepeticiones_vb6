/*
Collation Name para el order: "Latin-1 Spanish no case (56)" (Sortkey)

-000- a. FJS 20.05.2010 - Nuevo SP para el sistema IGM: Direcciones en CGM
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_Direccion'
go

if exists (select * from sysobjects where name = 'sp_IGM_Direccion' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_Direccion
go

create procedure dbo.sp_IGM_Direccion
	@cod_direccion	char(8)=null, 
	@habil			char(1)=null
as 
	select
		a.cod_direccion as 'codigo',
		a.nom_direccion as 'nombre',
		a.flg_habil as 'habilitado'
	from
		GesPet..Direccion a
	where
		(@cod_direccion is null or a.cod_direccion = @cod_direccion) and
		(@habil is null or a.flg_habil = @habil)
	order by
		sortkey(a.nom_direccion,56)
	return(0)
go

grant execute on dbo.sp_IGM_Direccion to GesPetUsr
go

grant execute on dbo.sp_IGM_Direccion to GrpTrnIGM
go

print 'Actualización realizada.'
go 