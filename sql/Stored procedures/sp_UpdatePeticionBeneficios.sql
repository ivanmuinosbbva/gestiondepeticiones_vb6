/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 17.02.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionBeneficios'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionBeneficios' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionBeneficios
go

create procedure dbo.sp_UpdatePeticionBeneficios
	@pet_nrointerno	int,
	@indicadorId	int,
	@subindicadorId	int,
	@valor			int
as
	if not exists (
		select pet_nrointerno 
		from GesPet..PeticionBeneficios 
		where 
			pet_nrointerno = @pet_nrointerno and
			indicadorId = @indicadorId and
			subindicadorId = @subindicadorId)
		
		INSERT INTO PeticionBeneficios 
		VALUES (@pet_nrointerno, @indicadorId, @subindicadorId, @valor, getdate(), SUSER_NAME())
	else
		update 
			GesPet..PeticionBeneficios
		set
			valor		= @valor,
			audit_fe	= getdate(),
			audit_usr	= SUSER_NAME()
		where 
			pet_nrointerno	= @pet_nrointerno and
			indicadorId		= @indicadorId and
			subindicadorId	= @subindicadorId

go

grant execute on dbo.sp_UpdatePeticionBeneficios to GesPetUsr
go

print 'Actualización realizada.'
go
