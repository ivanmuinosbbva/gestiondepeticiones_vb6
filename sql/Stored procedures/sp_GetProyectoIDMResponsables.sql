/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.16 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.05.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMResponsables'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMResponsables' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMResponsables
go

create procedure dbo.sp_GetProyectoIDMResponsables
	@ProjId			int=null,
	@ProjSubId		int=null,
	@ProjSubSId		int=null,
	@cod_nivel		char(4)=null,
	@cod_area		char(8)=null
as
	select 
		p.ProjId,
		p.ProjSubId,
		p.ProjSubSId,
		p.cod_nivel,
		p.cod_area,
		nom_area = (
			case
				when p.cod_nivel = 'DIRE' then 
					(select d.nom_direccion 
					from GesPet..Direccion d
					where d.cod_direccion = p.cod_area)
				when p.cod_nivel = 'GERE' then 
					(select d.nom_direccion + ' » ' + g.nom_gerencia 
					from 
						GesPet..Direccion d inner join
						GesPet..Gerencia g on (d.cod_direccion = g.cod_direccion)
					where g.cod_gerencia = p.cod_area)
				when p.cod_nivel = 'SECT' then
					(select d.nom_direccion + ' » ' + g.nom_gerencia + ' » ' + s.nom_sector
					from 
						GesPet..Direccion d inner join
						GesPet..Gerencia g on (d.cod_direccion = g.cod_direccion) inner join
						GesPet..Sector s on (g.cod_gerencia = s.cod_gerencia)
					where s.cod_sector = p.cod_area)
				when p.cod_nivel = 'GRUP' then
					(select d.nom_direccion + ' » ' + g.nom_gerencia + ' » ' + s.nom_sector + ' » ' + gr.nom_grupo
					from 
						GesPet..Direccion d inner join
						GesPet..Gerencia g on (d.cod_direccion = g.cod_direccion) inner join
						GesPet..Sector s on (g.cod_gerencia = s.cod_gerencia) inner join
						GesPet..Grupo gr on (s.cod_sector = gr.cod_sector)
					where gr.cod_grupo = p.cod_area)
			end),
		nom_responsable = (
			select 
				r.nom_recurso 
			from GesPet..Recurso r
			where
				((p.cod_nivel = 'DIRE' and r.cod_direccion = p.cod_area and r.cod_gerencia = '' and r.cod_sector = '' and r.cod_grupo = '') or
				 (p.cod_nivel = 'GERE' and r.cod_gerencia = p.cod_area and r.cod_sector = '' and r.cod_grupo = '') or 
				 (p.cod_nivel = 'SECT' and r.cod_sector = p.cod_area and r.cod_grupo = '') or 
				 (p.cod_nivel = 'GRUP' and r.cod_grupo =  p.cod_area)) and
				r.estado_recurso = 'A' and r.flg_cargoarea = 'S'),
		p.propietaria,
		dep_area = (case 
			when p.cod_nivel = 'GERE' then (
				select RTRIM(d.cod_direccion) + '/' + RTRIM(p.cod_area) 
				from GesPet..Gerencia d 
				where d.cod_gerencia = p.cod_area)
			when p.cod_nivel = 'SECT' then (
				select RTRIM(g.cod_direccion) + '/' + RTRIM(s.cod_gerencia) + '/' + RTRIM(p.cod_area)
				from GesPet..Sector s inner join
					 GesPet..Gerencia g on (s.cod_gerencia = g.cod_gerencia)
				where s.cod_sector = p.cod_area)
			when p.cod_nivel = 'GRUP' then (
				select RTRIM(ge.cod_direccion) + '/' + RTRIM(s.cod_gerencia) + '/' + RTRIM(g.cod_sector) + '/' + RTRIM(p.cod_area)
				from GesPet..Grupo g inner join
					 GesPet..Sector s on (g.cod_sector = s.cod_sector) inner join
					 GesPet..Gerencia ge on (s.cod_gerencia = ge.cod_gerencia)
				where g.cod_grupo = p.cod_area)
			else ''
		end),
		p.resp_user,
		p.resp_fecha
	from 
		GesPet.dbo.ProyectoIDMResponsables p
	where
		(@ProjId is null or p.ProjId = @ProjId) and
		(@ProjSubId is null or p.ProjSubId = @ProjSubId) and
		(@ProjSubSId is null or p.ProjSubSId = @ProjSubSId) and
		(@cod_nivel is null or p.cod_nivel = @cod_nivel) and
		(@cod_area is null or p.cod_area = @cod_area)
go

grant execute on dbo.sp_GetProyectoIDMResponsables to GesPetUsr
go

print 'Actualización realizada.'
go
