/*
*********************************************************************************************************************************
ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR
*********************************************************************************************************************************

-000- a. FJS 15.03.2010 - Nuevo SP para generar exportaci�n parcial de peticiones por Agrupamiento para la gente de Organizaci�n (Walter Salvi).
-001- a. FJS 15.04.2010 - Se modifica para soportar la misma funci�n sin cambiar la l�gica.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionesPorAgrup'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionesPorAgrup' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionesPorAgrup
go

create procedure dbo.sp_GetPeticionesPorAgrup
	@agr_nrointerno		int=null
as 

	-- Variables para el armado de las descripciones por petici�n
	declare	@pet_nrointerno		int
	declare @agr_nroasignado	int
	declare @cod_sector			char(8)
	declare @cod_grupo			char(8)
	declare	@horasResp			int
	declare	@horasRecu			int
	declare	@cont				int
	declare @ultimapos			int
	declare @titulo				char(50)
	declare @secuencia			int
	declare @ret_status			int
	declare @cantidad			int
	
	/*
	create table #PeticYHijo(  
		pet_nrointerno		int,  
		pet_nroasignado		int,  
		hij_cod_direccion	varchar(8),
		hij_cod_gerencia	varchar(8),
		hij_cod_sector		varchar(8),
		hij_cod_grupo		varchar(8))  
	*/
	  
	create table #AgrupXPetic(
		pet_nrointerno		int,
		agr_nrointerno		int) 

	create table #AgrupArbol(
		secuencia			int,  
		nivel				int,  
		agr_nrointerno		int)
	  
	begin  
		select	@secuencia = 0
		execute @ret_status = sp_GetAgrupArbol @agr_nrointerno, 0, @secuencia OUTPUT  
		if (@ret_status <> 0)  
			begin  
				if (@ret_status = 30003)
					begin  
						select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError  
						return (@ret_status)  
					end  
			end
		insert into #AgrupXPetic 
			(pet_nrointerno,
			 agr_nrointerno)
		select  
			AP.pet_nrointerno,
			AP.agr_nrointerno
		from 
			#AgrupArbol AA, 
			AgrupPetic AP, 
			Agrup AG  
		where 
			AA.agr_nrointerno = AP.agr_nrointerno and  
			AG.agr_nrointerno = AP.agr_nrointerno and  
			AG.agr_vigente = 'S'
	end

	create table #Peticiones (
		pet_empresa			int				null,
		pet_nrointerno		int				null,
		pet_nroasignado		int				null,
		pet_tipo			char(3)			null,
		pet_titulo			char(50)		null,
		pet_nom_estado		char(30)		null,
		pet_fe_ini_plan		char(10)		null,
		pet_fe_fin_plan		char(10)		null,
		pet_fe_ini_real		char(10)		null,
		pet_fe_fin_real		char(10)		null,
		pet_nroanexada		int				null,
		agr_nrointerno		int				null,
		agr_titulo			char(50)		null)
	/*
		Aqu� agrego el total de peticiones y sus datos principales
	*/
	insert into #Peticiones
		select
			1,
			PET.pet_nrointerno,
			PET.pet_nroasignado,
			PET.cod_tipo_peticion,
			PET.titulo,
			nom_estado = (select EST.nom_estado from GesPet..Estados EST where EST.cod_estado = PET.cod_estado),
			convert(char(10), PET.fe_ini_plan, 103),
			convert(char(10), PET.fe_fin_plan, 103),
			convert(char(10), PET.fe_ini_real, 103),
			convert(char(10), PET.fe_fin_real, 103),
			pet_nroanexada = (select x.pet_nroasignado from GesPet..Peticion x where x.pet_nrointerno = PET.pet_nroanexada and PET.pet_nroanexada <> 0),
			@agr_nrointerno,
			''
		from
			GesPet..Peticion PET inner join 
			#AgrupXPetic AXP on (AXP.pet_nrointerno = PET.pet_nrointerno) inner join 
			GesPet..Agrup AGR on (AXP.agr_nrointerno = AGR.agr_nrointerno)
		group by
			PET.pet_nrointerno,
			PET.pet_nroasignado,
			PET.cod_tipo_peticion,
			PET.titulo
		
		declare CursGrupo cursor for
		select
			pet_nrointerno
		from
			#Peticiones
		group by
			pet_nrointerno
		for read only

		open CursGrupo fetch CursGrupo into @pet_nrointerno
			while (@@sqlstatus = 0) 
			begin
				select @cantidad = 0
				select @cantidad = (select count(*) from GesPet..AgrupPetic c where c.pet_nrointerno = @pet_nrointerno)
				if @cantidad = 1
					begin
						select @agr_nroasignado = (select c.agr_nrointerno from GesPet..AgrupPetic c where c.pet_nrointerno = @pet_nrointerno)
						select @titulo = (select d.agr_titulo from GesPet..AgrupPetic c inner join Agrup d on c.agr_nrointerno = d.agr_nrointerno where c.pet_nrointerno = @pet_nrointerno)
						update #Peticiones
							set agr_nrointerno = @agr_nroasignado, agr_titulo = @titulo
						where pet_nrointerno = @pet_nrointerno
					end
				--else
					--begin
						--execute @titulo = sp_GetAgrupPeticHijo @agr_nroasignado, @pet_nrointerno
						--update #Peticiones
						--	set agr_nrointerno = @agr_nroasignado, agr_titulo = @titulo
						--	where pet_nrointerno = @pet_nrointerno
					--end
				fetch CursGrupo into @pet_nrointerno
			end
		close CursGrupo
		deallocate cursor CursGrupo
		
		-- Muestro el resultado
		select
			pet_nrointerno,
			pet_nroasignado,
			pet_tipo,
			pet_titulo,
			pet_nom_estado,
			pet_fe_ini_plan,
			pet_fe_fin_plan,
			pet_fe_ini_real,
			pet_fe_fin_real,
			pet_nroanexada,
			agr_nrointerno,
			agr_titulo
		from
			#Peticiones
		order by
			pet_nroasignado

		drop table #Peticiones
		drop table #AgrupXPetic
		drop table #AgrupArbol
	return(0)
go

grant execute on dbo.sp_GetPeticionesPorAgrup to GesPetUsr
go

print 'Actualizaci�n realizada.'