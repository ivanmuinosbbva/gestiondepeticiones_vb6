/*
-000- a. FJS 20.08.2010 - Nuevo SP para habilitar los estados a contemplar en IGM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_ABM_EstadoCGM'
go

if exists (select * from sysobjects where name = 'sp_IGM_ABM_EstadoCGM' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_ABM_EstadoCGM
go

create procedure dbo.sp_IGM_ABM_EstadoCGM
	@cod_estado		char(6),
	@hab			char(1)
as 
	if exists (select cod_estado from GesPet..Estados where cod_estado=@cod_estado)
		begin
			update GesPet..Estados
			set IGM_hab = @hab
			where cod_estado = @cod_estado
			return(0)
		end
	else
		begin
			select 30010 as ErrCode, 'Error: No existe el c�digo de estado especificado.' as ErrDesc 
			raiserror 30010 'Error: No existe el c�digo de estado especificado.'
			return (30010) 
		end
go

grant execute on dbo.sp_IGM_ABM_EstadoCGM to GesPetUsr
go

grant execute on dbo.sp_IGM_ABM_EstadoCGM to GrpTrnIGM
go

print 'Actualizaci�n realizada.'
go