/*
-001- a. FJS 28.04.2009 - Se agrega la cl�usula ORDER BY.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPerfil'
go

if exists (select * from sysobjects where name = 'sp_GetPerfil' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPerfil
go

create procedure dbo.sp_GetPerfil 
	@cod_perfil		char(4)=null,
	@habilitado		char(1)=null,
	@visible		char(1)=null
as 
	begin 
		select  
			p.cod_perfil, 
			p.nom_perfil,
			p.habilitado,
			p.visible,
			p.orden
		from 
			GesPet..Perfil p
		where   
			(@cod_perfil is null or LTRIM(RTRIM(@cod_perfil)) = LTRIM(RTRIM(@cod_perfil))) and 
			(@habilitado is null or p.habilitado = @habilitado) and 
			(@visible is null or p.visible = @visible)
		order by
			p.orden,
			p.nom_perfil
	end 
	return(0) 
go

grant execute on dbo.sp_GetPerfil to GesPetUsr 
go

print 'Actualizaci�n realizada.'
go
