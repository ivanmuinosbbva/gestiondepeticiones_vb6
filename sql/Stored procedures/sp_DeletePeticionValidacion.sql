/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 22.12.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionValidacion'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionValidacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionValidacion
go

create procedure dbo.sp_DeletePeticionValidacion
	@pet_nrointerno		int,
	@valid				int,
	@valitem			int
as
	delete from
		GesPet.dbo.PeticionValidacion
	where
		pet_nrointerno = @pet_nrointerno and
		valid = @valid and
		valitem = @valitem
go

grant execute on dbo.sp_DeletePeticionValidacion to GesPetUsr
go

print 'Actualización realizada.'
go
