/*
-000- a. FJS 26.06.2009 - Nuevo SP para el manejo de solicitudes a Carga de M�quina
-001- a. FJS 27.04.2014 - Nuevo: se agrega campo de solicitud de EMErgencia.
-002- a. FJS 29.07.2014 - Nuevo: se agrega la tabla con las descripciones de los tipos de solicitud.

*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetSolicitudNro'
go

if exists (select * from sysobjects where name = 'sp_GetSolicitudNro' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetSolicitudNro
go

create procedure dbo.sp_GetSolicitudNro
	@sol_nroasignado	int=null
as
	select 
		a.sol_nroasignado,
		a.sol_tipo as sol_tipocod,
		sol_tipo = (select x.nom_tipo from GesPet..TipoSolicitud x where x.cod_tipo = a.sol_tipo),		-- add -002- a.
		/* { del -002- a.
		sol_tipo = 
		case
			when a.sol_tipo = '1' then 'XCOM'
			when a.sol_tipo = '2' then 'UNLO'
			when a.sol_tipo = '3' then 'TCOR'
			when a.sol_tipo = '4' then 'SORT'
			when a.sol_tipo = '5' then 'XCOM*'
			when a.sol_tipo = '6' then 'SORT*'
		end,
		} */
		a.sol_eme,			-- add -001- a.
		a.sol_mask,
		a.sol_fecha,
		a.sol_recurso,
		nom_recurso = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_recurso),
		mail_recurso = (select x.email from GesPet..Recurso x where x.cod_recurso = a.sol_recurso),
		a.pet_nrointerno,
		pet_nroasignado = (select x.pet_nroasignado from GesPet..Peticion x where x.pet_nrointerno = a.pet_nrointerno),
		a.sol_resp_ejec,
		nom_resp_ejec = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_resp_ejec),
		mail_resp_ejec = (select x.email from GesPet..Recurso x where x.cod_recurso = a.sol_resp_ejec),
		a.sol_resp_sect,
		nom_resp_sect = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_resp_sect),
		mail_resp_sect = (select x.email from GesPet..Recurso x where x.cod_recurso = a.sol_resp_sect),
		a.sol_seguridad,
		nom_sol_seguridad = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_seguridad),
		a.jus_codigo,
		jus_desc = (select x.jus_descripcion from GesPet..Justificativos x where x.jus_codigo = a.jus_codigo),
		a.sol_texto,
		a.sol_fe_auto1,
		a.sol_fe_auto2,
		a.sol_fe_auto3,
		a.sol_file_prod,
		a.sol_file_desa,
		a.sol_file_out,
		file_prod = (case
			when a.sol_tipo = '2' then '-'
			when a.sol_tipo = '3' then a.sol_nrotabla
			else a.sol_file_prod
		end),
		a.sol_file_desa as file_desa,
		/*
		file_prod = (case
			when a.sol_tipo in ('2','3') then a.sol_file_out
			else a.sol_file_prod
		end),
		file_desa = (case
			when a.sol_tipo in ('2','3') then '-'
			when a.sol_tipo = '4' then a.sol_file_out
			else a.sol_file_desa
		end),
		*/
		a.sol_lib_Sysin,
		a.sol_mem_Sysin,
		a.sol_join,
		a.sol_nrotabla,
		a.fe_formato,
		a.sol_estado,
		nom_estado = (select x.nom_estado from GesPet..EstadoSolicitud x where x.cod_estado = a.sol_estado),		-- add -001- a.
		/* { del -001- a.
		nom_estado = 
		case
			when a.sol_estado = 'A' then 'Pendiente de aprobaci�n'
			when a.sol_estado = 'B' then 'Pendiente de aprobaci�n SI'
			when a.sol_estado = 'C' then 'Aprobado'
			when a.sol_estado = 'D' then 'Aprobado y en proceso de envio'
			when a.sol_estado = 'E' then 'Enviado'
			when a.sol_estado = 'F' then 'En espera de Restore'
			when a.sol_estado = 'G' then 'En espera (ejecuci�n diferida)'
		end,
		} */
		b.cod_grupo,
		b.cod_sector,
		b.cod_gerencia,
		b.cod_direccion,
		-- { add -001- a.
		aprobado = 
		case
			when a.sol_eme = 'S' then 'Requiere aprobaci�n nivel 3'
			when a.sol_eme = 'N' and a.sol_mask = 'N' and LTRIM(RTRIM(a.sol_recurso)) = LTRIM(RTRIM(a.sol_resp_ejec)) then 'Requiere aprobaci�n nivel 3'
			when a.sol_eme = 'N' and a.sol_mask = 'N' then 'Requiere aprobaci�n nivel 4 y 3'
			when a.sol_eme = 'N' and a.sol_mask = 'S' then 'No requiere aprobaci�n'
			else 'No requiere aprobaci�n'
		end
		-- }
		/* { del -001- a.
		aprobado = 
		case
			when a.sol_fe_auto1 is not null then 'L�der: ' + rtrim((select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_resp_ejec)) + ' el d�a ' + rtrim(convert(char(30), a.sol_fe_auto1,103))
			when a.sol_fe_auto2 is not null then 'Supervisor: ' + rtrim((select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_resp_sect)) + ' el d�a ' + rtrim(convert(char(30), a.sol_fe_auto2,103))
			when a.sol_mask = 'N' then 'Requiere aprobaci�n de nivel III'
			else 'No requiere aprobaci�n'
		end
		} */
		,a.sol_trnm_fe
		,a.sol_trnm_usr
		,a.sol_prod_cpybbl
		,a.sol_prod_cpynom
	from 
		GesPet..Solicitudes a left join 
		GesPet..Recurso b on a.sol_recurso = b.cod_recurso
	where
		(@sol_nroasignado is null or a.sol_nroasignado = @sol_nroasignado)
	order by 1
	return(0)
go

grant execute on dbo.sp_GetSolicitudNro to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
