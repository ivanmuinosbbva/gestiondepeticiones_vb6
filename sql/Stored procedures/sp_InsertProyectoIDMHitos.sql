/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.02.2011 - 
-001- a. FJS 16.03.2015 - Se agregan nuevos datos: marca de visibilidad en hitos.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertProyectoIDMHitos'
go

if exists (select * from sysobjects where name = 'sp_InsertProyectoIDMHitos' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertProyectoIDMHitos
go

create procedure dbo.sp_InsertProyectoIDMHitos
	@ProjId				int,
	@ProjSubId			int,
	@ProjSubSId			int,
	@hito_nombre		char(100),
	@hito_descripcion	char(255),
	@hito_fe_ini		char(20),
	@hito_fe_fin		char(20),
	@hito_estado		char(6),
	@cod_nivel			char(4),
	@cod_area			char(8),
	@orden				int,
	@hito_expos			char(1),
	@hito_id			int				-- upd -002- a.
as

	declare	@var_numero		int 	
	select @var_numero = 0 
	 
	if exists (select var_numero 
				from GesPet..Varios 
				where var_codigo = 'ULTHITO')
		begin 
			select @var_numero = var_numero 
			from GesPet..Varios where var_codigo = 'ULTHITO'
			
			select @var_numero = @var_numero + 1 
			
			update GesPet..Varios 
			set var_numero = @var_numero, var_texto = ''
			where var_codigo = 'ULTHITO' 
		end 
	else 
		begin 
			select @var_numero = 1 
			insert into GesPet..Varios (var_codigo, var_numero, var_texto) 
			values ('ULTHITO', 1, '')
		end 

	insert into GesPet.dbo.ProyectoIDMHitos (
		ProjId,
		ProjSubId,
		ProjSubSId,
		hit_nrointerno,
		hito_nombre,
		hito_descripcion,
		hito_fe_ini,
		hito_fe_fin,
		hito_estado,
		cod_nivel,
		cod_area,
		hito_fecha,
		hito_usuario,
		hst_nrointerno,
		hit_orden,
		hito_expos,
		hito_id)			-- upd -002- a.
	values (
		@ProjId,
		@ProjSubId,
		@ProjSubSId,
		@var_numero,
		@hito_nombre,
		@hito_descripcion,
		convert(smalldatetime,@hito_fe_ini),
		convert(smalldatetime,@hito_fe_fin),
		@hito_estado,
		@cod_nivel,
		@cod_area,
		getdate(),
		SUSER_NAME(),
		null,
		@orden,
		@hito_expos,
		@hito_id)			-- upd -002- a.
	return(0)
go

grant execute on dbo.sp_InsertProyectoIDMHitos to GesPetUsr
go

print 'Actualización realizada.'
go