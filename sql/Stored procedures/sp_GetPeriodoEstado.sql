/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 15.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeriodoEstado'
go

if exists (select * from sysobjects where name = 'sp_GetPeriodoEstado' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeriodoEstado
go

create procedure dbo.sp_GetPeriodoEstado
	@cod_estado_per	char(8)=null
as
	select 
		a.cod_estado_per,
		a.nom_estado_per
	from 
		GesPet.dbo.PeriodoEstado a
	where
		(a.cod_estado_per = @cod_estado_per or @cod_estado_per is null)
	return(0)
go

grant execute on dbo.sp_GetPeriodoEstado to GesPetUsr
go

print 'Actualización realizada.'
