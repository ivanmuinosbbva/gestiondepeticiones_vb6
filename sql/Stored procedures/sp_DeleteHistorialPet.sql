use GesPet
go

print 'Creando/actualizando SP: sp_DeleteHistorialPet'
go

if exists (select * from sysobjects where name = 'sp_DeleteHistorialPet' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteHistorialPet
go

create procedure dbo.sp_DeleteHistorialPet
	@pet_nrointerno  int,  
	@hst_nrointerno  int

as

begin tran
	begin
		delete 
			GesPet..Historial
		where
			pet_nrointerno = @pet_nrointerno and
			hst_nrointerno = @hst_nrointerno
	end

	begin
		delete 
			GesPet..HistorialMemo    
		where 
			hst_nrointerno = @hst_nrointerno
	end
commit tran

return(0) 
go

grant execute on dbo.sp_DeleteHistorialPet to GesPetUsr
go

print 'Actualización realizada.'
