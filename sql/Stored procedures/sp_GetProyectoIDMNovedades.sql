/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMNovedades'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMNovedades' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMNovedades
go

create procedure dbo.sp_GetProyectoIDMNovedades
	@ProjId			int=null,
	@ProjSubId		int=null,
	@ProjSubSId		int=null,
	--@mem_fecha		smalldatetime=null,
	@mem_fecha		char(20),
	@mem_campo		char(10)='NOVEDADES',
	@mem_secuencia	smallint
as
	select 
		n.ProjId,
		n.ProjSubId,
		n.ProjSubSId,
		n.mem_fecha,
		n.mem_campo,
		n.mem_secuencia,
		n.mem_texto,
		n.cod_usuario,
		nom_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = n.cod_usuario),
		n.mem_accion,
		n.mem_fecha2,
		n.cod_usuario2,
		nom_usuario2 = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = n.cod_usuario2)
	from 
		GesPet.dbo.ProyectoIDMNovedades n
	where
		n.ProjId = @ProjId and
		n.ProjSubId = @ProjSubId and
		n.ProjSubSId = @ProjSubSId and
		(@mem_fecha is null or n.mem_fecha = convert(smalldatetime, @mem_fecha)) and
		(@mem_campo is null or n.mem_campo = @mem_campo) and
		(@mem_secuencia is null or n.mem_secuencia = @mem_secuencia)
	order by
		n.ProjId,
		n.ProjSubId,
		n.ProjSubSId,
		n.mem_campo,
		n.mem_fecha	desc,
		n.mem_secuencia

	return(0)
go

grant execute on dbo.sp_GetProyectoIDMNovedades to GesPetUsr
go

print 'Actualización realizada.'
go
