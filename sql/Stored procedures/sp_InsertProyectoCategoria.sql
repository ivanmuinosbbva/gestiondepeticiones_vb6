/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.16 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 04.11.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertProyectoCategoria'
go

if exists (select * from sysobjects where name = 'sp_InsertProyectoCategoria' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertProyectoCategoria
go

create procedure dbo.sp_InsertProyectoCategoria
	@ProjCatId	int,
	@ProjCatNom	char(100)
as
	insert into GesPet.dbo.ProyectoCategoria (
		ProjCatId,
		ProjCatNom)
	values (
		@ProjCatId,
		@ProjCatNom)
	return(0)
go

grant execute on dbo.sp_InsertProyectoCategoria to GesPetUsr
go

print 'Actualización realizada.'
go
