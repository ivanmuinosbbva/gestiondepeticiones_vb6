/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go

print 'sp_GetAgrupNivelRecur'
go

if exists (select * from sysobjects where name = 'sp_GetAgrupNivelRecur' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetAgrupNivelRecur
go

create procedure dbo.sp_GetAgrupNivelRecur 
	@modo		char(4)='VIEW',
	@nivel		char(3)='PUB',
	@area		varchar(250)='',
	@vigencia	char(1)='T'
as 

	declare @cod_raiz	int
	declare @secuencia	int
	declare @xsecuencia int
	declare @ret_status int

	/*    Tabla Temporaria  */
	create table #AgrupArbol(
		secuencia		int,
		nivel 			int,
		agr_nrointerno 	int)
	create table #AgrupSalida(
		secuencia		int,
		nivel 			int,
		agr_nrointerno 	int)

		/* 
		se basa fuertemente en que todas las ramas del arbol deben tener
		la misma visibilidad que el padre
		*/
	if @modo='OWNR'
		begin
			declare CursRaiz cursor for
			select agr_nrointerno
			from  Agrup 
			where (agr_nropadre=0) and
				RTRIM(cod_usualta)=RTRIM(@area) and
				(@vigencia='T' or @vigencia=agr_vigente)
			order by agr_nrointerno ASC  
			for read only
		end
	/* para view o modi se selecciona por visibilidad y se filtra en el front-end*/
	if @modo='VIEW' or @modo='MODI' or @modo='SADM'
		begin 
			if @nivel='PUB'
			begin 
				declare CursRaiz cursor for
				select agr_nrointerno
				from  Agrup 
				where (agr_nropadre=0) and
					agr_visibilidad='PUB' and
					(@vigencia='T' or @vigencia=agr_vigente)
				order by agr_nrointerno ASC  
				for read only
			end 
			if @nivel='DIR'
			begin 
				declare CursRaiz cursor for
				select agr_nrointerno
				from  Agrup 
				where (agr_nropadre=0) and
					agr_visibilidad='DIR' and 
					(charindex('|' + RTRIM(cod_direccion) + '|' ,RTRIM(@area))>0) and 
					(@vigencia='T' or @vigencia=agr_vigente)
				order by agr_nrointerno ASC  
				for read only
			end 
			if @nivel='GER'
			begin 
				declare CursRaiz cursor for
				select agr_nrointerno
				from  Agrup 
				where (agr_nropadre=0) and
					agr_visibilidad='GER' and 
					(charindex('|' + RTRIM(cod_gerencia) + '|' ,RTRIM(@area))>0) and 
					(@vigencia='T' or @vigencia=agr_vigente)
				order by agr_nrointerno ASC  
				for read only
			end 
			if @nivel='SEC'
			begin 
				declare CursRaiz cursor for
				select agr_nrointerno
				from  Agrup 
				where (agr_nropadre=0) and
					agr_visibilidad='SEC' and
					(charindex('|' + RTRIM(cod_sector) + '|' ,RTRIM(@area))>0) and 
					(@vigencia='T' or @vigencia=agr_vigente)
				order by agr_nrointerno ASC  
				for read only
			end 
			if @nivel='GRU'
			begin 
				declare CursRaiz cursor for
				select agr_nrointerno
				from  Agrup 
				where (agr_nropadre=0) and
					agr_visibilidad='GRU' and
					(charindex('|' + RTRIM(cod_grupo) + '|' ,RTRIM(@area))>0) and 
					(@vigencia='T' or @vigencia=agr_vigente)
				order by agr_nrointerno ASC  
				for read only
			end 
			if @nivel='USR'
			begin 
				declare CursRaiz cursor for
				select agr_nrointerno
				from  Agrup 
				where (agr_nropadre=0) and
					agr_visibilidad='USR' and
					(@modo='SADM' or RTRIM(cod_usualta)=RTRIM(@area)) and
					(@vigencia='T' or @vigencia=agr_vigente)
				order by agr_nrointerno ASC  
				for read only
			end 
		end

	/*
	if @modo='MODI'
	begin 
		if @nivel='PUB'
		begin 
			declare CursRaiz cursor for
			select agr_nrointerno
			from  Agrup 
			where (agr_nropadre=0) and
				agr_actualiza='PUB' and
				(@vigencia='T' or @vigencia=agr_vigente)
			order by agr_nrointerno ASC  
			for read only
		end 
		if @nivel='DIR'
		begin 
			declare CursRaiz cursor for
			select agr_nrointerno
			from  Agrup 
			where (agr_nropadre=0) and
				agr_actualiza='DIR' and 
				(charindex('|' + RTRIM(cod_direccion) + '|' ,RTRIM(@area))>0) and 
				(@vigencia='T' or @vigencia=agr_vigente)
			order by agr_nrointerno ASC  
			for read only
		end 
		if @nivel='GER'
		begin 
			declare CursRaiz cursor for
			select agr_nrointerno
			from  Agrup 
			where (agr_nropadre=0) and
				agr_actualiza='GER' and 
				(charindex('|' + RTRIM(cod_gerencia) + '|' ,RTRIM(@area))>0) and 
				(@vigencia='T' or @vigencia=agr_vigente)
			order by agr_nrointerno ASC  
			for read only
		end 
		if @nivel='SEC'
		begin 
			declare CursRaiz cursor for
			select agr_nrointerno
			from  Agrup 
			where (agr_nropadre=0) and
				agr_actualiza='SEC' and
				(charindex('|' + RTRIM(cod_sector) + '|' ,RTRIM(@area))>0) and 
				(@vigencia='T' or @vigencia=agr_vigente)
			order by agr_nrointerno ASC  
			for read only
		end 
		if @nivel='GRU'
		begin 
			declare CursRaiz cursor for
			select agr_nrointerno
			from  Agrup 
			where (agr_nropadre=0) and
				agr_actualiza='GRU' and
				(charindex('|' + RTRIM(cod_grupo) + '|' ,RTRIM(@area))>0) and 
				(@vigencia='T' or @vigencia=agr_vigente)
			order by agr_nrointerno ASC  
			for read only
		end 
		if @nivel='USR'
		begin 
			declare CursRaiz cursor for
			select agr_nrointerno
			from  Agrup 
			where (agr_nropadre=0) and
				agr_actualiza='USR' and
				RTRIM(cod_usualta)=RTRIM(@area) and
				(@vigencia='T' or @vigencia=agr_vigente)
			order by agr_nrointerno ASC  
			for read only
		end 
	end
	*/
	select @secuencia=0

	open CursRaiz
	fetch CursRaiz into @cod_raiz
	while (@@sqlstatus = 0)
	begin
		/* con cada raiz arma el arbol */
		execute @ret_status = sp_GetAgrupArbol @cod_raiz, 0, @secuencia OUTPUT
		if (@ret_status <> 0)
		begin
			if (@ret_status = 30003)
				select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
			return (@ret_status)
		end
		/* guarda el arbol generado para esa raiz */
		insert into #AgrupSalida (secuencia, nivel, agr_nrointerno)
			select	secuencia, nivel, agr_nrointerno from #AgrupArbol
		
		delete #AgrupArbol

		fetch CursRaiz into @cod_raiz
	end
	close CursRaiz
	DEALLOCATE CURSOR CursRaiz

	/* devuelve los registros procesados con datos propios */
	select  B.secuencia,
		B.nivel,
		A.agr_nrointerno,
		A.agr_nropadre,
		nom_nropadre = (select P.agr_titulo from Agrup P where P.agr_nrointerno=A.agr_nropadre),
		A.agr_titulo,
		A.agr_vigente,
		A.cod_direccion,
		nom_direccion = (select D.nom_direccion from Direccion D where D.cod_direccion=A.cod_direccion),
		A.cod_gerencia,
		nom_gerencia = (select G.nom_gerencia from Gerencia G where G.cod_gerencia=A.cod_gerencia),
		A.cod_sector,
		nom_sector = (select S.nom_sector from Sector S where S.cod_sector=A.cod_sector),
		A.cod_grupo,
		nom_grupo = (select U.nom_grupo from Grupo U where U.cod_grupo=A.cod_grupo),
		A.cod_usualta,
		nom_usualta = (select R.nom_recurso from Recurso R where R.cod_recurso=A.cod_usualta),
		A.agr_visibilidad,
		A.agr_actualiza,
		A.agr_admpet,
		A.audit_user,
		A.audit_date
	from 	#AgrupSalida B, Agrup A
	where   (A.agr_nrointerno = B.agr_nrointerno)
	order by B.secuencia
	return(0) 
go

grant execute on dbo.sp_GetAgrupNivelRecur to GesPetUsr 
go
