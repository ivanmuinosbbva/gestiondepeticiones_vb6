/*
-000- a. FJS 15.06.2010 - Nuevo SP para...
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_Peticion'
go

if exists (select * from sysobjects where name = 'sp_IGM_Peticion' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_Peticion
go

create procedure dbo.sp_IGM_Peticion
	@pet_nrointerno		int=null
as 
	select
		a.pet_nrointerno,
		a.pet_nroasignado,
		a.titulo,
		a.cod_clase,
		nom_clase = isnull((select x.nom_clase from GesPet..Clase x where x.cod_clase = a.cod_clase),'* Sin clase'),
		a.cod_estado,
		nom_estado = isnull((select x.nom_estado from GesPet..Estados x where x.cod_estado = a.cod_estado),''),
		a.cod_direccion,
		nom_direccion = isnull((select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = a.cod_direccion),''),
		a.cod_gerencia,
		nom_gerencia = isnull((select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = a.cod_gerencia),''),
		a.cod_sector,
		nom_sector = isnull((select x.nom_sector from GesPet..Sector x where x.cod_sector = a.cod_sector),''),
		a.cod_bpar,
		nom_bpar = isnull((select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.cod_bpar),''),
		descripcion = isnull((select x.mem_texto from GesPet..PeticionMemo x where x.pet_nrointerno = a.pet_nrointerno and x.mem_campo = 'DESCRIPCIO' and x.mem_secuencia = 0),''),
		beneficios = isnull((select x.mem_texto from GesPet..PeticionMemo x where x.pet_nrointerno = a.pet_nrointerno and x.mem_campo = 'BENEFICIOS' and x.mem_secuencia = 0),''),
		a.fe_ini_plan,
		a.fe_fin_plan,
		a.fe_ini_real,
		a.fe_fin_real
	from
		GesPet..Peticion a
	where
		(@pet_nrointerno is null or a.pet_nrointerno = @pet_nrointerno)
	order by
		a.pet_nrointerno
	
	return(0)
go

grant execute on dbo.sp_IGM_Peticion to GesPetUsr
go

grant execute on dbo.sp_IGM_Peticion to GrpTrnIGM
go

print 'Actualización realizada.'
go
