/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
/*
--Intenta replicar la funcionalidad del VB
declare @pet_nrointerno	 int


declare @fIniPlan    smalldatetime
declare @fFinPlan    smalldatetime
declare @fIniReal    smalldatetime
declare @fFinReal    smalldatetime
declare @hsPresup    smallint
declare @new_estado	 char(6)

select @pet_nrointerno=pet_nrointerno from Peticion where pet_nroasignado = 4115

execute sp_GetIntegracionSectores
	@pet_nrointerno,
	@fIniPlan OUTPUT,
	@fFinPlan OUTPUT,
	@fIniReal OUTPUT,
	@fFinReal OUTPUT,
	@hsPresup OUTPUT,
	@new_estado OUTPUT

--    los ESTIOK solo integran HOras
--    si existe algun PLANIF,ESTIMA,ESTIOK
--    o alguno que no tenga fechas planificadas y tenga estados INTEGRABLES
--    ==> se nullea las fechas de integracion planifi,
--    porque de lo contrario estariamos presentando una fecha planificada
--    pero hay alguien que adeuda ese dato


*/
print 'sp_GetIntegracionSectores'
go
if exists (select * from sysobjects where name = 'sp_GetIntegracionSectores' and sysstat & 7 = 4)
drop procedure dbo.sp_GetIntegracionSectores
go
create procedure dbo.sp_GetIntegracionSectores
	@pet_nrointerno     int,
	@fIniPlan    smalldatetime OUTPUT,
	@fFinPlan    smalldatetime OUTPUT,
	@fIniReal    smalldatetime OUTPUT,
	@fFinReal    smalldatetime OUTPUT,
	@hsPresup    smallint OUTPUT,
	@new_estado	 char(6) OUTPUT
as

declare @auxHS int
declare @flgPlanif char(1)

select @flgPlanif = 'N'
select @hsPresup = 0
select @auxHS = 0
select @fIniPlan = NULL
select @fFinPlan = NULL
select @fIniReal = NULL
select @fFinReal = NULL


declare @cod_hijo varchar(8)
declare @fe_ini_plan smalldatetime
declare @fe_fin_plan smalldatetime
declare @fe_ini_real smalldatetime
declare @fe_fin_real smalldatetime
declare @horaspresup int
declare @cod_estado varchar(6)

declare @flg_rnkup int

declare @aEstado int
declare @nEstado int
declare @xEstado char(6)

select @nEstado=0
select @xEstado=''

declare @flg1 char(1)
declare @flg2 char(1)

select @flg1='N'
select @flg2='N'


select @hsPresup = horaspresup
	from Peticion
	where pet_nrointerno=@pet_nrointerno


DECLARE CursHijo CURSOR FOR
select	PSG.cod_sector,
	PSG.cod_estado,
	PSG.fe_ini_plan,
	PSG.fe_fin_plan,
	PSG.fe_ini_real,
	PSG.fe_fin_real,
	PSG.horaspresup,
	rnkup = CONVERT(int,EST.flg_rnkup)
from	PeticionSector PSG, 
	Estados EST
where	(@pet_nrointerno = PSG.pet_nrointerno) and
	(RTRIM(EST.cod_estado) = RTRIM(PSG.cod_estado))
for read only

OPEN CursHijo
FETCH CursHijo INTO
	@cod_hijo,
	@cod_estado,
	@fe_ini_plan,
	@fe_fin_plan,
	@fe_ini_real,
	@fe_fin_real,
	@horaspresup,
	@flg_rnkup
WHILE (@@sqlstatus = 0)
BEGIN
	--ver el nuevo estado
	IF charindex(RTRIM(@cod_estado),RTRIM('TERMIN'))>0
	BEGIN
		select @flg1 = 'S'
	END
	IF charindex(RTRIM(@cod_estado),RTRIM('PLANOK|PLANIF|ESTIOK|ESTIMA|EJECUC'))>0
	BEGIN
		select @flg2 = 'S'
	END
	IF @flg_rnkup > @nEstado
	BEGIN
		select @nEstado = @flg_rnkup
		select @xEstado = @cod_estado
	END

	--ver fechas y horas
	IF charindex(RTRIM(@cod_estado),RTRIM('ESTIOK|PLANOK|EJECUC|TERMIN'))>0
	BEGIN
		IF RTRIM(@cod_estado)<>'ESTIOK'
		BEGIN
			IF @fe_ini_plan IS NOT NULL
			BEGIN
				If (@fIniPlan IS NULL) Or (@fe_ini_plan < @fIniPlan)
				BEGIN
					select @fIniPlan = @fe_ini_plan
				END
			END
			ELSE
			BEGIN
				select @flgPlanif = 'S'
			END
			IF @fe_fin_plan IS NOT NULL
			BEGIN
				If (@fFinPlan IS NULL) Or (@fe_fin_plan < @fFinPlan)
				BEGIN
					select @fFinPlan = @fe_fin_plan
				END
			END
			ELSE
			BEGIN
				select @flgPlanif = 'S'
			END
			IF @fe_ini_real IS NOT NULL
			BEGIN
				If (@fIniReal IS NULL) Or (@fe_ini_real < @fIniReal)
				BEGIN
					select @fIniReal = @fe_ini_real
				END
			END
			IF @fe_fin_real IS NOT NULL
			BEGIN
				If (@fFinReal IS NULL) Or (@fe_fin_real < @fFinReal)
				BEGIN
					select @fFinReal = @fe_fin_real
				END
			END
		END
		IF @horaspresup IS NOT NULL
			select @auxHS = @auxHS + @horaspresup
		IF @auxHS > @hsPresup
			select @hsPresup = @auxHS
	END
	IF charindex(RTRIM(@cod_estado),RTRIM('PLANIF|ESTIMA|ESTIOK'))>0
		select @flgPlanif = 'S'

	FETCH CursHijo INTO
		@cod_hijo,
		@cod_estado,
		@fe_ini_plan,
		@fe_fin_plan,
		@fe_ini_real,
		@fe_fin_real,
		@horaspresup,
		@flg_rnkup
END
CLOSE CursHijo
DEALLOCATE CURSOR CursHijo

IF @flg1='S' AND @flg2='S'
BEGIN
	select @xEstado = 'EJECUC'
END

/* si hubo algun planif, NO arrastra las fechas planok */
IF @flgPlanif='S'
BEGIN	
	select @fIniPlan = NULL
	select @fFinPlan = NULL
END

If @xEstado <> 'TERMIN'
	select @fFinReal = NULL

IF charindex(RTRIM(@xEstado),RTRIM('ESTIOK|PLANOK|EJECUC|TERMIN|SUSPEN|CANCEL')) = 0
BEGIN
	select @fIniPlan = NULL
	select @fFinPlan = NULL
	select @fIniReal = NULL
	select @fFinReal = NULL
	select @hsPresup = 0
END

select @new_estado=@xEstado

return(0)
go

grant execute on dbo.sp_GetIntegracionSectores to GesPetUsr
go
