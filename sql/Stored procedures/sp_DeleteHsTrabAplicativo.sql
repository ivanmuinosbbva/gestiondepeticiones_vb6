/*
-000- a. FJS 19.05.2009 - Nuevo SP para manejo de carga de horas desglosado por Aplicativo
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteHsTrabAplicativo'
go

if exists (select * from sysobjects where name = 'sp_DeleteHsTrabAplicativo' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteHsTrabAplicativo
go

create procedure dbo.sp_DeleteHsTrabAplicativo
	@cod_recurso	char(10)=null,
	@cod_tarea		char(8)=null,
	@pet_nrointerno	int=null,
	@fe_desde		smalldatetime,
	@fe_hasta		smalldatetime,
	@horas			smallint=null,
	@app_id			char(30)=null
as
	delete from
		GesPet..HorasTrabajadasAplicativo
	where
		cod_recurso = @cod_recurso and
		cod_tarea = @cod_tarea and
		pet_nrointerno = @pet_nrointerno and
		fe_desde = @fe_desde and
		fe_hasta = @fe_hasta and
		horas = @horas and
		(app_id = @app_id or @app_id is null)
go

grant execute on dbo.sp_DeleteHsTrabAplicativo to GesPetUsr
go

print 'Actualización realizada.'
