/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 21.10.2015 - Este sp devuelve el siguiente nro. de orden (mayor + 1) para los nuevos hitos.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMHitosOrden'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMHitosOrden' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMHitosOrden
go

create procedure dbo.sp_GetProyectoIDMHitosOrden
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int
as
	select 
		MAX(a.hit_orden) + 1	as orden
	from
		GesPet..ProyectoIDMHitos a 
	where
		a.ProjId = @ProjId and
		a.ProjSubId = @ProjSubId and
		a.ProjSubSId = @ProjSubSId and
		a.hit_orden is not null
	return(0)
go

grant execute on dbo.sp_GetProyectoIDMHitosOrden to GesPetUsr
go

print 'Actualización realizada.'
go