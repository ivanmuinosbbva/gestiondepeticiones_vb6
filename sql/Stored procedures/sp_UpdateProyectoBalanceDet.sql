use GesPet
go
print 'sp_UpdateProyectoBalanceDet'
go
if exists (select * from sysobjects where name = 'sp_UpdateProyectoBalanceDet' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateProyectoBalanceDet
go
create procedure dbo.sp_UpdateProyectoBalanceDet
	@prj_nrointerno		int,
	@cod_BalanceRubro	char(8),
	@cod_BalanceSubRubro	char(8),
	@secuencia		int,
	@bde_texto		char(50),
	@bde_moneda		char(3),
	@xbde_estimado		char(15),
	@xbde_real		char(15)

as
declare	@bde_estimado	numeric(12,2)
declare	@bde_real	numeric(12,2)

select @bde_estimado=convert(numeric(12,2),@xbde_estimado)
select @bde_real=convert(numeric(12,2),@xbde_real)

if not exists (select prj_nrointerno from Proyecto where prj_nrointerno=@prj_nrointerno)
	begin 
		raiserror 30011 'Proyecto inexistente'   
		select 30011 as ErrCode , 'Proyecto inexistente' as ErrDesc   
		return (30011)   
	end 

update ProyectoBalanceDet set
	bde_texto	= @bde_texto, 
	bde_moneda	= @bde_moneda, 
	bde_estimado	= @bde_estimado,
	bde_real	= @bde_real,
	audit_user	= SUSER_NAME(), 
	audit_date	= getdate()
where	prj_nrointerno      = @prj_nrointerno      and 
	cod_BalanceRubro    = @cod_BalanceRubro    and 
	cod_BalanceSubRubro = @cod_BalanceSubRubro and 
	secuencia           = @secuencia

return(0)
go

grant execute on dbo.sp_UpdateProyectoBalanceDet to GesPetUsr
go
