/*
-001- a. FJS 15.09.2010 - Se agrega el atributo cod_clase.
*/


use GesPet
go

print 'Creando/actualizando SP: sp_GetUnaPetTituloGrp'
go

if exists (select * from sysobjects where name = 'sp_GetUnaPetTituloGrp' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetUnaPetTituloGrp
go

create procedure dbo.sp_GetUnaPetTituloGrp
	@pet_nrointerno		int=null,
	@titulo				varchar(59)=null,
	@cod_estado			varchar(250)=null,
	@cod_sector			char(8)=null,
	@cod_grupo			char(8)=null
as
	if @pet_nrointerno is null
		begin
			select 
				PET.pet_nrointerno,
				PET.pet_nroasignado,
				PET.titulo,
				PET.cod_clase,	-- add -001- a.
				PET.cod_tipo_peticion,
				PET.prioridad,
				PET.pet_nroanexada,
				PET.fe_pedido,
				PET.fe_requerida,
				PET.fe_comite,
				PET.fe_ini_plan,
				PET.fe_fin_plan,
				PET.fe_ini_real,
				PET.fe_fin_real,
				PET.horaspresup,
				PET.cod_direccion,
				PET.cod_gerencia,
				PET.cod_sector,
				PET.cod_usualta,
				PET.cod_solicitante,
				PET.cod_bpar,
				PET.cod_referente,
				PET.cod_supervisor,
				PET.cod_director,
				PET.cod_estado,
				nom_estado = (select EST.nom_estado from Estados EST where RTRIM(EST.cod_estado) = RTRIM(PET.cod_estado)),
				PET.fe_estado,
				PET.cod_situacion,
				nom_situacion = (select SIT.nom_situacion from Situaciones SIT where RTRIM(SIT.cod_situacion) = RTRIM(PET.cod_situacion))
			from	
				GesPet..Peticion PET
			where
				(@titulo is null or RTRIM(@titulo) is null or RTRIM(@titulo)='' or charindex(UPPER(RTRIM(@titulo)),UPPER(RTRIM(PET.titulo)))>0) and
				(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(PET.cod_estado),RTRIM(@cod_estado)) > 0) and
				((PET.pet_nrointerno in 
						(select GRP.pet_nrointerno from GesPet..PeticionGrupo GRP where 
						(@cod_sector is null or RTRIM(GRP.cod_sector) = RTRIM(@cod_sector)) and 
						(@cod_grupo is null or RTRIM(GRP.cod_grupo) = RTRIM(@cod_grupo)))))
		end
	else
		begin
			select 
				PET.pet_nrointerno,
				PET.pet_nroasignado,
				PET.titulo,
				PET.cod_clase,	-- add -001- a.
				PET.cod_tipo_peticion,
				PET.prioridad,
				PET.pet_nroanexada,
				PET.fe_pedido,
				PET.fe_requerida,
				PET.fe_comite,
				PET.fe_ini_plan,
				PET.fe_fin_plan,
				PET.fe_ini_real,
				PET.fe_fin_real,
				PET.horaspresup,
				PET.cod_direccion,
				PET.cod_gerencia,
				PET.cod_sector,
				PET.cod_usualta,
				PET.cod_solicitante,
				PET.cod_bpar,
				PET.cod_referente,
				PET.cod_supervisor,
				PET.cod_director,
				PET.cod_estado,
				nom_estado = (select EST.nom_estado from Estados EST where RTRIM(EST.cod_estado) = RTRIM(PET.cod_estado)),
				PET.fe_estado,
				PET.cod_situacion,
				nom_situacion = (select SIT.nom_situacion from Situaciones SIT where RTRIM(SIT.cod_situacion) = RTRIM(PET.cod_situacion))
			from	
				GesPet..Peticion PET
			where
				(@titulo is null or RTRIM(@titulo) is null or RTRIM(@titulo)='' or charindex(UPPER(RTRIM(@titulo)),UPPER(RTRIM(PET.titulo)))>0) and
				(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(PET.cod_estado),RTRIM(@cod_estado)) > 0) and
				((PET.pet_nrointerno in 
						(select GRP.pet_nrointerno from GesPet..PeticionGrupo GRP where 
						(@pet_nrointerno = GRP.pet_nrointerno) and 
						(@cod_sector is null or RTRIM(GRP.cod_sector) = RTRIM(@cod_sector)) and 
						(@cod_grupo is null or RTRIM(GRP.cod_grupo) = RTRIM(@cod_grupo)))))
		end
	return(0)
go

grant execute on dbo.sp_GetUnaPetTituloGrp to GesPetUsr
go

print 'Actualización realizada.'
go

/*
		(@cod_sector is null or RTRIM(@cod_sector) = RTRIM(GRP.cod_sector)) and
		(@cod_grupo is null or RTRIM(@cod_grupo) = RTRIM(GRP.cod_grupo))
*/