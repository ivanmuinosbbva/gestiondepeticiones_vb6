/*
-000- a. FJS 24.06.2011 - Nuevo SP para devolver los archivos DSN visados por SI y luego enviar aviso de correo.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHEDT101b'
go

if exists (select * from sysobjects where name = 'sp_GetHEDT101b' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHEDT101b
go

create procedure dbo.sp_GetHEDT101b
	@modo			char(1)='A',
	@cod_recurso	char(10),
	@fecha			char(15),
	@estado			char(1)
as 
	if @modo = 'A'
		begin
			select
				h1.dsn_userid
			from
				GesPet..HEDT001 h1 inner join
				GesPet..Recurso r on (h1.dsn_userid = r.cod_recurso)
			where
				h1.dsn_vb = @estado and 
				--h1.dsn_vb_userid = @cod_recurso and
				h1.dsn_vb_fe = convert(smalldatetime, @fecha) and
				r.estado_recurso = 'A'
			group by
				h1.dsn_userid
		end
	else
		begin
			select
				h1.dsn_id,
				h1.dsn_nom,
				h1.dsn_userid,
				h1.dsn_txt,
				dsn_nomprod =
				case
					when patindex('%D[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod) > 0 then substring(h1.dsn_nomprod,1,patindex('%D[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod)-1)
					when patindex('%F[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod) > 0 then substring(h1.dsn_nomprod,1,patindex('%F[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod)-1)
					when patindex('%' + '%%ODATE',h1.dsn_nomprod) > 0 then str_replace(h1.dsn_nomprod,'%%ODATE',null)
					else h1.dsn_nomprod
				end,
				r.email,
				h1.dsn_vb_userid,
				mail_si = (select x.email from GesPet..Recurso x where x.cod_recurso = h1.dsn_vb_userid)
			from
				GesPet..HEDT001 h1 inner join
				GesPet..Recurso r on (h1.dsn_userid = r.cod_recurso)
			where
				h1.dsn_vb = @estado and 
				(@cod_recurso is null or h1.dsn_userid = @cod_recurso) and
				h1.dsn_vb_fe = convert(smalldatetime, @fecha)
		end
	return(0)
go

grant execute on dbo.sp_GetHEDT101b to GesPetUsr
go

print 'Actualización realizada.'
go
