/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 22.12.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateValidacion'
go

if exists (select * from sysobjects where name = 'sp_UpdateValidacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateValidacion
go

create procedure dbo.sp_UpdateValidacion
	@valid		int,
	@valitem	int,
	@valdesc	varchar(255),
	@valorden	int,
	@valhab		char(1)
as
	update 
		GesPet.dbo.Validacion
	set
		valdesc		= @valdesc,
		valorden	= @valorden,
		valhab		= @valhab,
		audituser	= suser_name(),
		auditfecha	= getdate()
	where 
		valid = @valid and
		valitem = @valitem
go

grant execute on dbo.sp_UpdateValidacion to GesPetUsr
go

print 'Actualización realizada.'
go
