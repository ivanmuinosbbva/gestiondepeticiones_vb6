use GesPet
go

print 'Creando/actualizando SP: sp_GetUnaHorasTrabajadas'
go

if exists (select * from sysobjects where name = 'sp_GetUnaHorasTrabajadas' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetUnaHorasTrabajadas
go

create procedure dbo.sp_GetUnaHorasTrabajadas
	@cod_recurso	char(10),
	@cod_tarea		char(8)='',
	@pet_nrointerno	int=0,
	@fe_desde		smalldatetime,
	@fe_hasta		smalldatetime
as
	begin
		select
			Ht.cod_recurso,
			Ht.cod_tarea,
			nom_tarea = (select Ta.nom_tarea from Tarea Ta where Ta.cod_tarea=Ht.cod_tarea),
			Ht.pet_nrointerno,
			pet_nroasignado = (select x.pet_nroasignado from Peticion x where x.pet_nrointerno = Ht.pet_nrointerno),
			pet_titulo = (select x.titulo from Peticion x where x.pet_nrointerno = Ht.pet_nrointerno),
			Ht.fe_desde,
			Ht.fe_hasta,
			Ht.horas,
			Ht.trabsinasignar,
			Ht.observaciones
		from
			GesPet..HorasTrabajadas Ht
		where
			Ht.pet_nrointerno = @pet_nrointerno and
			Ht.cod_tarea = @cod_tarea and
			Ht.cod_recurso = @cod_recurso and
			Ht.fe_desde <= @fe_hasta and
			Ht.fe_hasta >= @fe_desde
	end
go

grant execute on dbo.sp_GetUnaHorasTrabajadas to GesPetUsr
go

print 'Actualización realizada.'
go


/*
	select
		Ht.cod_recurso,
		nom_recurso = (select Re.nom_recurso from Recurso Re where RTRIM(Re.cod_recurso)=RTRIM(Ht.cod_recurso)),
		Ht.cod_tarea,
		nom_tarea = (select Ta.nom_tarea from Tarea Ta where RTRIM(Ta.cod_tarea)=RTRIM(Ht.cod_tarea)),
		Ht.pet_nrointerno,
		Pt.pet_nroasignado,
		pet_titulo=Pt.titulo,
		Ht.fe_desde,
		Ht.fe_hasta,
		Ht.horas,
		Ht.trabsinasignar,
		Ht.observaciones
	from
		GesPet..HorasTrabajadas Ht,GesPet..Peticion Pt
	where
		RTRIM(Ht.cod_recurso) = RTRIM(@cod_recurso) and
		Ht.pet_nrointerno = @pet_nrointerno and
		(RTRIM(Ht.cod_tarea) is null or RTRIM(Ht.cod_tarea) = RTRIM(@cod_tarea)) and
		convert(char(8),Ht.fe_desde,112) = convert(char(8),@fe_desde,112) and
		convert(char(8),Ht.fe_hasta,112) = convert(char(8),@fe_hasta,112) and
		(Ht.pet_nrointerno *= Pt.pet_nrointerno)
*/