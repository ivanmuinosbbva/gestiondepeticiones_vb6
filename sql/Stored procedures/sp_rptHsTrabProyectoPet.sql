/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. FJS 07.01.2010 - Error del DBMS: Arithmetic overflow during implicit conversion of NUMERIC NULL value '166980000.00' to a NUMERIC field.
						  Arithmetic overflow occurred.
						  Se cambia la longitud de cada campo y variable de Numeric 10,2 a 12,2.
-002- a. FJS 02.10.2014 - Optimización de consulta.


Niveles de detalle (@detalle):

-: Petición (luego de recurso)	*

*/

use GesPet
go

print 'Creando / actualizando: sp_rptHsTrabProyectoPet'
go

if exists (select * from sysobjects where name = 'sp_rptHsTrabProyectoPet' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptHsTrabProyectoPet
go

create procedure dbo.sp_rptHsTrabProyectoPet 
	@fdesde		char(8)='NULL', 
	@fhasta		char(8)='NULL', 
	@projid		int,
	@projsubid	int,
	@projsubsid	int,
	@detalle	char(1)='1'
as 
	declare @sol_desde			smalldatetime 
	declare @sol_hasta			smalldatetime 

	if @fdesde is null
		select @fdesde = 'NULL'
	if @fhasta is null
		select @fhasta = 'NULL'

	if RTRIM(@fdesde)='NULL' 
		begin 
			select @sol_desde=dateadd(mm,-1,getdate()) 
			select @fdesde=convert(char(8),@sol_desde,112) 
		end 
	else 
		select @sol_desde=@fdesde 

	if RTRIM(@fhasta)='NULL' 
		begin 
			select @sol_hasta=dateadd(mm,1,getdate()) 
			select @fhasta=convert(char(8),@sol_hasta,112) 
		end 
	else 
		select @sol_hasta=@fhasta 
	
	-- Temporal con las peticiones vinculadas al proyecto
	SELECT pet_nrointerno
	INTO #peti
	FROM Peticion
	WHERE 
		pet_projid     = @projid and 
		pet_projsubid  = @projsubid and
		pet_projsubsid = @projsubsid
	

	CREATE TABLE #TmpRecurso(
		tcod_recurso		char(10)	null,
		tnom_recurso		char(50)	null,
		tcod_direccion		char(10)	null, 
		tcod_gerencia		char(10)	null, 
		tcod_sector			char(10)	null, 
		tcod_grupo			char(10)	null)

	insert into #TmpRecurso
		select 
			cod_recurso, 
			nom_recurso,
			cod_direccion, 
			cod_gerencia, 
			cod_sector, 
			cod_grupo
		from GesPet..Recurso

	insert into #TmpRecurso
		select 
			cod_fab, 
			nom_fab,
			cod_direccion, 
			cod_gerencia, 
			cod_sector, 
			cod_grupo
		from GesPet..Fabrica

	select
		tr.tcod_direccion,
		(select x.nom_direccion from Direccion x where x.cod_direccion = tr.tcod_direccion)		as nom_direccion,
		tr.tcod_gerencia,
		(select x.nom_gerencia from Gerencia x where x.cod_gerencia = tr.tcod_gerencia)			as nom_gerencia,
		tr.tcod_sector,
		(select x.nom_sector from Sector x where x.cod_sector = tr.tcod_sector)					as nom_sector,
		tr.tcod_grupo,
		(select x.nom_grupo from Grupo x where x.cod_grupo = tr.tcod_grupo)						as nom_grupo,
		tr.tcod_recurso,
		tr.tnom_recurso							as nom_recurso,
		ht.pet_nrointerno,
		(select p.pet_nroasignado from Peticion p where p.pet_nrointerno = ht.pet_nrointerno)	as pet_nroasignado,
		(select p.titulo from Peticion p where p.pet_nrointerno = ht.pet_nrointerno)			as titulo,
		(select p.cod_tipo_peticion from Peticion p where p.pet_nrointerno = ht.pet_nrointerno)	as cod_tipo,
		(select p.cod_clase from Peticion p where p.pet_nrointerno = ht.pet_nrointerno)			as cod_clase,
		MIN(ht.fe_desde)		as fe_desde,
		MAX(ht.fe_hasta)		as fe_hasta,
		0						as horas_asig_si,
		SUM(CONVERT(NUMERIC(12,2),ht.horas / CONVERT(NUMERIC(12,2),60)))		as horas_asig_no
	from
		HorasTrabajadas ht inner join
		#TmpRecurso tr on (tr.tcod_recurso = ht.cod_recurso)
	where
		(@fdesde is null or @fdesde='NULL' or ht.fe_desde >= @fdesde) and
		(@fhasta is null or @fhasta='NULL' or ht.fe_hasta <= @fhasta) and
		ht.trabsinasignar = 'S' and
		ht.pet_nrointerno in (
			select pet_nrointerno
			from #peti
			where pet_nrointerno = ht.pet_nrointerno)
	group by
		tr.tcod_direccion,
		tr.tcod_gerencia,
		tr.tcod_sector,
		tr.tcod_grupo,
		tr.tcod_recurso,
		tr.tnom_recurso,
		ht.pet_nrointerno
	UNION
	select
		tr.tcod_direccion,
		(select x.nom_direccion from Direccion x where x.cod_direccion = tr.tcod_direccion)		as nom_direccion,
		tr.tcod_gerencia,
		(select x.nom_gerencia from Gerencia x where x.cod_gerencia = tr.tcod_gerencia)			as nom_gerencia,
		tr.tcod_sector,
		(select x.nom_sector from Sector x where x.cod_sector = tr.tcod_sector)					as nom_sector,
		tr.tcod_grupo,
		(select x.nom_grupo from Grupo x where x.cod_grupo = tr.tcod_grupo)						as nom_grupo,
		tr.tcod_recurso,
		tr.tnom_recurso							as nom_recurso,
		ht.pet_nrointerno,
		(select p.pet_nroasignado from Peticion p where p.pet_nrointerno = ht.pet_nrointerno)	as pet_nroasignado,
		(select p.titulo from Peticion p where p.pet_nrointerno = ht.pet_nrointerno)			as titulo,
		(select p.cod_tipo_peticion from Peticion p where p.pet_nrointerno = ht.pet_nrointerno)	as cod_tipo,
		(select p.cod_clase from Peticion p where p.pet_nrointerno = ht.pet_nrointerno)			as cod_clase,
		MIN(ht.fe_desde)		as fe_desde,
		MAX(ht.fe_hasta)		as fe_hasta,
		SUM(CONVERT(NUMERIC(12,2),ht.horas / CONVERT(NUMERIC(12,2),60)))		as horas_asig_si,
		0						as horas_asig_no
	from
		HorasTrabajadas ht inner join
		#TmpRecurso tr on (tr.tcod_recurso = ht.cod_recurso)
	where
		(@fdesde is null or @fdesde='NULL' or ht.fe_desde >= @fdesde) and
		(@fhasta is null or @fhasta='NULL' or ht.fe_hasta <= @fhasta) and
		ht.trabsinasignar = 'N' and
		ht.pet_nrointerno in (
			select pet_nrointerno
			from #peti
			where pet_nrointerno = ht.pet_nrointerno)
	group by
		tr.tcod_direccion,
		tr.tcod_gerencia,
		tr.tcod_sector,
		tr.tcod_grupo,
		tr.tcod_recurso,
		tr.tnom_recurso,
		ht.pet_nrointerno
	order by
		tr.tcod_direccion,
		tr.tcod_gerencia,
		tr.tcod_sector,
		tr.tcod_grupo	
	
	return(0) 
go

grant execute on dbo.sp_rptHsTrabProyectoPet to GesPetUsr 
go

print 'Listo.'
go