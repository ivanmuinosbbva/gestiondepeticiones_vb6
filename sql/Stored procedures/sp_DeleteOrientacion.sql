/*
-000- a. FJS dd.mm.yyyy - Nuevo SP para
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteOrientacion'
go

if exists (select * from sysobjects where name = 'sp_DeleteOrientacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteOrientacion
go

create procedure dbo.sp_DeleteOrientacion
	@cod_orientacion char(2)
as 
	DELETE 
	FROM Orientacion 
	WHERE cod_orientacion = @cod_orientacion
	return(0)
go

grant execute on dbo.sp_DeleteOrientacion to GesPetUsr
go

print 'Actualización realizada.'
go
