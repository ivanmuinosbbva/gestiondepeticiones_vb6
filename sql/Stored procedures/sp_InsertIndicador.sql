/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertIndicador'
go

if exists (select * from sysobjects where name = 'sp_InsertIndicador' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertIndicador
go

create procedure dbo.sp_InsertIndicador
	@indicadorId	int,
	@indicadorNom	char(60),
	@indicadorHab	char(1)
as
	insert into GesPet.dbo.Indicador (
		indicadorId,
		indicadorNom,
		indicadorHab)
	values (
		@indicadorId,
		@indicadorNom,
		@indicadorHab)
go

grant execute on dbo.sp_InsertIndicador to GesPetUsr
go

print 'Actualización realizada.'
go
