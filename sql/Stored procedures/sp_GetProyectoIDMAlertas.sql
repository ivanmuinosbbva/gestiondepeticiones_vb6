/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMAlertas'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMAlertas' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMAlertas
go

create procedure dbo.sp_GetProyectoIDMAlertas
	@ProjId		int=null,
	@ProjSubId	int=null,
	@ProjSubSId	int=null
as
	select 
		a.ProjId,
		a.ProjSubId,
		a.ProjSubSId,
		a.alert_itm,
		a.alert_tipo,
		a.alert_desc,
		a.accion_id,
		a.alert_fe_ini,
		a.alert_fe_fin,
		a.responsables,
		a.observaciones,
		a.alert_status,
		alert_stadsc = case
			when a.alert_status = 'S' then 'Si'
			when a.alert_status = 'N' then 'No'
			else '-'
		end,
		a.alert_fecres,
		a.alert_fecha,
		a.alert_usuario
	from 
		GesPet.dbo.ProyectoIDMAlertas a
	where
		(a.ProjId = @ProjId or @ProjId is null) and
		(a.ProjSubId = @ProjSubId or @ProjSubId is null) and
		(a.ProjSubSId = @ProjSubSId or @ProjSubSId is null)
	order by
		a.ProjId,
		a.ProjSubId,
		a.ProjSubSId,
		a.alert_status,
		a.alert_tipo desc
	return(0)
go

grant execute on dbo.sp_GetProyectoIDMAlertas to GesPetUsr
go

print 'Actualización realizada.'
go