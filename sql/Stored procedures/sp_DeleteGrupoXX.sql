use GesPet
go
print 'sp_DeleteGrupoXX'
go
if exists (select * from sysobjects where name = 'sp_DeleteGrupoXX' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteGrupoXX
go
create procedure dbo.sp_DeleteGrupoXX 
           @cod_grupo   char(8) 
as 
 
     
    delete GesPet..RecursoPerfil where RTRIM(cod_nivel) = 'GRUP' and RTRIM(cod_area) = RTRIM(@cod_grupo) 
    delete GesPet..Grupo      where RTRIM(cod_grupo) = RTRIM(@cod_grupo) 
return(0) 
go



grant execute on dbo.sp_DeleteGrupoXX to GesPetUsr 
go


