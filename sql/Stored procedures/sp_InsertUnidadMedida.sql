/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertUnidadMedida'
go

if exists (select * from sysobjects where name = 'sp_InsertUnidadMedida' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertUnidadMedida
go

create procedure dbo.sp_InsertUnidadMedida
	@unimedId	char(10),
	@unimedDesc	char(60),
	@unimedSmb	char(4)
as
	insert into GesPet.dbo.UnidadMedida (
		unimedId,
		unimedDesc,
		unimedSmb)
	values (
		@unimedId,
		@unimedDesc,
		@unimedSmb)
go

grant execute on dbo.sp_InsertUnidadMedida to GesPetUsr
go

print 'Actualización realizada.'
go
