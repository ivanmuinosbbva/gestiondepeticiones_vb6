/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.07.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertCatalogoTabla'
go

if exists (select * from sysobjects where name = 'sp_InsertCatalogoTabla' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertCatalogoTabla
go

create procedure dbo.sp_InsertCatalogoTabla
	@id				int=null,
	@nombreTabla	char(40)=null,
	@baseDeDatos	char(40)=null,
	@esquema		char(40)=null,
	@DBMS			int=null,
	@servidorNombre	char(40)=null,
	@servidorPuerto	int=null,
	@app_id			char(30)=null,
	@datosSensibles	char(1)=null
as
	insert into GesPet.dbo.CatalogoTabla (
		id,
		nombreTabla,
		baseDeDatos,
		esquema,
		DBMS,
		servidorNombre,
		servidorPuerto,
		app_id,
		fe_alta,
		usr_alta,
		datosSensibles,
		visado,
		visadoFecha,
		visadoUsuario)
	values (
		@id,
		@nombreTabla,
		@baseDeDatos,
		@esquema,
		@DBMS,
		@servidorNombre,
		@servidorPuerto,
		@app_id,
		getDate(),
		suser_name(),
		@datosSensibles,
		null,
		null,
		null)
go

grant execute on dbo.sp_InsertCatalogoTabla to GesPetUsr
go
grant execute on dbo.sp_InsertCatalogoTabla to TrnCGMEnmascara
go
sp_procxmode 'dbo.sp_InsertCatalogoTabla','anymode'
go

print 'Actualización realizada.'
go
