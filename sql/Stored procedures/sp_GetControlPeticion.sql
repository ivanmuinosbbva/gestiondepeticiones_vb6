/*
*********************************************************************************************************************************
ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR
*********************************************************************************************************************************

-000- a. FJS 17.03.2010 - Nuevo SP para visualizar el estado de una petici�n respecto de lo pendiente para pasaje a Producci�n.
-001- a. FJS 28.10.2010 - Control T710: si la cantidad de grupos a controlar es cero (porque son los grupos exceptuados o no hay, entonces
						  el mensaje es que no aplica el control. Antes se marcaba como OK.
						  Este documento es OBLIGATORIO para todos los grupos (sin contar las excepciones).
						  Adem�s, en caso de ser solo uno, indico el nombre del grupo que adeuda el documento.
-001- b. FJS 29.10.2010 - Control CG04: seg�n Ariotti, si tiene documento CG04 la validaci�n es si existe un ALCP/ALCA/EML1
						  de fecha posterior o igual al CG04 (originalmente solo corroboraba ALCP/ALCA).
-001- c. FJS 29.10.2010 - Control C204: con que al menos uno de los grupos tenga C204, el control es v�lido.
-002- a. FJS 25.07.2011 - Nuevo control: para DyD (C104 - Dictamen de Seg. Inf.).
-003- a. FJS 21.11.2014 - Nuevo: se agrega a la tabla de excepciones del control del T710 los grupos de la Gerencia de Sistemas (ex DYD) no homologables.
-003- b. FJJ 10.04.2015 - Nuevo: se optimiza la carga de esta tabla de excepciones para evitar el "join".
-004- a. FJS 07.04.2015 - Nuevo: se corrige la validaci�n cuando el OK a las pruebas de usuario es parcial. En este caso, solo se valida que al menos un grupo t�cnico
						  se encuentre en ejecuci�n y dicho grupo tenga su C204 y su T710. El resto de los grupos pueden "deberlo", pero la petici�n queda v�lida
						  para pasaje a producci�n.
-004- b. FJS 07.04.2015 - Modificaci�n: se actualizan las leyendas.
-004- c. FJS 08.04.2015 - Modificaci�n: se agrega el control para el conforme de supervisor (OKPP, OKPF).
-005- a. FJS 10.04.2015 - Nuevo: se optimiza la carga de este valor y del cursor.
-005- b. FJS 10.04.2015 - Nuevo: esta tabla se elimina porque ya no se controla de esta manera los grupos que deben el T710.
-006- a. FJS 26.12.2016 - Nuevo: se habilitan los controles de validaci�n t�cnica para Seguridad inform�tica y Arquitectura.

*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetControlPeticion'
go

if exists (select * from sysobjects where name = 'sp_GetControlPeticion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetControlPeticion
go

create procedure dbo.sp_GetControlPeticion
	@pet_nrointerno		int,
	@cod_recurso		char(10),
	@modo				char(1)='1'
as 
	-- Variables para el proceso
	declare @pet_tipo				char(3)
	declare @pet_clase				char(4)
	declare @pet_modelo				int
	declare @pet_regulatorio		char(1)
	declare @pet_imptec				char(1)
	declare @pet_estado				char(6)
	declare @ctrol_mensaje			char(100)
	declare @cambio_alcance			smalldatetime
	declare @conforme_homo			char(1)
	declare @requiere_C100			char(1)
	declare @requiere_P950			char(1)
	declare @estado_final			char(1)
	declare @pet_conforme_f			char(1)
	declare @pet_conforme_p			char(1)
	declare @pet_conforme_s			char(1)			-- add -004- c.
	declare @pet_proyecto_idm		char(1)
	declare @cod_bpar				char(10)		-- Referente de sistemas de la petici�n.
	declare @nom_sector				char(80)		-- upd -005- a.
	declare @nom_grupo				char(80)		-- upd -005- a.
	declare @cod_estado				char(6)
	declare @origen_incidencia		varchar(20)

	declare @c_pet_nrointerno		int
	declare @c_cod_sector			char(8)
	declare @c_cod_grupo			char(8)
	declare @grupos_tecnicos		int
	declare @grupos_tecnicos_ejecuc	int
	declare @total_grupos			int
	declare @total_grupos_exc		int
	declare @contador				int
	declare @estado					varchar(30)		-- add -004- a.

	declare @control1				int
	declare @control3				int				-- Variable de control para ejecutar las validaciones t�cnicas (Seg. Inform�tica y Arquitectura).
	declare @fechaControlVT			smalldatetime	-- variable de control para la validaci�n t�cnica
	declare @pet_fe_pedido			smalldatetime	-- Variable para fecha de alta de la petici�n
	declare @textosVarios			varchar(255)

	declare @usuario_OK				varchar(80)

	--{ add -005- a. Total de grupos t�cnicos activos asociados a la petici�n y grupos t�cnicos activos
	select @total_grupos = (
		select count(1) 
		from 
			GesPet..PeticionGrupo pg 
		where
			pg.pet_nrointerno = @pet_nrointerno and
			pg.cod_estado not in ('RECHAZ','CANCEL','RECHTE','TERMIN','ANEXAD','ANULAD') and 
			pg.cod_grupo in (
				select g.cod_grupo
				from Grupo g
				where 
					g.cod_grupo = pg.cod_grupo and
					g.grupo_homologacion NOT IN ('H','1','2')))
					--g.grupo_homologacion <> 'H'))

	select @grupos_tecnicos = (
		select count(1) 
		from 
			GesPet..PeticionGrupo pg 
		where
			pg.pet_nrointerno = @pet_nrointerno and
			pg.cod_estado not in ('RECHAZ','CANCEL','RECHTE','TERMIN','ANEXAD','ANULAD') and 
			pg.cod_grupo in (
				select g.cod_grupo
				from Grupo g
				where 
					g.cod_grupo = pg.cod_grupo and
					g.grupo_homologacion = 'S'))
	--}

	-- En este cursor me guardo todos los grupos DYD "t�cnicos" en estado activo
	declare CursGrupo cursor for
		select  
			pg.pet_nrointerno, 
			pg.cod_sector, 
			pg.cod_grupo
		from GesPet..PeticionGrupo pg 
		where
			pg.pet_nrointerno = @pet_nrointerno and
			pg.cod_estado not in ('RECHAZ','CANCEL','RECHTE','TERMIN','ANEXAD','ANULAD') and 
			pg.cod_grupo in (
				select g.cod_grupo
				from Grupo g
				where 
					g.cod_grupo = pg.cod_grupo and
					g.grupo_homologacion = 'S')
	for read only
	
	-- Temporal de sectores y/o grupos exceptuados del control de T710
	create table #Excepciones ( 
		tipo		char(3)		null,
		codigo		char(8)		null)
 
	-- { add -003- b.
	insert into #Excepciones
		select 
			'GRU'			as tipo,
			g.cod_grupo		as codigo
		from Grupo g
		where g.flg_habil = 'S' and 
			g.es_ejecutor = 'S' and 
			g.grupo_homologacion = 'N' and
			g.cod_sector in (
				select s.cod_sector
				from Sector s
				where 
					s.cod_sector = g.cod_sector and
					s.cod_gerencia = 'DESA')
	--}

	-- Temporal para el detalle final de controles realizados a la petici�n.
	create table #Control ( 
		codigo			int			null,
		nivel			char(15)	null,
		control			char(60)	null,
		observaciones	char(100)	null,
		estado			char(20)	null,
		item			int			null)

	select @estado_final   = 'S'
	select @pet_conforme_p = 'N'
	select @pet_conforme_f = 'N'
	select @pet_conforme_s = 'N'		-- add -004- c.

	-- Determino si la petici�n tiene conforme a las pruebas de usuario parciales y/o finales
	if exists (select a.pet_nrointerno from GesPet..PeticionConf a where a.pet_nrointerno = @pet_nrointerno and a.ok_tipo = 'TESP')
		select @pet_conforme_p = 'S'
	
	if exists (select a.pet_nrointerno from GesPet..PeticionConf a where a.pet_nrointerno = @pet_nrointerno and a.ok_tipo = 'TEST')
		select @pet_conforme_f = 'S'

	--{ add -004- c.
	if exists (select a.pet_nrointerno from GesPet..PeticionConf a where a.pet_nrointerno = @pet_nrointerno and a.ok_tipo in ('OKPP','OKPF'))
		select @pet_conforme_s = 'S'
	--}

	insert into #Control values (1, 'Petici�n', 'Clasificaci�n de la petici�n', '', ' - ',						10)
	insert into #Control values (2,	'Petici�n', 'Indicador de regulatorio especificado', '', ' - ',				20)
	insert into #Control values (14,'Petici�n', 'Indicador de impacto tecnol�gico', '', ' - ',					25)
	insert into #Control values (3, 'Petici�n', 'Descripci�n del pedido', '', ' - ',							30)
	insert into #Control values (19,'Petici�n', 'Cuestionario para validaci�n t�cnica', '', ' - ',				31)
	insert into #Control values (20,'Petici�n', 'Grupo de Seguridad inform�tica asignado', '', ' - ',			32)
	insert into #Control values (21,'Petici�n', 'Grupo de Arquitectura t�cnica asignado', '', ' - ',			33)
	insert into #Control values (22,'Petici�n', 'Conforme t�cnico de Seguridad Inform�tica', '', ' - ',			34)
	insert into #Control values (23,'Petici�n', 'Conforme t�cnico de Arquitectura', '', ' - ',					35)
	insert into #Control values (4, 'Petici�n', 'Documento de alcance (C100 / P950)', '', ' - ',				40)
	insert into #Control values (5, 'Petici�n', 'Conforme a la documentaci�n de alcance', '', ' - ',			50)
	insert into #Control values (6, 'Petici�n', 'Cambio en el alcance (CG04)', '', ' - ',						60)
	insert into #Control values (7, 'Petici�n', 'Aceptaci�n de cambios en el alcance', '', ' - ',				70)
	insert into #Control values (15,'Petici�n', 'Grupo homologador asignado', '', ' - ',						75)
	insert into #Control values (12,'Petici�n', 'Grupos t�cnicos asociados', '', ' - ',							80)
	insert into #Control values (13,'Petici�n', 'Grupos t�cnicos en ejecuci�n', '', ' - ',						90)
	insert into #Control values (8, 'Grupo', 'Documento Casos de Prueba (C204)', '', ' - ',						100)
	insert into #Control values (9, 'Grupo', 'Documento Aplic. de Principios de Desarrollo (T710)', '', ' - ',	110)
	insert into #Control values (10,'Petici�n', 'Conforme de pruebas de usuario', '', ' - ',					120)
	insert into #Control values (11,'Petici�n', 'Conformes / observaciones de Homologaci�n', '', ' - ',			130)
	insert into #Control values (16,'Petici�n', 'Estado de la petici�n', '', ' - ',								140)
	insert into #Control values (99,'', 'ESTADO PARA PASAJE A PRODUCCION', '', 'No v�lida',						990)

	-- Obtengo los datos principales de la petici�n
	select
		@pet_tipo			= cod_tipo_peticion,
		@pet_clase			= cod_clase,
		@pet_modelo			= pet_sox001,
		@pet_regulatorio	= pet_regulatorio,
		@pet_imptec			= pet_imptech,
		@pet_estado			= cod_estado,
		@cod_bpar			= cod_bpar,
		@pet_fe_pedido		= fe_pedido,
		@origen_incidencia  = sgi_incidencia,
		@pet_proyecto_idm	=	
		case
			when exists (
				select pidm.ProjId 
				from GesPet..ProyectoIDM pidm 
				where pidm.ProjId = a.pet_projid and pidm.ProjSubId = a.pet_projsubid and pidm.ProjSubSId = a.pet_projsubsid) then 'S'
			else 'N'
		end
	from GesPet..Peticion a
	where a.pet_nrointerno = @pet_nrointerno

	-- Determino si para clases ATEN, OPTI, SPUF o CORR es necesario conforme de homologaci�n en caso que la petici�n
	-- tenga un conforme de usuario otorgado por el l�der (1: control / 0: sin control).
	select @control1 = (select var_numero from GesPet..Varios where var_codigo = 'CTRLPRD1')
	-- Determino si se realiza el control exigiendo el documento C104 - Dictamen de Seg. Inf. (0:No / 1:Si)
	
	/* 
		Determino si se realiza el control de validaci�n t�cnica p/ Seg. Inf. y Arquitectura (0:No / 1:Si)
		Para la implantaci�n, esto se habilita s�lo para peticiones dadas de alta luego de una fecha de control (por defecto 02/06/17).
	*/
	select @control3 = (select var_numero from GesPet..Varios where var_codigo = 'CTRLPET1')
	if (@control3 = 1)
		begin
			select @fechaControlVT = '20170602'		-- Por defecto
			select @fechaControlVT = var_fecha
			from Varios
			where var_codigo = 'CTRLPET1'
			
			if (@pet_fe_pedido < @fechaControlVT)
				select @control3 = 0
			else
				select @control3 = 1
		end

	if charindex(@pet_estado,'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD')>0
		 begin
			update #Control set observaciones = '-', estado = ' - '
			update #Control set observaciones = 'Petici�n en estado terminal', estado = 'N/A' where codigo = 16
			select @estado_final = 'N'
		 end
	else
		begin
			-- Si no existe ning�n grupo "t�cnico" asociado a la petici�n, entonces la petici�n no necesita este control
			if @grupos_tecnicos = 0		-- add -005- a.
				begin
					update #Control set observaciones = '-', estado = ' - '
					update #Control set observaciones = 'No hay grupos t�cnicos', estado = 'N/A' where codigo = 99
					select @estado_final = 'N'
				end
			else
				begin
					-- *******************************************************************************************************************
					-- 1. Clasificaci�n
					-- *******************************************************************************************************************
					if @pet_clase is null or @pet_clase = 'SINC' or @pet_clase = ''
						begin
							update GesPet..#Control 
							set observaciones = 'Petici�n a�n sin clasificar', estado = 'Pendiente' where codigo = 1
							select @estado_final = 'N'
						end
					else
						update GesPet..#Control 
						set observaciones = @pet_tipo + ', ' + @pet_clase, estado = 'OK' where codigo = 1

					-- *******************************************************************************************************************
					-- 2. Regulatorio
					-- *******************************************************************************************************************
					if @pet_regulatorio is null or @pet_regulatorio = '' or @pet_regulatorio = '-'
						begin
							update GesPet..#Control 
							set observaciones = 'Debe ser definido el atributo regulatorio para la petici�n', estado = 'Pendiente' where codigo = 2
							select @estado_final = 'N'
						end
					else
						update GesPet..#Control 
						set observaciones = @pet_regulatorio, estado = 'OK' where codigo = 2

					-- *******************************************************************************************************************
					-- 3. Descripci�n
					-- *******************************************************************************************************************
					if not exists (select mem_texto from GesPet..PeticionMemo where pet_nrointerno = @pet_nrointerno and mem_campo = 'DESCRIPCIO' and mem_texto <> '')
						begin
							update GesPet..#Control 
							set observaciones = 'Falta agregar una descripci�n para la petici�n', estado = 'Pendiente' where codigo = 3
							select @estado_final = 'N'
						end
					else
						update GesPet..#Control 
						set observaciones = '', estado = 'OK' where codigo = 3

					-- *******************************************************************************************************************
					-- 12. Grupo t�cnico (ex DYD homologable ) asociado a la petici�n (NUEV/EVOL/OPTI: 12 / OTHERWISE: 13)
					-- *******************************************************************************************************************
					if exists (
						select pet_nrointerno 
						from GesPet..PeticionGrupo a inner join GesPet..Grupo b on (a.cod_grupo = b.cod_grupo)
						where pet_nrointerno = @pet_nrointerno and b.grupo_homologacion = 'S')
							begin
								if exists (
									select pet_nrointerno 
									from GesPet..PeticionGrupo a inner join GesPet..Grupo b on (a.cod_grupo = b.cod_grupo)
									where pet_nrointerno = @pet_nrointerno and b.grupo_homologacion = 'S' and a.cod_estado not in ('RECHAZ','CANCEL','RECHTE','TERMIN','ANEXAD','ANULAD'))
										update GesPet..#Control 
										set estado = 'OK', observaciones = LTRIM(RTRIM(CONVERT(CHAR(8),@grupos_tecnicos))) + ' de ' + LTRIM(RTRIM(CONVERT(CHAR(8),@total_grupos))) where codigo = 12
								else
									begin
										update GesPet..#Control 
										set estado = 'Pendiente', observaciones = 'No hay ning�n grupo t�cnico activo' where codigo = 12
										select @estado_final = 'N'
									end
							end
					else
						begin
							update GesPet..#Control 
							set estado = 'Pendiente', observaciones = 'No hay ning�n grupo t�cnico asociado' where codigo = 12
							select @estado_final = 'N'
						end

					-- *******************************************************************************************************************
					-- 19. Validaci�n t�cnica (completitud del cuestionario t�cnico)
					-- *******************************************************************************************************************
					if @control3 = 1
						if charindex(@pet_clase, 'NUEV|EVOL|') > 0
							begin
								-- Busco existencia del respuestas al cuestionario de validaci�n t�cnica
								if not exists (
									select pet_nrointerno 
									from PeticionValidacion 
									where pet_nrointerno = @pet_nrointerno and valitemvalor is not null AND LTRIM(RTRIM(valitemvalor)) <> '')
										begin
											declare @referenteSistemas	varchar(100)
											select @referenteSistemas = r.nom_recurso
											from Recurso r
											where r.cod_recurso = @cod_bpar

											update GesPet..#Control 
											set observaciones = 'El referente de sistemas debe completar el cuestionario (' + LTRIM(RTRIM(@referenteSistemas)) + ')', estado = 'Pendiente' where codigo = 19
											
											select @estado_final = 'N'
										end
								else
									begin
										update GesPet..#Control 
										set observaciones = 'Completado', estado = 'OK' where codigo = 19
									end
							end
						else
							begin
								if @pet_clase = 'SINC'
									update GesPet..#Control 
									set observaciones = 'N/A' where codigo = 19
								else
									update GesPet..#Control 
									set observaciones = 'No requiere por la clase' where codigo = 19
							end
					else
						update GesPet..#Control 
						set observaciones = 'N/A' where codigo = 19
					--}

					-- *******************************************************************************************************************
					-- 20. Grupo de Seguridad inform�tica asignado
					-- *******************************************************************************************************************
					if @control3 = 1 and charindex(@pet_clase, 'NUEV|EVOL|') > 0
						if exists (
							select pet_nrointerno 
							from PeticionValidacion 
							where pet_nrointerno = @pet_nrointerno and 
								valid = 2 and valitemvalor is not null)
							if exists (
								select pet_nrointerno 
								from PeticionValidacion 
								where pet_nrointerno = @pet_nrointerno and 
									valid = 2 and 
									LTRIM(RTRIM(valitemvalor)) = 'S')

								if exists (select pet_nrointerno 
											from GesPet..PeticionGrupo a inner join GesPet..Grupo b on (a.cod_grupo = b.cod_grupo)
											where pet_nrointerno = @pet_nrointerno and b.grupo_homologacion = '1')
									begin
										select 
											@nom_grupo = g.nom_grupo, 
											@c_cod_sector = g.cod_sector
										from Grupo g where g.grupo_homologacion = '1'
										select @nom_sector = (select s.nom_sector from Sector s where s.cod_sector = @c_cod_sector)

										update GesPet..#Control 
										set estado = 'OK', observaciones = LTRIM(RTRIM(@nom_sector)) + ' � ' + LTRIM(RTRIM(@nom_grupo)) where codigo = 20
									end
								else
									begin
										update GesPet..#Control 
										set estado = 'Pendiente', observaciones = 'Falta el grupo de Seguridad Inform�tica' where codigo = 20
										select @estado_final = 'N'
									end
							else
								update GesPet..#Control set observaciones = 'No requerido por las respuestas dadas' where codigo = 20
						else
							update GesPet..#Control set observaciones = 'N/A' where codigo = 20
					else
						update GesPet..#Control set observaciones = 'N/A' where codigo = 20

					-- *******************************************************************************************************************
					-- 21. Grupo de Arquitectura T�cnica asignado
					-- *******************************************************************************************************************
					if @control3 = 1 and charindex(@pet_clase, 'NUEV|EVOL|') > 0
						if exists (
							select pet_nrointerno 
							from PeticionValidacion 
							where pet_nrointerno = @pet_nrointerno and 
								valid = 1 and valitemvalor is not null)
							
							if exists (
								select pet_nrointerno 
								from PeticionValidacion 
								where pet_nrointerno = @pet_nrointerno and 
									valid = 1 and LTRIM(RTRIM(valitemvalor)) = 'S')

									if exists (select pet_nrointerno 
												from GesPet..PeticionGrupo a inner join GesPet..Grupo b on (a.cod_grupo = b.cod_grupo)
												where pet_nrointerno = @pet_nrointerno and b.grupo_homologacion = '2')
										begin
											select 
												@nom_grupo = g.nom_grupo, 
												@c_cod_sector = g.cod_sector
											from Grupo g where g.grupo_homologacion = '2'
											select @nom_sector = (select s.nom_sector from Sector s where s.cod_sector = @c_cod_sector)

											update GesPet..#Control 
											set estado = 'OK', observaciones = LTRIM(RTRIM(@nom_sector)) + ' � ' + LTRIM(RTRIM(@nom_grupo)) where codigo = 21
										end
									else
										begin
											update GesPet..#Control 
											set estado = 'Pendiente', observaciones = 'Falta el grupo de Arquitectura T�cnica' where codigo = 21
											select @estado_final = 'N'
										end
							else
								update GesPet..#Control set observaciones = 'No requerido por las respuestas dadas' where codigo = 21
						else
							update GesPet..#Control set observaciones = 'N/A' where codigo = 21
					else
						update GesPet..#Control set observaciones = 'N/A' where codigo = 21
					
					--{ add -002- a.
					-- *******************************************************************************************************************
					-- 22. Conforme a la validaci�n t�cnica de Seguridad Inform�tica
					-- *******************************************************************************************************************
					if @control3 = 1 and charindex(@pet_clase, 'NUEV|EVOL|') > 0 and 
						exists (select pet_nrointerno from PeticionValidacion where pet_nrointerno = @pet_nrointerno and valid = 2 and valitemvalor = 'S')
						begin
							select @usuario_OK = ''
							-- Busco existencia del conforme 
							if not exists (select pet_nrointerno  
											from GesPet..PeticionConf
											where pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(SUBSTRING(ok_tipo,1,4))), 'SIOK|SIOB|SIRE|')>0)
								begin
									update GesPet..#Control 
									set observaciones = 'Falta el conforme de Seguridad Inform�tica', estado = 'Pendiente' where codigo = 22
									select @estado_final = 'N'
								end
							else
								begin
									select @usuario_OK = LTRIM(RTRIM((select x.nom_recurso from Recurso x where x.cod_recurso = pc.audit_user)))
									from GesPet..PeticionConf pc
									where pc.pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(SUBSTRING(ok_tipo,1,4))), 'SIOK|SIOB|SIRE|')>0 
									and pc.audit_date = (
										select MAX(pc.audit_date)
										from PeticionConf pc
										where pc.pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(SUBSTRING(ok_tipo,1,4))), 'SIOK|SIOB|SIRE|')>0)

									update GesPet..#Control 
									set observaciones = 'Otorgado por ' + LTRIM(RTRIM(@usuario_OK)), estado = 'OK' where codigo = 22
								end
						end
					else
						begin
							update GesPet..#Control 
							set observaciones = 'N/A' where codigo = 22
						end
					--}
					
					--{ add -006- a.
					-- *******************************************************************************************************************
					-- 18. Conforme a la validaci�n t�cnica de Arquitectura
					-- *******************************************************************************************************************
					if @control3 = 1 and charindex(@pet_clase, 'NUEV|EVOL|') > 0 and 
						exists (select pet_nrointerno from PeticionValidacion where pet_nrointerno = @pet_nrointerno and valid = 1 and valitemvalor = 'S')
						begin
							select @usuario_OK = ''
							-- Busco existencia del conforme 
							if not exists (select pet_nrointerno  
											from GesPet..PeticionConf
											where pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(SUBSTRING(ok_tipo,1,4))), 'AROK|AROB|ARRE|')>0)
								begin
									update GesPet..#Control 
									set observaciones = 'Falta el conforme de Arquitectura', estado = 'Pendiente' where codigo = 23
									select @estado_final = 'N'
								end
							else
								begin
									select @usuario_OK = LTRIM(RTRIM((select x.nom_recurso from Recurso x where x.cod_recurso = pc.audit_user)))
									from GesPet..PeticionConf pc
									where pc.pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(SUBSTRING(ok_tipo,1,4))), 'AROK|AROB|ARRE|')>0 
									and pc.audit_date = (
										select MAX(pc.audit_date)
										from PeticionConf pc
										where pc.pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(SUBSTRING(ok_tipo,1,4))), 'AROK|AROB|ARRE|')>0)

									update GesPet..#Control 
									set observaciones = 'Otorgado por ' + LTRIM(RTRIM(@usuario_OK)), estado = 'OK' where codigo = 23
								end
						end
					else
						begin
							update GesPet..#Control 
							set observaciones = 'N/A' where codigo = 23
						end

					-- *******************************************************************************************************************
					-- 14. Indicador de Impacto Tecnol�gico
					-- *******************************************************************************************************************
					if charindex(@pet_clase, 'NUEV|EVOL|OPTI') > 0
						if (@pet_imptec = '' or @pet_imptec = '-'  or @pet_imptec is null)
							begin
								update GesPet..#Control 
								set estado = 'Pendiente', observaciones = 'Falta especificar Impacto Tecnol�gico' where codigo = 14
								select @estado_final = 'N'
							end
						else
							update GesPet..#Control 
							set estado = 'OK', observaciones = LTRIM(RTRIM(@pet_imptec)) where codigo = 14
					else
						update GesPet..#Control 
						set estado = 'OK', observaciones = LTRIM(RTRIM(@pet_imptec)) where codigo = 14
					
					-- *******************************************************************************************************************
					-- 13. Grupo homologable (DYD) en Ejecuci�n
					-- *******************************************************************************************************************
					if exists (
						select pet_nrointerno 
						from GesPet..PeticionGrupo a inner join GesPet..Grupo b on (a.cod_grupo = b.cod_grupo)
						where pet_nrointerno = @pet_nrointerno and b.grupo_homologacion = 'S' and a.cod_estado = 'EJECUC')
							begin
								select @grupos_tecnicos_ejecuc = (
									select count(1)
									from PeticionGrupo pg inner join Grupo b on (pg.cod_grupo = b.cod_grupo)
									where pg.pet_nrointerno = @pet_nrointerno and b.grupo_homologacion = 'S' and pg.cod_estado = 'EJECUC')
								
								select @textosVarios = LTRIM(RTRIM(CONVERT(CHAR(8),@grupos_tecnicos_ejecuc))) + ' de ' + LTRIM(RTRIM(CONVERT(CHAR(8),@grupos_tecnicos)))
								
								update GesPet..#Control 
								set estado = 'OK', observaciones = LTRIM(RTRIM(@textosVarios)) where codigo = 13
							end
					else
						if exists (
							select pet_nrointerno 
							from GesPet..PeticionGrupo a inner join GesPet..Grupo b on (a.cod_grupo = b.cod_grupo)
							where pet_nrointerno = @pet_nrointerno and b.grupo_homologacion = 'S' and a.cod_estado = 'SUSPEN') and 
						   exists (
							-- Si estuvo al menos una vez en ejecuci�n
							select pet_nrointerno
							from Historial h
							where h.pet_nrointerno = @pet_nrointerno and h.pet_estado = 'EJECUC')
								begin
									update GesPet..#Control 
									set estado = 'OK' where codigo = 13
								end
						else
							begin
								update GesPet..#Control 
									set estado = 'Pendiente', observaciones = 'Ning�n grupo t�cnico en Ejecuci�n' where codigo = 13
								select @estado_final = 'N'
							end
					-- *******************************************************************************************************************
					-- 15. Grupo homologador asignado
					-- *******************************************************************************************************************
					if exists (select pet_nrointerno 
								from GesPet..PeticionGrupo a inner join GesPet..Grupo b on (a.cod_grupo = b.cod_grupo)
								where pet_nrointerno = @pet_nrointerno and b.grupo_homologacion = 'S')
						if exists (select pet_nrointerno from GesPet..PeticionGrupo a inner join GesPet..Grupo b on (a.cod_grupo = b.cod_grupo)
									where pet_nrointerno = @pet_nrointerno and b.grupo_homologacion = 'H')
							begin
								-- Guarda el nombre del grupo homologador
								select @nom_grupo = ''
								select @nom_sector = ''
								select @c_cod_sector = ''
								
								select 
									@nom_grupo = g.nom_grupo, 
									@c_cod_sector = g.cod_sector
								from Grupo g where g.grupo_homologacion = 'H'

								select @nom_sector = (select s.nom_sector from Sector s where s.cod_sector = @c_cod_sector)

								update GesPet..#Control 
								set estado = 'OK', observaciones = LTRIM(RTRIM(@nom_sector)) + ' � ' + LTRIM(RTRIM(@nom_grupo)) where codigo = 15
							end
						else
							begin
								update GesPet..#Control 
								set estado = 'Pendiente', observaciones = 'Falta el grupo homologador' where codigo = 15
								select @estado_final = 'N'
							end
					else
						begin
							update GesPet..#Control 
							set observaciones = 'N/A' where codigo = 15
						end

					if charindex(@pet_clase, 'NUEV|EVOL|OPTI') > 0
						begin
							-- *******************************************************************************************************************
							-- 40. Documentaci�n de alcance (Impacto tec. y/o Proyecto = C100 / Resto: P950)
							-- *******************************************************************************************************************
							--if @pet_imptec = 'S' or @pet_tipo = 'PRJ'		-- ACA!
							if (@pet_imptec = 'S' AND charindex(@pet_tipo,'PRJ|NOR|ESP|')>0)
								begin
									select @requiere_C100 = 'S'
									-- Busco existencia del C100
									if not exists (select pet_nrointerno from GesPet..PeticionAdjunto where pet_nrointerno = @pet_nrointerno and adj_tipo = 'C100')
										begin
											update GesPet..#Control 
											set observaciones = 'Falta documento de alcance tipo C100', estado = 'Pendiente' where codigo = 4
											select @estado_final = 'N'
										end
									else
										begin
											-- Determino qui�n lo adjunt�
											select @textosVarios = LTRIM(RTRIM((select x.nom_recurso from Recurso x where x.cod_recurso = pa.audit_user))) + ' (' + ltrim(rtrim(pa.adj_tipo)) + ')'
											from GesPet..PeticionAdjunto pa
											where pa.pet_nrointerno = @pet_nrointerno and ltrim(rtrim(pa.adj_tipo))='C100' 
											and pa.audit_date = (
												select MAX(pa.audit_date)
												from GesPet..PeticionAdjunto pa
												where pa.pet_nrointerno = @pet_nrointerno and ltrim(rtrim(pa.adj_tipo))='C100')

											update GesPet..#Control 
											set observaciones = LTRIM(RTRIM(@textosVarios)), estado = 'OK' where codigo = 4
										end
								end
							else
								begin
									select @requiere_P950 = 'S'		-- Busco existencia del P950
									if not exists (select pet_nrointerno from GesPet..PeticionAdjunto where pet_nrointerno = @pet_nrointerno and adj_tipo = 'P950')
										begin
											update GesPet..#Control 
											set observaciones = 'Falta documento de alcance tipo P950', estado = 'Pendiente' where codigo = 4
											select @estado_final = 'N'
										end
									else
										begin
											-- Determino qui�n lo adjunt�
											select @textosVarios = 'Subido por ' + LTRIM(RTRIM((select x.nom_recurso from Recurso x where x.cod_recurso = pa.audit_user))) + ' (' + ltrim(rtrim(pa.adj_tipo)) + ')'
											from GesPet..PeticionAdjunto pa
											where pa.pet_nrointerno = @pet_nrointerno and ltrim(rtrim(pa.adj_tipo))='P950' 
											and pa.audit_date = (
												select MAX(pa.audit_date)
												from GesPet..PeticionAdjunto pa
												where pa.pet_nrointerno = @pet_nrointerno and ltrim(rtrim(pa.adj_tipo))='P950')

											update GesPet..#Control 
											set observaciones = LTRIM(RTRIM(@textosVarios)), estado = 'OK' where codigo = 4
										end
								end
							-- *******************************************************************************************************************
							-- 5. Conforme a la documentaci�n de alcance
							-- *******************************************************************************************************************
							if (@requiere_C100 = 'S' or @requiere_P950 = 'S')
								begin
									--declare @usuario_OK		char(50)
									select @usuario_OK = ''
									if not exists (
											select pet_nrointerno  
											from GesPet..PeticionConf
											where pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(ok_tipo)), 'ALCA|ALCP')>0)
										begin
											update GesPet..#Control 
											set observaciones = 'Falta conforme a la documentaci�n de alcance', estado = 'Pendiente' where codigo = 5
											select @estado_final = 'N'
										end
									else
										begin
											select @usuario_OK = 'Otorgado por ' + LTRIM(RTRIM((select x.nom_recurso from Recurso x where x.cod_recurso = pc.audit_user))) + ' (' + LTRIM(RTRIM(pc.ok_tipo)) + ')'
											from GesPet..PeticionConf pc
											where pc.pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(ok_tipo)), 'ALCA|ALCP') > 0  
											and pc.audit_date = (
												select MAX(pc.audit_date)
												from PeticionConf pc
												where pc.pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(ok_tipo)), 'ALCA|ALCP') > 0)
											--group by pet_nrointerno

											update GesPet..#Control 
											set observaciones = LTRIM(RTRIM(@usuario_OK)), estado = 'OK' where codigo = 5
										end
								end
							-- *******************************************************************************************************************
							-- 6. Cambios en la documentaci�n de alcance (si hay CG04, el ALCA/ALCP debe ser igual o posterior al CG04)
							-- *******************************************************************************************************************
							if exists (select pet_nrointerno from GesPet..PeticionAdjunto where pet_nrointerno = @pet_nrointerno and adj_tipo = 'CG04')
								if not exists (select pet_nrointerno from GesPet..PeticionConf where pet_nrointerno = @pet_nrointerno and ok_tipo in ('ALCA','ALCP'))
									begin
										update GesPet..#Control 
										set observaciones = 'Falta conforme a la documentaci�n de alcance', estado = 'Pendiente' where codigo = 6
										--set observaciones = 'N/A', estado = ' - ' where codigo = 6
									end
								else
									begin
										-- Fecha de subida del documento de cambio de alcance
										select @cambio_alcance = audit_date
										from GesPet..PeticionAdjunto
										where pet_nrointerno = @pet_nrointerno and adj_tipo = 'CG04'

										-- Control de la fecha del ALCA/ALCP/EML1 respecto del CG04
										begin
											if (not exists (select x.pet_nrointerno from GesPet..PeticionConf x where x.pet_nrointerno = @pet_nrointerno and x.ok_tipo in ('ALCA','ALCP') and x.audit_date >= @cambio_alcance)) and
												(not exists (select x.pet_nrointerno from GesPet..PeticionAdjunto x where x.pet_nrointerno = @pet_nrointerno and x.adj_tipo = 'EML1' and x.audit_date >= @cambio_alcance))		-- add -001- b.
												begin
													update GesPet..#Control 
													set observaciones = 'El conforme de alcance o mail debe ser posterior al CG04', estado = 'Pendiente' where codigo = 6
													select @estado_final = 'N'
												end
											else
												update GesPet..#Control 
												set observaciones = '', estado = 'OK' where codigo = 6
										end
									end
							else
								update GesPet..#Control 
								set observaciones = 'N/A', estado = ' - ' where codigo = 6

							-- *******************************************************************************************************************
							-- 7. Aceptaci�n de cambios en el alcance 
							--	  (por ahora no se requiere aceptaci�n a los cambios del alcance)
							-- *******************************************************************************************************************
							update GesPet..#Control 
							set observaciones = 'N/A' where codigo = 7

							-- *******************************************************************************************************************
							-- 100. Documento de casos de prueba (C204)
							-- *******************************************************************************************************************
							select @contador = 0
							select @estado = ''				-- add -004- a.

							open CursGrupo 
							fetch CursGrupo into
								@c_pet_nrointerno,
								@c_cod_sector,
								@c_cod_grupo
								while (@@sqlstatus = 0) 
									begin
										if exists (select pet_nrointerno 
													from GesPet..PeticionAdjunto 
													where 
														pet_nrointerno = @c_pet_nrointerno and 
														((cod_sector = @c_cod_sector and cod_grupo = '') OR 
														cod_sector = @c_cod_sector and 
														cod_grupo = @c_cod_grupo) and 
														adj_tipo = 'C204')
											-- Guarda el total de aquellos que tienen el C204
											select @contador = @contador + 1
										else
											begin
												-- Guarda el nombre del sector y el grupo en caso que sea el �nico al que le falta el C204
												--{ add -005- a.
												select @nom_sector = s.nom_sector from Sector s where s.cod_sector = @c_cod_sector
												select @nom_grupo = g.nom_grupo from Grupo g where g.cod_grupo = @c_cod_grupo
												--}
											end
										fetch CursGrupo into
											@c_pet_nrointerno,
											@c_cod_sector,
											@c_cod_grupo
									end
							close CursGrupo
						
							--{ add -001- a.
							if @grupos_tecnicos = 0	-- No hay grupos t�cnicos en estado activo
								update GesPet..#Control 
								set observaciones = 'No hay grupos para este control', estado = ' - ' where codigo = 8
							else
							--}
								if @contador = @grupos_tecnicos
									update GesPet..#Control 
									set observaciones = '', estado = 'OK' where codigo = 8
								else
									if @contador = 0 and @grupos_tecnicos > 1 
									--if @contador = 0
										begin
											update GesPet..#Control 
											set observaciones = 'Todos los grupos t�cnicos', estado = 'Pendiente' where codigo = 8		-- upd -007- b. Se cambia "Todos los grupos" por "Todos los grupos t�cnicos"
											select @estado_final = 'N'
										end
									else
										begin
											--{ add -004- a.
											--select @estado_final = 'N'
											select @estado = 'Pendiente'

											if (@pet_conforme_f = 'S')
												begin
													--select @estado_final = 'N'
													select @estado = 'Pendiente'
												end
											if (@pet_conforme_p = 'S' and @pet_conforme_f = 'N')
												begin
													--select @estado_final = 'S'
													--select @estado_final = 'N'
													select @estado = 'OK / Parcial'
												end

											if (@grupos_tecnicos - @contador = 1)
												select @textosVarios = case
													when @grupos_tecnicos > 1 then 'Solo falta el grupo: ' + rtrim(ltrim(@nom_grupo))
													when @grupos_tecnicos = 1 then 'Solo falta el grupo: ' + rtrim(ltrim(@nom_grupo))
													--when @grupos_tecnicos = 1 then 'Grupo: ' + rtrim(ltrim(@nom_grupo))
												end
											else
												select @textosVarios = 'Faltan algunos grupos t�cnicos (' + LTRIM(RTRIM(convert(char(6), (@grupos_tecnicos - @contador)))) + '/' + LTRIM(RTRIM(convert(char(6), @grupos_tecnicos))) + ')'

											update GesPet..#Control
											set observaciones = LTRIM(RTRIM(@textosVarios)) , estado = LTRIM(RTRIM(@estado)) where codigo = 8		-- upd -007- b. Se cambia "Faltan grupos" por "Faltan algunos grupos t�cnicos"
											--}
										end
							
							-- *******************************************************************************************************************
							-- 9. Documento de Aplicaci�n de los principios de Desarrollo (T710)
							-- *******************************************************************************************************************
							if @pet_clase <> 'OPTI'
								begin
									select @contador = 0
									select @total_grupos_exc = 0

									open CursGrupo 
									fetch CursGrupo into
										@c_pet_nrointerno,
										@c_cod_sector,
										@c_cod_grupo
										while (@@sqlstatus = 0) 
											begin
												if not exists (select codigo from #Excepciones
																where (tipo = 'SEC' and codigo = @c_cod_sector) or 
																	  (tipo = 'GRU' and codigo = @c_cod_grupo))
													begin
														if exists (select pet_nrointerno 
																	from GesPet..PeticionAdjunto 
																	where 
																		pet_nrointerno = @c_pet_nrointerno and 
																		cod_sector = @c_cod_sector and 
																		cod_grupo = @c_cod_grupo and 
																		adj_tipo = 'T710')
															begin
																-- Guarda el total de aquellos que tienen el T710 (si no es una excepci�n)
																select @contador = @contador + 1
															end
														else
															begin
																-- Guarda el nombre del sector y grupo en caso que sea el �nico que le falta T710
																--{ add -005- a.
																select @nom_sector = s.nom_sector from Sector s where s.cod_sector = @c_cod_sector
																select @nom_grupo = g.nom_grupo from Grupo g where g.cod_grupo = @c_cod_grupo
																--}
															end
													end
												else
													begin
														select @total_grupos_exc = @total_grupos_exc + 1
													end
												fetch CursGrupo into
													@c_pet_nrointerno,
													@c_cod_sector,
													@c_cod_grupo
											end
									close CursGrupo

									-- Determino la cantidad real a controlar
									select @grupos_tecnicos = @grupos_tecnicos - @total_grupos_exc

									--{ add -001- a.
									if @grupos_tecnicos = 0
										update GesPet..#Control 
										set observaciones = 'No hay grupos para este control', estado = ' - ' where codigo = 9
									else
									--}
										if @contador = @grupos_tecnicos
											update GesPet..#Control 
											set observaciones = '', estado = 'OK' where codigo = 9
										else
											if @contador = 0 and @grupos_tecnicos > 1
												begin
													update GesPet..#Control 
													set observaciones = 'Todos los grupos t�cnicos', estado = 'Pendiente' where codigo = 9		-- upd -007- b. Se cambia "Todos los grupos" por "Todos los grupos t�cnicos"
													select @estado_final = 'N'
												end
											else
												begin
													--select @estado_final = 'N'
													select @estado = 'Pendiente'

													if (@pet_conforme_p = 'S')
														begin
															--select @estado_final = 'S'
															--select @estado_final = 'N'
															select @estado = 'OK / Parcial'
														end
													if (@pet_conforme_f = 'S')
														begin
															--select @estado_final = 'N'
															select @estado = 'Pendiente'
														end

													if (@grupos_tecnicos - @contador = 1)
														select @textosVarios = case
															when @grupos_tecnicos > 1 then 'Solo falta el grupo: ' + rtrim(ltrim(@nom_grupo))
															when @grupos_tecnicos = 1 then 'Solo falta el grupo: ' + rtrim(ltrim(@nom_grupo))
															--when @grupos_tecnicos = 1 then 'Grupo: ' + rtrim(ltrim(@nom_grupo))
														end
													else
														select @textosVarios = 'Faltan algunos grupos t�cnicos (' + LTRIM(RTRIM(convert(char(6), (@grupos_tecnicos - @contador)))) + '/' + LTRIM(RTRIM(convert(char(6), @grupos_tecnicos))) + ')'

													update GesPet..#Control
													set observaciones = LTRIM(RTRIM(@textosVarios)) , estado = LTRIM(RTRIM(@estado)) where codigo = 9
												end
								end
							else
								update GesPet..#Control 
								set observaciones = 'No requiere por la clase', estado = ' - ' where codigo = 9
							
							-- *******************************************************************************************************************
							-- 10. Conformes de prueba de usuario
							-- *******************************************************************************************************************
							if not exists (select pet_nrointerno 
											from GesPet..PeticionConf 
											where pet_nrointerno = @pet_nrointerno and ok_tipo in ('TEST','TESP'))
								begin
									update GesPet..#Control 
									set observaciones = 'Falta el conforme a las pruebas del usuario', estado = 'Pendiente' where codigo = 10
									select @estado_final = 'N'
								end
							else
								begin
									-- Obtengo el nombre del usuario
									select @textosVarios = 'Otorgado por ' + LTRIM(RTRIM((select x.nom_recurso from Recurso x where x.cod_recurso = pc.audit_user))) + ' (' + ltrim(rtrim(pc.ok_tipo)) + ')'
									from GesPet..PeticionConf pc
									where pc.pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(ok_tipo)), 'TEST|TESP|') > 0  
									and pc.audit_date = (
										select MAX(pc.audit_date)
										from PeticionConf pc
										where pc.pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(ok_tipo)), 'TEST|TESP|') > 0)

									update GesPet..#Control 
									set observaciones = LTRIM(RTRIM(@textosVarios)), estado = 'OK' where codigo = 10
								end

							-- *******************************************************************************************************************
							-- 11. Requiere conforme de homologaci�n para pasajes a Producci�n
							-- *******************************************************************************************************************
							if exists (select ok_tipo from GesPet..PeticionConf where pet_nrointerno = @pet_nrointerno and ok_tipo like 'HOMA%')
								begin
									update GesPet..#Control 
									set observaciones = 'Con observaciones mayores (OMA)', estado = 'Pendiente' where codigo = 11
									select @estado_final = 'N'
								end
							else
								if exists (select ok_tipo from GesPet..PeticionConf where pet_nrointerno = @pet_nrointerno and ok_tipo like 'HOME%')
									update GesPet..#Control 
									set observaciones = 'Con observaciones menores (OME)', estado = 'OK'  where codigo = 11
								else
									if exists (select ok_tipo from GesPet..PeticionConf where pet_nrointerno = @pet_nrointerno and ok_tipo like 'HSOB%')
										begin 
											-- Obtengo el nombre del usuario
											select @textosVarios = ''
											select @textosVarios = 'Otorgado por ' + LTRIM(RTRIM((select x.nom_recurso from Recurso x where x.cod_recurso = pc.audit_user)))
											from GesPet..PeticionConf pc
											where pc.pet_nrointerno = @pet_nrointerno and pc.ok_tipo LIKE 'HSOB._'
											and pc.audit_date = (
												select MAX(pc.audit_date)
												from PeticionConf pc
												where pc.pet_nrointerno = @pet_nrointerno and pc.ok_tipo LIKE 'HSOB._')

											update GesPet..#Control 
											set observaciones = LTRIM(RTRIM(@textosVarios)), estado = 'OK' where codigo = 11
										end
									else
										begin
											update GesPet..#Control 
											set observaciones = 'Falta el conforme de Homologaci�n', estado = 'Pendiente' where codigo = 11
											select @estado_final = 'N'
										end					
						end
					else
						begin
							-- *******************************************************************************************************************
							-- 4. Documentaci�n de alcance (Impacto tec. y/o Proyecto = C100 / Resto: P950)
							-- *******************************************************************************************************************
							if @pet_clase = 'SINC'
								update GesPet..#Control 
								set observaciones = 'N/A' where codigo = 4
							else
								update GesPet..#Control 
								set observaciones = 'Por la clase, no requiere documento de alcance.' where codigo = 4
							-- *******************************************************************************************************************
							-- 5. Conforme a la documentaci�n de alcance
							-- *******************************************************************************************************************
							if @pet_clase = 'SINC'
								update GesPet..#Control 
								set observaciones = 'N/A' where codigo = 5
							else
								update GesPet..#Control 
								set observaciones = 'Por la clase, no requiere documento de alcance.' where codigo = 5
							-- *******************************************************************************************************************
							-- 6. Cambios en la documentaci�n de alcance (si hay CG04, el ALCA/ALCP debe ser posterior al CG04)
							-- *******************************************************************************************************************
							if @pet_clase = 'SINC'
								update GesPet..#Control 
								set observaciones = 'N/A' where codigo = 6
							else
								update GesPet..#Control 
								set observaciones = 'Por la clase, no requiere documento de alcance.' where codigo = 6
							-- *******************************************************************************************************************
							-- 7. Aceptaci�n de cambios en el alcance
							-- *******************************************************************************************************************
							update GesPet..#Control 
							set observaciones = 'N/A' where codigo = 7
							-- *******************************************************************************************************************
							-- 8. Documento de casos de prueba (C204)
							-- *******************************************************************************************************************
							if @pet_clase = 'SINC'
								update GesPet..#Control 
								set observaciones = 'N/A' where codigo = 8
							else
								update GesPet..#Control 
								set observaciones = 'No requiere por la clase' where codigo = 8
							-- *******************************************************************************************************************
							-- 9. Documento de Aplicaci�n de los principios de Desarrollo (T710)
							-- *******************************************************************************************************************
							if @pet_clase = 'SINC'
								update GesPet..#Control 
								set observaciones = 'N/A' where codigo = 9
							else
								update GesPet..#Control 
								set observaciones = 'No requiere por la clase' where codigo = 9
							
							-- *******************************************************************************************************************
							-- 10. Conformes de prueba de usuario
							-- *******************************************************************************************************************
							if (LTRIM(RTRIM(@origen_incidencia)) = '' or @origen_incidencia is null)
								begin
									if not exists (select pet_nrointerno 
													from GesPet..PeticionConf 
													where pet_nrointerno = @pet_nrointerno and ok_tipo in ('TEST','TESP'))
										begin
											update GesPet..#Control 
											set observaciones = 'Falta el conforme final a las pruebas de usuario', estado = 'Pendiente' where codigo = 10
											select @estado_final = 'N'
										end
									else
										begin
											if not exists (select pet_nrointerno 
													from GesPet..PeticionConf 
													where pet_nrointerno = @pet_nrointerno and ok_tipo = 'TEST')
												begin
													update GesPet..#Control 
													set observaciones = 'Requiere un conforme final a las pruebas de usuario', estado = 'Pendiente' where codigo = 10
													select @estado_final = 'N'
												end
											else
												begin
													if exists (select pet_nrointerno from GesPet..PeticionConf where pet_nrointerno = @pet_nrointerno and ok_tipo = 'TEST' and ok_modo = 'ALT') 
														update GesPet..#Control 
														set observaciones = 'Otorgado por responsable de grupo', estado = 'OK' where codigo = 10
														--set observaciones = 'Otorgado por L�der', estado = 'OK' where codigo = 10
													else
														begin
															--{ add -004- a.
															-- Obtengo el nombre del usuario
															select @textosVarios = LTRIM(RTRIM((select x.nom_recurso from Recurso x where x.cod_recurso = pc.audit_user))) + ' (' + ltrim(rtrim(pc.ok_tipo)) + ')'
															from GesPet..PeticionConf pc
															where pc.pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(ok_tipo)), 'TEST|TESP|') > 0  
															and pc.audit_date = (
																select MAX(pc.audit_date)
																from PeticionConf pc
																where pc.pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(ok_tipo)), 'TEST|TESP|') > 0)

															--}
															update GesPet..#Control 
															set observaciones = LTRIM(RTRIM(@textosVarios)), estado = 'OK' where codigo = 10
														end
												end
										end
								end
							else	-- Es un correctivo proveniente de SGI
								begin
									if not exists (select pet_nrointerno 
													from GesPet..PeticionConf 
													where pet_nrointerno = @pet_nrointerno and ok_tipo in ('DATF','DATP'))
										begin
											update GesPet..#Control 
											set 
												control		  = 'Conforme de Datos de usuario',
												observaciones = 'Falta el conforme a los datos de usuario', estado = 'Pendiente' 
											where codigo = 10

											select @estado_final = 'N'
										end
									else
										begin
											if not exists (select pet_nrointerno 
													from GesPet..PeticionConf 
													where pet_nrointerno = @pet_nrointerno and ok_tipo = 'DATF')
												begin
													update GesPet..#Control 
													set 
														control		  = 'Conforme de Datos de usuario',
														observaciones = 'Requiere un conforme final a los datos de usuario', estado = 'Pendiente' 
													where codigo = 10
													select @estado_final = 'N'
												end
											else
												begin
													if exists (select pet_nrointerno from GesPet..PeticionConf where pet_nrointerno = @pet_nrointerno and ok_tipo = 'DATF') 
														update GesPet..#Control 
														set 
															control		  = 'Conforme de Datos de usuario',
															observaciones = 'Otorgado por responsable de grupo', estado = 'OK' 
														where codigo = 10
													else
														begin
															--{ add -004- a.
															-- Obtengo el nombre del usuario
															select @textosVarios = LTRIM(RTRIM((select x.nom_recurso from Recurso x where x.cod_recurso = pc.audit_user))) + ' (' + ltrim(rtrim(pc.ok_tipo)) + ')'
															from GesPet..PeticionConf pc
															where pc.pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(ok_tipo)), 'DATF|DATP|') > 0  
															and pc.audit_date = (
																select MAX(pc.audit_date)
																from PeticionConf pc
																where pc.pet_nrointerno = @pet_nrointerno and charindex(ltrim(rtrim(ok_tipo)), 'DATF|DATP|') > 0)

															--}
															update GesPet..#Control 
															set 
																control		  = 'Conforme de Datos de usuario',
																observaciones = LTRIM(RTRIM(@textosVarios)), estado = 'OK' 
															where codigo = 10
														end
												end
										end
								end


							-- *******************************************************************************************************************
							-- 11. No requere conforme de homologaci�n para pasajes a Producci�n
							-- *******************************************************************************************************************
							begin
								if @control1 = 1
									begin
										if exists (select pet_nrointerno 
													from GesPet..PeticionConf 
													where pet_nrointerno = @pet_nrointerno and ok_tipo like 'TES%' and ok_modo = 'ALT')
											begin
												if exists (select ok_tipo from GesPet..PeticionConf where pet_nrointerno = @pet_nrointerno and ok_tipo like 'HOMA%')
													if (@pet_conforme_s = 'S')
														update GesPet..#Control 
														set observaciones = 'Autorizado por responsable de sector (OMA)', estado = 'OK'  where codigo = 11
													else
														begin
															update GesPet..#Control 
															set observaciones = 'Con observaciones mayores (OMA)', estado = 'Pendiente'  where codigo = 11
															select @estado_final = 'N'
														end
												else
													if exists (select pet_nrointerno 
																from GesPet..PeticionConf 
																where pet_nrointerno = @pet_nrointerno and (ok_tipo like 'HSOB%' or ok_tipo like 'HOME%'))
														update GesPet..#Control 
														set observaciones = 'Requerido por conforme de responsable de grupo', estado = 'OK' where codigo = 11
													else
														begin
															update GesPet..#Control 
															set observaciones = 'Falta el conforme de Homologaci�n', estado = 'Pendiente'  where codigo = 11
															select @estado_final = 'N'
														end
											end
										else
											-- Verifica que no tenga ning�n OMA
											if exists (select pet_nrointerno 
														from GesPet..PeticionConf 
														where pet_nrointerno = @pet_nrointerno and ok_tipo like 'HOMA%')
												begin
													if (@pet_conforme_s = 'S')
														update GesPet..#Control 
														set observaciones = 'Autorizado por responsable de sector (OMA)', estado = 'OK'  where codigo = 11
													else
														begin
															update GesPet..#Control 
															set observaciones = 'Con observaciones mayores (OMA)', estado = 'Pendiente'  where codigo = 11
															select @estado_final = 'N'
														end
												end
											else
												update GesPet..#Control 
												set observaciones = 'N/A' where codigo = 11
									end
								else
									-- Verifica que no tenga ning�n OMA
									if exists (select pet_nrointerno 
												from GesPet..PeticionConf 
												where pet_nrointerno = @pet_nrointerno and ok_tipo like 'HOMA%')
										if (@pet_conforme_s = 'S')
											update GesPet..#Control 
											set observaciones = 'Autorizado por responsable de sector (OMA)', estado = 'OK'  where codigo = 11
										else
											begin
												update GesPet..#Control 
												set observaciones = 'Con observaciones mayores (OMA)', estado = 'Pendiente'  where codigo = 11
												select @estado_final = 'N'
											end
									else
										update GesPet..#Control 
										set observaciones = 'N/A' where codigo = 11
							end						
						end
				end
				-- Elimina la referencia al cursor
				deallocate cursor CursGrupo
			if @estado_final = 'S'
				begin
					update GesPet..#Control 
					set observaciones = '', estado = 'V�lida' where codigo = 99
					-- TODO: es conveniente que desde aqu� sea agregado a PeticionChangeMan el registro para dejar la petici�n v�lida para pasaje a prod???? 24.01.17
					/*
					INSERT INTO PeticionChangeMan 
					VALUES (@pet_nrointerno, )
					*/
				end
			else
				DELETE
				FROM PeticionChangeMan
				WHERE pet_nrointerno = @pet_nrointerno
		end
	
	-- AQUI MUESTRA LOS RESULTADOS
	if @modo = '1'
		select item, nivel, control, observaciones, estado, codigo
		from #Control
		order by item
	else
		select estado_final = @estado_final
	
	drop table #Excepciones
	drop table #Control
	--drop table #GruposSinT710		-- del -005- a.
go

grant execute on dbo.sp_GetControlPeticion to GesPetUsr
go

print 'Actualizaci�n realizada.'
go