use GesPet
go
print 'sp_InsertAvanceCoef'
go
if exists (select * from sysobjects where name = 'sp_InsertAvanceCoef' and sysstat & 7 = 4)
drop procedure dbo.sp_InsertAvanceCoef
go
create procedure dbo.sp_InsertAvanceCoef
	@cod_AvanceCoef	char(8)=null,
	@nom_AvanceCoef	char(30)=null,
	@cod_AvanceGrupo	char(8)=null,
	@flg_habil		char(1)=null,
	@nom_largo		char(100)=null,
	@porcentaje		int = 1


as

if exists (select cod_AvanceCoef from GesPet..AvanceCoef where RTRIM(cod_AvanceCoef) = RTRIM(@cod_AvanceCoef))
	begin 
		raiserror 30011 'Codigo de Avance Coeficiente Existente'   
		select 30011 as ErrCode , 'Codigo de Avance Coeficiente Existente' as ErrDesc   
		return (30011)   
	end 

	insert into GesPet..AvanceCoef
		(cod_AvanceCoef, nom_AvanceCoef, cod_AvanceGrupo, flg_habil,nom_largo, porcentaje)
	values
		(@cod_AvanceCoef, @nom_AvanceCoef,@cod_AvanceGrupo, @flg_habil, @nom_largo, @porcentaje)

return(0)
go


grant execute on dbo.sp_InsertAvanceCoef to GesPetUsr
go
