use GesPet
go
print 'sp_DeleteAccion'
go
if exists (select * from sysobjects where name = 'sp_DeleteAccion' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteAccion
go

create procedure dbo.sp_DeleteAccion 
    @cod_accion char(8)

as
    begin
        delete from GesPet.dbo.Acciones
        where cod_accion = @cod_accion
    end
return(0)
go

grant execute on dbo.sp_DeleteAccion  to GesPetUsr 
go
