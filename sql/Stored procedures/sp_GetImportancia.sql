use GesPet
go
print 'sp_GetImportancia'
go
if exists (select * from sysobjects where name = 'sp_GetImportancia' and sysstat & 7 = 4)
drop procedure dbo.sp_GetImportancia
go
create procedure dbo.sp_GetImportancia 
    @cod_importancia    char(2)=null, 
    @flg_habil      char(1)=null 
as 
begin 
    select  
        cod_importancia, 
        nom_importancia, 
	flg_habil 
    from 
        GesPet..Importancia 
    where   
        (@cod_importancia is null or RTRIM(@cod_importancia) is null or RTRIM(@cod_importancia)='' or RTRIM(cod_importancia) = RTRIM(@cod_importancia)) and  
        (@flg_habil is null or RTRIM(@flg_habil) is null or RTRIM(@flg_habil)='' or RTRIM(flg_habil) = RTRIM(@flg_habil)) 
    order by cod_importancia ASC  
end 
return(0) 
go



grant execute on dbo.sp_GetImportancia to GesPetUsr 
go


