/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 24.11.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertRptdet'
go

if exists (select * from sysobjects where name = 'sp_InsertRptdet' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertRptdet
go

create procedure dbo.sp_InsertRptdet
	@rptid		char(10),
	@rptitem	int,
	@rptnom		char(100),
	@rpthab		char(1)
as
	insert into GesPet.dbo.Rptdet (
		rptid,
		rptitem,
		rptnom,
		rpthab)
	values (
		@rptid,
		@rptitem,
		@rptnom,
		@rpthab)
go

grant execute on dbo.sp_InsertRptdet to GesPetUsr
go

print 'Actualización realizada.'
go
