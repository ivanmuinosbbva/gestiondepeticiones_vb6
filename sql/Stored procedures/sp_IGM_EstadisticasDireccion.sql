/*
-000- a. FJS 02.08.2010 - Nuevo SP para devolver las estad�sticas de horas de una direcci�n abierto por gerencia.
-001- a. FJS 28.10.2010 - Mejora: legibilidad del c�digo.
-002- a. FJS 10.01.2011 - Mejora: se hace el c�lculo de las horas directamente sobre las mismas (performance). Hecho por DTM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_EstadisticasDireccion'
go

if exists (select * from sysobjects where name = 'sp_IGM_EstadisticasDireccion' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_EstadisticasDireccion
go

create procedure dbo.sp_IGM_EstadisticasDireccion
	@cod_direccion	char(8)=null,
	@fe_desde		smalldatetime=null,
	@fe_hasta		smalldatetime=null
as 
	-- Horas TYO a�o en curso
	declare @fecha_actual_desde smalldatetime
	declare @fecha_actual_hasta smalldatetime
	declare @cod_direccion_eje  char(8)

	select @cod_direccion_eje = 'MEDIO'		-- Direcci�n de la cual se tomar�n las horas cargadas (recursos de TYO)
	
	-- Establecer las fechas del per�odo
	if @fe_desde is null
		select @fecha_actual_desde = convert(smalldatetime,convert(char(4),year(getdate())) + '0101 00:00')
	else
		select @fecha_actual_desde = @fe_desde

	if @fe_hasta is null
		select @fecha_actual_hasta = convert(smalldatetime,convert(char(4),year(getdate())) + '1231 23:59')
	else
		select @fecha_actual_hasta = @fe_hasta

	select
		T3.DIRECCION as cod_direccion,
		nom_direccion = (select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = T3.DIRECCION),
		T3.GERENCIA as cod_gerencia,
		nom_gerencia  = (select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = T3.GERENCIA),
		horas_tyo_periodo = convert(numeric(10,4), (SUM(T3.H_ACT)/ convert(numeric(10,4),60))),
		horas_tyo_total	  = convert(numeric(10,4), (SUM(T3.H_TOT)/ convert(numeric(10,4),60)))
	from
		 (select
			 DIRECCION = (select pp.cod_direccion from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
			 GERENCIA = (select pp.cod_gerencia from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
			 H_ACT,
			 H_TOT
		 from
			(select 
				COD_REC,
				N_PET,
				H_ACT = sum(HOR_ACT),
				H_TOT = sum(HOR_TOT)
			   from
				   (select 
						 COD_REC = h.cod_recurso,
						 N_PET = h.pet_nrointerno,
						 HOR_ACT = (case when (h.fe_desde >= @fecha_actual_desde) and 
											  (h.fe_hasta <= @fecha_actual_hasta) then h.horas
									else 0 end),
						 HOR_TOT = (case when h.fe_hasta <= @fecha_actual_hasta then h.horas
									else 0 end)
					from 
						 GesPet..HorasTrabajadas h 
					where
						 (h.pet_nrointerno > 0 and h.pet_nrointerno is not null )) T1
				   group by T1.COD_REC, T1.N_PET) T2
			 where
				  T2.COD_REC in (select r.cod_recurso from GesPet..Recurso r where r.cod_direccion = @cod_direccion_eje) and
				  T2.N_PET   in (select p.pet_nrointerno
							     from GesPet..Peticion p
								 where (@cod_direccion is null or p.cod_direccion = @cod_direccion) and
								       p.cod_gerencia in (select g.cod_gerencia from GesPet..Gerencia g where g.IGM_hab='S') and
									   p.cod_estado in (select e.cod_estado from GesPet..Estados e where e.IGM_hab = 'S'))) T3
	group by T3.DIRECCION, T3.GERENCIA
	return(0)
go

grant execute on dbo.sp_IGM_EstadisticasDireccion to GesPetUsr
go
grant execute on dbo.sp_IGM_EstadisticasDireccion to GrpTrnIGM
go
print 'Actualizaci�n realizada.'
go