/*  C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */
 
/*
-000- a. FJS 02.10.2009 - Agregado de DSN a HEDT001.
-001- a. FJS 18.06.2010 - Actualizaci�n: nuevos campos.
-002- a. FJS 27.12.2010 - Se agrega el campo dsn_nomprod para reflejar la tabla de producci�n de referencia.
-003- a. FJS 30.08.2012 - Pet. n� 51089
-004- a. FJS 12.03.2013 - Pet. N� 51031
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertHEDT001'
go

if exists (select * from sysobjects where name = 'sp_InsertHEDT001' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertHEDT001
go

create procedure dbo.sp_InsertHEDT001
	@dsn_id			char(8)=null,
	@dsn_nom		char(50)=null,
	@dsn_desc		char(255)=null,
	@dsn_enmas		char(1)=null,
	@cod_recurso	char(10)='',
	@dsn_nomrut		char(8)=null,
	@dsn_appid		char(30)=null,
	@dsn_nomprod	char(50)=null,		-- add -002- a.
	@dsn_cpy		char(1),
	@dsn_cpybbl		char(50),
	@dsn_cpynom		char(50)
as
	declare @fe_hoy		smalldatetime
	declare @fe_hoytxt	char(8)
	declare @user_id	char(10)
	declare @numero		int
	declare @num_char	char(8)

	select @fe_hoy		= getdate()
	select @user_id		= @cod_recurso		--SUSER_NAME()
	select @fe_hoytxt	= convert(char(8), @fe_hoy, 112)

	if exists (select x.dsn_id from GesPet..HEDT001 x where RTRIM(x.dsn_id) = RTRIM(@dsn_id))
		begin 
			raiserror 30011 'El c�digo del archivo ya existe. Revise.'
			select 30011 as ErrCode , 'El c�digo del archivo ya existe. Revise.' as ErrDesc
			return (30011)
		end
	else
		if exists (select x.dsn_nom from GesPet..HEDT001 x where LTRIM(RTRIM(x.dsn_nom)) = LTRIM(RTRIM(@dsn_nom)))
			begin 
				raiserror 30011 'El nombre del archivo ya existe. Revise.'
				select 30011 as ErrCode , 'El nombre del archivo ya existe. Revise.' as ErrDesc
				return (30011)
			end
		else
			begin
				-- Armo el c�digo del archivo
				select @numero = (select max(convert(int,substring(dsn_id, 2, len(dsn_id)))) + 1 from GesPet..HEDT001 where dsn_id like 'A%')
				select @num_char = convert(char(8), @numero)

				select @dsn_id = 'A' + 
					case 
						when datalength(rtrim(@num_char)) = 1 then '000000' + rtrim(@num_char)
						when datalength(rtrim(@num_char)) = 2 then '00000' + rtrim(@num_char)
						when datalength(rtrim(@num_char)) = 3 then '0000' + rtrim(@num_char)
						when datalength(rtrim(@num_char)) = 4 then '000' + rtrim(@num_char)
						when datalength(rtrim(@num_char)) = 5 then '00' + rtrim(@num_char)
						when datalength(rtrim(@num_char)) = 6 then '0' + rtrim(@num_char)
						when datalength(rtrim(@num_char)) = 7 then rtrim(@num_char)
					else
						'0000001'
					end

				insert into GesPet.dbo.HEDT001 (
					dsn_id,
					dsn_nom,
					dsn_desc,
					dsn_enmas,
					dsn_nomrut,
					dsn_feult,
					dsn_userid,
					dsn_vb,
					dsn_vb_fe,
					dsn_vb_userid,
					app_id,
					estado,
					fe_hst_estado
					--{ add -001- a.
					,dsn_txt
					,dsn_vb_ultfch
					--}
					,dsn_nomprod		-- add -002- a.
					,dsn_cpy
					,dsn_cpybbl
					,dsn_cpynom
					,dsn_cantuso)
				values (
					@dsn_id,
					@dsn_nom,
					@dsn_desc,
					@dsn_enmas,
					@dsn_nomrut,
					@fe_hoy,
					@user_id,
					--'N',				-- del -004- a.
					'B',				-- add -004- a. Nace como "Borrador"
					null,
					null,
					@dsn_appid,
					'A',
					null
					--{ add -001- a.
					,null
					,null
					--}
					,@dsn_nomprod		-- add -002- a.
					,@dsn_cpy
					,@dsn_cpybbl
					,@dsn_cpynom
					,0)
			end
go

grant execute on dbo.sp_InsertHEDT001 to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
