/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
print 'sp_InsAgrup'
go
if exists (select * from sysobjects where name = 'sp_InsAgrup' and sysstat & 7 = 4)
drop procedure dbo.sp_InsAgrup
go
create procedure dbo.sp_InsAgrup 
	@agr_nropadre		int=0,
	@agr_titulo  		char(50)='',
	@agr_vigente  		char(1)='',
	@cod_direccion 		char(8)='',
	@cod_gerencia  		char(8)='',
	@cod_sector   		char(8)='',
	@cod_grupo   		char(8)='',
	@cod_usualta		char(10)='',
	@agr_visibilidad	char(3)='',
	@agr_actualiza		char(3)='',
	@agr_admpet  		char(1)=''
as 
/*BEGIN TRAN*/ 
declare @proxnumero      int   
select @proxnumero = 0   
 
	execute sp_ProximoNumero 'ULAGRINT', @proxnumero OUTPUT   
	if exists (select agr_nrointerno from Agrup where agr_nrointerno = @proxnumero)   
	begin 
		select 30010 as ErrCode , 'Error al generar numero agrupamiento' as ErrDesc 
		raiserror  30010 'Error al generar numero agrupamiento'  
		return (30010) 
	end 
 
	insert into Agrup 
		(agr_nrointerno,
		agr_nropadre,
		agr_titulo,
		agr_vigente,
		cod_direccion,
		cod_gerencia,
		cod_sector,
		cod_grupo,
		cod_usualta,
		agr_visibilidad,
		agr_actualiza,
		agr_admpet,
		audit_user,
		audit_date)
	values   
		(@proxnumero,
		@agr_nropadre,  
		@agr_titulo, 
		@agr_vigente,   
		@cod_direccion, 
		@cod_gerencia, 
		@cod_sector, 
		@cod_grupo, 
		@cod_usualta, 
		@agr_visibilidad,         
		@agr_actualiza,         
		@agr_admpet,
		SuseR_NAME(),   
		getdate()   )
 
select @proxnumero  as valor_clave 
 
/*COMMIT TRAN*/ 
return(0)   
go



grant execute on dbo.sp_InsAgrup to GesPetUsr 
go


