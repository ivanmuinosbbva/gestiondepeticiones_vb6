/*
-001- a. FJS 13.01.2009 - Optimización del SP para mejorar la performance del aplicativo.
-002- a. FJS 12.01.2015 - Nuevo: se agrega el join con Grupo para saber si son grupos técnicos o funcionales.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionRecurso'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionRecurso' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionRecurso
go

create procedure dbo.sp_GetPeticionRecurso 
    @pet_nrointerno     int = 0, 
    @cod_grupo          char(8) = null
as 
	begin 
		select  
			Ps.pet_nrointerno, 
			Ps.cod_recurso, 
			nom_recurso = (select R.nom_recurso from Recurso R where R.cod_recurso=Ps.cod_recurso),
			Ps.cod_grupo, 
			Ps.cod_sector,           
			Ps.cod_gerencia,         
			Ps.cod_direccion,
			g.grupo_homologacion			-- add -002- a.
		from 
			--{ add -002- a.
			GesPet..PeticionRecurso Ps inner join
			GesPet..Grupo g on (g.cod_grupo = Ps.cod_grupo)
			--}
		where   
			(@pet_nrointerno = 0 or @pet_nrointerno = Ps.pet_nrointerno) and 
			(@cod_grupo is null or RTRIM(@cod_grupo) is null or RTRIM(@cod_grupo)='' or RTRIM(Ps.cod_grupo) = RTRIM(@cod_grupo)) 
		order by
			--{ add -001- a.
			Ps.pet_nrointerno,
			Ps.cod_grupo,
			Ps.cod_recurso
			--}
			--nom_recurso ASC				-- del -001- a.
	end 
	return(0) 
go

grant execute on dbo.sp_GetPeticionRecurso to GesPetUsr 
go

print 'Actualización realizada.'
go
