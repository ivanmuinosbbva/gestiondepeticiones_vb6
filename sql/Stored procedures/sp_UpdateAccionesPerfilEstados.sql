/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 31.05.2017 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateAccionesPerfilEstados'
go

if exists (select * from sysobjects where name = 'sp_UpdateAccionesPerfilEstados' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateAccionesPerfilEstados
go

create procedure dbo.sp_UpdateAccionesPerfilEstados
	@cod_version	int,
	@cod_accion		char(8),
	@cod_perfil		char(4),
	@cod_estado		char(6),
	@cod_estnew		char(6),
	@flg_hereda		char(1)
as
	update 
		GesPet.dbo.AccionesPerfilEstados
	set
		flg_hereda = @flg_hereda
	where 
		cod_version = @cod_version and
		cod_accion = @cod_accion and 
		cod_perfil = @cod_perfil and
		cod_estado = @cod_estado and 
		cod_estnew = @cod_estnew
go

grant execute on dbo.sp_UpdateAccionesPerfilEstados to GesPetUsr
go

print 'Actualización realizada.'
go
