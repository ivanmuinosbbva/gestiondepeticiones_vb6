/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
print 'sp_ChangeSectorGerencia'
go
if exists (select * from sysobjects where name = 'sp_ChangeSectorGerencia' and sysstat & 7 = 4)
drop procedure dbo.sp_ChangeSectorGerencia
go
create procedure dbo.sp_ChangeSectorGerencia
	@cod_sector	char(8),
	@new_gerencia	char(8)
as
declare @new_direccion char(8)
declare @cod_recurso char(10)
declare @old_gerencia char(8)
declare @old_direccion char(8)

if not exists (select Ge.cod_gerencia
	from GesPet..Gerencia Ge
	where RTRIM(Ge.cod_gerencia) = RTRIM(@new_gerencia))
begin
	raiserror  30010 'Sector Inexistente'
	select 30010 as ErrCode , 'Sector Inexistente'  as ErrDesc
	return (30010)
end

select  @new_direccion=Ge.cod_direccion
	from GesPet..Gerencia Ge
	where RTRIM(@new_gerencia) = RTRIM(Ge.cod_gerencia)

select  @old_direccion=Ge.cod_direccion,
	@old_gerencia=Se.cod_gerencia
from Sector Se, Gerencia Ge
where 	RTRIM(Se.cod_sector) = RTRIM(@cod_sector) and
	RTRIM(Se.cod_gerencia) = RTRIM(Ge.cod_gerencia)

declare CursRecursos Cursor
	for select cod_recurso
	from Recurso
	where RTRIM(cod_sector) = RTRIM(@cod_sector)
	for Read Only


BEGIN TRANSACTION
	/* perfiles */
	open CursRecursos
	fetch CursRecursos into @cod_recurso
	if @@sqlstatus = 0
	begin
		While @@sqlstatus = 0
		begin
			update RecursoPerfil
			set cod_area = @new_gerencia
			where RTRIM(cod_recurso) = @cod_recurso and
				RTRIM(cod_nivel) = 'GERE' and
				 RTRIM(cod_area) = @old_gerencia

			update RecursoPerfil
			set cod_area = @new_direccion
			where RTRIM(cod_recurso) = @cod_recurso and
				RTRIM(cod_nivel) = 'DIRE' and
				RTRIM(cod_area) = @old_direccion

			fetch CursRecursos into @cod_recurso
		end
	end
	close CursRecursos

	/* recursos */
	update GesPet..Recurso
	set	cod_direccion=@new_direccion,
		cod_gerencia = @new_gerencia
	where RTRIM(cod_sector) = RTRIM(@cod_sector)

	/* peticion-solicitante */
	update GesPet..Peticion
	set	cod_direccion=@new_direccion,
		cod_gerencia = @new_gerencia
	where RTRIM(cod_sector) = RTRIM(@cod_sector)
	
	/* PeticionSector */
	update GesPet..PeticionSector
	set	cod_direccion=@new_direccion,
		cod_gerencia = @new_gerencia
	where RTRIM(cod_sector) = RTRIM(@cod_sector)

	/* PeticionGrupo */
	update GesPet..PeticionGrupo
	set	cod_direccion=@new_direccion,
		cod_gerencia = @new_gerencia
	where RTRIM(cod_sector) = RTRIM(@cod_sector)

	/* los agrupamientos */
	update GesPet..Agrup
	set	cod_direccion=@new_direccion,
		cod_gerencia=@new_gerencia
	where RTRIM(cod_sector) = RTRIM(@cod_sector)

	/* los PeticionRecurso */
	update GesPet..PeticionRecurso
	set	cod_direccion=@new_direccion,
		cod_gerencia=@new_gerencia
	where RTRIM(cod_sector) = RTRIM(@cod_sector)

COMMIT TRANSACTION
DEALLOCATE CURSOR CursRecursos
return(0)
go



grant execute on dbo.sp_ChangeSectorGerencia to GesPetUsr
go


