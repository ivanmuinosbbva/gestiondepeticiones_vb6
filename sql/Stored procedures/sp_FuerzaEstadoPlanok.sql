/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
print 'sp_FuerzaEstadoPlanok'
go
if exists (select * from sysobjects where name = 'sp_FuerzaEstadoPlanok' and sysstat & 7 = 4)
drop procedure dbo.sp_FuerzaEstadoPlanok
go
create procedure dbo.sp_FuerzaEstadoPlanok 
    @pet_nrointerno     int, 
    @hst_nrointerno     int 
as 
declare @estadosopinion varchar(255) 
select @estadosopinion = 'OPINIO|OPINOK|EVALUA|EVALOK|ESTIMA|ESTIOK' 
BEGIN TRAN 
    update GesPet..PeticionGrupo 
    set cod_estado  =   'PLANIF', 
        fe_estado   =   getdate(), 
        cod_situacion =     '', 
        ult_accion  = '', 
        hst_nrointerno_sol= @hst_nrointerno, 
        hst_nrointerno_rsp= @hst_nrointerno, 
        audit_user = SUSER_NAME(),   
        audit_date = getdate()   
    where @pet_nrointerno=pet_nrointerno and 
        charindex(cod_estado,@estadosopinion)>0 
 
    update GesPet..PeticionSector 
    set cod_estado  =   'PLANIF', 
        fe_estado   =   getdate(), 
        cod_situacion =     '', 
        ult_accion  = '', 
        hst_nrointerno_sol= @hst_nrointerno, 
        hst_nrointerno_rsp= @hst_nrointerno, 
        audit_user = SUSER_NAME(),   
        audit_date = getdate()   
    where @pet_nrointerno=pet_nrointerno and 
        charindex(cod_estado,@estadosopinion)>0 
 
    update GesPet..Peticion 
    set cod_estado  =   'PLANIF', 
        fe_estado   =   getdate(), 
        cod_situacion = '', 
        ult_accion  = '', 
        audit_user = SUSER_NAME(),   
        audit_date = getdate()   
    where @pet_nrointerno=pet_nrointerno and 
        charindex(cod_estado,@estadosopinion)>0 
 
COMMIT TRAN 
return(0) 
go



grant execute on dbo.sp_FuerzaEstadoPlanok to GesPetUsr 
go


