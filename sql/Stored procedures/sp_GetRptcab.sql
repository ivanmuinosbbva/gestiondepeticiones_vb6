/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 24.11.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetRptcab'
go

if exists (select * from sysobjects where name = 'sp_GetRptcab' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRptcab
go

create procedure dbo.sp_GetRptcab
	@rptid	char(10)=null
as
	select 
		rpt.rptid,
		rpt.rptnom,
		rpt.rpthab,
		rpt.rptfch
	from 
		GesPet.dbo.Rptcab rpt
	where
		(rpt.rptid = @rptid or @rptid is null)
go

grant execute on dbo.sp_GetRptcab to GesPetUsr
go

print 'Actualización realizada.'
go
