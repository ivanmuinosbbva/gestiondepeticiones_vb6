/*
-000- a. FJS 31.10.2008 - Nuevo SP para devolver el nro. asignado de una petición.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPetNroAsignado'
go

if exists (select * from sysobjects where name = 'sp_GetPetNroAsignado' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPetNroAsignado
go

create procedure dbo.sp_GetPetNroAsignado
	@pet_nrointerno		int=null
as 
	select
		PET.pet_nrointerno,
		PET.pet_nroasignado
	from
		GesPet..Peticion PET
	where
		(PET.pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null)

go

grant execute on dbo.sp_GetPetNroAsignado to GesPetUsr
go

print 'Actualización realizada.'
