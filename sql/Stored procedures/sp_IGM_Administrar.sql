/*
-000- a. FJS 20.08.2010 - Nuevo SP para determinar al administrador de IGM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_Administrar'
go

if exists (select * from sysobjects where name = 'sp_IGM_Administrar' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_Administrar
go

create procedure dbo.sp_IGM_Administrar
	@cod_recurso		char(10)
as 
	select rp.cod_recurso
	from GesPet..RecursoPerfil rp
	where rp.cod_recurso = @cod_recurso and rp.cod_perfil = 'ADM1'
	return(0)
go

grant execute on dbo.sp_IGM_Administrar to GesPetUsr
go

grant execute on dbo.sp_IGM_Administrar to GrpTrnIGM
go

print 'Actualización realizada.'
go