use GesPet
go


print 'Actualizando procedimiento almacenado: sp_GetPeticionGrupoXt'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionGrupoXt' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionGrupoXt
go

create procedure dbo.sp_GetPeticionGrupoXt
	@pet_nrointerno     int,
	@cod_sector	 char(8)=null,
	@cod_grupo	  char(8)=null,
	@cod_estado varchar(250)=null
as
	begin
	select
		Ps.pet_nrointerno,
		Pt.pet_nroasignado,
		Pt.titulo,
		Ps.cod_grupo,
		nom_grupo = (select Gr.nom_grupo from Grupo Gr where RTRIM(Gr.cod_grupo)=RTRIM(Ps.cod_grupo)),
		Ps.cod_sector,
		nom_sector = (select Se.nom_sector from Sector Se where RTRIM(Se.cod_sector) = RTRIM(Ps.cod_sector)),
		Ps.cod_gerencia,
		nom_gerencia = (select Ge.nom_gerencia from Gerencia Ge where RTRIM(Ge.cod_gerencia) = RTRIM(Ps.cod_gerencia)),
		Ps.cod_direccion,
		nom_direccion = (select Di.nom_direccion from Direccion Di where RTRIM(Di.cod_direccion) = RTRIM(Ps.cod_direccion)),
		Ps.fe_ini_plan,
		Ps.fe_fin_plan,
		Ps.fe_ini_real,
		Ps.fe_fin_real,
		Ps.horaspresup,
		Ps.cod_estado,
		Ps.fe_estado,
		Ps.cod_situacion,
		Ps.hst_nrointerno_sol,
		Ps.hst_nrointerno_rsp,
		EST.nom_estado,
		EST.flg_rnkup,
		Ps.cant_planif,
		Ps.prio_ejecuc,
		Ps.info_adicio,
		grupo_homologacion = (select g.grupo_homologacion from Grupo g where g.cod_grupo = Ps.cod_grupo)
	from
		GesPet..PeticionGrupo Ps,
		GesPet..Peticion Pt,
		GesPet..Estados EST
	where
		(@pet_nrointerno =0 or @pet_nrointerno = Ps.pet_nrointerno) and
		(@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(Ps.cod_sector) = RTRIM(@cod_sector)) and
		(@cod_grupo is null or RTRIM(@cod_grupo) is null or RTRIM(@cod_grupo)='' or RTRIM(Ps.cod_grupo) = RTRIM(@cod_grupo)) and
		(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(Ps.cod_estado),RTRIM(@cod_estado))>0) and
		--(Ps.pet_nrointerno *= Pt.pet_nrointerno) and
		(Ps.pet_nrointerno = Pt.pet_nrointerno) and
		RTRIM(Ps.cod_estado) *= RTRIM(EST.cod_estado)
	order by Ps.cod_sector,Ps.cod_grupo
	end
return(0)
go

grant execute on dbo.sp_GetPeticionGrupoXt to GesPetUsr
go

print 'Actualización finalizada.'
go