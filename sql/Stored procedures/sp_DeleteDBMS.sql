/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.07.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteDBMS'
go

if exists (select * from sysobjects where name = 'sp_DeleteDBMS' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteDBMS
go

create procedure dbo.sp_DeleteDBMS
	@dbmsId	int
as
	delete from GesPet.dbo.DBMS
	where dbmsId = @dbmsId
go

grant execute on dbo.sp_DeleteDBMS to GesPetUsr
go

print 'Actualización realizada.'
go
