/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 24.11.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateRptdet'
go

if exists (select * from sysobjects where name = 'sp_UpdateRptdet' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateRptdet
go

create procedure dbo.sp_UpdateRptdet
	@rptid		char(10),
	@rptitem	int,
	@rptnom		char(100),
	@rpthab		char(1)
as
	update GesPet.dbo.Rptdet
	set
		rptnom = @rptnom,
		rpthab = @rpthab
	where 
		rptid = @rptid and
		rptitem = @rptitem
go

grant execute on dbo.sp_UpdateRptdet to GesPetUsr
go

print 'Actualización realizada.'
go
