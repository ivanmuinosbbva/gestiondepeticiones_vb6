/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 11.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteProyectoIDMMemo'
go

if exists (select * from sysobjects where name = 'sp_DeleteProyectoIDMMemo' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteProyectoIDMMemo
go

create procedure dbo.sp_DeleteProyectoIDMMemo
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int,
	@mem_campo		char(10),
	@mem_secuencia	smallint
as
	delete from
		GesPet.dbo.ProyectoIDMMemo
	where
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		(@mem_campo is null or mem_campo = @mem_campo) and
		(@mem_secuencia is null or mem_secuencia = @mem_secuencia)
	return(0)
go

grant execute on dbo.sp_DeleteProyectoIDMMemo to GesPetUsr
go

print 'Actualización realizada.'
go