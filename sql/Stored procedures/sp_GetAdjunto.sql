/*
-001- a. FJS 14.04.2010 - Se optimiza la consulta.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetAdjunto'
go

if exists (select * from sysobjects where name = 'sp_GetAdjunto' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetAdjunto
go

create procedure dbo.sp_GetAdjunto
	@pet_nrointerno	int=null,
	@adj_file	char(50)=null
as
	select
		Ad.pet_nrointerno,
		Ad.adj_file,
		Ad.adj_path,
		Ad.adj_texto,
		Ad.audit_user,
		nom_audit = isnull((select Ra.nom_recurso from Recurso Ra where RTRIM(Ra.cod_recurso)=RTRIM(Ad.audit_user)),''),
		Ad.audit_date
	from 
		Adjunto Ad
	where	
		--(@pet_nrointerno is null or pet_nrointerno=@pet_nrointerno) and	-- del -001- a.
		Ad.pet_nrointerno = @pet_nrointerno and
		(@adj_file is null or RTRIM(@adj_file) is null or RTRIM(@adj_file)='' or RTRIM(adj_file) = RTRIM(@adj_file))
	order by 
		pet_nrointerno,
		adj_file
	
	return(0)
go

grant execute on dbo.sp_GetAdjunto to GesPetUsr
go

print 'Actualización realizada.'
go