use GesPet
go
print 'sp_GetEstado'
go
if exists (select * from sysobjects where name = 'sp_GetEstado' and sysstat & 7 = 4)
drop procedure dbo.sp_GetEstado
go
create procedure dbo.sp_GetEstado 
             @cod_estado        char(6)=null 
as 
begin 
    select  
        cod_estado, 
        nom_estado, 
        flg_rnkup, 
        flg_herdlt1, 
        flg_petici, 
        flg_secgru 
    from 
        GesPet..Estados 
    where   
        (@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or RTRIM(cod_estado) = RTRIM(@cod_estado)) 
    order by nom_estado ASC  
end 
return(0) 
go



grant execute on dbo.sp_GetEstado to GesPetUsr 
go


