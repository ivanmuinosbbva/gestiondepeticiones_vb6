use GesPet
go
print 'sp_GetProyectoBalanceResumen'
go
if exists (select * from sysobjects where name = 'sp_GetProyectoBalanceResumen' and sysstat & 7 = 4)
drop procedure dbo.sp_GetProyectoBalanceResumen
go
create procedure dbo.sp_GetProyectoBalanceResumen 
	@prj_nrointerno int
as 

select 
	BDE.prj_nrointerno,
	BDE.cod_BalanceRubro,
	nom_BalanceRubro = isnull((select BRU.nom_BalanceRubro from BalanceRubro BRU where RTRIM(BRU.cod_BalanceRubro)=RTRIM(BDE.cod_BalanceRubro)),''),
	BDE.bde_moneda,
	bde_estimado = sum(BDE.bde_estimado),
	bde_real=sum(BDE.bde_real)
from  ProyectoBalanceDet BDE
where	BDE.prj_nrointerno=@prj_nrointerno
group by BDE.prj_nrointerno,BDE.cod_BalanceRubro,BDE.bde_moneda
order by BDE.prj_nrointerno,BDE.cod_BalanceRubro,BDE.bde_moneda
return(0) 
go

grant execute on dbo.sp_GetProyectoBalanceResumen to GesPetUsr 
go
