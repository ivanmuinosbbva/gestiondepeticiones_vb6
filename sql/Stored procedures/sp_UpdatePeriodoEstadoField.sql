/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeriodoEstadoField'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeriodoEstadoField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeriodoEstadoField
go

create procedure dbo.sp_UpdatePeriodoEstadoField
	@cod_estado_per	char(8),
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as
	if rtrim(@campo)='COD_ESTADO_PER'
		update GesPet.dbo.PeriodoEstado set cod_estado_per = @valortxt where 
			(cod_estado_per = @cod_estado_per or @cod_estado_per is null)

	if rtrim(@campo)='NOM_ESTADO_PER'
		update GesPet.dbo.PeriodoEstado set nom_estado_per = @valortxt where 
			(cod_estado_per = @cod_estado_per or @cod_estado_per is null)
	return(0)
go

grant execute on dbo.sp_UpdatePeriodoEstadoField to GesPetUsr
go

print 'Actualización realizada.'
