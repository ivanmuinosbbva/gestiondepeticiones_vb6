/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 12.02.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertAccionesGrupo'
go

if exists (select * from sysobjects where name = 'sp_InsertAccionesGrupo' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertAccionesGrupo
go

create procedure dbo.sp_InsertAccionesGrupo
	@accionGrupoId	int,
	@accionGrupoNom	varchar(50),
	@accionGrupoHab	char(1)
as
	insert into GesPet.dbo.AccionesGrupo (
		accionGrupoId,
		accionGrupoNom,
		accionGrupoHab)
	values (
		@accionGrupoId,
		@accionGrupoNom,
		@accionGrupoHab)
go

grant execute on dbo.sp_InsertAccionesGrupo to GesPetUsr
go

print 'Actualización realizada.'
go
