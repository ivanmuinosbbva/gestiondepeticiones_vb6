/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 27.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePerfil'
go

if exists (select * from sysobjects where name = 'sp_DeletePerfil' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePerfil
go

create procedure dbo.sp_DeletePerfil
	@cod_perfil	char(4)
as
	delete from
		GesPet.dbo.Perfil
	where
		(cod_perfil = @cod_perfil or @cod_perfil is null)
	return(0)
go

grant execute on dbo.sp_DeletePerfil to GesPetUsr
go

print 'Actualización realizada.'
