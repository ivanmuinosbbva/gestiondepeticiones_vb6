use GesPet
go
print 'sp_DelMensajesEvento'
go
if exists (select * from sysobjects where name = 'sp_DelMensajesEvento' and sysstat & 7 = 4)
drop procedure dbo.sp_DelMensajesEvento
go
create procedure dbo.sp_DelMensajesEvento 
    @evt_alcance char(3), 
    @cod_accion char(8), 
    @cod_estado char(6), 
    @cod_perfil char(4) 
as 
    delete  GesPet..MensajesEvento where 
        RTRIM(evt_alcance)=RTRIM(@evt_alcance) and 
        RTRIM(cod_accion)=RTRIM(@cod_accion) and 
        RTRIM(cod_estado)=RTRIM(@cod_estado) and 
        RTRIM(cod_perfil)=RTRIM(@cod_perfil) 
return(0) 
go



grant execute on dbo.sp_DelMensajesEvento to GesPetUsr 
go


