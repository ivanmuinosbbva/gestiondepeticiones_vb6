/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePrioridad'
go

if exists (select * from sysobjects where name = 'sp_DeletePrioridad' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePrioridad
go

create procedure dbo.sp_DeletePrioridad
	@prior_id	char(1)
as
	delete from
		GesPet.dbo.Prioridad
	where
		(prior_id = @prior_id or @prior_id is null)
	return(0)
go

grant execute on dbo.sp_DeletePrioridad to GesPetUsr
go

print 'Actualización realizada.'
