/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 06.06.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateInterfazTera'
go

if exists (select * from sysobjects where name = 'sp_UpdateInterfazTera' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateInterfazTera
go

create procedure dbo.sp_UpdateInterfazTera
	@dbName		char(30),
	@objname	char(50),
	@objlabel	char(50),
	@objhab		char(1)
as
	update 
		GesPet.dbo.InterfazTera
	set
		objlabel	= @objlabel,
		objhab		= @objhab
	where 
		dbName = @dbName and
		objname = @objname
go

grant execute on dbo.sp_UpdateInterfazTera to GesPetUsr
go

print 'Actualización realizada.'
go
