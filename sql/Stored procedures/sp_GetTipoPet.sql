/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.10.2014 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetTipoPet'
go

if exists (select * from sysobjects where name = 'sp_GetTipoPet' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetTipoPet
go

create procedure dbo.sp_GetTipoPet
	@cod_tipopet	char(3)=NULL
as
	select 
		p.cod_tipopet,
		p.nom_tipopet
	from 
		GesPet.dbo.TipoPet p
	where
		(@cod_tipopet is null or p.cod_tipopet = @cod_tipopet)
go

grant execute on dbo.sp_GetTipoPet to GesPetUsr
go

print 'Actualización realizada.'
go
