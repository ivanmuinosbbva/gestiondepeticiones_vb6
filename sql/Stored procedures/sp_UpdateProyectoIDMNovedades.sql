/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoIDMNovedades'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoIDMNovedades' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoIDMNovedades
go

create procedure dbo.sp_UpdateProyectoIDMNovedades
	@borra_last		char(1)='N',
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int,
	--@mem_fecha		datetime,
	@mem_fecha		char(20),
	@mem_campo		char(10),
	@mem_secuencia	smallint,
	@mem_texto		varchar(255),
	@cod_usuario	char(10)
as
	declare @textoaux	varchar(255)   
	declare @textomemo	varchar(255)

	select @textomemo = ''

	if exists (select ProjId from GesPet.dbo.ProyectoIDMNovedades   
			where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and 
				mem_fecha = convert(datetime, @mem_fecha) and 
				mem_campo = @mem_campo and
				mem_secuencia = @mem_secuencia)
		begin
			update 
				GesPet.dbo.ProyectoIDMNovedades
			set
				mem_texto	 = @mem_texto,
				--cod_usuario	= @cod_usuario,
				mem_accion	 = 'UPD',
				mem_fecha2	 = getdate(),
				cod_usuario2 = @cod_usuario
			where 
				ProjId		= @ProjId and
				ProjSubId	= @ProjSubId and
				ProjSubSId	= @ProjSubSId and
				mem_fecha	= convert(datetime, @mem_fecha) and
				mem_campo	= @mem_campo and
				mem_secuencia = @mem_secuencia
		end
	else
		begin
			insert into GesPet.dbo.ProyectoIDMNovedades (
				ProjId,
				ProjSubId,
				ProjSubSId,
				mem_fecha,
				mem_campo,
				mem_secuencia,
				mem_texto,
				cod_usuario,
				mem_accion,
				mem_fecha2,
				cod_usuario2) 
			values (
				@ProjId,
				@ProjSubId,
				@ProjSubSId,
				convert(datetime, @mem_fecha),
				@mem_campo,
				@mem_secuencia,
				@mem_texto,
				@cod_usuario,
				'ADD',
				null,
				null)
		end
	if @borra_last = 'S'   
		begin 
			delete from GesPet.dbo.ProyectoIDMNovedades  
			where 
				ProjId = @ProjId and 
				ProjSubId = @ProjSubId and 
				ProjSubSId = @ProjSubSId and 
				mem_fecha = convert(datetime, @mem_fecha) and 
				mem_campo = @mem_campo and
				mem_secuencia > @mem_secuencia
		end
	return(0)
go

grant execute on dbo.sp_UpdateProyectoIDMNovedades to GesPetUsr
go

print 'Actualización realizada.'
go