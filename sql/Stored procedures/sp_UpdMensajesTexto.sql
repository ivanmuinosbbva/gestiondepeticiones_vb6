use GesPet
go
print 'sp_UpdMensajesTexto'
go
if exists (select * from sysobjects where name = 'sp_UpdMensajesTexto' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdMensajesTexto
go
create procedure dbo.sp_UpdMensajesTexto 
    @cod_txtmsg int=0, 
    @msg_texto      varchar(255) 
as 
 
declare @proxnumero      int   
select @proxnumero = 0   
 
if @cod_txtmsg = 0 
begin    
    execute sp_ProximoNumero 'ULMSGTXT', @proxnumero OUTPUT   
    if exists (select cod_txtmsg from GesPet..MensajesTexto where cod_txtmsg = @cod_txtmsg)   
    begin 
        select 30010 as ErrCode , 'Error al generar numero interno' as ErrDesc 
        raiserror  30010 'Error al generar numero interno'  
        return (30010) 
    end 
end 
else 
begin 
    select @proxnumero = @cod_txtmsg 
end 
 
if exists (select cod_txtmsg from GesPet..MensajesTexto where cod_txtmsg = @cod_txtmsg)   
begin   
    update GesPet..MensajesTexto set 
        msg_texto=   @msg_texto 
    where cod_txtmsg = @cod_txtmsg 
end 
else 
begin 
    insert into GesPet..MensajesTexto  
        (cod_txtmsg,msg_texto) 
    values 
        (@proxnumero,@msg_texto) 
end 
return(0) 
go



grant execute on dbo.sp_UpdMensajesTexto to GesPetUsr 
go


