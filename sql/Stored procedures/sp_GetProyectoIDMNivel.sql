/*
-000- a. FJS 02.07.2009 - Nuevo SP para devolver todos los proyectos de IDM de un cierto nivel.
-001- a. FJS 04.12.2012 - Modificación: se modifica el join para traer todo, independientemente de su clasificación y categorización.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMNivel'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMNivel' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMNivel
go

create procedure dbo.sp_GetProyectoIDMNivel
	@nivel			char(1)=null,
	@ProjId			int=null, 
	@ProjSubId		int=null, 
	@ProjSubsId		int=null
as 
	-- Inicializo los códigos
	if @ProjId is null
		select @ProjId = 0
	if @ProjSubId is null
		select @ProjSubId = 0
	if @ProjSubsId is null
		select @ProjSubsId = 0
	
	-- Primer nivel: Macro proyectos
	if @nivel = '1'
		begin
			select
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId,
				PRJ.ProjNom,
				PRJ.ProjFchAlta,
				PRJ.ProjCatId,
				PRJ.ProjClaseId,
				CAT.ProjCatNom,
				CLS.ProjClaseNom
			from
				GesPet..ProyectoIDM PRJ left join 
				GesPet..ProyectoCategoria CAT on (PRJ.ProjCatId = CAT.ProjCatId) left join
				GesPet..ProyectoClase CLS on (PRJ.ProjCatId = CLS.ProjCatId and PRJ.ProjClaseId = CLS.ProjClaseId)
			where
				(PRJ.ProjId <> 0 and PRJ.ProjId is not null) and
				(PRJ.ProjSubId = 0) and
				(PRJ.ProjSubSId = 0)
			order by
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId
			return(0)
		end

	-- Segundo nivel: Sub proyectos
	if @nivel = '2'
		begin
			select
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId,
				PRJ.ProjNom,
				PRJ.ProjFchAlta,
				PRJ.ProjCatId,
				PRJ.ProjClaseId,
				CAT.ProjCatNom,
				CLS.ProjClaseNom
			from
				GesPet..ProyectoIDM PRJ left join 
				GesPet..ProyectoCategoria CAT on (PRJ.ProjCatId = CAT.ProjCatId) left join
				GesPet..ProyectoClase CLS on (PRJ.ProjCatId = CLS.ProjCatId and PRJ.ProjClaseId = CLS.ProjClaseId)
			where
				(PRJ.ProjId = @ProjId ) and
				(PRJ.ProjSubId <> 0 and PRJ.ProjSubId is not null) and
				(PRJ.ProjSubSId = 0)
			order by
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId
			return(0)
		end

	-- Tercer nivel: Proyectos
	if @nivel = '3'
		begin
			select
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId,
				PRJ.ProjNom,
				PRJ.ProjFchAlta,
				PRJ.ProjCatId,
				PRJ.ProjClaseId,
				CAT.ProjCatNom,
				CLS.ProjClaseNom
			from
				GesPet..ProyectoIDM PRJ left join 
				GesPet..ProyectoCategoria CAT on (PRJ.ProjCatId = CAT.ProjCatId) left join
				GesPet..ProyectoClase CLS on (PRJ.ProjCatId = CLS.ProjCatId and PRJ.ProjClaseId = CLS.ProjClaseId)
			where
				(PRJ.ProjId = @ProjId ) and
				(PRJ.ProjSubId = @ProjSubId)
			order by
				PRJ.ProjId,
				PRJ.ProjSubId,
				PRJ.ProjSubSId
			return(0)
		end
go

grant execute on dbo.sp_GetProyectoIDMNivel to GesPetUsr
go

print 'Actualización realizada.'
