/*

'P': Pendiente de proceso
'B': Pendiente de autorizar
'X': Rechazado

'E': En proceso
'K': Finalizado OK
'W': Finalizado con error

-000- a. FJS 13.05.2016 - Nuevo SP para marcar como "procesadas" las solicitudes de transmisión de archivos (SSDD).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateSolicitudesSSDDENM1'
go

if exists (select * from sysobjects where name = 'sp_UpdateSolicitudesSSDDENM1' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateSolicitudesSSDDENM1
go

create procedure dbo.sp_UpdateSolicitudesSSDDENM1
	@sol_nroasignado 	int,
	@sol_estado			char(1)='K'
as 
	if @sol_estado = 'E'		-- En proceso
		UPDATE SolicitudesSSDD 
		SET sol_estado = @sol_estado
		WHERE sol_nroasignado = @sol_nroasignado
	else
		UPDATE SolicitudesSSDD 
		SET 
			sol_trnm_fe		= getDate(),
			sol_trnm_usr	= SUSER_NAME(),
			sol_estado		= @sol_estado
		WHERE 
			sol_nroasignado = @sol_nroasignado
		return(0)
go

grant execute on dbo.sp_UpdateSolicitudesSSDDENM1 to GesPetUsr
go
grant execute on dbo.sp_UpdateSolicitudesSSDDENM1 to TrnCGMEnmascara
go
sp_procxmode 'dbo.sp_UpdateSolicitudesSSDDENM1','anymode'
go

print 'Actualización realizada.'
go
