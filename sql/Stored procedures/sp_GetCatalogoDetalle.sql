/*
-000- a. FJS 13.05.2016 - Nuevo SP para obtener el detalle de una tabla del catálogo (SSDD).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetCatalogoDetalle'
go

if exists (select * from sysobjects where name = 'sp_GetCatalogoDetalle' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetCatalogoDetalle
go

create procedure dbo.sp_GetCatalogoDetalle
	@id		int=null
as 
	select 
		cd.id,
		cd.campo,
		cd.pos_desde,
		cd.longitud,
		cd.tipo_id,
		h11.tipo_nom,
		cd.subtipo_id,
		h12.subtipo_nom,
		cd.rutina
	from 
		CatalogoDetalle cd LEFT join
		HEDT011 h11 on (h11.tipo_id = cd.tipo_id) inner join 
		HEDT012 h12 on (h12.tipo_id = cd.tipo_id AND h12.subtipo_id = cd.subtipo_id)
	where 
		cd.id = @id
	order by
		cd.pos_desde	ASC

	return(0)
go

grant execute on dbo.sp_GetCatalogoDetalle to GesPetUsr
go
grant execute on dbo.sp_GetCatalogoDetalle to TrnCGMEnmascara
go
sp_procxmode 'dbo.sp_GetCatalogoDetalle','anymode'
go

print 'Actualización realizada.'
go
