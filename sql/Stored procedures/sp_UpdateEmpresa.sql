/*
-000- a. FJS 11.01.2016 - SPs para el mantenimiento de la tabla Empresa.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateEmpresa'
go

if exists (select * from sysobjects where name = 'sp_UpdateEmpresa' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateEmpresa
go

create procedure dbo.sp_UpdateEmpresa
	@empid		int,
	@empnom		varchar(50)='',
	@emphab		char(1),
	@empabrev	varchar(30)=''
as
	UPDATE GesPet.dbo.Empresa
	SET 
		empnom		= @empnom,
		emphab		= @emphab,
		empabrev	= @empabrev
	WHERE 
		empid = @empid
	return(0)
go

grant execute on dbo.sp_UpdateEmpresa to GesPetUsr
go

print 'Actualización realizada.'
go