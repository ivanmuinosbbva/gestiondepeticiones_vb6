use GesPet
go
print 'sp_GetProyectoXT'
go
if exists (select * from sysobjects where name = 'sp_GetProyectoXT' and sysstat & 7 = 4)
drop procedure dbo.sp_GetProyectoXT
go
create procedure dbo.sp_GetProyectoXT 
	@prj_nrointerno	int,
	@titulo varchar(50)='',
	@cod_estado varchar(250)=''
as 

declare @secuencia int
declare @ret_status     int

select 
	PRJ.prj_nrointerno,
	PRJ.titulo,
	PRJ.corp_local,
	PRJ.cod_orientacion,
	PRJ.importancia_cod,
	PRJ.importancia_prf,
	PRJ.importancia_usr,
	importancia_usrname = isnull((select R2.nom_recurso from Recurso R2 where PRJ.importancia_usr=R2.cod_recurso),''),
	PRJ.cod_estado,
	nom_estado = isnull((select ESTPRJ.nom_estado from Estados ESTPRJ where RTRIM(ESTPRJ.cod_estado)=RTRIM(PRJ.cod_estado)),''),
	PRJ.fe_estado,
	PRJ.fe_ini_plan,
	PRJ.fe_fin_plan,
	PRJ.fe_ini_real,
	PRJ.fe_fin_real,
	PRJ.fe_fec_plan,
	PRJ.fe_ini_orig,
	PRJ.fe_fin_orig,
	PRJ.fe_fec_orig,
	PRJ.cant_planif,
	PRJ.cod_usualta,
	PRJ.fec_alta,
	PRJ.ult_accion,
	PRJ.audit_user,
	PRJ.audit_date
from  Proyecto PRJ
where	@prj_nrointerno=0 or PRJ.prj_nrointerno=@prj_nrointerno and
	(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or charindex(RTRIM(PRJ.cod_estado),RTRIM(@cod_estado))>0) and 
	(@titulo is null or RTRIM(@titulo) is null or RTRIM(@titulo)='' or charindex(RTRIM(@titulo),RTRIM(PRJ.titulo))>0)

return(0) 
go



grant execute on dbo.sp_GetProyectoXT to GesPetUsr 
go


