/*
-000- a. FJS 19.05.2009 - Nuevo SP para manejo de carga de horas desglosado por Aplicativo
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHsTrabAplicativo'
go

if exists (select * from sysobjects where name = 'sp_GetHsTrabAplicativo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHsTrabAplicativo
go

create procedure dbo.sp_GetHsTrabAplicativo
	@cod_recurso		char(10)=null,
	@cod_tarea			char(8)=null,
	@pet_nrointerno		int=null,
	@fe_desde			smalldatetime=null,
	@fe_hasta			smalldatetime=null,
	@horas				smallint=null,
	@app_id				char(30)=null,
	@app_horas			smallint=null
as
	select 
		a.cod_recurso,
		a.cod_tarea,
		a.pet_nrointerno,
		pet_nroasignado = (select x.pet_nroasignado from GesPet..Peticion x where x.pet_nrointerno = a.pet_nrointerno),
		a.fe_desde,
		a.fe_hasta,
		a.horas,
		a.app_id,
		b.app_nombre,
		a.app_horas,
		a.hsapp_texto
	from 
		GesPet..HorasTrabajadasAplicativo a left join 
		GesPet..Aplicativo b on a.app_id = b.app_id
	where
		(a.cod_recurso = @cod_recurso or @cod_recurso is null) and
		(a.cod_tarea = @cod_tarea or @cod_tarea is null) and
		(a.pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null) and
		(a.fe_desde = @fe_desde or @fe_desde is null) and
		(a.fe_hasta = @fe_hasta or @fe_hasta is null) and
		(a.horas = @horas or @horas is null) and
		(a.app_id = @app_id or @app_id is null) and
		(a.app_horas = @app_horas or @app_horas is null)
go

grant execute on dbo.sp_GetHsTrabAplicativo to GesPetUsr
go

print 'Actualización realizada.'
