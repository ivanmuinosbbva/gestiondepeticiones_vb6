/*
************************************************************************************************************************
Autor: 			Fernando J. Spitz
Propósito:		Devolver el nombre de pila y apellido de un recurso
Fecha:			24.07.2007
Ambiente:		SyBase 11 y 12 / SQL Server
Doc. Referencia:	N/A
Última actualización:	
************************************************************************************************************************
Versiones:
************************************************************************************************************************
*/

print 'Creando el sp_GetRecursoNombre...'
go
use GesPet
go
if exists (select * from sysobjects where name = 'sp_GetRecursoNombre' and sysstat & 7 = 4)
drop procedure dbo.sp_GetRecursoNombre
go
create procedure dbo.sp_GetRecursoNombre   
             @cod_recurso	varchar(10)=''   
as   
	select	r.nom_recurso 
	from	GesPet..Recurso r
	where	r.cod_recurso = @cod_recurso

return(0)
go

grant execute on dbo.sp_GetRecursoNombre to GesPetUsr 
go

print 'Actualización finalizada.'

