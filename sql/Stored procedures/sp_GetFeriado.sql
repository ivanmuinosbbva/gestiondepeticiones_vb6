/*
-001- a. FJS 01.07.2009 - Se inicializan los parámetros recibidos y se agrega condicionamiento para devolver todos los registros de ser necesario.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetFeriado'
go

if exists (select * from sysobjects where name = 'sp_GetFeriado' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetFeriado
go

create procedure dbo.sp_GetFeriado 
	@fechadesde		smalldatetime=null, 
    @fechahasta     smalldatetime=null 
as 
	select 
		f.fecha 
	from 
		GesPet..Feriado f
	where
		(@fechadesde is null or f.fecha >= @fechadesde) and 
		(@fechahasta is null or f.fecha <= @fechahasta)
   order by 
		f.fecha 

return(0)
go

grant execute on dbo.sp_GetFeriado to GesPetUsr 
go

print 'Actualización realizada.'
go
