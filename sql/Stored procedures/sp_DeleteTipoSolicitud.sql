/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.07.2014 - Nuevo:
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteTipoSolicitud'
go

if exists (select * from sysobjects where name = 'sp_DeleteTipoSolicitud' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteTipoSolicitud
go

create procedure dbo.sp_DeleteTipoSolicitud
	@cod_tipo	char(1)=''
as
	delete from
		GesPet.dbo.TipoSolicitud
	where
		cod_tipo = @cod_tipo
go

grant execute on dbo.sp_DeleteTipoSolicitud to GesPetUsr
go

print 'Actualización realizada.'
go
