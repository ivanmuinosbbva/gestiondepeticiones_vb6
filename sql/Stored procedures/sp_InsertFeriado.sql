use GesPet
go
print 'sp_InsertFeriado'
go
if exists (select * from sysobjects where name = 'sp_InsertFeriado' and sysstat & 7 = 4)
drop procedure dbo.sp_InsertFeriado
go
create procedure dbo.sp_InsertFeriado 
             @fecha         smalldatetime 
as 
   insert into Feriado    (fecha) 
        values         (@fecha) 
   return(0) 
go



grant execute on dbo.sp_InsertFeriado to GesPetUsr 
go


