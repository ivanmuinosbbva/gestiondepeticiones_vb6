use GesPet
go

print 'sp_DeleteRecursoPerfil'
go

if exists (select * from sysobjects where name = 'sp_DeleteRecursoPerfil' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteRecursoPerfil
go

create procedure dbo.sp_DeleteRecursoPerfil 
	@cod_recurso char(10), 
    @cod_perfil  char(4)
as 

declare @cod_nivel	char(4)
declare @cod_area	char(8)

if exists (select cod_recurso from GesPet..RecursoPerfil where RTRIM(cod_recurso) = RTRIM(@cod_recurso) and RTRIM(cod_perfil) = RTRIM(@cod_perfil)) 
	begin 
		select
			@cod_nivel = rp.cod_nivel,
			@cod_area = rp.cod_area
		from RecursoPerfil rp
		where
			RTRIM(cod_recurso) = RTRIM(@cod_recurso) and 
			RTRIM(cod_perfil) = RTRIM(@cod_perfil) 

		delete GesPet..RecursoPerfil 
		where 
			RTRIM(cod_recurso) = RTRIM(@cod_recurso) and 
			RTRIM(cod_perfil) = RTRIM(@cod_perfil) 

		INSERT INTO GesPet..RecursoPerfilAudit 
		VALUES (@cod_recurso, @cod_perfil, 3, @cod_nivel, @cod_area, getdate(), suser_name())
	end 
else 
	begin 
		raiserror 30001 'RecursoPerfil Inexistente'   
		select 30001 as ErrCode , 'RecursoPerfil Inexistente' as ErrDesc
		return (30001)
	end
return(0) 
go

grant execute on dbo.sp_DeleteRecursoPerfil to GesPetUsr 
go
