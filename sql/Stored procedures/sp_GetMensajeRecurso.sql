/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. FJS 22.03.2010 - Se agrega el campo de los textos fijos de los mensajes (para poder filtrarlos).
-002- a. FJS 10.04.2015 - Nuevo: se agregan datos de la petición para facilitar la tarea de homologación al identificar las peticiones por clase.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetMensajeRecurso'
go

if exists (select * from sysobjects where name = 'sp_GetMensajeRecurso' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetMensajeRecurso
go

create procedure dbo.sp_GetMensajeRecurso
	@cod_recurso	char(10),
	@fecdesde		varchar(8)='NULL',
	@fechasta		varchar(8)='NULL'
as
	set dateformat ymd
	SET NOCOUNT ON

	declare @cod_perfil char(4)
	declare @cod_nivel	char(4)
	declare @cod_area	char(8)

	CREATE TABLE #TmpSalida(
			msg_nrointerno int not null,
			cod_perfil varchar(10) not null,
			cod_direccion varchar(10) not null,
			cod_gerencia varchar(10) null,
			cod_sector varchar(10) null,
			cod_grupo varchar(10) null)

	DECLARE CursPerfil CURSOR FOR
		select cod_perfil, cod_nivel, cod_area
		from RecursoPerfil
		where RTRIM(cod_recurso)=RTRIM(@cod_recurso)
		for read only

	OPEN CursPerfil
	FETCH CursPerfil INTO @cod_perfil,@cod_nivel,@cod_area
	WHILE (@@sqlstatus = 0)
	BEGIN
		insert #TmpSalida
		select
			Me.msg_nrointerno,
			Me.cod_perfil,
			isnull(Ge.cod_direccion,''),
			isnull(Gr.cod_gerencia,''),
			isnull(Gr.cod_sector,''),
			''
		from	
			GesPet..Mensajes Me,
			GesPet..Sector  Gr (index idx_Sector),
			GesPet..Gerencia Ge
		where   (Me.cod_nivel='SECT') and
			(Me.cod_perfil=@cod_perfil) and
			(RTRIM(Gr.cod_sector)=RTRIM(Me.cod_area)) and
			(RTRIM(Gr.cod_gerencia)=RTRIM(Ge.cod_gerencia)) and
			((@cod_nivel='DIRE' and RTRIM(Ge.cod_direccion)=RTRIM(@cod_area)) or
			(@cod_nivel='GERE' and RTRIM(Ge.cod_gerencia)=RTRIM(@cod_area)) or
			(@cod_nivel='SECT' and RTRIM(Gr.cod_sector)=RTRIM(@cod_area)))

		insert #TmpSalida
			select
				Me.msg_nrointerno,
				Me.cod_perfil,
				isnull(Ge.cod_direccion,''),
				isnull(Gr.cod_gerencia,''),
				isnull(Gr.cod_sector,''),
				isnull(Su.cod_grupo,'')
			from	
				GesPet..Mensajes Me,
				GesPet..Grupo  Su,
				GesPet..Sector  Gr (index idx_Sector),
				GesPet..Gerencia Ge
			where   (Me.cod_nivel='GRUP') and
				(Me.cod_perfil=@cod_perfil) and
				(RTRIM(Su.cod_grupo)=RTRIM(Me.cod_area)) and
				(RTRIM(Gr.cod_sector)=RTRIM(Su.cod_sector)) and
				(RTRIM(Gr.cod_gerencia)=RTRIM(Ge.cod_gerencia)) and
				((@cod_nivel='DIRE' and RTRIM(Ge.cod_direccion)=RTRIM(@cod_area)) or
				(@cod_nivel='GERE' and RTRIM(Ge.cod_gerencia)=RTRIM(@cod_area)) or
				(@cod_nivel='SECT' and RTRIM(Gr.cod_sector)=RTRIM(@cod_area)) or
				(@cod_nivel='GRUP' and RTRIM(Su.cod_grupo)=RTRIM(@cod_area)))

		FETCH CursPerfil INTO @cod_perfil,@cod_nivel,@cod_area
	END
	CLOSE CursPerfil
	DEALLOCATE CURSOR CursPerfil

	select  
		Tx.msg_nrointerno,
		Me.fe_mensaje,
		Me.pet_nrointerno,
		Me.cod_perfil,
		Me.txt_txtmsg,
		Me.cod_txtmsg,			-- add -001- a.
		Pe.pet_nroasignado,
		--{ add -002- a.
		Pe.cod_tipo_peticion,
		Pe.cod_clase,
		--}
		Pe.titulo,
		Pf.nom_perfil,
		Mt.msg_texto
	from	
		#TmpSalida Tx,
		GesPet..Mensajes Me,
		GesPet..Peticion Pe,
		GesPet..MensajesTexto Mt,
		GesPet..Perfil Pf
	where   Me.msg_nrointerno=Tx.msg_nrointerno and
		(@fecdesde='NULL' or convert(char(8),Me.fe_mensaje,112)>=@fecdesde) and
		(@fechasta='NULL' or convert(char(8),Me.fe_mensaje,112)<=@fechasta) and
		(Me.pet_nrointerno*=Pe.pet_nrointerno) and
		(Me.cod_perfil*=Pf.cod_perfil) and
		(Me.cod_txtmsg*=Mt.cod_txtmsg)
	order by Tx.msg_nrointerno DESC
	return(0)
go

grant execute on dbo.sp_GetMensajeRecurso to GesPetUsr
go

print 'Actualización realizada.'
go