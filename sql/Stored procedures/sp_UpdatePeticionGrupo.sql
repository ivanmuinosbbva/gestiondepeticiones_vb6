/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. FJS 09.05.2008 - Se agrego el manejo de los nuevos campos fe_produccion, fe_suspension y cod_motivo.
-002- a. FJS 26.01.2011 - Agregado: se actualiza el estado del proyecto asociado a la petición.
-003- a. FJS 18.07.2016 - FIX: se corrige porque genera error de overflow.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionGrupo'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionGrupo' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionGrupo
go

create procedure dbo.sp_UpdatePeticionGrupo 
    @pet_nrointerno     int, 
    @cod_grupo          char(8), 
    @fe_ini_plan        smalldatetime=null, 
    @fe_fin_plan        smalldatetime=null, 
    @fe_ini_real        smalldatetime=null, 
    @fe_fin_real        smalldatetime=null, 
	--{ add -001- a.
	@fe_produccion		smalldatetime=null, 
	@fe_suspension		smalldatetime=null, 
	@cod_motivo			int,
	--}
    --@horaspresup        smallint=0,			-- del -003- a.
	@horaspresup        int=0,					-- add -003- a.
    @cod_estado         char(6)='', 
    @fe_estado			smalldatetime=null, 
    @cod_situacion      char(6)='', 
    @hst_nrointerno_sol int=0, 
    @hst_nrointerno_rsp int=0 
as 
	declare @cod_gerencia		char(8) 
    declare @cod_direccion		char(8)
    declare @cod_sector			char(8)
 	declare @x_fin_plan			smalldatetime 
	declare @x_ini_plan			smalldatetime 
	declare @x_fin_orig			smalldatetime 
	declare @x_ini_orig			smalldatetime 
	declare @cant_planif		int
	declare @replanif			char(1)
	--{ add -002- a.
	declare @pet_projid			int
	declare @pet_projsubid		int
	declare @pet_projsubsid		int
	declare @new_estado			char(6)
	declare @old_estado			char(6)
	declare @tipo_proyecto		char(1)
	--}
	
	select @replanif = 'N'
	select @cod_sector=cod_sector from Grupo where cod_grupo=@cod_grupo 
	select @cod_gerencia=cod_gerencia from Sector where cod_sector=@cod_sector 
	select @cod_direccion=cod_direccion from Gerencia where cod_gerencia=@cod_gerencia 
	 
	if exists (select cod_grupo from GesPet..PeticionGrupo where @pet_nrointerno=pet_nrointerno and RTRIM(cod_grupo)=RTRIM(@cod_grupo)) 
		begin 
			select  
				@x_ini_orig  = fe_ini_orig,
				@x_fin_orig  = fe_fin_orig, 
				@x_ini_plan  = fe_ini_plan, 
				@x_fin_plan  = fe_fin_plan,
				@cant_planif = cant_planif
			from   
				GesPet..PeticionGrupo  
			where @pet_nrointerno=pet_nrointerno and 
				RTRIM(cod_grupo)=RTRIM(@cod_grupo) 

			if  (@fe_ini_plan is not null or @fe_fin_plan is not null) and charindex(@cod_estado,'PLANOK|EJECUC|TERMIN')>0
			/* solo los estados que me interesan */
			/* y seguro que estoy mandando fechas */
			begin 
				if ((@x_ini_plan is null) or ((@x_ini_plan is not null) and ('X'+convert(char(8),@x_ini_plan,112)<>'X'+convert(char(8),@fe_ini_plan,112)))) or 
				   ((@x_fin_plan is null) or ((@x_fin_plan is not null) and ('X'+convert(char(8),@x_fin_plan,112)<>'X'+convert(char(8),@fe_fin_plan,112)))) 
				/* es una fecha nueva o distinta a la que existia */
				/* entonces puedo decir que estoy planificando una vez más */
				begin 
					if @cant_planif is null
						select @cant_planif = 0
					
					select @cant_planif = @cant_planif + 1

					if @cant_planif = 1
						select @replanif = 'P'	-- Primera planificación
					else
						select @replanif = 'R'	-- Replanificación
				end
			end
			
			update 
				GesPet..PeticionGrupo 
			set 	
				cod_sector		   = @cod_sector, 
				cod_gerencia	   = @cod_gerencia, 
				cod_direccion	   = @cod_direccion, 
				fe_ini_plan		   = @fe_ini_plan, 
				fe_fin_plan		   = @fe_fin_plan, 
				fe_ini_real        = @fe_ini_real, 
				fe_fin_real        = @fe_fin_real, 
				--{ add -001- a.
				fe_produccion	   = @fe_produccion,
				fe_suspension	   = @fe_suspension,
				cod_motivo		   = @cod_motivo,
				--}
				horaspresup		   = @horaspresup, 
				cod_estado  	   = @cod_estado, 
				fe_estado		   = @fe_estado, 
				cod_situacion	   = @cod_situacion, 
				hst_nrointerno_sol = @hst_nrointerno_sol, 
				hst_nrointerno_rsp = @hst_nrointerno_rsp, 
				audit_user		   = SUSER_NAME(),   
				audit_date		   = getdate()
			where 
				@pet_nrointerno = pet_nrointerno and 
				RTRIM(cod_grupo)=RTRIM(@cod_grupo)

			if @replanif = 'P'
				update GesPet..PeticionGrupo  
				set fe_ini_orig = @fe_ini_plan, 
					fe_fin_orig = @fe_fin_plan,         
					cant_planif	= @cant_planif,
					fe_fec_orig = getdate(),
					fe_fec_plan = getdate()
				where @pet_nrointerno=pet_nrointerno and 
					RTRIM(cod_grupo)=RTRIM(@cod_grupo) 

			if @replanif = 'R'
				update GesPet..PeticionGrupo  
				set cant_planif	= @cant_planif,
					fe_fec_plan = getdate()
				where @pet_nrointerno=pet_nrointerno and 
					RTRIM(cod_grupo)=RTRIM(@cod_grupo) 

			if  (@fe_ini_plan is null or @fe_fin_plan is null)
				update GesPet..PeticionGrupo  
				set fe_fec_plan = null
				where @pet_nrointerno=pet_nrointerno and 
					RTRIM(cod_grupo)=RTRIM(@cod_grupo)


			--{ add -002- a.
			select 
				@pet_projid = p.pet_projid,
				@pet_projsubid = p.pet_projsubid,
				@pet_projsubsid = p.pet_projsubsid
			from GesPet..Peticion p
			where p.pet_nrointerno = @pet_nrointerno

			if exists 
				(select 
					pr.ProjId
				 from GesPet..ProyectoIDM pr
				 where 
					pr.ProjId = @pet_projid and
					pr.ProjSubId = @pet_projsubid and 
					pr.ProjSubSId = @pet_projsubsid)
				begin
					select 
						@tipo_proyecto = pr.tipo_proyecto,
						@old_estado	= pr.cod_estado
					 from GesPet..ProyectoIDM pr
					 where 
						pr.ProjId = @pet_projid and
						pr.ProjSubId = @pet_projsubid and 
						pr.ProjSubSId = @pet_projsubsid

					if charindex(@old_estado,'DESEST|CANCEL|TERMIN|')=0 and @tipo_proyecto = 'P'
						begin
							-- Obtengo el nuevo estado
							exec sp_GetEstadoProyectoIDM @pet_projid, @pet_projsubid, @pet_projsubsid, @new_estado output
							update 
								GesPet..ProyectoIDM
							set
								cod_estado  = @new_estado,
								fch_estado  = getdate(),
								usr_estado  = SUSER_NAME()
							where 
								ProjId = @pet_projid and
								ProjSubId = @pet_projsubid and
								ProjSubSId = @pet_projsubsid
						end
				end
			--}
		end 
	else 
		begin 
			raiserror 30001 'Modificacion de Grupo inexistente'   
			select 30001 as ErrCode , 'Modificacion de Grupo inexistente' as ErrDesc   
			return (30001)   
		end 
	return(0) 
go

grant execute on dbo.sp_UpdatePeticionGrupo to GesPetUsr 
go

print 'Actualización finalizada.'
go