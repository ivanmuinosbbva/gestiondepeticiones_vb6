use GesPet
go

print 'sp_GetAgrupDepen'
go
if exists (select * from sysobjects where name = 'sp_GetAgrupDepen' and sysstat & 7 = 4)
drop procedure dbo.sp_GetAgrupDepen
go
create procedure dbo.sp_GetAgrupDepen 
	@agr_nrointerno int = 0
as 


declare @secuencia int
declare @ret_status     int


/*    Tabla Temporaria  */
create table #AgrupArbol(
			secuencia	int,
			nivel 		int,
			agr_nrointerno 	int)


/* ejecuta el store procedure recursivo que arma la tabla de trabajo */
select @secuencia=0
execute @ret_status = sp_GetAgrupArbol @agr_nrointerno, 0, @secuencia OUTPUT
if (@ret_status <> 0)
    begin
       if (@ret_status = 30003)
          select 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as DescError
       return (@ret_status)
    end

	/* devuelve los registros procesados con datos propios */
	select  B.secuencia,
		B.nivel,
		A.agr_nrointerno,
		A.agr_nropadre,
		A.agr_titulo,
		A.agr_vigente,
		A.cod_direccion,
		nom_direccion = (select D.nom_direccion from Direccion D where D.cod_direccion=A.cod_direccion),
		A.cod_gerencia,
		nom_gerencia = (select G.nom_gerencia from Gerencia G where G.cod_gerencia=A.cod_gerencia),
		A.cod_sector,
		nom_sector = (select S.nom_sector from Sector S where S.cod_sector=A.cod_sector),
		A.cod_grupo,
		nom_grupo = (select U.nom_grupo from Grupo U where U.cod_grupo=A.cod_grupo),
		A.cod_usualta,
		nom_usualta = (select R.nom_recurso from Recurso R where R.cod_recurso=A.cod_usualta),
		A.agr_visibilidad,
		A.agr_actualiza,
		A.audit_user,
		A.audit_date
	from 	#AgrupArbol B, Agrup A
	where   (A.agr_nrointerno = B.agr_nrointerno)
	order by B.secuencia

return(0) 
go



grant execute on dbo.sp_GetAgrupDepen to GesPetUsr 
go

