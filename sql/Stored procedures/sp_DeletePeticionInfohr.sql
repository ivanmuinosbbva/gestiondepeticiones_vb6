/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 13.08.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionInfohr'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionInfohr' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionInfohr
go

create procedure dbo.sp_DeletePeticionInfohr
	@pet_nrointerno	int,
	@info_id		int,
	@info_idver		int,
	@info_item		int,
	@cod_recurso	char(10)
as
	delete from
		GesPet..PeticionInfohr
	where
		pet_nrointerno = @pet_nrointerno and
		(@info_id is null or info_id = @info_id) and
		(@info_idver is null or info_idver = @info_idver) and
		(@info_item is null or info_item = @info_item) and
		(@cod_recurso is null or cod_recurso = @cod_recurso)
go

grant execute on dbo.sp_DeletePeticionInfohr to GesPetUsr
go

print 'Actualización realizada.'
go
