use GesPet
go
print 'sp_GetProyectoAvanceCab'
go
if exists (select * from sysobjects where name = 'sp_GetProyectoAvanceCab' and sysstat & 7 = 4)
drop procedure dbo.sp_GetProyectoAvanceCab
go
create procedure dbo.sp_GetProyectoAvanceCab 
	@prj_nrointerno	int=0,
	@pav_aamm	char(6)=null
as 

select 
	AVC.prj_nrointerno,
	AVC.pav_aamm,
	AVC.pav_manual,
	AVC.pav_calcul,
	AVC.audit_user,
	AVC.audit_date
from  ProyectoAvanceCab AVC
where	@prj_nrointerno=0 or AVC.prj_nrointerno=@prj_nrointerno and
	(@pav_aamm is null or RTRIM(@pav_aamm) is null or RTRIM(@pav_aamm)='' or RTRIM(AVC.pav_aamm)=RTRIM(@pav_aamm))
order by AVC.prj_nrointerno,AVC.pav_aamm DESC
return(0) 
go

grant execute on dbo.sp_GetProyectoAvanceCab to GesPetUsr 
go
