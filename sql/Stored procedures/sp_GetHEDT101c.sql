/*
-000- a. FJS 04.09.2012 - Nuevo SP para devolver todos los nombres de archivos productivos del catálogo de DSN.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHEDT101c'
go

if exists (select * from sysobjects where name = 'sp_GetHEDT101c' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHEDT101c
go

create procedure dbo.sp_GetHEDT101c
	@dsn_nomprod	char(50)
as 
	select 
		a.dsn_nomprod,
		a.dsn_cpynom,
		a.dsn_cpybbl
	from 
		GesPet..HEDT001 a
	where upper(a.dsn_nomprod) like '%' + upper(LTRIM(RTRIM(@dsn_nomprod))) + '%' 
	group by 
		a.dsn_nomprod,
		a.dsn_cpynom,
		a.dsn_cpybbl
	having 
		a.dsn_nomprod is not null and a.dsn_nomprod <> ''
	--order by a.dsn_nomprod, a.dsn_feult desc
	return(0)
go

grant execute on dbo.sp_GetHEDT101c to GesPetUsr
go

print 'Actualización realizada.'
go

/*
		dsn_nomprod =
		case
			when patindex('%D[0-9][0-9][0-9][0-9][0-9][0-9]',a.dsn_nomprod) > 0 then substring(a.dsn_nomprod,1,patindex('%D[0-9][0-9][0-9][0-9][0-9][0-9]',a.dsn_nomprod)-1)
			when patindex('%F[0-9][0-9][0-9][0-9][0-9][0-9]',a.dsn_nomprod) > 0 then substring(a.dsn_nomprod,1,patindex('%F[0-9][0-9][0-9][0-9][0-9][0-9]',a.dsn_nomprod)-1)
			when patindex('%' + '%%ODATE',a.dsn_nomprod) > 0 then str_replace(a.dsn_nomprod,'%%ODATE',null)
			else a.dsn_nomprod
		end,
*/