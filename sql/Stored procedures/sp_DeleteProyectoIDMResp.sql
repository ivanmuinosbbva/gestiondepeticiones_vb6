/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.16 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.05.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteProyectoIDMResp'
go

if exists (select * from sysobjects where name = 'sp_DeleteProyectoIDMResp' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteProyectoIDMResp
go

create procedure dbo.sp_DeleteProyectoIDMResp
	@ProjId		int,
	@ProjSubId	int,
	@ProjSubSId	int,
	@cod_nivel	char(4),
	@cod_area	char(8)
as
	delete from
		GesPet.dbo.ProyectoIDMResponsables
	where
		ProjId		= @ProjId and
		ProjSubId	= @ProjSubId and
		ProjSubSId	= @ProjSubSId and
		cod_nivel	= @cod_nivel and
		cod_area	= @cod_area
go

grant execute on dbo.sp_DeleteProyectoIDMResp to GesPetUsr
go

print 'Actualización realizada.'
go
