/*
-001- a. FJS 12.02.2016 - Nuevo: se agregan campos para funcionalidades de la nueva gerencia BPE.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetAccion'
go

if exists (select * from sysobjects where name = 'sp_GetAccion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetAccion
go

create procedure dbo.sp_GetAccion 
	@cod_accion  char(8)=null 
as 
	begin 
		select  
			a.cod_accion, 
			a.nom_accion,
			a.IGM_hab,
			a.tipo_accion,
			a.agrupador,
			a.leyenda,
			a.orden,
			a.porEstado,
			a.multiplesEstados
		from 
			GesPet..Acciones a
		where   
			(@cod_accion is null or 
			RTRIM(@cod_accion) is null or 
			RTRIM(@cod_accion)='' or 
			RTRIM(a.cod_accion) = RTRIM(@cod_accion))
		order by 
			a.nom_accion 
	end 
	return(0)
go

grant execute on dbo.sp_GetAccion to GesPetUsr 
go

exec sp_procxmode 'sp_GetAccion', 'anymode'

print 'Actualización realizada.'
go
