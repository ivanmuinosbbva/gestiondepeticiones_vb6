use GesPet
go
print 'sp_UpdateAvanceGrupo'
go
if exists (select * from sysobjects where name = 'sp_UpdateAvanceGrupo' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateAvanceGrupo
go
create procedure dbo.sp_UpdateAvanceGrupo
	@modo			char(1)='A',
	@cod_AvanceGrupo	char(8),
	@nom_AvanceGrupo	char(30),	
	@flg_habil		char(1)=null,
	@txt_AvanceGrupo	char(100)
as



if not exists (select cod_AvanceGrupo from GesPet..AvanceGrupo where RTRIM(cod_AvanceGrupo) = RTRIM(@cod_AvanceGrupo))
	begin 
		raiserror 30011 'Balance Rubro Inexistente'   
		select 30011 as ErrCode , 'Balance Rubro Inexistente' as ErrDesc   
		return (30011)   
	end 

BEGIN TRANSACTION
	if RTRIM(@flg_habil)='N'
	begin
		update GesPet..AvanceCoef
		set flg_habil = 'N'
		from	GesPet..AvanceCoef Bsr
		where   (RTRIM(Bsr.cod_AvanceGrupo) = RTRIM(@cod_AvanceGrupo))
	end
	update GesPet..AvanceGrupo
	set nom_AvanceGrupo = @nom_AvanceGrupo,
	    flg_habil       = @flg_habil,
	    txt_AvanceGrupo = @txt_AvanceGrupo
	where RTRIM(cod_AvanceGrupo) = RTRIM(@cod_AvanceGrupo)
COMMIT

return(0)
go

grant execute on dbo.sp_UpdateAvanceGrupo to GesPetUsr
go
