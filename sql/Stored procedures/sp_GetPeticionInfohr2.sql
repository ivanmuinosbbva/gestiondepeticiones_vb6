use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionInfohr2'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionInfohr2' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionInfohr2
go

create procedure dbo.sp_GetPeticionInfohr2
as 
	select
		d.pet_nrointerno,
		p.pet_nroasignado,
		d.info_id,
		d.info_idver,
		infoprot_nom = (select ic.infoprot_nom from Infoproc ic where ic.infoprot_id = c.infoprot_id),
		c.info_tipo,
		c.info_punto,
		d.info_item,
		r.cod_recurso,
		nom_recurso = (select x.nom_recurso from Recurso x where x.cod_recurso = r.cod_recurso),
		info_valor = case
			when isnull(r.info_valor, d.info_valor) = 1 then 'SOB'
			when isnull(r.info_valor, d.info_valor) = 2 then 'OME'
			when isnull(r.info_valor, d.info_valor) = 3 then 'OMA'
		end,
		i.infoprot_itemdsc,
		i.infoprot_itemresp
	from
		PeticionInfohd d left join
		PeticionInfohc c on (c.pet_nrointerno = d.pet_nrointerno and c.info_id = d.info_id and c.info_idver = d.info_idver) left join 
		PeticionInfohr r on (d.pet_nrointerno = r.pet_nrointerno and d.info_id = r.info_id and d.info_idver = r.info_idver and d.info_item = r.info_item) inner join
		Infoproi i on (i.infoprot_item = d.info_item) inner join 
		Peticion p on (d.pet_nrointerno = p.pet_nrointerno)
	where 
		d.info_valor > 1 and 
		charindex(p.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0
	order by
		d.pet_nrointerno,
		d.info_id,
		d.info_idver,
		r.cod_recurso

	return(0)
go

grant execute on dbo.sp_GetPeticionInfohr2 to GesPetUsr
go

print 'Actualización realizada.'
go
