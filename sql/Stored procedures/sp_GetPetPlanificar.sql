/*
-000- a. FJS 03.12.2010 - Nuevo SP para permitir seleccionar peticiones ya planificadas en períodos anteriores, para el período actual.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPetPlanificar'
go

if exists (select * from sysobjects where name = 'sp_GetPetPlanificar' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPetPlanificar
go

create procedure dbo.sp_GetPetPlanificar
	@per_nrointerno	int=null,
	@referente_rgyp	char(10)=null,
	@referente_desa	char(10)=null,
	@situacion		char(4)=null,
	@estado			char(4)=null,
	@cod_grupo		char(8)=null,
	@priorizadas	char(1)=null
as 
	select
		pc.per_nrointerno,
		per.per_abrev,
		a.fe_pedido,
		a.pet_nroasignado,
		a.cod_tipo_peticion,
		a.cod_clase,
		a.titulo,
		pc.prioridad,
		a.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..Estados x where x.cod_estado = a.cod_estado),
		a.cod_bpar,
		nom_bpar = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.cod_bpar),
		a.cod_BPE,
		nom_BPE = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.cod_BPE),
		puntuacion = case 
			when a.puntuacion is null or a.puntuacion = 0 then 9999999
			when (a.pet_projid is not null and a.pet_projid <> 0) then 9999999
			else
				a.puntuacion
		end,
		analisis = case 
			when a.horaspresup > 0 then '•'
			else '-'
		end,
		planificado = case
			when (a.fe_ini_plan is not null and a.fe_fin_plan is not null) then '•'
			else '-'
		end,
		a.horaspresup,
		a.fe_ini_plan,
		a.fe_fin_plan,
		a.fe_ini_real,
		a.fe_fin_real,
		a.cant_planif,
		compromiso = case
			when month(a.fe_fin_plan) = 1	then 'ENE-' + CONVERT(CHAR(4),YEAR(a.fe_fin_plan))
			when month(a.fe_fin_plan) = 2	then 'FEB-' + CONVERT(CHAR(4),YEAR(a.fe_fin_plan))
			when month(a.fe_fin_plan) = 3	then 'MAR-' + CONVERT(CHAR(4),YEAR(a.fe_fin_plan))
			when month(a.fe_fin_plan) = 4	then 'ABR-' + CONVERT(CHAR(4),YEAR(a.fe_fin_plan))
			when month(a.fe_fin_plan) = 5	then 'MAY-' + CONVERT(CHAR(4),YEAR(a.fe_fin_plan))
			when month(a.fe_fin_plan) = 6	then 'JUN-' + CONVERT(CHAR(4),YEAR(a.fe_fin_plan))
			when month(a.fe_fin_plan) = 7	then 'JUL-' + CONVERT(CHAR(4),YEAR(a.fe_fin_plan))
			when month(a.fe_fin_plan) = 8	then 'AGO-' + CONVERT(CHAR(4),YEAR(a.fe_fin_plan))
			when month(a.fe_fin_plan) = 9	then 'SEP-' + CONVERT(CHAR(4),YEAR(a.fe_fin_plan))
			when month(a.fe_fin_plan) = 10	then 'OCT-' + CONVERT(CHAR(4),YEAR(a.fe_fin_plan))
			when month(a.fe_fin_plan) = 11	then 'NOV-' + CONVERT(CHAR(4),YEAR(a.fe_fin_plan))
			when month(a.fe_fin_plan) = 12	then 'DIC-' + CONVERT(CHAR(4),YEAR(a.fe_fin_plan))
			else '-'
		end,
		cumplido = case
			when month(a.fe_fin_real) = 1	then 'ENE-' + CONVERT(CHAR(4),YEAR(a.fe_fin_real))
			when month(a.fe_fin_real) = 2	then 'FEB-' + CONVERT(CHAR(4),YEAR(a.fe_fin_real))
			when month(a.fe_fin_real) = 3	then 'MAR-' + CONVERT(CHAR(4),YEAR(a.fe_fin_real))
			when month(a.fe_fin_real) = 4	then 'ABR-' + CONVERT(CHAR(4),YEAR(a.fe_fin_real))
			when month(a.fe_fin_real) = 5	then 'MAY-' + CONVERT(CHAR(4),YEAR(a.fe_fin_real))
			when month(a.fe_fin_real) = 6	then 'JUN-' + CONVERT(CHAR(4),YEAR(a.fe_fin_real))
			when month(a.fe_fin_real) = 7	then 'JUL-' + CONVERT(CHAR(4),YEAR(a.fe_fin_real))
			when month(a.fe_fin_real) = 8	then 'AGO-' + CONVERT(CHAR(4),YEAR(a.fe_fin_real))
			when month(a.fe_fin_real) = 9	then 'SEP-' + CONVERT(CHAR(4),YEAR(a.fe_fin_real))
			when month(a.fe_fin_real) = 10	then 'OCT-' + CONVERT(CHAR(4),YEAR(a.fe_fin_real))
			when month(a.fe_fin_real) = 11	then 'NOV-' + CONVERT(CHAR(4),YEAR(a.fe_fin_real))
			when month(a.fe_fin_real) = 12	then 'DIC-' + CONVERT(CHAR(4),YEAR(a.fe_fin_real))
			else '-'
		end,
		pc.pet_nrointerno,
		a.pet_regulatorio,
		origen = case
			when pc.plan_origen = 1 then '^'		-- Agregadas automáticamente al periodo actual por proceso
			when pc.plan_origen = 2 then '+'		-- Agregadas manualmente al periodo por BPE
			when pc.plan_origen = 3 then '!'		-- Viene del periodo anterior
		end,
		a.cod_sector,
		nom_sector = (
			select LTRIM(RTRIM(g.nom_gerencia)) + ' » ' + LTRIM(RTRIM(s.nom_sector))
			from Sector s inner join Gerencia g on (g.cod_gerencia = s.cod_gerencia) 
			where s.cod_sector = a.cod_sector),
		a.pet_projid,
		a.pet_projsubid,
		a.pet_projsubsid,
		ProjNom = (select x.ProjNom from ProyectoIDM x where x.ProjId = a.pet_projid and x.ProjSubId = a.pet_projsubid and x.ProjSubSId = a.pet_projsubsid)
	from 
		GesPet..PeticionPlancab pc left join
		GesPet..Peticion a on (pc.pet_nrointerno = a.pet_nrointerno) left join
		GesPet..Periodo per on (pc.per_nrointerno = per.per_nrointerno)
	where
		(@per_nrointerno is null or pc.per_nrointerno = @per_nrointerno) and
		(@referente_rgyp is null or a.cod_BPE = @referente_rgyp) and 
		(@referente_desa is null or a.cod_bpar = @referente_desa) and 
		(@estado is null or (@estado = 'ACTI' and charindex(a.cod_estado, 'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0)) and 
		(@situacion is null OR 
			(@situacion = 'PEND' and (a.horaspresup = 0 OR a.fe_fin_plan is null)) OR
			(@situacion = 'ANAL' and a.horaspresup > 0) OR
			(@situacion = 'COMP' and (a.horaspresup > 0 and a.fe_fin_plan is not null))
		) and 
		(@cod_grupo is null or @cod_grupo IN (
			select pg.cod_grupo
			from PeticionGrupo pg
			where pg.pet_nrointerno = pc.pet_nrointerno and pg.cod_grupo = @cod_grupo
			group by pg.cod_grupo)) and 
		(@priorizadas is null or @priorizadas = 'S' and pc.prioridad > 0)
	order by
		pc.per_nrointerno,
		a.puntuacion DESC
	
	return (0)
go

grant execute on dbo.sp_GetPetPlanificar to GesPetUsr
go

print 'Actualización realizada.'
go