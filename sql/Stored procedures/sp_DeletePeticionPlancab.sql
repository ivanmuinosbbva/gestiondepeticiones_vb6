/*
-000- a. FJS 20.07.2010 - Nuevo SP para...
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionPlancab'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionPlancab' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionPlancab
go

create procedure dbo.sp_DeletePeticionPlancab
	@per_nrointerno		int, 
	@pet_nrointerno		int
as 
	declare @pet_nroasignado	int
	declare @mensaje			varchar(255)
	declare @hst_nrointerno		int
	declare @cod_estado			char(6)
	declare @per_abrev			varchar(15)

	/*
	if exists (
		select p.pet_nrointerno
		from Peticion p
		where p.pet_nrointerno = @pet_nrointerno)
		begin
			select 
				@pet_nroasignado = p.pet_nroasignado,
				@cod_estado		 = p.cod_estado
			from Peticion p
			where p.pet_nrointerno = @pet_nrointerno

			select @per_abrev = pe.per_abrev
			from Periodo pe
			where pe.per_nrointerno = @per_nrointerno

			-- Agrego en el historial la eliminación de la petición de la planificación
			select @mensaje = 'La petición ' + LTRIM(RTRIM(CONVERT(VARCHAR(12),@pet_nroasignado))) + 
							' ha sido quitada manualmente de la planificación ' + LTRIM(RTRIM(@per_abrev)) + '.'

			exec sp_AddHistorial @hst_nrointerno OUTPUT, @pet_nrointerno, 'PDELPRI0', @cod_estado, null,null,null,null,null,null
			exec sp_AddHistorialMemo @hst_nrointerno, 0, @mensaje
		end
	*/
	
	/*
	-- Elimina el detalle
	delete from 
		GesPet..PeticionPlandet
	where
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null)
	
	-- Elimina los comentarios y/o textos relacionados
	delete from
		GesPet..PeticionPlantxt
	where
		pet_nrointerno = @pet_nrointerno and
		mem_campo = 'PLANIF'
	*/
	
	-- Elimina la cabecera	
	delete from 
		GesPet..PeticionPlancab
	where
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null) and
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null)
	return(0)
go

grant execute on dbo.sp_DeletePeticionPlancab to GesPetUsr
go

print 'Actualización realizada.'
go

