/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.07.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertCatalogoDetalle'
go

if exists (select * from sysobjects where name = 'sp_InsertCatalogoDetalle' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertCatalogoDetalle
go

create procedure dbo.sp_InsertCatalogoDetalle
	@id			int=null,
	@campo		char(50)=null,
	@pos_desde	int=null,
	@longitud	int=null,
	@tipo_id	char(1)=null,
	@subtipo_id	char(1)=null,
	@rutina		char(8)=null
as
	insert into GesPet.dbo.CatalogoDetalle (
		id,
		campo,
		pos_desde,
		longitud,
		tipo_id,
		subtipo_id,
		rutina)
	values (
		@id,
		@campo,
		@pos_desde,
		@longitud,
		@tipo_id,
		@subtipo_id,
		@rutina)
go

grant execute on dbo.sp_InsertCatalogoDetalle to GesPetUsr
go
grant execute on dbo.sp_InsertCatalogoDetalle to TrnCGMEnmascara
go
sp_procxmode 'dbo.sp_InsertCatalogoDetalle','anymode'
go

print 'Actualización realizada.'
go
