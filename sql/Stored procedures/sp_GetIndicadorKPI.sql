/*
-000- a. FJS 17.02.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetIndicadorKPI'
go

if exists (select * from sysobjects where name = 'sp_GetIndicadorKPI' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetIndicadorKPI
go

create procedure dbo.sp_GetIndicadorKPI
	@kpiid		int=null,
	@empresa	int
as
	declare @empresa_inter		int
	declare @empresa_default	int 
	
	select @empresa_default = 1

	select @empresa_inter = isnull(ie.empid_indicador,@empresa_default)
	from
		Empresa e LEFT JOIN 
		IndicadoresEmpresa ie ON (e.empid = ie.empid_peticion)
	where e.empid = @empresa
	
	select 
		k.kpiid,
		k.kpinom,
		k.kpireq,
		k.kpihab,
		k.unimedId,
		um.unimedDesc,
		um.unimedSmb,
		k.kpiayuda,
		k.kpiemp
	from 
		IndicadorKPI k inner join
		UnidadMedida um on (um.unimedId = k.unimedId)
	where
		(@kpiid is null or k.kpiid = @kpiid) and 
		(k.kpiemp = @empresa_inter)
	order by
		k.kpiid
	return(0)
go

grant execute on dbo.sp_GetIndicadorKPI to GesPetUsr
go

print 'Actualización realizada.'
go
