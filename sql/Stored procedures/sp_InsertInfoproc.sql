/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertInfoproc'
go

if exists (select * from sysobjects where name = 'sp_InsertInfoproc' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertInfoproc
go

create procedure dbo.sp_InsertInfoproc
	@infoprot_id		int,
	@infoprot_nom		char(50),
	@infoprot_tipo		char(1),
	@infoprot_punto		char(1),
	@infoprot_estado	char(1),
	@infoprot_vermaj	smallint,
	@infoprot_vermin	smallint
as
	insert into GesPet.dbo.Infoproc (
		infoprot_id,
		infoprot_nom,
		infoprot_tipo,
		infoprot_punto,
		infoprot_estado,
		infoprot_vermaj,
		infoprot_vermin)
	values (
		@infoprot_id,
		@infoprot_nom,
		@infoprot_tipo,
		@infoprot_punto,
		@infoprot_estado,
		@infoprot_vermaj,
		@infoprot_vermin)
go

grant execute on dbo.sp_InsertInfoproc to GesPetUsr
go

print 'Actualización realizada.'
go
