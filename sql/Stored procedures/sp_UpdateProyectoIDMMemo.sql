/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 11.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoIDMMemo'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoIDMMemo' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoIDMMemo
go

create procedure dbo.sp_UpdateProyectoIDMMemo
	@borra_last		char(1)='N',
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int,
	@mem_campo		char(10)='DESCRIPCIO',
	@mem_secuencia	smallint,
	@mem_texto		varchar(255)
as
	declare @textoaux	varchar(255)   
	declare @textomemo	varchar(255)

	select @textomemo = ''

	if exists (select ProjId from GesPet.dbo.ProyectoIDMMemo   
				where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and mem_campo = @mem_campo and
					mem_secuencia = @mem_secuencia)
		begin   
			update 
				GesPet.dbo.ProyectoIDMMemo
			set
				mem_texto = @mem_texto
			where 
				ProjId			= @ProjId and
				ProjSubId		= @ProjSubId and
				ProjSubSId		= @ProjSubSId and
				mem_campo		= @mem_campo and
				mem_secuencia	= @mem_secuencia
		end
	else
		begin
			insert into GesPet.dbo.ProyectoIDMMemo (
				ProjId,
				ProjSubId,
				ProjSubSId,
				mem_campo,
				mem_secuencia,
				mem_texto) 
			values (
				@ProjId,
				@ProjSubId,
				@ProjSubSId,
				@mem_campo,
				@mem_secuencia,
				@mem_texto)
		end
	
	if @borra_last = 'S'   
		begin   
			delete from GesPet.dbo.ProyectoIDMMemo   
			where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId and mem_campo = @mem_campo and
					mem_secuencia > @mem_secuencia
		end
	return(0)
go

grant execute on dbo.sp_UpdateProyectoIDMMemo to GesPetUsr
go

print 'Actualización realizada.'
go
