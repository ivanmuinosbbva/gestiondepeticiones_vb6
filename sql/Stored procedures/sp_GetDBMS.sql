/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.07.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetDBMS'
go

if exists (select * from sysobjects where name = 'sp_GetDBMS' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetDBMS
go

create procedure dbo.sp_GetDBMS
	@dbmsId		int=null
as
	select 
		ag.dbmsId,
		ag.dbmsNom
	from 
		GesPet.dbo.DBMS ag
	where
		(ag.dbmsId = @dbmsId or @dbmsId is null) 
go

grant execute on dbo.sp_GetDBMS to GesPetUsr
go

print 'Actualización realizada.'
go
