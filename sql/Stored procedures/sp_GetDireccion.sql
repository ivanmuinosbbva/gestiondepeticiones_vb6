/*
-000- a. FJS dd.mm.yyyy - Nuevo SP para
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetDireccion'
go

if exists (select * from sysobjects where name = 'sp_GetDireccion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetDireccion
go

create procedure dbo.sp_GetDireccion 
	@cod_direccion	char(8)=null, 
	@flg_habil		char(1)=null 
as 
	begin 
		select 
			cod_direccion,
			nom_direccion,
			flg_habil,
			es_ejecutor,
			IGM_hab,
			abrev_direccion
		from 
			GesPet..Direccion 
		where 
			(@cod_direccion is null or RTRIM(@cod_direccion) is null or RTRIM(@cod_direccion)='' or RTRIM(cod_direccion) = RTRIM(@cod_direccion)) and 
			(@flg_habil is null or RTRIM(@flg_habil) is null or RTRIM(@flg_habil)='' or RTRIM(flg_habil) = RTRIM(@flg_habil)) 
		order by  
			nom_direccion ASC 
	end 
go

grant execute on dbo.sp_GetDireccion to GesPetUsr
go

sp_procxmode 'sp_GetDireccion', 'anymode'
go

print 'Actualización realizada.'
go



 
