use GesPet
go
print 'sp_DeletePerfilCostBenef'
go
if exists (select * from sysobjects where name = 'sp_DeletePerfilCostBenef' and sysstat & 7 = 4)
drop procedure dbo.sp_DeletePerfilCostBenef
go
create procedure dbo.sp_DeletePerfilCostBenef 
           @cod_PerfilCostBenef       char(8) 
as 

if exists (select cod_PerfilCostBenef from GesPet..PerfilCostBenef where RTRIM(cod_PerfilCostBenef) = RTRIM(@cod_PerfilCostBenef)) 
begin 
    delete GesPet..PerfilCostBenef 
      where RTRIM(cod_PerfilCostBenef) = RTRIM(@cod_PerfilCostBenef) 
end 
else 
begin 
    raiserror 30001 'Perfil Costo Beneficio Inexistente'   
    select 30001 as ErrCode , 'Perfil Costo Beneficio Inexistente' as ErrDesc   
    return (30001)   
end 
return(0) 
go


grant execute on dbo.sp_DeletePerfilCostBenef to GesPetUsr 
go
