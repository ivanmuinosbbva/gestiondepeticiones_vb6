use GesPet
go

print 'sp_DeletePeticionChangeMan'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionChangeMan' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionChangeMan
go

create procedure dbo.sp_DeletePeticionChangeMan  
       @pet_nrointerno  int
as  
   delete from GesPet..PeticionChangeMan  
   where pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null
return(0)
go

grant Execute on dbo.sp_DeletePeticionChangeMan to GesPetUsr 
go