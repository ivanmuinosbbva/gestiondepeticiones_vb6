/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetInfoproi'
go

if exists (select * from sysobjects where name = 'sp_GetInfoproi' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetInfoproi
go

create procedure dbo.sp_GetInfoproi
	@infoprot_item		int=0
as
	select 
		p.infoprot_item,
		p.infoprot_itemdsc,
		p.infoprot_itemest,
		p.infoprot_itemtpo,
		p.infoprot_itemres,
		p.infoprot_itemresp
	from 
		GesPet.dbo.Infoproi p
	where
		(p.infoprot_item = @infoprot_item or @infoprot_item is null)
go

grant execute on dbo.sp_GetInfoproi to GesPetUsr
go

print 'Actualización realizada.'
go
