/*
-000- a. FJS 27.10.2008 - Nuevo SP para devolver las clases de categorías para proyectos IDM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyIDMClase'
go

if exists (select * from sysobjects where name = 'sp_GetProyIDMClase' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyIDMClase
go

create procedure dbo.sp_GetProyIDMClase
	@ProjCatId		int=null, 
	@ProjClaseId	int=null, 
	@ProjClaseNom	varchar(100)=null 
as 
	select
		PRJ.ProjCatId,
		PRJ.ProjClaseId,
		PRJ.ProjClaseNom
	from 
		GesPet..ProyectoClase PRJ
	where
		(@ProjCatId	is null or PRJ.ProjCatId = @ProjCatId) and 
		(@ProjClaseId is null or PRJ.ProjClaseId = @ProjClaseId) and 
		(@ProjClaseNom is null or upper(PRJ.ProjClaseNom) like '%' + upper(ltrim(rtrim(@ProjClaseNom))) + '%')
go

grant execute on dbo.sp_GetProyIDMClase to GesPetUsr
go

print 'Actualización realizada.'
go
