use GesPet
go
print 'sp_GetAgrupNivel'
go
if exists (select * from sysobjects where name = 'sp_GetAgrupNivel' and sysstat & 7 = 4)
drop procedure dbo.sp_GetAgrupNivel
go
create procedure dbo.sp_GetAgrupNivel 
	@visibilidad		char(3)='PUB',
	@area			varchar(10)='',
	@vigencia		char(1)='T'
as 

if @visibilidad='PUB'
begin 
    select  
	A.agr_nrointerno,
	A.agr_nropadre,
	A.agr_titulo,
	A.agr_vigente,
	A.cod_direccion,
	nom_direccion = (select D.nom_direccion from Direccion D where D.cod_direccion=A.cod_direccion),
	A.cod_gerencia,
	nom_gerencia = (select G.nom_gerencia from Gerencia G where G.cod_gerencia=A.cod_gerencia),
	A.cod_sector,
	nom_sector = (select S.nom_sector from Sector S where S.cod_sector=A.cod_sector),
	A.cod_grupo,
	nom_grupo = (select U.nom_grupo from Grupo U where U.cod_grupo=A.cod_grupo),
	A.cod_usualta,
	nom_usualta = (select R.nom_recurso from Recurso R where R.cod_recurso=A.cod_usualta),
	A.agr_visibilidad,
	A.agr_actualiza,
	A.audit_user,
	A.audit_date
    from  Agrup A
    where agr_visibilidad='PUB' and
	(@vigencia='T' or @vigencia=agr_vigente)
    order by agr_nrointerno ASC  
end 
if @visibilidad='DIR'
begin 
    select  
	A.agr_nrointerno,
	A.agr_nropadre,
	A.agr_titulo,
	A.agr_vigente,
	A.cod_direccion,
	nom_direccion = (select D.nom_direccion from Direccion D where D.cod_direccion=A.cod_direccion),
	A.cod_gerencia,
	nom_gerencia = (select G.nom_gerencia from Gerencia G where G.cod_gerencia=A.cod_gerencia),
	A.cod_sector,
	nom_sector = (select S.nom_sector from Sector S where S.cod_sector=A.cod_sector),
	A.cod_grupo,
	nom_grupo = (select U.nom_grupo from Grupo U where U.cod_grupo=A.cod_grupo),
	A.cod_usualta,
	nom_usualta = (select R.nom_recurso from Recurso R where R.cod_recurso=A.cod_usualta),
	A.agr_visibilidad,
	A.agr_actualiza,
	A.audit_user,
	A.audit_date
    from  Agrup A
    where agr_visibilidad='DIR' and 
	RTRIM(cod_direccion)=RTRIM(@area) and
	(@vigencia='T' or @vigencia=agr_vigente)
    order by agr_nrointerno ASC  
end 
if @visibilidad='GER'
begin 
    select  
	A.agr_nrointerno,
	A.agr_nropadre,
	A.agr_titulo,
	A.agr_vigente,
	A.cod_direccion,
	nom_direccion = (select D.nom_direccion from Direccion D where D.cod_direccion=A.cod_direccion),
	A.cod_gerencia,
	nom_gerencia = (select G.nom_gerencia from Gerencia G where G.cod_gerencia=A.cod_gerencia),
	A.cod_sector,
	nom_sector = (select S.nom_sector from Sector S where S.cod_sector=A.cod_sector),
	A.cod_grupo,
	nom_grupo = (select U.nom_grupo from Grupo U where U.cod_grupo=A.cod_grupo),
	A.cod_usualta,
	nom_usualta = (select R.nom_recurso from Recurso R where R.cod_recurso=A.cod_usualta),
	A.agr_visibilidad,
	A.agr_actualiza,
	A.audit_user,
	A.audit_date
    from  Agrup A
    where agr_visibilidad='GER' and RTRIM(cod_gerencia)=RTRIM(@area) and
	(@vigencia='T' or @vigencia=agr_vigente)
    order by agr_nrointerno ASC  
end 
if @visibilidad='SEC'
begin 
    select  
	A.agr_nrointerno,
	A.agr_nropadre,
	A.agr_titulo,
	A.agr_vigente,
	A.cod_direccion,
	nom_direccion = (select D.nom_direccion from Direccion D where D.cod_direccion=A.cod_direccion),
	A.cod_gerencia,
	nom_gerencia = (select G.nom_gerencia from Gerencia G where G.cod_gerencia=A.cod_gerencia),
	A.cod_sector,
	nom_sector = (select S.nom_sector from Sector S where S.cod_sector=A.cod_sector),
	A.cod_grupo,
	nom_grupo = (select U.nom_grupo from Grupo U where U.cod_grupo=A.cod_grupo),
	A.cod_usualta,
	nom_usualta = (select R.nom_recurso from Recurso R where R.cod_recurso=A.cod_usualta),
	A.agr_visibilidad,
	A.agr_actualiza,
	A.audit_user,
	A.audit_date
    from  Agrup A
    where agr_visibilidad='SEC' and RTRIM(cod_sector)=RTRIM(@area) and
	(@vigencia='T' or @vigencia=agr_vigente)
    order by agr_nrointerno ASC  
end 
if @visibilidad='GRU'
begin 
    select  
	A.agr_nrointerno,
	A.agr_nropadre,
	A.agr_titulo,
	A.agr_vigente,
	A.cod_direccion,
	nom_direccion = (select D.nom_direccion from Direccion D where D.cod_direccion=A.cod_direccion),
	A.cod_gerencia,
	nom_gerencia = (select G.nom_gerencia from Gerencia G where G.cod_gerencia=A.cod_gerencia),
	A.cod_sector,
	nom_sector = (select S.nom_sector from Sector S where S.cod_sector=A.cod_sector),
	A.cod_grupo,
	nom_grupo = (select U.nom_grupo from Grupo U where U.cod_grupo=A.cod_grupo),
	A.cod_usualta,
	nom_usualta = (select R.nom_recurso from Recurso R where R.cod_recurso=A.cod_usualta),
	A.agr_visibilidad,
	A.agr_actualiza,
	A.audit_user,
	A.audit_date
    from  Agrup A
    where agr_visibilidad='GRU' and RTRIM(cod_grupo)=RTRIM(@area) and
	(@vigencia='T' or @vigencia=agr_vigente)
    order by agr_nrointerno ASC  
end 
if @visibilidad='USR'
begin 
    select  
	A.agr_nrointerno,
	A.agr_nropadre,
	A.agr_titulo,
	A.agr_vigente,
	A.cod_direccion,
	nom_direccion = (select D.nom_direccion from Direccion D where D.cod_direccion=A.cod_direccion),
	A.cod_gerencia,
	nom_gerencia = (select G.nom_gerencia from Gerencia G where G.cod_gerencia=A.cod_gerencia),
	A.cod_sector,
	nom_sector = (select S.nom_sector from Sector S where S.cod_sector=A.cod_sector),
	A.cod_grupo,
	nom_grupo = (select U.nom_grupo from Grupo U where U.cod_grupo=A.cod_grupo),
	A.cod_usualta,
	nom_usualta = (select R.nom_recurso from Recurso R where R.cod_recurso=A.cod_usualta),
	A.agr_visibilidad,
	A.agr_actualiza,
	A.audit_user,
	A.audit_date
    from  Agrup A
    where agr_visibilidad='USR' and RTRIM(cod_usualta)=RTRIM(@area) and
	(@vigencia='T' or @vigencia=agr_vigente)
    order by agr_nrointerno ASC  
end 
return(0) 
go



grant execute on dbo.sp_GetAgrupNivel to GesPetUsr 
go
