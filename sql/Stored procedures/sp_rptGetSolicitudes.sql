/*
-000- a. FJS 12.01.2010 - Nuevo SP para listar las solicitudes pedidas sin enmascaramiento de datos.
-001- a. FJS 01.08.2014 - Nuevo: se agrega tabla con los tipos de solicitud.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_rptGetSolicitudes'
go

if exists (select * from sysobjects where name = 'sp_rptGetSolicitudes' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptGetSolicitudes
go

create procedure dbo.sp_rptGetSolicitudes
	@fch_desde		char(8)='NULL', 
	@fch_hasta		char(8)='NULL',
	@emergencias	char(4)='NULL',
	@cod_recurso	char(10)='NULL'
as 
	/*
	-- Variables para guardar la estructura del supervisor o l�der
	declare @r_cod_sector		char(8)
	declare @r_cod_grupo		char(8)

	select
		@r_cod_sector = x.cod_sector,
		@r_cod_grupo = x.cod_grupo
	from
		GesPet..Recurso x
	where
		x.cod_recurso = @cod_recurso
	*/

	-- Consulta resultado
	select 
		a.sol_nroasignado,
		a.sol_fecha,
		a.sol_mask,
		sol_tipo = (select x.nom_tipo from GesPet..TipoSolicitud x where x.cod_tipo = a.sol_tipo),		-- add -001- a.
		/* { del -001- a.
		sol_tipo = 
		case
			when a.sol_tipo = '1' then 'XCOM'
			when a.sol_tipo = '2' then 'UNLO'
			when a.sol_tipo = '3' then 'TCOR'
			when a.sol_tipo = '4' then 'SORT'
			when a.sol_tipo = '5' then 'XCOM*'
			when a.sol_tipo = '6' then 'SORT*'
		end,
		} */
		sol_file = 
		case
			when a.sol_tipo = '2' then a.sol_mem_Sysin
			when a.sol_tipo = '3' then a.sol_nrotabla
			else a.sol_file_prod
		end,
		sol_petnroasignado = (select x.pet_nroasignado from GesPet..Peticion x where x.pet_nrointerno = a.pet_nrointerno),
		a.sol_recurso,
		nom_recurso = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_recurso),
		b.cod_sector,
		b.cod_grupo,
		a.sol_resp_sect,
		nom_resp_sect = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_resp_sect),
		jus_desc = (select x.jus_descripcion from GesPet..Justificativos x where x.jus_codigo = a.jus_codigo),
		a.sol_texto
	from 
		GesPet..Solicitudes a left join 
		GesPet..Recurso b on a.sol_recurso = b.cod_recurso
	where 
		(@emergencias = 'NULL' or (@emergencias = 'S' and a.sol_mask = 'N')) and
		(@fch_desde = 'NULL' or convert(char(8), a.sol_fecha, 112) >= @fch_desde) and
		(@fch_hasta = 'NULL' or convert(char(8), a.sol_fecha, 112) <= @fch_hasta) and
		(@cod_recurso = 'NULL' or (@cod_recurso = a.sol_resp_ejec or @cod_recurso = a.sol_resp_sect))
		/* 

		-- De esta manera, listaria las solicitudes del supervisor o lider pedido sin tener en cuenta lo hist�rico
		-- y esta mal, porque todo lo solicitado cuando otros lideres o supervisores estuvieron a cargo, quedarian
		-- contabilizados para el supervisor o lider actual.

		(@cod_recurso = 'NULL' or 
			(b.cod_sector = @r_cod_sector and b.cod_grupo = @r_cod_grupo) or 
			(b.cod_sector = @r_cod_sector))
		*/
	order by 
		a.sol_fecha desc
go
 
grant execute on dbo.sp_rptGetSolicitudes to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
