use GesPet
go
print 'sp_AddHistorialMemo'
go
if exists (select * from sysobjects where name = 'sp_AddHistorialMemo' and sysstat & 7 = 4)
drop procedure dbo.sp_AddHistorialMemo
go
create procedure dbo.sp_AddHistorialMemo   
    @hst_nrointerno int,   
    @hst_secuencia smallint, 
    @mem_texto      char(255)   
as   
       insert into GesPet..HistorialMemo   
           (hst_nrointerno,   
            hst_secuencia,   
            mem_texto)   
       values   
           (@hst_nrointerno,   
            @hst_secuencia,   
            @mem_texto)   
return(0)   
go



grant execute on dbo.sp_AddHistorialMemo to GesPetUsr 
go
