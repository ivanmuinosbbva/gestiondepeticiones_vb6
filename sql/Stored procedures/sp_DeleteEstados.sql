/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 28.10.2010 - SPs para el mantenimiento de la tabla Estados
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteEstados'
go

if exists (select * from sysobjects where name = 'sp_DeleteEstados' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteEstados
go

create procedure dbo.sp_DeleteEstados
	@cod_estado	char(6)
as
	delete from GesPet.dbo.Estados
	where (cod_estado = @cod_estado or @cod_estado is null)
	return(0)		-- Código de retorno
go

grant execute on dbo.sp_DeleteEstados to GesPetUsr
go

print 'Actualización realizada.'
go