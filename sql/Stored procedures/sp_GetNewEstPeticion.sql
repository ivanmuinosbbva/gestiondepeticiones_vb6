/* 
ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR 
Intenta replicar la funcionalidad del VB 
*/

/*
-001- a. FJS 23.04.2009 - Sin usar.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetNewEstPeticion'
go

if exists (select * from sysobjects where name = 'sp_GetNewEstPeticion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetNewEstPeticion
go

create procedure dbo.sp_GetNewEstPeticion
	@pet_nrointerno     int,
	@new_estado			char(6) OUTPUT
as
	declare @cod_hijo	varchar(8)
	declare @cod_estado varchar(6)
	declare @flg_rnkup	int

	declare @aEstado	int
	declare @nEstado	int
	declare @xEstado	char(6)

	select @nEstado = 0
	select @xEstado = ''

	declare @flg1		char(1)
	declare @flg2		char(1)

	select @flg1 = 'N'
	select @flg2 = 'N'


	declare CursHijo cursor for
		select
			PSG.cod_sector,
			PSG.cod_estado,
			rnkup = CONVERT(int,EST.flg_rnkup)
		from
			GesPet..PeticionSector PSG, 
			GesPet..Estados EST
		where	
			(@pet_nrointerno = PSG.pet_nrointerno) and
			(RTRIM(EST.cod_estado) = RTRIM(PSG.cod_estado))
		order by 
			EST.rnkup
	for read only

	open CursHijo
		fetch CursHijo into 
			@cod_hijo,
			@cod_estado,
			@flg_rnkup
		while (@@sqlstatus = 0)
			begin
				-- Si el estado del sector es "Finalizado", enciende el primer flag
				if charindex(RTRIM(@cod_estado),RTRIM('TERMIN'))>0
					begin
						select @flg1 = 'S'
					end
				/* 
					Si el estado del sector es alguno de �stos, enciende el segundo flag

					Estados:

					- A planificar
					- Planificado
					- A estimar esfuerzo
					- Esfuerzo estimado
					- En ejecuci�n
					- Suspendido temporariamente
				*/
				if charindex(RTRIM(@cod_estado),RTRIM('EJECUC|PLANIF|PLANOK|ESTIMA|ESTIOK'))>0
					begin
						select @flg2 = 'S'
					end
				/* 
					Compara si el ranking del estado actual es mayor (en valor absoluto) que el del �ltimo estado del sector evaluado.
					La primera evaluaci�n del ciclo comienza con @nEstado en cero. Si alguno de los flags ha sido encendido, entonces
					@nEstado toma el valor del ranking del estado del primer sector evaluado. Si no es un estado de sector con valor de
					ranking, entonces @nEstado permanece con valor cero.
					En @xEstado guardamos el estado de sector que determin� el nuevo valor de @nEstado.
				*/
				if @flg_rnkup > @nEstado
					begin
						select @nEstado = @flg_rnkup
						select @xEstado = @cod_estado
					end
				fetch CursHijo into 
					@cod_hijo,
					@cod_estado,
					@flg_rnkup
			end
	close CursHijo
	deallocate cursor CursHijo
	
	/* 
		Si al menos uno de los sectores ha finalizado (@flg1) pero alguno de los sectores continua en alguno de los estados mencionados para activar el segundo 
		flag, entonces el estado general de la petici�n debe continuar en Ejecuci�n.
	*/
	if @flg1='S' and @flg2='S'
		begin
			select @xEstado = 'EJECUC'
		end

	select @new_estado = @xEstado
return(0)
go

grant execute on dbo.sp_GetNewEstPeticion to GesPetUsr
go

print 'Actualizaci�n realizada.'