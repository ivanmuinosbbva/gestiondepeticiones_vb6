/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetMotivosSuspen'
go

if exists (select * from sysobjects where name = 'sp_GetMotivosSuspen' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetMotivosSuspen
go

create procedure dbo.sp_GetMotivosSuspen
	@cod_motivo_suspen	int=null,
	@estado_hab			char(1)=null
as
	select 
		a.cod_motivo_suspen,
		a.nom_motivo_suspen,
		a.estado_hab
	from 
		GesPet.dbo.MotivosSuspen a
	where
		(a.cod_motivo_suspen = @cod_motivo_suspen or @cod_motivo_suspen is null) and
		(a.estado_hab = @estado_hab or @estado_hab is null)
	return(0)
go

grant execute on dbo.sp_GetMotivosSuspen to GesPetUsr
go

print 'Actualización realizada.'
