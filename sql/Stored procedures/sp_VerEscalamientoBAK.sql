/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go

print 'sp_VerEscalamiento'
go
if exists (select * from sysobjects where name = 'sp_VerEscalamiento' and sysstat & 7 = 4)
   drop procedure dbo.sp_VerEscalamiento
go
create procedure dbo.sp_VerEscalamiento
as

declare @pet_nrointerno int
declare @pzo_desde int
declare @pzo_hasta int
declare @pzo_hoy int
declare @cod_hijo varchar(8)
declare @cod_estado varchar(8)
declare @cod_accion varchar(8)
declare @ult_accion varchar(8)
declare @fe_ini_plan smalldatetime
declare @fe_fin_plan smalldatetime
declare @fec_estado smalldatetime
declare @fec_hoy smalldatetime
declare @nom_estado varchar(40)
declare @aux_txtmsg varchar(250)
declare @aux_txtprx varchar(250)
declare @txt_extend varchar(250)
declare @txt_pzofin varchar(1)

select @fec_hoy = getdate()


DECLARE CursPeticion CURSOR FOR
select	PET.pet_nrointerno,
	PET.cod_estado,
	PET.ult_accion,
	PET.fe_estado,
	ESC.pzo_desde,
	ESC.pzo_hasta,
	ESC.cod_accion
from	Peticion PET, Escalamientos ESC
where  	(RTRIM(ESC.evt_alcance) = 'PET') and
	(RTRIM(ESC.cod_estado) = RTRIM(PET.cod_estado))
for read only

DECLARE CursSector CURSOR FOR
select	PGR.pet_nrointerno,
	PGR.cod_sector,
	PGR.cod_estado,
	PGR.ult_accion,
	PGR.fe_estado,
	PGR.fe_ini_plan,
	PGR.fe_fin_plan,
	ESC.pzo_desde,
	ESC.pzo_hasta,
	ESC.cod_accion
from	PeticionSector PGR, Escalamientos ESC
where	(RTRIM(ESC.evt_alcance) = 'SEC') and
	(RTRIM(ESC.cod_estado) = RTRIM(PGR.cod_estado))
for read only

DECLARE CursGrupo CURSOR FOR
select	PSG.pet_nrointerno,
	PSG.cod_grupo,
	PSG.cod_estado,
	PSG.ult_accion,
	PSG.fe_estado,
	PSG.fe_ini_plan,
	PSG.fe_fin_plan,
	ESC.pzo_desde,
	ESC.pzo_hasta,
	ESC.cod_accion
from	PeticionGrupo PSG, Escalamientos ESC
where	(RTRIM(ESC.evt_alcance) = 'GRU') and
	(RTRIM(ESC.cod_estado) = RTRIM(PSG.cod_estado))
order by PSG.pet_nrointerno
for read only



OPEN CursPeticion
FETCH CursPeticion INTO 
	@pet_nrointerno,
	@cod_estado,
	@ult_accion,
	@fec_estado,
	@pzo_desde,
	@pzo_hasta,
	@cod_accion
WHILE (@@sqlstatus = 0)
BEGIN
	select @pzo_hoy = datediff(day,@fec_estado,@fec_hoy)

	if (@pzo_hoy >= @pzo_desde and @pzo_hoy <= @pzo_hasta) and (RTRIM(@ult_accion) is null or RTRIM(@ult_accion)='' or RTRIM(@ult_accion)<>RTRIM(@cod_accion))
	begin
/*
		print	'%1! %2! %3! %4! %5! %6! %7!',
			@cod_accion,
			@ult_accion,
			@pet_nrointerno,
			@cod_estado,
			@fec_estado,
			@pzo_hoy,
			@pzo_hasta
*/
		if @cod_accion='ESC1PET'
			execute sp_UpdatePetField  
			    @pet_nrointerno=@pet_nrointerno,  
			    @campo='SITUAC',  
			    @valortxt='DEMOR1',  
			    @valordate=null,  
			    @valornum=null  
		if @cod_accion='ESC2PET'
			execute sp_UpdatePetField  
			    @pet_nrointerno=@pet_nrointerno,  
			    @campo='SITUAC',  
			    @valortxt='DEMOR2',  
			    @valordate=null,  
			    @valornum=null  
		execute sp_UpdatePetField  
			@pet_nrointerno=@pet_nrointerno,  
			@campo='ULTACC',  
			@valortxt=@cod_accion,  
			@valordate=null,  
			@valornum=null  

		select @nom_estado = nom_estado from Estados where RTRIM(cod_estado)=RTRIM(@cod_estado)
		select @aux_txtprx=' La fecha límite para su intervención es: ' + convert(varchar(10),dateadd(day,@pzo_hasta,@fec_estado),103) + '.'
		select @aux_txtmsg=' La Peticion se encuentra en el estado: ' + RTRIM(@nom_estado) + '  desde el: ' + convert(varchar(10),@fec_estado,103) + '.'

		execute sp_DoMensaje
			@evt_alcance='PET',
			@cod_accion=@cod_accion,
			@pet_nrointerno=@pet_nrointerno,
			@cod_hijo='',
			@dsc_estado=@cod_estado,
			@txt_txtmsg=@aux_txtmsg,
			@txt_txtprx=@aux_txtprx
	end

	FETCH CursPeticion INTO 
		@pet_nrointerno,
		@cod_estado,
		@ult_accion,
		@fec_estado,
		@pzo_desde,
		@pzo_hasta,
		@cod_accion
END
CLOSE CursPeticion
DEALLOCATE CURSOR CursPeticion
/* listo PETICION */



OPEN CursSector
FETCH CursSector INTO 
	@pet_nrointerno,
	@cod_hijo,
	@cod_estado,
	@ult_accion,
	@fec_estado,
	@fe_ini_plan,
	@fe_fin_plan,
	@pzo_desde,
	@pzo_hasta,
	@cod_accion
WHILE (@@sqlstatus = 0)
BEGIN
	select @nom_estado = nom_estado from Estados where RTRIM(cod_estado)=RTRIM(@cod_estado)

	if @cod_estado<>'PLANOK' and @cod_estado<>'EJECUC'
	begin
		select @pzo_hoy = datediff(day,@fec_estado,@fec_hoy)
		select @aux_txtprx=' La fecha límite para su intervención es: ' + convert(varchar(10),dateadd(day,@pzo_hasta,@fec_estado),103) + '.'
		select @aux_txtmsg=' El Sector se encuentra en el estado: ' + RTRIM(@nom_estado) + '  desde el: ' + convert(varchar(10),@fec_estado,103) + '.'
	end

	if @cod_estado='PLANOK'
	begin
		select @pzo_hoy = datediff(day,@fe_ini_plan,@fec_hoy)
		select @aux_txtprx=' La fecha límite para su intervención es: ' + convert(varchar(10),dateadd(day,@pzo_hasta,@fe_ini_plan),103) + '.'
		select @aux_txtmsg=' El Sector se encuentra en el estado: ' + RTRIM(@nom_estado) + '  con fecha planificada de inicio: ' + convert(varchar(10),@fe_ini_plan,103) + '.'
	end

	if @cod_estado='EJECUC'
	begin
		select @pzo_hoy = datediff(day,@fe_fin_plan,@fec_hoy)
		select @aux_txtprx=' La fecha límite para su intervención es: ' + convert(varchar(10),dateadd(day,@pzo_hasta,@fe_fin_plan),103) + '.'
		select @aux_txtmsg=' El Sector se encuentra en el estado: ' + RTRIM(@nom_estado) + '  con fecha planificada de finalizacion: ' + convert(varchar(10),@fe_fin_plan,103) + '.'
	end


	if (@pzo_hoy >= @pzo_desde and @pzo_hoy <= @pzo_hasta) and (RTRIM(@ult_accion) is null or RTRIM(@ult_accion)='' or RTRIM(@ult_accion)<>RTRIM(@cod_accion))
	begin
/*
		print	'%1! %2! %3! %4! %5! %6! %7! %8! %9! %10!',
			@cod_accion,
			@pet_nrointerno,
			@cod_hijo,
			@cod_estado,
			@fec_estado,
			@fe_ini_plan,
			@fe_fin_plan,
			@pzo_desde,
			@pzo_hoy,
			@pzo_hasta
*/
		if @cod_accion='ESC1SEC'
			execute sp_UpdatePetSecField  
			    @pet_nrointerno=@pet_nrointerno,  
   			    @cod_sector=@cod_hijo,
			    @campo='SITUAC',  
			    @valortxt='DEMOR1',  
			    @valordate=null,  
			    @valornum=null  
		if @cod_accion='ESC2SEC'
			execute sp_UpdatePetSecField  
			    @pet_nrointerno=@pet_nrointerno,  
			    @cod_sector=@cod_hijo,
			    @campo='SITUAC',  
			    @valortxt='DEMOR2',  
			    @valordate=null,  
			    @valornum=null  
		execute sp_UpdatePetSecField  
			@pet_nrointerno=@pet_nrointerno,  
			@cod_sector=@cod_hijo,
			@campo='ULTACC',  
			@valortxt=@cod_accion,  
			@valordate=null,  
			@valornum=null  
		execute sp_DoMensaje
			@evt_alcance='SEC',
			@cod_accion=@cod_accion,
			@pet_nrointerno=@pet_nrointerno,
			@cod_hijo=@cod_hijo,
			@dsc_estado=@cod_estado,
			@txt_txtmsg=@aux_txtmsg,
			@txt_txtprx=@aux_txtprx

	end
	FETCH CursSector INTO 
		@pet_nrointerno,
		@cod_hijo,
		@cod_estado,
		@ult_accion,
		@fec_estado,
		@fe_ini_plan,
		@fe_fin_plan,
		@pzo_desde,
		@pzo_hasta,
		@cod_accion
END
CLOSE CursSector
DEALLOCATE CURSOR CursSector


OPEN CursGrupo
FETCH CursGrupo INTO 
	@pet_nrointerno,
	@cod_hijo,
	@cod_estado,
	@ult_accion,
	@fec_estado,
	@fe_ini_plan,
	@fe_fin_plan,
	@pzo_desde,
	@pzo_hasta,
	@cod_accion
WHILE (@@sqlstatus = 0)
BEGIN
	select @nom_estado = nom_estado from Estados where RTRIM(cod_estado)=RTRIM(@cod_estado)

	if @cod_estado<>'PLANOK' and @cod_estado<>'EJECUC'
	begin
		select @pzo_hoy = datediff(day,@fec_estado,@fec_hoy)
		select @aux_txtmsg=' El Grupo se encuentra en el estado: ' + RTRIM(@nom_estado) + '  desde el: ' + convert(varchar(10),@fec_estado,103) + '.'
		select @aux_txtprx=' La fecha límite para su intervención es: ' + convert(varchar(10),dateadd(day,@pzo_hasta,@fec_estado),103) + '.'
	end

	if @cod_estado='PLANOK'
	begin
		select @pzo_hoy = datediff(day,@fe_ini_plan,@fec_hoy)
		select @aux_txtmsg=' El Grupo se encuentra en el estado: ' + RTRIM(@nom_estado) + '  con fecha planificada de inicio: ' + convert(varchar(10),@fe_ini_plan,103) + '.'
		select @aux_txtprx=' La fecha límite para su intervención es: ' + convert(varchar(10),dateadd(day,@pzo_hasta,@fe_ini_plan),103) + '.'
	end

	if @cod_estado='EJECUC'
	begin
		select @pzo_hoy = datediff(day,@fe_fin_plan,@fec_hoy)
		select @aux_txtmsg=' El Grupo se encuentra en el estado: ' + RTRIM(@nom_estado) + '  con fecha planificada de finalizacion: ' + convert(varchar(10),@fe_fin_plan,103) + '.'
		select @aux_txtprx=' La fecha límite para su intervención es: ' + convert(varchar(10),dateadd(day,@pzo_hasta,@fe_fin_plan),103) + '.'
	end

/*		print	'%1! %2! %3! %4! %5! %6! %7! %8! %9! %10!',
			@cod_accion,
			@pet_nrointerno,
			@cod_hijo,
			@cod_estado,
			@fec_estado,
			@fe_ini_plan,
			@fe_fin_plan,
			@pzo_desde,
			@pzo_hoy,
			@pzo_hasta
*/
	if (@pzo_hoy >= @pzo_desde and @pzo_hoy <= @pzo_hasta) and (RTRIM(@ult_accion) is null or RTRIM(@ult_accion)='' or RTRIM(@ult_accion)<>RTRIM(@cod_accion))
	begin
/*
		print	'%1! %2! %3! %4! %5! %6! %7! %8! %9! %10!',
			@cod_accion,
			@pet_nrointerno,
			@cod_hijo,
			@cod_estado,
			@fec_estado,
			@fe_ini_plan,
			@fe_fin_plan,
			@pzo_desde,
			@pzo_hoy,
			@pzo_hasta
*/
		if @cod_accion='ESC1GRU'
			execute sp_UpdatePetSubField  
			    @pet_nrointerno=@pet_nrointerno,  
   			    @cod_grupo=@cod_hijo,
			    @campo='SITUAC',  
			    @valortxt='DEMOR1',  
			    @valordate=null,  
			    @valornum=null  
		if @cod_accion='ESC2GRU'
			execute sp_UpdatePetSubField  
			    @pet_nrointerno=@pet_nrointerno,  
   			    @cod_grupo=@cod_hijo,
			    @campo='SITUAC',  
			    @valortxt='DEMOR2',  
			    @valordate=null,  
			    @valornum=null  
		execute sp_UpdatePetSubField  
			@pet_nrointerno=@pet_nrointerno,  
			@cod_grupo=@cod_hijo,
			@campo='ULTACC',  
			@valortxt=@cod_accion,  
			@valordate=null,  
			@valornum=null  
		execute sp_DoMensaje
			@evt_alcance='GRU',
			@cod_accion=@cod_accion,
			@pet_nrointerno=@pet_nrointerno,
			@cod_hijo=@cod_hijo,
			@dsc_estado=@cod_estado,
			@txt_txtmsg=@aux_txtmsg,
			@txt_txtprx=@aux_txtprx
	end
	FETCH CursGrupo INTO 
		@pet_nrointerno,
		@cod_hijo,
		@cod_estado,
		@ult_accion,
		@fec_estado,
		@fe_ini_plan,
		@fe_fin_plan,
		@pzo_desde,
		@pzo_hasta,
		@cod_accion
END
CLOSE CursGrupo
DEALLOCATE CURSOR CursGrupo

return(0)
go
grant execute on dbo.sp_VerEscalamiento to GesPetUsr
go
