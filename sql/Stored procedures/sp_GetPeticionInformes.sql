/*
-000- a. FJS 16.04.2015 - Nuevo: crea de manera automática el encabezado y detalle de un informe de homologación.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionInformes'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionInformes' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionInformes
go

create procedure dbo.sp_GetPeticionInformes
	@pet_nrointerno		int=null,
	@cod_estado			varchar(100)=null,
	@cod_homologador	char(10)=null, 
	@fecha_desde		smalldatetime=null,
	@fecha_hasta		smalldatetime=null
	/*,
	@infoprot_id		int,
	@cod_recurso		char(10)
	*/
as
	/*
		Todas las peticiones en estado activo:

		- Con informes iniciales
		- Con seguimiento
		- Pendientes de revalidar
	*/
	select
		p.pet_emp,
		p.pet_nroasignado,
		p.pet_nrointerno,
		p.cod_tipo_peticion,
		p.cod_clase,
		p.pet_imptech,
		p.pet_regulatorio,
		pet_ro = isnull(p.pet_ro,'-'),
		p.titulo,
		p.cod_solicitante,
		nom_solicitante = (select r.nom_recurso from Recurso r where r.cod_recurso = p.cod_solicitante),
		p.cod_estado,
		nom_estado = (select e.nom_estado from Estados e where e.cod_estado = p.cod_estado),
		p.fe_estado,
		p.cod_bpar,
		nom_bpar = (select r.nom_recurso from Recurso r where r.cod_recurso = p.cod_bpar),
		ih.info_id,
		ih.info_idver,
		ih.info_fecha,    
		ih.info_tipo,
		info_tiponom = (case ih.info_tipo
			when 'C' then 'Completo'
			when 'R' then 'Reducido'
		end),
		ih.info_punto,
		info_puntonom = (case ih.info_punto
			when 1 then '1er.'
			when 2 then '2do.'
			when 3 then '3er.'
		end),
		ih.info_recurso,
		nom_homologador = (select r.nom_recurso from Recurso r where r.cod_recurso = ih.info_recurso),
		ih.info_estado,
		info_estadonom = (case ih.info_estado
			when 'CONFEC' then 'En confección'
			when 'SUSPEN' then 'En suspensión'
			when 'TERMIN' then 'Finalizado'
			when 'CANCEL' then 'Cancelado'
			when 'VERIFI' then 'Verificado'
			when 'BAJA'	then 'Baja'
		end),
		ih.infoprot_id,
		infoprot_nom = (select ic.infoprot_nom from Infoproc ic where ic.infoprot_id = ih.infoprot_id),
		info_cantidad = (select count(1) from PeticionInfohc phc where phc.pet_nrointerno = ih.pet_nrointerno)
	from 
		Peticion p inner join
		vw_InformesHomoUltimos ih on (ih.pet_nrointerno = p.pet_nrointerno)
	where 
		p.pet_nrointerno in (
			select pc.pet_nrointerno
			from PeticionInfohc pc 
			where pc.pet_nrointerno = p.pet_nrointerno
			group by pc.pet_nrointerno) and
		(@pet_nrointerno is null or p.pet_nrointerno = @pet_nrointerno) and
		(@cod_estado is null or charindex(ih.info_estado, ltrim(rtrim(@cod_estado))) > 0) and
		(@cod_homologador is null or ih.info_recurso = ltrim(rtrim(@cod_homologador))) and
		(@fecha_desde is null or convert(char(8),ih.info_fecha,112) >= convert(char(8),@fecha_desde,112)) and
		(@fecha_hasta is null or convert(char(8),ih.info_fecha,112) <= convert(char(8),@fecha_hasta,112))
	order by
		ih.info_fecha,
		p.pet_nroasignado,
		p.cod_tipo_peticion,
		p.cod_clase
	return(0)
go

grant execute on dbo.sp_GetPeticionInformes to GesPetUsr
go

print 'Actualización realizada.'
go


/*
pc.info_id,
		ic.infoprot_nom,
		pc.info_tipo,
		info_tiponom = (case pc.info_tipo
			when 'C' then 'Completo'
			when 'R' then 'Reducido'
		end),
		pc.info_punto,
		pc.info_fecha,
		pc.info_recurso,
		nom_homologador = (select r.nom_recurso from Recurso r where r.cod_recurso = pc.info_recurso),
		pc.info_estado,
		pc.infoprot_id
*/