/*
-000- a. FJS 26.03.2009 - Nuevo SP para determinar si una petición es histórica o no.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionEsHistorica'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionEsHistorica' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionEsHistorica
go

create procedure dbo.sp_GetPeticionEsHistorica
	@pet_nrointerno		int = null
as 
	select
		a.pet_nrointerno
	from
		GesPet..hPeticion a
	where
		a.pet_nrointerno = @pet_nrointerno or @pet_nrointerno is null
	order by
		a.pet_nrointerno

	return(0)
go

grant execute on dbo.sp_GetPeticionEsHistorica to GesPetUsr
go

print 'Actualización realizada.'
