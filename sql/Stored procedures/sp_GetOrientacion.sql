/*
-001- a. FJS 13.01.2009 - Optimización de SP para mejorar la performance.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetOrientacion'
go

if exists (select * from sysobjects where name = 'sp_GetOrientacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetOrientacion
go

create procedure dbo.sp_GetOrientacion 
    @cod_orientacion    char(2)=null, 
    @flg_habil			char(1)=null 
as 
	begin 
		select  
			o.cod_orientacion, 
			o.nom_orientacion, 
			o.flg_habil,
			o.orden,
			o.descripcion
		from 
			GesPet..Orientacion o
		where   
			(@cod_orientacion is null or RTRIM(@cod_orientacion) is null or RTRIM(@cod_orientacion)='' or RTRIM(cod_orientacion) = RTRIM(@cod_orientacion)) and  
			(@flg_habil is null or RTRIM(@flg_habil) is null or RTRIM(@flg_habil)='' or RTRIM(flg_habil) = RTRIM(@flg_habil)) 
		order by 
			o.orden
		/*{ del 
		order by 
			nom_orientacion,
			flg_habil		-- add -001- a.
		*/
	end 
return(0) 
go

grant execute on dbo.sp_GetOrientacion to GesPetUsr 
go

print 'Actualización realizada.'
