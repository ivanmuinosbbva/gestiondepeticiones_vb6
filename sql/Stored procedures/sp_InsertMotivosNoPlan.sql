/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertMotivosNoPlan'
go

if exists (select * from sysobjects where name = 'sp_InsertMotivosNoPlan' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertMotivosNoPlan
go

create procedure dbo.sp_InsertMotivosNoPlan
	@cod_motivo_noplan	int,
	@nom_motivo_noplan	char(50),
	@estado_hab			char(1)='S'
as
	insert into GesPet.dbo.MotivosNoPlan (
		cod_motivo_noplan,
		nom_motivo_noplan,
		estado_hab)
	values (
		@cod_motivo_noplan,
		@nom_motivo_noplan,
		@estado_hab)
	return(0)
go

grant execute on dbo.sp_InsertMotivosNoPlan to GesPetUsr
go

print 'Actualización realizada.'
