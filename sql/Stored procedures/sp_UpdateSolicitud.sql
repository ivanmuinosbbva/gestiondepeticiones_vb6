/*
-000- a. FJS 26.06.2009 - Nuevo SP para el manejo de solicitudes a Carga de M�quina
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateSolicitudes'
go

if exists (select * from sysobjects where name = 'sp_UpdateSolicitudes' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateSolicitudes
go

create procedure dbo.sp_UpdateSolicitudes
	@sol_nroasignado	int=null,
	@sol_tipo			char(1)=null,
	@sol_mask			char(1)=null,
	@sol_fecha			smalldatetime=null,
	@sol_recurso		char(10)=null,
	@pet_nrointerno		int=null,
	@sol_resp_ejec		char(10)=null,
	@sol_resp_sect		char(10)=null,
	@sol_seguridad		char(10)=null,
	@jus_codigo			int=null,
	@sol_texto			char(255)=null,
	@sol_fe_auto1		smalldatetime=null,
	@sol_fe_auto2		smalldatetime=null,
	@sol_fe_auto3		smalldatetime=null,
	@sol_file_prod		char(255)=null,
	@sol_file_desa		char(255)=null,
	@sol_file_out		char(255)=null,
	@sol_lib_Sysin		char(255)=null,
	@sol_mem_Sysin		char(255)=null,
	@sol_join			char(1)=null,
	@sol_nrotabla		char(4)=null
as

declare @fe_formato		char(27)
declare @fe_componente	char(2)
declare @today			smalldatetime

/* 
==========================================================================================================================================================
INICIO: formateo de la fecha y la hora
==========================================================================================================================================================
*/

select @today = getdate()
select @fe_formato = '- '

-- D�a
select @fe_componente = convert(char(2), datepart(dd, @today))

if abs(convert(int, @fe_componente)) < 10
	select @fe_componente = '0' + rtrim(@fe_componente)

	select @fe_formato = rtrim(@fe_formato) + ' ' + @fe_componente + ' / '

-- Mes
select @fe_componente = convert(char(2), datepart(mm, @today))

if abs(convert(int, @fe_componente)) < 10
	select @fe_componente = '0' + rtrim(@fe_componente)

	select @fe_formato = rtrim(@fe_formato) + ' ' + @fe_componente + ' / '

-- A�o
select @fe_formato = rtrim(@fe_formato) + ' ' + substring(convert(char(4), datepart(yy, @today)), 3, 2) + ' - '

-- Horas
select @fe_componente = convert(char(2), datepart(hh, @today))

if abs(convert(int, @fe_componente)) < 10
	if abs(convert(int, @fe_componente)) = 0
		select @fe_componente = '00'
	else
		select @fe_componente = '0' + rtrim(@fe_componente)

	select @fe_formato = rtrim(@fe_formato) + ' ' + @fe_componente + ':'

-- Minutos
select @fe_componente = convert(char(2), datepart(mi, @today))

if abs(convert(int, @fe_componente)) < 10
	if abs(convert(int, @fe_componente)) = 0
		select @fe_componente = '00'
	else
		select @fe_componente = '0' + rtrim(@fe_componente)
	
	select @fe_formato = rtrim(@fe_formato) + @fe_componente + ':'

-- Segundos
select @fe_componente = convert(char(2), datepart(ss, @today))

if abs(convert(int, @fe_componente)) < 10
	if abs(convert(int, @fe_componente)) = 0
		select @fe_componente = '00'
	else
		select @fe_componente = '0' + rtrim(@fe_componente)
	
	select @fe_formato = rtrim(@fe_formato) + @fe_componente + ' -'

/* 
==========================================================================================================================================================
FIN: formateo de la fecha y la hora
==========================================================================================================================================================
*/

	update 
		GesPet..Solicitudes
	set
		sol_tipo		= @sol_tipo,
		sol_mask		= @sol_mask,
		sol_fecha		= @today,
		sol_recurso		= @sol_recurso,
		pet_nrointerno	= @pet_nrointerno,
		sol_resp_ejec	= @sol_resp_ejec,
		sol_resp_sect	= @sol_resp_sect,
		sol_seguridad	= @sol_seguridad,
		jus_codigo		= @jus_codigo,
		sol_texto		= @sol_texto,
		sol_fe_auto1	= @sol_fe_auto1,
		sol_fe_auto2	= @sol_fe_auto2,
		sol_fe_auto3	= @sol_fe_auto3,
		sol_file_prod	= @sol_file_prod,
		sol_file_desa	= @sol_file_desa,
		sol_file_out	= @sol_file_out,
		sol_lib_Sysin	= @sol_lib_Sysin,
		sol_mem_Sysin	= @sol_mem_Sysin,
		sol_join		= @sol_join,
		sol_nrotabla	= @sol_nrotabla,
		fe_formato		= @fe_formato
	where 
		sol_nroasignado = @sol_nroasignado
go

grant execute on dbo.sp_UpdateSolicitudes to GesPetUsr
go

print 'Actualizaci�n realizada.'
