/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. FJS 15.01.2009 - Se agregan nuevos campos para actualizar.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateRecursoField'
go

if exists (select * from sysobjects where name = 'sp_UpdateRecursoField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateRecursoField
go

create procedure dbo.sp_UpdateRecursoField 
    @cod_recurso	char(10), 
    @campo			char(15), 
    @valortxt		char(100)=null, 
    @valordate		smalldatetime=null, 
    @valornum		int=null 
 
as 
if exists (select cod_recurso from GesPet..Recurso where RTRIM(cod_recurso) = RTRIM(@cod_recurso)) 
	begin 
		if RTRIM(@campo)='DIRE' 
			update GesPet..Recurso 
			set cod_direccion = SUBSTRING(RTRIM(@valortxt) + '        ',1,8), 
				audit_user = SUSER_NAME(), 
				audit_date = getdate() 
			where RTRIM(cod_recurso) = RTRIM(@cod_recurso) 
		if RTRIM(@campo)='GERE' 
			update GesPet..Recurso 
			set cod_gerencia = SUBSTRING(RTRIM(@valortxt) + '        ',1,8), 
				audit_user = SUSER_NAME(), 
				audit_date = getdate() 
			where RTRIM(cod_recurso) = RTRIM(@cod_recurso) 
		if RTRIM(@campo)='SECT' 
			update GesPet..Recurso 
			 set    cod_sector = SUBSTRING(RTRIM(@valortxt) + '        ',1,8), 
				audit_user = SUSER_NAME(), 
				audit_date = getdate() 
			where RTRIM(cod_recurso) = RTRIM(@cod_recurso) 
		if RTRIM(@campo)='GRUP' 
			update GesPet..Recurso 
			set cod_grupo = SUBSTRING(RTRIM(@valortxt) + '        ',1,8), 
				audit_user = SUSER_NAME(), 
				audit_date = getdate() 
			where RTRIM(cod_recurso) = RTRIM(@cod_recurso) 
		if RTRIM(@campo)='CARG' 
			update GesPet..Recurso 
			set flg_cargoarea = SUBSTRING(RTRIM(@valortxt) + ' ',1,1), 
				audit_user = SUSER_NAME(), 
				audit_date = getdate() 
			where RTRIM(cod_recurso) = RTRIM(@cod_recurso) 
		end 
		--{ add -001- a.
		if RTRIM(@campo)='FE_SEG' 
			update 
				GesPet..Recurso
			set 
				fe_seguridad = @valordate, 
				audit_user = SUSER_NAME(), 
				audit_date = getdate()
			where 
				RTRIM(cod_recurso) = RTRIM(@cod_recurso)
		--}
		--{ add -002- a.
		if lower(RTRIM(@campo))='estado_recurso' 
			update GesPet..Recurso
			set estado_recurso = @valortxt
			where RTRIM(cod_recurso) = RTRIM(@cod_recurso)

		if lower(RTRIM(@campo))='horasdiarias' 
			update GesPet..Recurso
			set horasdiarias = @valornum
			where RTRIM(cod_recurso) = RTRIM(@cod_recurso)

		if lower(RTRIM(@campo))='observaciones' 
			update GesPet..Recurso
			set observaciones = @valortxt
			where RTRIM(cod_recurso) = RTRIM(@cod_recurso)

		if lower(RTRIM(@campo))='email' 
			update GesPet..Recurso
			set email = @valortxt
			where RTRIM(cod_recurso) = RTRIM(@cod_recurso)

		if lower(RTRIM(@campo))='euser' 
			update GesPet..Recurso
			set euser = @valortxt
			where RTRIM(cod_recurso) = RTRIM(@cod_recurso)
		--}
	return(0) 
go

grant execute on dbo.sp_UpdateRecursoField to GesPetUsr 
go

print 'Actualización realizada.'
go
