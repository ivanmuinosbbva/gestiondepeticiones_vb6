use GesPet
go
print 'sp_UpdatePrjFechas'
go
if exists (select * from sysobjects where name = 'sp_UpdatePrjFechas' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdatePrjFechas
go
create procedure dbo.sp_UpdatePrjFechas
    @prj_nrointerno     int,
    @fe_ini_plan    smalldatetime=null,
    @fe_fin_plan    smalldatetime=null,
    @fe_ini_real    smalldatetime=null,
    @fe_fin_real    smalldatetime=null
as
declare @x_fin_orig smalldatetime
declare @x_ini_orig smalldatetime
declare @x_fin_plan smalldatetime
declare @x_ini_plan smalldatetime
declare @cod_estado char(6)
declare @cant_planif int
declare @replanif char(1)
select @replanif = 'N'

declare @aux_txtmsg varchar(250)

select  @x_ini_orig=fe_ini_orig,@x_fin_orig=fe_fin_orig, @x_ini_plan=fe_ini_plan, @x_fin_plan=fe_fin_plan,@cod_estado=cod_estado,@cant_planif=cant_planif
from   Proyecto
where  prj_nrointerno = @prj_nrointerno

if  (@fe_ini_plan is not null or @fe_fin_plan is not null) and charindex(@cod_estado,'PLANOK|EJECUC|TERMIN')>0
/* solo los estados que me interesan */
/* y seguro que estoy mandando fechas */
begin
	if ((@x_ini_plan is null) or ((@x_ini_plan is not null) and ('X'+convert(char(8),@x_ini_plan,112)<>'X'+convert(char(8),@fe_ini_plan,112)))) or
	   ((@x_fin_plan is null) or ((@x_fin_plan is not null) and ('X'+convert(char(8),@x_fin_plan,112)<>'X'+convert(char(8),@fe_fin_plan,112))))
	/* es una fecha nueva o distinta a la que existia */
	/* entonces puedo decir que estoy planificando una vez m�s */
	begin
		if @cant_planif is null
			select @cant_planif = 0
		select @cant_planif = @cant_planif + 1
		if @cant_planif = 1
			select @replanif = 'P'
		else
			select @replanif = 'R'
	end
end

/*
if (@x_fin_plan is not null) and
   (@fe_fin_plan is not null) and
   ('X' + convert(char(8),@x_fin_plan,112)<> 'X' + convert(char(8),@fe_fin_plan,112))
begin
    select @aux_txtmsg='Cambio de fecha planificada de finalizaci�n del: ' + convert(varchar(10),@x_fin_plan,103) + '  al: ' + convert(varchar(10),@fe_fin_plan,103) + '.'

    execute sp_DoMensaje
        @evt_alcance='PET',
        @cod_accion='PCHGFEC',
        @prj_nrointerno=@prj_nrointerno,
        @cod_hijo='',
        @dsc_estado='NULL',
        @txt_txtmsg=@aux_txtmsg,
        @txt_txtprx=''

end
*/
update Proyecto
set fe_ini_plan     =  @fe_ini_plan,
    fe_fin_plan     =  @fe_fin_plan,
    fe_ini_real     =  @fe_ini_real,
    fe_fin_real     =  @fe_fin_real
where  prj_nrointerno = @prj_nrointerno


if @replanif = 'P'
update Proyecto
set fe_ini_orig = @fe_ini_plan,
    fe_fin_orig = @fe_fin_plan,
    cant_planif	= @cant_planif,
    fe_fec_orig = getdate(),
    fe_fec_plan = getdate()
where  prj_nrointerno = @prj_nrointerno

if @replanif = 'R'
update Proyecto
set cant_planif	= @cant_planif,
    fe_fec_plan = getdate()
where  prj_nrointerno = @prj_nrointerno

if  (@fe_ini_plan is null or @fe_fin_plan is null)
update Proyecto
set fe_fec_plan = null
where  prj_nrointerno = @prj_nrointerno

return(0)
go


grant execute on dbo.sp_UpdatePrjFechas to GesPetUsr
go
