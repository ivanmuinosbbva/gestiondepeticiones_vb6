/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 02.10.2009 - 
-001- a. FJS 03.11.2009 - Se agrega el campo de actualización de estado por proceso de transmisión.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHEDT002'
go

if exists (select * from sysobjects where name = 'sp_GetHEDT002' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHEDT002
go

create procedure dbo.sp_GetHEDT002
	@dsn_id		char(8)=null,
	@cpy_id		char(8)=null,
	@cpy_dsc	char(50)=null
as
	select 
		a.dsn_id,
		a.cpy_id,
		a.cpy_dsc,
		a.cpy_nomrut,
		a.cpy_feult,
		a.cpy_userid,
		--a.estado,
		a.fe_hst_estado,		-- add -001- a.
		b.estado
	from 
		GesPet..HEDT002 a inner join
		GesPet..HEDT001 b on (a.dsn_id = b.dsn_id)
	where
		(a.dsn_id = @dsn_id or @dsn_id is null) and
		(a.cpy_id = @cpy_id or @cpy_id is null) and
		(a.cpy_dsc = @cpy_dsc or @cpy_dsc is null)
go

grant execute on dbo.sp_GetHEDT002 to GesPetUsr
go

print 'Actualización realizada.'
go
