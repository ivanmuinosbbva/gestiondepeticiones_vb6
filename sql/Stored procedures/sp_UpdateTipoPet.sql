/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.10.2014 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateTipoPet'
go

if exists (select * from sysobjects where name = 'sp_UpdateTipoPet' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateTipoPet
go

create procedure dbo.sp_UpdateTipoPet
	@cod_tipopet	char(3),
	@nom_tipopet	char(50)
as
	update 
		GesPet.dbo.TipoPet
	set
		nom_tipopet = @nom_tipopet
	where 
		cod_tipopet = @cod_tipopet
go

grant execute on dbo.sp_UpdateTipoPet to GesPetUsr
go

print 'Actualización realizada.'
go
