/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 10.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMHitos2'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMHitos2' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMHitos2
go

create procedure dbo.sp_GetProyectoIDMHitos2
	@ProjId		int=null,
	@ProjSubId	int=null,
	@ProjSubSId	int=null
as
	select
		PRJ.ProjId,
		PRJ.ProjSubId,
		PRJ.ProjSubSId,
		PRJ.ProjNom,
		PRJ.ProjFchAlta,
		PRJ.ProjCatId,
		CAT.ProjCatNom,
		PRJ.ProjClaseId,
		CLS.ProjClaseNom,
		'nivel' = 
			case
				when PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '1'
				when not PRJ.ProjSubId = 0 and PRJ.ProjSubSId = 0 then '2'
				when not PRJ.ProjSubId = 0 and not PRJ.ProjSubSId = 0 then '3'
			end,
		PRJ.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..EstadosIDM x where x.cod_estado = PRJ.cod_estado),
		/*
		nom_estado = case
			when PRJ.cod_estado is null then case
				when PRJ.cod_estado2 = 'PLANIF' then 'A Planificar'
				when PRJ.cod_estado2 = 'DESEST' then 'Desestimado'
				when PRJ.cod_estado2 = 'EJECUC' then 'En Ejecución'
				when PRJ.cod_estado2 = 'PRUEBA' then 'En Prueba'
				when PRJ.cod_estado2 = 'TERMIN' then 'Finalizado'
				when PRJ.cod_estado2 = 'SUSPEN' then 'Suspendido'
				end
			else (select x.nom_estado from GesPet..Estados x where x.cod_estado = PRJ.cod_estado)
		end,
		*/
		PRJ.fch_estado,
		PRJ.usr_estado,
		nom_usuario = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = PRJ.usr_estado)
		,PRJ.marca
		,PRJ.plan_tyo
		,plan_TYO_nom = case
			when PRJ.plan_tyo = 'S' then 'Si'
			when PRJ.plan_tyo = 'N' then 'No'
			else ''
		end
		,PRJ.empresa
		,nom_empresa = case
			when PRJ.empresa = 'B' then 'Banco'
			when PRJ.empresa = 'C' then 'Consolidar'
			when PRJ.empresa = 'D' then 'RCF'
			when PRJ.empresa = 'E' then 'PSA'
			when PRJ.empresa = 'F' then 'RCF/PSA'
			else ''
		end
		,PRJ.area
		,area_nom = case
			when PRJ.area = 'C' then 'Corporativo'
			when PRJ.area = 'R' then 'Regional'
			when PRJ.area = 'L' then 'Local'
			else ''
		end
		,PRJ.clas3
		,clas3_nom = case
			when PRJ.clas3 = '1' then '2013'
			when PRJ.clas3 = '2' then '2014'
			when PRJ.clas3 = '3' then '2015'
			when PRJ.clas3 = '4' then '2013 a 2014'
			when PRJ.clas3 = '5' then '2013 a 2015'
			when PRJ.clas3 = '6' then '2014 a 2015'
			else ''
		end
		,PRJ.semaphore
		,PRJ.cod_direccion
		,PRJ.cod_gerencia
		,PRJ.cod_sector
		,nom_areasoli = (case
			when PRJ.cod_gerencia is null then 
				RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion))
			when PRJ.cod_sector is null then
				RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) + ' » ' + 
				RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia))
			else
				RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion)) + ' » ' + 
				RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_direccion = PRJ.cod_direccion and g.cod_gerencia = PRJ.cod_gerencia)) + ' » ' + 
				RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_gerencia = PRJ.cod_gerencia and s.cod_sector = PRJ.cod_sector))
		end)
		,PRJ.cod_direccion_p
		,PRJ.cod_gerencia_p
		,PRJ.cod_sector_p
		,PRJ.cod_grupo_p
		,nom_areaprop = (case
			when PRJ.cod_gerencia_p is null then 
				RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p))
			when PRJ.cod_sector_p is null then
				RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
				' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p))
			when PRJ.cod_grupo_p is null then
				RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
				' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
				' » ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p))
			else
				RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = PRJ.cod_direccion_p)) + 
				' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = PRJ.cod_gerencia_p)) + 
				' » ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = PRJ.cod_sector_p)) +
				' » ' + RTRIM((select gr.nom_grupo from GesPet..Grupo gr where gr.cod_grupo = PRJ.cod_grupo_p))
		end)
		,PRJ.avance
		,PRJ.cod_estado2
		,PRJ.fe_modif
		,PRJ.fe_inicio
		,PRJ.fe_fin
		,PRJ.fe_finreprog
		,PRJ.semaphore2
		,PRJ.sema_fe_modif
		,PRJ.sema_usuario
		,PRJ.tipo_proyecto
		,PRJ.totalhs_gerencia
		,PRJ.totalhs_sector
		,PRJ.totalreplan
		,orden = (PRJ.ProjId * 100000 + PRJ.ProjSubId * 100 + PRJ.ProjSubSId),
		a.hito_nombre,
		a.hito_descripcion,
		a.hito_fe_ini,
		a.hito_fe_fin,
		a.hito_estado,
		nom_estado_hito = isnull((select x.descripcion from GesPet..ProyectoIDMHitosEstados x where x.hito_estado = a.hito_estado),''),
		a.cod_nivel,
		a.cod_area,
		nom_area = (case 
			when a.cod_nivel = 'DIRE' then (select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = a.cod_area)
			when a.cod_nivel = 'GERE' then (select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = a.cod_area)
			when a.cod_nivel = 'SECT' then (select x.nom_sector from GesPet..Sector x where x.cod_sector = a.cod_area)
			when a.cod_nivel = 'GRUP' then (select x.nom_grupo from GesPet..Grupo x where x.cod_grupo = a.cod_area)
		end),
		dep_area = (case 
			when a.cod_nivel = 'GERE' then (
				select RTRIM(d.cod_direccion) + '/' + RTRIM(a.cod_area) 
				from GesPet..Gerencia d 
				where d.cod_gerencia = a.cod_area)
			when a.cod_nivel = 'SECT' then (
				select RTRIM(g.cod_direccion) + '/' + RTRIM(s.cod_gerencia) + '/' + RTRIM(a.cod_area)
				from GesPet..Sector s inner join
					 GesPet..Gerencia g on (s.cod_gerencia = g.cod_gerencia)
				where s.cod_sector = a.cod_area)
			when a.cod_nivel = 'GRUP' then (
				select RTRIM(ge.cod_direccion) + '/' + RTRIM(s.cod_gerencia) + '/' + RTRIM(g.cod_sector) + '/' + RTRIM(a.cod_area)
				from GesPet..Grupo g inner join
					 GesPet..Sector s on (g.cod_sector = s.cod_sector) inner join
					 GesPet..Gerencia ge on (s.cod_gerencia = ge.cod_gerencia)
				where g.cod_grupo = a.cod_area)
			else ''
		end),
		a.hito_fecha,
		a.hito_usuario,
		a.hst_nrointerno
	from
		GesPet..ProyectoIDM PRJ left join 
		GesPet..ProyectoCategoria CAT on (PRJ.ProjCatId = CAT.ProjCatId) left join
		GesPet..ProyectoClase CLS on (PRJ.ProjCatId = CLS.ProjCatId and PRJ.ProjClaseId = CLS.ProjClaseId) left join
		GesPet..ProyectoIDMHitos a on (PRJ.ProjId = a.ProjId and PRJ.ProjSubId = a.ProjSubId and PRJ.ProjSubSId = a.ProjSubSId)
	where
		(@ProjId is null or PRJ.ProjId = @ProjId) and
		(@ProjSubId is null or PRJ.ProjSubId = @ProjSubId) and
		(@ProjSubSId is null or PRJ.ProjSubSId = @ProjSubSId)
	order by
		PRJ.ProjId,
		PRJ.ProjSubId,
		PRJ.ProjSubSId
	return(0)
go

grant execute on dbo.sp_GetProyectoIDMHitos2 to GesPetUsr
go

print 'Actualización realizada.'
go