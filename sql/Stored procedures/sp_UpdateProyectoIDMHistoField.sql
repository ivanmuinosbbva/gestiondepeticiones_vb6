/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.11.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoIDMHistoField'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoIDMHistoField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoIDMHistoField
go

create procedure dbo.sp_UpdateProyectoIDMHistoField
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int,
	@hst_nrointerno	int,
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as

if UPPER(RTRIM(@campo))='PROJID'
	update 
		GesPet.dbo.ProyectoIDMHisto
	set
		ProjId = @valornum
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		hst_nrointerno = @hst_nrointerno
if UPPER(RTRIM(@campo))='PROJSUBID'
	update 
		GesPet.dbo.ProyectoIDMHisto
	set
		ProjSubId = @valornum
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		hst_nrointerno = @hst_nrointerno
if UPPER(RTRIM(@campo))='PROJSUBSID'
	update 
		GesPet.dbo.ProyectoIDMHisto
	set
		ProjSubSId = @valornum
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		hst_nrointerno = @hst_nrointerno
if UPPER(RTRIM(@campo))='HST_NROINTERNO'
	update 
		GesPet.dbo.ProyectoIDMHisto
	set
		hst_nrointerno = @valornum
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		hst_nrointerno = @hst_nrointerno
if UPPER(RTRIM(@campo))='HST_FECHA'
	update 
		GesPet.dbo.ProyectoIDMHisto
	set
		hst_fecha = @valordate
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		hst_nrointerno = @hst_nrointerno
if UPPER(RTRIM(@campo))='COD_EVENTO'
	update 
		GesPet.dbo.ProyectoIDMHisto
	set
		cod_evento = @valortxt
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		hst_nrointerno = @hst_nrointerno
if UPPER(RTRIM(@campo))='PROJ_ESTADO'
	update 
		GesPet.dbo.ProyectoIDMHisto
	set
		proj_estado = @valortxt
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		hst_nrointerno = @hst_nrointerno
if UPPER(RTRIM(@campo))='REPLC_USER'
	update 
		GesPet.dbo.ProyectoIDMHisto
	set
		replc_user = @valortxt
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		hst_nrointerno = @hst_nrointerno
if UPPER(RTRIM(@campo))='AUDIT_USER'
	update 
		GesPet.dbo.ProyectoIDMHisto
	set
		audit_user = @valortxt
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		hst_nrointerno = @hst_nrointerno
go

grant execute on dbo.sp_UpdateProyectoIDMHistoField to GesPetUsr
go

print 'Actualización realizada.'
go
