/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.07.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateCatalogoTabla'
go

if exists (select * from sysobjects where name = 'sp_UpdateCatalogoTabla' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateCatalogoTabla
go

create procedure dbo.sp_UpdateCatalogoTabla
	@id				int=null,
	@nombreTabla	char(40)=null,
	@baseDeDatos	char(40)=null,
	@esquema		char(40)=null,
	@DBMS			int=null,
	@servidorNombre	char(40)=null,
	@servidorPuerto	int=null,
	@app_id			char(30)=null,
	@datosSensibles	char(1)=null
as
	update 
		GesPet.dbo.CatalogoTabla
	set
		nombreTabla		= @nombreTabla,
		baseDeDatos		= @baseDeDatos,
		esquema			= @esquema,
		DBMS			= @DBMS,
		servidorNombre	= @servidorNombre,
		servidorPuerto	= @servidorPuerto,
		app_id			= @app_id,
		datosSensibles	= @datosSensibles
	where 
		id = @id
go

grant execute on dbo.sp_UpdateCatalogoTabla to GesPetUsr
go

print 'Actualización realizada.'
go
