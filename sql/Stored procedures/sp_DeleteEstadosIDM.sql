/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 05.12.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteEstadosIDM'
go

if exists (select * from sysobjects where name = 'sp_DeleteEstadosIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteEstadosIDM
go

create procedure dbo.sp_DeleteEstadosIDM
	@cod_estado	char(6)=''
as
	delete from
		GesPet.dbo.EstadosIDM
	where
		cod_estado = @cod_estado
go

grant execute on dbo.sp_DeleteEstadosIDM to GesPetUsr
go

print 'Actualización realizada.'
go
