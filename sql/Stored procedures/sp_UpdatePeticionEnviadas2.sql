/*
-000- a. FJS 05.05.2009 - Nuevo SP para actualizar peticiones en estado "En ejecución"
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionEnviadas2'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionEnviadas2' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionEnviadas2
go

create procedure dbo.sp_UpdatePeticionEnviadas2
	@pet_nrointerno	int=null,
	@pet_sector	char(8)=null,
	@pet_grupo	char(8)=null,
	@pet_record	char(80)=null,
	@pet_usrid	char(10)=null,
	@pet_date	smalldatetime=null,
	@pet_done	char(1)=null
as
	update 
		GesPet.dbo.PeticionEnviadas2
	set
		pet_record = @pet_record,
		pet_usrid = @pet_usrid,
		pet_date = @pet_date,
		pet_done = @pet_done
	where 
		(pet_nrointerno = @pet_nrointerno or @pet_nrointerno = null) and
		(pet_sector = @pet_sector or @pet_sector = null) and
		(pet_grupo = @pet_grupo or @pet_grupo = null)
go

grant execute on dbo.sp_UpdatePeticionEnviadas2 to GesPetUsr
go

print 'Actualización realizada.'
