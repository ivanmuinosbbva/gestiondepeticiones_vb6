/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetInfoprod'
go

if exists (select * from sysobjects where name = 'sp_GetInfoprod' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetInfoprod
go

create procedure dbo.sp_GetInfoprod
	@infoprot_id		int=null,
	@infoprot_item		int=null
as
	select 
		p.infoprot_id,
		p.infoprot_item,
		p.infoprot_req,
		req_desc = (case
			when p.infoprot_req = 'S' then 'Obligatorio'
			when p.infoprot_req = 'R' then 'Recomendado'
			when p.infoprot_req = 'N' then 'Opcional'
		end),
		p.infoprot_itemest,
		p.infoprot_gru,
		p.infoprot_orden
	from 
		GesPet.dbo.Infoprod p
	where
		(p.infoprot_id = @infoprot_id or @infoprot_id is null) and
		(p.infoprot_item = @infoprot_item or @infoprot_item is null)
go

grant execute on dbo.sp_GetInfoprod to GesPetUsr
go

print 'Actualización realizada.'
go

