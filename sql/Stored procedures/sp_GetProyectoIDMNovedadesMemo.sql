/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 11.02.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetProyectoIDMNovedadesMemo'
go

if exists (select * from sysobjects where name = 'sp_GetProyectoIDMNovedadesMemo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetProyectoIDMNovedadesMemo
go

create procedure dbo.sp_GetProyectoIDMNovedadesMemo
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int,
	@mem_fecha		datetime,
	@mem_campo		char(10)=null
as
	select 
		m.ProjId,
		m.ProjSubId,
		m.ProjSubSId,
		m.mem_fecha,
		m.mem_campo,
		m.mem_secuencia,
		m.mem_texto
	from 
		GesPet.dbo.ProyectoIDMNovedades m
	where
		m.ProjId = @ProjId and
		m.ProjSubId = @ProjSubId and
		m.ProjSubSId = @ProjSubSId and
		(@mem_fecha is null or m.mem_fecha = @mem_fecha) and
		(@mem_campo is null or m.mem_campo = @mem_campo)
	order by
		m.ProjId,
		m.ProjSubId,
		m.ProjSubSId,
		m.mem_fecha,
		m.mem_campo,
		m.mem_secuencia
	return(0)
go

grant execute on dbo.sp_GetProyectoIDMNovedadesMemo to GesPetUsr
go

print 'Actualización realizada.'
go