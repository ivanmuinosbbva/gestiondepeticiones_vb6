/*
-000- a. FJS 17.02.2016 - No hay que filtrar por empresa, puesto que la carga est� restringida a los KPI de la empresa.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionKPI'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionKPI' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionKPI
go

create procedure dbo.sp_GetPeticionKPI
	@pet_nrointerno		int,
	@empresa			int
as
	select 
		p.pet_nrointerno,
		p.pet_kpiid,
		p.pet_kpidesc,
		i.kpinom,
		i.kpireq,
		i.kpihab,
		i.kpiayuda,
		i.unimedId,
		i.kpiemp,
		p.kpi_unimedId,
		unimedDesc = (select um.unimedDesc from UnidadMedida um where um.unimedId = p.kpi_unimedId),
		p.valorEsperado,
		p.valorPartida,
		p.tiempoEsperado,
		p.unimedTiempo,
		unimedTiempoDesc = case
			when p.unimedTiempo = 'MES' and p.tiempoEsperado > 1 then 'Meses'
			when p.unimedTiempo = 'MES' and p.tiempoEsperado = 1 then 'Mes'
			when p.unimedTiempo = 'ANO' and p.tiempoEsperado > 1 then 'A�os'
			when p.unimedTiempo = 'ANO' and p.tiempoEsperado = 1 then 'A�o'
		end,
		p.audit_fecha,
		p.audit_usuario
	from 
		IndicadorKPI i left join
		PeticionKPI p on (i.kpiid = p.pet_kpiid)
	where
		p.pet_nrointerno = @pet_nrointerno
	return(0)
go

grant execute on dbo.sp_GetPeticionKPI to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
