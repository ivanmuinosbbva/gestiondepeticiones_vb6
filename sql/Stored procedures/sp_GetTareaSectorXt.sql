use GesPet
go
print 'sp_GetTareaSectorXt'
go
if exists (select * from sysobjects where name = 'sp_GetTareaSectorXt' and sysstat & 7 = 4)
drop procedure dbo.sp_GetTareaSectorXt
go
create procedure dbo.sp_GetTareaSectorXt
	 @cod_tarea     char(8)=null,
	 @cod_sector	char(8)=null
as
begin
select
	Tg.cod_sector,
	nom_sector = (select Se.nom_sector from Sector Se where RTRIM(Se.cod_sector) = RTRIM(Tg.cod_sector)),	
	Tg.cod_tarea,
	Ta.nom_tarea,
	Ta.flg_habil
from
	GesPet..TareaSector Tg, GesPet..Sector Gr, GesPet..Tarea Ta
where
	(@cod_sector is null or RTRIM(@cod_sector) is null or RTRIM(@cod_sector)='' or RTRIM(Tg.cod_sector) = RTRIM(@cod_sector)) and
	(@cod_tarea is null or RTRIM(@cod_tarea) is null or RTRIM(@cod_tarea)='' or RTRIM(Tg.cod_tarea) = RTRIM(@cod_tarea)) and
	(RTRIM(Tg.cod_sector) *= RTRIM(Gr.cod_sector)) and
	(RTRIM(Tg.cod_tarea) *= RTRIM(Ta.cod_tarea))
order by Tg.cod_sector,Tg.cod_tarea
end
return(0)
go



grant execute on dbo.sp_GetTareaSectorXt to GesPetUsr
go


