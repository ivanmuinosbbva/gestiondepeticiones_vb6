use GesPet
go
print 'sp_UpdatePeticionMemo'
go
if exists (select * from sysobjects where name = 'sp_UpdatePeticionMemo' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdatePeticionMemo
go
create procedure dbo.sp_UpdatePeticionMemo   
    @borra_last       char(1)='N',   
    @pet_nrointerno int,   
    @mem_campo      char(10),   
    @mem_secuencia  smallint,   
    @mem_texto      char(255)   
as   
   
declare @textoaux varchar (255)   
declare @textomemo varchar (255)   
select @textomemo = ''   
   
if exists (select pet_nrointerno from GesPet..PeticionMemo   
        where pet_nrointerno = @pet_nrointerno and   
            RTRIM(mem_campo) = RTRIM(@mem_campo) and   
            mem_secuencia = @mem_secuencia)   
    begin   
        update GesPet..PeticionMemo   
        set mem_texto = @mem_texto   
        where pet_nrointerno = @pet_nrointerno and   
            RTRIM(mem_campo) = RTRIM(@mem_campo) and   
            mem_secuencia = @mem_secuencia   
    end   
    else   
    begin   
       insert into GesPet..PeticionMemo   
           (pet_nrointerno,   
            mem_campo,   
            mem_secuencia,   
            mem_texto)   
       values   
           (@pet_nrointerno,   
            @mem_campo,   
            @mem_secuencia,   
            @mem_texto)   
    end   
   
if @borra_last = 'S'   
    begin   
        delete from GesPet..PeticionMemo   
        where pet_nrointerno = @pet_nrointerno and   
            RTRIM(mem_campo) = RTRIM(@mem_campo) and   
            mem_secuencia > @mem_secuencia   
    end   
   
return(0)   
go

grant execute on dbo.sp_UpdatePeticionMemo to GesPetUsr 
go

grant execute on dbo.sp_UpdatePeticionMemo to RolGesIncConex
go

sp_procxmode 'sp_UpdatePeticionMemo', anymode
go

print 'Actualización realizada.'
go