/*
-001- a. FJS 13.01.2009 - Optimización de SP para mejorar la performance del aplicativo.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionAnexadas'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionAnexadas' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionAnexadas
go

create procedure dbo.sp_GetPeticionAnexadas
	@pet_nrointerno		int
as
	select 
		PET.pet_nrointerno,
		PET.pet_nroasignado,
		PET.titulo,
		PET.cod_tipo_peticion,
		PET.prioridad,
		PET.pet_nroanexada,
		PET.fe_pedido,
		PET.fe_requerida,
		PET.fe_comite,
		PET.fe_ini_plan,
		PET.fe_fin_plan,
		PET.fe_ini_real,
		PET.fe_fin_real,
		PET.horaspresup,
		PET.cod_direccion,
		PET.cod_gerencia,
		PET.cod_sector,
		PET.cod_usualta,
		PET.cod_solicitante,
		PET.cod_referente,
		PET.cod_supervisor,
		PET.cod_director,
		PET.cod_estado,
		nom_estado = (select EST.nom_estado from Estados EST where RTRIM(EST.cod_estado) = RTRIM(PET.cod_estado)),
		PET.fe_estado,
		PET.cod_situacion,
		nom_situacion = (select SIT.nom_situacion from Situaciones SIT where RTRIM(SIT.cod_situacion) = RTRIM(PET.cod_situacion))
	from 
		Peticion PET
	where 
		PET.pet_nroanexada = @pet_nrointerno
	order by 
		PET.pet_nroasignado
		--PET.pet_nrointerno	-- del -001- a.

return(0)
go

grant execute on dbo.sp_GetPeticionAnexadas to GesPetUsr
go

print 'Actualización realizada.'
