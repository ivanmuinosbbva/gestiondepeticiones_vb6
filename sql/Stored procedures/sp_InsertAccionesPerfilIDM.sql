/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 05.12.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertAccionesPerfilIDM'
go

if exists (select * from sysobjects where name = 'sp_InsertAccionesPerfilIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertAccionesPerfilIDM
go

create procedure dbo.sp_InsertAccionesPerfilIDM
	@cod_accion	char(8),
	@cod_perfil	char(4),
	@cod_estado	char(6),
	@cod_estnew	char(6),
	@flg_hereda	char(1)
as
	insert into GesPet.dbo.AccionesPerfilIDM (
		cod_accion,
		cod_perfil,
		cod_estado,
		cod_estnew,
		flg_hereda)
	values (
		@cod_accion,
		@cod_perfil,
		@cod_estado,
		@cod_estnew,
		@flg_hereda)
go

grant execute on dbo.sp_InsertAccionesPerfilIDM to GesPetUsr
go

print 'Actualización realizada.'
go
