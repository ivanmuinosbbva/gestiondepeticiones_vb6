use GesPet
go
print 'sp_GetAgrupPadre'
go
if exists (select * from sysobjects where name = 'sp_GetAgrupPadre' and sysstat & 7 = 4)
drop procedure dbo.sp_GetAgrupPadre
go
create procedure dbo.sp_GetAgrupPadre 
	@agr_nrointerno	int
as 

declare @agr_nrointernoX int
declare @agr_nrointernoY int
declare @agr_nropadreX int

select @agr_nrointernoY=@agr_nrointerno
select @agr_nropadreX=agr_nropadre,@agr_nrointernoX=agr_nrointerno from Agrup where @agr_nrointernoY=agr_nrointerno
while ((@agr_nropadreX is not null) and (@agr_nropadreX > 0))
begin
	select @agr_nrointernoY=@agr_nropadreX
	select @agr_nropadreX=agr_nropadre,@agr_nrointernoX=agr_nrointerno from Agrup where @agr_nrointernoY=agr_nrointerno
end

    
    select  
	A.agr_nrointerno,
	A.agr_nropadre,
	A.agr_titulo,
	A.agr_vigente,
	A.cod_direccion,
	nom_direccion = (select D.nom_direccion from Direccion D where D.cod_direccion=A.cod_direccion),
	A.cod_gerencia,
	nom_gerencia = (select G.nom_gerencia from Gerencia G where G.cod_gerencia=A.cod_gerencia),
	A.cod_sector,
	nom_sector = (select S.nom_sector from Sector S where S.cod_sector=A.cod_sector),
	A.cod_grupo,
	nom_grupo = (select U.nom_grupo from Grupo U where U.cod_grupo=A.cod_grupo),
	A.cod_usualta,
	nom_usualta = (select R.nom_recurso from Recurso R where R.cod_recurso=A.cod_usualta),
	A.agr_visibilidad,
	A.agr_actualiza,
	A.audit_user,
	A.audit_date
    from 
        Agrup A
    where   A.agr_nrointerno = @agr_nrointernoY
return(0) 
go



grant execute on dbo.sp_GetAgrupPadre to GesPetUsr 
go
