/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPrioridad'
go

if exists (select * from sysobjects where name = 'sp_InsertPrioridad' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPrioridad
go

create procedure dbo.sp_InsertPrioridad
	@prior_id	char(1),
	@prior_dsc	char(50),
	@prior_ord	int,
	@prior_hab	char(1)='S'
as
	insert into GesPet.dbo.Prioridad (
		prior_id,
		prior_dsc,
		prior_ord,
		prior_hab)
	values (
		@prior_id,
		@prior_dsc,
		@prior_ord,
		@prior_hab)
	return(0)
go

grant execute on dbo.sp_InsertPrioridad to GesPetUsr
go

print 'Actualización realizada.'
go