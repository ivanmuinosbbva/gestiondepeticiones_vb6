use GesPet
go

print 'sp_GetAccionPerfilConfig'
go

if exists (select * from sysobjects where name = 'sp_GetAccionPerfilConfig' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetAccionPerfilConfig
go

create procedure dbo.sp_GetAccionPerfilConfig
	@agrupador		int=1,
	@cod_perfil		char(4)
as
	begin
		select 
			a.agrupador,
			nom_grupo = (select ag.accionGrupoNom from AccionesGrupo ag where ag.accionGrupoId = a.agrupador),
			a.cod_accion,
			a.porEstado,
			a.multiplesEstados,
			a.orden,
			a.leyenda,
			permiso = case 
				when (select count(1) from AccionesPerfil ap where ap.cod_accion = a.cod_accion and ap.cod_perfil = LTRIM(RTRIM(@cod_perfil))) > 0 then 'S'
				else 'N'
			end, 
			cantidad = (select count(1) from AccionesPerfil ap where ap.cod_accion = a.cod_accion and ap.cod_perfil = LTRIM(RTRIM(@cod_perfil)))
		from Acciones a
		where 
			a.tipo_accion = 'ACC' and 
			(@agrupador is null or a.agrupador = @agrupador)
		order by 
			a.agrupador,
			a.orden
	end
	return(0)
go

grant execute on dbo.sp_GetAccionPerfilConfig to GesPetUsr
go




/*
		select
			Ap.cod_accion,
			Ap.cod_perfil,
			nom_perfil = (select x.nom_perfil from GesPet..Perfil x where x.cod_perfil = Ap.cod_perfil),
			Ap.cod_estado,
			cod_estnew=Ew.cod_estado,
			nom_accion = (select Ac.nom_accion from Acciones Ac where RTRIM(Ac.cod_accion) = RTRIM(Ap.cod_accion)),
			Ap.flg_hereda,
			Es.nom_estado,
			nom_estnew=Ew.nom_estado
		from
			GesPet..AccionesPerfil Ap,
			GesPet..Estados Es,
			GesPet..Estados Ew
		where
			(@cod_accion is null or RTRIM(@cod_accion) is null or RTRIM(@cod_accion)='' or RTRIM(Ap.cod_accion) = RTRIM(@cod_accion) or RTRIM(Ap.cod_accion)='ALL') and
			(@cod_estado is null or RTRIM(@cod_estado) is null or RTRIM(@cod_estado)='' or RTRIM(Ap.cod_estado) = RTRIM(@cod_estado)) and
			(@cod_perfil is null or RTRIM(@cod_perfil) is null or RTRIM(@cod_perfil)='' or RTRIM(Ap.cod_perfil) = RTRIM(@cod_perfil)) and
			(RTRIM(Ap.cod_estado)*=RTRIM(Es.cod_estado)) and
			(RTRIM(Ap.cod_estnew)*=RTRIM(Ew.cod_estado))
		order by 
			Ap.cod_accion,
			Ap.cod_perfil,
			Es.nom_estado
*/