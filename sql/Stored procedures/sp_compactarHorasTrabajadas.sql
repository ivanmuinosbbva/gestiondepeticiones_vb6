/*
-000- a. FJS 30.09.2014 - Nuevo SP para compactar las horas cargadas de un período.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_compactarHorasTrabajadas'
go

if exists (select * from sysobjects where name = 'sp_compactarHorasTrabajadas' and sysstat & 7 = 4)
	drop procedure dbo.sp_compactarHorasTrabajadas
go

create procedure dbo.sp_compactarHorasTrabajadas
	@fecha_desde		smalldatetime,
	@fecha_hasta		smalldatetime
as 
	create table #HorasTrabajadasTemp (
		cod_recurso		char(10)		NULL,
		cod_tarea		char(8)			NULL,
		pet_nrointerno	int				NULL,
		fe_desde		smalldatetime	NULL,
		fe_hasta		smalldatetime	NULL,
		horas			int				NULL,
		trabsinasignar	char(1)			NULL,
		observaciones	varchar(255)	NULL,
		audit_user		char(10)		NULL,
		audit_date		smalldatetime	NULL)

	insert into #HorasTrabajadasTemp
		select 
			cod_recurso,
			cod_tarea,
			pet_nrointerno,
			min(fe_desde),
			max(fe_hasta),
			sum(horas),
			trabsinasignar,
			'',
			'BATCH',
			getdate()
		 from HorasTrabajadas
		 where 
			fe_desde >= @fecha_desde and
			fe_hasta <= @fecha_hasta
		 group by cod_recurso
				, cod_tarea
				, pet_nrointerno
				, month(fe_desde) & year(fe_desde)
				, month(fe_hasta) & year(fe_hasta)
				, trabsinasignar
	
	delete from HorasTrabajadas
	where 
		fe_desde >= @fecha_desde and
		fe_hasta <= @fecha_hasta
	
	INSERT INTO HorasTrabajadas
		SELECT
			cod_recurso,
			cod_tarea,
			pet_nrointerno,
			fe_desde,
			fe_hasta,
			horas,
			trabsinasignar,
			observaciones,
			audit_user,
			audit_date
		FROM #HorasTrabajadasTemp

	drop table #HorasTrabajadasTemp

	return(0)
go

grant execute on dbo.sp_compactarHorasTrabajadas to GesPetUsr
go

print 'Actualización realizada.'
go



/*
create procedure dbo.sp_compactarHorasTrabajadas
	@fecha		smalldatetime
as 
	declare @fecha_filtro	smalldatetime

	--select @fecha = convert(smalldatetime, '20140930')

	if month(@fecha) > 9
		--select convert(char(4),year(@fecha)) + convert(char(2),month(@fecha)) + '01'
		select @fecha = convert(char(4),year(@fecha)) + convert(char(2),month(@fecha)) + '01'
	else
		--select convert(char(4),year(@fecha)) + '0' + LTRIM(RTRIM(convert(char(2),month(@fecha)))) + '01'
		select @fecha = convert(char(4),year(@fecha)) + '0' + LTRIM(RTRIM(convert(char(2),month(@fecha)))) + '01'

	select @fecha_filtro = dateadd(mm, -12, @fecha)
	select convert(char(10),@fecha_filtro,112)

	create table #HorasTrabajadasTemp (
		cod_recurso		char(10)		NULL,
		cod_tarea		char(8)			NULL,
		pet_nrointerno	int				NULL,
		fe_desde		smalldatetime	NULL,
		fe_hasta		smalldatetime	NULL,
		horas			int				NULL,
		trabsinasignar	char(1)			NULL,
		observaciones	varchar(255)	NULL,
		audit_user		char(10)		NULL,
		audit_date		smalldatetime	NULL)

	insert into #HorasTrabajadasTemp
		select 
			cod_recurso,
			cod_tarea,
			pet_nrointerno,
			min(fe_desde),
			max(fe_hasta),
			sum(horas),
			trabsinasignar,
			'',
			'BATCH',
			getdate()
		 from HorasTrabajadas
		 where fe_hasta < @fecha_filtro
		 group by cod_recurso
				, cod_tarea
				, pet_nrointerno
				, month(fe_desde) & year(fe_desde)
				, month(fe_hasta) & year(fe_hasta)
				, trabsinasignar
		union 
		select 
			cod_recurso,
			cod_tarea,
			pet_nrointerno,
			fe_desde,
			fe_hasta,
			horas,
			trabsinasignar,
			observaciones,
			audit_user,
			audit_date
		from HorasTrabajadas
		where fe_hasta >= @fecha_filtro
	
	truncate table HorasTrabajadas

	INSERT INTO HorasTrabajadas
		SELECT
			cod_recurso,
			cod_tarea,
			pet_nrointerno,
			fe_desde,
			fe_hasta,
			horas,
			trabsinasignar,
			observaciones,
			audit_user,
			audit_date
		FROM #HorasTrabajadasTemp

	drop table #HorasTrabajadasTemp

	return(0)
go
*/