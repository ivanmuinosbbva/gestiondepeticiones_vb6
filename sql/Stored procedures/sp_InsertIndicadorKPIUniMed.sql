/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertIndicadorKPIUniMed'
go

if exists (select * from sysobjects where name = 'sp_InsertIndicadorKPIUniMed' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertIndicadorKPIUniMed
go

create procedure dbo.sp_InsertIndicadorKPIUniMed
	@kpiid		int,
	@unimedId	char(10)
as
	insert into GesPet.dbo.IndicadorKPIUniMed (
		kpiid,
		unimedId)
	values (
		@kpiid,
		@unimedId)
go

grant execute on dbo.sp_InsertIndicadorKPIUniMed to GesPetUsr
go

print 'Actualización realizada.'
go
