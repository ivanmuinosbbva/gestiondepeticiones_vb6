/*
-000- a. FJS 16.04.2015 - Nuevo: elimina el encabezado y detalles de un informe de homologación.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionInforme'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionInforme' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionInforme
go

create procedure dbo.sp_DeletePeticionInforme
	@pet_nrointerno		int,
	@info_id			int=null,
	@info_idver			int=null
as
	-- Responsables
	DELETE 
	FROM PeticionInfohr
	WHERE 
		pet_nrointerno = @pet_nrointerno and 
		(info_id = @info_id or @info_id is null) and
		(info_idver = @info_idver or @info_idver is null)

	-- Observaciones
	DELETE 
	FROM PeticionInfoht
	WHERE 
		pet_nrointerno = @pet_nrointerno and 
		(info_id = @info_id or @info_id is null) and
		(info_idver = @info_idver or @info_idver is null)
	
	-- Detalle
	DELETE 
	FROM PeticionInfohd
	WHERE 
		pet_nrointerno = @pet_nrointerno and 
		(info_id = @info_id or @info_id is null) and
		(info_idver = @info_idver or @info_idver is null)

	-- Cabecera
	DELETE 
	FROM PeticionInfohc
	WHERE 
		pet_nrointerno = @pet_nrointerno and 
		(info_id = @info_id or @info_id is null) and
		(info_idver = @info_idver or @info_idver is null)
	return(0)
go

grant execute on dbo.sp_DeletePeticionInforme to GesPetUsr
go

print 'Actualización realizada.'
go
