/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 15.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeriodoEstado'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeriodoEstado' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeriodoEstado
go

create procedure dbo.sp_UpdatePeriodoEstado
	@cod_estado_per	char(8),
	@nom_estado_per	char(100)
as
	update 
		GesPet.dbo.PeriodoEstado
	set
		nom_estado_per = @nom_estado_per
	where 
		cod_estado_per = @cod_estado_per
	return(0)
go

grant execute on dbo.sp_UpdatePeriodoEstado to GesPetUsr
go

print 'Actualización realizada.'
