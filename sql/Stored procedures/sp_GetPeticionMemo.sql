/*
-001- a. FJS 13.01.2009 - Optimización de SP para mejorar la performance del aplicativo.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionMemo'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionMemo' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionMemo
go

create procedure dbo.sp_GetPeticionMemo 
    @pet_nrointerno		int,
	@mem_campo			char(10)
as   
    select 
		PM.mem_texto   
    from 
		GesPet..PeticionMemo PM
    where 
		PM.pet_nrointerno = @pet_nrointerno and
		RTRIM(PM.mem_campo) = RTRIM(@mem_campo) 
    order by
		--{ add -001- a.
		PM.pet_nrointerno, 
		PM.mem_campo,
		--}
		PM.mem_secuencia

return(0)
go

grant execute on dbo.sp_GetPeticionMemo to GesPetUsr 
go

print 'Actualización realizada.'