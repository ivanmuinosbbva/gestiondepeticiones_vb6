/*
-000- a. XXX dd.mm.aaaa - Nuevo SP para...
*/

use GesPet
go

print 'Creando/actualizando SP: sp_IGM_ParticipantesProyecto'
go

if exists (select * from sysobjects where name = 'sp_IGM_ParticipantesProyecto' and sysstat & 7 = 4)
	drop procedure dbo.sp_IGM_ParticipantesProyecto
go

create procedure dbo.sp_IGM_ParticipantesProyecto
	@projid			int=null,
	@projsubid		int=null,
	@projsubsid		int=null
as 
	declare @fecha_actual_desde smalldatetime
	declare @fecha_actual_hasta smalldatetime
	declare @cod_direccion		char(8)
	declare @cod_direccion_eje  char(8)

	select @fecha_actual_desde	= convert(smalldatetime,convert(char(4),year(getdate())) + '0101 00:00')
	select @fecha_actual_hasta	= convert(smalldatetime,convert(char(4),year(getdate())) + '1231 23:59')
	select @cod_direccion_eje	= 'MEDIO'

	select
		T3.DIRECCION		as cod_direccion,
		nom_direccion = (select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = T3.DIRECCION),
		T3.GERENCIA			as cod_gerencia,
		nom_gerencia = (select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = T3.GERENCIA),
		T3.PROJID			as pet_projid, 
		T3.PROJSUBID		as pet_projsubid,
		T3.PROJSUBSID		as pet_projsubsid,
		fe_plan_desde		= min(T3.F_PL_DESDE),
		fe_plan_hasta		= max(T3.F_PL_HASTA),
		fe_real_desde		= min(T3.F_RE_DESDE),
		fe_real_hasta		= max(T3.F_RE_HASTA),
		horas_tyo_periodo	= convert(numeric(10,4), (SUM(T3.H_ACT)/ convert(numeric(10,4),60))),
		horas_tyo_total		= convert(numeric(10,4), (SUM(T3.H_TOT)/ convert(numeric(10,4),60)))
	from
		 (select
			 DIRECCION	= (select pp.cod_direccion from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
			 GERENCIA	= (select pp.cod_gerencia from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
			 PROJID		= (select isnull(pp.pet_projid,0) from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
			 PROJSUBID  = (select isnull(pp.pet_projsubid,0) from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
			 PROJSUBSID = (select isnull(pp.pet_projsubsid,0) from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
			 F_PL_DESDE = (select pp.fe_ini_plan from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
			 F_PL_HASTA = (select pp.fe_fin_plan from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
			 F_RE_DESDE = (select pp.fe_ini_real from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
			 F_RE_HASTA = (select pp.fe_fin_real from GesPet..Peticion pp where pp.pet_nrointerno = T2.N_PET),
			 H_ACT,
			 H_TOT
		 from
			(select 
				COD_REC,
				N_PET,
				H_ACT = sum(HOR_ACT),
				H_TOT = sum(HOR_TOT)
			 from
				   (select 
						 COD_REC = h.cod_recurso,
						 N_PET   = h.pet_nrointerno,
						 HOR_ACT = (case when (h.fe_desde >= @fecha_actual_desde) and 
											  (h.fe_hasta <= @fecha_actual_hasta) then h.horas
									else 0 
									end),
						 HOR_TOT = (case when h.fe_hasta <= @fecha_actual_hasta then h.horas
									else 0 
									end)
					from 
						 GesPet..HorasTrabajadas h 
					where
						 (h.pet_nrointerno > 0 and h.pet_nrointerno is not null ) ) T1
				   group by T1.COD_REC, T1.N_PET) T2
			 where
				  T2.COD_REC in (select r.cod_recurso from GesPet..Recurso r where r.cod_direccion = @cod_direccion_eje) and
				  T2.N_PET   in (select p.pet_nrointerno
								 from GesPet..Peticion p
 								 where p.cod_gerencia in (select g.cod_gerencia from GesPet..Gerencia g where g.IGM_hab='S') and
									   p.cod_estado   in (select e.cod_estado from GesPet..Estados e where e.IGM_hab = 'S') and
									   (@projid	is null or p.pet_projid = @projid) and 
									   (@projsubid is null or p.pet_projsubid = @projsubid) and 
									   (@projsubsid is null or p.pet_projsubsid = @projsubsid)
								)) T3
		group by 
			T3.DIRECCION, 
			T3.GERENCIA, 
			T3.PROJID, 
			T3.PROJSUBID, 
			T3.PROJSUBSID 
	return(0)
go

grant execute on dbo.sp_IGM_ParticipantesProyecto to GesPetUsr
go

grant execute on dbo.sp_IGM_ParticipantesProyecto to GrpTrnIGM
go

print 'Actualizaci�n realizada.'
go

	/*
	declare @c_cod_direccion	char(8)
	declare @c_cod_gerencia		char(8)
	declare @c_pet_nrointerno	int
	declare @c_ProjId			int
	declare @c_ProjSubId		int
	declare @c_ProjSubSId		int
	declare @p_horas_act		int
	declare @p_horas_tot		int

	-- Horas TYO a�o en curso
	declare @fecha_actual_desde		char(8)
	declare @fecha_actual_hasta		char(8)

	-- Fechas de planificaci�n y reales
	declare @fecha_plan_desde	smalldatetime
	declare @fecha_plan_hasta	smalldatetime
	declare @fecha_real_desde	smalldatetime
	declare @fecha_real_hasta	smalldatetime

	-- Establecer las fechas del a�o en curso
	select @fecha_actual_desde = convert(char(4), datepart(yy,getdate())) + '0101'
	select @fecha_actual_hasta = convert(char(8), getdate(), 112)


	create table #TmpSalida(
		cod_direccion		char(8)			null,
		nom_direccion		char(30)		null,
		cod_gerencia		char(8)			null,
		nom_gerencia		char(30)		null,
		pet_projid			int				null,
		pet_projsubid		int				null,
		pet_projsubsid		int				null,
		fe_plan_desde		smalldatetime	null,
		fe_plan_hasta		smalldatetime	null,
		fe_real_desde		smalldatetime	null,
		fe_real_hasta		smalldatetime	null,
		horas_tyo_actual	numeric(10,4)	null,
		horas_tyo_total		numeric(10,4)	null)
	
	insert into #TmpSalida
		select
			r.cod_direccion,
			nom_direccion = isnull((select x.nom_direccion from GesPet..Direccion x where x.cod_direccion = r.cod_direccion),''),
			r.cod_gerencia,
			nom_gerencia = isnull((select x.nom_gerencia from GesPet..Gerencia x where x.cod_gerencia = r.cod_gerencia),''),
			p.pet_projid,
			p.pet_projsubid,
			p.pet_projsubsid,
			null,
			null,
			null,
			null,
			0,
			0
		from
			GesPet..HorasTrabajadas h inner join
			GesPet..Recurso r on (h.cod_recurso = r.cod_recurso) inner join
			GesPet..Peticion p on (h.pet_nrointerno = p.pet_nrointerno) inner join
			GesPet..Gerencia g on (r.cod_gerencia = g.cod_gerencia)
		where
			(r.cod_direccion = 'MEDIO') and (g.IGM_hab = 'S') and 
			(@projid is null or p.pet_projid = @projid) and 
			(@projsubid is null or  p.pet_projsubid = @projsubid) and 
			(@projsubsid is null or  p.pet_projsubsid = @projsubsid)
		group by
			r.cod_direccion,
			r.cod_gerencia,
			p.pet_projid,
			p.pet_projsubid,
			p.pet_projsubsid

	declare cursTmp Cursor for 
		select 
			cod_direccion,
			cod_gerencia,
			pet_projid,
			pet_projsubid,
			pet_projsubsid
		from #TmpSalida
		group by 
			cod_direccion,
			cod_gerencia,
			pet_projid,
			pet_projsubid,
			pet_projsubsid
		for Read Only
	
	open cursTmp
	fetch cursTmp into @c_cod_direccion, @c_cod_gerencia, @c_ProjId, @c_ProjSubId, @c_ProjSubSId
	if @@sqlstatus = 0
	begin
		while @@sqlstatus = 0
		begin
			-- Horas TYO a�o en curso
			exec sp_HorasTrabajadasXArea @fecha_actual_desde, @fecha_actual_hasta, 'GERE', @c_cod_gerencia, @c_ProjId, @c_ProjSubId, @c_ProjSubSId, null, @p_horas_act output
			-- Horas TYO totales
			exec sp_HorasTrabajadasXArea 'NULL', 'NULL', 'GERE', @c_cod_gerencia, @c_ProjId, @c_ProjSubId, @c_ProjSubSId, null, @p_horas_tot output
		
			select 
				@fecha_plan_desde = min(x.fe_ini_plan),
				@fecha_plan_hasta = max(x.fe_fin_plan),
				@fecha_real_desde = min(x.fe_ini_real),
				@fecha_real_hasta = max(x.fe_fin_real)
			from GesPet..Peticion x 
			where pet_projid = @c_ProjId and pet_projsubid = @c_ProjSubId and pet_projsubsid = @c_ProjSubSId

			update #TmpSalida
			set 
				fe_plan_desde = @fecha_plan_desde,
				fe_plan_hasta = @fecha_plan_hasta,
				fe_real_desde = @fecha_real_desde,
				fe_real_hasta = @fecha_real_hasta,
				horas_tyo_actual = convert(numeric(10,4), (@p_horas_act/ convert(numeric(10,4),60))),
				horas_tyo_total  = convert(numeric(10,4), (@p_horas_tot/ convert(numeric(10,4),60)))
			where 
				pet_projid = @c_ProjId and 
				pet_projsubid = @c_ProjSubId and 
				pet_projsubsid = @c_ProjSubSId and
				RTRIM(cod_direccion) = RTRIM(@c_cod_direccion) and
				RTRIM(cod_gerencia)  = RTRIM(@c_cod_gerencia)
			fetch cursTmp into @c_cod_direccion, @c_cod_gerencia, @c_ProjId, @c_ProjSubId, @c_ProjSubSId
		end
	end
	close cursTmp
	deallocate cursor cursTmp

	select 
		a.pet_projid,
		a.pet_projsubid,
		a.pet_projsubsid,
		a.fe_plan_desde,
		a.fe_plan_hasta,
		a.fe_real_desde,
		a.fe_real_hasta,
		a.cod_direccion,
		a.nom_direccion,
		a.cod_gerencia,
		a.nom_gerencia,
		horas_tyo_actual = isnull(a.horas_tyo_actual,0),
		horas_tyo_total = isnull(a.horas_tyo_total,0)
	from
		#TmpSalida a
	where
		(a.pet_projid <> 0)
	
	drop table #TmpSalida
	*/