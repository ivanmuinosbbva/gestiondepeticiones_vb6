/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.16 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 04.11.2011 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteProyectoClase'
go

if exists (select * from sysobjects where name = 'sp_DeleteProyectoClase' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteProyectoClase
go

create procedure dbo.sp_DeleteProyectoClase
	@ProjCatId		int,
	@ProjClaseId	int
as
	delete from
		GesPet.dbo.ProyectoClase
	where
		ProjCatId = @ProjCatId and
		ProjClaseId = @ProjClaseId
	return(0)
go

grant execute on dbo.sp_DeleteProyectoClase to GesPetUsr
go

print 'Actualización realizada.'
go
