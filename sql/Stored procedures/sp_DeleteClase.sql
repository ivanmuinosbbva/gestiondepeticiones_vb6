/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.10.2014 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteClase'
go

if exists (select * from sysobjects where name = 'sp_DeleteClase' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteClase
go

create procedure dbo.sp_DeleteClase
	@cod_clase	char(4)=''
as
	delete from
		GesPet.dbo.Clase
	where
		cod_clase = @cod_clase
go

grant execute on dbo.sp_DeleteClase to GesPetUsr
go

print 'Actualización realizada.'
go
