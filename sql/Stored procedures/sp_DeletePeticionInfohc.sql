/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionInfohc'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionInfohc' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionInfohc
go

create procedure dbo.sp_DeletePeticionInfohc
	@pet_nrointerno	int=0,
	@info_id		int=0,
	@info_idver		int=null
as
	-- Responsables
	DELETE 
	FROM PeticionInfohr
	WHERE 
		pet_nrointerno = @pet_nrointerno and 
		info_id = @info_id and
		(@info_idver is null or info_idver = @info_idver)

	-- Observaciones
	DELETE 
	FROM PeticionInfoht
	WHERE pet_nrointerno = @pet_nrointerno and 
		info_id = @info_id and
		(@info_idver is null or info_idver = @info_idver)

	-- Detalle
	delete from GesPet..PeticionInfohd
	where
		pet_nrointerno = @pet_nrointerno and
		info_id = @info_id and
		(@info_idver is null or info_idver = @info_idver)
	
	-- Cabecera
	delete from GesPet..PeticionInfohc
	where
		pet_nrointerno = @pet_nrointerno and
		info_id = @info_id and
		(@info_idver is null or info_idver = @info_idver)
	return(0)
go

grant execute on dbo.sp_DeletePeticionInfohc to GesPetUsr
go

print 'Actualización realizada.'
go
