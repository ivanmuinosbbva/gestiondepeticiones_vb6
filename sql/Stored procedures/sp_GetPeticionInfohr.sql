/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionInfohr'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionInfohr' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionInfohr
go

create procedure dbo.sp_GetPeticionInfohr
	@pet_nrointerno	int=null,
	@info_id		int=null,
	@info_idver		int=null
as
	select 
		pr.pet_nrointerno,
		pr.info_id,
		pr.info_idver,
		pr.info_item,
		pr.cod_recurso,
		nom_recurso = (select r.nom_recurso from Recurso r where r.cod_recurso = pr.cod_recurso),
		email = (select r.email from Recurso r where r.cod_recurso = pr.cod_recurso),
		euser = (select r.euser from Recurso r where r.cod_recurso = pr.cod_recurso),
		pr.cod_perfil,
		nom_perfil = (select p.nom_perfil from Perfil p where p.cod_perfil = pr.cod_perfil),
		cod_nivel = isnull(pr.cod_nivel,'-'),
		cod_area = isnull(pr.cod_area,'-'),
		pr.info_valor,
		nom_valor = case 
			when pr.info_valor = 1 then 'SOB'
			when pr.info_valor = 2 then 'OME'
			when pr.info_valor = 3 then 'OMA'
			else '-'
		end,
		pr.info_comen,
		i.infoprot_itemdsc,
		i.infoprot_itemresp,
		pr.fecha_envio,
		pr.fecha_reenvio
	from 
		PeticionInfohr pr inner join 
		PeticionInfohd pd on (pr.pet_nrointerno = pd.pet_nrointerno and pr.info_id = pd.info_id and pr.info_idver = pd.info_idver and pr.info_item = pd.info_item) inner join
		Infoproi i on (pd.info_item = i.infoprot_item)
	where 
		pr.pet_nrointerno = @pet_nrointerno and
		pr.info_id = @info_id and
		(@info_idver is null or pr.info_idver = @info_idver) and 
		pd.info_estado <> 'S'
	order by
		pr.pet_nrointerno,
		pr.info_id,
		pr.info_idver,
		pr.cod_recurso,
		pd.info_orden
	
	return(0)
go

grant execute on dbo.sp_GetPeticionInfohr to GesPetUsr
go

print 'Actualización realizada.'
go
