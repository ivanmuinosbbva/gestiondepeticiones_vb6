use GesPet
go
print 'sp_GetTipoPrj'
go
if exists (select * from sysobjects where name = 'sp_GetTipoPrj' and sysstat & 7 = 4)
drop procedure dbo.sp_GetTipoPrj
go
create procedure dbo.sp_GetTipoPrj
	@cod_tipoprj       char(8)=null,
	@flg_habil	char(1)=null

as
begin
	select
		BSU.cod_tipoprj,
		BSU.nom_tipoprj,
		BSU.secuencia,
		BSU.flg_habil

	from
		TipoPrj BSU
	where
		(
		(@cod_tipoprj is null or RTRIM(@cod_tipoprj) is null or RTRIM(@cod_tipoprj)='' or RTRIM(cod_tipoprj) = RTRIM(@cod_tipoprj)) and
		(@flg_habil is null or RTRIM(@flg_habil) is null or RTRIM(@flg_habil)='' or RTRIM(flg_habil) = RTRIM(@flg_habil))
		)
	order by BSU.secuencia ASC
end
return(0)
go

grant execute on dbo.sp_GetTipoPrj to GesPetUsr
go

