use GesPet
go
print 'sp_ChgEstadoAprobado'
go
if exists (select * from sysobjects where name = 'sp_ChgEstadoAprobado' and sysstat & 7 = 4)
drop procedure dbo.sp_ChgEstadoAprobado
go
create procedure dbo.sp_ChgEstadoAprobado 
    @pet_nrointerno     int 
as 
declare @estadosopinion varchar(255) 
select @estadosopinion = 'OPINIO|OPINOK|EVALUA|EVALOK' 
BEGIN TRAN 
    delete GesPet..PeticionGrupo 
    where @pet_nrointerno=pet_nrointerno and 
        charindex(cod_estado,@estadosopinion)>0 
 
    delete GesPet..PeticionSector 
    where @pet_nrointerno=pet_nrointerno and 
        charindex(cod_estado,@estadosopinion)>0 and 
        not exists(select Ps.cod_grupo from  GesPet..PeticionGrupo Ps where RTRIM(Ps.cod_sector)=RTRIM(PeticionSector.cod_sector)) 
 
COMMIT TRAN 
return(0) 
go



grant execute on dbo.sp_ChgEstadoAprobado to GesPetUsr 
go


