use GesPet
go
print 'sp_GetAvanceCoef'
go
if exists (select * from sysobjects where name = 'sp_GetAvanceCoef' and sysstat & 7 = 4)
drop procedure dbo.sp_GetAvanceCoef
go
create procedure dbo.sp_GetAvanceCoef
	@cod_AvanceCoef		char(8)=null,
	@nom_AvanceCoef		char(30)=null,
	@cod_AvanceGrupo	char(8)=null,
	@flg_habil		char(1)=null,
	@nom_largo		char(100)=null,
	@porcentaje		int = 0


as
begin
	select
		cod_AvanceCoef,
		nom_AvanceCoef,
		cod_AvanceGrupo,
		nom_largo,
		porcentaje,
		flg_habil

	from
		GesPet..AvanceCoef
	where
		(@cod_AvanceCoef is null or RTRIM(@cod_AvanceCoef) is null or RTRIM(@cod_AvanceCoef)='' or RTRIM(cod_AvanceCoef) = RTRIM(@cod_AvanceCoef)) and
		(@cod_AvanceGrupo is null or RTRIM(@cod_AvanceGrupo) is null or RTRIM(@cod_AvanceGrupo)='' or RTRIM(cod_AvanceGrupo) = RTRIM(@cod_AvanceGrupo)) and
		(@flg_habil is null or RTRIM(@flg_habil) is null or RTRIM(@flg_habil)='' or RTRIM(flg_habil) = RTRIM(@flg_habil))
end
return(0)
go

grant execute on dbo.sp_GetAvanceCoef to GesPetUsr
go
