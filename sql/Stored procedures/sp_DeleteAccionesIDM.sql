/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 24.06.2013 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteAccionesIDM'
go

if exists (select * from sysobjects where name = 'sp_DeleteAccionesIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteAccionesIDM
go

create procedure dbo.sp_DeleteAccionesIDM
	@cod_accion	char(8)=''
as
	delete from GesPet..AccionesIDM
	where cod_accion = @cod_accion
go

grant execute on dbo.sp_DeleteAccionesIDM to GesPetUsr
go

print 'Actualización realizada.'
go
