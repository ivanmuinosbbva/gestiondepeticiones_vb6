/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 23.02.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticionKPI'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticionKPI' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticionKPI
go

create procedure dbo.sp_DeletePeticionKPI
	@pet_nrointerno		int,
	@pet_kpiid			int
as
	delete from
		GesPet.dbo.PeticionKPI
	where
		pet_nrointerno = @pet_nrointerno and 
		(@pet_kpiid is null or pet_kpiid = @pet_kpiid)
go

grant execute on dbo.sp_DeletePeticionKPI to GesPetUsr
go

print 'Actualización realizada.'
go
