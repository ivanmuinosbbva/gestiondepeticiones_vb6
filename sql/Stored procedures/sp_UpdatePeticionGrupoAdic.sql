/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go
print 'sp_UpdatePeticionGrupoAdic'
go
if exists (select * from sysobjects where name = 'sp_UpdatePeticionGrupoAdic' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdatePeticionGrupoAdic
go
create procedure dbo.sp_UpdatePeticionGrupoAdic 
    @pet_nrointerno     int, 
    @cod_grupo          char(8), 
    @prio_ejecuc        int=0, 
    @info_adicio        char(250)=''
as 
 
 
if exists (select cod_grupo from GesPet..PeticionGrupo where @pet_nrointerno=pet_nrointerno and RTRIM(cod_grupo)=RTRIM(@cod_grupo)) 
begin 
	
	update GesPet..PeticionGrupo 
	set prio_ejecuc=   @prio_ejecuc, 
		info_adicio       =   @info_adicio,
		audit_user = SUSER_NAME(),   
		audit_date = getdate()   
	where @pet_nrointerno=pet_nrointerno and 
		RTRIM(cod_grupo)=RTRIM(@cod_grupo) 

end 
else 
begin 
    raiserror 30001 'Modificacion de Grupo inexistente'   
    select 30001 as ErrCode , 'Modificacion de Grupo inexistente' as ErrDesc   
    return (30001)   
end 
return(0) 
go



grant execute on dbo.sp_UpdatePeticionGrupoAdic to GesPetUsr 
go


