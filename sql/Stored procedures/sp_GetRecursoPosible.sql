use GesPet
go
print 'sp_GetRecursoPosible'
go
if exists (select * from sysobjects where name = 'sp_GetRecursoPosible' and sysstat & 7 = 4)
drop procedure dbo.sp_GetRecursoPosible
go
create procedure dbo.sp_GetRecursoPosible 
                 @cod_recurso   varchar(10) 
as 
 
    select cod_recurso, 
        nom_recurso, 
        sector=1 
    from GesPet..Recurso 
    where RTRIM(cod_recurso) = RTRIM(@cod_recurso) 
    UNION ALL 
    select cod_recurso, 
        nom_recurso, 
        sector=2 
    from GesPet..Recurso 
    where  (RTRIM(estado_recurso)='A') and 
        ((RTRIM(dlg_recurso) is not null and RTRIM(dlg_recurso)<>'')) and (RTRIM(dlg_recurso) = RTRIM(@cod_recurso)) and 
        (dlg_desde is not null and convert(char(8),dlg_desde,112) <= convert(char(8),getdate(),112)) and 
        (dlg_hasta is not null and convert(char(8),dlg_hasta,112) >= convert(char(8),getdate(),112)) 
    ORDER BY sector,nom_recurso 
   return(0) 
go



grant execute on dbo.sp_GetRecursoPosible to GesPetUsr 
go


