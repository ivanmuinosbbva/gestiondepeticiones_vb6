/*  C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.02.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeticionInfohd'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeticionInfohd' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeticionInfohd
go

create procedure dbo.sp_UpdatePeticionInfohd
	@pet_nrointerno	int,
	@info_id		int,
	@info_idver		int,
	@info_item		int,
	@info_estado	char(1),
	@info_valor		int,
	@info_orden		int
as
	declare @c_cod_grupo		char(8)
	declare @c_cod_sector		char(8)
	declare @c_cod_gerencia		char(8)
	declare @c_cod_direccion	char(8)
	declare @responsable		char(10)
	declare @cod_recurso		char(10)
	declare @defaulttext		varchar(255)
	declare @nivel				char(4)
	declare @area				char(8)
	declare @tipo				char(3)
	declare @clase				char(4)
	declare @referenteSistema	char(10)

	-- Actualizo el item del informe.
	update 
		GesPet..PeticionInfohd
	set
		info_estado	= @info_estado,
		info_valor	= @info_valor,
		info_orden	= @info_orden
	where 
		pet_nrointerno = @pet_nrointerno and
		info_id = @info_id and
		info_idver = @info_idver and
		info_item = @info_item
	
	-- En caso que el control del item "no sea positivo"... agrego los responsables a la tabla de responsables.
	--if @info_estado = 'N'
	--C|P|
	--if charindex(LTRIM(RTRIM(@info_estado)),'N|V|R|L|U|') > 0
	if @info_estado = 'N' or (@info_valor in (2,3) and charindex(LTRIM(RTRIM(@info_estado)),'N|V|R|L|U|')>0)
		begin
			-- Obtengo el tipo y clase de petici�n por si es necesario determinar responsables a partir de estos datos
			select 
				@clase				= p.cod_clase,
				@tipo				= p.cod_tipo_peticion,
				@referenteSistema	= p.cod_bpar
			from Peticion p
			where p.pet_nrointerno = @pet_nrointerno

			select 
				@responsable = i.infoprot_itemres,
				@defaulttext = i.infoprot_itemresp
			from Infoproi i
			where i.infoprot_item = @info_item
			
			-- Los responsables son o el Referente de Sistema o alguno de los recursos de la parte solicitante + Referente de sistema.
			if charindex(LTRIM(RTRIM(@responsable)), 'SOLI|REFE|AUTO|SUPE|BPAR|')>0
				begin
					select @cod_recurso = case
						when @responsable = 'SOLI' then p.cod_solicitante
						when @responsable = 'REFE' then p.cod_referente
						when @responsable = 'AUTO' then p.cod_supervisor
						when @responsable = 'SUPE' then p.cod_director
						when @responsable = 'BPAR' then p.cod_bpar
					end
					from Peticion p
					where p.pet_nrointerno = @pet_nrointerno

					-- Perfil para el responsable
					SELECT 
						@nivel = rp.cod_nivel, 
						@area  = rp.cod_area
					FROM RecursoPerfil rp
					WHERE 
						rp.cod_recurso = LTRIM(RTRIM(@cod_recurso)) and
						rp.cod_perfil = @responsable

					--if LTRIM(RTRIM(@info_comen)) = '' OR @info_comen IS NULL
					--	select @info_comen = LTRIM(RTRIM(@defaulttext))

					if not exists (
						select pr.info_item 
						from PeticionInfohr pr
						where pr.pet_nrointerno = @pet_nrointerno and pr.info_id = @info_id and info_idver = @info_idver and pr.info_item = @info_item and pr.cod_recurso = @cod_recurso)
							INSERT INTO PeticionInfohr (pet_nrointerno, info_id, info_idver, info_item, cod_recurso, cod_perfil, cod_nivel, cod_area, info_valor, info_comen)
							VALUES (@pet_nrointerno, @info_id, @info_idver, @info_item, @cod_recurso, @responsable, @nivel, @area, @info_valor, @defaulttext)
					else
						UPDATE PeticionInfohr
						SET cod_perfil = @responsable, cod_nivel=@nivel, cod_area=@area, info_valor=@info_valor
						WHERE pet_nrointerno = @pet_nrointerno and info_id = @info_id and info_idver = @info_idver and info_item = @info_item and cod_recurso = @cod_recurso
				end
			else
				-- Los responsables son alguno de los responsables t�cnicos o funcionales, la parte ejecutora.
				begin
					if @responsable = 'MULT'
						begin
							-- Para el caso MULT, los responsables son los BPs, y los responsables de ejecuci�n, en el caso de las NORMALES y los l�deres en caso de las propias
							-- Borro el item y vuelvo a generarlo por cada responsable
							delete from PeticionInfohr
							where pet_nrointerno = @pet_nrointerno and info_id = @info_id and info_idver = @info_idver and info_item = @info_item
							
							-- Referente de sistema
							INSERT INTO PeticionInfohr (pet_nrointerno, info_id, info_idver, info_item, cod_recurso, cod_perfil, cod_nivel, cod_area, info_valor, info_comen)
							VALUES (@pet_nrointerno, @info_id, @info_idver, @info_item, @referenteSistema, 'BPAR', @nivel, @area, @info_valor, @defaulttext)
							
							INSERT INTO PeticionInfohr (pet_nrointerno, info_id, info_idver, info_item, cod_recurso, cod_perfil, cod_nivel, cod_area, info_valor, info_comen)  
								SELECT @pet_nrointerno, @info_id, @info_idver, @info_item, r.cod_recurso, 'CGRU', @nivel, @area, @info_valor, @defaulttext
								FROM Recurso r
								WHERE 
									r.cod_grupo in (
										select pg.cod_grupo
										from 
											PeticionGrupo pg inner join
											Grupo g on (pg.cod_grupo = g.cod_grupo)
										where 
											pg.pet_nrointerno = @pet_nrointerno and
											charindex(pg.cod_estado,'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and
											g.grupo_homologacion <> 'H') and
									r.flg_cargoarea= 'S' and r.estado_recurso = 'A'
							/*
							if not exists (
								select pr.info_item 
								from PeticionInfohr pr 
								where pr.pet_nrointerno = @pet_nrointerno and pr.info_id = @info_id and info_idver = @info_idver and pr.info_item = @info_item and pr.cod_recurso = @cod_recurso)
									INSERT INTO PeticionInfohr (pet_nrointerno, info_id, info_idver, info_item, cod_recurso, cod_perfil, cod_nivel, cod_area, info_valor, info_comen)
									VALUES (@pet_nrointerno, @info_id, @info_idver, @info_item, @cod_recurso, @responsable, @nivel,@area, @info_valor, @defaulttext)
							else
								UPDATE PeticionInfohr
								SET cod_perfil = @responsable, cod_nivel=@nivel, cod_area=@area, info_valor=@info_valor, info_comen=@info_comen
								WHERE pet_nrointerno = @pet_nrointerno and info_id = @info_id and info_idver = @info_idver and info_item = @info_item and cod_recurso = @cod_recurso
							*/
						end
							/*
							if @info_estado = 'V'		-- Supervisor
								begin
									-- Si existe m�s de un supervisor en la parte ejecutora, traigo el que puso el OK de usuario
									select 
										@cod_recurso = r.cod_recurso,
										@nivel		 = 'SECT',
										@area		 = r.cod_sector
									from
										PeticionConf pc inner join
										Recurso r on (pc.audit_user = r.cod_recurso)
									where 
										pc.pet_nrointerno = @pet_nrointerno and 
										pc.ok_modo <> 'N/A' and
										pc.ok_tipo in ('TEST','TESP') and
										r.flg_cargoarea = 'S' and 
										r.cod_sector in (
											select ps.cod_sector
											from PeticionSector ps
											where ps.pet_nrointerno = @pet_nrointerno and 
												charindex('RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|',ps.cod_estado)=0
										)
								end
							if @info_estado = 'U'		-- Usuario
								begin
									select @cod_recurso = p.cod_solicitante
									from Peticion p
									where p.pet_nrointerno = @pet_nrointerno
								end

							if @info_estado = 'R'		-- Referente de sistema
								begin
									select @cod_recurso = p.cod_bpar
									from Peticion p
									where p.pet_nrointerno = @pet_nrointerno
								end

							if not exists (
								select pr.info_item 
								from PeticionInfohr pr
								where pr.pet_nrointerno = @pet_nrointerno and pr.info_id = @info_id and info_idver = @info_idver and pr.info_item = @info_item and pr.cod_recurso = @cod_recurso)
									INSERT INTO PeticionInfohr (pet_nrointerno, info_id, info_idver, info_item, cod_recurso, cod_perfil, cod_nivel, cod_area, info_valor, info_comen)
									VALUES (@pet_nrointerno, @info_id, @info_idver, @info_item, @cod_recurso, @responsable, @nivel,@area, @info_valor, @defaulttext)
							else
								UPDATE PeticionInfohr
								SET cod_perfil = @responsable, cod_nivel=@nivel, cod_area=@area, info_valor=@info_valor, info_comen=@info_comen
								WHERE pet_nrointerno = @pet_nrointerno and info_id = @info_id and info_idver = @info_idver and info_item = @info_item and cod_recurso = @cod_recurso
						*/
					if @responsable = 'GRUT'
						begin
							declare CursGrupo cursor for
								select 
									pg.cod_grupo,
									pg.cod_sector,
									pg.cod_gerencia,
									pg.cod_direccion
								from 
									PeticionGrupo pg inner join
									Grupo g on (g.cod_grupo = pg.cod_grupo)
								where 
									pg.pet_nrointerno = @pet_nrointerno and 
									g.grupo_homologacion = 'S'
							for read only

							open CursGrupo fetch CursGrupo into @c_cod_grupo, @c_cod_sector, @c_cod_gerencia, @c_cod_direccion while (@@sqlstatus = 0)
								begin
									-- Obtengo el legajo del recurso responsable de grupo t�cnico
									select @cod_recurso = r.cod_recurso
									from Recurso r
									where 
										r.flg_cargoarea = 'S' and
										r.cod_gerencia = LTRIM(RTRIM(@c_cod_gerencia)) and
										r.cod_direccion = LTRIM(RTRIM(@c_cod_direccion)) and
										r.cod_sector = LTRIM(RTRIM(@c_cod_sector)) and 
										r.cod_grupo = LTRIM(RTRIM(@c_cod_grupo))
									fetch CursGrupo into @c_cod_grupo, @c_cod_sector, @c_cod_gerencia, @c_cod_direccion
									
									if @c_cod_grupo is null or LTRIM(RTRIM(@c_cod_grupo)) = ''
										begin
											select @nivel = 'SECT'
											select @area  = @c_cod_sector
											select @responsable = 'CSEC'
										end
									else
										begin
											select @nivel = 'GRUP'
											select @area = @c_cod_grupo
											select @responsable = 'CGRU'
										end
										
									if not exists (
										select pr.info_item 
										from PeticionInfohr pr
										where pr.pet_nrointerno = @pet_nrointerno and pr.info_id = @info_id and info_idver = @info_idver and pr.info_item = @info_item and pr.cod_recurso = @cod_recurso)
											INSERT INTO PeticionInfohr (pet_nrointerno, info_id, info_idver, info_item, cod_recurso, cod_perfil, cod_nivel, cod_area, info_valor, info_comen)
											VALUES (@pet_nrointerno, @info_id, @info_idver, @info_item, @cod_recurso, @responsable, @nivel,@area, @info_valor, @defaulttext)
									else
										UPDATE PeticionInfohr
										SET cod_perfil = @responsable, cod_nivel=@nivel, cod_area=@area, info_valor=@info_valor
										WHERE pet_nrointerno = @pet_nrointerno and info_id = @info_id and info_idver = @info_idver and info_item = @info_item and cod_recurso = @cod_recurso
								end
							close CursGrupo
							
							-- Elimina la referencia al cursor
							deallocate cursor CursGrupo
						end
						/*
						SELECT @pet_nrointerno, @info_id, @info_item
							INTO PeticionInfohr
							FROM 
								PeticionInfohd pd inner join
								PeticionGrupo pg on (pd.pet_nrointerno = pg.pet_nrointerno) inner join
								Grupo g on (g.cod_grupo = pg.cod_grupo)
							WHERE 
								pd.pet_nrointerno = @pet_nrointerno and pd.info_id = @info_id and 
								g.grupo_homologacion = 'S'
							GROUP BY
							ORDER BY
						*/
					
					/*
					else if @responsable = 'GRUF'

					else if @responsable = 'SECT'

					else if @responsable = 'SECF'
					-- ddddd
					*/
				end
		end
	else
		begin
			DELETE 
			FROM PeticionInfohr
			WHERE pet_nrointerno = @pet_nrointerno and info_id = @info_id and info_idver = @info_idver and info_item = @info_item
		end

go

grant execute on dbo.sp_UpdatePeticionInfohd to GesPetUsr
go

print 'Actualizaci�n realizada.'
go

--'GRUT|GRUF|SECT|SECF|'