/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.07.2014 - Nuevo:
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertTipoSolicitud'
go

if exists (select * from sysobjects where name = 'sp_InsertTipoSolicitud' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertTipoSolicitud
go

create procedure dbo.sp_InsertTipoSolicitud
	@cod_tipo	char(1),
	@nom_tipo	char(20),
	@dsc_tipo	char(50)
as
	insert into GesPet.dbo.TipoSolicitud (
		cod_tipo,
		nom_tipo,
		dsc_tipo)
	values (
		@cod_tipo,
		@nom_tipo,
		@dsc_tipo)
go

grant execute on dbo.sp_InsertTipoSolicitud to GesPetUsr
go

print 'Actualización realizada.'
go
