/*
-000- a. FJS 19.11.2015 - Nuevo SP para ejecutar consultas varias.
-001- a. FJS 21.06.2016 - Modificaci�n: se cambia la condici�n para ver los OMA de peticiones activas.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_rptPeticionesVarios1'
go

if exists (select * from sysobjects where name = 'sp_rptPeticionesVarios1' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptPeticionesVarios1
go

create procedure dbo.sp_rptPeticionesVarios1
	@opcion			int=1,
	@nivel			char(4)='NULL',
	@area			char(8)='NULL'
as 
	-- Para excluir al grupo homologador de los informes
	declare @grupo_homologacion		char(8)
	select @grupo_homologacion = g.cod_grupo
	from Grupo g
	where g.grupo_homologacion = 'H'

	create table #temporalInforme (
		agrupamiento		char(40)		NULL,			-- Concatenaci�n de c�digos de �rea
		cod_direccion		char(10)		NULL,
		nom_direccion		varchar(60)		NULL,
		cod_gerencia		char(10)		NULL,
		nom_gerencia		varchar(60)		NULL,
		cod_sector			char(10)		NULL,
		nom_sector			varchar(60)		NULL,
		cod_grupo			char(10)		NULL,
		nom_grupo			varchar(60)		NULL,
		pet_nrointerno		int				NULL,
		pet_nroasignado		int				NULL,
		cod_tipo_peticion	char(3)			NULL,
		cod_clase			char(4)			NULL,
		titulo				varchar(100)	NULL,
		cod_estado			char(6)			NULL,
		nom_estado			varchar(40)		NULL,
		fe_estado			smalldatetime	NULL,
		regulatorio			char(1)			NULL,
		projid				int				NULL,
		projsubid			int				NULL,
		projsubsid			int				NULL,
		fe_ini_plan			smalldatetime	NULL,
		fe_fin_plan			smalldatetime	NULL,
		fe_ultima_carga		smalldatetime	NULL,
		dias				int				NULL)

	-- ***************************************************************************
	-- 1. Peticiones al RS (Referente de Sistema) por m�s de 30 d�as
	-- ***************************************************************************
	if @opcion = 1
		begin
			INSERT INTO #temporalInforme 
			select
				p.cod_bpar		as agrupamiento,
				p.cod_bpar		as cod_direccion,
				nom_direccion = (select x.nom_recurso from Recurso x where x.cod_recurso = p.cod_bpar),
				null			as cod_gerencia,
				null			as nom_gerencia,
				null			as cod_sector,
				null			as nom_sector,
				null			as cod_grupo,
				null			as nom_grupo,
				p.pet_nrointerno,
				p.pet_nroasignado,
				p.cod_tipo_peticion,
				p.cod_clase,
				p.titulo,
				p.cod_estado,
				nom_estado = (select x.nom_estado from Estados x where x.cod_estado = p.cod_estado),
				p.fe_estado,
				p.pet_regulatorio,
				p.pet_projid,
				p.pet_projsubid,
				p.pet_projsubsid,
				p.fe_ini_plan,
				p.fe_fin_plan,
				null,
				DATEDIFF(DAY, p.fe_estado, getdate())	as 'dias'
			from
				Peticion p 
			where
				p.cod_estado = 'COMITE' and 
				p.cod_tipo_peticion <> 'PRO' and 
				DATEDIFF(DAY, p.fe_estado, getdate()) > 30
			order by 1
		end
	
	-- ***************************************************************************
	-- 2. Peticiones "A estimar esfuerzo" y "A planificar" por m�s de 30 d�as
	-- ***************************************************************************
	if @opcion = 2
		begin
			INSERT INTO #temporalInforme 
			select
				RTRIM(pg.cod_direccion) + '|' + RTRIM(pg.cod_gerencia) + '|' + RTRIM(pg.cod_sector) + '|' + RTRIM(pg.cod_grupo)	as agrupamiento,
				pg.cod_direccion,
				nom_direccion = (select x.nom_direccion from Direccion x where x.cod_direccion = pg.cod_direccion),
				pg.cod_gerencia,
				nom_gerencia = (select x.nom_gerencia from Gerencia x where x.cod_gerencia = pg.cod_gerencia),
				pg.cod_sector,
				nom_sector = (select x.nom_sector from Sector x where x.cod_sector = pg.cod_sector),
				pg.cod_grupo,
				nom_grupo = (select x.nom_grupo from Grupo x where x.cod_grupo = pg.cod_grupo),
				p.pet_nrointerno,
				p.pet_nroasignado,
				p.cod_tipo_peticion,
				p.cod_clase,
				p.titulo,
				p.cod_estado,
				nom_estado = (select x.nom_estado from Estados x where x.cod_estado = p.cod_estado),
				p.fe_estado,
				p.pet_regulatorio,
				p.pet_projid,
				p.pet_projsubid,
				p.pet_projsubsid,
				p.fe_ini_plan,
				p.fe_fin_plan,
				null,
				DATEDIFF(DAY, p.fe_estado, getdate())	as 'dias'
			from
				Peticion p inner join 
				PeticionGrupo pg on (p.pet_nrointerno = pg.pet_nrointerno and pg.cod_grupo <> @grupo_homologacion)
			where 
				((@nivel is null or RTRIM(@nivel)='NULL') or
				(@nivel='DIRE' and RTRIM(pg.cod_direccion)=RTRIM(@area)) or
				(@nivel='GERE' and RTRIM(pg.cod_gerencia)=RTRIM(@area)) or
				(@nivel='SECT' and RTRIM(pg.cod_sector)=RTRIM(@area)) or
				(@nivel='GRUP' and RTRIM(pg.cod_grupo)=RTRIM(@area))) and
				charindex(p.cod_estado, 'ESTIMA|PLANIF|') > 0 and
				DATEDIFF(DAY, p.fe_estado, getdate()) > 30
			order by
				pg.cod_direccion,
				pg.cod_gerencia,
				pg.cod_sector,
				pg.cod_grupo,
				pg.pet_nrointerno
		end
	
	-- *****************************************************************************************
	-- 3. Peticiones "A estimar esfuerzo" y "A planificar" con m�s de 30 horas cargadas
	-- *****************************************************************************************
	if @opcion = 3
		begin
			INSERT INTO #temporalInforme 
			select
				RTRIM(pg.cod_direccion) + '|' + RTRIM(pg.cod_gerencia) + '|' + RTRIM(pg.cod_sector) + '|' + RTRIM(pg.cod_grupo)	as agrupamiento,
				pg.cod_direccion,
				nom_direccion = (select x.nom_direccion from Direccion x where x.cod_direccion = pg.cod_direccion),
				pg.cod_gerencia,
				nom_gerencia = (select x.nom_gerencia from Gerencia x where x.cod_gerencia = pg.cod_gerencia),
				pg.cod_sector,
				nom_sector = (select x.nom_sector from Sector x where x.cod_sector = pg.cod_sector),
				pg.cod_grupo,
				nom_grupo = (select x.nom_grupo from Grupo x where x.cod_grupo = pg.cod_grupo),
				p.pet_nrointerno,
				p.pet_nroasignado,
				p.cod_tipo_peticion,
				p.cod_clase,
				p.titulo,
				p.cod_estado,
				nom_estado = (select x.nom_estado from Estados x where x.cod_estado = p.cod_estado),
				p.fe_estado,
				p.pet_regulatorio,
				p.pet_projid,
				p.pet_projsubid,
				p.pet_projsubsid,
				p.fe_ini_plan,
				p.fe_fin_plan,
				null,
				horas = (select SUM(convert(decimal(10,2),ht.horas) / convert(decimal(10,2),60)) 
						 from HorasTrabajadas ht 
						 where ht.pet_nrointerno = p.pet_nrointerno group by ht.pet_nrointerno)
			from
				Peticion p inner join 
				PeticionGrupo pg on (p.pet_nrointerno = pg.pet_nrointerno and pg.cod_grupo <> @grupo_homologacion)
			where 
				((@nivel is null or RTRIM(@nivel)='NULL') or
				(@nivel='DIRE' and RTRIM(pg.cod_direccion)=RTRIM(@area)) or
				(@nivel='GERE' and RTRIM(pg.cod_gerencia)=RTRIM(@area)) or
				(@nivel='SECT' and RTRIM(pg.cod_sector)=RTRIM(@area)) or
				(@nivel='GRUP' and RTRIM(pg.cod_grupo)=RTRIM(@area))) and
				charindex(p.cod_estado, 'ESTIMA|PLANIF|') > 0 and
				p.pet_nrointerno in (
					select ht.pet_nrointerno
					from HorasTrabajadas ht
					where 
						ht.pet_nrointerno = p.pet_nrointerno
					group by ht.pet_nrointerno
					having SUM(ht.horas) > 1800)
		end

	-- *****************************************************************************************
	-- 4. Peticiones "A estimar esfuerzo" y "A planificar" con m�s de 30 d�as de atraso respecto
	--    de su fecha de inicio planificado.
	-- *****************************************************************************************
	if @opcion = 4
		begin
			INSERT INTO #temporalInforme 
			select
				RTRIM(pg.cod_direccion) + '|' + RTRIM(pg.cod_gerencia) + '|' + RTRIM(pg.cod_sector) + '|' + RTRIM(pg.cod_grupo)	as agrupamiento,
				pg.cod_direccion,
				nom_direccion = (select x.nom_direccion from Direccion x where x.cod_direccion = pg.cod_direccion),
				pg.cod_gerencia,
				nom_gerencia = (select x.nom_gerencia from Gerencia x where x.cod_gerencia = pg.cod_gerencia),
				pg.cod_sector,
				nom_sector = (select x.nom_sector from Sector x where x.cod_sector = pg.cod_sector),
				pg.cod_grupo,
				nom_grupo = (select x.nom_grupo from Grupo x where x.cod_grupo = pg.cod_grupo),
				p.pet_nrointerno,
				p.pet_nroasignado,
				p.cod_tipo_peticion,
				p.cod_clase,
				p.titulo,
				p.cod_estado,
				nom_estado = (select x.nom_estado from Estados x where x.cod_estado = p.cod_estado),
				p.fe_estado,
				p.pet_regulatorio,
				p.pet_projid,
				p.pet_projsubid,
				p.pet_projsubsid,
				p.fe_ini_plan,
				p.fe_fin_plan,
				null,
				DATEDIFF(DAY, p.fe_ini_plan, p.fe_ini_real)	as 'dias'
			from
				Peticion p inner join 
				PeticionGrupo pg on (p.pet_nrointerno = pg.pet_nrointerno and pg.cod_grupo <> @grupo_homologacion)
			where 
				((@nivel is null or RTRIM(@nivel)='NULL') or
				(@nivel='DIRE' and RTRIM(pg.cod_direccion)=RTRIM(@area)) or
				(@nivel='GERE' and RTRIM(pg.cod_gerencia)=RTRIM(@area)) or
				(@nivel='SECT' and RTRIM(pg.cod_sector)=RTRIM(@area)) or
				(@nivel='GRUP' and RTRIM(pg.cod_grupo)=RTRIM(@area))) and
				charindex(p.cod_estado,'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') <> 0 and
				DATEDIFF(DAY, p.fe_ini_plan, p.fe_ini_real) > 30
		end

	-- *****************************************************************************************
	-- 5. Peticiones "En ejecuci�n" por m�s de un a�o
	-- *****************************************************************************************
	if @opcion = 5
		begin
			INSERT INTO #temporalInforme 
			select
				RTRIM(pg.cod_direccion) + '|' + RTRIM(pg.cod_gerencia) + '|' + RTRIM(pg.cod_sector) + '|' + RTRIM(pg.cod_grupo)	as agrupamiento,
				pg.cod_direccion,
				nom_direccion = (select x.nom_direccion from Direccion x where x.cod_direccion = pg.cod_direccion),
				pg.cod_gerencia,
				nom_gerencia = (select x.nom_gerencia from Gerencia x where x.cod_gerencia = pg.cod_gerencia),
				pg.cod_sector,
				nom_sector = (select x.nom_sector from Sector x where x.cod_sector = pg.cod_sector),
				pg.cod_grupo,
				nom_grupo = (select x.nom_grupo from Grupo x where x.cod_grupo = pg.cod_grupo),
				p.pet_nrointerno,
				p.pet_nroasignado,
				p.cod_tipo_peticion,
				p.cod_clase,
				p.titulo,
				p.cod_estado,
				nom_estado = (select x.nom_estado from Estados x where x.cod_estado = p.cod_estado),
				p.fe_estado,
				p.pet_regulatorio,
				p.pet_projid,
				p.pet_projsubid,
				p.pet_projsubsid,
				p.fe_ini_plan,
				p.fe_fin_plan,
				null,
				DATEDIFF(DAY, p.fe_estado, getdate())	as 'dias'
			from
				Peticion p inner join 
				PeticionGrupo pg on (p.pet_nrointerno = pg.pet_nrointerno and pg.cod_grupo <> @grupo_homologacion)
			where 
				((@nivel is null or RTRIM(@nivel)='NULL') or
				(@nivel='DIRE' and RTRIM(pg.cod_direccion)=RTRIM(@area)) or
				(@nivel='GERE' and RTRIM(pg.cod_gerencia)=RTRIM(@area)) or
				(@nivel='SECT' and RTRIM(pg.cod_sector)=RTRIM(@area)) or
				(@nivel='GRUP' and RTRIM(pg.cod_grupo)=RTRIM(@area))) and
				p.cod_estado = 'EJECUC' and
				DATEDIFF(DAY, p.fe_estado, getdate()) > 365
		end

	-- *****************************************************************************************
	-- 6. Peticiones "En ejecuci�n" con m�s de 30 d�as de atraso respecto de su fecha de 
	--	  finalizaci�n planificada.
	-- *****************************************************************************************
	if @opcion = 6
		begin
			INSERT INTO #temporalInforme 
			select
				RTRIM(pg.cod_direccion) + '|' + RTRIM(pg.cod_gerencia) + '|' + RTRIM(pg.cod_sector) + '|' + RTRIM(pg.cod_grupo)	as agrupamiento,
				pg.cod_direccion,
				nom_direccion = (select x.nom_direccion from Direccion x where x.cod_direccion = pg.cod_direccion),
				pg.cod_gerencia,
				nom_gerencia = (select x.nom_gerencia from Gerencia x where x.cod_gerencia = pg.cod_gerencia),
				pg.cod_sector,
				nom_sector = (select x.nom_sector from Sector x where x.cod_sector = pg.cod_sector),
				pg.cod_grupo,
				nom_grupo = (select x.nom_grupo from Grupo x where x.cod_grupo = pg.cod_grupo),
				p.pet_nrointerno,
				p.pet_nroasignado,
				p.cod_tipo_peticion,
				p.cod_clase,
				p.titulo,
				p.cod_estado,
				nom_estado = (select x.nom_estado from Estados x where x.cod_estado = p.cod_estado),
				p.fe_estado,
				p.pet_regulatorio,
				p.pet_projid,
				p.pet_projsubid,
				p.pet_projsubsid,
				p.fe_ini_plan,
				p.fe_fin_plan,
				null,
				DATEDIFF(DAY, p.fe_estado, getdate())	as 'dias'
			from
				Peticion p inner join 
				PeticionGrupo pg on (p.pet_nrointerno = pg.pet_nrointerno and pg.cod_grupo <> @grupo_homologacion)
			where 
				((@nivel is null or RTRIM(@nivel)='NULL') or
				(@nivel='DIRE' and RTRIM(pg.cod_direccion)=RTRIM(@area)) or
				(@nivel='GERE' and RTRIM(pg.cod_gerencia)=RTRIM(@area)) or
				(@nivel='SECT' and RTRIM(pg.cod_sector)=RTRIM(@area)) or
				(@nivel='GRUP' and RTRIM(pg.cod_grupo)=RTRIM(@area))) and
				p.cod_estado = 'EJECUC' and
				DATEDIFF(DAY, p.fe_fin_plan, getdate()) > 30
		end
		
	-- *****************************************************************************************
	-- 7. Peticiones "En ejecuci�n" sin horas cargadas en los �ltimos 60 d�as
	-- *****************************************************************************************
	if @opcion = 7
		begin
			INSERT INTO #temporalInforme 
			select
				RTRIM(pg.cod_direccion) + '|' + RTRIM(pg.cod_gerencia) + '|' + RTRIM(pg.cod_sector) + '|' + RTRIM(pg.cod_grupo)	as agrupamiento,
				pg.cod_direccion,
				nom_direccion = (select x.nom_direccion from Direccion x where x.cod_direccion = pg.cod_direccion),
				pg.cod_gerencia,
				nom_gerencia = (select x.nom_gerencia from Gerencia x where x.cod_gerencia = pg.cod_gerencia),
				pg.cod_sector,
				nom_sector = (select x.nom_sector from Sector x where x.cod_sector = pg.cod_sector),
				pg.cod_grupo,
				nom_grupo = (select x.nom_grupo from Grupo x where x.cod_grupo = pg.cod_grupo),
				p.pet_nrointerno,
				p.pet_nroasignado,
				p.cod_tipo_peticion,
				p.cod_clase,
				p.titulo,
				p.cod_estado,
				nom_estado = (select x.nom_estado from Estados x where x.cod_estado = p.cod_estado),
				p.fe_estado,
				p.pet_regulatorio,
				p.pet_projid,
				p.pet_projsubid,
				p.pet_projsubsid,
				p.fe_ini_plan,
				p.fe_fin_plan,
				ultima_carga = (select MAX(ht.fe_desde)
								from HorasTrabajadas ht 
								where ht.pet_nrointerno = p.pet_nrointerno group by ht.pet_nrointerno),
				dias = (select DATEDIFF(DAY, MAX(ht.fe_desde), getdate())
						from HorasTrabajadas ht 
						where ht.pet_nrointerno = p.pet_nrointerno group by ht.pet_nrointerno)
			from
				Peticion p inner join 
				PeticionGrupo pg on (p.pet_nrointerno = pg.pet_nrointerno and pg.cod_grupo <> @grupo_homologacion)
			where 
				((@nivel is null or RTRIM(@nivel)='NULL') or
				(@nivel='DIRE' and RTRIM(pg.cod_direccion)=RTRIM(@area)) or
				(@nivel='GERE' and RTRIM(pg.cod_gerencia)=RTRIM(@area)) or
				(@nivel='SECT' and RTRIM(pg.cod_sector)=RTRIM(@area)) or
				(@nivel='GRUP' and RTRIM(pg.cod_grupo)=RTRIM(@area))) and
				p.cod_estado = 'EJECUC' and
				p.pet_nrointerno in (
					select ht.pet_nrointerno
					from HorasTrabajadas ht
					where ht.pet_nrointerno = p.pet_nrointerno
					group by ht.pet_nrointerno
					having DATEDIFF(DAY, MAX(ht.fe_desde), getdate()) > 60)
		end
		
	-- *****************************************************************************************
	-- 9. Peticiones en estado "Suspendida" por m�s de 60 d�as 
	-- *****************************************************************************************
	if @opcion = 9
		begin
			INSERT INTO #temporalInforme 
			select
				RTRIM(pg.cod_direccion) + '|' + RTRIM(pg.cod_gerencia) + '|' + RTRIM(pg.cod_sector) + '|' + RTRIM(pg.cod_grupo)	as agrupamiento,
				pg.cod_direccion,
				nom_direccion = (select x.nom_direccion from Direccion x where x.cod_direccion = pg.cod_direccion),
				pg.cod_gerencia,
				nom_gerencia = (select x.nom_gerencia from Gerencia x where x.cod_gerencia = pg.cod_gerencia),
				pg.cod_sector,
				nom_sector = (select x.nom_sector from Sector x where x.cod_sector = pg.cod_sector),
				pg.cod_grupo,
				nom_grupo = (select x.nom_grupo from Grupo x where x.cod_grupo = pg.cod_grupo),
				p.pet_nrointerno,
				p.pet_nroasignado,
				p.cod_tipo_peticion,
				p.cod_clase,
				p.titulo,
				p.cod_estado,
				nom_estado = (select x.nom_estado from Estados x where x.cod_estado = p.cod_estado),
				p.fe_estado,
				p.pet_regulatorio,
				p.pet_projid,
				p.pet_projsubid,
				p.pet_projsubsid,
				p.fe_ini_plan,
				p.fe_fin_plan,
				null,
				DATEDIFF(DAY, p.fe_estado, getdate())	as 'dias'
			from
				Peticion p inner join 
				PeticionGrupo pg on (p.pet_nrointerno = pg.pet_nrointerno and pg.cod_grupo <> @grupo_homologacion)
			where 
				((@nivel is null or RTRIM(@nivel)='NULL') or
				(@nivel='DIRE' and RTRIM(pg.cod_direccion)=RTRIM(@area)) or
				(@nivel='GERE' and RTRIM(pg.cod_gerencia)=RTRIM(@area)) or
				(@nivel='SECT' and RTRIM(pg.cod_sector)=RTRIM(@area)) or
				(@nivel='GRUP' and RTRIM(pg.cod_grupo)=RTRIM(@area))) and
				p.cod_estado = 'SUSPEN' and
				DATEDIFF(DAY, p.fe_estado, getdate()) > 60
		end

	-- *****************************************************************************************
	-- 10. Peticiones con m�s de 40 horas, cargadas por un grupo no vinculado a la petici�n.
	-- *****************************************************************************************
	if @opcion = 10
		begin
			INSERT INTO #temporalInforme 
			select
				RTRIM(pg.cod_direccion) + '|' + RTRIM(pg.cod_gerencia) + '|' + RTRIM(pg.cod_sector) + '|' + RTRIM(pg.cod_grupo)	as agrupamiento,
				pg.cod_direccion,
				nom_direccion = (select x.nom_direccion from Direccion x where x.cod_direccion = pg.cod_direccion),
				pg.cod_gerencia,
				nom_gerencia = (select x.nom_gerencia from Gerencia x where x.cod_gerencia = pg.cod_gerencia),
				pg.cod_sector,
				nom_sector = (select x.nom_sector from Sector x where x.cod_sector = pg.cod_sector),
				pg.cod_grupo,
				nom_grupo = (select x.nom_grupo from Grupo x where x.cod_grupo = pg.cod_grupo),
				p.pet_nrointerno,
				p.pet_nroasignado,
				p.cod_tipo_peticion,
				p.cod_clase,
				p.titulo,
				p.cod_estado,
				nom_estado = (select x.nom_estado from Estados x where x.cod_estado = p.cod_estado),
				p.fe_estado,
				p.pet_regulatorio,
				p.pet_projid,
				p.pet_projsubid,
				p.pet_projsubsid,
				p.fe_ini_plan,
				p.fe_fin_plan,
				null,
				DATEDIFF(DAY, p.fe_estado, getdate())	as 'dias'
			from
				Peticion p inner join 
				PeticionGrupo pg on (p.pet_nrointerno = pg.pet_nrointerno and pg.cod_grupo <> @grupo_homologacion)
			where 
				((@nivel is null or RTRIM(@nivel)='NULL') or
				(@nivel='DIRE' and RTRIM(pg.cod_direccion)=RTRIM(@area)) or
				(@nivel='GERE' and RTRIM(pg.cod_gerencia)=RTRIM(@area)) or
				(@nivel='SECT' and RTRIM(pg.cod_sector)=RTRIM(@area)) or
				(@nivel='GRUP' and RTRIM(pg.cod_grupo)=RTRIM(@area))) and
				p.cod_estado = 'SUSPEN' and
				DATEDIFF(DAY, p.fe_estado, getdate()) > 60
		end

	-- *****************************************************************************************
	-- 11. Peticiones con TEST por m�s de 60 d�as, sin importar el estado
	-- *****************************************************************************************
	if @opcion = 11
		begin
			INSERT INTO #temporalInforme 
			select
				RTRIM(pg.cod_direccion) + '|' + RTRIM(pg.cod_gerencia) + '|' + RTRIM(pg.cod_sector) + '|' + RTRIM(pg.cod_grupo)	as agrupamiento,
				pg.cod_direccion,
				nom_direccion = (select x.nom_direccion from Direccion x where x.cod_direccion = pg.cod_direccion),
				pg.cod_gerencia,
				nom_gerencia = (select x.nom_gerencia from Gerencia x where x.cod_gerencia = pg.cod_gerencia),
				pg.cod_sector,
				nom_sector = (select x.nom_sector from Sector x where x.cod_sector = pg.cod_sector),
				pg.cod_grupo,
				nom_grupo = (select x.nom_grupo from Grupo x where x.cod_grupo = pg.cod_grupo),
				p.pet_nrointerno,
				p.pet_nroasignado,
				p.cod_tipo_peticion,
				p.cod_clase,
				p.titulo,
				p.cod_estado,
				nom_estado = (select x.nom_estado from Estados x where x.cod_estado = p.cod_estado),
				p.fe_estado,
				p.pet_regulatorio,
				p.pet_projid,
				p.pet_projsubid,
				p.pet_projsubsid,
				p.fe_ini_plan,
				p.fe_fin_plan,
				null,
				'dias' = (select DATEDIFF(DAY, MAX(pc.audit_date), getdate())
						  from PeticionConf pc
						  where 
							pc.pet_nrointerno = p.pet_nrointerno and
							pc.ok_tipo = 'TEST'
						  group by pc.pet_nrointerno)
			from
				Peticion p inner join 
				PeticionGrupo pg on (p.pet_nrointerno = pg.pet_nrointerno and pg.cod_grupo <> @grupo_homologacion)
			where 
				((@nivel is null or RTRIM(@nivel)='NULL') or
				(@nivel='DIRE' and RTRIM(pg.cod_direccion)=RTRIM(@area)) or
				(@nivel='GERE' and RTRIM(pg.cod_gerencia)=RTRIM(@area)) or
				(@nivel='SECT' and RTRIM(pg.cod_sector)=RTRIM(@area)) or
				(@nivel='GRUP' and RTRIM(pg.cod_grupo)=RTRIM(@area))) and
				p.pet_nrointerno in (
					select pc.pet_nrointerno
					from PeticionConf pc
					where 
						pc.pet_nrointerno = p.pet_nrointerno and
						pc.ok_tipo = 'TEST'
					group by pc.pet_nrointerno
					having DATEDIFF(DAY, MAX(pc.audit_date), getdate()) > 60 
				)
		end

	-- ***********************************************************************************************
	-- 12. Peticiones de clase "Correctivo", "Atenci�n de usuario" y "Spufi" en estado "En ejecuci�n" 
	--	   con m�s de 50 horas cargadas.
	-- ***********************************************************************************************
	if @opcion = 12
		begin
			INSERT INTO #temporalInforme 
			select
				RTRIM(pg.cod_direccion) + '|' + RTRIM(pg.cod_gerencia) + '|' + RTRIM(pg.cod_sector) + '|' + RTRIM(pg.cod_grupo)	as agrupamiento,
				pg.cod_direccion,
				nom_direccion = (select x.nom_direccion from Direccion x where x.cod_direccion = pg.cod_direccion),
				pg.cod_gerencia,
				nom_gerencia = (select x.nom_gerencia from Gerencia x where x.cod_gerencia = pg.cod_gerencia),
				pg.cod_sector,
				nom_sector = (select x.nom_sector from Sector x where x.cod_sector = pg.cod_sector),
				pg.cod_grupo,
				nom_grupo = (select x.nom_grupo from Grupo x where x.cod_grupo = pg.cod_grupo),
				p.pet_nrointerno,
				p.pet_nroasignado,
				p.cod_tipo_peticion,
				p.cod_clase,
				p.titulo,
				p.cod_estado,
				nom_estado = (select x.nom_estado from Estados x where x.cod_estado = p.cod_estado),
				p.fe_estado,
				p.pet_regulatorio,
				p.pet_projid,
				p.pet_projsubid,
				p.pet_projsubsid,
				p.fe_ini_plan,
				p.fe_fin_plan,
				null,
				horas = (select SUM(convert(decimal(10,2),ht.horas) / convert(decimal(10,2),60)) 
						 from HorasTrabajadas ht 
						 where ht.pet_nrointerno = p.pet_nrointerno group by ht.pet_nrointerno)
			from
				Peticion p inner join 
				PeticionGrupo pg on (p.pet_nrointerno = pg.pet_nrointerno and pg.cod_grupo <> @grupo_homologacion)
			where 
				((@nivel is null or RTRIM(@nivel)='NULL') or
				(@nivel='DIRE' and RTRIM(pg.cod_direccion)=RTRIM(@area)) or
				(@nivel='GERE' and RTRIM(pg.cod_gerencia)=RTRIM(@area)) or
				(@nivel='SECT' and RTRIM(pg.cod_sector)=RTRIM(@area)) or
				(@nivel='GRUP' and RTRIM(pg.cod_grupo)=RTRIM(@area))) and
				p.cod_estado = 'EJECUC' and
				p.pet_nrointerno in (
					select ht.pet_nrointerno
					from HorasTrabajadas ht
					where 
						ht.pet_nrointerno = p.pet_nrointerno
					group by ht.pet_nrointerno
					having SUM(ht.horas) > 3000
				) and 
				charindex(p.cod_clase,'CORR|ATEN|SPUF|') > 0
		end
	
	-- ***********************************************************************************************
	-- 13. Peticiones de clase "Spufi" en estado "En ejecuci�n" con m�s de 15 d�as desde su fecha de
	--	   inicio real.
	-- ***********************************************************************************************
	if @opcion = 13
		begin
			INSERT INTO #temporalInforme 
			select
				RTRIM(pg.cod_direccion) + '|' + RTRIM(pg.cod_gerencia) + '|' + RTRIM(pg.cod_sector) + '|' + RTRIM(pg.cod_grupo)	as agrupamiento,
				pg.cod_direccion,
				nom_direccion = (select x.nom_direccion from Direccion x where x.cod_direccion = pg.cod_direccion),
				pg.cod_gerencia,
				nom_gerencia = (select x.nom_gerencia from Gerencia x where x.cod_gerencia = pg.cod_gerencia),
				pg.cod_sector,
				nom_sector = (select x.nom_sector from Sector x where x.cod_sector = pg.cod_sector),
				pg.cod_grupo,
				nom_grupo = (select x.nom_grupo from Grupo x where x.cod_grupo = pg.cod_grupo),
				p.pet_nrointerno,
				p.pet_nroasignado,
				p.cod_tipo_peticion,
				p.cod_clase,
				p.titulo,
				p.cod_estado,
				nom_estado = (select x.nom_estado from Estados x where x.cod_estado = p.cod_estado),
				p.fe_estado,
				p.pet_regulatorio,
				p.pet_projid,
				p.pet_projsubid,
				p.pet_projsubsid,
				p.fe_ini_plan,
				p.fe_fin_plan,
				null,
				DATEDIFF(DAY, p.fe_ini_real, getdate())	as 'dias'
			from
				Peticion p inner join 
				PeticionGrupo pg on (p.pet_nrointerno = pg.pet_nrointerno and pg.cod_grupo <> @grupo_homologacion)
			where 
				((@nivel is null or RTRIM(@nivel)='NULL') or
				(@nivel='DIRE' and RTRIM(pg.cod_direccion)=RTRIM(@area)) or
				(@nivel='GERE' and RTRIM(pg.cod_gerencia)=RTRIM(@area)) or
				(@nivel='SECT' and RTRIM(pg.cod_sector)=RTRIM(@area)) or
				(@nivel='GRUP' and RTRIM(pg.cod_grupo)=RTRIM(@area))) and
				p.cod_estado = 'EJECUC' and
				p.cod_clase = 'SPUF' and 
				DATEDIFF(DAY, p.fe_ini_real, getdate()) > 15
		end

	-- ***********************************************************************************************
	-- 14. Peticiones "activas" c/ OMAs pendientes.
	-- ***********************************************************************************************
	if @opcion = 14
		begin
			INSERT INTO #temporalInforme 
			select
				RTRIM(pg.cod_direccion) + '|' + RTRIM(pg.cod_gerencia) + '|' + RTRIM(pg.cod_sector) + '|' + RTRIM(pg.cod_grupo)	as agrupamiento,
				pg.cod_direccion,
				nom_direccion = (select x.nom_direccion from Direccion x where x.cod_direccion = pg.cod_direccion),
				pg.cod_gerencia,
				nom_gerencia = (select x.nom_gerencia from Gerencia x where x.cod_gerencia = pg.cod_gerencia),
				pg.cod_sector,
				nom_sector = (select x.nom_sector from Sector x where x.cod_sector = pg.cod_sector),
				pg.cod_grupo,
				nom_grupo = (select x.nom_grupo from Grupo x where x.cod_grupo = pg.cod_grupo),
				p.pet_nrointerno,
				p.pet_nroasignado,
				p.cod_tipo_peticion,
				p.cod_clase,
				p.titulo,
				p.cod_estado,
				nom_estado = (select x.nom_estado from Estados x where x.cod_estado = p.cod_estado),
				p.fe_estado,
				p.pet_regulatorio,
				p.pet_projid,
				p.pet_projsubid,
				p.pet_projsubsid,
				p.fe_ini_plan,
				p.fe_fin_plan,
				null,
				null
			from
				Peticion p inner join 
				PeticionGrupo pg on (p.pet_nrointerno = pg.pet_nrointerno and pg.cod_grupo <> @grupo_homologacion)
			where 
				((@nivel is null or RTRIM(@nivel)='NULL') or
				(@nivel='DIRE' and RTRIM(pg.cod_direccion)=RTRIM(@area)) or
				(@nivel='GERE' and RTRIM(pg.cod_gerencia)=RTRIM(@area)) or
				(@nivel='SECT' and RTRIM(pg.cod_sector)=RTRIM(@area)) or
				(@nivel='GRUP' and RTRIM(pg.cod_grupo)=RTRIM(@area))) and
				charindex(p.cod_estado,'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|') = 0 and		-- upd -001- a. S�lo activas.
				p.pet_nrointerno in (
					select d.pet_nrointerno
					from PeticionInfohd d
					where d.pet_nrointerno = p.pet_nrointerno 
					group by d.pet_nrointerno
					having MAX(d.info_valor) = 1)
		end

	-- ************************************************************************************************************************************************************************
	-- Final
	-- ************************************************************************************************************************************************************************
	select 
		agrupamiento,
		cod_direccion,
		nom_direccion,
		cod_gerencia,
		nom_gerencia,
		cod_sector,
		nom_sector,
		cod_grupo,
		nom_grupo,
		pet_nrointerno,
		pet_nroasignado,
		cod_tipo_peticion,
		cod_clase,
		titulo,
		cod_estado,
		nom_estado,
		fe_estado,
		regulatorio,
		projid,
		projsubid,
		projsubsid,
		fe_ini_plan,
		fe_fin_plan,
		fe_ultima_carga,
		dias
	from #temporalInforme

go

grant execute on dbo.sp_rptPeticionesVarios1 to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
