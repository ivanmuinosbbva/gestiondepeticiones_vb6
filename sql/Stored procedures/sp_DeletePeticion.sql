/*
-001- a. FJS 18.03.2009 - Se actualiza el SP para contemplar todas las tablas que involucran a peticiones.
-002- a. FJS 20.01.2011 - Faltaría eliminar los registros de HistorialMemo.
-003- a. FJS 24.02.2016 - Nuevo: se agrega la parte de BPE.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeticion'
go

if exists (select * from sysobjects where name = 'sp_DeletePeticion' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeticion
go

create procedure dbo.sp_DeletePeticion 
       @pet_nrointerno  int 
as 
	begin tran
		delete from GesPet..PeticionGrupo 
		where pet_nrointerno = @pet_nrointerno 
	 
		delete from GesPet..PeticionSector 
		where pet_nrointerno = @pet_nrointerno 
	 
		delete from GesPet..PeticionMemo 
		where pet_nrointerno = @pet_nrointerno 

		delete from GesPet..PeticionAdjunto
		where pet_nrointerno = @pet_nrointerno 

		delete from GesPet..PeticionConf
		where pet_nrointerno = @pet_nrointerno 

		delete from GesPet..PeticionChangeMan
		where pet_nrointerno = @pet_nrointerno 

		delete from GesPet..PeticionEnviadas
		where pet_nrointerno = @pet_nrointerno 

		delete from GesPet..PeticionRecurso
		where pet_nrointerno = @pet_nrointerno 

		delete from GesPet..Mensajes 
		where pet_nrointerno = @pet_nrointerno 
	 
		delete from GesPet..PeticionPlancab 
		where pet_nrointerno = @pet_nrointerno 
		
		/*
		delete from GesPet..PeticionPlandet 
		where pet_nrointerno = @pet_nrointerno 
		*/
		
		delete HistorialMemo 
		from 
			GesPet..Historial h inner join
			GesPet..HistorialMemo hm on (h.hst_nrointerno = hm.hst_nrointerno)
		where h.pet_nrointerno = @pet_nrointerno

		delete from GesPet..Historial 
		where pet_nrointerno = @pet_nrointerno 

		delete from GesPet..AgrupPetic
		where pet_nrointerno = @pet_nrointerno 

		delete from GesPet..PeticionBeneficios
		where pet_nrointerno = @pet_nrointerno

		delete from GesPet..PeticionKPI
		where pet_nrointerno = @pet_nrointerno

		delete from GesPet..PeticionValidacion
		where pet_nrointerno = @pet_nrointerno

		delete from GesPet..Peticion 
		where pet_nrointerno = @pet_nrointerno 
	commit tran
return(0) 
go

grant execute on dbo.sp_DeletePeticion to GesPetUsr 
go

print 'Actualización realizada.'
go