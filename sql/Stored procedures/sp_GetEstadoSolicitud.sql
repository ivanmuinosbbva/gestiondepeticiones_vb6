/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 11.08.2014 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetEstadoSolicitud'
go

if exists (select * from sysobjects where name = 'sp_GetEstadoSolicitud' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetEstadoSolicitud
go

create procedure dbo.sp_GetEstadoSolicitud
	@cod_estado	char(1)=''
as
	select 
		p.cod_estado,
		p.nom_estado
	from 
		GesPet.dbo.EstadoSolicitud p
	where
		(@cod_estado is null or p.cod_estado = @cod_estado)
go

grant execute on dbo.sp_GetEstadoSolicitud to GesPetUsr
go

print 'Actualización realizada.'
go
