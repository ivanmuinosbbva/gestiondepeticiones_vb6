/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */
use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePeriodo'
go

if exists (select * from sysobjects where name = 'sp_UpdatePeriodo' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePeriodo
go

create procedure dbo.sp_UpdatePeriodo
	@per_nrointerno	int,
	@per_nombre		char(50),
	@per_tipo		char(1),
	@fe_desde		smalldatetime,
	@fe_hasta		smalldatetime,
	@per_estado		char(6),
	@per_abrev		char(15)
as
	update 
		GesPet.dbo.Periodo
	set
		per_nombre		= @per_nombre,
		per_tipo		= @per_tipo,
		fe_desde		= @fe_desde,
		fe_hasta		= @fe_hasta,
		per_estado		= @per_estado,
		per_ultfchest	= getdate(),
		per_ultusrid	= SUSER_NAME(),
		per_abrev		= @per_abrev
	where 
		per_nrointerno = @per_nrointerno
	return(0)
go

grant execute on dbo.sp_UpdatePeriodo to GesPetUsr
go

print 'Actualización realizada.'
go
