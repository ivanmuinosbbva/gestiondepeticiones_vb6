/*
-000- a. FJS 12.05.2009 - Nuevo SP para manejo de Aplicativos
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertAplicativo'
go

if exists (select * from sysobjects where name = 'sp_InsertAplicativo' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertAplicativo
go

create procedure dbo.sp_InsertAplicativo
	@app_id				char(30)='',
	@app_nombre			char(100)=null,
	@app_hab			char(1)='S',
	@cod_direccion		char(8)=null,
	@cod_gerencia		char(8)=null,
	@cod_sector			char(8)=null,
	@cod_grupo			char(8)=null,
	@cod_r_direccion	char(10)=null,
	@cod_r_gerencia		char(10)=null,
	@cod_r_sector		char(10)=null,
	@cod_r_grupo		char(10)=null,
	@app_amb			char(1)=null,
	@app_abrev			char(10)=null
as
	insert into GesPet..Aplicativo (
		app_id,
		app_nombre,
		app_hab,
		cod_direccion,
		cod_gerencia,
		cod_sector,
		cod_grupo,
		cod_r_direccion,
		cod_r_gerencia,
		cod_r_sector,
		cod_r_grupo,
		app_amb,
		app_fe_alta,
		app_fe_lupd,
		app_abrev)
	values (
		@app_id,
		@app_nombre,
		@app_hab,
		@cod_direccion,
		@cod_gerencia,
		@cod_sector,
		@cod_grupo,
		@cod_r_direccion,
		@cod_r_gerencia,
		@cod_r_sector,
		@cod_r_grupo,
		@app_amb,
		getdate(),
		null,
		@app_abrev)
go

grant execute on dbo.sp_InsertAplicativo to GesPetUsr
go

print 'Actualización realizada.'
