/*
-000- a. FJS dd.mm.yyyy - Nuevo SP para
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateOrientacion'
go

if exists (select * from sysobjects where name = 'sp_UpdateOrientacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateOrientacion
go

create procedure dbo.sp_UpdateOrientacion
	@cod_orientacion char(2),
	@nom_orientacion char(30),
	@flg_habil       char(1),
	@orden           int,
	@descripcion     varchar(255)
as 
	UPDATE Orientacion 
	SET 
		nom_orientacion = @nom_orientacion, 
		flg_habil		= @flg_habil, 
		orden			= @orden, 
		descripcion		= @descripcion
	where
		cod_orientacion = @cod_orientacion
go

grant execute on dbo.sp_UpdateOrientacion to GesPetUsr
go

print 'Actualización realizada.'
go
