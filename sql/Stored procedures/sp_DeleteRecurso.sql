use GesPet
go
print 'sp_DeleteRecurso'
go
if exists (select * from sysobjects where name = 'sp_DeleteRecurso' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteRecurso
go
create procedure dbo.sp_DeleteRecurso 
    @cod_recurso char(10) 
as 
 
declare @flg_cargoarea  char(1) 
 
if exists (select cod_recurso from GesPet..Recurso where RTRIM(cod_recurso) = RTRIM(@cod_recurso)) 
begin 
    select @flg_cargoarea=flg_cargoarea from GesPet..Recurso where RTRIM(cod_recurso) = RTRIM(@cod_recurso) 
    if @flg_cargoarea = 'S' 
    begin 
        raiserror  30015 'El recurso esta a cargo de un area'  
        select 30015 as ErrCode , 'El recurso esta a cargo de un area'  as ErrDesc 
    return (30015) 
    end 
 
    if exists (select cod_recurso from GesPet..Recurso where RTRIM(dlg_recurso) = RTRIM(@cod_recurso)) 
    begin 
        raiserror  30011 'Existen Recursos derivados a este Recurso'  
        select 30011 as ErrCode , 'Existen Recursos derivados a este Recurso'  as ErrDesc 
    return (30011) 
    end 
    /* 
    if exists (select cod_recurso from GesPet..Recurso where RTRIM(cod_recurso) = RTRIM(@dlg_recurso)) 
    begin 
        raiserror  30011 'Existen Recursos dependientes'  
        select 30011 as ErrCode , 'Existen Recursos dependientes'  as ErrDesc 
    return (30011) 
    end 
    if exists (select cod_recurso from GesPet..Peticion where RTRIM(cod_recurso) = RTRIM(@cod_grupo)) 
    begin 
        raiserror  30011 'Existen Peticiones asociadas a este Grupo'  
        select 30011 as ErrCode , 'Existen Peticiones asociadas a este Grupo'  as ErrDesc 
    return (30011) 
    end 
    */ 
    BEGIN TRANSACTION 
    delete GesPet..Recurso 
      where RTRIM(cod_recurso) = RTRIM(@cod_recurso) 
     
    delete GesPet..RecursoPerfil 
      where RTRIM(cod_recurso) = RTRIM(@cod_recurso) 
 
    COMMIT TRANSACTION 
end 
else 
begin 
    raiserror 30001 'Recurso Inexistente'   
    select 30001 as ErrCode , 'Recurso Inexistente' as ErrDesc   
    return (30001)   
end 
return(0) 
go



grant execute on dbo.sp_DeleteRecurso to GesPetUsr 
go


