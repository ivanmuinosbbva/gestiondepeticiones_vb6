/*
-000- a. FJS 13.05.2016 - Nuevo SP para obtener las solicitudes de transmisión de archivos (SSDD).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetSolicitudesSSDD'
go

if exists (select * from sysobjects where name = 'sp_GetSolicitudesSSDD' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetSolicitudesSSDD
go

create procedure dbo.sp_GetSolicitudesSSDD
	@sol_nroasignado 	int=null,
	@sol_recurso		char(10)=null,
	@sol_estado			char(1)=null
as 
	select 
		s.sol_nroasignado,
		s.sol_mask,
		s.sol_fecha,
		s.sol_recurso,
		nom_recurso = (select r.nom_recurso from Recurso r where r.cod_recurso = s.sol_recurso),
		s.jus_codigo,
		jus_descripcion = (select j.jus_descripcion from Justificativos j where j.jus_codigo = s.jus_codigo),
		s.sol_texto,
		s.sol_trnm_fe,
		s.sol_trnm_usr,
		s.pet_nroasignado,
		s.pet_nrointerno,
		dbmsNom = (select db.dbmsNom from DBMS db where db.dbmsId = ct.DBMS),
		s.tablaId,
		ct.nombreTabla,
		ct.baseDeDatos,
		ct.esquema,
		ct.DBMS,
		ct.servidorNombre,
		ct.servidorPuerto,
		s.sol_archivo,
		s.sol_eme,
		s.sol_diferida,
		s.sol_estado,
		nom_estado = (case 
			when s.sol_estado = 'P' then 'Pendiente de proceso'
			when s.sol_estado = 'B' then 'Pendiente de autorizar'
			when s.sol_estado = 'X' then 'Rechazado'
			when s.sol_estado = 'E' then 'En proceso'
			when s.sol_estado = 'K' then 'Finalizado OK'
			when s.sol_estado = 'W' then 'Finalizado con error'
		end),
		s.fechaAprobacion,
		s.aprobador
	from 
		SolicitudesSSDD s inner join
		CatalogoTabla ct on (ct.id = s.tablaId)
	where 
		(@sol_nroasignado is null or s.sol_nroasignado = @sol_nroasignado) AND
		(@sol_recurso is null or s.sol_recurso = @sol_recurso) AND 
		(@sol_estado is null or s.sol_estado = @sol_estado)
	order by 
		s.sol_recurso,
		s.sol_nroasignado
	return(0)
go

grant execute on dbo.sp_GetSolicitudesSSDD to GesPetUsr
go
grant execute on dbo.sp_GetSolicitudesSSDD to TrnCGMEnmascara
go
sp_procxmode 'dbo.sp_GetSolicitudesSSDD','anymode'
go

print 'Actualización realizada.'
go
