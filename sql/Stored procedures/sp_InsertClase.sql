/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.10.2014 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertClase'
go

if exists (select * from sysobjects where name = 'sp_InsertClase' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertClase
go

create procedure dbo.sp_InsertClase
	@cod_clase	char(4),
	@nom_clase	char(40)
as
	insert into GesPet.dbo.Clase (
		cod_clase,
		nom_clase)
	values (
		@cod_clase,
		@nom_clase)
go

grant execute on dbo.sp_InsertClase to GesPetUsr
go

print 'Actualización realizada.'
go
