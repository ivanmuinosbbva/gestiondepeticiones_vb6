/*  C�digo generado autom�ticamente por el programa ~ SQLConstructor v1.1.11 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.12.2010 - Este sp muestra todos aquellos DSN que tienen el mismo nombre productivo a partir de un c�digo DSN concreto.
-001- a. FJS 21.03.2011 - Al  momento de detectar los cat�logos con mismos nombres productivos, truncaremos de la b�squeda el �ltimo calificador cuando termine en:
						  %%odate, Dnnnnnn (siendo nnnnnn cualquier valor num�rico), Fnnnnnn (siendo nnnnnn cualquier valor num�rico).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetHEDT101a'
go

if exists (select * from sysobjects where name = 'sp_GetHEDT101a' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHEDT101a
go

create procedure dbo.sp_GetHEDT101a
	@dsn_id		char(8)=null
as
	declare	@dsn_nomprod	char(50)
	declare @dsn_enmas		char(1)
	
	-- Obtengo el nombre productivo "formateado" del archivo DSN a verificar.
	select 
		@dsn_nomprod = 
		case
			when patindex('%D[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod) > 0 then substring(h1.dsn_nomprod,1,patindex('%D[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod)-1)
			when patindex('%F[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod) > 0 then substring(h1.dsn_nomprod,1,patindex('%F[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod)-1)
			when patindex('%' + '%%ODATE',h1.dsn_nomprod) > 0 then str_replace(h1.dsn_nomprod,'%%ODATE',null)
			else h1.dsn_nomprod
		end,
		@dsn_enmas = h1.dsn_enmas
	from GesPet..HEDT001 h1
	where h1.dsn_id = @dsn_id or @dsn_id is null
	
	--{ add -001- a.
	select
		h1.dsn_id,
		h1.dsn_enmas, 
		h1.dsn_vb,
		h1.dsn_vb_fe,
		h1.dsn_vb_userid,
		valido = 
			case
				when h1.dsn_enmas = 'N' then 'S'
				when ((select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = h1.dsn_id) > 0 and
					  (select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = h1.dsn_id) > 0 and h1.dsn_id not in
					  (select x.dsn_id from GesPet..HEDT003 x where x.dsn_id = h1.dsn_id and x.cpo_nomrut = '' or x.cpo_nomrut is null)) 
					and h1.dsn_enmas = 'S' then 'S'
				else 'N'
			end,
		@dsn_nomprod as 'dsn_nomprodfmt'
	from GesPet..HEDT001 h1
	where (
			--charindex(h1.estado,'C|B|')>0 and
			h1.dsn_nomprod = @dsn_nomprod and 
			h1.dsn_enmas = @dsn_enmas
		) or (
			--charindex(h1.estado,'C|B|')>0 and
			((patindex('%D[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod) > 0 and substring(h1.dsn_nomprod,1,patindex('%D[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod)-1) = @dsn_nomprod) or 
			 (patindex('%F[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod) > 0 and substring(h1.dsn_nomprod,1,patindex('%F[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod)-1) = @dsn_nomprod) or 
			 (patindex('%' + '%%ODATE',h1.dsn_nomprod) > 0 and str_replace(h1.dsn_nomprod,'%%ODATE',null) = @dsn_nomprod)) and
			h1.dsn_enmas = @dsn_enmas
		)
	return(0)
go

grant execute on dbo.sp_GetHEDT101a to GesPetUsr
go

print 'Actualizaci�n realizada.'
go

--substring(h1.dsn_nomprod,1,char_length(h1.dsn_nomprod) - charindex('.',reverse(h1.dsn_nomprod))) = @dsn_nomprod and
--h1.dsn_nomprod like @dsn_nomprod and

	/*
	-- Si no devuelve nada, entonces debe forzar a devolver al menos su propio dsn
	if (
		select
			count(1) from GesPet..HEDT001 h1
		where 
			charindex(h1.estado,'C|B|')>0 and 
			((patindex('%D[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod) > 0 and substring(h1.dsn_nomprod,1,patindex('%D[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod)-1) = @dsn_nomprod) or 
			 (patindex('%F[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod) > 0 and substring(h1.dsn_nomprod,1,patindex('%F[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod)-1) = @dsn_nomprod) or 
			 (patindex('%' + '%%ODATE',h1.dsn_nomprod) > 0 and str_replace(h1.dsn_nomprod,'%%ODATE',null) = @dsn_nomprod)) and
			h1.dsn_enmas = @dsn_enmas
		) = 0
		begin
			select
				h1.dsn_id,
				h1.dsn_enmas, 
				h1.dsn_vb,
				h1.dsn_vb_fe,
				h1.dsn_vb_userid,
				valido = 
					case
						when h1.dsn_enmas = 'N' then 'S'
						when ((select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = h1.dsn_id) > 0 and
							  (select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = h1.dsn_id) > 0 and h1.dsn_id not in
							  (select x.dsn_id from GesPet..HEDT003 x where x.dsn_id = h1.dsn_id and x.cpo_nomrut = '' or x.cpo_nomrut is null)) 
							and h1.dsn_enmas = 'S' then 'S'
						else
							'N'
					end
			from GesPet..HEDT001 h1
			where 
				charindex(h1.estado,'C|B|')>0 and
				h1.dsn_id = @dsn_id and
				h1.dsn_enmas = @dsn_enmas
		end
	else
		begin
			select
				h1.dsn_id,
				h1.dsn_enmas, 
				h1.dsn_vb,
				h1.dsn_vb_fe,
				h1.dsn_vb_userid,
				valido = 
					case
						when h1.dsn_enmas = 'N' then 'S'
						when ((select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = h1.dsn_id) > 0 and
							  (select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = h1.dsn_id) > 0 and h1.dsn_id not in
							  (select x.dsn_id from GesPet..HEDT003 x where x.dsn_id = h1.dsn_id and x.cpo_nomrut = '' or x.cpo_nomrut is null)) 
							and h1.dsn_enmas = 'S' then 'S'
						else
							'N'
					end
			from GesPet..HEDT001 h1
			where 
				--h1.estado = 'C' and					-- del -001- a.
				charindex(h1.estado,'C|B|')>0 and		-- add -001- a.
				((patindex('%D[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod) > 0 and substring(h1.dsn_nomprod,1,patindex('%D[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod)-1) = @dsn_nomprod) or 
				 (patindex('%F[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod) > 0 and substring(h1.dsn_nomprod,1,patindex('%F[0-9][0-9][0-9][0-9][0-9][0-9]',h1.dsn_nomprod)-1) = @dsn_nomprod) or 
				 (patindex('%' + '%%ODATE',h1.dsn_nomprod) > 0 and str_replace(h1.dsn_nomprod,'%%ODATE',null) = @dsn_nomprod)) and
				h1.dsn_enmas = @dsn_enmas
		end
	*/