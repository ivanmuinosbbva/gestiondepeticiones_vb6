/*
-001- a. FJS 02.01.2015 - Se adapta la longitud de las variables al nuevo ancho de las columnas modificadas.
-002- a. FJS 01.11.2016 - Se agrega la posibilidad de utilizar los nombres cortos de las direcciones y gerencias.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_rptEstructura'
go

if exists (select * from sysobjects where name = 'sp_rptEstructura' and sysstat & 7 = 4)
	drop procedure dbo.sp_rptEstructura
go

/* sp_rptEstructura 'MEDIO','DESA','NULL','NULL','2' */
create procedure dbo.sp_rptEstructura 
	 @cod_direccion     char(8)='NULL',
	 @cod_gerencia      char(8)='NULL',
	 @cod_sector		char(8)='NULL',
	 @cod_grupo			char(8)='NULL',
	 @detalle           char(1)='0'
as 
	set nocount on

	if @cod_direccion is null
		set @cod_direccion = 'NULL'
	if @cod_gerencia is null
		set @cod_gerencia = 'NULL'
	if @cod_sector is null
		set @cod_sector = 'NULL'
	if @cod_grupo is null
		set @cod_grupo = 'NULL'
	if @detalle is null
		set @detalle = '0'

	CREATE TABLE #TmpSalida( 
			cod_direccion	char(10)	null, 
			nom_direccion	char(80)	null,
			cod_gerencia	char(10)	null, 
			nom_gerencia	char(80)	null,
			cod_sector		char(10)	null, 
			nom_sector		char(80)	null,
			cod_grupo		char(10)	null, 
			nom_grupo		char(80)	null,
			abrev_direccion varchar(30)	null,
			abrev_gerencia	varchar(30)	null)
	 
		 
		insert #TmpSalida 
		select 
			Di.cod_direccion, 
			Di.nom_direccion, 
			cod_gerencia='', 
			nom_gerencia='', 
			cod_sector='', 
			nom_sector='', 
			cod_grupo='', 
			nom_grupo='',
			Di.abrev_direccion,
			abrev_gerencia=''
		from 
			GesPet..Direccion Di 
		where   
			((RTRIM(@cod_direccion)='NULL' or RTRIM(Di.cod_direccion)=RTRIM(@cod_direccion)) and ISNULL(Di.flg_habil,'S')<>'N') 
	 
		insert #TmpSalida 
		select  
			Di.cod_direccion, 
			Di.nom_direccion, 
			Ge.cod_gerencia, 
			Ge.nom_gerencia, 
			cod_sector='', 
			nom_sector='', 
			cod_grupo='', 
			nom_grupo='',
			Di.abrev_direccion,
			Ge.abrev_gerencia
		from 
			GesPet..Direccion Di, 
			GesPet..Gerencia Ge 
		where   
			((RTRIM(@cod_direccion)='NULL' or RTRIM(Di.cod_direccion)=RTRIM(@cod_direccion)) and ISNULL(Di.flg_habil,'S')<>'N') and 
			((RTRIM(@cod_gerencia)='NULL' or RTRIM(Ge.cod_gerencia)=RTRIM(@cod_gerencia)) and ISNULL(Ge.flg_habil,'S')<>'N') and  
			(RTRIM(Di.cod_direccion) = RTRIM(Ge.cod_direccion)) 
	 
		insert #TmpSalida 
		select  
			Di.cod_direccion, 
			Di.nom_direccion, 
			Ge.cod_gerencia, 
			Ge.nom_gerencia, 
			Gr.cod_sector, 
			Gr.nom_sector, 
			cod_grupo='', 
			nom_grupo='',
			Di.abrev_direccion,
			Ge.abrev_gerencia
		from 
			GesPet..Direccion Di, 
			GesPet..Gerencia Ge, 
			GesPet..Sector Gr 
		where   
			((RTRIM(@cod_direccion)='NULL' or RTRIM(Di.cod_direccion)=RTRIM(@cod_direccion)) and ISNULL(Di.flg_habil,'S')<>'N') and 
			((RTRIM(@cod_gerencia)='NULL' or RTRIM(Ge.cod_gerencia)=RTRIM(@cod_gerencia)) and ISNULL(Ge.flg_habil,'S')<>'N') and  
			((RTRIM(@cod_sector)='NULL' or RTRIM(Gr.cod_sector)=RTRIM(@cod_sector)) and ISNULL(Gr.flg_habil,'S')<>'N') and 
			(RTRIM(Di.cod_direccion) = RTRIM(Ge.cod_direccion)) and 
			(RTRIM(Ge.cod_gerencia) = RTRIM(Gr.cod_gerencia)) 
	 
		insert #TmpSalida 
		select  
			Di.cod_direccion, 
			Di.nom_direccion, 
			Ge.cod_gerencia, 
			Ge.nom_gerencia, 
			Gr.cod_sector, 
			Gr.nom_sector, 
			Su.cod_grupo, 
			Su.nom_grupo,
			Di.abrev_direccion,
			Ge.abrev_gerencia
		from 
			GesPet..Direccion Di, 
			GesPet..Gerencia Ge, 
			GesPet..Sector Gr, 
			GesPet..Grupo Su 
		where   
			((RTRIM(@cod_direccion)='NULL' or RTRIM(Di.cod_direccion)=RTRIM(@cod_direccion)) and ISNULL(Di.flg_habil,'S')<>'N') and 
			((RTRIM(@cod_gerencia)='NULL' or RTRIM(Ge.cod_gerencia)=RTRIM(@cod_gerencia)) and ISNULL(Ge.flg_habil,'S')<>'N') and  
			((RTRIM(@cod_sector)='NULL' or RTRIM(Gr.cod_sector)=RTRIM(@cod_sector)) and ISNULL(Gr.flg_habil,'S')<>'N') and 
			((RTRIM(@cod_grupo)='NULL' or RTRIM(Su.cod_grupo)=RTRIM(@cod_grupo)) and ISNULL(Su.flg_habil,'S')<>'N') and  
			(RTRIM(Di.cod_direccion) = RTRIM(Ge.cod_direccion)) and 
			(RTRIM(Ge.cod_gerencia) = RTRIM(Gr.cod_gerencia)) and 
			(RTRIM(Su.cod_sector) = RTRIM(Gr.cod_sector)) 
	 
	/* 
	select * from #TmpSalida 
		order by cod_direccion, 
			 cod_gerencia, 
			 cod_sector, 
			 cod_grupo 
	*/ 

	-- Perfiles
	if @detalle='0' 
		select  Ts.cod_direccion, 
			Ts.nom_direccion, 
			Ts.cod_gerencia, 
			Ts.nom_gerencia, 
			Ts.cod_sector, 
			Ts.nom_sector, 
			Ts.cod_grupo, 
			Ts.nom_grupo, 
			Re.cod_recurso, 
			Re.nom_recurso, 
			Re.flg_cargoarea, 
			Rp.cod_perfil, 
			nom_perfil = (select Pe.nom_perfil from GesPet..Perfil Pe where RTRIM(Rp.cod_perfil) = RTRIM(Pe.cod_perfil)), 
			Rp.cod_nivel, 
			Rp.cod_area 
		from GesPet..Recurso Re, #TmpSalida Ts, 
			GesPet..RecursoPerfil Rp 
		where 
			(RTRIM(@cod_direccion)='NULL' or RTRIM(Ts.cod_direccion) = RTRIM(@cod_direccion)) and 
			(RTRIM(@cod_gerencia)='NULL' or RTRIM(Ts.cod_gerencia) = RTRIM(@cod_gerencia)) and  
			(RTRIM(@cod_sector)='NULL' or RTRIM(Ts.cod_sector) = RTRIM(@cod_sector)) and 
			(RTRIM(@cod_grupo)='NULL' or RTRIM(Ts.cod_grupo) = RTRIM(@cod_grupo)) and  
			(RTRIM(Re.estado_recurso) = 'A') and  
			((RTRIM(Ts.cod_direccion) is null and RTRIM(Re.cod_direccion) is null) or RTRIM(Ts.cod_direccion) = RTRIM(Re.cod_direccion)) and 
			((RTRIM(Ts.cod_gerencia) is null and RTRIM(Re.cod_gerencia) is null) or RTRIM(Ts.cod_gerencia) = RTRIM(Re.cod_gerencia)) and 
			((RTRIM(Ts.cod_sector) is null and RTRIM(Re.cod_sector) is null) or RTRIM(Ts.cod_sector) = RTRIM(Re.cod_sector)) and 
			((RTRIM(Ts.cod_grupo) is null and RTRIM(Re.cod_grupo) is null) or RTRIM(Ts.cod_grupo) = RTRIM(Re.cod_grupo)) and 
			(RTRIM(Re.cod_recurso) *= RTRIM(Rp.cod_recurso))
		UNION ALL 
		select  Ts.cod_direccion, 
			Ts.nom_direccion, 
			Ts.cod_gerencia, 
			Ts.nom_gerencia, 
			Ts.cod_sector, 
			Ts.nom_sector, 
			Ts.cod_grupo, 
			Ts.nom_grupo, 
			cod_recurso='', 
			nom_recurso='', 
			flg_cargoarea='X', 
			cod_perfil='', 
			nom_perfil='', 
			cod_nivel='', 
			cod_area='' 
			from #TmpSalida Ts 
		order by Ts.cod_direccion, 
			 Ts.cod_gerencia, 
			 Ts.cod_sector, 
			 Ts.cod_grupo, 
			 flg_cargoarea desc, 
			 nom_recurso asc 
	
	-- Recursos
	if @detalle='1' 
		select  Ts.cod_direccion, 
			Ts.nom_direccion, 
			Ts.cod_gerencia, 
			Ts.nom_gerencia, 
			Ts.cod_sector, 
			Ts.nom_sector, 
			Ts.cod_grupo, 
			Ts.nom_grupo, 
			Re.cod_recurso, 
			Re.nom_recurso, 
			Re.flg_cargoarea, 
			cod_perfil='', 
			nom_perfil='', 
			cod_nivel='', 
			cod_area='' 
			from GesPet..Recurso Re, #TmpSalida Ts 
		where 
			(RTRIM(@cod_direccion)='NULL' or RTRIM(Ts.cod_direccion) = RTRIM(@cod_direccion)) and 
			(RTRIM(@cod_gerencia)='NULL' or RTRIM(Ts.cod_gerencia) = RTRIM(@cod_gerencia)) and  
			(RTRIM(@cod_sector)='NULL' or RTRIM(Ts.cod_sector) = RTRIM(@cod_sector)) and 
			(RTRIM(@cod_grupo)='NULL' or RTRIM(Ts.cod_grupo) = RTRIM(@cod_grupo)) and  
			(RTRIM(Re.estado_recurso) = 'A') and  
			((RTRIM(Ts.cod_direccion) is null and RTRIM(Re.cod_direccion) is null) or RTRIM(Ts.cod_direccion) = RTRIM(Re.cod_direccion)) and 
			((RTRIM(Ts.cod_gerencia) is null and RTRIM(Re.cod_gerencia) is null) or RTRIM(Ts.cod_gerencia) = RTRIM(Re.cod_gerencia)) and 
			((RTRIM(Ts.cod_sector) is null and RTRIM(Re.cod_sector) is null) or RTRIM(Ts.cod_sector) = RTRIM(Re.cod_sector)) and 
			((RTRIM(Ts.cod_grupo) is null and RTRIM(Re.cod_grupo) is null) or RTRIM(Ts.cod_grupo) = RTRIM(Re.cod_grupo)) 
		UNION ALL 
		select  Ts.cod_direccion, 
			Ts.nom_direccion, 
			Ts.cod_gerencia, 
			Ts.nom_gerencia, 
			Ts.cod_sector, 
			Ts.nom_sector, 
			Ts.cod_grupo, 
			Ts.nom_grupo, 
			cod_recurso='', 
			nom_recurso='', 
			flg_cargoarea='X', 
			cod_perfil='', 
			nom_perfil='', 
			cod_nivel='', 
			cod_area='' 
			from #TmpSalida Ts 
		order by Ts.cod_direccion, 
			 Ts.cod_gerencia, 
			 Ts.cod_sector, 
			 Ts.cod_grupo, 
			 flg_cargoarea desc, 
			 nom_recurso asc 
	
	-- Areas y BP
	if @detalle='2' 
	begin
		--update #TmpSalida
		--set cod_recurso = Sec.cod_bpar
		--from Sector Sec, #TmpSalida Ts
		--where Ts.cod_sector is not null and
		--Ts.cod_grupo='' and
		--RTRIM(Sec.cod_sector) = RTRIM(Ts.cod_sector)

		select  Ts.cod_direccion, 
			Ts.nom_direccion, 
			Ts.cod_gerencia, 
			Ts.nom_gerencia, 
			Ts.cod_sector, 
			Ts.nom_sector, 
			Ts.cod_grupo, 
			Ts.nom_grupo, 
			cod_recurso=isnull(Sec.cod_bpar,''),  
			nom_recurso=isnull((select Rec.nom_recurso from Recurso Rec where RTRIM(Rec.cod_recurso) =* RTRIM(Sec.cod_bpar)),''),  
			flg_cargoarea='X',  
			cod_perfil='',  
			nom_perfil='',  
			cod_nivel='',  
			cod_area='',
			Ts.abrev_direccion,
			Ts.abrev_gerencia
		from #TmpSalida Ts , Sector Sec  
		where 	
			Ts.cod_sector<>'' and 
			Ts.cod_grupo='' and 
			RTRIM(Sec.cod_sector) =* RTRIM(Ts.cod_sector) 
		UNION ALL
		select  Ts.cod_direccion, 
			Ts.nom_direccion, 
			Ts.cod_gerencia, 
			Ts.nom_gerencia, 
			Ts.cod_sector, 
			Ts.nom_sector, 
			Ts.cod_grupo, 
			Ts.nom_grupo, 
			cod_recurso='', 
			nom_recurso='', 
			flg_cargoarea='X', 
			cod_perfil='', 
			nom_perfil='', 
			cod_nivel='', 
			cod_area='',
			Ts.abrev_direccion,
			Ts.abrev_gerencia
		from #TmpSalida Ts
		where 
			Ts.cod_sector='' or
			Ts.cod_grupo<>''
		order by Ts.cod_direccion, 
			 Ts.cod_gerencia, 
			 Ts.cod_sector, 
			 Ts.cod_grupo 
	 end
	/* 
	DROP TABLE #TmpSalida 
	*/ 

	if @detalle = '3'
		begin
			select  
				Ts.cod_direccion, 
				Ts.nom_direccion, 
				Ts.cod_gerencia, 
				Ts.nom_gerencia, 
				Ts.cod_sector, 
				Ts.nom_sector, 
				Ts.cod_grupo, 
				Ts.nom_grupo, 
				r.cod_recurso,
				r.nom_recurso,
				flg_cargoarea,
				cod_perfil='',  
				nom_perfil='',  
				cod_nivel='',  
				cod_area='' 
			from 
				#TmpSalida Ts inner join 
				Recurso r on (
					Ts.cod_direccion = r.cod_direccion and 
					Ts.cod_gerencia = r.cod_gerencia and 
					Ts.cod_sector = r.cod_sector and 
					Ts.cod_grupo = r.cod_grupo)
			where r.flg_cargoarea = 'S'
			order by 
				Ts.cod_direccion, 
				Ts.cod_gerencia, 
				Ts.cod_sector, 
				Ts.cod_grupo 
		end
	return(0) 
go

grant execute on dbo.sp_rptEstructura to GesPetUsr 
go

print 'Actualización realizada.'
go