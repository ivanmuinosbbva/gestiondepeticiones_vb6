/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.10.2014 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetClase'
go

if exists (select * from sysobjects where name = 'sp_GetClase' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetClase
go

create procedure dbo.sp_GetClase
	@cod_clase	char(4)=NULL
as
	select 
		p.cod_clase,
		p.nom_clase
	from 
		GesPet.dbo.Clase p
	where
		(@cod_clase is NULL or p.cod_clase = @cod_clase)
go

grant execute on dbo.sp_GetClase to GesPetUsr
go

print 'Actualización realizada.'
go
