use GesPet
go
print 'sp_DelAgrupAgrup'
go
if exists (select * from sysobjects where name = 'sp_DelAgrupAgrup' and sysstat & 7 = 4)
drop procedure dbo.sp_DelAgrupAgrup
go
create procedure dbo.sp_DelAgrupAgrup 
	@agr_nrointerno		int=0,
	@agr_nropadre		int=0
as 

	if not exists (select agr_nrointerno from Agrup where agr_nrointerno=@agr_nrointerno and agr_nropadre=@agr_nropadre)   
	begin 
		select 30010 as ErrCode , 'Error: No existe tal relacion de agrupamientos' as ErrDesc 
		raiserror  30010 'Error: No existe tal relacion de agrupamientos'  
		return (30010) 
	end 
 
	update Agrup set 
		agr_nropadre = 0
	where  agr_nrointerno=@agr_nrointerno and agr_nropadre=@agr_nropadre
 
return(0)   
go



grant execute on dbo.sp_DelAgrupAgrup to GesPetUsr 
go
