use GesPet
go
print 'sp_GetAgrupPeticHijo'
go
if exists (select * from sysobjects where name = 'sp_GetAgrupPeticHijo' and sysstat & 7 = 4)
drop procedure dbo.sp_GetAgrupPeticHijo
go
/*
devuelve todos los nodos de AgrupPetic que tenga la peticion @pet_nrointerno y que
cuelguen de la raiz @agr_nroraiz
*/
create procedure dbo.sp_GetAgrupPeticHijo 
             @agr_nroraiz   int=0,
             @pet_nrointerno   int=0
as 

declare @agr_nrointernoX int
declare @agr_nrointernoY int
declare @agr_nropadreX int
declare @agr_candidato int

create table #CursLista (agr_nrointerno	int)

create table #CursPadre (agr_nrointerno	int)

create table #CursHijos (agr_nrointerno	int)

/* busca todos los nodos AgrupPetic que tiene esa peticion */
insert into #CursPadre
	select Ag.agr_nrointerno
	from   AgrupPetic Ag
	where  Ag.pet_nrointerno = @pet_nrointerno

set rowcount 1
while (exists (select agr_nrointerno from #CursPadre))
begin
	/* con cada nodo encontrado */
	select @agr_candidato = agr_nrointerno from #CursPadre


	/* busca el padre raiz */
	set rowcount 0
	delete #CursLista
	set rowcount 1

	select @agr_nrointernoY=@agr_candidato
	select @agr_nropadreX=agr_nropadre,@agr_nrointernoX=agr_nrointerno from Agrup where @agr_nrointernoY=agr_nrointerno
	while ((@agr_nropadreX is not null) and (@agr_nropadreX > 0))
	begin
		/* print '%1!', @agr_nropadreX */
		
		if exists (select agr_nrointerno from #CursLista where agr_nrointerno=@agr_nropadreX)
		begin
			drop table #CursHijos
			drop table #CursPadre
			drop table #CursLista
			/*print '%1! ,RECURSIVO' , @agr_nropadreX */
			raiserror 30011 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO'   
			select 30011 as ErrCode , 'EL ARBOL DE AGRUPAMIENTOS SOLICITADO ES RECURSIVO' as ErrDesc   
			return (30011)   
		end
	
		insert into #CursLista 	(agr_nrointerno) values (@agr_nropadreX)

		select @agr_nrointernoY=@agr_nropadreX
		select @agr_nropadreX=agr_nropadre,@agr_nrointernoX=agr_nrointerno from Agrup where @agr_nrointernoY=agr_nrointerno
	end
	
	
	/* si el raiz es el que estoy buscando lo insertamos */
	if @agr_nrointernoY=@agr_nroraiz
	begin
		insert into #CursHijos 
			(agr_nrointerno)
		values 
			(@agr_candidato)
	end

	delete #CursPadre
end
set rowcount 0

select  CH.agr_nrointerno,
	agr_titulo = (select AG.agr_titulo from Agrup AG where AG.agr_nrointerno=CH.agr_nrointerno)
from 	#CursHijos CH
order by agr_titulo

drop table #CursHijos
drop table #CursPadre
drop table #CursLista

return(0) 
go


grant execute on dbo.sp_GetAgrupPeticHijo to GesPetUsr 
go
