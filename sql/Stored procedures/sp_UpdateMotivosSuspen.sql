/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateMotivosSuspen'
go

if exists (select * from sysobjects where name = 'sp_UpdateMotivosSuspen' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateMotivosSuspen
go

create procedure dbo.sp_UpdateMotivosSuspen
	@cod_motivo_suspen	int,
	@nom_motivo_suspen	char(50),
	@estado_hab			char(1)
as
	update 
		GesPet.dbo.MotivosSuspen
	set
		nom_motivo_suspen = @nom_motivo_suspen,
		estado_hab		  = @estado_hab
	where 
		(cod_motivo_suspen = @cod_motivo_suspen or @cod_motivo_suspen is null)
	return(0)
go

grant execute on dbo.sp_UpdateMotivosSuspen to GesPetUsr
go

print 'Actualización realizada.'
