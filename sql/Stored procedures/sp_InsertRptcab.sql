/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 24.11.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertRptcab'
go

if exists (select * from sysobjects where name = 'sp_InsertRptcab' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertRptcab
go

create procedure dbo.sp_InsertRptcab
	@rptid		char(10),
	@rptnom		char(50),
	@rpthab		char(1),
	@rptfch		smalldatetime
as
	insert into GesPet.dbo.Rptcab (
		rptid,
		rptnom,
		rpthab,
		rptfch)
	values (
		@rptid,
		@rptnom,
		@rpthab,
		@rptfch)
go

grant execute on dbo.sp_InsertRptcab to GesPetUsr
go

print 'Actualización realizada.'
go
