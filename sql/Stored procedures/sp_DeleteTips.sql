/*
-000- a. FJS 24.11.2008 - Nuevo SP para eliminar un TIP.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteTips'
go

if exists (select * from sysobjects where name = 'sp_DeleteTips' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteTips
go

create procedure dbo.sp_DeleteTips
	@tips_version	char(10),
	@tips_tipo		char(6),
	@tips_nro		int,
	@tips_rng		int
as
	delete from
		GesPet..Tips
	where
		(tips_version = @tips_version or @tips_version is null) and 
		(tips_tipo = @tips_tipo or @tips_tipo is null) and 
		(tips_nro = @tips_nro or @tips_nro is null) and 
		(tips_rng = @tips_rng or @tips_rng is null) 
go

grant execute on dbo.sp_DeleteTips to GesPetUsr
go

print 'Actualización realizada.'
