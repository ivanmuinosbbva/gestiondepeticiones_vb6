/*
-001- a. FJS 13.01.2009 - Optimización del SP para mejorar la performance del aplicativo.
-002- a. FJS 04.09.2012 - Se indican las horas cargadas hasta el momento.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetPeticionRecursoHoras'
go

if exists (select * from sysobjects where name = 'sp_GetPeticionRecursoHoras' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetPeticionRecursoHoras
go

create procedure dbo.sp_GetPeticionRecursoHoras 
    @pet_nrointerno     int = 0, 
    @cod_grupo          char(8) = null
as 
	begin 
		select  
			Ps.pet_nrointerno, 
			Ps.cod_recurso, 
			nom_recurso = (select R.nom_recurso from Recurso R where R.cod_recurso=Ps.cod_recurso),
			Ps.cod_grupo, 
			Ps.cod_sector,           
			Ps.cod_gerencia,         
			Ps.cod_direccion,
			horas = (
			case
				when Pg.horaspresup > 0 then isnull((
					(convert(numeric(10,2), (select sum(horas)/60
					 from GesPet..HorasTrabajadas
					 where pet_nrointerno = Ps.pet_nrointerno and cod_recurso = Ps.cod_recurso)))/
					 convert(numeric(10,2),Pg.horaspresup)),0)
				else 0
			end)
		from 
			GesPet..PeticionRecurso Ps inner join
			GesPet..PeticionGrupo Pg on (Ps.pet_nrointerno = Pg.pet_nrointerno and Ps.cod_grupo = Pg.cod_grupo)
		where   
			(@pet_nrointerno = 0 or @pet_nrointerno = Ps.pet_nrointerno) and 
			(@cod_grupo is null or RTRIM(@cod_grupo) is null or RTRIM(@cod_grupo)='' or RTRIM(Ps.cod_grupo) = RTRIM(@cod_grupo)) 
		order by
			Ps.pet_nrointerno,
			Ps.cod_grupo,
			Ps.cod_recurso
	end 
	return(0)
go

grant execute on dbo.sp_GetPeticionRecursoHoras to GesPetUsr 
go

print 'Actualización realizada.'
go