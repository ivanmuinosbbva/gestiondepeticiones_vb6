/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.13 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 19.08.2010 - Planificación DYD
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertEstadosDesarrollo'
go

if exists (select * from sysobjects where name = 'sp_InsertEstadosDesarrollo' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertEstadosDesarrollo
go

create procedure dbo.sp_InsertEstadosDesarrollo
	@cod_estado_desa	int,
	@nom_estado_desa	char(50),
	@estado_hab			char(1)='S'
as
	insert into GesPet.dbo.EstadosDesarrollo (
		cod_estado_desa,
		nom_estado_desa,
		estado_hab)
	values (
		@cod_estado_desa,
		@nom_estado_desa,
		@estado_hab)
	return(0)
go

grant execute on dbo.sp_InsertEstadosDesarrollo to GesPetUsr
go

print 'Actualización realizada.'
