/*
-000- a. FJS 05.05.2009 - Proyectos IDM
-001- a. FJS 19.09.2011 - Se actualiza para eliminar todos los datos de las tablas de Proyecto IDM.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteProyectoIDM'
go

if exists (select * from sysobjects where name = 'sp_DeleteProyectoIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteProyectoIDM
go

create procedure dbo.sp_DeleteProyectoIDM
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int
as
	-- Tabla: ProyectoIDMAlertas
	delete from
		GesPet..ProyectoIDMAlertas
	where
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId
	
	-- Tabla: ProyectoIDMHitos
	delete from
		GesPet..ProyectoIDMHitos
	where
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId

	-- Tabla: ProyectoIDMMemo
	delete from
		GesPet..ProyectoIDMMemo
	where
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId

	-- Tabla: ProyectoIDMNovedades
	delete from
		GesPet..ProyectoIDMNovedades
	where
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId

	-- Tabla: ProyectoIDM
	delete from
		GesPet..ProyectoIDM
	where
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId
	return(0)
go

grant execute on dbo.sp_DeleteProyectoIDM to GesPetUsr
go

print 'Actualización realizada.'
go
