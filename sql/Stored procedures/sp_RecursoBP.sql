/*
-001- a. FJS 01.10.2008 - Nuevo SP para publicar y ser utilizado por un aplicativo externo a CGM.
-002- a. FJS 09.01.2009 - Se agrega la dirección de email del recurso.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_RecursoBP'
go

if exists (select * from sysobjects where name = 'sp_RecursoBP' and sysstat & 7 = 4)
	drop procedure dbo.sp_RecursoBP
go

create procedure dbo.sp_RecursoBP
	@cod_recurso		char(10)
as 
	select
		SEC.cod_bpar,
		nom_bpar = (select R.nom_recurso from GesPet..Recurso R where R.cod_recurso = SEC.cod_bpar)
		,email = (select R.email from GesPet..Recurso R where R.cod_recurso = SEC.cod_bpar)		-- add -002- a.
	from
		GesPet..Recurso REC left join
		GesPet..Sector SEC on (REC.cod_sector = SEC.cod_sector)
	where
		REC.cod_recurso = @cod_recurso and
		(SEC.cod_bpar is not null or SEC.cod_bpar <> '')
return(0)
go

grant execute on dbo.sp_RecursoBP to GesPetUsr
go

grant execute on dbo.sp_RecursoBP to grpapsolcom
go


print 'Actualización realizada.'
