use GesPet
go
print 'sp_DeleteBalanceSubRubro'
go
if exists (select * from sysobjects where name = 'sp_DeleteBalanceSubRubro' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteBalanceSubRubro
go
create procedure dbo.sp_DeleteBalanceSubRubro 
	@cod_BalanceRubro	char(8)=null,
	@cod_BalanceSubRubro	char(8)=null
as 

if not exists (select cod_BalanceSubRubro from BalanceSubRubro where RTRIM(cod_BalanceRubro)=RTRIM(@cod_BalanceRubro) and RTRIM(cod_BalanceSubRubro)=RTRIM(@cod_BalanceSubRubro))
begin
	raiserror 30001 'SubRubro Inexistente'   
	select 30001 as ErrCode , 'SubRubro Inexistente' as ErrDesc   
	return (30001)   
end 

if exists (select cod_BalanceSubRubro from ProyectoBalanceDet where RTRIM(cod_BalanceRubro)=RTRIM(@cod_BalanceRubro) and RTRIM(cod_BalanceSubRubro)=RTRIM(@cod_BalanceSubRubro))
begin
	raiserror 30001 'Existe una referencia en Proyectos'   
	select 30001 as ErrCode , 'Existe una referencia en Proyectos' as ErrDesc   
	return (30001)   
end 

delete BalanceSubRubro 
where RTRIM(cod_BalanceRubro)=RTRIM(@cod_BalanceRubro) and RTRIM(cod_BalanceSubRubro)=RTRIM(@cod_BalanceSubRubro) 


return(0) 
go

grant execute on dbo.sp_DeleteBalanceSubRubro to GesPetUsr 
go

