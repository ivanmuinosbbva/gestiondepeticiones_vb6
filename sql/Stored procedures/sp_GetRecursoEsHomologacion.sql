/*
-000- a. - FJS 23.04.2008 - Nuevo SP para saber si un determinado recurso pertenece al grupo homologador.
*/

use GesPet
go

print 'Creando/actualizando el SP: sp_GetRecursoEsHomologacion'
go

if exists (select * from sysobjects where name = 'sp_GetRecursoEsHomologacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetRecursoEsHomologacion
go

create procedure dbo.sp_GetRecursoEsHomologacion
    @cod_recurso    char(10)=null
as

declare @cod_grupo_homologador  char(8)

select @cod_grupo_homologador = 
    (select 
        GRP.cod_grupo 
     from
        GesPet.dbo.Grupo GRP
     where
        GRP.grupo_homologacion = 'H'
    )

    select
        REC.cod_recurso,
        REC.nom_recurso,
        REC.vinculo,
        REC.cod_gerencia,
        REC.cod_direccion,
        REC.cod_sector,
        REC.cod_grupo,
        REC.flg_cargoarea,
        REC.estado_recurso,
        REC.horasdiarias,
        REC.observaciones,
        REC.dlg_recurso,
        REC.dlg_desde,
        REC.dlg_hasta,
        REC.audit_user,
        REC.audit_date,
        REC.email,
        REC.euser
    from
        GesPet.dbo.Recurso REC
    where 
        (RTRIM(REC.cod_recurso) = RTRIM(@cod_recurso) or RTRIM(@cod_recurso) is null )and
        (RTRIM(REC.cod_grupo) = RTRIM(@cod_grupo_homologador))

return(0)
go

grant execute on dbo.sp_GetRecursoEsHomologacion to GesPetUsr
go

print 'Actualización realizada.'
