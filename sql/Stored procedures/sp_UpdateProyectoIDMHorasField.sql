/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 02.01.2013 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoIDMHorasField'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoIDMHorasField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoIDMHorasField
go

create procedure dbo.sp_UpdateProyectoIDMHorasField
	@ProjId			int,
	@ProjSubId		int,
	@ProjSubSId		int,
	@cod_nivel		char(3),
	@cod_area		char(8),
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as

if UPPER(RTRIM(@campo))='COD_NIVEL'
	update GesPet.dbo.ProyectoIDMHoras
	set cod_nivel = @valortxt
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		cod_nivel = @cod_nivel and 
		cod_area = @cod_area

if UPPER(RTRIM(@campo))='COD_AREA'
	update GesPet.dbo.ProyectoIDMHoras
	set cod_area = @valortxt
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		cod_nivel = @cod_nivel and 
		cod_area = @cod_area

if UPPER(RTRIM(@campo))='CANT_HORAS'
	update GesPet.dbo.ProyectoIDMHoras
	set cant_horas = @valornum
	where 
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		cod_nivel = @cod_nivel and 
		cod_area = @cod_area
go

grant execute on dbo.sp_UpdateProyectoIDMHorasField to GesPetUsr
go

print 'Actualización realizada.'
go
