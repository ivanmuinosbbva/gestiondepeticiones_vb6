use GesPet
go
/* execute sp_UpdateProyectoAvanceCab 3,'200704' */
print 'sp_UpdateProyectoAvanceCab'
go
if exists (select * from sysobjects where name = 'sp_UpdateProyectoAvanceCab' and sysstat & 7 = 4)
drop procedure dbo.sp_UpdateProyectoAvanceCab
go
create procedure dbo.sp_UpdateProyectoAvanceCab
	@prj_nrointerno		int,
	@pav_aamm	char(6),
	@pav_manual	int
as


if not exists (select prj_nrointerno from ProyectoAvanceCab where prj_nrointerno=@prj_nrointerno and pav_aamm=@pav_aamm)
	begin 
		raiserror 30011 'Planilla de avance inexistente'   
		select 30011 as ErrCode , 'Planilla de avance inexistente' as ErrDesc   
		return (30011)   
	end 

update ProyectoAvanceCab
set pav_manual=@pav_manual
where	prj_nrointerno=@prj_nrointerno and 
	pav_aamm=@pav_aamm
return(0)
go

grant execute on dbo.sp_UpdateProyectoAvanceCab to GesPetUsr
go
