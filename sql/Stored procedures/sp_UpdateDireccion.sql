/*
-000- a. FJS 25.01.2016 - Creaci�n.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateDireccion'
go

if exists (select * from sysobjects where name = 'sp_UpdateDireccion' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateDireccion
go

create procedure dbo.sp_UpdateDireccion 
	@modo			char(1)='A', 
	@cod_direccion	char(8), 
	@nom_direccion	char(80), 
	@flg_habil		char(1)=null, 
	@es_ejecutor	char(1)='S', 
	@IGM			char(1)='S',
	@abreviatura	varchar(30) 
as 
	if @modo='M' 
		begin 
			if not exists (select cod_direccion from GesPet..Direccion where RTRIM(cod_direccion) = RTRIM(@cod_direccion)) 
			begin  
				raiserror 30011 'Direcci�n Inexistente'    
				select 30011 as ErrCode , 'Direcci�n Inexistente' as ErrDesc    
				return (30011)    
			end  
		end 
	else 
		begin 
			if exists (select cod_direccion from GesPet..Direccion where RTRIM(cod_direccion) = RTRIM(@cod_direccion)) 
				begin  
					raiserror 30011 'Codigo de Direcci�n Existente'    
					select 30011 as ErrCode , 'Codigo de Direcci�n Existente' as ErrDesc    
		 
					return (30011)    
				end  
		end 
 
	if @modo='M' 
		begin 
			BEGIN TRANSACTION 
				if RTRIM(@flg_habil)='N' 
				begin 
					update GesPet..Grupo 
					set flg_habil = 'N' 
					from GesPet..Grupo Su, GesPet..Sector Gr, GesPet..Gerencia Ge 
					where 
						(RTRIM(Su.cod_sector) = RTRIM(Gr.cod_sector)) and 
						(RTRIM(Gr.cod_gerencia) = RTRIM(Ge.cod_gerencia)) and 
						(RTRIM(Ge.cod_direccion) = RTRIM(@cod_direccion)) 
	 
					update GesPet..Sector 
					set flg_habil = 'N' 
					from GesPet..Sector Gr, GesPet..Gerencia Ge 
					where (RTRIM(Gr.cod_gerencia) = RTRIM(Ge.cod_gerencia)) and 
						(RTRIM(Ge.cod_direccion) = RTRIM(@cod_direccion)) 
	 
					update GesPet..Gerencia 
					set flg_habil = 'N' 
					from GesPet..Gerencia Ge 
					where (RTRIM(Ge.cod_direccion) = RTRIM(@cod_direccion)) 
				end 
				update GesPet..Direccion 
				set nom_direccion	= @nom_direccion, 
					flg_habil		= @flg_habil, 
					es_ejecutor		= @es_ejecutor,
					IGM_hab			= @IGM,
					abrev_direccion = @abreviatura
				where RTRIM(cod_direccion) = RTRIM(@cod_direccion) 
			COMMIT 
		end 
	else 
		begin 
			insert into GesPet..Direccion (cod_direccion, nom_direccion, flg_habil, es_ejecutor, IGM_hab, abrev_direccion) 
			values (@cod_direccion, @nom_direccion, @flg_habil, @es_ejecutor, @IGM, @abreviatura) 
		end 
	return(0) 
go

grant execute on dbo.sp_UpdateDireccion to GesPetUsr
go

print 'Actualizaci�n realizada.'
go
