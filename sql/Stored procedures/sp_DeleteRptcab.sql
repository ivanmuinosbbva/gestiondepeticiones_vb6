/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 24.11.2015 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteRptcab'
go

if exists (select * from sysobjects where name = 'sp_DeleteRptcab' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteRptcab
go

create procedure dbo.sp_DeleteRptcab
	@rptid	char(10)
as
	delete from GesPet.dbo.Rptcab
	where rptid = @rptid
go

grant execute on dbo.sp_DeleteRptcab to GesPetUsr
go

print 'Actualización realizada.'
go
