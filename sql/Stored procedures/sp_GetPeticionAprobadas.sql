use GesPet
go
print 'sp_GetPeticionAprobadas'
go
if exists (select * from sysobjects where name = 'sp_GetPeticionAprobadas' and sysstat & 7 = 4)
drop procedure dbo.sp_GetPeticionAprobadas
go

create procedure dbo.sp_GetPeticionAprobadas
   (@NroDsd     int,
    @NroHst     int,
    @FchDsd     char(8),
    @FchHst     char(8),
    @Estado     char(6))

as

set dateformat ymd

select
    Pet.pet_nroasignado,
    Pet.cod_tipo_peticion,
    Pet.cod_clase,
    (select nom_estado from GesPet..Estados where cod_estado = Pet.cod_estado) as nom_estado,
    Conf.ok_tipo,
    Conf.audit_date,
    Conf.audit_user,
    (select nom_recurso from GesPet..Recurso where cod_recurso = Conf.audit_user) as nom_recurso,
    Conf.ok_secuencia,
    Conf.pet_nrointerno
from
    GesPet..PeticionConf Conf left join GesPet..Peticion Pet On
    (Conf.pet_nrointerno = Pet.pet_nrointerno)
where
    Conf.ok_tipo in ('TEST','TESP') and
    Pet.pet_nroasignado > 0 and
    (convert(char(8),Conf.audit_date,112)>=@FchDsd OR @FchDsd IS NULL) and
    (convert(char(8),Conf.audit_date,112)<=@FchHst OR @FchHst IS NULL) and
    (Pet.pet_nroasignado >= @NroDsd OR @NroDsd IS NULL) and
    (Pet.pet_nroasignado <= @NroHst OR @NroHst IS NULL) and
    (Pet.cod_estado = @Estado Or @Estado IS NULL)
group by
    Pet.pet_nroasignado,
    Conf.pet_nrointerno,
    Pet.cod_tipo_peticion,
    Pet.cod_clase,
    Conf.ok_tipo,
    Conf.audit_date,
    Conf.audit_user,
    Conf.ok_secuencia
order by
    Conf.audit_date,
    Pet.pet_nroasignado,
    Conf.pet_nrointerno,
    Pet.cod_tipo_peticion,
    Pet.cod_clase,
    Conf.ok_tipo, 
    Conf.audit_user, 
    Conf.ok_secuencia

return(0)
go

grant Execute on dbo.sp_GetPeticionAprobadas to GesPetUsr 
go
