use GesPet
go
print 'sp_DeleteTipoPrj'
go
if exists (select * from sysobjects where name = 'sp_DeleteTipoPrj' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteTipoPrj
go
create procedure dbo.sp_DeleteTipoPrj 
	@cod_tipoprj  char(8)=null
as 

if not exists (select cod_tipoprj from TipoPrj where RTRIM(cod_tipoprj)=RTRIM(@cod_tipoprj))
begin
	raiserror 30001 'TipoPrj Inexistente'   
	select 30001 as ErrCode , 'TipoPrj Inexistente' as ErrDesc   
	return (30001)   
end 

if exists (select cod_tipoprj from Proyecto where RTRIM(cod_tipoprj)=RTRIM(@cod_tipoprj))
begin
	raiserror 30001 'Existe una referencia en Proyectos'   
	select 30001 as ErrCode , 'Existe una referencia en Proyectos' as ErrDesc   
	return (30001)   
end 

delete TipoPrj 
where RTRIM(cod_tipoprj)=RTRIM(@cod_tipoprj) 


return(0) 
go

grant execute on dbo.sp_DeleteTipoPrj to GesPetUsr 
go

