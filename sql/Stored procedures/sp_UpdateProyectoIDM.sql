/* TSQL / Sybase */
/*
-000- a. FJS 05.05.2009 - Proyectos IDM
-001- a. FJS 16.06.2010 - Se agregan los nuevos campos para actualizar.
-002- a. FJS 31.08.2010 - Se agrega un nuevo atributo para "marcar" al proyecto (esto le sirve al usuario para separar en dos grupos los proyectos).
-003- a. FJS 12.01.2016 - Modificación: se cambia el tipo de dato para empresa (se crea un campo nuevo).
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdateProyectoIDM'
go

if exists (select * from sysobjects where name = 'sp_UpdateProyectoIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdateProyectoIDM
go

create procedure dbo.sp_UpdateProyectoIDM
	@ProjId				int,
	@ProjSubId			int,
	@ProjSubSId			int,
	@ProjNom			char(100),
	@ProjCatId			int,
	@ProjClaseId		int,
	--@marca				char(1),
	@plan_tyo			char(1),
	--@empresa			char(1),		-- del -003- a.
	@empresa			int,			-- add -003- a.
	@area				char(1),
	@clas3				char(1),
	@semaphore			char(1),
	@cod_direccion		char(8),
	@cod_gerencia		char(8),
	@cod_sector			char(8),
	@cod_direccion_p	char(8),
	@cod_gerencia_p		char(8),
	@cod_sector_p		char(8),
	@cod_grupo_p		char(8),
	@avance				int,
	@fe_inicio			smalldatetime,
	@fe_fin				smalldatetime,
	@fe_finreprog		smalldatetime,
	@fe_ult_nove		smalldatetime,
	@tipo_proyecto		char(1),
	@totalhs_gerencia	int,
	@totalhs_sector		int,
	@totalreplan		int,
	@codigo_gps			char(11)
as
	/*
	--{ add -004- a.
	declare @cod_estado		char(6)
	declare @new_estado		char(6)
	
	-- Obtengo el estado actual
	select @cod_estado = cod_estado
	from GesPet..ProyectoIDM
	where
		ProjId = @ProjId and 
		ProjSubId = @ProjSubId and 
		ProjSubSId = @ProjSubSId

	-- Obtengo el nuevo estado
	exec sp_GetEstadoProyectoIDM @ProjId, @ProjSubId, @ProjSubSId, @new_estado output
	*/

	if exists (select ProjId from GesPet..ProyectoIDM where ProjId = @ProjId and ProjSubId = @ProjSubId and ProjSubSId = @ProjSubSId)
		begin
			update 
				GesPet.dbo.ProyectoIDM
			set
				ProjNom			 = @ProjNom,
				ProjCatId		 = @ProjCatId,
				ProjClaseId		 = @ProjClaseId,
				--marca			 = @marca,
				plan_tyo		 = @plan_tyo,
				--empresa			 = @empresa,		-- del -003- a.
				codigoEmpresa	 = @empresa,			-- add -003- a.
				area			 = @area,
				clas3			 = @clas3,
				semaphore		 = @semaphore,
				cod_direccion	 = @cod_direccion,
				cod_gerencia	 = @cod_gerencia,
				cod_sector		 = @cod_sector,
				cod_direccion_p	 = @cod_direccion_p,
				cod_gerencia_p	 = @cod_gerencia_p,
				cod_sector_p	 = @cod_sector_p,
				cod_grupo_p		 = @cod_grupo_p,
				avance			 = @avance,
				fe_modif		 = getdate(),
				fe_inicio		 = @fe_inicio,
				fe_fin			 = @fe_fin,
				fe_finreprog	 = @fe_finreprog,
				fe_ult_nove		 = @fe_ult_nove,
				semaphore2		 = null,
				sema_fe_modif	 = null,
				sema_usuario	 = null,
				tipo_proyecto	 = @tipo_proyecto,
				totalhs_gerencia = @totalhs_gerencia,
				totalhs_sector	 = @totalhs_sector,
				totalreplan		 = @totalreplan,
				codigo_gps		 = @codigo_gps
			where 
				ProjId			= @ProjId and
				ProjSubId		= @ProjSubId and
				ProjSubSId		= @ProjSubSId
		end
	return(0)
go

grant execute on dbo.sp_UpdateProyectoIDM to GesPetUsr
go

print 'Actualización realizada.'
go
