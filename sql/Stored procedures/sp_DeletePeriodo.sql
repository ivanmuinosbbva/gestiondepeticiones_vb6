/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeletePeriodo'
go

if exists (select * from sysobjects where name = 'sp_DeletePeriodo' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeletePeriodo
go

create procedure dbo.sp_DeletePeriodo
	@per_nrointerno	int
as
	delete from
		GesPet.dbo.Periodo
	where
		(per_nrointerno = @per_nrointerno or @per_nrointerno is null)
	return(0)
go

grant execute on dbo.sp_DeletePeriodo to GesPetUsr
go

print 'Actualización realizada.'
