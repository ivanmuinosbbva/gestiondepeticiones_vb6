/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */

/*
-001- a. FJS 01.09.2015 - Nuevo SP para CGMw.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_wInsertRecurso'
go

if exists (select * from sysobjects where name = 'sp_wInsertRecurso' and sysstat & 7 = 4)
	drop procedure dbo.sp_wInsertRecurso
go

create procedure dbo.sp_wInsertRecurso 
    @cod_recurso		char(10), 
    @nom_recurso		char(50), 
    @vinculo			char(1), 
    @cod_direccion		char(8), 
    @cod_gerencia		char(8), 
    @cod_sector			char(8), 
    @cod_grupo			char(8), 
    @flg_cargoarea		char(1), 
    @estado_recurso		char(1), 
    @horasdiarias		smallint, 
    @observaciones		varchar(255)=null, 
    @dlg_recurso		char(10), 
    @dlg_desde			smalldatetime=null, 
    @dlg_hasta			smalldatetime=null,
    @email				varchar(60)=null,
    @euser				char(1)
as 
	if exists (select cod_recurso from GesPet..Recurso where RTRIM(cod_recurso) = RTRIM(@cod_recurso)) 
		begin 
			raiserror 30012 'Ya existe un recurso con ese c�digo'
			select 30012 as ErrCode , 'Ya existe un recurso con ese c�digo' as ErrDesc
			return (30012)
		end
	else 
		begin 
			insert into GesPet..Recurso (
				cod_recurso,        
				nom_recurso,         
				vinculo,         
				cod_direccion,    
				cod_gerencia,     
				cod_sector,      
				cod_grupo,     
				flg_cargoarea,    
				estado_recurso,  
				horasdiarias,     
				observaciones,    
				dlg_recurso,     
				dlg_desde,       
				dlg_hasta,
				email, 
				euser,
				audit_user, 
				audit_date)
			values (
				@cod_recurso,       
				@nom_recurso, 
				@vinculo, 
				@cod_direccion, 
				@cod_gerencia, 
				@cod_sector, 
				@cod_grupo, 
				@flg_cargoarea, 
				@estado_recurso, 
				@horasdiarias, 
				@observaciones, 
				@dlg_recurso, 
				@dlg_desde, 
				@dlg_hasta, 
				@email, 
				@euser,
				SUSER_NAME(), 
				getdate())
		end
go

sp_procxmode 'sp_wInsertRecurso', anymode
go

grant execute on dbo.sp_wInsertRecurso to GesPetUsr 
go

print 'Actualizaci�n realizada.'
go