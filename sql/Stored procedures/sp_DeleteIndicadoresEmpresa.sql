/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 09.06.2017 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteIndicadoresEmpresa'
go

if exists (select * from sysobjects where name = 'sp_DeleteIndicadoresEmpresa' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteIndicadoresEmpresa
go

create procedure dbo.sp_DeleteIndicadoresEmpresa
	@empid_peticion		int,
	@empid_indicador	int
as
	delete from
		GesPet.dbo.IndicadoresEmpresa
	where
		empid_peticion = @empid_peticion and
		empid_indicador = @empid_indicador 
go

grant execute on dbo.sp_DeleteIndicadoresEmpresa to GesPetUsr
go

print 'Actualización realizada.'
go
