/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 13.08.2015 - 
-001- a. FJS 23.11.2015 - Nuevo: en caso que los datos de estructura del recurso no vengan como parámetro, el sistema determinará el perfil del mismo.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertPeticionInfohr'
go

if exists (select * from sysobjects where name = 'sp_InsertPeticionInfohr' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertPeticionInfohr
go

create procedure dbo.sp_InsertPeticionInfohr
	@pet_nrointerno	int,
	@info_id		int,
	@info_idver		int,
	@info_item		int,
	@cod_recurso	char(10),
	@cod_perfil		char(4),
	@cod_nivel		char(4),
	@cod_area		char(8),
	@info_valor		int,
	@info_comen		char(255)
as
	IF (select count(1) from Peticion p where p.pet_nrointerno = @pet_nrointerno and p.cod_bpar = @cod_recurso) > 0
		BEGIN
			SELECT @cod_perfil	= 'BPAR'
			SELECT @cod_nivel	= 'BBVA'
			SELECT @cod_area	= 'BBVA'
		END
	ELSE 
		BEGIN
			SELECT @cod_perfil = 'CGRU', @cod_nivel = 'GRUP', @cod_area = r.cod_grupo
			FROM Recurso r
			WHERE LTRIM(RTRIM(r.cod_recurso)) = LTRIM(RTRIM(@cod_recurso))
		END
	
	insert into GesPet.dbo.PeticionInfohr (
		pet_nrointerno,
		info_id,
		info_idver,
		info_item,
		cod_recurso,
		cod_perfil,
		cod_nivel,
		cod_area,
		info_valor,
		info_comen,
		fecha_envio,
		fecha_reenvio)
	values (
		@pet_nrointerno,
		@info_id,
		@info_idver,
		@info_item,
		@cod_recurso,
		@cod_perfil,
		@cod_nivel,
		@cod_area,
		@info_valor,
		LTRIM(RTRIM(@info_comen)),
		null,
		null)
go

grant execute on dbo.sp_InsertPeticionInfohr to GesPetUsr
go

print 'Actualización realizada.'
go
