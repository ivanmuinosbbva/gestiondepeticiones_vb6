/*
-000- a. FJS dd.mm.yyyy - Nuevo SP para
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertOrientacion'
go

if exists (select * from sysobjects where name = 'sp_InsertOrientacion' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertOrientacion
go

create procedure dbo.sp_InsertOrientacion
	@cod_orientacion char(2),
	@nom_orientacion char(30),
	@flg_habil       char(1),
	@orden           int,
	@descripcion     varchar(255)
as 
	INSERT INTO Orientacion 
	VALUES (@cod_orientacion, @nom_orientacion, @flg_habil, @orden, @descripcion)
	return(0)
go

grant execute on dbo.sp_InsertOrientacion to GesPetUsr
go

print 'Actualización realizada.'
go
