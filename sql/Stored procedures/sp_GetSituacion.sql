use GesPet
go
print 'sp_GetSituacion'
go
if exists (select * from sysobjects where name = 'sp_GetSituacion' and sysstat & 7 = 4)
drop procedure dbo.sp_GetSituacion
go
create procedure dbo.sp_GetSituacion 
             @cod_situacion     char(6)=null 
as 
begin 
    select  
        cod_situacion, 
        nom_situacion 
    from 
        GesPet..Situaciones 
    where   
        (@cod_situacion is null or RTRIM(@cod_situacion) is null or RTRIM(@cod_situacion)='' or RTRIM(cod_situacion) = RTRIM(@cod_situacion)) 
    order by nom_situacion ASC  
end 
return(0) 
go



grant execute on dbo.sp_GetSituacion to GesPetUsr 
go


