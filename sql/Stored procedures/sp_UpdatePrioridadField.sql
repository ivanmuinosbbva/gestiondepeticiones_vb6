/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.15 by Fernando J. Spitz (2009) ~ */

/*
-000- a. FJS 23.09.2010 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_UpdatePrioridadField'
go

if exists (select * from sysobjects where name = 'sp_UpdatePrioridadField' and sysstat & 7 = 4)
	drop procedure dbo.sp_UpdatePrioridadField
go

create procedure dbo.sp_UpdatePrioridadField
	@prior_id		char(1),
	@campo			char(80),
	@valortxt		char(255),
	@valordate		smalldatetime,
	@valornum		int
as
	if rtrim(@campo)='PRIOR_ID'
		update GesPet.dbo.Prioridad	set prior_id = @valortxt where 
			(prior_id = @prior_id or @prior_id is null)

	if rtrim(@campo)='PRIOR_DSC'
		update GesPet.dbo.Prioridad set prior_dsc = @valortxt where 
			(prior_id = @prior_id or @prior_id is null)

	if rtrim(@campo)='PRIOR_ORD'
		update GesPet.dbo.Prioridad set prior_ord = @valornum where 
			(prior_id = @prior_id or @prior_id is null)

	if rtrim(@campo)='PRIOR_HAB'
		update GesPet.dbo.Prioridad set prior_hab = @valortxt where 
			(prior_id = @prior_id or @prior_id is null)
	return(0)
go

grant execute on dbo.sp_UpdatePrioridadField to GesPetUsr
go

print 'Actualización realizada.'
go
