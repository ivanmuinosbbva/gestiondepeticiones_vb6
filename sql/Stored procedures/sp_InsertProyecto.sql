/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
use GesPet
go

print 'sp_InsertProyecto'
go

if exists (select * from sysobjects where name = 'sp_InsertProyecto' and sysstat & 7 = 4)
drop procedure dbo.sp_InsertProyecto
go

create procedure dbo.sp_InsertProyecto
	@titulo			char(100),
	@corp_local		char(1),
	@cod_orientacion	char(2)='',
	@importancia_cod	char(2)='',
	@importancia_prf	char(4)='',
	@importancia_usr	char(10)='',
	@fec_requerida		smalldatetime=null,
	@cod_direccion		char(8) ,
	@cod_gerencia		char(8) ,
	@cod_sector		char(8) ,
	@cod_solicitante	char(10),
	@cod_bpar		char(10) ,
	@cod_usualta		char(10),
	@prj_costo		char(1),	
	@fe_ini_esti		smalldatetime=null,
	@fe_fin_esti		smalldatetime=null,
	@cod_tipoprj		char(8)
as
/*BEGIN TRAN*/
declare @proxnumero	  int
select @proxnumero = 0

execute sp_ProximoNumero 'ULPRYINT', @proxnumero OUTPUT
if exists (select prj_nrointerno from GesPet..Proyecto where prj_nrointerno = @proxnumero)
	begin
		select 30010 as ErrCode , 'Error al generar numero interno' as ErrDesc
		raiserror  30010 'Existen Problemas para generar el numero de Proyecto'
		return (30010)
end

insert into GesPet..Proyecto
   (	prj_nrointerno,
	titulo,
	corp_local,
	cod_orientacion,
	cod_tipoprj,
	importancia_cod,
	importancia_prf,
	importancia_usr,
	fec_requerida,
	cod_direccion,
	cod_gerencia,
	cod_sector,
	cod_solicitante,
	cod_bpar,
	prj_costo,
	fe_ini_esti,
	fe_fin_esti,
	cod_estado,
	fe_estado,
	fe_ini_plan,
	fe_fin_plan,
	fe_ini_real,
	fe_fin_real,
	fe_fec_plan,
	fe_fin_orig,
	fe_ini_orig,
	fe_fec_orig,
	cod_usualta,
	fec_alta,
	horaspresup,
	cant_planif,
	ult_accion,
	audit_user,
	audit_date
)
values
	(@proxnumero,
	@titulo,
	@corp_local,
	@cod_orientacion,
	@cod_tipoprj,
	@importancia_cod,
	@importancia_prf,
	@importancia_usr,
	@fec_requerida,
	@cod_direccion,
	@cod_gerencia,
	@cod_sector,
	@cod_solicitante,
	@cod_bpar,
	@prj_costo,
	@fe_ini_esti,
	@fe_fin_esti,
	'COMITE',
	getdate(),
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	null,
	@cod_usualta,
	getdate(),
	0,
	0,
	'PNEW000',	
	SUSER_NAME(),
	getdate()
)

select @proxnumero  as valor_clave

	/*COMMIT TRAN*/
return(0)
go

grant execute on dbo.sp_InsertProyecto to GesPetUsr
go
