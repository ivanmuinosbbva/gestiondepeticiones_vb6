/*
-001- a. FJS 26.05.2009 - Se agrega el DELETE para la tabla HorasTrabajadasAplicativo (para no dejar registros hu�rfanos).
*/

use GesPet
go

print 'sp_DeleteHorasTrabajadas'
go

if exists (select * from sysobjects where name = 'sp_DeleteHorasTrabajadas' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteHorasTrabajadas
go

create procedure dbo.sp_DeleteHorasTrabajadas
	@cod_recurso	char(10),
	@cod_tarea		char(8)=null,
	@pet_nrointerno	int=0,
	@fe_desde		smalldatetime,
	@fe_hasta		smalldatetime
as

if exists (select cod_recurso from GesPet..HorasTrabajadas where
	RTRIM(cod_recurso) = RTRIM(@cod_recurso) and
	pet_nrointerno = @pet_nrointerno and
	(RTRIM(cod_tarea) is null or RTRIM(cod_tarea)='' or RTRIM(cod_tarea) = RTRIM(@cod_tarea)) and
	convert(char(8),fe_desde,112) = convert(char(8),@fe_desde,112) and
	convert(char(8),fe_hasta,112) = convert(char(8),@fe_hasta,112))
	begin
		delete GesPet..HorasTrabajadas
		where 
			RTRIM(cod_recurso) = RTRIM(@cod_recurso) and
			pet_nrointerno = @pet_nrointerno and
			(RTRIM(cod_tarea) is null or RTRIM(cod_tarea)='' or RTRIM(cod_tarea) = RTRIM(@cod_tarea)) and
			convert(char(8),fe_desde,112) = convert(char(8),@fe_desde,112) and
			convert(char(8),fe_hasta,112) = convert(char(8),@fe_hasta,112)
		--{ add -001- a.
		delete GesPet..HorasTrabajadasAplicativo
		where 
			RTRIM(cod_recurso) = RTRIM(@cod_recurso) and
			pet_nrointerno = @pet_nrointerno and
			(RTRIM(cod_tarea) is null or RTRIM(cod_tarea)='' or RTRIM(cod_tarea) = RTRIM(@cod_tarea)) and
			convert(char(8),fe_desde,112) = convert(char(8),@fe_desde,112) and
			convert(char(8),fe_hasta,112) = convert(char(8),@fe_hasta,112)
		--}
	end
else
	begin
		raiserror 30001 'HorasTrabajadas Inexistente'
		select 30001 as ErrCode , 'HorasTrabajadas Inexistente' as ErrDesc
		return (30001)
	end
return(0)
go



grant execute on dbo.sp_DeleteHorasTrabajadas to GesPetUsr
go


