/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 06.06.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteInterfazTera'
go

if exists (select * from sysobjects where name = 'sp_DeleteInterfazTera' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteInterfazTera
go

create procedure dbo.sp_DeleteInterfazTera
	@dbName		char(30)='',
	@objname	char(50)=''
as
	delete from
		GesPet.dbo.InterfazTera
	where
		dbName = @dbName and
		objname = @objname
go

grant execute on dbo.sp_DeleteInterfazTera to GesPetUsr
go

print 'Actualización realizada.'
go
