use GesPet
go
print 'sp_DeleteRecursoXX'
go
if exists (select * from sysobjects where name = 'sp_DeleteRecursoXX' and sysstat & 7 = 4)
drop procedure dbo.sp_DeleteRecursoXX
go
create procedure dbo.sp_DeleteRecursoXX
    @cod_recurso char(10) 
as 
    BEGIN TRANSACTION 
    delete GesPet..Recurso 
      where RTRIM(cod_recurso) = RTRIM(@cod_recurso) 
     
    delete GesPet..RecursoPerfil 
      where RTRIM(cod_recurso) = RTRIM(@cod_recurso) 
 
    COMMIT TRANSACTION 
return(0) 
go



grant execute on dbo.sp_DeleteRecursoXX to GesPetUsr 
go


