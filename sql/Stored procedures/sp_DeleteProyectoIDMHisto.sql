/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 29.11.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DeleteProyectoIDMHisto'
go

if exists (select * from sysobjects where name = 'sp_DeleteProyectoIDMHisto' and sysstat & 7 = 4)
	drop procedure dbo.sp_DeleteProyectoIDMHisto
go

create procedure dbo.sp_DeleteProyectoIDMHisto
	@ProjId	int=0,
	@ProjSubId	int=0,
	@ProjSubSId	int=0,
	@hst_nrointerno	int=0
as
	delete from
		GesPet.dbo.ProyectoIDMHisto
	where
		ProjId = @ProjId and
		ProjSubId = @ProjSubId and
		ProjSubSId = @ProjSubSId and
		hst_nrointerno = @hst_nrointerno
go

grant execute on dbo.sp_DeleteProyectoIDMHisto to GesPetUsr
go

print 'Actualización realizada.'
go
