/*
*********************************************************************************************************************************
ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR
*********************************************************************************************************************************

-000- a. FJS 17.03.2010 - Nuevo SP para visualizar el estado de una petici�n respecto de lo pendiente para pasaje a Producci�n.
-001- a. FJS 28.10.2010 - Control T710: si la cantidad de grupos a controlar es cero (porque son los grupos exceptuados o no hay, entonces
						  el mensaje es que no aplica el control. Antes se marcaba como OK.
						  Este documento es OBLIGATORIO para todos los grupos (sin contar las excepciones).
						  Adem�s, en caso de ser solo uno, indico el nombre del grupo que adeuda el documento.
-001- b. FJS 29.10.2010 - Control CG04: seg�n Ariotti, si tiene documento CG04 la validaci�n es si existe un ALCP/ALCA/EML1
						  de fecha posterior o igual al CG04 (originalmente solo corroboraba ALCP/ALCA).
-001- c. FJS 29.10.2010 - Control C204: con que al menos uno de los grupos tenga C204, el control es v�lido.
-002- a. FJS 25.07.2011 - Nuevo control: para DyD (C104 - Dictamen de Seg. Inf.).
-003- a. FJS 21.11.2014 - Nuevo: se agrega a la tabla de excepciones del control del T710 los grupos de la Gerencia de Sistemas (ex DYD) no homologables.
-003- b. FJJ 10.04.2015 - Nuevo: se optimiza la carga de esta tabla de excepciones para evitar el "join".
-004- a. FJS 07.04.2015 - Nuevo: se corrige la validaci�n cuando el OK a las pruebas de usuario es parcial. En este caso, solo se valida que al menos un grupo t�cnico
						  se encuentre en ejecuci�n y dicho grupo tenga su C204 y su T710. El resto de los grupos pueden "deberlo", pero la petici�n queda v�lida
						  para pasaje a producci�n.
-004- b. FJS 07.04.2015 - Modificaci�n: se actualizan las leyendas.
-004- c. FJS 08.04.2015 - Modificaci�n: se agrega el control para el conforme de supervisor (OKPP, OKPF).
-005- a. FJS 10.04.2015 - Nuevo: se optimiza la carga de este valor y del cursor.
-005- b. FJS 10.04.2015 - Nuevo: esta tabla se elimina porque ya no se controla de esta manera los grupos que deben el T710.
*/

use GesPet
go

print 'Creando/actualizando SP: sp_DIM_GetControlPeticion'
go

if exists (select * from sysobjects where name = 'sp_DIM_GetControlPeticion' and sysstat & 7 = 4)
	drop procedure dbo.sp_DIM_GetControlPeticion
go

create procedure dbo.sp_DIM_GetControlPeticion
	@pet_nrointerno		int,
	@cod_recurso		char(10)
as 
	exec sp_GetControlPeticion @pet_nrointerno, @cod_recurso, '1'
go

grant execute on dbo.sp_DIM_GetControlPeticion to GesPetUsr
go
grant execute on dbo.sp_DIM_GetControlPeticion to dimension
go
print 'Actualizaci�n realizada.'
go
