use GesPet
go

/* --------------   Tabla Temporaria (para que compile)	-------- */

print 'Crea Tabla Temporaria AgrupArbol'
create table #AgrupArbol(
	secuencia	int,
	nivel 		int,
	agr_nrointerno 	int)
go


print 'sp_GetAgrupArbol'
go
if exists (select * from sysobjects where name = 'sp_GetAgrupArbol' and sysstat & 7 = 4)
drop procedure dbo.sp_GetAgrupArbol
go
create procedure dbo.sp_GetAgrupArbol 
	@agr_nrointerno int = 0,
	@nivel		int = 0,
	@secuencia	int=0 OUTPUT
as 

declare	@cod_padre	int
declare @cod_hijo	int
declare @nivel_int	int
declare @ret_status     int

set rowcount 0

/* Verifica si ya existe en el arbol, el Agrupamiento solicitado */
if not @agr_nrointerno=0
   begin
       if exists (select * from #AgrupArbol where agr_nrointerno = @agr_nrointerno)
	return(30003)
   end

select @nivel_int = @nivel + 1

/* primero trae los datos del sujeto en cuestion */
/* o de todos los que no dependen de nadie (si se invoca con 0)*/


create table #CursPadre (agr_nrointerno	int)

create table #CursHijos (agr_nrointerno	int)

insert into #CursPadre
	select Ag.agr_nrointerno
	from   Agrup Ag
	where  (@agr_nrointerno=0 AND Ag.agr_nropadre=0) OR
		Ag.agr_nrointerno=@agr_nrointerno
	order by Ag.agr_titulo,Ag.agr_nrointerno


while (exists (select agr_nrointerno from #CursPadre))
begin
	set rowcount 1
	select @cod_padre = agr_nrointerno from #CursPadre
	select @secuencia = @secuencia + 1
	insert into #AgrupArbol
		(secuencia, nivel, agr_nrointerno)
		values
		(@secuencia, @nivel, @cod_padre)

	/* con cada sujeto encontrado busca sus dependientes */
	/* y los procesa en el nivel proximo 	*/
	set rowcount 0
	insert into #CursHijos
	select Ag.agr_nrointerno
		from   Agrup Ag
		where  Ag.agr_nropadre = @cod_padre
		order by Ag.agr_titulo,Ag.agr_nrointerno
	while (exists (select agr_nrointerno from #CursHijos))
	begin
		set rowcount 1
		select @cod_hijo = agr_nrointerno from #CursHijos
		execute @ret_status = sp_GetAgrupArbol @cod_hijo, @nivel_int, @secuencia OUTPUT
		if (@ret_status <> 0)
			return (@ret_status)
		set rowcount 1
		delete #CursHijos
	end

	/* sigue procesando los registros de este nivel */
	set rowcount 1
	delete #CursPadre
	set rowcount 0
end

set rowcount 0
drop table #CursHijos
drop table #CursPadre
return(0) 
go

print 'Borra Tabla Temporaria'
drop table #AgrupArbol
go




grant execute on dbo.sp_GetAgrupArbol to GesPetUsr 
go
