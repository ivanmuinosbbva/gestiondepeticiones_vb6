/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 22.04.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_GetIndicador'
go

if exists (select * from sysobjects where name = 'sp_GetIndicador' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetIndicador
go

create procedure dbo.sp_GetIndicador
	@indicadorId	int=0,
	@empresa		int
as
	declare @empresa_inter		int
	declare @empresa_default	int 
	
	select @empresa_default = 1

	select @empresa_inter = isnull(ie.empid_indicador,@empresa_default)
	from
		Empresa e LEFT JOIN 
		IndicadoresEmpresa ie ON (e.empid = ie.empid_peticion)
	where e.empid = @empresa

	select 
		ag.indicadorId,
		ag.indicadorNom,
		ag.indicadorHab,
		ag.indicadorEmp,
		ag.indicadorCategoria
	from 
		GesPet.dbo.Indicador ag
	where
		(@indicadorId is null or ag.indicadorId = @indicadorId) and 
		(ag.indicadorEmp = @empresa_inter)
	return 0
go

grant execute on dbo.sp_GetIndicador to GesPetUsr
go

print 'Actualización realizada.'
go
