use GesPet
go
print 'sp_InsertBalanceRubro'
go
if exists (select * from sysobjects where name = 'sp_InsertBalanceRubro' and sysstat & 7 = 4)
drop procedure dbo.sp_InsertBalanceRubro
go
create procedure dbo.sp_InsertBalanceRubro
	@cod_BalanceRubro	char(8),
	@nom_BalanceRubro	char(30),
	@flg_habil		char(1)=null

as

if exists (select cod_BalanceRubro from GesPet..BalanceRubro where RTRIM(cod_BalanceRubro) = RTRIM(@cod_BalanceRubro))
	begin 
		raiserror 30011 'Codigo de Balance-Rubro Existente'   
		select 30011 as ErrCode , 'Codigo de Balance-Rubro Existente' as ErrDesc   
		return (30011)   
	end
else
	begin 
	insert into GesPet..BalanceRubro
			(cod_BalanceRubro, nom_BalanceRubro, flg_habil)
		values
			(@cod_BalanceRubro, @nom_BalanceRubro, @flg_habil)
	end 
return(0)
go


grant execute on dbo.sp_InsertBalanceRubro to GesPetUsr
go
