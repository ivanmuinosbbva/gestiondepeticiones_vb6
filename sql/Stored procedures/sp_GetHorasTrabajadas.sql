use GesPet
go

print 'Creando/actualizando SP: sp_GetHorasTrabajadas'
go

if exists (select * from sysobjects where name = 'sp_GetHorasTrabajadas' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetHorasTrabajadas
go

create procedure dbo.sp_GetHorasTrabajadas 
    @cod_recurso char(10), 
    @fechadesde  smalldatetime=null, 
    @fechahasta  smalldatetime=null 
as 
	begin 
		select  
			cod_recurso, 
			cod_tarea, 
			pet_nrointerno, 
			fe_desde, 
			fe_hasta, 
			horas, 
			trabsinasignar, 
			observaciones,
			tipo
		from 
			GesPet..HorasTrabajadas 
		where   
			(RTRIM(@cod_recurso) = RTRIM(cod_recurso)) and 
			(@fechadesde is null or convert(char(8),fe_hasta,112) >= convert(char(8),@fechadesde,112)) and 
			(@fechahasta is null or convert(char(8),fe_desde,112) <= convert(char(8),@fechahasta,112)) 
		order by cod_recurso,convert(char(8),fe_desde,112),convert(char(8),fe_hasta,112) ASC  
	end 
	return(0) 
go



grant execute on dbo.sp_GetHorasTrabajadas to GesPetUsr 
go

print 'Actualización realizada.'
go

print 'Fin de proceso.'
go
