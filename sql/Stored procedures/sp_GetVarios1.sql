/*
-000- FJS 22.08.2007 - Nuevo SP para utilizar es ocasiones especiales
*/

use GesPet
go
print 'Creando/actualizando SP: sp_GetVarios1'
go
if exists (select * from sysobjects where name = 'sp_GetVarios1' and sysstat & 7 = 4)
	drop procedure dbo.sp_GetVarios1
go

create procedure dbo.sp_GetVarios1 
	@var_codigo        char(8)=null 
as 

if @var_codigo is null 
	begin 
		select 
			var_codigo, 
			var_numero, 
			var_texto, 
			var_fecha 
		from 
			GesPet..Varios 
		order by 
			var_codigo, 
			var_numero
	end
else 
	begin 
		select 
			var_codigo, 
			var_numero, 
			var_texto, 
			var_fecha 
		from 
			GesPet..Varios 
		where 
			var_codigo LIKE '%' + RTRIM(LTRIM(@var_codigo)) + '%'
		order by 
			var_codigo, 
			var_numero
	end 
return(0)
go

grant execute on dbo.sp_GetVarios1 to GesPetUsr 
go


print 'Actualización realizada.'