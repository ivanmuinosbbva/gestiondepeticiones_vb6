/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.20 by Fernando J. Spitz (2012) ~ */

/*
-000- a. FJS 05.12.2012 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertEstadosIDM'
go

if exists (select * from sysobjects where name = 'sp_InsertEstadosIDM' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertEstadosIDM
go

create procedure dbo.sp_InsertEstadosIDM
	@cod_estado	char(6),
	@nom_estado	char(30),
	@flg_rnkup	char(2),
	@flg_herdlt1	char(1),
	@flg_petici	char(1),
	@flg_secgru	char(1)
as
	insert into GesPet.dbo.EstadosIDM (
		cod_estado,
		nom_estado,
		flg_rnkup,
		flg_herdlt1,
		flg_petici,
		flg_secgru)
	values (
		@cod_estado,
		@nom_estado,
		@flg_rnkup,
		@flg_herdlt1,
		@flg_petici,
		@flg_secgru)
go

grant execute on dbo.sp_InsertEstadosIDM to GesPetUsr
go

print 'Actualización realizada.'
go
