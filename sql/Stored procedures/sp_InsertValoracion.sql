/*  Código generado automáticamente por el programa ~ SQLConstructor v1.1.23 by Fernando J. Spitz (2015) ~ */

/*
-000- a. FJS 11.03.2016 - 
*/

use GesPet
go

print 'Creando/actualizando SP: sp_InsertValoracion'
go

if exists (select * from sysobjects where name = 'sp_InsertValoracion' and sysstat & 7 = 4)
	drop procedure dbo.sp_InsertValoracion
go

create procedure dbo.sp_InsertValoracion
	@valorId		int,
	@valorTexto		varchar(255),
	@valorNum		real,
	@valorOrden		int,
	@valorHab		char(1),
	@valorDriver	int
as
	insert into GesPet.dbo.Valoracion (
		valorId,
		valorTexto,
		valorNum,
		valorOrden,
		valorHab,
		valorDriver)
	values (
		@valorId,
		@valorTexto,
		@valorNum,
		@valorOrden,
		@valorHab,
		@valorDriver)
go

grant execute on dbo.sp_InsertValoracion to GesPetUsr
go

print 'Actualización realizada.'
go
