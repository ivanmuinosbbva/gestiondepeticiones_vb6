-- Versionamiento de la aplicaci�n
update
	GesPet..Varios
set
	var_texto = 'v5.1.0',
    var_fecha = getdate()
where
	var_codigo = 'CGMVER'
go

-- Nuevo par�metro
insert into GesPet..Varios values
('CGMCHGM', 0, '147', getdate())
go

-- Cambio de Sector x Grupo (men� Desarrollo)
update
	GesPet..Varios
set
	var_texto = '24-11',
    var_fecha = getdate()
where
	var_codigo = 'CGMDESA'
go