print 'Total de registros en el historial:'
go

select count(*)
from GesPet..Historial
go

print 'Registros con inconsistencias entre nombres:'
go

select count(*)
from GesPet..Historial
where audit_user is null or audit_user = ''
go

print 'Actualización de registros con inconsistencias entre nombres:'
go

update GesPet..Historial
set audit_user = replc_user
where audit_user is null or audit_user = ''
go