/*
Parametrizaci�n inicial de grupo homologador
--------------------------------------------

Alta de grupo homologador (en realidad el grupo de Goldzjtain pasa a ser Homologaci�n
y el nuevo grupo pasa a ser el de Daniel Goldzjtain

*/

print 'Pasando el grupo actual "Goldsztajn, Daniel" como grupo homologador "Homologaci�n" (24-03)...' 
go

update 
	GesPet..Grupo
set 
	nom_grupo = 'Homologaci�n',
	grupo_homologacion = 'H'
where
	cod_grupo = '24-03' and
	cod_sector = '147'
go

print 'Agregando nuevo grupo "Goldsztajn, Daniel" (147-01)...'
go

insert into GesPet..Grupo
(cod_grupo,
 nom_grupo,
 cod_sector,
 flg_habil,
 es_ejecutor, 
 cod_bpar, 
 grupo_homologacion)
values
('147-01',
 'Goldsztajn, Daniel',
 '147',
 'S',
 'S',
 '',
 'N')
go

-- Nuevo evento para reflejar el cambio de estado a Ejecuci�n del grupo Homologador
print 'Nuevo evento para reflejar el cambio de estado a Ejecuci�n del grupo Homologador...'
go

sp_InsMensajesEvento 'GRU', 'GNEW000', 'EJECUC', 'CGRU', 43, 'PET', '', '', 'N', 'S'
go

-- Nuevos mensajes para la parte de Homologaci�n
print 'Nuevos mensajes para la parte de Homologaci�n (16 nuevos mensajes)...'
go

insert into MensajesTexto
values (43, 'Se ha ingresado un conforme de pruebas del usuario y su grupo ha pasado al estado <<En Ejecuci�n>>')
go
insert into MensajesTexto
values (44, 'La petici�n ha recibido un documento de alcance P950.')
go
insert into MensajesTexto
values (45, 'La petici�n ha recibido un documento de alcance C100.')
go
insert into MensajesTexto
values (46, 'La petici�n ha recibido un documento de cambio del alcance CG04.')
go
insert into MensajesTexto
values (47, 'La petici�n ha recibido un documento de casos de prueba C204.')
go
insert into MensajesTexto
values (48, 'La petici�n ha recibido un email de conforme al alcance (EML1).')
go
insert into MensajesTexto
values (49, 'La petici�n ha recibido un email de conforme a las pruebas del usuario (EML2).')
go
insert into MensajesTexto
values (100, 'La petici�n ha recibido un conforme al documento de alcance (ALCA/ALCP).')
go
insert into MensajesTexto
values (101, 'La petici�n ha recibido un conforme de pruebas del usuario (TEST/TESP).')
go
insert into MensajesTexto
values (102, 'La petici�n ha recibido un conforme de pruebas del supervisor p/pasaje a Prod. (OKPP/OKPF).')
go
insert into MensajesTexto
values (110, 'Se agreg� un nuevo grupo homologable a la petici�n.')
go
insert into MensajesTexto
values (111, 'Se agreg� un nuevo sector a la petici�n.')
go
insert into MensajesTexto
values (112, 'Se carg� por primera vez la fecha prevista de pasaje a Producci�n para el Grupo.')
go
insert into MensajesTexto
values (113, 'Se modific� la fecha prevista de pasaje a Producci�n para el Grupo.')
go
insert into MensajesTexto
values (114, 'Se modific� la clase de la petici�n.')
go
insert into MensajesTexto
values (115, 'Un grupo ha cambiado de estado.')
go
