
-- Recurso: Alonso, Ana Silvina
delete
from GesPet..RecursoPerfil
where cod_recurso = 'A94533' and (cod_perfil = '' or cod_perfil is null)
go

-- Recurso: Carro, Silvia
delete
from GesPet..RecursoPerfil
where cod_recurso = 'A76040' and (cod_perfil = '' or cod_perfil is null)
go

-- Recurso: Katz, Lorena Andrea
delete
from GesPet..RecursoPerfil
where cod_recurso = 'A79769' and (cod_perfil = '' or cod_perfil is null)
go

-- Recurso: Michanie, Diana
delete
from GesPet..RecursoPerfil
where cod_recurso = 'E029161' and (cod_perfil = '' or cod_perfil is null)
go

-- Recurso: Quevedo, Anal�a
delete
from GesPet..RecursoPerfil
where cod_recurso = 'A40357' and (cod_perfil = '' or cod_perfil is null)
go

-- Recurso: Salgado, Marina Giselle
delete
from GesPet..RecursoPerfil
where cod_recurso = 'A115373' and (cod_perfil = '' or cod_perfil is null)
go
