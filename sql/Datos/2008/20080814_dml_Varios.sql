-- Parametrización de Desarrollo
print 'Parametrización de Desarrollo...'
go

-- Grupo de Beta Marroquín
insert into GesPet..Varios
	(var_codigo,
	 var_numero,
	 var_texto,
	 var_fecha)
values
	('25|25-02',
	 2,
	 'CGMEXC',
	 getdate())
go

-- Todo el sector de Carlos Patrigniani
insert into GesPet..Varios
	(var_codigo,
	 var_numero,
	 var_texto,
	 var_fecha)
values
	('11',
	 1,
	 'CGMEXC',
	 getdate())
go


-- Versionamiento de implantación de CGM
print 'Versionamiento de implantación de CGM...'
go

update
	GesPet..Varios
set
	var_texto = 'v5.2.0',
	var_fecha = getdate()
where
	var_codigo = 'CGMVER'
go
-- Agregado de sector/recurso para catalogadores
print 'Versionamiento de implantación de CGM...'
go

-- Agregado de nuevo perfil para Catalogadores
insert into GesPet..Perfil values 
('CTLG', 'Catalogador')
go

-- Fecha de control para el nuevo T710
insert into GesPet..Varios values
('T710', 0, '2008/08/15', getdate())

-- Nuevo evento para el cambio de tipo de una petición
insert into GesPet..Acciones values
('PCHGTYPE', 'Cambio de tipo de la petición')

print 'Final de la parametrización.'
go