-- Versionamiento de implantación de CGM
print 'Versionamiento de implantación de CGM...'
go

update
	GesPet..Varios
set
	var_texto = 'v5.3.0',
	var_fecha = getdate()
where
	var_codigo = 'CGMVER'
go

-- Nuevos eventos
print 'Agregado de nuevos eventos...'
go

-- Nuevo evento para registrar la habilitación para pasaje a Producción de la petición
insert into GesPet..Acciones values
('HABPROD', 'Hab. p/ pasaje a Producción')
go

insert into GesPet..Acciones values
('V.OKALCP', 'Vencimiento del Conforme parcial al documento de Alcance')
go

insert into GesPet..Acciones values
('V.OKTESP', 'Vencimiento del Conforme parcial de pruebas de Usuario')
go

insert into GesPet..Acciones values
('V.OKPP', 'Vencimiento del Conforme parcial de Pasaje a Producción sin conforme de Homologación')
go

insert into GesPet..Acciones values
('V.HSOB.P', 'Vencimiento del Conforme parcial de Homologación - Sin observaciones')
go

insert into GesPet..Acciones values
('V.HOME.P', 'Vencimiento del Conforme parcial de Homologación - Con observaciones menores')
go

insert into GesPet..Acciones values
('V.HOMA.P', 'Vencimiento del No conforme parcial de Homologación - Con observaciones mayores')
go

insert into GesPet..Acciones values
('VALIDA', 'Vencimiento de la validez de la petición')
go

insert into GesPet..Acciones values
('PCHGREGU', 'Indicador de Regulatorio')
go





-- Nuevos eventos para la remoción de documentos
insert into GesPet..MensajesTexto values
(50, 'La petición ha recibido un documento Principios de Desarrollo Aplicativo T710.')
go

insert into GesPet..MensajesTexto values
(144, 'Ha sido quitado de la petición el documento de alcance P950.')
go

insert into GesPet..MensajesTexto values
(145, 'Ha sido quitado de la petición el documento de alcance C100.')
go

insert into GesPet..MensajesTexto values
(146, 'Ha sido quitado de la petición el documento de cambio del alcance CG04.')
go

insert into GesPet..MensajesTexto values
(147, 'Ha sido quitado de la petición el documento de casos de prueba C204.')
go

insert into GesPet..MensajesTexto values
(148, 'Ha sido quitado de la petición el email de conforme al alcance (EML1).')
go

insert into GesPet..MensajesTexto values
(149, 'Ha sido quitado de la petición el email de conforme a las pruebas del usuario (EML2).')
go

insert into GesPet..MensajesTexto values
(150, 'Ha sido quitado de la petición el documento de Principios de Desarrollo Aplicativo T710.')
go
