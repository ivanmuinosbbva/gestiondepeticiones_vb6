-- Parametrización de Desarrollo
print 'Parametrización de Desarrollo...'
go

insert into GesPet..Varios
	(var_codigo, 
	 var_numero, 
	 var_texto, 
	 var_fecha) 
values
	('CGMDESA', 
	 0, 
	 '147', 
	 getdate())
go

-- Versionamiento de implantación de CGM
print 'Versionamiento de implantación de CGM...'
go

update
	GesPet..Varios
set
	var_texto = 'v5.0.0',
	var_fecha = getdate()
where
	var_codigo = 'CGMVER'
go