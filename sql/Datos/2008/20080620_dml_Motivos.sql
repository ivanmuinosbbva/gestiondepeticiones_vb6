-- Motivos de suspensión temporaria (Homologación)

insert into GesPet.dbo.Motivos (cod_motivo, nom_motivo, hab_motivo) values (1, 'En espera de OK de usuario', 'S')
go
insert into GesPet.dbo.Motivos (cod_motivo, nom_motivo, hab_motivo) values (2, 'En espera finalización otro grupo de DyD', 'S')
go
insert into GesPet.dbo.Motivos (cod_motivo, nom_motivo, hab_motivo) values (3, 'Cambio de prioridades por DyD', 'S')
go
insert into GesPet.dbo.Motivos (cod_motivo, nom_motivo, hab_motivo) values (4, 'Cambio de prioridades por BP', 'S')
go
insert into GesPet.dbo.Motivos (cod_motivo, nom_motivo, hab_motivo) values (5, 'Cambio de prioridades por Solicitante', 'S')
go
insert into GesPet.dbo.Motivos (cod_motivo, nom_motivo, hab_motivo) values (6, 'Definiciones funcionales pendientes', 'S')
go
insert into GesPet.dbo.Motivos (cod_motivo, nom_motivo, hab_motivo) values (7, 'En espera de solución técnica de terceros', 'S')
go
insert into GesPet.dbo.Motivos (cod_motivo, nom_motivo, hab_motivo) values (8, 'En espera prueba usuario', 'S')
go
insert into GesPet.dbo.Motivos (cod_motivo, nom_motivo, hab_motivo) values (9, 'En espera redefinición de alcance', 'S')
go
insert into GesPet.dbo.Motivos (cod_motivo, nom_motivo, hab_motivo) values (10, 'En espera aceptación de alcance', 'S')
go
insert into GesPet.dbo.Motivos (cod_motivo, nom_motivo, hab_motivo) values (9999, 'Otros', 'S')
go