-- Resp. de Ejecución pueden pasar el grupo a "Finalizado" desde "Suspendido temporalmente"
insert into GesPet..AccionesPerfil values
('GCHGEST', 'CGRU', 'SUSPEN', 'TERMIN', '')
go

-- Nuevo mensaje para identificar cuando un grupo adjunta un documento T710
insert into GesPet..MensajesTexto values
(50, 'La petición ha recibido un documento de Principios de Desarrollo Aplicativo T710.')