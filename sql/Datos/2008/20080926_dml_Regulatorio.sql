-- Aqu� cuento la cantidad de peticiones que tienen identificado como Regulatorio N� 1
select count(*)
from GesPet..Peticion
where prioridad = '1'
go

-- Actualizaci�n masiva de peticiones que deben llevar el indicador de regulatorio
update GesPet..Peticion
set pet_regulatorio = 'S'
where pet_nrointerno in (
	select pet_nrointerno
	from GesPet..Peticion
	where prioridad = '1')
go

-- Todo lo que era antiguamente Regulatorio (en Prioridad) ahora pasa a ser la prioridad m�s alta
update GesPet..Peticion
set prioridad = '2'
where prioridad = '1'
go

