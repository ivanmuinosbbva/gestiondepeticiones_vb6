-- Vuelta atrás de la actualización anterior
print 'Inicialización del dato de Fecha prevista de pasaje a Producción'
update
    GesPet..PeticionGrupo
set
    fe_produccion = null
where
    fe_produccion is not null
go

-- Control: cantidad de grupos a actualizar con Fecha prevista de pasaje a Producción
print 'Cantidad de Grupos que serán actualizados'
go
select
    count(*)
from
    GesPet..PeticionGrupo GRP inner join GesPet..Peticion PET on GRP.pet_nrointerno = PET.pet_nrointerno 
where
    GRP.fe_fin_plan is not null and
    GRP.fe_produccion is null and
    GRP.cod_estado = 'EJECUC' and
	PET.cod_clase in ('NUEV','EVOL','OPTI')
go

-- Control: detalle de grupos a actualizar con Fecha prevista de pasaje a Producción
print 'Detalle de Grupos que serán actualizados'
go
select
    PET.pet_nrointerno,
    PET.pet_nroasignado,
    GRP.cod_grupo,
    GRP.cod_estado,
    GRP.fe_fin_plan
from
    GesPet..PeticionGrupo GRP inner join GesPet..Peticion PET on (GRP.pet_nrointerno = PET.pet_nrointerno)
where
    GRP.fe_fin_plan is not null and
    GRP.fe_produccion is null and
    GRP.cod_estado = 'EJECUC' and
	PET.cod_clase in ('NUEV','EVOL','OPTI')
group by
    PET.pet_nrointerno,
    PET.pet_nroasignado,
    GRP.cod_grupo,
    GRP.cod_estado,
    GRP.fe_fin_plan
go

-- Actualización: se copia la fecha de fin de planificación al nuevo dato de Fecha prevista de pasaje a Producción
print 'Actualizando Grupos en estado "En ejecución"'
go

set rowcount 100
go
-- Actualiza lotes de 100 registros
while
	(
	(select 
		count(1)
     from
    	 GesPet..PeticionGrupo GRU inner join
         GesPet..Peticion PET on (GRU.pet_nrointerno = PET.pet_nrointerno)
	 where
    	 GRU.fe_fin_plan is not null and
    	 GRU.fe_produccion is null and
    	 GRU.cod_estado = 'EJECUC' and
		 PET.cod_clase in ('NUEV','EVOL','OPTI')) > 0 )

		update
    		GesPet..PeticionGrupo
		set
    		GRU.fe_produccion = GRU.fe_fin_plan
		from
		    GesPet..PeticionGrupo GRU, GesPet..Peticion PET
		where
		    GRU.pet_nrointerno = PET.pet_nrointerno and
		    GRU.fe_fin_plan is not null and
		    GRU.fe_produccion is null and
		    GRU.cod_estado = 'EJECUC' and
			PET.cod_clase in ('NUEV','EVOL','OPTI')
go