-- Control: cantidad de grupos a actualizar con Fecha prevista de pasaje a Producci�n
print 'Cantidad de Grupos que ser�n actualizados'
go
select
    count(*)
from
    GesPet..PeticionGrupo GRP
where
    GRP.fe_fin_plan is not null and
    GRP.fe_produccion is null and
    GRP.cod_estado = 'EJECUC'
go

-- Control: detalle de grupos a actualizar con Fecha prevista de pasaje a Producci�n
print 'Detalle de Grupos que ser�n actualizados'
go
select
    PET.pet_nrointerno,
    PET.pet_nroasignado,
    GRP.cod_grupo,
    GRP.cod_estado,
    GRP.fe_fin_plan
from
    GesPet..PeticionGrupo GRP inner join GesPet..Peticion PET on (GRP.pet_nrointerno = PET.pet_nrointerno)
where
    GRP.fe_fin_plan is not null and
    GRP.fe_produccion is null and
    GRP.cod_estado = 'EJECUC'
group by
    PET.pet_nrointerno,
    PET.pet_nroasignado,
    GRP.cod_grupo,
    GRP.cod_estado,
    GRP.fe_fin_plan
go

-- Actualizaci�n: se copia la fecha de fin de planificaci�n al nuevo dato de Fecha prevista de pasaje a Producci�n - 
update
	GesPet..PeticionGrupo
set
	GRU.fe_produccion = GRU.fe_fin_plan
from
	GesPet..PeticionGrupo GRU, GesPet..Peticion PET
where
	GRU.pet_nrointerno = PET.pet_nrointerno and
	GRU.fe_fin_plan is not null and
	GRU.fe_produccion is null and
	GRU.cod_estado = 'EJECUC'
go