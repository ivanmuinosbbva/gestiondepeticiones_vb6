-- Nuevas acciones para los conformes de Homologación

insert into GesPet.dbo.Acciones (cod_accion, nom_accion) values ('HSOB.P', 'Conf. parc. Homo.s/observ.')
go
insert into GesPet.dbo.Acciones (cod_accion, nom_accion) values ('HSOB.F', 'Conf. fin. Homo.s/observ.')
go
insert into GesPet.dbo.Acciones (cod_accion, nom_accion) values ('HOME.P', 'Conf. parc. Homo.c/observ.men.')
go
insert into GesPet.dbo.Acciones (cod_accion, nom_accion) values ('HOME.F', 'Conf. fin. Homo.c/observ.men.')
go
insert into GesPet.dbo.Acciones (cod_accion, nom_accion) values ('HOMA.F', 'No Conf. parc. Homo.c/observ.m')
go
insert into GesPet.dbo.Acciones (cod_accion, nom_accion) values ('HOMA.F', 'No Conf. fin. Homo.c/observ.ma')
go
insert into GesPet.dbo.Acciones (cod_accion, nom_accion) values ('OKPP', 'Ok parc. Pje.Prod.s/Conf.Homol')
go
insert into GesPet.dbo.Acciones (cod_accion, nom_accion) values ('OKPF', 'Ok fin. Pje.Prod.s/ Conf.Homol')
go
insert into GesPet.dbo.Acciones (cod_accion, nom_accion) values ('AGCHGEST', 'Cambio Estado Grupo (auto.)')
go
insert into GesPet.dbo.Acciones (cod_accion, nom_accion) values ('A.HOMA.P', 'No Conf. parc. Homo. (auto.)')
go
insert into GesPet.dbo.Acciones (cod_accion, nom_accion) values ('PAUTHOM', 'Grupo Homologador automático')
go
insert into GesPet.dbo.Acciones (cod_accion, nom_accion) values ('PCHGPRD', 'Fecha prevista pasaje a prod.')
go
insert into GesPet.dbo.Acciones (cod_accion, nom_accion) values ('HOMOLOGA', 'Para homologación')
go