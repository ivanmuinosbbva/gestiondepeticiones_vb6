-- Se permite modificar una petición en estado "En ejecución" para el perfil "Business Partner"
sp_UpdateAccionesPerfil 'PMOD000', 'BPAR', 'EJECUC', ''
go

-- Se permite modificar una petición en estado "En ejecución" para el perfil "Responsable de Ejecución"
sp_UpdateAccionesPerfil 'PMOD000', 'CGRU', 'EJECUC', ''
go

-- Se permite modificar una petición en estado "En ejecución" para el perfil "Responsable de Sector"
sp_UpdateAccionesPerfil 'PMOD000', 'CSEC', 'EJECUC', ''
go

-- Se permite modificar una petición en estados "terminales" (RECHAZ|CANCEL|RECHTE|TERMIN|ANULAD|ANEXAD) para el perfil "Business Partner"
sp_UpdateAccionesPerfil 'PMOD000', 'BPAR', 'RECHAZ', ''
go

sp_UpdateAccionesPerfil 'PMOD000', 'BPAR', 'CANCEL', ''
go

sp_UpdateAccionesPerfil 'PMOD000', 'BPAR', 'RECHTE', ''
go

sp_UpdateAccionesPerfil 'PMOD000', 'BPAR', 'TERMIN', ''
go

sp_UpdateAccionesPerfil 'PMOD000', 'BPAR', 'ANULAD', ''
go

sp_UpdateAccionesPerfil 'PMOD000', 'BPAR', 'ANEXAD', ''
go

-- Se agregan dos nuevos eventos: Cambio en la Visibilidad y la Prioridad de una petición
sp_InsertAcciones 'PCHGPRIO', 'Cambio de prioridad'
go

sp_InsertAcciones 'PCHGVISI', 'Cambio de visibilidad'
go