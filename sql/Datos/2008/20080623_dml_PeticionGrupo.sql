-- Control: cantidad de grupos a actualizar con Fecha prevista de pasaje a Producci�n
print 'Cantidad de Grupos que ser�n actualizados'
go
select
    count(*)
from
    GesPet..PeticionGrupo GRP
where
    GRP.fe_fin_plan is not null and
    GRP.fe_produccion is null and
    GRP.cod_estado = 'EJECUC'
go

-- Control: detalle de grupos a actualizar con Fecha prevista de pasaje a Producci�n
print 'Detalle de Grupos que ser�n actualizados'
go
select
    PET.pet_nrointerno,
    PET.pet_nroasignado,
    GRP.cod_grupo,
    GRP.cod_estado,
    GRP.fe_fin_plan
from
    GesPet..PeticionGrupo GRP inner join GesPet..Peticion PET on (GRP.pet_nrointerno = PET.pet_nrointerno)
where
    GRP.fe_fin_plan is not null and
    GRP.fe_produccion is null and
    GRP.cod_estado = 'EJECUC'
group by
    PET.pet_nrointerno,
    PET.pet_nroasignado,
    GRP.cod_grupo,
    GRP.cod_estado,
    GRP.fe_fin_plan
go

/*
- Actualizaci�n: se copia la fecha de fin de planificaci�n al nuevo dato de Fecha prevista de pasaje a Producci�n - 

23.06.2008

FJS - Se ejecuta de esta manera por error durante ejecuci�n en Producci�n el d�a 20.06.2008

Server Message:  Number  1105, Severity  17
Server 'DSPROD02', Line 1:
Can't allocate space for object 'syslogs' in database 'GesPet' because 'logsegment' segment is full/has no free extents. If you ran out of space in syslogs, dump the transaction log. Otherwise, use ALTER DATABASE to increase the size of the segment. 
Server Message:  Number  7412, Severity  10
Server 'DSPROD02', Line 1:
Space available in the log segment has fallen critically low in database 'GesPet'.  All future modifications to this database will be aborted until the log is successfully dumped and space becomes available.
*/

set rowcount 100
go
-- Actualiza lotes de 100 registros
while
	(
	(select 
		count(1)
     from
    	 GesPet..PeticionGrupo GRU inner join
         GesPet..Peticion PET on (GRU.pet_nrointerno = PET.pet_nrointerno)
	 where
    	 GRU.fe_fin_plan is not null and
    	 GRU.fe_produccion is null and
    	 GRU.cod_estado = 'EJECUC') > 0 )

		update
    		GesPet..PeticionGrupo
		set
    		GRU.fe_produccion = GRU.fe_fin_plan
		from
		    GesPet..PeticionGrupo GRU, GesPet..Peticion PET
		where
		    GRU.pet_nrointerno = PET.pet_nrointerno and
		    GRU.fe_fin_plan is not null and
		    GRU.fe_produccion is null and
		    GRU.cod_estado = 'EJECUC'
go