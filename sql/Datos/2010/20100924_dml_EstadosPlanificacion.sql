insert into GesPet..EstadosPlanificacion values (1, 'Sin planificar', 'S')
go
insert into GesPet..EstadosPlanificacion values (2, 'Planificada', 'S')
go
insert into GesPet..EstadosPlanificacion values (3, 'Planificada condicionalmente', 'S')
go
insert into GesPet..EstadosPlanificacion values (4, 'No se puede planificar', 'S')
go
insert into GesPet..EstadosPlanificacion values (5, 'Desplanificada', 'S')
go
insert into GesPet..EstadosPlanificacion values (6, 'Rechazada', 'S')
go
insert into GesPet..EstadosPlanificacion values (7, 'Desarrollo finalizado', 'S')
go