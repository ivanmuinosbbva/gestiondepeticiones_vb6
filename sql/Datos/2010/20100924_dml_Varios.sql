-- Cantidad de d�as para la eliminaci�n de solicitudes TSO sin aprobaci�n
insert into GesPet..Varios values ('TSOENM1', 60, '', getdate())
go
-- Cambio de versi�n
update GesPet..Varios 
set 
	var_texto = 'v5.12.0',
	var_fecha = getdate()
where var_codigo = 'CGMVER'
go
-- Habilitaciones para planificaci�n
insert into GesPet..Varios values ('PLANBP', 0, null, getdate())
go
insert into GesPet..Varios values ('PLANDYD', 0, null, getdate())
go
insert into GesPet..Varios values ('PLANOP1', 0, null, getdate())
go
insert into GesPet..Varios values ('PLANOP2', 0, null, getdate())
go
