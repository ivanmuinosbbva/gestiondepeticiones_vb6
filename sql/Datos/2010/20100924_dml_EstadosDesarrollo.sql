insert into GesPet..EstadosDesarrollo values (1, 'No iniciado', 'S')
go
insert into GesPet..EstadosDesarrollo values (2, 'En desarrollo (adelantado)', 'S')
go
insert into GesPet..EstadosDesarrollo values (3, 'En desarrollo (atrasado)', 'S')
go
insert into GesPet..EstadosDesarrollo values (4, 'En desarrollo (planeado)', 'S')
go
insert into GesPet..EstadosDesarrollo values (5, 'En prueba funcional de DYD', 'S')
go
insert into GesPet..EstadosDesarrollo values (6, 'En prueba de usuario', 'S')
go
insert into GesPet..EstadosDesarrollo values (7, 'Suspendido', 'S')
go
insert into GesPet..EstadosDesarrollo values (8, 'Implementado', 'S')
go
insert into GesPet..EstadosDesarrollo values (9, 'Cancelado/Rechazado', 'S')
go
insert into GesPet..EstadosDesarrollo values (10, 'Replanificado', 'N')
go
insert into GesPet..EstadosDesarrollo values (11, 'Desplanificado', 'S')
go
insert into GesPet..EstadosDesarrollo values (12, 'N/A', 'S')
go