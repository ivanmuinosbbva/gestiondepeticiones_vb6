-- Consulta para ejecutar en producción
print 'PeticionChangeMan'
go

select 
	a.pet_nrointerno, 
	b.pet_nroasignado as 'Peticion',
	--substring(a.pet_record, 4, 7)
	convert(int, substring(a.pet_record, 4, 7)) as 'ChangeMan'
from GesPet..PeticionChangeMan a left join GesPet..Peticion b on a.pet_nrointerno = b.pet_nrointerno
where b.pet_nroasignado is not null and
b.pet_nroasignado <> convert(int, substring(a.pet_record, 4, 7))
go

print 'PeticionEnviadas'
go

select 
	a.pet_nrointerno, 
	b.pet_nroasignado as 'Peticion',
	--substring(a.pet_record, 4, 7)
	convert(int, substring(a.pet_record, 4, 7)) as 'Enviadas'
from GesPet..PeticionEnviadas a left join GesPet..Peticion b on a.pet_nrointerno = b.pet_nrointerno
where b.pet_nroasignado is not null and
b.pet_nroasignado <> convert(int, substring(a.pet_record, 4, 7))
go