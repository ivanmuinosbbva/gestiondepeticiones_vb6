-- 2. Todas las horas trabajadas para estas peticiones
select
	a.pet_nrointerno,
	pet_nroasignado = (select x.pet_nroasignado from GesPet..Peticion x where x.pet_nrointerno = a.pet_nrointerno),
	a.cod_recurso,
	a.cod_tarea,
	a.fe_desde,
	a.fe_hasta,
	a.horas,
	a.trabsinasignar
from
	GesPet..HorasTrabajadas a
where 
	a.pet_nrointerno in 
	(select s.pet_nrointerno 
		from GesPet..Peticion s 
		where s.pet_projid = 159 and 
			s.pet_projsubid = 0 and 
			s.pet_projsubsid = 0)
go
