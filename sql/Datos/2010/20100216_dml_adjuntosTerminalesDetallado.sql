/*
TERMINALES
Detalle de peticiones en estado terminal con documentos adjuntos y tama�os de los mismos
*/
select 
	b.pet_nroasignado, 
	count(*) as 'total documentos',
	sum(datalength(adj_objeto)) as 'tama�o en bytes',
	convert(numeric(10,4), sum(datalength(a.adj_objeto)) / convert(numeric(10,4), 1024)) as 'tama�o en kilobytes',
	convert(numeric(10,6), sum(datalength(a.adj_objeto)) / convert(numeric(10,6), 1024) / convert(numeric(10,6), 1024)) as 'tama�o en megabytes',
	convert(numeric(10,4), avg(datalength(a.adj_objeto)) / convert(numeric(10,4), 1024)) as 'promedio en kilobytes',
	convert(numeric(10,6), avg(datalength(a.adj_objeto)) / convert(numeric(10,6), 1024) / convert(numeric(10,6), 1024)) as 'promedio en megabytes',
	convert(numeric(10,4), min(datalength(a.adj_objeto)) / convert(numeric(10,4), 1024)) as 'm�nimo en kilobytes',
	convert(numeric(10,6), min(datalength(a.adj_objeto)) / convert(numeric(10,6), 1024) / convert(numeric(10,6), 1024)) as 'm�nimo en megabytes',
	convert(numeric(10,4), max(datalength(a.adj_objeto)) / convert(numeric(10,4), 1024)) as 'm�ximo en kilobytes',
	convert(numeric(10,6), max(datalength(a.adj_objeto)) / convert(numeric(10,6), 1024) / convert(numeric(10,6), 1024)) as 'm�ximo en megabytes'
from 
	GesPet..PeticionAdjunto a inner join 
	GesPet..Peticion b on a.pet_nrointerno = b.pet_nrointerno
where
	b.cod_estado in ('RECHAZ','CANCEL','RECHTE','TERMIN','ANEXAD','ANULAD')
group by
	b.pet_nroasignado
