insert into GesPet..Documento values ('C102','Diseño de solución','S')
go
insert into GesPet..Documento values ('C100','Documento de alcance (c/impacto tecnológico y/o proyecto)','S')
go
insert into GesPet..Documento values ('P950','Documento de alcance (s/impacto tecnológico ni proyecto)','S')
go
insert into GesPet..Documento values ('C204','Casos de prueba','S')
go
insert into GesPet..Documento values ('CG04','Documento de cambio de alcance','S')
go
insert into GesPet..Documento values ('T700','Plan de implantación Desarrollo','S')
go
insert into GesPet..Documento values ('T710','Principios de Desarrollo de Aplicativos','S')
go
insert into GesPet..Documento values ('C310','Definición de Base de Datos','S')
go
insert into GesPet..Documento values ('EML1','Mail de OK alcance','S')
go
insert into GesPet..Documento values ('EML2','Mail de OK prueba de usuario','S')
go
insert into GesPet..Documento values ('OTRO','Otros documentos','S')
go
insert into GesPet..Documento values ('IDH1','Informe de Homologación SOB','S')
go
insert into GesPet..Documento values ('IDH2','Informe de Homologación OME','S')
go
insert into GesPet..Documento values ('IDH3','Informe de Homologación OMA','S')
go



-- Los documentos de homologación se inhabilitan para la selección por parte del usuario.
update GesPet..Documento
set hab_doc = 'N'
where cod_doc like 'IDH%'
go