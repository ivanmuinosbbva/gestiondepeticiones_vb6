select 
	a.pet_nroasignado
from 
	GesPet..Peticion a inner join 
	GesPet..PeticionEnviadas b on a.pet_nrointerno = b.pet_nrointerno
where 
	not exists (
		select x.pet_nrointerno 
		from GesPet..PeticionGrupo x 
		where x.pet_nrointerno = a.pet_nrointerno and x.cod_gerencia = 'DESA') and
a.pet_nrointerno in (select x.pet_nrointerno from GesPet..PeticionGrupo x)
go
