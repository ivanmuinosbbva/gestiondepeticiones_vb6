/*
===========================================================
ACTUALIZACION DE ESTADO
===========================================================

Aquellos DSN del vuelco inicial que no han sido visados a�n 
que no hayan sido utilizados para generar solicitudes
*/

update 
	GesPet..HEDT001
set 
	estado = 'A'
from 
	GesPet..HEDT001 x
where 
	x.dsn_vb = 'N' and
	left(x.dsn_id,1) = 'V' and
	x.estado = 'C' and
	x.dsn_nom not in (
		select
			a.dsn_nom
		from
			GesPet..Solicitudes b inner join GesPet..HEDT001 a on (
			(b.sol_tipo in ('1','5') and ltrim(rtrim(a.dsn_nom)) = ltrim(rtrim(substring(substring(substring(b.sol_file_desa, charindex('.', b.sol_file_desa)+1,255),charindex('.',substring(b.sol_file_desa, charindex('.', b.sol_file_desa) + 1, 255))+1,255),charindex('.',substring(substring(b.sol_file_desa, charindex('.', b.sol_file_desa)+1,255),charindex('.',substring(b.sol_file_desa, charindex('.', b.sol_file_desa) + 1, 255))+1,255))+1,255)))) or
			(b.sol_tipo in ('2','3','4','6') and ltrim(rtrim(a.dsn_nom)) = ltrim(rtrim(substring(substring(substring(b.sol_file_out, charindex('.', b.sol_file_out)+1,255),charindex('.',substring(b.sol_file_out, charindex('.', b.sol_file_out) + 1, 255))+1,255),charindex('.',substring(substring(b.sol_file_out, charindex('.', b.sol_file_out)+1,255),charindex('.',substring(b.sol_file_out, charindex('.', b.sol_file_out) + 1, 255))+1,255))+1,255)))))
		group by 
			a.dsn_nom)
go
