-- Solicitudes para utilidad de Seguridad Informática
select
	a.sol_nroasignado,
	sol_tipo = 
		case 
			when a.sol_tipo = '1' then 'XCOM'
			when a.sol_tipo = '5' then 'XCOM*'
			when a.sol_tipo = '2' then 'UNLO'
			when a.sol_tipo = '3' then 'TCOR'
			when a.sol_tipo = '4' then 'SORT'
			when a.sol_tipo = '6' then 'SORT*'
		end,
	a.sol_mask,
	a.sol_fecha,
	sol_recurso = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_recurso),
	sol_supervisor = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.sol_resp_sect),
	pet_nroasignado = (select x.pet_nroasignado from GesPet..Peticion x where x.pet_nrointerno = a.pet_nrointerno),
	justificacion = (select x.jus_descripcion from GesPet..Justificativos x where x.jus_codigo = a.jus_codigo),
	dsn_prod = 
		case
			when a.sol_tipo = '1' then a.sol_file_prod
			when a.sol_tipo = '2' then a.sol_mem_Sysin
			when a.sol_tipo = '4' then a.sol_file_prod
			when a.sol_tipo = '5' then a.sol_file_out
			when a.sol_tipo = '6' then a.sol_file_prod
			else a.sol_nrotabla
		end,
	dsn_desa = 
		case 
			when a.sol_tipo = '1' then a.sol_file_desa
			when a.sol_tipo = '2' then a.sol_file_out
			when a.sol_tipo = '3' then a.sol_file_out
			when a.sol_tipo = '4' then a.sol_file_out
			when a.sol_tipo = '5' then a.sol_file_desa
			when a.sol_tipo = '6' then a.sol_file_out
		end
from 
	GesPet..Solicitudes a
where 
	convert(smalldatetime, a.sol_fecha, 112) >= '20091102'	-- Las generadas a partir del enmascaramiento de datos (02.11.2009)
