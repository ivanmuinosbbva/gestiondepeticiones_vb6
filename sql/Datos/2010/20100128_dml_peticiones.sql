-- 1. Estas son las peticiones involucradas en el proyecto IDM 159.0.0

select 
	a.pet_nroasignado,
	a.cod_tipo_peticion,
	a.cod_clase,
	a.titulo,
	descripcion = (select x.mem_texto from GesPet..PeticionMemo x where x.pet_nrointerno = a.pet_nrointerno and x.mem_campo = 'DESCRIPCIO' and mem_secuencia = 0)
from
	GesPet..Peticion a
where
	a.pet_projid = 159 and a.pet_projsubid = 0 and a.pet_projsubsid = 0
go