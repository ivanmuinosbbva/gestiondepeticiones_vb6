-- Control pasaje Producci�n p/ Homologaci�n (si esta activado con 1, las peticiones no pueden pasar a v�lidas)
insert into GesPet..Varios values ('CTRLPRD1', 1, '', getdate())
go

-- Cambio de versi�n
update GesPet..Varios 
set 
	var_texto = 'v5.11.0',
	var_fecha = getdate()
where var_codigo = 'CGMVER'
go
