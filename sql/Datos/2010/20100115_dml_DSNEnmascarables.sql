-- DSN que tienen declarados datos sensibles
select 
	a.dsn_id,
	a.dsn_nom,
	a.dsn_enmas,
	a.dsn_vb,
	valido = 
		case
			when a.dsn_enmas = 'N' then 'Si'
			when ((select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = a.dsn_id) > 0 and
				  (select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id) > 0 and a.dsn_id not in
				  (select x.dsn_id from GesPet..HEDT003 x where x.dsn_id = a.dsn_id and x.cpo_nomrut = '' or x.cpo_nomrut is null)) 
				and a.dsn_enmas = 'S' then 'Si'
			else
				'No'
		end
from GesPet..HEDT001 a
where a.dsn_enmas <> 'N'