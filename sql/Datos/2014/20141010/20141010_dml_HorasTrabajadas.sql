use GesPet
go

create table #HorasTrabajadasTemp (
	cod_recurso		char(10)		NULL,
	cod_tarea		char(8)			NULL,
	pet_nrointerno	int				NULL,
	fe_desde		smalldatetime	NULL,
	fe_hasta		smalldatetime	NULL,
	horas			int				NULL,
	trabsinasignar	char(1)			NULL,
	observaciones	varchar(255)	NULL,
	audit_user		char(10)		NULL,
	audit_date		smalldatetime	NULL)

insert into #HorasTrabajadasTemp
	select 
		cod_recurso,
		cod_tarea,
		pet_nrointerno,
		min(fe_desde),
		max(fe_hasta),
		sum(horas),
		trabsinasignar,
		'',
		'BATCH',
		getdate()
	 from HorasTrabajadas
	 where fe_hasta < '20140101'
	 group by cod_recurso
			, cod_tarea
			, pet_nrointerno
			, month(fe_desde) & year(fe_desde)
			, month(fe_hasta) & year(fe_hasta)
			, trabsinasignar
	union 
	select 
		cod_recurso,
		cod_tarea,
		pet_nrointerno,
		fe_desde,
		fe_hasta,
		horas,
		trabsinasignar,
		observaciones,
		audit_user,
		audit_date
	from HorasTrabajadas
	where fe_hasta >= '20140101'

truncate table HorasTrabajadas

INSERT INTO HorasTrabajadas
	SELECT
		cod_recurso,
		cod_tarea,
		pet_nrointerno,
		fe_desde,
		fe_hasta,
		horas,
		trabsinasignar,
		observaciones,
		audit_user,
		audit_date
	FROM #HorasTrabajadasTemp

drop table #HorasTrabajadasTemp

