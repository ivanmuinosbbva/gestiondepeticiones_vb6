/*
	03.07.2014
	Actualizamos el nuevo campo "sol_eme" con N 
	por defecto (no son solicitudes de emergencia).
*/

UPDATE Solicitudes
SET sol_eme = 'N'
WHERE sol_eme is null
go

