-- 07.07.2014 - Los estados que puede tener una solicitud a CM

insert into EstadoSolicitud values ('A','Pendiente aprobación')
go
insert into EstadoSolicitud values ('B','Pendiente aprobación (ex post)')
go
insert into EstadoSolicitud values ('C','Aprobado')
go
insert into EstadoSolicitud values ('D','Aprobado y en proceso de envio')
go
insert into EstadoSolicitud values ('E','Enviado y finalizado')
go
insert into EstadoSolicitud values ('F','En espera de Restore')
go
insert into EstadoSolicitud values ('G','En espera (ejecución diferida)')
go
