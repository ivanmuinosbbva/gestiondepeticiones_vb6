insert into TipoSolicitud values ('1','XCOM','Copia de archivo a desarrollo')
go
insert into TipoSolicitud values ('2','UNLO','Unload de tabla')
go
insert into TipoSolicitud values ('3','TCOR','Extracci�n de tabla del VSAM')
go
insert into TipoSolicitud values ('4','SORT','Copia con selecci�n de datos')
go
insert into TipoSolicitud values ('5','XCOM*','Restore de backup de XCOM')
go
insert into TipoSolicitud values ('6','SORT*','Restore de backup de SORT')
go
insert into TipoSolicitud values ('7','UNLO*','Restore de backup de UNLO')
go