-- Parametrización: agregado de nuevas acciones para el nuevo perfil de Analista Ejecutor
sp_InsertAccionPerfil 'PNEW000', 'ANAL', 'ALL', '', null
go
sp_InsertAccionPerfil 'PINTERV', 'ANAL', 'ESTIMA', '', null
go
sp_InsertAccionPerfil 'PINTERV', 'ANAL', 'PLANIF', '', null
go
sp_InsertAccionPerfil 'PINTERV', 'ANAL', 'REVISA', '', null
go
sp_InsertAccionPerfil 'PINTERV', 'ANAL', 'EVALUA', '', null
go
sp_InsertAccionPerfil 'PINTERV', 'ANAL', 'OPINIO', '', null
go
