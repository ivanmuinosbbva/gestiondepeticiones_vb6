-- 01.12.2009 - Arreglo por importación inicial mal definida: los tipos de dato estaban mal parseados en las planillas del catalogo inicial.

-- HEDT003
sp_UpdateHEDT003Field 'V0001484', 'DNC0227', 'PA-NROCTA', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001493', 'DNC0080', 'TD-NROCTA', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001495', 'DNS0030', 'L-NROCTA', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001496', 'DNX1300', 'PCW-NROCTA','CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001497', 'DNS0030', 'L-NROCTA', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001741', 'YQTCLOG', 'NRO-CONTRATO', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001741', 'YQTCLOG', 'NRO-CONTRATO2', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001847', 'CRC13011', 'NRO-CUIL-APOD', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001847', 'CRC13011', 'NRO-CUIL-BENF', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001847', 'CRC13011', 'TI-DOC-APOD', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001847', 'CRC13011', 'TI-DOC-BENF', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001848', 'CRC14011', 'APELL-NOM-BENF', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001848', 'CRC14011', 'TI-DOC-BENF', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001850', 'RCTCAFP0', 'TAFP-RCT-TIPODOC', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001741', 'YQTCLOG', 'NRO-CONT', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001496', 'DNX1300', 'PCW-NROC', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001484', 'DNC0227', 'PA-NROCT', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001493', 'DNC0080', 'TD-NROCT', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001847', 'CRC13011', 'DOCUMENT', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001848', 'CRC14011', 'APELL-NO', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001848', 'CRC14011', 'DOCUMENT', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001850', 'RCTCAFP0', 'DOCUMENT', 'CPO_TYPE', 'C', null, null
go
sp_UpdateHEDT003Field 'V0001498', 'DNC0780', 'LOUT-NROCTA', 'CPO_TYPE', '3', null, null
go

-- HEDT001
sp_UpdateHEDT001Field 'V0001741', 'ESTADO', 'B', null, null
go
sp_UpdateHEDT001Field 'V0001484', 'ESTADO', 'B', null, null
go
sp_UpdateHEDT001Field 'V0001847', 'ESTADO', 'B', null, null
go
sp_UpdateHEDT001Field 'V0001848', 'ESTADO', 'B', null, null
go
sp_UpdateHEDT001Field 'V0001850', 'ESTADO', 'B', null, null
go
sp_UpdateHEDT001Field 'V0001493', 'ESTADO', 'B', null, null
go
sp_UpdateHEDT001Field 'V0001495', 'ESTADO', 'B', null, null
go
sp_UpdateHEDT001Field 'V0001496', 'ESTADO', 'B', null, null
go
sp_UpdateHEDT001Field 'V0001497', 'ESTADO', 'B', null, null
go
sp_UpdateHEDT001Field 'V0001498', 'ESTADO', 'B', null, null
go