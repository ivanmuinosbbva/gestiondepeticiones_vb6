-- Versionamiento de implantación de CGM
print 'Versionamiento de implantación de CGM...'
go

update
	GesPet..Varios
set
	var_texto = 'v5.5.0',
	var_fecha = getdate()
where
	var_codigo = 'CGMVER'
go