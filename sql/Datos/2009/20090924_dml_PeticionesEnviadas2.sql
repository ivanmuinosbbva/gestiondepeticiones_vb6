-- Elimina aquellas peticiones que se encuentran en la tabla de peticiones válidas (depuración inicial)
delete
 from GesPet..PeticionEnviadas2
 where pet_nrointerno in (
    select
        b.pet_nrointerno
    from
        GesPet..PeticionEnviadas a inner join
        GesPet..PeticionEnviadas2 b on a.pet_nrointerno = b.pet_nrointerno
    group by
        b.pet_nrointerno)