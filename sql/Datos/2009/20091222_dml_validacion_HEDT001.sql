-- Detalle de archivos DSN con declaración de datos sensibles (proceso de enmascaramiento) que no possen el detalle de los copys y campos correspondientes.

select
	a.dsn_id,
	a.dsn_nom,
	valido = 
		case
			when a.dsn_enmas = 'N' then 'Si'
			when ((select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = a.dsn_id) > 0 and
				  (select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id) > 0 and a.dsn_id not in
				  (select x.dsn_id from GesPet..HEDT003 x where x.dsn_id = a.dsn_id and x.cpo_nomrut = '' or x.cpo_nomrut is null)) 
				and a.dsn_enmas = 'S' then 'Si'
			else
				'No'
		end,
	motivo = 
		case
			when 
				a.dsn_enmas = 'S' and
				(select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = a.dsn_id) > 0 and
				(select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id) = 0
					then 'Falta detalle de CAMPOS'
			when
				a.dsn_enmas = 'S' and
				(select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = a.dsn_id) = 0 and
				(select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id) = 0
					then 'Falta detalle de COPYS y CAMPOS'
			when 
				a.dsn_enmas = 'S' and
				(select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = a.dsn_id) = 0 and
				(select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id) > 0
					then 'Falta detalle de COPYS (hay CAMPOS definidos)'
			when
				a.dsn_enmas = 'S' and
				(select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = a.dsn_id) > 0 and
				(select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id) > 0 and
				(select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id and x.cpo_nomrut = '' or x.cpo_nomrut is null) > 0
					then 'Falta rutina de enmascaramiento en CAMPO'
			else
				'Otro motivo'
		end
from
	GesPet..HEDT001 a
where
	a.dsn_enmas = 'S'
group by
	a.dsn_id, a.dsn_nom, a.dsn_enmas
having
	not ((select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = a.dsn_id) > 0 and
         (select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id) > 0) or
	((select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = a.dsn_id) > 0 and
     (select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id) > 0 and
	 (select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id and x.cpo_nomrut = '') > 0) or 
	((select count(x.dsn_id) from GesPet..HEDT002 x where x.dsn_id = a.dsn_id) > 0 and
     (select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id) > 0 and
	 (select count(x.dsn_id) from GesPet..HEDT003 x where x.dsn_id = a.dsn_id and x.cpo_nomrut is null) > 0)
order by
	a.dsn_id