-- Cambio de c�digo de prioridad ALTO: 1 x 2
update GesPet..Peticion
set 
	prioridad = '2'
from 
	GesPet..Peticion a inner join 
	GesPet..PeticionMemo b on a.pet_nrointerno = b.pet_nrointerno inner join 
	GesPet..AgrupPetic c on a.pet_nrointerno = c.pet_nrointerno
where
	a.cod_tipo_peticion in ('AUX','AUI') and 
	a.cod_solicitante = 'E999999' and
	(a.prioridad = '1' or a.prioridad = '') and
	b.mem_campo = 'CARACTERIS' and
	upper(b.mem_texto) like '%ALT_%' and
	c.agr_nrointerno in (807,809,808,812,811,815,814)
go

-- Cambio de c�digo de prioridad MEDIO: 2 x 3
update GesPet..Peticion
set 
	prioridad = '3'
from 
	GesPet..Peticion a inner join 
	GesPet..PeticionMemo b on a.pet_nrointerno = b.pet_nrointerno inner join 
	GesPet..AgrupPetic c on a.pet_nrointerno = c.pet_nrointerno
where
	a.cod_tipo_peticion in ('AUX','AUI') and 
	a.cod_solicitante = 'E999999' and
	(a.prioridad = '2' or a.prioridad = '') and
	b.mem_campo = 'CARACTERIS' and
	upper(b.mem_texto) like '%MEDI_%' and
	c.agr_nrointerno in (807,809,808,812,811,815,814)
go

-- Cambio de c�digo de prioridad BAJO: 3 x 4
update GesPet..Peticion
set 
	prioridad = '4'
from 
	GesPet..Peticion a inner join 
	GesPet..PeticionMemo b on a.pet_nrointerno = b.pet_nrointerno inner join 
	GesPet..AgrupPetic c on a.pet_nrointerno = c.pet_nrointerno
where
	a.cod_tipo_peticion in ('AUX','AUI') and 
	a.cod_solicitante = 'E999999' and
	(a.prioridad = '3' or a.prioridad = '') and
	b.mem_campo = 'CARACTERIS' and
	upper(b.mem_texto) like '%BAJ_%' and
	c.agr_nrointerno in (807,809,808,812,811,815,814)
go