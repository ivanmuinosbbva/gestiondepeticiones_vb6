-- Agregado manual en el historial de registro de observación sobre el cambio de prioridad

use GesPet
go

declare cur_peticiones cursor for 
	-- Nivel de prioridad: ALTO
	select 
		a.pet_nrointerno,
		a.cod_estado
	from 
		GesPet..Peticion a inner join 
		GesPet..PeticionMemo b on a.pet_nrointerno = b.pet_nrointerno inner join 
		GesPet..AgrupPetic c on a.pet_nrointerno = c.pet_nrointerno
	where
		a.cod_tipo_peticion in ('AUX','AUI') and 
		a.cod_solicitante = 'E999999' and
		b.mem_campo = 'CARACTERIS' and
		upper(b.mem_texto) like '%ALT_%' and
		c.agr_nrointerno in (807,809,808,812,811,815,814)
	group by
		a.pet_nrointerno,
		a.cod_estado
	union all
	-- Nivel de prioridad: MEDIO
	select 
		a.pet_nrointerno,
		a.cod_estado
	from 
		GesPet..Peticion a inner join 
		GesPet..PeticionMemo b on a.pet_nrointerno = b.pet_nrointerno inner join 
		GesPet..AgrupPetic c on a.pet_nrointerno = c.pet_nrointerno
	where
		a.cod_tipo_peticion in ('AUX','AUI') and 
		a.cod_solicitante = 'E999999' and
		b.mem_campo = 'CARACTERIS' and
		upper(b.mem_texto) like '%MEDI_%' and
		c.agr_nrointerno in (807,809,808,812,811,815,814)
	group by
		a.pet_nrointerno,
		a.cod_estado
	union all
	-- Nivel de prioridad: BAJO
	select 
		a.pet_nrointerno,
		a.cod_estado
	from 
		GesPet..Peticion a inner join 
		GesPet..PeticionMemo b on a.pet_nrointerno = b.pet_nrointerno inner join 
		GesPet..AgrupPetic c on a.pet_nrointerno = c.pet_nrointerno
	where
		a.cod_tipo_peticion in ('AUX','AUI') and 
		a.cod_solicitante = 'E999999' and
		b.mem_campo = 'CARACTERIS' and
		upper(b.mem_texto) like '%BAJ_%' and
		c.agr_nrointerno in (807,809,808,812,811,815,814)
	group by
		a.pet_nrointerno,
		a.cod_estado
for read only 
go

begin tran 

declare @pet_nrointerno		int
declare @hst_nrointerno 	int
declare @pet_estado 		char(6)

	open cur_peticiones 
    fetch cur_peticiones into @pet_nrointerno, @pet_estado
    while (@@SQLSTATUS = 0) 
		begin 
			execute GesPet..sp_AddHistorial @hst_nrointerno output, @pet_nrointerno, 'HSTADD', @pet_estado, null, null, null, null, 'A82033'
			execute GesPet..sp_AddHistorialMemo @hst_nrointerno, 0, 
				'Cambio manual en el código de prioridad y normalización de campo «Características especiales», para regularizar códigos por importación de peticiones de auditoría.'
			fetch cur_peticiones into @pet_nrointerno, @pet_estado
		end
	close cur_peticiones
commit tran

deallocate cursor cur_peticiones

go