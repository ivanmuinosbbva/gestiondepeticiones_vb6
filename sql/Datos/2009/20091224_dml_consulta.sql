-- Nivel de prioridad: ALTO
select 
	'ALTO', 
	a.pet_nrointerno,
	a.pet_nroasignado,
	a.cod_tipo_peticion,
	a.cod_solicitante,
	a.prioridad,
	b.mem_texto
from 
	GesPet..Peticion a inner join 
	GesPet..PeticionMemo b on a.pet_nrointerno = b.pet_nrointerno inner join 
	GesPet..AgrupPetic c on a.pet_nrointerno = c.pet_nrointerno
where
	a.cod_tipo_peticion in ('AUX','AUI') and 
	a.cod_solicitante = 'E999999' and
	b.mem_campo = 'CARACTERIS' and
	upper(b.mem_texto) like '%ALT_%' and
	c.agr_nrointerno in (807,809,808,812,811,815,814)
union all

-- Nivel de prioridad: MEDIO
select 
	'MEDIO', 
	a.pet_nrointerno,
	a.pet_nroasignado,
	a.cod_tipo_peticion,
	a.cod_solicitante,
	a.prioridad,
	b.mem_texto
from 
	GesPet..Peticion a inner join 
	GesPet..PeticionMemo b on a.pet_nrointerno = b.pet_nrointerno inner join 
	GesPet..AgrupPetic c on a.pet_nrointerno = c.pet_nrointerno
where
	a.cod_tipo_peticion in ('AUX','AUI') and 
	a.cod_solicitante = 'E999999' and
	b.mem_campo = 'CARACTERIS' and
	upper(b.mem_texto) like '%MEDI_%' and
	c.agr_nrointerno in (807,809,808,812,811,815,814)
union all

-- Nivel de prioridad: BAJO
select 
	'BAJO', 
	a.pet_nrointerno,
	a.pet_nroasignado,
	a.cod_tipo_peticion,
	a.cod_solicitante,
	a.prioridad,
	b.mem_texto
from 
	GesPet..Peticion a inner join 
	GesPet..PeticionMemo b on a.pet_nrointerno = b.pet_nrointerno inner join 
	GesPet..AgrupPetic c on a.pet_nrointerno = c.pet_nrointerno
where
	a.cod_tipo_peticion in ('AUX','AUI') and 
	a.cod_solicitante = 'E999999' and
	b.mem_campo = 'CARACTERIS' and
	upper(b.mem_texto) like '%BAJ_%' and
	c.agr_nrointerno in (807,809,808,812,811,815,814)
go