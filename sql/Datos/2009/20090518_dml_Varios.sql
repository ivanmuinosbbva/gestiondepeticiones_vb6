-- Cambio de versión
update GesPet..Varios set var_texto = 'v5.6.0' where var_codigo = 'CGMVER'
go
-- Parametrización para el vencimiento de contraseña periódico
insert into GesPet..Varios (var_codigo, var_numero, var_texto, var_fecha) values ('LOGOFF', 0, 'N', getdate())
go
-- Parametrización para almacenar el histórico de contraseñas
insert into GesPet..Varios (var_codigo, var_numero, var_texto, var_fecha) values ('LOGHIS', 1, 12, getdate())
go
-- Parametrización de días para el RPT8
insert into GesPet..Varios (var_codigo, var_numero, var_texto, var_fecha) values ('RPT8', 7, '', getdate())
go

