-- Nuevas categor�as

print 'Agregado de categor�as (2)...'
go

insert into GesPet..ProyectoCategoria values (8, 'Proyectos Regulatorios')
go

insert into GesPet..ProyectoCategoria values (9, 'Proyectos 2008 en Estado Terminal')
go

-- Nuevas clases

print 'Agregado de clases (7)...'
go

insert into GesPet..ProyectoClase values (1, 7, 'Servicing')
go

insert into GesPet..ProyectoClase values (1, 8, 'Distribuci�n')
go

insert into GesPet..ProyectoClase values (1, 9, 'Industrializaci�n Operativa')
go

insert into GesPet..ProyectoClase values (1, 10, 'Reducci�n de costes')
go

insert into GesPet..ProyectoClase values (1, 11, 'Negocio Particulares')
go

insert into GesPet..ProyectoClase values (8, 0, 'N/A')
go

insert into GesPet..ProyectoClase values (9, 0, 'N/A')
go

-- Nuevos Proyectos IDM (29 nuevos proyectos IDM)

print 'Agregado de proyectos IDM (29)...'
go

insert into GesPet..ProyectoIDM values (1, 1, 1, 'Encoladores - Expansi�n', getdate(), 1, 7)
go

insert into GesPet..ProyectoIDM values (1, 1, 2, 'Reconocimiento Cliente', getdate(), 1, 7)
go

insert into GesPet..ProyectoIDM values (1, 5, 1, 'Cartel Digital - Expansi�n', getdate(), 1, 7)
go

insert into GesPet..ProyectoIDM values (1, 5, 2, 'Tasa Tarjeta', getdate(), 1, 7)
go

insert into GesPet..ProyectoIDM values (2, 4, 2, 'Kiosco Franc�s Express - Expansi�n', getdate(), 1, 7)
go

insert into GesPet..ProyectoIDM values (22, 2, 0, 'Bibe  - Extensi�n Prueba Piloto - CAV', getdate(), 1, 8)
go

insert into GesPet..ProyectoIDM values (24, 0, 0, 'Centros de Recaudaci�n ', getdate(), 1, 7)
go

insert into GesPet..ProyectoIDM values (25, 0, 0, 'Modelo de Atenci�n Telef�nica en Sucursal', getdate(), 1, 7)
go

insert into GesPet..ProyectoIDM values (26, 0, 0, 'Esquemas de Medici�n y Repositorio de Transacciones', getdate(), 1, 7)
go

insert into GesPet..ProyectoIDM values (27, 0, 0, 'NMA - Nuevo Modelo de Atenci�n', getdate(), 1, 8)
go

insert into GesPet..ProyectoIDM values (28, 0, 0, 'Legajo Digital del Cliente', getdate(), 1, 9)
go

insert into GesPet..ProyectoIDM values (30, 0, 0, 'PEP', getdate(), 8, 1)
go

insert into GesPet..ProyectoIDM values (31, 0, 0, 'Datos Mandatorios', getdate(), 8, 1)
go

insert into GesPet..ProyectoIDM values (32, 0, 0, 'Cierre Obligatorio de cuentas BCRA A 4809', getdate(), 8, 1)
go

insert into GesPet..ProyectoIDM values (33, 0, 0, 'Ley 26476 � Apertura Cuenta especial (por blanqueo de capitales)', getdate(), 8, 1)
go

insert into GesPet..ProyectoIDM values (34, 0, 0, 'Ley 26476 � Regularizaci�n impositiva (identificaci�n del depositante) ', getdate(), 8, 1)
go

insert into GesPet..ProyectoIDM values (35, 0, 0, 'Adaptaci�n de Plazos Fijos para la Anses', getdate(), 8, 1)
go

insert into GesPet..ProyectoIDM values (54, 1, 0, 'Nuevo Sistema de Comisiones - Etapa II', getdate(), 5, 0)
go

insert into GesPet..ProyectoIDM values (70, 1, 0, 'Franc�s Net Nuevo Sitio', getdate(), 1, 7)
go

insert into GesPet..ProyectoIDM values (70, 2, 0, 'Clientes.Datos en Canales', getdate(), 1, 7)
go

insert into GesPet..ProyectoIDM values (114, 0, 0, 'Reingenier�a del M�dulo de Financiamiento Activo y Pasivo y de Taller de Productos del Sistema Corporativo de Comextcorp', getdate(), 4, 0)
go

insert into GesPet..ProyectoIDM values (115, 0, 0, 'Ampliaci�n Consultiva y Operativa de Productos de Comercio Exterior por Internet ', getdate(), 4, 0)
go

insert into GesPet..ProyectoIDM values (116, 0, 0, 'Datawarehouse para RCF', getdate(), 4, 0)
go

insert into GesPet..ProyectoIDM values (117, 0, 0, 'Nueva L�nea de Pr�stamo Hipotecario', getdate(), 4, 0)
go

insert into GesPet..ProyectoIDM values (118, 0, 0, 'Manejo de carteras del propio Banco con distinta valuaci�n ', getdate(), 4, 0)
go

insert into GesPet..ProyectoIDM values (119, 0, 0, 'Compra venta de opciones', getdate(), 4, 0)
go

insert into GesPet..ProyectoIDM values (120, 0, 0, 'Nuevas regulaciones para protecci�n del inversor', getdate(), 4, 0)
go

insert into GesPet..ProyectoIDM values (121, 0, 0, 'Documentaci�n del producto Custodia para bancas minoristas', getdate(), 4, 0)
go

insert into GesPet..ProyectoIDM values (122, 0, 0, 'Captura de dep�sitos a Plazo Fijo para PSA', getdate(), 4, 0)
go


-- Actualizaci�n de proyectos IDM (93 proyectos)

print 'Actualizaci�n de proyectos IDM (93)...'
go

update GesPet..ProyectoIDM set ProjNom = 'Call Center Regional - Etapa II', ProjCatId = 5, ProjClaseId = 0 where ProjId = 11 and ProjSubId = 2 and ProjSubSId = 2
go

update GesPet..ProyectoIDM set ProjNom = 'Desarrollo / potenciaci�n de Canales para Autogesti�n', ProjCatId = 1, ProjClaseId = 7 where ProjId = 1 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Dep�sitos de Cheques en Empresas (TIBAF)', ProjCatId = 1, ProjClaseId = 7 where ProjId = 2 and ProjSubId = 5 and ProjSubSId = 1
go

update GesPet..ProyectoIDM set ProjNom = 'SIC - Informaci�n de Riesgo', ProjCatId = 2, ProjClaseId = 1 where ProjId = 61 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'BEM - Branch Effectiveness Modeling', ProjCatId = 9, ProjClaseId = 0 where ProjId = 1 and ProjSubId = 2 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Instalaci�n de CAF en puestos de caja', ProjCatId = 9, ProjClaseId = 0 where ProjId = 1 and ProjSubId = 3 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Cartel Digital', ProjCatId = 9, ProjClaseId = 0 where ProjId = 1 and ProjSubId = 5 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Plan de Migraci�n de Transacciones', ProjCatId = 9, ProjClaseId = 0 where ProjId = 2 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Instalaci�n de Totem', ProjCatId = 9, ProjClaseId = 0 where ProjId = 2 and ProjSubId = 1 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Centralizaci�n de Reclamos', ProjCatId = 9, ProjClaseId = 0 where ProjId = 2 and ProjSubId = 2 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Recambio de QDB Autoconsultas', ProjCatId = 9, ProjClaseId = 0 where ProjId = 2 and ProjSubId = 3 and ProjSubSId = 2
go

update GesPet..ProyectoIDM set ProjNom = 'Kiosco Franc�s Express', ProjCatId = 9, ProjClaseId = 0 where ProjId = 2 and ProjSubId = 4 and ProjSubSId = 1
go

update GesPet..ProyectoIDM set ProjNom = 'Impresi�n de Extractos en AU', ProjCatId = 9, ProjClaseId = 0 where ProjId = 2 and ProjSubId = 6 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Industrializaci�n Operativa', ProjCatId = 9, ProjClaseId = 0 where ProjId = 3 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Funcionalidad de ventas de paquetes en campa�as (Workflow de paquetes)', ProjCatId = 9, ProjClaseId = 0 where ProjId = 7 and ProjSubId = 2 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Expansi�n de prescriptores', ProjCatId = 9, ProjClaseId = 0 where ProjId = 8 and ProjSubId = 2 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Pr�stamos Pre-Aprobados por ATM', ProjCatId = 9, ProjClaseId = 0 where ProjId = 9 and ProjSubId = 1 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Pr�stamos Pre-Aprobados por Franc�s Net', ProjCatId = 9, ProjClaseId = 0 where ProjId = 9 and ProjSubId = 2 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Modelo de Ventas y Servicios', ProjCatId = 9, ProjClaseId = 0 where ProjId = 11 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Captura digital de Datos', ProjCatId = 9, ProjClaseId = 0 where ProjId = 11 and ProjSubId = 1 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Desarrollo de Televentas', ProjCatId = 9, ProjClaseId = 0 where ProjId = 11 and ProjSubId = 2 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Call Center  - Local', ProjCatId = 9, ProjClaseId = 0 where ProjId = 11 and ProjSubId = 2 and ProjSubSId = 1
go

update GesPet..ProyectoIDM set ProjNom = 'Entrega de comprobantes de Pensiones y Rentas', ProjCatId = 9, ProjClaseId = 0 where ProjId = 11 and ProjSubId = 3 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Retenci�n de cartera', ProjCatId = 9, ProjClaseId = 0 where ProjId = 11 and ProjSubId = 4 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = '@voluci�n', ProjCatId = 9, ProjClaseId = 0 where ProjId = 11 and ProjSubId = 5 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = '@voluci�n - Piloto', ProjCatId = 9, ProjClaseId = 0 where ProjId = 11 and ProjSubId = 5 and ProjSubSId = 1
go

update GesPet..ProyectoIDM set ProjNom = '@voluci�n - Sucursal II', ProjCatId = 9, ProjClaseId = 0 where ProjId = 11 and ProjSubId = 5 and ProjSubSId = 2
go

update GesPet..ProyectoIDM set ProjNom = 'BI Herramienta de Explotaci�n Integral', ProjCatId = 9, ProjClaseId = 0 where ProjId = 12 and ProjSubId = 3 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'BI - Modelo de Datos', ProjCatId = 9, ProjClaseId = 0 where ProjId = 12 and ProjSubId = 4 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Descentralizaci�n de Riesgos', ProjCatId = 9, ProjClaseId = 0 where ProjId = 13 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Iniciativas para la descentralizaci�n', ProjCatId = 9, ProjClaseId = 0 where ProjId = 13 and ProjSubId = 1 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Procedimiento de SMSV en NEV', ProjCatId = 9, ProjClaseId = 0 where ProjId = 13 and ProjSubId = 2 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Up grade de Tarjetas de cr�dito', ProjCatId = 9, ProjClaseId = 0 where ProjId = 14 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Promoci�n segmentable', ProjCatId = 9, ProjClaseId = 0 where ProjId = 16 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Altas en punto de venta', ProjCatId = 9, ProjClaseId = 0 where ProjId = 17 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Piloto Cerro Catedral', ProjCatId = 9, ProjClaseId = 0 where ProjId = 17 and ProjSubId = 1 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Centro de Informaci�n Consolidar', ProjCatId = 9, ProjClaseId = 0 where ProjId = 19 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'DWH Salud', ProjCatId = 9, ProjClaseId = 0 where ProjId = 19 and ProjSubId = 1 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Extractos por e-mail (Club de Bancos)', ProjCatId = 9, ProjClaseId = 0 where ProjId = 20 and ProjSubId = 1 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Bibe  - Prueba Piloto', ProjCatId = 9, ProjClaseId = 0 where ProjId = 22 and ProjSubId = 1 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'FIRCOSOFT', ProjCatId = 9, ProjClaseId = 0 where ProjId = 59 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Gestor de Campa�as', ProjCatId = 9, ProjClaseId = 0 where ProjId = 78 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Lar Host Argentina - Etapa I', ProjCatId = 9, ProjClaseId = 0 where ProjId = 152 and ProjSubId = 1 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'BBVA Cash', ProjCatId = 9, ProjClaseId = 0 where ProjId = 153 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Rating Empresas Corporativas', ProjCatId = 9, ProjClaseId = 0 where ProjId = 154 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Scorating', ProjCatId = 9, ProjClaseId = 0 where ProjId = 155 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'TECOM', ProjCatId = 9, ProjClaseId = 0 where ProjId = 157 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'BUP y Mejora de Calidad de Datos', ProjCatId = 9, ProjClaseId = 0 where ProjId = 50 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Traslados de ART', ProjCatId = 9, ProjClaseId = 0 where ProjId = 52 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Nuevo Sistema de Comisiones', ProjCatId = 9, ProjClaseId = 0 where ProjId = 54 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'DWH - Seguros 2da Etapa', ProjCatId = 9, ProjClaseId = 0 where ProjId = 57 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Permanencia de Paciente', ProjCatId = 9, ProjClaseId = 0 where ProjId = 58 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Contact Center / IVR (L�nea Franc�s / Franc�s Inversiones)', ProjCatId = 9, ProjClaseId = 0 where ProjId = 64 and ProjSubId = 2 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Informaci�n de Seguimiento', ProjCatId = 9, ProjClaseId = 0 where ProjId = 67 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Banca Patrimonial', ProjCatId = 9, ProjClaseId = 0 where ProjId = 83 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Leasing para Empleados', ProjCatId = 9, ProjClaseId = 0 where ProjId = 86 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Leasing para Empleados - Piloto', ProjCatId = 9, ProjClaseId = 0 where ProjId = 86 and ProjSubId = 1 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Leasing para Empleados - Etapa I', ProjCatId = 9, ProjClaseId = 0 where ProjId = 86 and ProjSubId = 2 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Atenci�n de Clientes corporativos en sucursales individuos', ProjCatId = 9, ProjClaseId = 0 where ProjId = 93 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Factura electr�nica', ProjCatId = 9, ProjClaseId = 0 where ProjId = 95 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'AMEX : Pedido de Reposici�n y carga Autom�tica de Cheques', ProjCatId = 9, ProjClaseId = 0 where ProjId = 96 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Implantaci�n de Star', ProjCatId = 9, ProjClaseId = 0 where ProjId = 98 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Carga de operaciones de Distribuci�n sobre Cuenta Corriente', ProjCatId = 9, ProjClaseId = 0 where ProjId = 100 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Unificaci�n de los procesos de captura de precios de cierre', ProjCatId = 9, ProjClaseId = 0 where ProjId = 101 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Im�genes', ProjCatId = 9, ProjClaseId = 0 where ProjId = 103 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Visualizaci�n de Im�genes por J�piter en la Red', ProjCatId = 9, ProjClaseId = 0 where ProjId = 103 and ProjSubId = 1 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Continuidad del Negocio', ProjCatId = 9, ProjClaseId = 0 where ProjId = 106 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Sinergia de las �reas de Compras del Banco con Consolidar', ProjCatId = 9, ProjClaseId = 0 where ProjId = 111 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'LAR - Pensiones', ProjCatId = 9, ProjClaseId = 0 where ProjId = 158 and ProjSubId = 1 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Crecimiento CPU de Mainframe (500-1000 Mips)', ProjCatId = 9, ProjClaseId = 0 where ProjId = 170 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Renovaci�n de L�nea SUN , Consolidaci�n de equipos medianos  - peque�os', ProjCatId = 9, ProjClaseId = 0 where ProjId = 172 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Upgrade de la Contingencia de Sun', ProjCatId = 9, ProjClaseId = 0 where ProjId = 173 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Tarjeta de Coordenadas para Canales Alternativos Individuos', ProjCatId = 9, ProjClaseId = 0 where ProjId = 183 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Reingenier�a Esquema de Seguridad Perimetral', ProjCatId = 9, ProjClaseId = 0 where ProjId = 184 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Replanteo administraci�n Seguridad en DB2 (Informe de Auditoria)', ProjCatId = 9, ProjClaseId = 0 where ProjId = 186 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Digitalizaci�n del sistema CCTV en toda la red', ProjCatId = 9, ProjClaseId = 0 where ProjId = 190 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Franc�s Net / Redise�o', ProjCatId = 1, ProjClaseId = 7 where ProjId = 70 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Encoladores', ProjCatId = 1, ProjClaseId = 7 where ProjId = 1 and ProjSubId = 1 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Servicing Operativo', ProjCatId = 1, ProjClaseId = 7 where ProjId = 1 and ProjSubId = 4 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Identificaci�n Biom�trica de Clientes', ProjCatId = 1, ProjClaseId = 7 where ProjId = 1 and ProjSubId = 6 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Recambio de ATM y QDB', ProjCatId = 1, ProjClaseId = 7 where ProjId = 2 and ProjSubId = 3 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Recambio de ATM', ProjCatId = 1, ProjClaseId = 7 where ProjId = 2 and ProjSubId = 3 and ProjSubSId = 1
go

update GesPet..ProyectoIDM set ProjNom = 'Kiosco en Sucursales', ProjCatId = 1, ProjClaseId = 7 where ProjId = 2 and ProjSubId = 4 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Reingenier�a de Dep�sitos', ProjCatId = 1, ProjClaseId = 7 where ProjId = 2 and ProjSubId = 5 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Dep�sitos de Cheques con im�genes por QDB', ProjCatId = 1, ProjClaseId = 7 where ProjId = 2 and ProjSubId = 5 and ProjSubSId = 2
go

update GesPet..ProyectoIDM set ProjNom = 'Nueva navegaci�n Autoservicio', ProjCatId = 1, ProjClaseId = 7 where ProjId = 2 and ProjSubId = 7 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Nueva navegaci�n IVR', ProjCatId = 1, ProjClaseId = 7 where ProjId = 2 and ProjSubId = 8 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Industrializaci�n Operativa Etapa II', ProjCatId = 1, ProjClaseId = 10 where ProjId = 3 and ProjSubId = 1 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Portal Comercial', ProjCatId = 1, ProjClaseId = 8 where ProjId = 7 and ProjSubId = 3 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'AUR - Activaci�n, Utilizaci�n y Retenci�n de Clientes', ProjCatId = 1, ProjClaseId = 11 where ProjId = 7 and ProjSubId = 4 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Proceso de Correspondencia', ProjCatId = 1, ProjClaseId = 10 where ProjId = 20 and ProjSubId = 0 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Distribuci�n de Extractos', ProjCatId = 1, ProjClaseId = 10 where ProjId = 20 and ProjSubId = 2 and ProjSubSId = 0
go

update GesPet..ProyectoIDM set ProjNom = 'Bibe', ProjCatId = 1, ProjClaseId = 8 where ProjId = 22 and ProjSubId = 0 and ProjSubSId = 0
go

