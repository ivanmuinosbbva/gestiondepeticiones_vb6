-- Borro todos los campos del vuelco inicial
delete from GesPet..HEDT003
where dsn_id like 'V%'

-- Vuelvo a cargar el vuelco inicial
insert into GesPet..HEDT003 values ('V0000001', 'ATMCAF4', 'CAFKEY', '1', '19', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000001', 'ATMCAF4', 'CAFNOMBRE', '174', '30', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000001', 'ATMCAF4', 'CUENTA', '329', '340', '9', '', 'Tipo de cuenta','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000004', 'ATMFACT', 'MOV-PAN', '5', '19', 'X', '', 'Nro. de tarjeta','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000011', 'ATMINVE1', 'INV-NRO-TARJETA', '1', '19', 'X', '', 'Nro tarjeta','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000011', 'ATMINVE1', 'INV-NOM', '30', '24', 'X', '', 'Apellido y nombre','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000012', 'ATMDEP', 'DEPOACCPAN', '18', '19', 'X', '', 'Nro tarjeta','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000012', 'ATMDEP', 'DEPOTNCTA', '94', '10', 'X', '', 'Sucursar, cuenta y d�gito','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000012', 'ATMDEP', 'DEPONOMCTA', '122', '32', 'X', '', 'Denominacion de la cuenta','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000026', 'OGTCEMS', 'EMS-EMISORA-EXT', '6', '10', 'X', '', 'nombre corto','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000026', 'OGTCEMS', 'EMS-NOMBRE-EMISORA', '21', '40', 'X', '', 'nombre','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000026', 'OGTCEMS', 'EMS-CUENTA-PRIMERA', '114', '20', 'X', '', 'cta (eeeesssstttcccccdrr)','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000026', 'OGTCEMS', 'EMS-CUENTA-SEGUNDA', '134', '20', 'X', '', 'cta (eeeesssstttcccccdrr)','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000026', 'OGTCEMS', 'EMS-CODIGO-NIF', '237', '14', 'X', '', 'cuit-sec','HE8CCUIT', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000031', 'CAH01091', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000032', 'CAH25031', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000033', 'CAH23031', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000034', 'CAH06031', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000035', 'CAH39011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000036', 'CAH03011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000037', 'CAH04011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000038', 'CAH08011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000039', 'CAH11021', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000040', 'CAH13061', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000041', 'CAH22011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000042', 'CAH30021', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA1', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000043', 'CAH41011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA1', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000044', 'CAH50021', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA1', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000045', 'CAH52011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA1', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000046', 'CAH59011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA1', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000047', 'CAH73011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA1', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000048', 'CAH87011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA1', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000049', 'CAH91011', 'CUENTA', '3', '12', 'X', '', '','HE8CCTA1', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000050', 'CAH94011', 'CUENTA', '102', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000051', 'CAH98031', 'CUENTA', '114', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000052', 'CAH01091', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000053', 'CPA04081', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000054', 'CPA04081', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000055', 'CAH01091', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000067', 'CCP67011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000069', 'CCP73011', 'CUENTA', '8', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000070', 'CCP75011', 'CUENTA', '8', '25', 'X', '', '','HE8CCTA4', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000071', 'CCP81011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000072', 'CCP97011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000073', 'CPA32012', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000769', 'GPC4221', 'GPC4221-APELLIDO', '41', '55', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000770', 'GPC4221', 'GPC4221-APELLIDO', '41', '55', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000771', 'GPC4222', 'GPC4222-APELLIDO', '34', '55', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000772', 'GPC4222', 'GPC4222-APELLIDO', '34', '55', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000773', 'GPC4222', 'GPC4222-APELLIDO', '34', '55', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000774', 'GPC4223', 'GPC4223-APELLIDO', '18', '55', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000781', 'GPEC6301', 'GPEC6301-CUIT-EXP', '18', '11', '9', '', '','HE8CCUIT', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000782', 'GPEC6302', 'GPEC6301-CUIT-EXP', '18', '11', '9', '', '','HE8CCUIT', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000783', 'GPEC6303', 'GPEC6301-CUIT-EXP', '18', '11', '9', '', '','HE8CCUIT', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000857', 'GPTC0641', 'T064-NRO-CUIT-IMP', '1', '11', 'X', '', '','HE8CCUIT', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000857', 'GPTC0641', 'T064-NRO-CLIENTE   ', '12', '7', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000859', 'GPTC0651', 'T065-NRO-CUIT-IMP', '1', '11', 'X', '', '','HE8CCUIT', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000859', 'GPTC0651', 'T065-NRO-CLIENTE', '12', '7', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000862', 'GPTC0671', 'T067-DENOMINACION', '13', '30', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000875', 'GPTC0741', 'T741-GPMBCLB1', '25', '70', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000890', 'GPTC0821', 'T082-NRO-ID', '60', '11', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000890', 'GPTC0821', 'T082-BENF-EXTE', '89', '50', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000890', 'GPTC0821', 'T082-NOM-CLIENTE', '305', '50', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0000902', 'GPTC0921', 'T921-GPDTADI', '322', '60', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001008', 'DLTCEMB0', 'DOCUMENTO', '73', '15', 'X', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001008', 'DLTCEMB0', 'APEYNOM', '88', '50', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001008', 'DLTCEMB0', 'APEYNOMABREV', '193', '16', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001011', 'DLEC0220', 'APEYNOM', '9', '50', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001011', 'DLEC0220', 'NROTEL', '73', '12', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001035', 'TCPASRE', 'DOCUMENTO', '18', '12', 'X', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001049', 'TJWC1002', 'DOCUMENTO', '3', '15', 'X', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001049', 'TJWC1002', 'NOMYAP-D', '29', '50', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001050', 'TJWC1003', 'NYAP-D', '69', '50', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001050', 'TJWC1003', 'DOCUMENTO', '120', '15', 'X', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001389', 'CBT03011', 'CUENTA', '9', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001389', 'CBT03011', 'NRO-SECU-PFIJO', '21', '3', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001390', 'CBT17011', 'COD-PREGUNTA', '20', '2', 'X', '', '','HE8CCAR0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001390', 'CBT17011', 'COD-RESULTADO', '24', '1', 'X', '', '','HE8CCAR0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001391', 'CBT19011', 'COD-CBU', '8', '22', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001392', 'CBT20011', 'CLA-CANAL', '12', '10', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001393', 'CBT27011', 'NRO-CUENTA', '42', '12', 'X', '', '','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001394', 'CBT34011', 'TELEFONO', '10', '13', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001394', 'CBT34011', 'COD-OPERADOR', '23', '4', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001395', 'CBT35011', 'CUENTA', '15', '25', 'X', '', '','HE8CCTA4', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001396', 'CBT36011', 'COD-CBU', '29', '22', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001396', 'CBT36011', 'NRO-CUIL', '52', '11', 'X', '', '','HE8CCUIT', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001396', 'CBT36011', 'IMP-LIM-OTOR', '106', '8', '9', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001396', 'CBT36011', 'IMP-LIM-USADO', '114', '8', '9', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001396', 'CBT36011', 'ID-UNI-ULT-OPER', '122', '19', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001415', 'CNEG0033', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001420', 'DESTINO1', 'CUENTA ', '10', '17', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001420', 'DESTINO1', 'NOMBREC', '31', '29', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001420', 'DESTINO1', 'CUENTAE', '60', '23', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001421', 'DESTINO2', 'CUENTA ', '10', '17', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001422', 'DATAN16', 'CUENTA', '7', '19', '9', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001423', 'DATAN19', 'CUENTA ', '10', '17', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001423', 'DATAN19', 'CUENTAE', '27', '23', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001423', 'DATAN19', 'NOMBRE CLIENTE', '77', '24', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001424', 'RECAUD1', 'CUENTA', '1', '12', '9', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001425', 'TELEDEP1', 'CUENTA', '1', '12', '9', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001426', 'TELEDEP2', 'CUENTA', '1', '12', '9', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001427', 'TELEDIS1', 'CUENTA', '1', '12', '9', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001428', 'DEBCONS', 'CTA-CCC', '67', '18', '9', '', '','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001429', 'MERGALL', 'CTA-CCC', '67', '18', '9', '', '','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001477', 'DNC4305', 'DSF5-NOMBRE', '29', '55', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001481', 'DNC4310', 'DN4310-NOMBRE', '29', '55', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001484', 'DNC0227', 'PA-NROCTA', '19', '4', 'PD', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001493', 'DNC0080', 'TD-NROCTA', '15', '4', 'PD', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001494', 'DNC0092', 'TS-NOMBRE', '9', '25', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001495', 'DNS0030', 'L-NROCTA', '4', '4', 'PD', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001496', 'DNX1300', 'PCW-NROCTA', '19', '4', 'PD', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001497', 'DNS0030', 'L-NROCTA', '4', '4', 'PD', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001498', 'DNC0780', 'LOUT-NROCTA', '5', '7', 'ZD', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001502', 'DNC9711A', 'ASC2-NOMBRE', '18', '35', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001507', 'DNC0980N', 'DEUDOR-NOMBRE', '12', '60', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001510', 'DNC0980N', 'DEUDOR-NOMBRE', '12', '60', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001514', 'DNC0980', 'CENDEU-NOMCLI', '60', '55', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001515', 'DNCTCL01', 'NOMBRE', '23', '95', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001516', 'DNCTCL16', 'NOMBRE', '38', '95', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001611', 'PKFCMAN', 'PKFCMAND-CUENTA', '9', '20', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001611', 'PKFCMAN', 'PKFCMAND-NOM-CTE', '61', '60', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001637', 'CLDFIRCO', 'CLDFIRCO-NOMBRE', '9', '18', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001638', 'PEPS001', 'WSV-CUIT-PEP', '1', '11', 'X', '', '','HE8CCUIT', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001638', 'PEPS001', 'WSV-APE-NOM-PEP', '13', '80', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001638', 'PEPS001', 'WSV-DOCUMENTO', '94', '22', 'X', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001725', 'CTK06011', 'CUENTA', '37', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001729', 'CTK85011', 'CUENTA', '20', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001733', 'PUTCACU', 'CUENTA', '30', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001735', 'PUTCCTA', 'CUENTA', '10', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001736', 'PUTCCTC', 'CUENTA', '34', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001740', 'YQTCCON', 'CUENTA', '35', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001741', 'YQTCLOG', 'NRO-CONT-1', '36', '18', 'PD', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001741', 'YQTCLOG', 'CUENTA-1', '80', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001741', 'YQTCLOG', 'NRO-CONT-2', '335', '18', 'PD', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001741', 'YQTCLOG', 'CUENTA-2', '369', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001742', 'YQTCVEN', 'DOCUMENTO', '53', '15', 'X', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001742', 'YQTCVEN', 'NOM-APELL', '68', '30', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001743', 'FBTCADH', 'ADH-CCONTRIB', '21', '2', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001743', 'FBTCADH', 'ADH-NCONTRIB', '23', '13', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001796', 'CBZ51011', 'CUENTA-1', '66', '12', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001796', 'CBZ51011', 'CUENTA-2', '78', '12', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001797', 'CCP03011', 'CUENTA', '7', '12', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001798', 'CCP08011', 'CUENTA', '1', '12', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001799', 'CCP35011', 'CUENTA', '3', '12', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001800', 'CCP37011', 'CUENTA', '34', '12', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001801', 'CCP43011', 'CUENTA', '4', '12', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001802', 'CCP45011', 'CUENTA', '1', '12', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001803', 'CCP46011', 'CUENTA', '4', '12', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001804', 'CCP57011', 'CUENTA', '6', '25', 'X', '', '','HE8CCTA4', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001805', 'CCP59011', 'CUENTA', '8', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001806', 'CCP63011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001808', 'CCP72011', 'CUENTA-1', '23', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001808', 'CCP72011', 'CUENTA-2', '35', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001810', 'CCP75011', 'CUENTA', '8', '25', 'X', '', '','HE8CCTA4', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001814', 'CPA03031', 'CUENTA', '33', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001816', 'CPS34011', 'CUENTA', '33', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001817', 'CPS34011', 'CUENTA', '33', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001818', 'CTA01011', 'CUENTA', '33', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001819', 'CTA03021', 'CUENTA', '143', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001820', 'CTA08011', 'CUENTA', '145', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001821', 'CTA10011', 'CUENTA', '51', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001822', 'CTA11011', 'CUENTA', '145', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001823', 'CTA01021', 'CUENTA', '13', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001824', 'CTA03011', 'CUENTA', '49', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001825', 'CVA18011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001826', 'CAH02011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001827', 'CAH05011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001828', 'CAH58011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001830', 'BGGTSER', 'CUENTA', '5', '14', 'X', '', '','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001831', 'BGGTMAR', 'CUENTA', '9', '14', 'X', '', '','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001832', 'BGGTMRC', 'CUENTA', '5', '14', 'X', '', '','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001833', 'BGGTCGC', 'CUENTA', '5', '14', 'X', '', '','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001839', 'CPQ66011', 'CUENTA', '1', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001839', 'CPQ66011', 'CPQ66011-COMENT', '21', '70', 'X', '', '','HE8CCAR0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001840', 'CPQ98011', 'CUENTA', '13', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001847', 'CRC13011', 'NRO-CUIL-BENF', '40', '6', '9', '', '','HE8CCUIT', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001847', 'CRC13011', 'APE-NOMB-BENF', '46', '27', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001847', 'CRC13011', 'DOCUMENTO-1', '73', '7', 'PD', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001847', 'CRC13011', 'NRO-CUIL-APOD', '82', '6', '9', '', '','HE8CCUIT', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001847', 'CRC13011', 'APE-NOMB-APOD', '88', '27', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001847', 'CRC13011', 'DOCUMENTO-2', '115', '7', 'PD', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001848', 'CRC14011', 'CUENTA', '8', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001848', 'CRC14011', 'APELL-NOM-BENF', '21', '27', 'PD', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001848', 'CRC14011', 'DOCUMENTO', '64', '7', 'PD', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001850', 'RCTCAFP0', 'DOCUMENTO', '100', '8', 'PD', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001851', 'RCTCMOV0', 'CUENTA', '12', '32', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001851', 'RCTCMOV0', 'NOMBRE', '136', '60', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001852', 'RCTCDET0', 'CUENTA', '12', '18', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001853', 'RCTCVLR0', 'CUENTA-1', '12', '18', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001853', 'RCTCVLR0', 'CUENTA-2', '132', '18', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001856', 'JBVCDIM0', 'DOCUMENTO', '15', '22', 'X', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001857', 'WWGTPER', 'DOCUMENTO', '14', '20', 'X', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001857', 'WWGTPER', 'NOMBRE', '42', '70', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001858', 'CLDTCBCR', 'BCR-NRO-CUENTA', '8', '10', '9', '', '','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001858', 'CLDTCBCR', 'BCR-TI-ID-TRIB', '142', '2', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001858', 'CLDTCBCR', 'BCR-NRO-ID-TRIB', '144', '11', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001858', 'CLDTCBCR', 'DOCUMENTO', '155', '15', 'X', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001858', 'CLDTCBCR', 'NOMBRE', '170', '55', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001859', 'CLDTCDOC', 'CLDTCDOC-NOM-CLI', '16', '55', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001860', 'CLDTCOPE', 'OPE-NRO-CUENTA', '8', '10', '9', '', '','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001860', 'CLDTCOPE', 'OPE-TI-ID-TRIB', '145', '2', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001860', 'CLDTCOPE', 'OPE-NRO-ID-TRIB', '147', '11', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001860', 'CLDTCOPE', 'DOCUMENTO', '158', '15', 'X', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001860', 'CLDTCOPE', 'NOMBRE', '173', '55', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001864', 'CNETCCCH', 'CUENTA', '4', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001865', 'CNETCCCT', 'CUENTA', '4', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001866', 'CNETCCGT', 'CUENTA', '4', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001867', 'CNETCCPE', 'CUENTA', '18', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001868', 'CNE03011', 'CUENTA', '8', '12', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001869', 'CNETCMAR', 'CUENTA', '1', '22', 'X', '', '','HE8CCTA0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001873', 'TJTCADI', 'ADI-NRO-CLIEN', '59', '8', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001873', 'TJTCADI', 'ADI-NRO', '70', '13', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001873', 'TJTCADI', 'ADI-NRO-CBU', '83', '22', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001875', 'TJTCEXT', 'CUENTA-1', '60', '14', 'X', '', '','HE8CCTA5', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001875', 'TJTCEXT', 'CTA-CRED-PAGO', '77', '10', 'X', '', '','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001875', 'TJTCEXT', 'TARJETA-PAGO', '87', '16', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001875', 'TJTCEXT', 'CUENTA-2', '107', '14', 'X', '', '','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001876', 'TJTCLIQ', 'CUENTA-1', '89', '14', 'X', '', '','HE8CCTA5', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001876', 'TJTCLIQ', 'CUENTA-2', '107', '14', 'X', '', '','HE8CCTA5', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001877', 'TJTCMAC', 'NRO-SOCIO-TIT', '58', '16', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001877', 'TJTCMAC', 'MAC-TX-DENOMINACION', '197', '40', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001877', 'TJTCMAC', 'CUENTA-1', '253', '14', 'X', '', '','HE8CCTA5', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001877', 'TJTCMAC', 'CUENTA-2', '271', '14', 'X', '', '','HE8CCTA5', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001879', 'TJTCMAT', 'MAT-TI-DOC', '271', '15', 'X', '', '','HE8CDOC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001879', 'TJTCMAT', 'SECUENCIA-TAR', '286', '16', 'X', '', '','HE8CNRO0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001880', 'TJTCMOM', 'CCC-SUC-DEBITO', '70', '4', 'X', '', '','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001880', 'TJTCMOM', 'CCC-CTA-DEBITO', '74', '10', 'X', '', '','HE8CCCC0', getdate(), null,'A', null)
GO
insert into GesPet..HEDT003 values ('V0001880', 'TJTCMOM', 'TX-DENOMINACION', '103', '40', 'X', '', '','HE8CAYN0', getdate(), null,'A', null)
GO