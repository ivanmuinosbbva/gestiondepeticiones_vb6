truncate table GesPet..HEDT001
go

insert into GesPet..HEDT001 values ('V0000001', 'TB.D002.KSDS.MAES.CAF.BBV', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000004', 'TB.D062.KSDS.MOVI.ATMMOV', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000011', 'TB.D006.KSDS.MAES.INVENT.BBV', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000012', 'TB.D014.KSDS.MOVI.ATMDEP', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000015', 'TB.D021.KSDS.PARA.STC.PARAM.BBV ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000016', 'TB.D100.KSDS.PARAM.SEGM.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000026', 'OGFD.BAT1SBAS.A00OGEMS', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000031', 'BGFD.TBASICUE.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000032', 'BGFD.TSALDIACC.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000033', 'BGFD.TSALDCA.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000034', 'BGFD.ACUERDOS.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000035', 'BGFD.ACUERDIA.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000036', 'BGFD.MOVICUE.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000037', 'BGFD.MOVILIB.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000038', 'BGFD.TCONFORM.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000039', 'BGFD.TVONP.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000040', 'BGFD.RECHAZADO.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000041', 'BGFD.TCAMPER.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000042', 'BGFD.TSALDEFCA.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000043', 'BGFD.LIQACUER.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000044', 'BGFD.TSALDEFCC.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000045', 'BGFD.TSALDEVE.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000046', 'BGFD.MAYSDEUCC.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000047', 'BGFD.TBOLETA.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000048', 'BGFD.TELEDIS.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000049', 'BGFD.RELCAMP.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000050', 'BGFD.TMEP.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000051', 'BGFD.TRANSFER.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000052', 'BGFD.TBK01.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000053', 'BGFD.TRETEPAP.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000054', 'BGFD.TBK04.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000055', 'BGFD.TBK012.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000067', 'BGFD.TSALDIAFD.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000069', 'BGFD.SUSCFAX.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000071', 'BGFD.COMPBATCH.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000072', 'BGFD.CAMBESTADO.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000073', 'BGFD.JOURNAL.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000075', 'XCFD.XCDT742.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000076', 'XCFD.XCDTABC.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000077', 'XCFD.XCDTAC0.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000078', 'XCFD.XCDTAFE.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000079', 'XCFD.XCDTAFI.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000081', 'XCFD.XCDTALA.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000082', 'XCFD.XCDTAR1.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000083', 'XCFD.XCDTAR2.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000084', 'XCFD.XCDTAR3.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000085', 'XCFD.XCDTARA.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000086', 'XCFD.XCDTAVA.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000087', 'XCFD.XCDTBAN.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000088', 'XCFD.XCDTCIN.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000089', 'XCFD.XCDTCLA.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000090', 'XCFD.XCDTCOP.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000091', 'XCFD.XCDTCOR.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000092', 'XCFD.XCDTCOU.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000093', 'XCFD.XCDTCUA.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000094', 'XCFD.XCDTDI1.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000095', 'XCFD.XCDTDIS.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000096', 'XCFD.XCDTEMB.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000097', 'XCFD.XCDTES0.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000098', 'XCFD.XCDTES1.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000099', 'XCFD.XCDTFAC.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000100', 'XCFD.XCDTFON.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000101', 'XCFD.XCDTFSG.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000102', 'XCFD.XCDTFUN.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000103', 'XCFD.XCDTGCO.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000104', 'XCFD.XCDTHIS.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000105', 'XCFD.XCDTHJF.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000106', 'XCFD.XCDTIBC.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000107', 'XCFD.XCDTICO.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000108', 'XCFD.XCDTIFO.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000109', 'XCFD.XCDTIN0.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000110', 'XCFD.XCDTIN1.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000111', 'XCFD.XCDTIN2.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000112', 'XCFD.XCDTIN3.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000113', 'XCFD.XCDTINC.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000114', 'XCFD.XCDTINS.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000115', 'XCFD.XCDTINV.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000116', 'XCFD.XCDTL01.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000117', 'XCFD.XCDTL04.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000118', 'XCFD.XCDTL05.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000119', 'XCFD.XCDTL07.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000120', 'XCFD.XCDTL08.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000121', 'XCFD.XCDTL09.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000122', 'XCFD.XCDTL10.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000123', 'XCFD.XCDTL11.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000124', 'XCFD.XCDTL12.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000125', 'XCFD.XCDTL13.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000126', 'XCFD.XCDTL15.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000127', 'XCFD.XCDTL16.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000128', 'XCFD.XCDTL17.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000129', 'XCFD.XCDTL18.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000130', 'XCFD.XCDTL19.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000131', 'XCFD.XCDTLCG.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000132', 'XCFD.XCDTLCI.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000133', 'XCFD.XCDTLGH.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000134', 'XCFD.XCDTLOG.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000135', 'XCFD.XCDTM71.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000136', 'XCFD.XCDTM72.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000137', 'XCFD.XCDTMBC.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000138', 'XCFD.XCDTMDC.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000139', 'XCFD.XCDTMDF.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000140', 'XCFD.XCDTMER.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000141', 'XCFD.XCDTMOP.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000142', 'XCFD.XCDTOP0.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000143', 'XCFD.XCDTOP1.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000144', 'XCFD.XCDTOP2.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000145', 'XCFD.XCDTOP3.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000146', 'XCFD.XCDTOP4.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000147', 'XCFD.XCDTOP5.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000148', 'XCFD.XCDTOP6.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000149', 'XCFD.XCDTOP7.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000151', 'XCFD.XCDTOPN.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000152', 'XCFD.XCDTOPP.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000153', 'XCFD.XCDTOPS.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000154', 'XCFD.XCDTOPT.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000155', 'XCFD.XCDTOPW.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000156', 'XCFD.XCDTOPX.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000157', 'XCFD.XCDTOR1.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000158', 'XCFD.XCDTPAT.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000159', 'XCFD.XCDTPEM.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000160', 'XCFD.XCDTPET.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000161', 'XCFD.XCDTPJ1.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000162', 'XCFD.XCDTPJ2.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000163', 'XCFD.XCDTPJ3.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000164', 'XCFD.XCDTPRP.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000165', 'XCFD.XCDTPTA.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000166', 'XCFD.XCDTRA0.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000167', 'XCFD.XCDTRA1.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000168', 'XCFD.XCDTRA2.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000169', 'XCFD.XCDTRA3.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000170', 'XCFD.XCDTRA4.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000171', 'XCFD.XCDTRA5.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000172', 'XCFD.XCDTRA6.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000173', 'XCFD.XCDTRA7.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000174', 'XCFD.XCDTRA8.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000175', 'XCFD.XCDTRA9.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000176', 'XCFD.XCDTRAA.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000177', 'XCFD.XCDTRE1.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000178', 'XCFD.XCDTREV.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000179', 'XCFD.XCDTRTR.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000180', 'XCFD.XCDTSAC.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000181', 'XCFD.XCDTSAL.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000182', 'XCFD.XCDTSBC.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000183', 'XCFD.XCDTSEC.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000184', 'XCFD.XCDTSMO.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000185', 'XCFD.XCDTSTM.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000186', 'XCFD.XCDTSTR.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000187', 'XCFD.XCDTSUI.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000188', 'XCFD.XCDTSW0.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000189', 'XCFD.XCDTSW1.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000190', 'XCFD.XCDTSW2.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000191', 'XCFD.XCDTSW3.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000192', 'XCFD.XCDTSW4.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000193', 'XCFD.XCDTSW5.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000194', 'XCFD.XCDTSW6.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000195', 'XCFD.XCDTSW7.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000196', 'XCFD.XCDTSW8.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000197', 'XCFD.XCDTSW9.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000198', 'XCFD.XCDTSWA.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000199', 'XCFD.XCDTSWB.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000200', 'XCFD.XCDTSWC.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000201', 'XCFD.XCDTSWD.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000202', 'XCFD.XCDTSWE.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000203', 'XCFD.XCDTSWF.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000204', 'XCFD.XCDTSWG.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000205', 'XCFD.XCDTSWH.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000206', 'XCFD.XCDTSWI.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000207', 'XCFD.XCDTSWJ.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000209', 'XCFD.XCDTSWL.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000210', 'XCFD.XCDTSWM.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000211', 'XCFD.XCDTTA1.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000212', 'XCFD.XCDTTCL.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000213', 'XCFD.XCDTTCS.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000214', 'XCFD.XCDTTMF.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000215', 'XCFD.XCDTTR1.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000216', 'XCFD.XCDTTTP.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000217', 'XCFD.XCDTTTR.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000218', 'XCFD.XCDTTXL.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000219', 'XCFD.XCDTVAS.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000220', 'XCFD.XCDTVCF.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000222', 'XCFD.XCDTWB2.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000223', 'XCFD.XCDTWB3.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000224', 'XCFD.XCDTWB4.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000225', 'XCFD.XCDTWB5.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000226', 'XCFD.XCDTWB6.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000227', 'XCFD.XCDTWBE.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000228', 'XCFD.XCDTXNS.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000229', 'XCFD.XCDTXP0.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000230', 'XCFD.XCDTXP2.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000231', 'XCFD.XCDTXP3.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000232', 'XCFD.XCDTXP4.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000233', 'XCFD.XCDTXP7.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000235', 'XCFD.XCDTXPP.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000236', 'XCFD.XCDTXPW.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000237', 'XCFD.XCDTXTR.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000238', 'XCFD.XCDTY01.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000239', 'XCFD.XCDTY02.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000240', 'XCFD.XCDTY03.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000241', 'XCFD.XCDTY04.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000242', 'XCFD.XCDTY05.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000243', 'XCFD.XCDTY06.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000244', 'XCFD.XCDTY07.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000245', 'XCFD.XCDTY11.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000246', 'XCFD.XCDTY12.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000247', 'XCFD.XCDTY13.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000248', 'XCFD.XCDTY14.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000249', 'XCFD.XCDTY15.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000250', 'XCFD.XCDTY16.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000251', 'XCFD.XCDTY17.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000252', 'XCFD.XCDTY18.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000253', 'XCFD.XCDTY21.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000254', 'XCFD.XCDTY22.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000255', 'XCFP.BAT1NBAS.DATGEN1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000256', 'XCFP.BAT1NBAS.DATOS1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000257', 'XCFP.BAT1NBAS.DIVISAS.BCOFRAN', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000258', 'XCFP.BAT1NBAS.DIVISAS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000259', 'XCFP.BAT1NBAS.DIVISAS.OTROS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000260', 'XCFP.BAT1NBAS.SISCEN05', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000261', 'XCFP.BAT1NBAS.XCDTOP1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000266', 'XCFP.BAT1SBAS.ACTICANC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000267', 'XCFP.BAT1SBAS.ARCHRESU', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000268', 'XCFP.BAT1SBAS.ARCHTCLA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000269', 'XCFP.BAT1SBAS.ARCHTMOP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000270', 'XCFP.BAT1SBAS.ARCHTSDO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000271', 'XCFP.BAT1SBAS.AVIRECDE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000272', 'XCFP.BAT1SBAS.AVIRECDI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000273', 'XCFP.BAT1SBAS.AVIRECOI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000274', 'XCFP.BAT1SBAS.AVIREFIA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000275', 'XCFP.BAT1SBAS.AVISOCDE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000276', 'XCFP.BAT1SBAS.AVISOCDI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000277', 'XCFP.BAT1SBAS.AVISOCOE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000278', 'XCFP.BAT1SBAS.AVISOCOI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000279', 'XCFP.BAT1SBAS.AVISOFIA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000280', 'XCFP.BAT1SBAS.A000F060', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000281', 'XCFP.BAT1SBAS.A000SOR1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000282', 'XCFP.BAT1SBAS.A000SOR2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000283', 'XCFP.BAT1SBAS.A000SV20', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000284', 'XCFP.BAT1SBAS.A000SV30', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000285', 'XCFP.BAT1SBAS.A000SV40', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000286', 'XCFP.BAT1SBAS.A000S620', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000287', 'XCFP.BAT1SBAS.A000S630', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000288', 'XCFP.BAT1SBAS.BALAMIS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000289', 'XCFP.BAT1SBAS.BALANCE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000290', 'XCFP.BAT1SBAS.BALMIS2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000291', 'XCFP.BAT1SBAS.BALMIS3', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000292', 'XCFP.BAT1SBAS.CABCOMEX', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000293', 'XCFP.BAT1SBAS.CABCOM02', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000294', 'XCFP.BAT1SBAS.CANCE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000295', 'XCFP.BAT1SBAS.CANCELA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000296', 'XCFP.BAT1SBAS.CANCTEMP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000297', 'XCFP.BAT1SBAS.CANCUNO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000298', 'XCFP.BAT1SBAS.CANFTEMP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000299', 'XCFP.BAT1SBAS.CANSELE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000300', 'XCFP.BAT1SBAS.CDE01', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000301', 'XCFP.BAT1SBAS.CDE02', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000302', 'XCFP.BAT1SBAS.CDIZZ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000303', 'XCFP.BAT1SBAS.CDI00', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000304', 'XCFP.BAT1SBAS.CDI01', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000320', 'XCFP.BAT1SBAS.CDI02', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000321', 'XCFP.BAT1SBAS.CDI09', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000322', 'XCFP.BAT1SBAS.CDI10', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000323', 'XCFP.BAT1SBAS.CDI10A', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000324', 'XCFP.BAT1SBAS.CDI11', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000325', 'XCFP.BAT1SBAS.CDI12', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000326', 'XCFP.BAT1SBAS.CDI13A', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000327', 'XCFP.BAT1SBAS.CDI13B', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000328', 'XCFP.BAT1SBAS.CDI14A', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000329', 'XCFP.BAT1SBAS.CDI14B', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000330', 'XCFP.BAT1SBAS.CDI16', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000331', 'XCFP.BAT1SBAS.CLIENTE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000332', 'XCFP.BAT1SBAS.CONCILI0.BKUP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000333', 'XCFP.BAT1SBAS.CONCILI0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000334', 'XCFP.BAT1SBAS.CONTABLE.RAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000335', 'XCFP.BAT1SBAS.DEVEPROY', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000336', 'XCFP.BAT1SBAS.DEVESORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000337', 'XCFP.BAT1SBAS.EXPORT0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000338', 'XCFP.BAT1SBAS.EXPORT1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000339', 'XCFP.BAT1SBAS.EXPORT2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000340', 'XCFP.BAT1SBAS.EXPORT3', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000341', 'XCFP.BAT1SBAS.EXPORT4', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000342', 'XCFP.BAT1SBAS.EXPORT5', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000343', 'XCFP.BAT1SBAS.EXPORT6', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000344', 'XCFP.BAT1SBAS.EXPORT7', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000345', 'XCFP.BAT1SBAS.EXPORT8', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000346', 'XCFP.BAT1SBAS.EXPORT9', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000347', 'XCFP.BAT1SBAS.FIA41', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000348', 'XCFP.BAT1SBAS.FIA51', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000349', 'XCFP.BAT1SBAS.FIA61', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000350', 'XCFP.BAT1SBAS.FINAP120', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000351', 'XCFP.BAT1SBAS.FINAS120', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000352', 'XCFP.BAT1SBAS.FINOS120', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000353', 'XCFP.BAT1SBAS.FINOT120', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000354', 'XCFP.BAT1SBAS.IDNETTXT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000355', 'XCFP.BAT1SBAS.IDPCOME1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000356', 'XCFP.BAT1SBAS.IMPOR00', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000357', 'XCFP.BAT1SBAS.INCOR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000358', 'XCFP.BAT1SBAS.INPOSIND', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000359', 'XCFP.BAT1SBAS.INVCTEMP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000360', 'XCFP.BAT1SBAS.INVFTEMP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000361', 'XCFP.BAT1SBAS.ISREG00', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000362', 'XCFP.BAT1SBAS.MISCONCI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000363', 'XCFP.BAT1SBAS.MISCONCS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000364', 'XCFP.BAT1SBAS.MODISP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000365', 'XCFP.BAT1SBAS.OPEPEN10', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000366', 'XCFP.BAT1SBAS.OPEPEN11', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000367', 'XCFP.BAT1SBAS.OPEPEN30', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000368', 'XCFP.BAT1SBAS.OPEPEN40', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000369', 'XCFP.BAT1SBAS.OPEPEN41', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000370', 'XCFP.BAT1SBAS.OPEPEN50', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000371', 'XCFP.BAT1SBAS.OPEPEN70', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000372', 'XCFP.BAT1SBAS.OPERACI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000373', 'XCFP.BAT1SBAS.OPESORT1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000374', 'XCFP.BAT1SBAS.OPESORT2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000375', 'XCFP.BAT1SBAS.OPESOR01', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000376', 'XCFP.BAT1SBAS.OPESOR01.OFI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000377', 'XCFP.BAT1SBAS.OPESOR02', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000378', 'XCFP.BAT1SBAS.OPESOR02.OFI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000379', 'XCFP.BAT1SBAS.OPESOR03', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000380', 'XCFP.BAT1SBAS.OPESOR03.OFI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000381', 'XCFP.BAT1SBAS.PASECXL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000382', 'XCFP.BAT1SBAS.RESUME1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000383', 'XCFP.BAT1SBAS.RESUME2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000384', 'XCFP.BAT1SBAS.RIELIQUI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000385', 'XCFP.BAT1SBAS.RIELIQU2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000386', 'XCFP.BAT1SBAS.RISC001D', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000387', 'XCFP.BAT1SBAS.UNLOAD.GCDEPROS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000388', 'XCFP.BAT1SBAS.UNLOAD.GCDIPROS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000389', 'XCFP.BAT1SBAS.UNLOAD.GCOEPROS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000390', 'XCFP.BAT1SBAS.UNLOAD.GCOIPROS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000391', 'XCFP.BAT1SBAS.UNLOAD.XCJDTA65', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000392', 'XCFP.BAT1SBAS.UNLOAD1.GCDIPROS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000393', 'XCFP.BAT1SBAS.UNLOAD1.GCOIPROS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000394', 'XCFP.BAT1SBAS.UNLOAD2.GCDIPROS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000395', 'XCFP.BAT1SBAS.UNLOAD2.GCOIPROS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000396', 'XCFP.BAT1SBAS.UNLOAD3.GCDIPROS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000397', 'XCFP.BAT1SBAS.VENCIMI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000398', 'XCFP.BAT1SBAS.XCDQBAT0.COB', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000399', 'XCFP.BAT1SBAS.XCDQCIN0.COB', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000400', 'XCFP.BAT1SBAS.XCDQSEQ0.COB', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000401', 'XCFP.BAT1SBAS.XCDQTAC0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000402', 'XCFP.BAT1SBAS.XCDQTAC0.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000403', 'XCFP.BAT1SBAS.XCDQTAC1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000404', 'XCFP.BAT1SBAS.XCDT1018', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0004205', 'XCFP.BAT1SBAS.XCDT610', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000406', 'XCFP.BAT1SBAS.XCFSA060', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000407', 'XCFP.BAT1SBAS.XCFSA080', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000408', 'XCFP.BAT1SBAS.XCFSA210', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000409', 'XCFP.BAT1SBAS.XCF80SOR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000410', 'XCFP.BAT1SBAS.XCF86SOR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000411', 'XCFP.BAT1SBAS.XCIMPOR0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000412', 'XCFP.BAT1SBAS.XCIMPOR1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000413', 'XCFP.BAT1SBAS.XCIMPOR2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000414', 'XCFP.BAT1SBAS.XCJDTAC1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000415', 'XCFP.BAT1SBAS.XCJDTAC2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000416', 'XCFP.BAT1SBAS.XCPENDI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000417', 'XCFP.BAT1SBAS.XCUCARGA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000418', 'XCFP.BAT1SBAS.XCUCARGB', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000419', 'XCFP.BAT1SBAS.XCUCOMEX', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000420', 'XCFP.BAT1SBAS.XCUGASTO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000421', 'XCFP.BAT1SBAS.XCUPAGOS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000422', 'XCFP.BAT1SBAS.XCUTURIS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000423', 'XCFP.BAT1STDR.XCECAG1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000424', 'XCFP.BAT1UNLO.DESCTCLA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000425', 'XCFP.BAT1UNLO.DESCTCOP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000426', 'XCFP.BAT1UNLO.DESCTMOP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000427', 'XCFP.BAT1UNLO.REPORTE.FINACTIV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000428', 'XCFP.BAT1UNLO.REPORTE.FINACTI2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000429', 'XCFP.BAT1UNLO.XCDTLOG', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000430', 'XCFP.BAT1UNLO.XCDTL018', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000431', 'XCFP.BAT1UNLO.XCECAG1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000432', 'XCFP.BAT1UNLO.XCUCOMEX', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000433', 'XCFP.BAT1UNLO.XCULIQ05', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000434', 'XCFP.BAT1UNLO.XCULIQ18', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000435', 'XCFP.CONCILIA.MISINVEN', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000436', 'XCFP.CONCILIA.MISINVEN.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000437', 'XCFP.CONCILIA.XCINV.SORT1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000438', 'XCFP.CUENTAQH', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000439', 'XCFP.GPL.XCF110SO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000440', 'XCFP.GPL.XCF150SO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000441', 'XCFP.GPL.XCF80SOR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000442', 'XCFP.TEMPOR00.INTFCONT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000443', 'XCFD.BAT1CONS.ABCFCCUE.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000444', 'XCFD.BAT1CONS.ABCFCCUE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000445', 'XCFD.BAT1CONS.ABCFCNRE.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000446', 'XCFD.BAT1CONS.ABCFCNRE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000447', 'XCFD.BAT1CONS.ABCFCONE.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000448', 'XCFD.BAT1CONS.ABCFCONE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000449', 'XCFD.BAT1CONS.ABCFRECE.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000450', 'XCFD.BAT1CONS.ABCFRECE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000451', 'XCFD.BAT1CONS.ABCFSALE.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000452', 'XCFD.BAT1CONS.ABCFSALE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000453', 'XCFD.BAT1CONS.ABCIADEL.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000454', 'XCFD.BAT1CONS.ABCICONC.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000455', 'XCFD.BAT1CONS.ABCIDNET.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000456', 'XCFD.BAT1CONS.ABCIINTE.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000457', 'XCFD.BAT1CONS.ABCIINVE.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000458', 'XCFD.BAT1CONS.ABCIJOUR.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000459', 'XCFD.BAT1CONS.ABCIRECL.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000460', 'XCFD.BAT1CONS.ABCITCIN.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000461', 'XCFD.BAT1CONS.ABCITCOP.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000462', 'XCFD.BAT1CONS.ABCITLOG.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000463', 'XCFD.BAT1CONS.ABCITL04.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000464', 'XCFD.BAT1CONS.ABCITL09.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000465', 'XCFD.BAT1CONS.ABCITL10.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000466', 'XCFD.BAT1CONS.ABCITOPA.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000467', 'XCFD.BAT1CONS.ABCITOPP.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000468', 'XCFD.BAT1CONS.ABCITOPW.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000469', 'XCFD.BAT1CONS.ABCITOPX.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000470', 'XCFD.BAT1CONS.ABCITOP0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000471', 'XCFD.BAT1CONS.ABCITOP1.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000472', 'XCFD.BAT1CONS.ABCITOP2.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000473', 'XCFD.BAT1CONS.ABCITOP3.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000474', 'XCFD.BAT1CONS.ABCITOP4.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000475', 'XCFD.BAT1CONS.ABCITOP5.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000476', 'XCFD.BAT1CONS.ABCITTA1.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000477', 'XCFD.BAT1CONS.ABCTINTE.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000478', 'XCFD.BAT1CONS.ABCTRLOG.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000479', 'XCFD.BAT1CONS.ABCTTRA0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000480', 'XCFD.BAT1CONS.ABCTTRA1.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000481', 'XCFD.BAT1CONS.ABCWCONC.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000482', 'XCFD.BAT1CONS.ABCWINTE.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000483', 'XCFD.BAT1CONS.ABCWINVE.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000484', 'XCFD.BAT1CONS.ABCWJOUR.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000485', 'XCFD.BAT1CONS.ABCWTCIN.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000486', 'XCFD.BAT1CONS.ABCWTCOP.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000487', 'XCFD.BAT1CONS.ABCWTLOG.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000488', 'XCFD.BAT1CONS.ABCWTOPA.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000489', 'XCFD.BAT1CONS.ABCWTOPP.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000490', 'XCFD.BAT1CONS.ABCWTOPW.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000491', 'XCFD.BAT1CONS.ABCWTOPX.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000492', 'XCFD.BAT1CONS.ABCWTOP0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000493', 'XCFD.BAT1CONS.ABCWTOP1.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000494', 'XCFD.BAT1CONS.ABCWTOP2.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000495', 'XCFD.BAT1CONS.ABCWTOP3.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000496', 'XCFD.BAT1CONS.ABCWTOP4.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000497', 'XCFD.BAT1CONS.ABCWTOP5.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000498', 'XCFD.BAT1CONS.ABCWTTA1.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000499', 'XCFD.BAT1CONS.ABFCCCU0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000500', 'XCFD.BAT1CONS.ABFCCCU0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000501', 'XCFD.BAT1CONS.ABFCCNR0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000502', 'XCFD.BAT1CONS.ABFCCNR0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000503', 'XCFD.BAT1CONS.ABFCCON0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000504', 'XCFD.BAT1CONS.ABFCCON0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000505', 'XCFD.BAT1CONS.ABFCHA40.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000506', 'XCFD.BAT1CONS.ABFCHA40', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000507', 'XCFD.BAT1CONS.ABFCREC0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000508', 'XCFD.BAT1CONS.ABFCREC0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000509', 'XCFD.BAT1CONS.ABFCSAL0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000510', 'XCFD.BAT1CONS.ABFCSAL0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000511', 'XCFD.BAT1CONS.R840SAL3.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000512', 'XCFD.BAT1CONS.R840TMP0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000513', 'XCFD.BAT1CONS.R860TMP7.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000514', 'XCFD.BAT1CONS.XCFCACON.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000516', 'XCFD.BAT1CONS.XCFCAREC.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000517', 'XCFD.BAT1CONS.XCFCASAL.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000519', 'XCFD.BAT1CONS.XCFCCCU0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000520', 'XCFD.BAT1CONS.XCFCCNR0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000521', 'XCFD.BAT1CONS.XCFCCON0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000522', 'XCFD.BAT1CONS.XCFCHCON.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000523', 'XCFD.BAT1CONS.XCFCHIMP.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000531', 'XCFD.BAT1SBAS.ABCRRLOG.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000532', 'XCFD.BAT1SBAS.ABCTR800.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000533', 'XCFD.BAT1SBAS.ABCTR810.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000534', 'XCFD.BAT1SBAS.ABCTR830.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000535', 'XCFD.BAT1SBAS.ABFCSAL0.SORT1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000536', 'XCFD.BAT1SBAS.ABFCSAL0.SORT2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000537', 'XCFD.BAT1SBAS.ABFCSAL0.SORT3', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000538', 'XCFD.BAT1SBAS.CONCILIA.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000539', 'XCFD.BAT1SBAS.CONCILIA.SORT1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000540', 'XCFD.BAT1SBAS.CONCILIA.XCINV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000541', 'XCFD.BAT1SBAS.RA00TCOP.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000542', 'XCFD.BAT1SBAS.RA10TCOP.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000543', 'XCFD.BAT1SBAS.RBK1HA4H.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000544', 'XCFD.BAT1SBAS.RBK1HA4M.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000545', 'XCFD.BAT1SBAS.RBK1HA40.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000553', 'XCFD.BAT1SBAS.RBK1VAL0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000554', 'XCFD.BAT1SBAS.RISC000D', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000555', 'XCFD.BAT1SBAS.R690DCOP.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000556', 'XCFD.BAT1SBAS.R690DINV.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000557', 'XCFD.BAT1SBAS.R690HCOA.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000558', 'XCFD.BAT1SBAS.R690HCO0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000559', 'XCFD.BAT1SBAS.R690HCO1.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000560', 'XCFD.BAT1SBAS.R690HCO3.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000561', 'XCFD.BAT1SBAS.R690HCO4.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000562', 'XCFD.BAT1SBAS.R690OPEC.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000563', 'XCFD.BAT1SBAS.R690OPEF.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000564', 'XCFD.BAT1SBAS.R690WCOP.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000565', 'XCFD.BAT1SBAS.R700TMP0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000566', 'XCFD.BAT1SBAS.R800TMPS.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000567', 'XCFD.BAT1SBAS.R800TMP0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000568', 'XCFD.BAT1SBAS.R800TMP1.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000569', 'XCFD.BAT1SBAS.R800TMP2.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000570', 'XCFD.BAT1SBAS.R800TMP3.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000571', 'XCFD.BAT1SBAS.R800TMP4.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000572', 'XCFD.BAT1SBAS.R800TMP5.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000573', 'XCFD.BAT1SBAS.R800TMP7.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000574', 'XCFD.BAT1SBAS.R810LOG0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000575', 'XCFD.BAT1SBAS.R810LOG1.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000576', 'XCFD.BAT1SBAS.R810LOG2.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000577', 'XCFD.BAT1SBAS.R810LOG3.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000578', 'XCFD.BAT1SBAS.R810LOG4.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000579', 'XCFD.BAT1SBAS.R810LOG5.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000580', 'XCFD.BAT1SBAS.R810MOVI.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000581', 'XCFD.BAT1SBAS.R810MOVS.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000582', 'XCFD.BAT1SBAS.R810TMPA.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000583', 'XCFD.BAT1SBAS.R810TMPB.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000584', 'XCFD.BAT1SBAS.R810TMPC.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000585', 'XCFD.BAT1SBAS.R810TMPD.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000586', 'XCFD.BAT1SBAS.R810TMPE.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000587', 'XCFD.BAT1SBAS.R810TMPF.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000588', 'XCFD.BAT1SBAS.R810TMPG.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000589', 'XCFD.BAT1SBAS.R810TMPH.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000590', 'XCFD.BAT1SBAS.R810TMPI.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000591', 'XCFD.BAT1SBAS.R810TMPJ.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000592', 'XCFD.BAT1SBAS.R810TMPX.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000593', 'XCFD.BAT1SBAS.R810TMPY.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000594', 'XCFD.BAT1SBAS.R810TMPZ.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000595', 'XCFD.BAT1SBAS.R810TMP1.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000596', 'XCFD.BAT1SBAS.R810TMP2.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000597', 'XCFD.BAT1SBAS.R810TMP3.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000598', 'XCFD.BAT1SBAS.R810TMP4.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000599', 'XCFD.BAT1SBAS.R810TMP5.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000600', 'XCFD.BAT1SBAS.R810TMP6.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000601', 'XCFD.BAT1SBAS.R810TMP7.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000602', 'XCFD.BAT1SBAS.R810TMP8.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000603', 'XCFD.BAT1SBAS.R810TMP9.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000604', 'XCFD.BAT1SBAS.R820TMP1.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000605', 'XCFD.BAT1SBAS.R830TMPA.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000606', 'XCFD.BAT1SBAS.R830TMPB.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000607', 'XCFD.BAT1SBAS.R830TMPC.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000608', 'XCFD.BAT1SBAS.R830TMPD.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000609', 'XCFD.BAT1SBAS.R830TMPE.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000610', 'XCFD.BAT1SBAS.R830TMP1.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000611', 'XCFD.BAT1SBAS.R830TMP2.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000612', 'XCFD.BAT1SBAS.R830TMP3.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000613', 'XCFD.BAT1SBAS.R840CON0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000614', 'XCFD.BAT1SBAS.R840SAL0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000615', 'XCFD.BAT1SBAS.R840SAL1.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000616', 'XCFD.BAT1SBAS.R840SAL2.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000617', 'XCFD.BAT1SBAS.R860TMPA.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000618', 'XCFD.BAT1SBAS.R860TMPS.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000619', 'XCFD.BAT1SBAS.R860TMP0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000620', 'XCFD.BAT1SBAS.R860TMP1.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000621', 'XCFD.BAT1SBAS.R860TMP2.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000622', 'XCFD.BAT1SBAS.R860TMP3.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000623', 'XCFD.BAT1SBAS.R860TMP4.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000624', 'XCFD.BAT1SBAS.R860TMP5.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000625', 'XCFD.BAT1SBAS.R860TMP6.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000626', 'XCFD.BAT1SBAS.R860TMP8.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000627', 'XCFD.BAT1SBAS.R860TMP9.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000628', 'XCFD.BAT1SBAS.R900DES0.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000629', 'XCFD.BAT1SBAS.R900DES1.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000630', 'XCFD.BAT1SBAS.XCFCHCON.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000631', 'XCFD.FIX.IMPAINVE.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000632', 'XCFD.FIX.IMPAINVT.ABRAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000633', 'XCFD.PAGOS.FORMAT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000634', 'XCFD.TCTC0090.XCJP0001', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000635', 'XCFD.TCTC1120.XCJP0001', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000636', 'XCFD.UNLOAD.XCJP0001', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000637', 'XCFD.XCJP0001.DETALLE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000638', 'XCFD.XCJP0001.ERRORES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000639', 'XCFD.XCJP0001.ERRORES2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000641', 'XCFD.XC4CMTSE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000642', 'XCFD.XC4CMTSE.UNLOAD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000643', 'GPFD.GADT001.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000645', 'GPFD.GADT002.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000646', 'GPFD.GADT002.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000647', 'GPFD.GADT003.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000648', 'GPFD.GADT003.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000649', 'GPFD.GADT004.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000650', 'GPFD.GADT004.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000651', 'GPFD.GADT006.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000652', 'GPFD.GADT006.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000653', 'GPFD.GADT007.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000654', 'GPFD.GADT007.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000655', 'GPFD.GADT008.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000656', 'GPFD.GADT008.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000657', 'GPFD.GADT009.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000659', 'GPFD.GADT010.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000660', 'GPFD.GADT010.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000661', 'GPFD.GADT011.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000662', 'GPFD.GADT011.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000663', 'GPFD.GADT013.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000664', 'GPFD.GADT013.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000665', 'GPFD.GADT014.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000666', 'GPFD.GADT014.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000667', 'GPFD.GADT015.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000668', 'GPFD.GADT015.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000669', 'GPFD.GADT016.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000670', 'GPFD.GADT016.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000671', 'GPFD.GADT017.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000672', 'GPFD.GADT017.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000673', 'GPFD.GADT018.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000674', 'GPFD.GADT018.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000675', 'GPFD.GADT038.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000676', 'GPFD.GADT038.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000677', 'GPFD.GADT039.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000678', 'GPFD.GADT039.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000679', 'GAFD.GAJPAMEX.UNLOADGA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000680', 'GAFD.GP4CIAMX.SALIDA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000681', 'GAFD.BAT1NBAS.FECONTAB', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000682', 'CO.GA100002.GALS0300', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000684', 'GAFD.BAT1NBAS.A0014600', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000685', 'GAFD.BAT1NBAS.A0014700', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000686', 'CO.GA110001.GALS4700', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000687', 'GAFD.ENVIADO.AMEX', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000688', 'GAFD.SOLICITA.GA4C3800', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000689', 'GAFD.RECIBIDO.AMEX', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000690', 'GAFD.RECIBIDO.AMEX.SINENCA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000691', 'GAFD.RECIBIDO.AMEX.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000692', 'GPFD.AMXTRA.CONFIRMA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000693', 'GPFD.GA4C4800.UNLOAD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000694', 'CO.GA4C4800.S2DQEST0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000695', 'GAFD.GAJS6000.GADT008.UNLOAD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000696', 'HAFD.STDRNBAS.PRO14400.D1AAMMDD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000697', 'GPFD.GP4CI250.SALIDA.DAAMMDD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000698', 'WQFD.BAT1STDR.ADFI.DAAMMDD ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000699', 'WQFD.BAT1STDR.WQ1.DAAMMDD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000700', 'HAFD.STDRSBAS.PRECORON ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000701', 'GPFD.MADT001.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000702', 'GPFD.MADT002.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000703', 'GPFD.MADT003.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000704', 'GPFD.MADT004.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000705', 'GPFD.MADT001.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000706', 'GPFD.MADT002.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000707', 'GPFD.MADT003.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000708', 'GPFD.MADT004.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000709', 'GTFD.BAT1CONS.ACUSES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000711', 'GTFD.BAT1CONS.MT470REC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000712', 'GTFD.MT100REC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000713', 'GTFD.BAT1CONS.MT100REC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000714', 'GTFD.CLAINT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000715', 'GPFD.BAT1CONS.ACUSES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000716', 'GTFD.BAT1CONS.ACUSES.FRAG', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000717', 'GTFD.BAT1CONS.ACUSES.MEOK', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000718', 'GTFD.BAT1CONS.ACUSES.MERR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000719', 'GPFD.ACUSES.SINSIGNO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000720', 'GTFD.CIC1CONS.FSF003K', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000721', 'GPFD.BAT1CONS.MT100202', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000724', 'GPFD.MT100202', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000725', 'GPFD.BAT1CONS.MT1XXXXX', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000726', 'GPFD.BAT1CONS.MT4XXXXX', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000727', 'GPFD.BAT1CONS.MT7XXXXX', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000729', 'GPFD.BAT1CONS.MT6XXXXX', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000730', 'GPFD.BAT1CONS.MT940XXX', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000731', 'GPFD.BAT1CONS.MT199202', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000732', 'GPFD.BAT1CONS.MT1XXSUP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000733', 'GTFD.MT1XXSUP.SWIFT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000734', 'GTFD.BAT1CONS.CTRL.SWIFT.MT1XXSUP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000735', 'GPFD.BAT1CONS.MT1XXINF', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000736', 'GTFD.MT1XXINF.SWIFT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000737', 'GTFD.BAT1CONS.CTRL.SWIFT.MT1XXINF', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000738', 'GTFD.MT4XXXXX.SWIFT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000739', 'GTFD.BAT1CONS.CTRL.SWIFT.MT4XXXXX', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000740', 'GTFD.MT7XXXXX.SWIFT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000741', 'GTFD.BAT1CONS.CTRL.SWIFT.MT7XXXXX', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000742', 'GTFD.MT6XXXXX.SWIFT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000743', 'GTFD.BAT1CONS.CTRL.SWIFT.MT6XXXXX', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000744', 'GTFD.BAT1CONS.CTRL.SWIFT.MT940XXX', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000745', 'GTFD.MT470REC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000746', 'GTFD.MT470REC.MT256', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000747', 'GTFD.BAT1NBAS.A0013200', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000748', 'GTFD.BAT1NBAS.A0013200.MEOK', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000749', 'GTFD.BAT1NBAS.A0013200.MERR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000750', 'GTFD.BAT1NBAS.A0013300', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000752', 'GTFD.SWIFT.TRANSIN', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000753', 'GPFD.GTDT001.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000755', 'CP.USBSWIFT.TEXT.FB', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000769', 'GPFD.GPJPCOCO.UNLOADTR', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000770', 'GPFD.GPJPCOCO.UNLOADPA', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000771', 'GPFD.GPJPCOCO.UNLOADCO', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000772', 'GPFD.GPJPCOCO.UNLOADCE', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000773', 'GPFD.GPJPCOCO.UNLOAD.C3609', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000774', 'GPFD.GPJPCOCO.UNLOAD.C3811', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000781', 'GPFD.BAT1SBAS.RECT6301', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000782', 'GPFD.BAT1SBAS.RECT6302', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000783', 'GPFD.BAT1SBAS.RECT6303', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000784', 'GPFD.GP3C6970.INCIDEN', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000785', 'GPFD.SD1P9990.GP9991.SALIDA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000789', 'GPFD.GPDT001.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000791', 'GPFD.GPDT002.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000793', 'GPFD.GPDT004.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000795', 'GPFD.GPDT005.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000797', 'GPFD.GPDT006.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000798', 'GPFD.GPDT006.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000799', 'GPFD.GPDT008.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000800', 'GPFD.GPDT008.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000801', 'GPFD.GPDT009.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000802', 'GPFD.GPDT009.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000803', 'GPFD.GPDT010.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000804', 'GPFD.GPDT010.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000805', 'GPFD.GPDT011.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000806', 'GPFD.GPDT011.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000807', 'GPFD.GPDT012.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000809', 'GPFD.GPDT016.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000811', 'GPFD.GPDT017.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000813', 'GPFD.GPDT019.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000814', 'GPFD.GPDT019.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000815', 'GPFD.GPDT021.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000816', 'GPFD.GPDT021.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000817', 'GPFD.GPDT022.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000818', 'GPFD.GPDT022.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000819', 'GPFD.GPDT023.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000821', 'GPFD.GPDT035.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000822', 'GPFD.GPDT035.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000823', 'GPFD.GPDT038.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000825', 'GPFD.GPDT039.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000827', 'GPFD.GPDT040.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000829', 'GPFD.GPDT043.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000832', 'GPFD.GPDT045.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000835', 'GPFD.GPDT047.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000836', 'GPFD.GPDT048.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000837', 'GPFD.GPDT048.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000838', 'GPFD.GPDT049.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000840', 'GPFD.GPDT052.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000841', 'GPFD.GPDT052.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000842', 'GPFD.GPDT053.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000843', 'GPFD.GPDT053.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000844', 'GPFD.GPDT056.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000845', 'GPFD.GPDT056.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000847', 'GPFD.GPDT058.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000848', 'GPFD.GPDT059.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000850', 'GPFD.GPDT061.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000852', 'GPFD.GPDT062.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000854', 'GPFD.GPDT063.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000856', 'GPFD.GPDT064.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000857', 'GPFD.GPDT064.TOTAL', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000858', 'GPFD.GPDT065.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000859', 'GPFD.GPDT065.TOTAL', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000860', 'GPFD.GPDT066.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000862', 'GPFD.GPDT067.TOTAL', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000863', 'GPFD.GPDT067.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000865', 'GPFD.GPDT068.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000866', 'GPFD.GPDT069.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000868', 'GPFD.GPDT070.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000871', 'GPFD.GPDT071.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000873', 'GPFD.GPDT072.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000875', 'GPFD.GPDT074.TOTAL', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000876', 'GPFD.GPDT074.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000877', 'GPFD.GPDT075.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000879', 'GPFD.GPDT076.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000882', 'GPFD.GPDT077.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000884', 'GPFD.GPDT078.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000886', 'GPFD.GPDT080.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000889', 'GPFD.GPDT081.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000890', 'GPFD.GPDT082.TOTAL', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000891', 'GPFD.GPDT082.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000892', 'GPFD.GPDT084.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000893', 'GPFD.GPDT084.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000894', 'GPFD.GPDT086.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000895', 'GPFD.GPDT086.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000896', 'GPFD.GPDT087.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000898', 'GPFD.GPDT088.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000899', 'GPFD.GPDT088.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000900', 'GPFD.GPDT091.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000901', 'GPFD.GPDT091.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000902', 'GPFD.GPDT092.TOTAL', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000903', 'GPFD.GPDT092.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000905', 'GPFD.GPDT093.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000906', 'GPFD.GPDT095.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000908', 'GPFD.GPDT096.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000909', 'GPFD.GPDT096.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000911', 'GPFD.GPDT099.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000912', 'GPFD.GPDT100.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000913', 'GPFD.GPDT100.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000914', 'GPFD.GPDT101.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000915', 'GPFD.GPDT101.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000917', 'GPFD.GPDT102.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000919', 'GPFD.GPDTB02.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000921', 'GPFD.GPDTB12.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000922', 'GPFD.GPDTLOG.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000923', 'GPFD.GPDTLOG.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000924', 'GPFD.GPDTCON.TOTAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000925', 'GPFD.GPDTCON.PUNTUAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000928', 'GPFD.GP4C4250.ERROR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000930', 'GPFD.GPJPI001.HEADER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000933', 'GPFD.GPJPI004.HEADER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000943', 'GPFD.GPJPI002.HEADER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000944', 'XCFD.XCJP0001.HEADER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000945', 'EDFD.EDJPI001.HEADER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000947', 'GPFD.GPJPI003.HEADER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000949', 'GPFD.GP4C4130.SALIDA.ERROR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000954', 'GPFD.GP4C4139.MAILS.CLIENTES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000956', 'GPFD.GP4C4140.MAILS.SUCUR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000958', 'GPFD.GP4C4143.TOTALES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000959', 'GPFD.GP4C4157.SALIDA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000970', 'QR.FIX.PROCESO.TRANFER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000971', 'GPFD.GP4C4599.SALIDA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000972', 'GPFD.GP4C4599.SALERROR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000973', 'GPFD.GPJP4598.ENT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000974', 'GPFD.GPJP4598.SALIDA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000975', 'GPFD.GPJP4598.SALERROR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000976', 'GPFD.GPJP4598.SALCERO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000983', 'GPFD.GPDT087.PARCIAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000985', 'GPFD.GPDT097.PARCIAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000987', 'GPFD.GPDT098.PARCIAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000988', 'GPFD.GP4C4181.LIQDIV.UNLOAD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000989', 'GPFD.GP4C4181.LIQDIV.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000990', 'GPFD.GP4C4181.ERRORES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000991', 'GPFD.GP4C4181.TOTALES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000992', 'GPFD.GP4C4181.FORMATO.TOTALES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000993', 'GPFD.UNLOAD.GP4C4182', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000994', 'GPFD.GP4C3840.SALIDA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000995', 'GPFD.GP4C4596.GPJP3840.SALIDA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000996', 'GPFD.GP4C4012.UNLOAD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000997', 'GPFD.GP4C4014.UNLOAD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000998', 'GPFD.GP4C4104.CUMPL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0000999', 'GPFD.GP4C4080.UNLOAD.TMOVICUE ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001000', 'GPFD.GP4C4080.UNLOAD.TMOVILIB', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001001', 'GPFD.GPJP4175.SALIDA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001002', 'GPFD.GPJP4175.GPDT084', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001003', 'GPFD.GP4CA310.SALIDA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001005', 'DL.DLJP300A.UNLOAD.DLDTACC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001006', 'DL.DLJP300A.UNLOAD.DLDTEAC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001007', 'DL.DLJP300A.UNLOAD.DLDTEDI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001008', 'DL.DLJP300A.UNLOAD.DLDTEMB', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001010', 'DL.DLJP300A.UNLOAD.DLDTTDI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001011', 'DL.DLJP3260.REPORTE', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001016', 'DL.D030.PS.MOVI.ESTADOS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001017', 'DL.D031.PS.MOVI.ACC2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001018', 'DL.D036.PS.MOVI.CRTAREND', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001019', 'DL.D040.PS.MOVI.FISICOS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001022', 'TJ.SEQ.TJDTALT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001023', 'TJ.SEQ.TJDTCRO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001030', 'TJ.SEQ.TJDTMOV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001031', 'TJ.SEQ.TJDTPAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001032', 'TJ.SEQ.TJDTPGN', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001033', 'TJ.SEQ.TJDTPRO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001034', 'TJ.SEQ.TJDTRCD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001035', 'TO.D380.KSDS.MAES.PASEA.RECUP', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001036', 'TO.D011.KSDS.PARA.CRONO.GRAMA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001037', 'CMFD.BAT1VSAM.FERIADOS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001038', 'TCFD.STDRVSAM.TCDF0001', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001040', 'TJFD.JOURFAC.MAST', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001041', 'TJFD.STDRSBAS.JOURCUA ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001042', 'TJFD.STDRSBAS.JOURPRE ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001043', 'TJFD.PREPAGA.JOURPRE ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001044', 'TJFD.STDRSBAS.TJJOUR.DEV ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001045', 'TJFD.STDRSBAS.JOURSAM ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001046', 'TJFD.STDRSBAS.JOURCCM ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001047', 'TJFD.STDRSBAS.JOURIDM ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001048', 'TV.D055.KSDS.MAES.TCVI085.DEUDA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001049', 'TJ.TJ4CSP42.CLIENTES.PADRON', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001050', 'TJ.TJ4CSP41.PRODUC.PADRON', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001051', 'TJ.SEQ.TJDTSAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001052', 'TO.D165.KSDS.MOVI.TABLA.ASIENTOS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001053', 'TJFD.BAT1SBAS.VISA.BASECRU.EMIS.UNZ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001054', 'TJFD.BAT1SBAS.VISA.BASECRU.PAGA.UNZ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001055', 'TV.D125.SECU.MOVI.CRUCECIM.VISA.MANA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001056', 'TV.D128.SECU.MOVI.CRUCECNM.VISA.MANA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001057', 'TD.D175.ESDS.MOVI.CLEARING.DIARIO.TRA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001058', 'TV.D189.SECU.LABEL1.FUSI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001061', 'TJFD.BAT1SBAS.TJ4C5200.LIQCOMP.UNIFICA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001062', 'TJ.SEQ.TJDTMOCF', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001063', 'TV.D806.KSDS.TABLA.RECHAZOS.TRAMITES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001064', 'TV.D804.SECU.NOVED.RECHA.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001065', 'TV.D803.SECU.NOVED.ACEPT.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001066', 'TJFD.BAT1SECU.VUELTA.DEBCUOPR.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001067', 'TJFD.BAT1NBAS.PREPAGA.AUTORIZ.ZIP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001068', 'TV.D557.SECU.ACEPTADA.MASIVO.EMPRESAS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001069', 'TV.D557.SECU.RECHAZAD.MASIVO.EMPRESAS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001070', 'TJ.PREPAGA.LOGIN.REIMPR.MOVIRECH', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001071', 'TJ.PREPAGA.LOGIN.REIMPR.MOVIACEP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001072', 'TV.D128.SECU.MOVI.CRUCECNM.VISA.XC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001073', 'TV.D125.SECU.MOVI.CRUCECIM.VISA.XC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001074', 'TJ.D001.SP.BLOQ.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001075', 'TJ.D001.SP.AR.PARTICI.VISA.ZIP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001076', 'TJ.FIX.TJSGMNAC.PROMO.ACEPTADA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001077', 'TJ.FIX.TJSGMNRE.PROMO.RECHAZAD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001078', 'TJ.FIX.TJSGMTER.PROMO.TABLAERR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001079', 'TJ.CAIDA.CAPREM.RECH', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001080', 'TJ.CAIDA.CAPREM.ACEP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001081', 'TJFD.BAT1NBAS.SALDOS.USUARIO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001082', 'TJFD.CBEVE010.EVENTOS.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001083', 'TJFD.CBCOD010.CONCI.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001084', 'TJFD.BAT1NBAS.MOVIM.PRESENTA.ZIP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001085', 'TJFD.BAT1SBAS.VISA.BASECRU.PAGADORA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001086', 'TJFD.BAT1SBAS.VISA.BASECRU.EMISORA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001087', 'TV.D481.SECU.TRAN.CRUCE.EMIS.B017', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001088', 'TV.D479.SECU.TRAN.CRUCE.PAGA.B017', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001089', 'TV.D557.SECU.LIST.MOVI.PRESENT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001090', 'TV.D557.SECU.MOVI.MOVI.PRESENT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001091', 'TV.D443.SECU.CTROL.TABLA.ERROR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001092', 'TV.D442.SECU.CTROL.AJUST.RECH', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001093', 'TV.D441.SECU.CTROL.PAGOS.RECH', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001094', 'TV.D440.SECU.CTROL.AJUST.ACEP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001095', 'TV.D439.SECU.CTROL.PAGOS.ACEP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001096', 'TV.D805.SECU.NOVED.NO.EMPAQ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001097', 'TJFD.BAT1SBAS.PREPAGA.RECH', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001098', 'TV.D671.SECU.MOVI.TRANS.DIARIA.B017', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001099', 'TJ.D001.SP.PUNTOS.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001100', 'TV.D548.SECU.MAESTAB.TRAN.B017', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001101', 'TV.D558.SECU.LIST.MOVI.FACTURAD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001102', 'TV.D558.SECU.MOVI.MOVI.FACTURAD.ZIP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001103', 'TV.D556.SECU.LIST.INVEN.CUOTACE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001104', 'TV.D556.SECU.MOVI.INVEN.CUOTACE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001105', 'TJFD.BAT1NBAS.CODCONCI.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001106', 'TJFD.BAT1NBAS.CODOPERA.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001107', 'TJFD.COMVISA.XCOM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001108', 'TV.D559.SECU.MOVI.FACT.COMCARD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001109', 'TJFD.BAT1NBAS.MOVIM.LIQUIDA.ZIP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001110', 'TJFD.BAT1NBAS.MOVIM.LIQUIDA.CONSBI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001111', 'TJFD.BAT1SBAS.INTDEV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001112', 'TJFD.CAMBIO.CARTERA.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001113', 'TJFD.TJ4C9990.MOV.EXTRAC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001114', 'TD.D307.SECU.MOVI.TCTARJ.TRANSMI.L350', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001115', 'TD.D182.SECU.MOVI.ALT.CUENACEP.TRANSMI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001116', 'TD.TJ.D001.VTA.GAF.NOVE.MASTER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001117', 'TD.D174.ESDS.MOVI.LIQCOMP.M48HS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001118', 'TD.D455.ESDS.MOVI.CB141D.MAST', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001119', 'TD.D454.ESDS.MOVI.CB141P.MAST', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001120', 'TD.D692.SECU.MOVI.ADELLINK.TRANSMI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001121', 'TD.D564.SECU.ENTRAFT.CC120DD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001122', 'TD.D175.ESDS.MOVI.CLEARING.DIARIO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001123', 'TD.D167.ESDS.MOVI.LIQCOMP.A10Y12', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001124', 'TD.D172.ESDS.MOVI.LIQCOMP.A48HS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001125', 'TJ.BL005DI.HABIL.DIARIAS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001126', 'TD.D207.SECU.MOVI.CUENTAS.UNB.BCOCRED', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001127', 'TJ.D001.SP.PUNTOS.MASTER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001128', 'TD.D405.SECU.TRAN.PADRON.COMER.B016', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001129', 'TD.D136.SECU.MOVI.TARBOLE.TRANSMI.INT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001130', 'TD.D136.SECU.MOVI.TARBOLE.TRANSMI.LOC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001131', 'TJFD.BAT1SBAS.RENOV.ARGE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001132', 'TJFD.BAT1SBAS.MASTER.SO443D', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001133', 'TD.D403.SECU.TRAN.TOCTACTE.B016', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001135', 'TJFD.BAT1NBAS.MOV.LIQUIDAC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001136', 'TD.D691.SECU.MOV.CTACTE.SEMANAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001137', 'TJFD.BAT1SBAS.MASTER.IN141DP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001138', 'TJFD.BAT1SBAS.LIDER.IN141DP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001139', 'TJFD.BAT1SBAS.ARGEN.IN141DP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001140', 'TJ.RESUM.ARGEN', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001141', 'TJ.RESUM.MASTER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001142', 'TJ.RESUM.LIDER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001143', 'TJFD.CUOTAS.PEND.MASTER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001144', 'TD.D206.SECU.MOVI.CUENTAS.EXE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001145', 'TJFD.BAT1SBAS.CC118D.IVA.MASTER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001146', 'TJFD.BATINBAS.CL542D.RET', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001147', 'TJFD.BATINBAS.CL543D.PERC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001148', 'TD.D533.SECU.ENTRAFT.CL122D.PERBSAS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001149', 'TD.D534.SECU.ENTRAFT.CL636D.DGRMEND', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001150', 'TD.D546.SECU.ENTRAFT.CL733D.DGRERIO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001151', 'TD.D555.SECU.ENTRAFT.CL862A.RETCORD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001152', 'TD.D556.SECU.ENTRAFT.CL863A.RETCORD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001153', 'TD.D557.SECU.ENTRAFT.CL864A.RETCORD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001154', 'TD.D559.SECU.ENTRAFT.CL865D.ARCIBA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001155', 'TJFD.BAT1SBAS.SELLADO.MISIONES.CC919D', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001156', 'TJFD.BAT1SBAS.SELLADO.MISIONES.CCM34D', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001157', 'TJFD.BAT1SBAS.SELLADO.MISIONES.CC919L', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001158', 'TJFD.BAT1SBAS.SELLADO.MISIONES.CCM04D', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001159', 'TO.D641.SECU.TRAN.TICKET.B016', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001160', 'TO.D223.SECU.MOVI.CLEARING.TICKET', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001161', 'TD.D559.SECU.ENTRAFT.CL865T.ARCIBA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001162', 'TD.D549.SECU.ENTRAFT.CL775T.SICORE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001163', 'TD.D533.SECU.ENTRAFT.CL122T.PERBSAS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001164', 'HAFD.INTERFAZ.TV01.CCC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001165', 'HAFD.INTERFAZ.TV05.CCC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001166', 'HAFD.INTERFAZ.AIB01', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001167', 'HAFD.INTERFAZ.MIB01', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001168', 'HAFD.INTERFAZ.T1.BATCH', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001169', 'HAFD.INTERFAZ.TD07', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001170', 'HAFD.INTERFAZ.TD08', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001171', 'HAFD.INTERFAZ.TD11', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001172', 'HAFD.INTERFAZ.TD16', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001173', 'HAFD.INTERFAZ.TIB01', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001174', 'HAFD.INTERFAZ.TJ.ADE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001175', 'HAFD.INTERFAZ.TJ.BATCH', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001176', 'HAFD.INTERFAZ.TJ.DEC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001177', 'HAFD.INTERFAZ.TJ.DEV.BATCH', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001178', 'HAFD.INTERFAZ.TJ.FAC.MAST', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001179', 'HAFD.INTERFAZ.TJ.FAC.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001180', 'HAFD.INTERFAZ.TJ.IDV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001181', 'HAFD.INTERFAZ.TJ.PEV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001182', 'HAFD.INTERFAZ.TJ.PRE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001183', 'HAFD.INTERFAZ.TJ.PREPAGA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001185', 'HAFD.INTERFAZ.TO001.PARCMC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001186', 'HAFD.INTERFAZ.TO001.PARCVI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001187', 'HAFD.INTERFAZ.TO02SHR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001188', 'HAFD.INTERFAZ.TV01SHR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001189', 'HAFD.INTERFAZ.TV05SHR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001190', 'HAFD.INTERFAZ.VIB01', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001191', 'HAFD.INTERFAZ.TJ.SAM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001192', 'HAFD.INTERFAZ.TJ.SAV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001193', 'CP.OPS.MERGALL0.ULTMOV.VSM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001194', 'TJFD.BAT1SBAS.CP.OPS.MERGALL0.ULTMOV,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001195', 'TJFD.BAT1SBAS.OPS,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001196', 'TJFD.BAT1SBAS.OPS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001197', 'TJFD.BAT1SBAS.TJ4C5100.OPS,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001198', 'TJFD.BAT1SBAS.TJ4C6600.S1DQOPS0.OPS,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001199', 'TJFD.BAT1SBAS.TJ4C6600.UNLOAD.OPS,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001200', 'TJFD.BAT1SBAS.TJ4C6650.UNLOAD.OPS,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001201', 'TJFD.BAT1SBAS.TJ4C6940.OPS.S', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001202', 'TJFD.BAT1SBAS.TJFD.BAT1SBAS.OPS,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001203', 'TJFD.BAT1SBAS.TJLDCD06.OPS,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001204', 'TJFD.BAT1SBAS.TJLDCD06.OPS.S1,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001205', 'TJFD.OPSBF.CAMARA.PAGOCOM.BR9COP2,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001206', 'TJFD.TJDES002.TJ4C3720.OPS.SM,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001207', 'TJFD.TJDES002.TJ4C3720.OPSBF,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001208', 'TJFD.BAT1NBAS.TPGNV.IMDEF               ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001209', 'TJFD.STDRSBAS.JOURIDV ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001210', 'TJFD.UNLOAD.TJDTCUP.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001211', 'TJFD.UNLOAD.TJDTMOV.IMDEF', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001212', 'TJFD.BAT1NBAS.MOVIM.LIQUIDA.IMDEF', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001213', 'TJFD.BAT1NBAS.MOVIM.IMDEF.CONSBI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001214', 'TJFD.BAT1NBAS.IMDEF.VISA.CON6698.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001215', 'TJFD.BAT1NBAS.VISA.EXTRANJ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001216', 'TJFD.UNLOAD.TJDTMOV.SALANT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001217', 'TJFD.BAT1NBAS.IMDEF.VISA.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001218', 'TJFD.UNLOAD.TJDTMOV.SALBANC.NUEVO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001219', 'TV.D151.SECU.MOVI.TCTOTAL.CONVISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001220', 'TJFD.TJDTMOV.SALDOS.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001221', 'TJFD.BAT1NBAS.RESLIQ.INCID2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001222', 'TJFD.BAT1NBAS.CUPLIQ.VISA.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001223', 'TJFD.TCVCIMDE.MOVTOS.SINPAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001224', 'TJFD.TJNLIDSA.TJ4C6785.LISTADO.CODOPE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001225', 'TJFD.BAT1NBAS.PAGLIQ.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001226', 'TJFD.BAT1NBAS.CUPLIQ.VISA.DEP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001227', 'TJFD.BAT1NBAS.RESLIQ.VISA.DEP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001228', 'TJFD.BAT1NBAS.MOVIM.CONSBI.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001229', 'TJFD.BAT1NBAS.PAGLIQ.INCID', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001230', 'TJFD.BAT1NBAS.PAGLIQ.INCI2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001231', 'TJFD.BAT1NBAS.TPGNM.IMDEF.MAST     ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001232', 'TJFD.UNLOAD.TJDTMAC.MAST            ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001233', 'TJFD.UNLOAD.TJDTCUP.MAST   ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001234', 'TJFD.BAT1NBAS.ITEMS.CC027D', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001235', 'TJFD.BAT1NBAS.MOVIMPRE.CC102D', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001236', 'TJFD.BAT1NBAS.REGDAT.ITEMLIQ.CC27D    ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001237', 'TJFD.BAT1NBAS.IMDEF.MAST.CON6698 ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001238', 'TJFD.BAT1NBAS.MAST.EXTRANJ          ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001239', 'TJFD.IMPDEF.MAST.PROV.SORT         ,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001240', 'TJFD.BAT1NBAS.IMDEF.MAST.SORT ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001241', 'TJFD.MOVIM.LIQUID.MASTER.AJUS ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001242', 'TJFD.BAT1NBAS.AJUSLIQ.MAST    ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001243', 'TJFD.UNLOAD.TJDTMOV.SALANT.MAST', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001244', 'TJFD.UNLOAD.TJDTMOV.SBNUE.MAST', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001245', 'TJFD.TJDTMOV.SALDOS.MAST', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001246', 'TJFD.TJDTMOV.SALDOS.DIFER.MAST', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001247', 'TJFD.BAT1NBAS.IMDEF.MASTER.CON6698.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001248', 'TJFD.TJDTMOV.SBNUE.NOCERR.MAST', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001249', 'TJFD.BAT1NBAS.MOVIMPRE.CC102D.ACUM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001250', 'TJFD.BAT1NBAS.REGDAT.CC027D.ACUM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001251', 'TJFD.BAT1NBAS.MOVIM.LIQUIDA.ACUM VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001252', 'TJFD.BAT1NBAS.TJDTMOV.SALMARC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001253', 'TJFD.BAT1NBAS.IMDEF.SALACRE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001254', 'TJFD.BAT1NBAS.IMDEF.SALDEUD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001255', 'TJFD.BAT1NBAS.TJDTMOV.SALMARC.MAST ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001256', 'TJFD.BAT1NBAS.IMDEF.MAST.SALACRE ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001257', 'TJFD.BAT1NBAS.IMDEF.MAST.SALDEUD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001258', 'TJFD.TJNEMSBV.TJDTMOV.VISA.SB', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001259', 'TJFD.TJNEMSBV.TJDTMOV.VISA.LQ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001260', 'TJFD.TJNEMSBV.TJDTMOV.VISA.SB.DIF', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001261', 'TJFD.TJMEMSBM.TJDTMOV.MASTER.SB', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001262', 'TJFD.TJMEMSBM.TJDTMOV.MASTER.LQ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001263', 'TJFD.TJMEMSBM.TJDTMOV.MASTER.SB.DIF', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001264', 'TJFD.BAT1NBAS.TJDTMOV.CUACAR.VISA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001265', 'TJFD.UNLOAD.TJDTMAC.VISA.VTOANT ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001266', 'TJFD.BAT1NBAS.CUACAR.CON6698,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001267', 'TJFD.BAT1NBAS.TJ4C6698.NCUCAV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001268', 'TJFD.BAT1NBAS.CUACAR.CON6698.DESSUC.VV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001269', 'TJFD.BAT1NBAS.CUACAR.CON6698.MAST', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001270', 'TJFD.BAT1NBAS.CUACAR.CON6698.DESSUC.MM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001271', 'TJFD.BAT1NBAS.CONSAAC.CON6698', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001272', 'TJFD.BAT1NBAS.CONSAAC.INCID,', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001273', 'TJFD.BAT1NBAS.CONSAAC.CON6698.MAST ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001274', 'TJFD.BAT1NBAS.CONSAAC.INCID.MAST ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001275', 'TJFD.STDRSBAS.JOURSAV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001277', 'TJFD.STDRSBAS.JOURCCV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001278', 'TJFD.STDRNCCV.CONCILI0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001279', 'QHFD.CONCILIA.LISTACCV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001282', 'TJFD.STDRNCCM.CONCILI0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001283', 'QHFD.CONCILIA.LISTACCM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001284', 'TJFD.BAT1NBAS.VISA.EXTRANJ.ACUM,D', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001285', 'TJFD.BAT1NBAS.EXTRANJE.VISA.IMDEF', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001286', 'TJFD.BAT1NBAS.MAST.EXTRANJ.ACUM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001287', 'TJFD.BAT1NBAS.EXTRANJE.MAST.IMDEF', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001288', 'QHFD.CONCILIA.LISTADEO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001289', 'QHFD.CONTABLE.DEC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001290', 'TJFD.BAT1SBAS.AJUSTES.L12', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001291', 'TJFD.BAT1SBAS.ALTAS.L17', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001292', 'TJFD.BAT1SBAS.ANTICIPO.L13', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001293', 'TJFD.BAT1SBAS.RECUPERO.L14', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001294', 'TJFD.BAT1SBAS.RESOLUC.L15', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001295', 'TJFD.STDRNDEO.CONCILI0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001296', 'TJFD.TJDESCC1.JOURDEC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001297', 'TJFD.TJDESCO1.JOURDEO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001298', 'TJFD.TJDESINV.TJ4CINV0.E1DQUNL0.S', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001299', 'TJFD.TJDESINV.TJ4CINV0.S1DQREC0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001300', 'TJFD.TJDESLI1.TJ4C3800.UNLTREC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001301', 'TJFD.TJDESLI1.TJ4C3810.S2DQSAL0.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001302', 'TJFD.TJDESLI1.TJ4C3810.UNLTMOV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001303', 'TJFD.TJDESLI1.TJ4C3820.S2DQINC0.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001304', 'TJFD.TJDESLI1.TJ4C3820.UNLTEVE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001305', 'TJFD.TJDESLI1.TJ4C3830.S2DQINC0.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001306', 'TJFD.TJDESLI1.TJ4C3830.UNLTEVE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001307', 'TJFD.TJDESLI1.TJ4C3840.UNLTREC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001308', 'TJFD.TJDESLI1.TJ4C3850.S2DQSAL0.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001309', 'TJFD.TJDESLI1.TJ4C3850.UNLTREC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001310', 'TJFD.TJDESLI1.TJ4C3860.E1DESC01.SM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001311', 'TJFD.TJDESLI1.TJ4C3860.E2AJUS01.SM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001312', 'TJFD.TJDESLI1.TJ4C3870.S2DQEVE1.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001313', 'TJFD.TJDESLI1.TJ4C3870.UNLTEVE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001314', 'TJFD.TJDESLI1.TJ4C3880.E1DESC01', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001315', 'TJFD.TJDESLI1.TJ4C3890.MOVIPRES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001316', 'TJFD.TJDESLI1.TJ4C9030.UNLTINC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001317', 'TJFD.TJDESLI1.TJ4C9120.UNLTREC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001318', 'TJFD.TJDESLI1.UNEVEREC.ELECTRON', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001319', 'TJFD.TJDESLI1.UNEVEREC.ELECTRON.L1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001320', 'TJFD.TJDESLI1.UNEVEREC.ELECTRON.L2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001321', 'TJFD.TJDESLI1.UNEVEREC.ELECTRON.L3', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001322', 'TJFD.TJDESLI2.JOURDEC.ACUM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001323', 'TJFD.TJDESLI2.JOURDEC.ACUM.OLD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001324', 'TJFD.TJDESLI2.JOURDEC.ACUM.TODO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001325', 'TJFD.TJDESLI2.JOURDEC.IMPACDIA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001326', 'TJFD.TJDESLI2.TJ4C3900.E2DQREC0', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001327', 'TJFD.TJDESLI2.TJ4C3900.S1DQINV0.L1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001328', 'TJFD.TJDESLI2.TJ4C3900.S1DQINV0.L2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001329', 'TJFD.TJDESLI2.TJ4C3900.S2DQERR.L1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001330', 'TJFD.TJDESLI2.TJ4C3900.S2DQERR.L2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001331', 'TJFD.TJDES001.FTPSMSV.LABEL12', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001332', 'TJFD.TJDES001.FTPSMSV.LABEL15', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001333', 'TJFD.TJDES001.FTPSMSV.LABEL17', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001334', 'TJFD.TJDES001.TJ4C3620.E1DQ0L17.S', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001335', 'TJFD.TJDES001.TJ4C3620.S1DQINC.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001336', 'TJFD.TJDES001.TJ4C3640.E1DQ0L12.S', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001337', 'TJFD.TJDES001.TJ4C3640.S1DQINC.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001338', 'TJFD.TJDES001.TJ4C3650.E1DQ0L13.S', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001339', 'TJFD.TJDES001.TJ4C3650.S1DQINC.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001340', 'TJFD.TJDES001.TJ4C3660.E1DQ0L14.S', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001341', 'TJFD.TJDES001.TJ4C3660.S1DQINC.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001342', 'TJFD.TJDES001.TJ4C3670.E1DQ0L15.S', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001343', 'TJFD.TJDES001.TJ4C3670.S1DQINC.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001344', 'TJFD.TJDES001.TJ4C3670.S2DQSMSV.SM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001345', 'TJFD.TJDES001.TJ4C3680.S1DQINC.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001346', 'TJFD.TJDES001.TJ4C3680.UNLTEVE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001347', 'TJFD.TJDES001.TJ4C3690.S1JDTEVE.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001348', 'TJFD.TJDES001.TJ4C3690.UNLTEVE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001349', 'TJFD.TJDES002.TJ4C3700.S1DQDESC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001350', 'TJFD.TJDES002.TJ4C3700.S2DQINC.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001351', 'TJFD.TJDES002.TJ4C3700.UNLTEVE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001352', 'TJFD.TJDES002.TJ4C3700.UNLTREC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001353', 'TJFD.TJDES002.TJ4C3710.S1DQDESC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001354', 'TJFD.TJDES002.TJ4C3710.S1DQDESC.SM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001355', 'TJFD.TJDES002.TJ4C3710.S2DQINC.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001356', 'TJFD.TJDES002.TJ4C3710.UNLTMOV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001357', 'TJFD.TJDES002.TJ4C3720.AJU.SM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001358', 'TJFD.TJDES002.TJ4C3730.E1DQDESC.S', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001359', 'TJFD.TJDES002.TJ4C3730.S1DQ6690', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001360', 'TJFD.TJDES002.TJ4C3730.S2DQEXT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001361', 'TJFD.TJDES002.TJ4C3730.S2DQEXTR.ACUM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001362', 'TJFD.TJDES002.TJ4C3730.S2DQEXTR.DIA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001364', 'TJFD.TJDES002.TJ4C3730.S3DQDBAN', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001365', 'TJFD.TJDES002.TJ4C3730.S4DQINC.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001366', 'TJFD.TJDES002.UNLOPGN.DES1', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001367', 'TJFD.TJDES002.UNLOPGN.DES2', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001368', 'TJFD.TJDES003.TJ4C37XX.S1DQ6690.S', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001369', 'TJFD.TJDES003.TJ4C3740.S1DQ6690', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001370', 'TJFD.TJDES003.TJ4C3740.S2DQINC.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001371', 'TJFD.TJDES003.TJ4C3740.UNLTREC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001372', 'TJFD.TJDES003.TJ4C3750.S1DQ6690', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001373', 'TJFD.TJDES003.TJ4C3750.S2DQINC.ER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001374', 'TJFD.TJDES003.TJ4C3750.UNLTEVE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001375', 'TV.D671.SECU.LABEL12.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001376', 'TV.D671.SECU.LABEL13.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001377', 'TV.D671.SECU.LABEL14.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001378', 'TV.D671.SECU.LABEL15.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001379', 'TV.D671.SECU.LABEL17.SORT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001380', 'TV.D672.SECU.MOVI.TRANS.DIA.ACUM.BF', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001381', 'BK.IMG.BTJP001.BVPTJREC.P00', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001382', 'BK.IMG.BTJP001.BVPTJEVE.P00', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001383', 'BK.IMG.BTJP001.BVPTJPGN.P00', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001384', 'BK.IMG.BTJP001.BVPTJASI.P00', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001385', 'BK.IMG.BTJP001.BVPTJRCD.P00', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001386', 'BK.IMG.BTJP001.BVPTJIPN.P00', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001387', 'TV.D046.SECU.MOVI.CONSU.VISA.BCOFRAN', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001388', 'TD.D223.KSDS.MAES.CUEN.MESANT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001397', 'ENM.BT.TBT08', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001398', 'ENM.BT.TBT12', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001399', 'ENM.BT.TBT13', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001400', 'ENM.BT.TBT14', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001401', 'ENM.BT.TBT15', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001402', 'ENM.BT.TBT16', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001404', 'ENM.BT.TBT21', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001405', 'ENM.BT.TBT28', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001406', 'ENM.BT.TBT29', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001407', 'ENM.BT.TBT30', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001408', 'ENM.BT.TBT33', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001409', 'ENM.BT.TBT37', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001389', 'ENM.BT.TBT03', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001390', 'ENM.BT.TBT18', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001391', 'ENM.BT.TBT19', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001392', 'ENM.BT.TBT20', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001393', 'ENM.BT.TBT27', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001394', 'ENM.BT.TBT34', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001395', 'ENM.BT.TBT35', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001396', 'ENM.BT.TBT36', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001410', 'NE.D010.KSDS.PARAM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001411', 'NE.D520.ESDS.LOGG', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001412', 'NE.DPQULOG', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001415', 'NE.D533.KSDS.TRANSF.DEBCONS', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001420', 'NE.D800.SECU.MOVI.DESTINO.LON166', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001421', 'NE.D801.SECU.MOVI.DESTINO.LON2006', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001422', 'NE.D803.SECU.MOVI.DAMM.DATANE16.SINCOMP', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001423', 'NE.D808.SECU.MOVI.DACBR.DATANE19', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001424', 'NE.CUENTAS.RECAUDAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001425', 'NE.D000.SECU.MOV.TELEDEP', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001426', 'NE.D000.SECU.MOV.TELEDIS.NUE', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001427', 'NE.D701.TELEDIS.COMISION.SORT', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001428', 'NE.SECU.OPSBBV.DNET.DEBCONS', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001429', 'NE.SECU.OPS.MERGALL0', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001432', 'NE.D006.ESDS.FECH.DB01D91', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001450', 'DN.D003.KSDS.RELABCRA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001451', 'DN.D004.KSDS.REFINANC.RC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001452', 'DN.D007.KSDS.ELIMCDSF', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001453', 'DN.D012.ESDS.LOGGING', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001454', 'DN.D014.KSDS.PARAMSIS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001455', 'DN.D015.KSDS.BCRA0000.ENTLIQ', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001456', 'DN.D016.KSDS.PREVCLIE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001457', 'DN.D018.ESDS.DNRESPON', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001458', 'DN.D019.KSDS.SITCLIEN', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001459', 'DN.D039.ESDS.PREV0300.CONTABIL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001460', 'DN.D039.ESDS.PREV0300.NEW', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001461', 'DN.D040.KSDS.MOVI.ARRESAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001462', 'DN.D042.ESDS.MOVI.CALIF', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001464', 'DN.D052.ESDS.LOGGING.HIST', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001465', 'DN.D060.KSDS.MAES.PRODUC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001472', 'DN.D101.ESDS.SAL09252.HISTO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001473', 'DN.D199.ESDS.LOGGING', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001474', 'DN.D204.KSDS.LOGG.A2562', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001475', 'DN.D204.KSDS.LOGG.A3119', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001476', 'DN.D206.SECU.MOVI.A3119.D43012', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001477', 'DN.D206.SECU.MOVI.A3119.D430567', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001478', 'DN.D206.SECU.MOVI.A3119.D4380', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001479', 'DN.D206.SECU.MOVI.A3119.IDCD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001480', 'DN.D206.SECU.MOVI.A3119.IDPRES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001481', 'DN.D207.SECU.MOVI.BAJACON', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001482', 'DN.D226.KSDS.MOVI.A3119.OPERCAB', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001484', 'DN.D227.ESDS.PREVACT', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001485', 'DN.D227.KSDS.MOVI.A3119.OPERDET', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001486', 'DN.D228.ESDS.MOVI.A3119.CABANT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001487', 'DN.D228.KSDS.MOVI.A3119.LINEAS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001488', 'DN.D266.KSDS.STOCK.REFINANC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001489', 'DN.D286.KSDS.MOVI.DN.SALPRO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001490', 'DN.D304.KSDS.LOGG.A2448', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001491', 'DN.D311.KSDS.MAES.CODPAIS.ALTBCRA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001492', 'DN.D312.KSDS.MAES.TIPDOC99.BCRA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001493', 'DN.D428.KSDS.SALSEM70.DETDEU', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001494', 'DN.D429.KSDS.SALSEM50.SUMDEU', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001495', 'DN.D441.ESDS.SALSEM03', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001496', 'DN.D447.ESDS.PREV0300.NEW', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001497', 'DN.D586.ESDS.MSR8.SALDOS.ATRASO', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001498', 'DN.D780.SECU.SAL00003.TXT', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001499', 'DN.D970.ESDS.MOVI.GARAN', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001500', 'DN.D970.SECU.MOVI.OPERC.CCCPI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001501', 'DN.D970.SECU.MOVI.S16.CONDUP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001502', 'DN.D971.SECU.DNC09701', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001503', 'DN.D971.SECU.DNC0974', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001504', 'DN.D971.SECU.EQUIVALE.CORTES.TIPOPE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001505', 'DN.D971.SECU.EQUIVALE.TIPODO.CODGAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001506', 'DN.D975.SECU.MOVI.BAJACON', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001507', 'DN.D976.SECU.MAES.PADRON', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001508', 'DN.D977.SECU.MOVI.DEUDORES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001509', 'DN.D978.SECU.MAES.NOMENT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001510', 'DN.D979.SECU.MAES.NOMDEU', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001511', 'DN.D980.DEUDA.CENTRAL.CENCLI.F', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001512', 'DN.D980.DEUDA.CENTRAL.CENDES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001513', 'DN.D980.DEUDA.CENTRAL.FECCDR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001514', 'DN.D981.DEUDA.CENTRAL.CONVERTI', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001515', 'DN.D999.SECU.UNLOAD.BASICOSN', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001516', 'DN.D999.SECU.UNLOAD.DOCUMEND', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001517', 'DN.D971.ESDS.MOVI.OPERDAT.S01', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001518', 'DN.D971.ESDS.MOVI.OPERDAT.S13', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001519', 'DN.D971.SECU.OPERDAT.S08', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001520', 'DN.D971.ESDS.MOVI.OPERDAT.S25', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001521', 'DN.D972.ESDS.MOVI.DETACU', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001522', 'DN.D970.SECU.MOVI.GARAN.SAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001523', 'DN.D971.ESDS.MOVI.OPERDAT.S06', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001524', 'DN.D971.ESDS.MOVI.OPERDAT.S10', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001525', 'DN.D973.ESDS.MOVI.DETARJE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001526', 'DN.D971.SECU.MOVI.OPERDAT.SLR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001527', 'DN.D971.SECU.MOVI.OPERDAT.S87', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001528', 'DN.D971.ESDS.MOVI.OPERDAT.S94', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001529', 'DN.D971.ESDS.MOVI.OPERDAT.S96', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001530', 'DN.D971.ESDS.MOVI.OPERDAT.S98', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001565', 'PK.FIX.PEROLES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001611', 'PK.FIX.AR.ALERTAS.SUN', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001637', 'LDFD.BAT1SBAS.MT.FIRCO.SOFT.FIJO', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001638', 'LDFD.BAT1SBAS.MT.PEPS.SORT', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001647', 'LDFD.BAT1SBAS.MT.CTA.INMOV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001648', 'LDFD.BAT1SBAS.MT.CONTROL.PLDE0710', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001649', 'LDFD.BAT1SBAS.MT.CONTROL.PLDE0715', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001650', 'LDFD.BAT1SBAS.MT.CONTROL.PLDE0720', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001651', 'LDFD.BAT1SBAS.MT.CONTROL.PLDE0740', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001652', 'WW.FIX.UNLO.WWDTTRA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001653', 'WW.FIX.UNLO.WWDTINS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001654', 'WW.FIX.UNLO.WWDTRUT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001655', 'WW.FIX.UNLO.WWDTPEN', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001656', 'WW.FIX.UNLO.WWDTREV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001657', 'WW.FIX.UNLO.WWDTCME', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001658', 'WW.FIX.UNLO.WWDTOPC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001659', 'WW.FIX.UNLO.WWDTPRO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001660', 'WW.FIX.UNLO.WWDTEVE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001661', 'WW.FIX.UNLO.WWDTACT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001662', 'WW.FIX.UNLO.WWDTREL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001663', 'WW.FIX.UNLO.WWDTXML', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001664', 'WW.FIX.UNLO.WWDTINV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001665', 'WW.FIX.UNLO.WWDTAYU', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001666', 'WW.FIX.UNLO.WWDTVER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001667', 'WW.FIX.UNLO.WWDTVRA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001669', 'ER.UNLD.CBU', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001670', 'ER.UNLD.EVA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001671', 'ER.UNLD.MAN', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001672', 'ER.UNLD.MCB', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001673', 'TK.UNLD.TTK00', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001674', 'TK.UNLD.TTK05', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001675', 'TK.UNLD.TTK08', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001676', 'TK.UNLD.TTK32', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001677', 'TK.UNLD.TTK37', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001678', 'TK.UNLD.TTK40', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001679', 'TK.UNLD.TTK77', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001680', 'TK.UNLD.TTK93', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001681', 'TK.UNLD.TTK125', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001682', 'TK.D010.SECU.A3405', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001683', 'TK.D010.SECU.A3742', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001684', 'JB.UNLD.ADI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001685', 'JB.UNLD.CAM', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001686', 'JB.UNLD.CDT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001687', 'JB.UNLD.CMP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001688', 'JB.UNLD.COD', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001689', 'JB.UNLD.CSA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001690', 'JB.UNLD.DCA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001691', 'JB.UNLD.DES', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001692', 'JB.UNLD.DIR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001693', 'JB.UNLD.DSA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001694', 'JB.UNLD.FIL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001695', 'JB.UNLD.LAY', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001696', 'JB.UNLD.LCA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001697', 'JB.UNLD.LEY', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001698', 'JB.UNLD.ODE', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001699', 'JB.UNLD.PAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001700', 'JB.UNLD.PER', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001701', 'JB.UNLD.PRO', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001702', 'JB.UNLD.REL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001703', 'JB.UNLD.RFI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001704', 'JB.UNLD.SAL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001705', 'JB.UNLD.SBT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001706', 'JB.UNLD.SEG', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001707', 'JB.UNLD.TRF', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001708', 'JB.UNLD.TSA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001709', 'JB.UNLD.VLA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001710', 'JB.UNLD.VPS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001711', 'JB.UNLD.VPT', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001712', 'JB.UNLD.VTA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001713', 'PU.UNLD.ASI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001714', 'PU.UNLD.BAN', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001715', 'PU.UNLD.REG', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001716', 'PU.UNLD.TAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001717', 'YQ.JYQILOG0.YQDTARV', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001718', 'YQ.UNLD.HIS', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001719', 'YQ.UNLD.PAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001720', 'YQ.JYQILOG0.YQDTPUA', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001721', 'YQ.JYQILOG0.YQDTSOL', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001722', 'YQ.JYQILOG0.YQDTVEP', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001725', 'TK.UNLD.TTK06', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001729', 'TK.UNLD.TTK85', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001733', 'PU.UNLD.ACU', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001735', 'PU.UNLD.CTA', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001736', 'PU.UNLD.CTC', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001740', 'YQ.JYQILOG0.YQDTCON', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001741', 'YQ.UNLD.LOG', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001742', 'YQ.JYQILOG0.YQDTVEN', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001743', 'FBFD.FBDTADH.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001796', 'BGFD.OPEBUZON.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001797', 'BGFD.ACUMTRCUE.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001798', 'BGFD.IMPTOEXC.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001799', 'BGFD.LIQUIMPU.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001800', 'BGFD.TREMITIDO.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001801', 'BGFD.TCP43.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001802', 'BGFD.IMPSTAFE.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001803', 'BGFD.SUCFIRMA.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001805', 'BGFD.COMPENS.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001806', 'BGFD.SALDIACC2.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001808', 'BGFD.INSTRUM.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001814', 'BGFD.TINTEPAP.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001816', 'BGFD.CUENCUEN.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001817', 'BGFD.TCOMISUE.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001818', 'BGFD.TTALOCUE.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001819', 'BGFD.RECLAMO.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001820', 'BGFD.AJUSTES.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001821', 'BGFD.IMAGREMI.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001822', 'BGFD.RECHCOMP.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001823', 'BGFD.TRECHAZO.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001824', 'BGFD.OTROSIS.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001825', 'BGFD.CTACERR.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001826', 'BGFD.TBASIPLA.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001827', 'BGFD.TMOVIPLA.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001828', 'BGFD.TIMPUES.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001830', 'BGFD.BGDTSER.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001831', 'BGFD.BGDTMAR.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001832', 'BGFD.BGDTMRC.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001833', 'BGFD.BGDTCGC.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001839', 'BGFD.EXTRA.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001840', 'BGFD.RELPAQ.UNLOAD', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001847', 'RC.SECU.ORPAPREV', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001848', 'RC.SECU.REBENCTA', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001850', 'RCFD.BAT1SBAS.A00RCAFP', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001851', 'RCFD.BAT1SBAS.A00RCMOV', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001852', 'RCFD.BAT1SBAS.A00RCDET', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001853', 'RCFD.BAT1SBAS.A00RCVLR', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001856', 'JB.UNLD.DIM', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001857', 'WW.FIX.UNLO.WWDTPRS', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001858', 'LDFD.BAT1CONS.LDDTBCR.TOTAL', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001859', 'LDFD.BAT1CONS.UNLOAD.LDDTDOC', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001860', 'LDFD.BAT1CONS.LDDTOPE.TOTAL', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001861', 'LDFD.BAT1CONS.UNLOAD.LDDTPAI', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001862', 'LDFD.BAT1CONS.UNLOAD.LDDTPAR', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001863', 'LDFD.BAT1SBAS.PERF.RGO.CLDTCPPC', '', 'N', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001864', 'ENM.NE.NEDTCCH', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001865', 'ENM.NE.NEDTCCT', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001866', 'ENM.NE.NEDTCGT', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001867', 'ENM.NE.NEDTCPE', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001868', 'ENM.NE.TNE03.ABOCUECLI', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001869', 'ENM.NE.NEDTMAR', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001873', 'TJ.SEQ.TJDTADI', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001875', 'TJ.SEQ.TJDTEXT', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001876', 'TJ.SEQ.TJDTLIQ', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001877', 'TJ.SEQ.TJDTMAC', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001879', 'TJ.SEQ.TJDTMAT', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO
insert into GesPet..HEDT001 values ('V0001880', 'TJ.SEQ.TJDTMOM', '', 'S', '', getdate(), null,'N',null,null,null,'A', null)
GO