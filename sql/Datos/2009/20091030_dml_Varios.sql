-- Par�metro para enmascaramiento de datos sensibles
update GesPet..Varios 
set 
	var_numero = 1,
	var_fecha  = getdate()
where var_codigo = 'TSOENM'
go

-- Cambio de versi�n
update GesPet..Varios 
set 
	var_texto = 'v5.10.0',
	var_fecha = getdate()
where var_codigo = 'CGMVER'
go
