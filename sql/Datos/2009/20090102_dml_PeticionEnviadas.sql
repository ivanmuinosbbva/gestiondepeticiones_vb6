-- Control 
select ENV.pet_nrointerno
from GesPet..PeticionEnviadas ENV left join GesPet..Peticion PT on (ENV.pet_nrointerno = PT.pet_nrointerno)
where ENV.pet_nrointerno in (
	select PET.pet_nrointerno
	from GesPet..Peticion PET
	where PET.cod_estado in ('TERMIN', 'RECHAZ', 'CANCEL', 'RECHTE', 'ANULAD', 'ANEXAD'))
go

-- Elimina de la tabla de peticiones v�lidas (PeticionEnviadas)
delete
from GesPet..PeticionEnviadas
where pet_nrointerno in (
	select ENV.pet_nrointerno
	from GesPet..PeticionEnviadas ENV left join GesPet..Peticion PT on (ENV.pet_nrointerno = PT.pet_nrointerno)
	where ENV.pet_nrointerno in (
		select PET.pet_nrointerno
		from GesPet..Peticion PET
		where PET.cod_estado in ('TERMIN', 'RECHAZ', 'CANCEL', 'RECHTE', 'ANULAD', 'ANEXAD')))
go