-- Alta masiva de nuevo perfil de Analista Ejecutor en dirección de MEDIOS


-- ********************************************************************************************************************************
-- 1. Aquellos que son SOLICITANTES en MEDIOS pero al mismo tiempo son o Resp. de Ejecución o Resp. de Sector
-- Solo eliminamos su perfil de de solicitante
-- ********************************************************************************************************************************
print '1. Cantidad de registros seleccionados para eliminación:'
go
select 1
from GesPet..RecursoPerfil a
where
    exists (select 1 from GesPet..Recurso b where a.cod_recurso = b.cod_recurso and b.cod_direccion = 'MEDIO') and
    a.cod_perfil = 'SOLI' and
    a.cod_recurso in (
    select c.cod_recurso
    from GesPet..RecursoPerfil c
    where c.cod_perfil in ('SOLI','CGRU','CSEC')
    group by c.cod_recurso
    having count(*) > 1
)
go

print 'Borrado de registros...'
go

delete
from GesPet..RecursoPerfil
where
    exists (select 1 from GesPet..Recurso b where GesPet..RecursoPerfil.cod_recurso = b.cod_recurso and b.cod_direccion = 'MEDIO') and
    cod_perfil = 'SOLI' and
    cod_recurso in (
    select c.cod_recurso
    from GesPet..RecursoPerfil c
    where c.cod_perfil in ('SOLI','CGRU','CSEC')
    group by c.cod_recurso
    having count(*) > 1
)
go

-- ********************************************************************************************************************************
-- 2. Solo recursos de MEDIOS con perfil SOLICITANTE (ESTE PERFIL y ninguno de Resp. de Ejecución o de Sector. Puede tener otros)
-- Se elimina su perfil de Solicitante y se adiciona el nuevo perfil de Analista Ejecutor (primero adicionamos, luego quitamos)
-- ********************************************************************************************************************************

-- 2.1. Agrego primero el perfil de Analista Ejecutor para todos aquellos Solicitantes de MEDIOS)
print 'Cantidad de registros seleccionados para eliminación:'
go

insert into GesPet..RecursoPerfil
select
    a.cod_recurso,
    'ANAL',
    a.cod_nivel,
    a.cod_area
from
    GesPet..RecursoPerfil a
where
    exists (select 1 from GesPet..Recurso b where a.cod_recurso = b.cod_recurso and b.cod_direccion = 'MEDIO') and
    a.cod_perfil = 'SOLI' and
    a.cod_recurso in
    (
    select c.cod_recurso
    from GesPet..RecursoPerfil c
    where
        not exists (
            select 1
            from GesPet..RecursoPerfil z
            where z.cod_recurso = a.cod_recurso and
            z.cod_perfil in ('CSEC','CGRU')
                    ) and
        c.cod_perfil in ('SOLI')
    group by c.cod_recurso
    )
go

-- 2.2. Elimino los perfiles de Solicitante porque ahora deben quedar como Analistas
print 'Borrado de registros...'
go

delete
from GesPet..RecursoPerfil
where
    exists (select 1 from GesPet..Recurso b where GesPet..RecursoPerfil.cod_recurso = b.cod_recurso and b.cod_direccion = 'MEDIO') and
    GesPet..RecursoPerfil.cod_perfil = 'SOLI' and
    GesPet..RecursoPerfil.cod_recurso in
    (
    select c.cod_recurso
    from GesPet..RecursoPerfil c
    where
        not exists (
            select 1
            from GesPet..RecursoPerfil z
            where z.cod_recurso = GesPet..RecursoPerfil.cod_recurso and
            z.cod_perfil in ('CSEC','CGRU')
                    ) and
        c.cod_perfil in ('SOLI')
    group by c.cod_recurso
    )
go

-- ********************************************************************************************************************************
-- Aquellos que tienen perfiles de Referente, Autorizante o Supervisor
-- ********************************************************************************************************************************
print 'Cantidad de registros seleccionados para eliminación:'
go

select 1
from GesPet..RecursoPerfil c
where
    exists (select 1 from GesPet..Recurso b where c.cod_recurso = b.cod_recurso and b.cod_direccion = 'MEDIO') and
    c.cod_perfil in ('AUTO','SUPE','REFE')
go

print 'Borrado de registros...'
go

delete
from GesPet..RecursoPerfil
where
    exists (select 1 from GesPet..Recurso b where GesPet..RecursoPerfil.cod_recurso = b.cod_recurso and b.cod_direccion = 'MEDIO') and
    GesPet..RecursoPerfil.cod_perfil in ('AUTO','SUPE','REFE')
go
