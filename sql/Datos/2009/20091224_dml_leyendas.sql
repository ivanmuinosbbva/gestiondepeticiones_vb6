-- Cambio de leyenda: ALTO x NIVEL RIESGO: ALTO
update 
	GesPet..PeticionMemo
set 
	mem_texto = 'NIVEL RIESGO: ALTO'
from 
	GesPet..Peticion a inner join 
	GesPet..PeticionMemo b on a.pet_nrointerno = b.pet_nrointerno inner join 
	GesPet..AgrupPetic c on a.pet_nrointerno = c.pet_nrointerno
where
	a.cod_tipo_peticion in ('AUX','AUI') and 
	a.cod_solicitante = 'E999999' and
	b.mem_campo = 'CARACTERIS' and
	upper(b.mem_texto) like '%ALT_%' and
	c.agr_nrointerno in (807,809,808,812,811,815,814)
go

-- Cambio de leyenda: MEDIO x NIVEL RIESGO: MEDIO
update 
	GesPet..PeticionMemo
set 
	mem_texto = 'NIVEL RIESGO: MEDIO'
from 
	GesPet..Peticion a inner join 
	GesPet..PeticionMemo b on a.pet_nrointerno = b.pet_nrointerno inner join 
	GesPet..AgrupPetic c on a.pet_nrointerno = c.pet_nrointerno
where
	a.cod_tipo_peticion in ('AUX','AUI') and 
	a.cod_solicitante = 'E999999' and
	b.mem_campo = 'CARACTERIS' and
	upper(b.mem_texto) like '%MEDI_%' and
	c.agr_nrointerno in (807,809,808,812,811,815,814)
go

-- Cambio de leyenda: BAJO x NIVEL RIESGO: BAJO
update 
	GesPet..PeticionMemo
set 
	mem_texto = 'NIVEL RIESGO: BAJO'
from 
	GesPet..Peticion a inner join 
	GesPet..PeticionMemo b on a.pet_nrointerno = b.pet_nrointerno inner join 
	GesPet..AgrupPetic c on a.pet_nrointerno = c.pet_nrointerno
where
	a.cod_tipo_peticion in ('AUX','AUI') and 
	a.cod_solicitante = 'E999999' and
	b.mem_campo = 'CARACTERIS' and
	upper(b.mem_texto) like '%BAJ_%' and
	c.agr_nrointerno in (807,809,808,812,811,815,814)
go