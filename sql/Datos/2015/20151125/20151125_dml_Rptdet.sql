INSERT INTO Rptdet VALUES ('RPT1', 1, 'Peticiones al Referente de Sistema por > 30 d�as', 'S')
go
INSERT INTO Rptdet VALUES ('RPT1', 2, 'Peticiones "A estimar esfuerzo / A planificar" por > 30 d�as', 'S')
go
INSERT INTO Rptdet VALUES ('RPT1', 3, 'Peticiones "A estimar esfuerzo / A planificar" con > 30 horas cargadas ', 'S')
go
INSERT INTO Rptdet VALUES ('RPT1', 4, 'Peticiones con > 30 d�as de atraso respecto de su fecha de inicio de planificaci�n', 'S')
go
INSERT INTO Rptdet VALUES ('RPT1', 5, 'Peticiones "En ejecuci�n" por > un a�o', 'S')
go
INSERT INTO Rptdet VALUES ('RPT1', 6, 'Peticiones "En ejecuci�n" con > 30 d�as de atraso respecto de su fecha de finalizaci�n planificada', 'S')
go
INSERT INTO Rptdet VALUES ('RPT1', 7, 'Peticiones "En ejecuci�n" sin horas cargadas en los �ltimos 60 d�as', 'S')
go
INSERT INTO Rptdet VALUES ('RPT1', 8, 'Peticiones "En ejecuci�n" con fecha prevista de pasaje a producci�n vencida por > 15 d�as', 'S')
go
INSERT INTO Rptdet VALUES ('RPT1', 9, 'Peticiones en estado "Suspendido" por > 60 d�as', 'S')
go
INSERT INTO Rptdet VALUES ('RPT1', 10, 'Peticiones con > 40 horas cargadas por un grupo no vinculado a la petici�n', 'S')
go
INSERT INTO Rptdet VALUES ('RPT1', 11, 'Petici�n con TEST por > 60 d�as, sin importar el estado', 'S')
go
INSERT INTO Rptdet VALUES ('RPT1', 12, 'Peticiones de clase Correctivo, Atenci�n a usuario y SPUFI en "Ejecuci�n" con > 50 horas cargadas', 'S')
go
INSERT INTO Rptdet VALUES ('RPT1', 13, 'Peticiones de clase SPUFI en "Ejecuci�n" con m�s de 15 d�as desde su fecha de inicio real', 'S')
go
INSERT INTO Rptdet VALUES ('RPT1', 14, 'OMAs pendientes', 'S')
go
