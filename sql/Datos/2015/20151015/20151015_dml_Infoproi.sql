-- Items
INSERT INTO Infoproi VALUES (1, '�La clasificaci�n de la petici�n es la esperada?', 'S', 'A', 'BPAR', 'Debe revisar la clase de la petici�n, porque no es correcta.')
GO
INSERT INTO Infoproi VALUES (2, '�El campo descripci�n del pedido es claro y describe razonablemente un objetivo?', 'S', 'A', 'BPAR', 'No esta claramente especificada la descripci�n de la petici�n.')
GO
INSERT INTO Infoproi VALUES (3, '�Es correcto el impacto tecnol�gico declarado?', 'S', 'A', 'BPAR', 'NULL')
GO
INSERT INTO Infoproi VALUES (4, 'Si la petici�n tiene IT, �se refleja en el C100 la intervenci�n de las �reas t�cnicas correspondientes?', 'S', 'A', 'BPAR', 'NULL')
GO
INSERT INTO Infoproi VALUES (5, 'Documento de alcance adjuntado', 'S', 'B', 'MULT', 'NULL')
GO
INSERT INTO Infoproi VALUES (6, '�Es correcto el documento adjunto?', 'S', 'A', 'BPAR', 'NULL')
GO
INSERT INTO Infoproi VALUES (7, 'Completitud del documento de alcance', 'S', 'A', 'BPAR', 'NULL')
GO
INSERT INTO Infoproi VALUES (8, '�Se realiz� la aceptaci�n del documento de alcance?', 'S', 'A', 'BPAR', 'NULL')
GO
INSERT INTO Infoproi VALUES (9, '�Qui�n realiz� la aceptaci�n del documento de alcance?', 'S', 'C', 'BPAR', 'NULL')
GO
INSERT INTO Infoproi VALUES (10, '�Se adjunta mail de usuario donde expresa su conforme al documento de alcance?', 'S', 'A', 'BPAR', 'NULL')
GO
INSERT INTO Infoproi VALUES (11, '�El mail adjunto contiene el conforme expreso del usuario?', 'S', 'A', 'BPAR', 'NULL')
GO
INSERT INTO Infoproi VALUES (12, '�Se adjunta el documento CG04?', 'S', 'A', 'BPAR', 'NULL')
GO
INSERT INTO Infoproi VALUES (13, 'Completitud del documento CG04', 'S', 'A', 'BPAR', 'NULL')
GO
INSERT INTO Infoproi VALUES (14, '�Se adjunta el documento C204 de Pruebas Unitarias?', 'S', 'A', 'GRUT', 'Falta el documento C204.')
GO
INSERT INTO Infoproi VALUES (15, 'Completitud del C204 de Pruebas Unitarias', 'S', 'A', 'GRUT', 'NULL')
GO
INSERT INTO Infoproi VALUES (16, '�Se adjunta el documento C204 de Pruebas de Sistema?', 'S', 'A', 'GRUT', 'Faltan las Pruebas de Sistema del documento C204.')
GO
INSERT INTO Infoproi VALUES (17, 'Completitud del C204 de Pruebas de Sistema', 'S', 'A', 'GRUT', 'El documento C204 (pruebas de sistema) est� incompleto o alguno de los items del mismo son incorrectos.')
GO
INSERT INTO Infoproi VALUES (18, '�Se ingres� el OK de las pruebas de usuario?', 'S', 'A', 'BPAR', 'NULL')
GO
INSERT INTO Infoproi VALUES (19, '�Qui�n ingres� el OK de las pruebas de usuario?', 'S', 'C', 'BPAR', 'NULL')
GO
INSERT INTO Infoproi VALUES (20, '�Se adjunta mail de usuario donde expresa su conforme a las pruebas?', 'S', 'A', 'BPAR', 'NULL')
GO
INSERT INTO Infoproi VALUES (21, '�El mail adjunto contiene el conforme expreso del usuario a las pruebas?', 'S', 'A', 'BPAR', 'NULL')
GO
INSERT INTO Infoproi VALUES (22, '�Se adjunta el documento T710?', 'S', 'A', 'GRUT', 'NULL')
GO
INSERT INTO Infoproi VALUES (23, 'Completitud del documento T710', 'S', 'A', 'GRUT', 'NULL')
GO
INSERT INTO Infoproi VALUES (24, 'Se hicieron verificaciones adicionales �fueron satisfactorias?', 'S', 'A', 'NULL', 'NULL')
GO
INSERT INTO Infoproi VALUES (25, 'Otras observaciones', 'N', 'A', 'NULL', 'NULL')
GO

/*
insert into Infoproi values (1, '�La clasificaci�n de la petici�n es la esperada?', 'S')
go
insert into Infoproi values (2, '�El campo descripci�n del pedido es claro y describe razonablemente un objetivo?', 'S')
go
insert into Infoproi values (3, '�Es correcto el impacto tecnol�gico declarado?', 'S')
go
insert into Infoproi values (4, 'Si la petici�n tiene IT, �se refleja en el C100 la intervenci�n de las �reas t�cnicas correspondientes?', 'S')
go
insert into Infoproi values (5, 'Documento de alcance adjuntado', 'S')
go
insert into Infoproi values (6, '�Es correcto el documento adjunto?', 'S')
go
insert into Infoproi values (7, 'Completitud del documento de alcance', 'S')
go
insert into Infoproi values (8, '�Se realiz� la aceptaci�n del documento de alcance?', 'S')
go
insert into Infoproi values (9, '�Qui�n realiz� la aceptaci�n del documento de alcance?', 'S')
go
insert into Infoproi values (10, '�Se adjunta mail de usuario donde expresa su conforme al documento de alcance?', 'S')
go
insert into Infoproi values (11, '�El mail adjunto contiene el conforme expreso del usuario?', 'S')
go
insert into Infoproi values (12, '�Se adjunta el documento CG04?', 'S')
go
insert into Infoproi values (13, 'Completitud del documento CG04', 'S')
go
insert into Infoproi values (14, '�Se adjunta el documento C204 de Pruebas Unitarias?', 'S')
go
insert into Infoproi values (15, 'Completitud del C204 de Pruebas Unitarias', 'S')
go
insert into Infoproi values (16, '�Se adjunta el documento C204 de Pruebas de Sistema?', 'S')
go
insert into Infoproi values (17, 'Completitud del C204 de Pruebas de Sistema', 'S')
go
insert into Infoproi values (18, '�Se ingres� el OK de las pruebas de usuario?', 'S')
go
insert into Infoproi values (19, '�Qui�n ingres� el OK de las pruebas de usuario?', 'S')
go
insert into Infoproi values (20, '�Se adjunta mail de usuario donde expresa su conforme a las pruebas?', 'S')
go
insert into Infoproi values (21, '�El mail adjunto contiene el conforme expreso del usuario a las pruebas?', 'S')
go
insert into Infoproi values (22, '�Se adjunta el documento T710?', 'S')
go
insert into Infoproi values (23, 'Completitud del documento T710', 'S')
go
insert into Infoproi values (24, 'Se hicieron verificaciones adicionales �fueron satisfactorias?', 'S')
go
*/