-- Infoproc
insert into Infoproc values (1, 'Informe Inicial', 'C', '1', 'S',1,0)
go
insert into Infoproc values (2, 'Informe de Seguimiento', 'C', '2', 'S',1,0)
go
insert into Infoproc values (3, 'Informe Preliminar', 'C', '3', 'S',1,0)
go
insert into Infoproc values (4, 'Informe reducido (Inicial/Seguim.)', 'R', '1', 'S',1,0)			-- Para peticiones CORR, ATEN y SPUF.
go
insert into Infoproc values (5, 'Informe reducido (Final)', 'R', '1', 'S',1,0)						-- Para peticiones CORR, ATEN y SPUF.
go
insert into Infoproc values (6, 'Informe OKPP', 'C', '4', 'S',1,0)
go
