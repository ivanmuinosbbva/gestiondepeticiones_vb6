-- Cambia la denominación de Business Partner a Referente de Sistema
use GesPet
go

update Perfil
set nom_perfil = 'Referente de Sistema'
where cod_perfil = 'BPAR'
go

