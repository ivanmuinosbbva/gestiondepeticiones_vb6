insert into Acciones values ('SIOK.P', 'Conforme (Seguridad Informática)', 'N', 'ACC', null, '', null, 'N','N')
go
insert into Acciones values ('SIOB.P', 'Conforme con observaciones (Seguridad Informática)', 'N', 'ACC', null, '', null, 'N','N')
go
insert into Acciones values ('SIRE.P', 'No conforme (Seguridad Informática)', 'N', 'ACC', null, '', null, 'N','N')
go
insert into Acciones values ('AROK.P', 'Conforme (Arquitectura)', 'N', 'ACC', null, '', null, 'N','N')
go
insert into Acciones values ('AROB.P', 'Conforme con observaciones (Arquitectura)', 'N', 'ACC', null, '', null, 'N','N')
go
insert into Acciones values ('ARRE.P', 'No conforme (Arquitectura)', 'N', 'ACC', null, '', null, 'N','N')
go

insert into Acciones values ('SIOK.F', 'Conforme (Seguridad Informática)', 'N', 'ACC', null, '', null, 'N','N')
go
insert into Acciones values ('SIOB.F', 'Conforme con observaciones (Seguridad Informática)', 'N', 'ACC', null, '', null, 'N','N')
go
insert into Acciones values ('SIRE.F', 'No conforme (Seguridad Informática)', 'N', 'ACC', null, '', null, 'N','N')
go
insert into Acciones values ('AROK.F', 'Conforme (Arquitectura)', 'N', 'ACC', null, '', null, 'N','N')
go
insert into Acciones values ('AROB.F', 'Conforme con observaciones (Arquitectura)', 'N', 'ACC', null, '', null, 'N','N')
go
insert into Acciones values ('ARRE.F', 'No conforme (Arquitectura)', 'N', 'ACC', null, '', null, 'N','N')
go

insert into Acciones values ('PCHGPRI1','Asignación inicial de priorización','N','EVT',0,'Asignación inicial de priorización',null,'N','N')
go
insert into Acciones values ('PCHGPRI2','Cambio de priorización','N','EVT',0,'Cambio de priorización',null,'N','N')
go

insert into Acciones values ('PCHGVAL1','Asignación inicial de valoración','N','EVT',0,'Asignación inicial de valorización',null,'N','N')
go
insert into Acciones values ('PCHGVAL2','Cambio de valoración','N','EVT',0,'Cambio en la valoración',null,'N','N')
go

insert into Acciones values ('PCHGPRI0','Asignación automática a la planificación', 'N','ACC', 0, '', 0, 'N','N')
go


