update Documento
set actual = 'N'
where cod_doc IN (
'ALCA',
'COST',
'MAIL',
'PLEJ',
'PLIM',
'REQU',
'T700')

update Documento
set actual = 'S'
where cod_doc IN (
'C100',
'C102',
'C104',
'C204',
'C310',
'CG04',
'EML1',
'EML2',
'EML3',
'IDH1',
'IDH2',
'IDH3',
'OTRO',
'P950',
'T710')
