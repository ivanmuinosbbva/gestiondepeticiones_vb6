-- Vuelco inicial de planificación (Periodos)

-- Septiembre 2016
INSERT INTO Periodo 
VALUES (1, 'Mensual - 2016, Septiembre', 'M', '20160912', '20161011', 'PLAN40', '20161011', SUSER_NAME(),'C1-2016-09')
go

-- Octubre 2016
INSERT INTO Periodo 
VALUES (2, 'Mensual - 2016, Octubre', 'M', '20161012', '20161114', 'PLAN40', '20161115', SUSER_NAME(),'C2-2016-10')
go

-- Noviembre 2016
INSERT INTO Periodo 
VALUES (3, 'Mensual - 2016, Noviembre', 'M', '20161115', '20161214', 'PLAN40', '20161214', SUSER_NAME(),'C3-2016-11')
go

-- Diciembre 2016
INSERT INTO Periodo 
VALUES (4, 'Mensual - 2016, Diciembre-Enero', 'X', '20161215', '20161231', 'PLAN40', '20161231', SUSER_NAME(),'C4-2016-12')
go

-- Febrero 2017
INSERT INTO Periodo 
VALUES (5, 'Mensual - 2017, Febrero', 'M', '20170202', '20170303', 'PLAN40', '20170303', SUSER_NAME(),'C5-2017-02')
go

-- Marzo 2017
INSERT INTO Periodo 
VALUES (6, 'Mensual - 2017, Marzo', 'M', '20170303', '20170404', 'PLAN40', '20170404', SUSER_NAME(),'C6-2017-03')
go

-- Abril 2017
INSERT INTO Periodo 
VALUES (7, 'Mensual - 2017, Abril', 'M', '20170404', '20170501', 'PLAN40', '20170501', SUSER_NAME(),'C7-2017-04')
go

-- Mayo 2017
INSERT INTO Periodo 
VALUES (8, 'Mensual - 2017, Mayo', 'M', '20170501', '20170531', 'PLAN40', '20170531', SUSER_NAME(),'C8-2017-05')
go
