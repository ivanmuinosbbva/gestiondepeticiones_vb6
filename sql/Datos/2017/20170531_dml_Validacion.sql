insert into Validacion values (1, 1, 'El proyecto requiere una infraestructura o licencias que no existen en el pa�s', 1, 'S', suser_name(), getdate())
GO
insert into Validacion values (1, 2, 'Renovaci�n de licencias con impacto a mediano o largo plazo', 2, 'S', suser_name(), getdate())
GO
insert into Validacion values (1, 3, 'Renovaci�n de licencias cuyo monto es mayor a $ 100.000.-', 3, 'S', suser_name(), getdate())
GO
insert into Validacion values (1, 4, 'Novedad tecnol�gica, por utilizaci�n de nuevos productos/arquitectura', 4, 'S', suser_name(), getdate())
GO
insert into Validacion values (1, 5, 'Nuevos m�dulos en plataforma distribu�da o host', 5, 'S', suser_name(), getdate())
GO
insert into Validacion values (1, 6, 'Riesgo obsolencia tecnol�gica, software/herramientas sin soporte o no escalables', 6, 'S', suser_name(), getdate())
GO
insert into Validacion values (1, 7, 'Reingenier�a, ya sea funcional o t�cnica', 7, 'S', suser_name(), getdate())
GO
insert into Validacion values (1, 8, 'Estrat�gicos y/o globales para el grupo', 8, 'S', suser_name(), getdate())
GO
insert into Validacion values (1, 9, 'Afectaci�n en el nivel de seguridad', 9, 'S', suser_name(), getdate())
GO
insert into Validacion values (1, 10, 'Proyectos que implican conexiones con el exterior tanto de entrada como salida', 10, 'S', suser_name(), getdate())
GO
insert into Validacion values (1, 11, 'Evolutivos con significaci�n relevante para el negocio', 11, 'S', suser_name(), getdate())
GO
insert into Validacion values (1, 12, 'Solicitud de participaci�n expresa', 12, 'S', suser_name(), getdate())
GO
insert into Validacion values (2, 1, 'Renovaciones o adquisiciones de licencias que sean de seguridad', 1, 'S', suser_name(), getdate())
GO
insert into Validacion values (2, 2, 'Evolutivos o cambios que modifiquen el mecanismo de seguridad actual (Ejemplo: Gesti�n de Claves, Perfiles, Certificados)', 2, 'S', suser_name(), getdate())
GO
insert into Validacion values (2, 3, 'Proyectos de infraestructura / desarrollos que lleven consigo salida de informaci�n', 4, 'S', suser_name(), getdate())
GO
insert into Validacion values (2, 4, 'Contrataci�n de nuevo servicio o plataforma (Ejemplo: agente virtual, servicio de impresi�n, aplicaci�n de RRHH)', 7, 'S', suser_name(), getdate())
GO
insert into Validacion values (2, 5, 'Adquisici�n de nueva aplicaci�n con operatoria on-line o desarrollo de nueva aplicaci�n por parte del Grupo BBVA o del propio BBVA Banco Franc�s.', 6, 'S', suser_name(), getdate())
GO
insert into Validacion values (2, 6, 'Evolutivos o cambios que modifiquen par�metros de seguridad (Ejemplo: Caducidad de Claves, Monto de Trasferencia, Monto de Extracci�n)', 3, 'S', suser_name(), getdate())
GO
insert into Validacion values (2, 7, 'Proyectos normativos que implican desarrollos', 5, 'S', suser_name(), getdate())
GO
insert into Validacion values (2, 8, 'Solicitud de participaci�n expresa', 999, 'S', suser_name(), getdate())
GO
