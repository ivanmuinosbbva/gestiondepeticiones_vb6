
-- Indica la fecha de inicio general del proceso. 
INSERT INTO Varios 
VALUES ('PLN001', 0, 'Inicio general', getdate())
go

-- La periodicidad del proceso (meses, semanas, etc.)
INSERT INTO Varios 
VALUES ('PLN002', 1, 'M', getdate())
go

-- Ac� se guarda la pr�xima fecha de inicio del siguiente proceso.
INSERT INTO Varios 
VALUES ('PLN003', 1, 'Pr�ximo inicio de proceso', getdate())
go

/* 
Luego de iniciado un proceso, ac� se determina la cantidad de d�as 
cuando se hace la aprobaci�n autom�tica de peticiones (pasa el estado 
del "Ref. de RGyP" al "Ref. de sistemas"). 
*/
INSERT INTO Varios 
VALUES ('PLN004', 3, 'Dias habiles para la aprobacion', getdate())
go

/*
Para identificar proyectos estrat�gicos y de reingenier�a
*/

INSERT INTO Varios 
VALUES ('PRJ_PES', 7, 'Proyectos estrat�gicos', getdate())
go

INSERT INTO Varios 
VALUES ('PRJ_ENG', 8, 'Proyectos reingenier�a', getdate())
go
