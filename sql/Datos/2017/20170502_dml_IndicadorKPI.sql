-- Nuevos KPI para empresas vinculadas
INSERT INTO IndicadorKPI VALUES (100, 'Horas hombre', 'S','S','HRSANU','',2)
go
INSERT INTO IndicadorKPI VALUES (101, 'Ventas', 'S','S','UNIMEN','',2)
go
INSERT INTO IndicadorKPI VALUES (102, 'Comisiones', 'S','S','PESMEN','',2)
go
INSERT INTO IndicadorKPI VALUES (103, 'Operaciones / Transacciones', 'S','S','UNIMEN','',2)
go
INSERT INTO IndicadorKPI VALUES (104, 'Tiempo del proceso', 'S','S','MIN','',2)
go
INSERT INTO IndicadorKPI VALUES (105, 'Clientes', 'S','S','UNIMEN','',2)
go
INSERT INTO IndicadorKPI VALUES (106, 'Disponibilidad', 'S','S','MIN','',2)
go
INSERT INTO IndicadorKPI VALUES (107, 'Reclamos / Consultas', 'S','S','UNIMEN','',2)
go
INSERT INTO IndicadorKPI VALUES (108, 'IRENE', 'S','S','IRENE','',2)
go

-- Actualiza el dato de empresa para los viejos KPI
UPDATE IndicadorKPI
SET kpiemp = 1
WHERE kpiid < 100
go
