UPDATE Drivers
SET driverReq = 'S'
WHERE driverId in (3,6,101,104,105) 
go

-- IMPACTOS
insert into Drivers values (100,'Voz del cliente (IRENE)','Si el pedido mejora un punto evaluado en IRENE, indicar el valor obtenido en la �ltima encuesta.', 9, 'SOLI','S', 100, 'S')
go
insert into Drivers values (101,'Impacto en Resultados','Indicar si el desarrollo generar� un incremento en ingresos netos o reducci�n de costos en $ anuales.', 8, 'SOLI','S', 100, 'S')
go
insert into Drivers values (102,'Estrat�gico','Indicar si el desarrollo est� asociado a un proyecto estrat�gico de la Direcci�n.', 9, 'SOLI','S', 100, 'S')
go
insert into Drivers values (103,'Mundo Digital','Indicar si desarrolla nueva funcionalidad en Web o Mobile.', 5, 'SOLI','S', 100, 'S')
go
insert into Drivers values (104,'Productividad','Indicar si libera tiempo de funcionarios.', 5, 'SOLI','S', 100, 'S')
go
insert into Drivers values (105,'Foco en el cliente','Reduce tiempo de proceso para el cliente.', 8, 'SOLI','S', 100, 'S')
go
insert into Drivers values (106,'Grado de innovaci�n','Indicar si el requerimiento representa una innovaci�n.', 7, 'SOLI','S', 100, 'S')
go
insert into Drivers values (107,'Oportunidad de corto plazo','Indicar si la oportunidad del negocio requiere una fecha espec�fica de implementaci�n en el corto plazo.', 5, 'SOLI','S', 100, 'S')
go

-- FACILIDAD
insert into Drivers values (150,'N�mero de �reas involucradas','Indicar la cantidad de gerencias participantes en que colaboran en el proyecto.', 1, 'BPE','S', 101, 'S')
go
insert into Drivers values (151,'Costo estimado de ejecuci�n','Indicar la estimaci�n de hs de ejecici�n.', 5, 'BPE','S', 101, 'S')
go
insert into Drivers values (152,'Tiempo estimado de ejecuci�n','Indicar la estimaci�n de tiempo para la implementaci�n.', 4, 'BPE','S', 101, 'S')
go
insert into Drivers values (153,'Requiere aprovisionamiento','Indicar el grado de participaci�n de Infraestructura Tecnolog�a y Seguridad Inform�tica.', 3, 'BPE','S', 101, 'S')
go
