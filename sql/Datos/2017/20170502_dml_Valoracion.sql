insert into Valoracion values (1000, 'NO',0,1,'S',100)
go
insert into Valoracion values (1001, '76 a 100',1,2,'S',100)
go
insert into Valoracion values (1002, '51 a 75',3,3,'S',100)
go
insert into Valoracion values (1003, '26 a 50',4,4,'S',100)
go
insert into Valoracion values (1004, '0 a 25',5,5,'S',100)
go

insert into Valoracion values (1010, '$0 x a�o',0,1,'S',101)
go
insert into Valoracion values (1011, 'hasta $100M x a�o',0.5, 2,'S',101)
go
insert into Valoracion values (1012, 'hasta $500M x a�o',1.0, 3,'S',101)
go
insert into Valoracion values (1013, 'hasta $1MM x a�o',2.0, 4,'S',101)
go
insert into Valoracion values (1014, 'hasta $2MM x a�o',4.0, 5,'S',101)
go
insert into Valoracion values (1015, 'm�s de $2MM x a�o',5.0, 6,'S',101)
go

insert into Valoracion values (1020, 'No asociado',0.0, 1,'S',102)
go
insert into Valoracion values (1021, 'Asociado con seguimiento Local',3.0, 2,'S',102)
go
insert into Valoracion values (1022, 'Asociado con seguimiento Corporativo',5.0, 3,'S',102)
go

insert into Valoracion values (1030, 'No agrega nueva funcionalidad',0.0, 1,'S',103)
go
insert into Valoracion values (1031, 'Agrega nueva funcionalidad en Web o Mobile',5.0, 2,'S',103)
go

insert into Valoracion values (1040, 'No libera tiempo o reduce la carga operativa de forma significativa', 0.0, 1,'S',104)
go
insert into Valoracion values (1041, 'Libera tiempo, pero no se traduce en un alto ahorro', 1.0, 2,'S',104)
go
insert into Valoracion values (1042, 'Libera tiempo operativo en Cobranzas', 2.0, 3,'S',104)
go
insert into Valoracion values (1043, 'Libera tiempo operativo en Siniestros', 3.0, 4,'S',104)
go
insert into Valoracion values (1044, 'Libera tiempo operativo en Emisi�n', 4.0, 5,'S',104)
go
insert into Valoracion values (1045, 'Libera tiempo operativo en Tecnica y Suscripci�n', 5.0, 6,'S',104)
go

insert into Valoracion values (1050, 'No reduce', 0.0, 1,'S',105)
go
insert into Valoracion values (1051, 'Hasta un 25%', 2.0, 2,'S',105)
go
insert into Valoracion values (1052, 'Hasta un 50%', 2.0, 3,'S',105)
go
insert into Valoracion values (1053, 'Hasta un 75%', 4.0, 4,'S',105)
go
insert into Valoracion values (1054, 'M�s de un 75%', 5.0, 5,'S',105)
go


insert into Valoracion values (1060, 'No tiene', 0.0, 1,'S',106)
go
insert into Valoracion values (1061, 'Agrega atributos a un producto existente para alcanzar la competencia', 3.0, 2,'S',106)
go
insert into Valoracion values (1062, 'Agrega atributos novedosos en el mercado para un producto existente', 4.0, 3,'S',106)
go
insert into Valoracion values (1063, 'Genera un nuevo producto que no existe en el mercado', 5.0, 4,'S',106)
go

insert into Valoracion values (1070, 'No es una oportunidad del negocio que requiera una fecha espec�fica de implementaci�n en el corto plazo', 0.0, 1,'S',107)
go
insert into Valoracion values (1071, 'Es una oportunidad del negocio que requiere una fecha espec�fica de implementaci�n en el corto plazo', 5.0, 2,'S',107)
go


insert into Valoracion values (1100, 'Implica la colaboraci�n de 1 area (fragmentaci�n baja)', 5.0, 1,'S',150)
go
insert into Valoracion values (1101, 'Implica la colaboraci�n de 2-3 �reas (fragmentaci�n media)', 3.0, 2,'S',150)
go
insert into Valoracion values (1102, 'Implica la colaboraci�n de >=4 �reas (fragmentaci�n alta)', 0.0, 3,'S',150)
go


insert into Valoracion values (1110, 'Bajo costo de ejecuci�n (hasta 250 hs)', 5.0, 1,'S',151)
go
insert into Valoracion values (1111, 'Moderado costo de ejecuci�n (hasta 500 hs)', 3.0, 2,'S',151)
go
insert into Valoracion values (1112, 'Alto costo de ejecuci�n (hasta 1M hs)', 1.0, 3,'S',151)
go
insert into Valoracion values (1113, 'Muy alto costo de ejecuci�n (m�s de 1M hs)', 0.0, 4,'S',151)
go

insert into Valoracion values (1120, 'Desarrollo inform�tico peque�o (<3m)', 5.0, 1,'S',152)
go
insert into Valoracion values (1121, 'Desarrollo inform�tico medio (3m-6m)', 3.0, 2,'S',152)
go
insert into Valoracion values (1123, 'Gran desarrollo inform�tico (>6m)', 0.0, 3,'S',152)
go

insert into Valoracion values (1130, 'Nula', 5.0, 1,'S',153)
go
insert into Valoracion values (1131, 'Baja', 3.0, 2,'S',153)
go
insert into Valoracion values (1132, 'Media', 2.0, 3,'S',153)
go
insert into Valoracion values (1133, 'Alta', 0.0, 4,'S',153)
go
