-- Actualizo los nuevos datos para los registros existentes
UPDATE Indicador
SET 
	indicadorEmp = 1,
	indicadorCategoria = 'IMPACTO'
WHERE  
	indicadorId = 1
go

UPDATE Indicador
SET 
	indicadorEmp = 1,
	indicadorCategoria = 'FACILIDA'
WHERE  
	indicadorId = 2
go

-- Agrego los nuevos indicadores para otras empresas
INSERT INTO Indicador 
VALUES (100, 'Impacto Vinculadas', 'S',2,'IMPACTO')
go
INSERT INTO Indicador 
VALUES (101, 'Facilidad Vinculadas', 'S',2,'FACILIDA')
go
