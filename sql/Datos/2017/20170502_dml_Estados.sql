-- Actualizacion de datos de Estados
update Estados
set terminal = 'N'
go

update Estados
set terminal = 'S'
where 
	charindex(cod_estado,'RECHAZ|CANCEL|RECHTE|TERMIN|ANEXAD|ANULAD|')>0
go

-- Indica las imagenes a utilizar
-- Estados que avanzan (por default)

-- Estados que retroceden
UPDATE Estados
SET imagen = 3
WHERE cod_estado in ("DEVSOL","DEVREF","DEVSUP","DEVAUT")
go

-- Estados terminales
UPDATE Estados
SET imagen = 1
WHERE cod_estado in ("RECHAZ","CANCEL","RECHTE","TERMIN","ANEXAD","ANULAD")
go


-- Estados por default para otros estados
UPDATE Estados
SET default_est = 'OPINOK'
WHERE cod_estado = 'OPINIO'
go
UPDATE Estados
SET default_est = 'EVALOK'
WHERE cod_estado = 'EVALUA'
go
UPDATE Estados
SET default_est = 'EJECUC'
WHERE cod_estado = 'SUSPEN'
go
UPDATE Estados
SET default_est = 'EJECUC'
WHERE cod_estado = 'PLANOK'
go
UPDATE Estados
SET default_est = 'PLANOK'
WHERE cod_estado = 'PLANIF'
go
UPDATE Estados
SET default_est = 'PLANIF'
WHERE cod_estado = 'ESTIOK'
go
UPDATE Estados
SET default_est = 'ESTIOK'
WHERE cod_estado = 'ESTIMA'
go
UPDATE Estados
SET default_est = 'TERMIN'
WHERE cod_estado = 'EJECUC'
go
UPDATE Estados
SET default_est = 'COMITE'
WHERE cod_estado = 'REFBPE'
go
