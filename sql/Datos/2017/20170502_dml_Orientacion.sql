update Orientacion set descripcion = 'Requerimientos que inciden directamente sobre la experiencia de los clientes al efectuar consultas u operar por cualquiera de los canales habilitados. Se espera que la resoluci�n de estos pedidos repercuta favorablemente sobre el IRENE.' where cod_orientacion = 'AA'
go
update Orientacion set descripcion = 'Apunta a reducir los costos o aumentar los ingresos mediante la reducci�n de carga operativa y la optimizaci�n de aplicativos y procesos.' where cod_orientacion = 'E'
go
update Orientacion set descripcion = 'Resoluci�n de observaciones de auditor�a, normativa vigente y deficiencias que se detecten en materia de seguridad y control interno.' where cod_orientacion = 'AD'
go
update Orientacion set descripcion = 'Desarrollo de procesos y aplicativos que sostienen el v�nculo comercial y la venta de productos. No tienen como objetivo inmediato la mejora de la experiencia del cliente.' where cod_orientacion = 'D'
go
update Orientacion set descripcion = 'Pedidos que inciden sobre los resultados, a pesar de no tener car�cter comercial ni aumentar la productividad.  Incluye tambi�n los relacionados a la asignaci�n de los resultados a las distintas �reas.' where cod_orientacion = 'G'
go
update Orientacion set descripcion = 'Pedidos que apuntan a reducir o controlar los riesgos crediticios y financieros, m�s all� de que est�n o no originados en la Dir. de Riesgos. No incluye riesgos de car�cter reputacional u operacional que quedan dentro de Control y normas regulatorias.' where cod_orientacion = 'S'
go
update Orientacion set descripcion = 'Pedidos que caen en esta categor�a por su tem�tica relacionada con Contabilidad y Finanzas. No es relevante para el cliente ni las �reas comerciales, no suma eficiencia, no mejora el control, no incrementa el beneficio ni reduce los riesgos.' where cod_orientacion = 'F'
go
update Orientacion set descripcion = 'Necesidades de �ndole t�cnica que no se encuentran directamente atadas a objetivos m�s concretos.' where cod_orientacion = 'I'
go
update Orientacion set descripcion = 'Casos en los que se hace necesario corregir datos err�neos, compensar sus efectos o actualizarlos mediante un proceso de sistemas.' where cod_orientacion = 'AB'
go
update Orientacion set descripcion = 'Pedidos originados en una decisi�n Corporativa o Gerencial y que no se ajustan a ninguno de las otras orientaciones.' where cod_orientacion = 'AC'
go

-- Agregado de nuevo dato Orientaci�n
INSERT INTO Orientacion  
VALUES ('AE','Mejora de Procesos','S',25,'Mejora de procesos nuevos/existentes')
go
