/*
-- F�bricas por Gerencia
insert into GesPet..Fabrica values ('FSWDYD01','F�brica de Software DYD - Host','DESA','MEDIO',null,null,'A')
go
insert into GesPet..Fabrica values ('FSWORG01','F�brica BP','ORG.','MEDIO',null,null,'A')
go
insert into GesPet..Fabrica values ('FSWDYD02','F�brica de Software DyD - Java','DESA','MEDIO',null,null,'A')
go
insert into GesPet..Fabrica values ('FSWDYD03','Paquetes de Horas a Consultora','DESA','MEDIO',null,null,'A')
go
*/

-- F�bricas por Gerencia (BP)
insert into GesPet..Fabrica values ('FSWORG01','F�brica BP','ORG.','MEDIO',null,null,'A')
GO

-- F�bricas por grupo (DYD)
insert into GesPet..Fabrica values ('F-DYD-H-01', 'F�brica DyD Host - Canales Individuos', 'DESA', 'MEDIO', '11', '25-01', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-02', 'F�brica DyD Host - Front-End Altamira', 'DESA', 'MEDIO', '11', '25-02', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-03', 'F�brica DyD Host - Canales Empresas', 'DESA', 'MEDIO', '11', '25-03', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-04', 'F�brica DyD Host - Canales Telef�nicos', 'DESA', 'MEDIO', '11', '25-09', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-05', 'F�brica DyD Host - Arq. Aplicativa y T�c. HOST', 'DESA', 'MEDIO', '11', '11-19', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-06', 'F�brica DyD Host - Arq. Aplicativa y T�c. no-HOST', 'DESA', 'MEDIO', '11', '11-20', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-07', 'F�brica DyD Host - Contab., Imp. y R. Informativo', 'DESA', 'MEDIO', '12', '24-01', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-08', 'F�brica DyD Host - PI, Hermes y Sist.Corp.de Inf.', 'DESA', 'MEDIO', '12', '24-02', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-09', 'F�brica DyD Host - Business Intelligence', 'DESA', 'MEDIO', '12', '12-08', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-10', 'F�brica DyD Host - Clientes, Agenda-Cartera Comer', 'DESA', 'MEDIO', '12', '13-09', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-11', 'F�brica DyD Host - Efectos, Remesas y Cheques', 'DESA', 'MEDIO', '13', '13-01', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-12', 'F�brica DyD Host - Paquetes, Cuentas, MEP', 'DESA', 'MEDIO', '13', '13-04', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-13', 'F�brica DyD Host - Banelco y Domiciliaciones', 'DESA', 'MEDIO', '13', '13-08', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-14', 'F�brica DyD Host - Cart. Pasivas, Recaud. y Pagos', 'DESA', 'MEDIO', '13', '13-10', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-15', 'F�brica DyD Host - Cust.,Mesa Dinero y WallStreet', 'DESA', 'MEDIO', '13', '13-11', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-16', 'F�brica DyD Host - PaP, Conv., Imp. y Pzo.Fij.', 'DESA', 'MEDIO', '13', '13-12', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-17', 'F�brica DyD Host - RRHH y Legales', 'DESA', 'MEDIO', '24', '25-07', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-18', 'F�brica DyD Host - Sistemas Centrales', 'DESA', 'MEDIO', '24', '24-08', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-19', 'F�brica DyD Host - RiesEmp,RecJud,Mora,Cont,Gt�as', 'DESA', 'MEDIO', '24', '24-09', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-20', 'F�brica DyD Host - HRies,Wkf,Vtas,Post-vta,EvMas', 'DESA', 'MEDIO', '24', '24-10', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-21', 'F�brica DyD Host - Sistemas Microinform�ticos', 'DESA', 'MEDIO', '24', '11-21', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-22', 'F�brica DyD Host - Seg.de Inciden. y C.de Mandos', 'DESA', 'MEDIO', '147', '13-06', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-23', 'F�brica DyD Host - Metodolog�a, CGM y Ent.Previos', 'DESA', 'MEDIO', '147', '24-11', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-24', 'F�brica DyD Host - Comercio Exterior y Extranjero', 'DESA', 'MEDIO', '148', '13-03', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-25', 'F�brica DyD Host - Empresas Vinculadas', 'DESA', 'MEDIO', '148', '12-06', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-26', 'F�brica DyD Host - Tarjetas Productos y Cobranzas', 'DESA', 'MEDIO', '148', '24-06', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-27', 'F�brica DyD Host - Productos Activos', 'DESA', 'MEDIO', '148', '12-07', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-28', 'F�brica DyD Host - Tarjetas Consumos y Comercios', 'DESA', 'MEDIO', '148', '24-12', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-29', 'F�brica DyD Host - Swift Regulatorios y Canales', 'DESA', 'MEDIO', '148', '24-13', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-30', 'F�brica DyD Host - Retiro, Internet y Cross', 'DESA', 'MEDIO', '251', '251-01', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-H-31', 'F�brica DyD Host - Seguros', 'DESA', 'MEDIO', '251', '251-02', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-01', 'F�brica DyD Java - Canales Individuos', 'DESA', 'MEDIO', '11', '25-01', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-02', 'F�brica DyD Java - Front-End Altamira', 'DESA', 'MEDIO', '11', '25-02', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-03', 'F�brica DyD Java - Canales Empresas', 'DESA', 'MEDIO', '11', '25-03', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-04', 'F�brica DyD Java - Canales Telef�nicos', 'DESA', 'MEDIO', '11', '25-09', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-05', 'F�brica DyD Java - Arq. Aplicativa y T�c. HOST', 'DESA', 'MEDIO', '11', '11-19', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-06', 'F�brica DyD Java - Arq. Aplicativa y T�c. no-HOST', 'DESA', 'MEDIO', '11', '11-20', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-07', 'F�brica DyD Java - Contab., Imp. y R. Informativo', 'DESA', 'MEDIO', '12', '24-01', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-08', 'F�brica DyD Java - PI, Hermes y Sist.Corp.de Inf.', 'DESA', 'MEDIO', '12', '24-02', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-09', 'F�brica DyD Java - Business Intelligence', 'DESA', 'MEDIO', '12', '12-08', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-10', 'F�brica DyD Java - Clientes, Agenda-Cartera Comer', 'DESA', 'MEDIO', '12', '13-09', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-11', 'F�brica DyD Java - Efectos, Remesas y Cheques', 'DESA', 'MEDIO', '13', '13-01', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-12', 'F�brica DyD Java - Paquetes, Cuentas, MEP', 'DESA', 'MEDIO', '13', '13-04', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-13', 'F�brica DyD Java - Banelco y Domiciliaciones', 'DESA', 'MEDIO', '13', '13-08', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-14', 'F�brica DyD Java - Cart. Pasivas, Recaud. y Pagos', 'DESA', 'MEDIO', '13', '13-10', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-15', 'F�brica DyD Java - Cust.,Mesa Dinero y WallStreet', 'DESA', 'MEDIO', '13', '13-11', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-16', 'F�brica DyD Java - PaP, Conv., Imp. y Pzo.Fij.', 'DESA', 'MEDIO', '13', '13-12', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-17', 'F�brica DyD Java - RRHH y Legales', 'DESA', 'MEDIO', '24', '25-07', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-18', 'F�brica DyD Java - Sistemas Centrales', 'DESA', 'MEDIO', '24', '24-08', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-19', 'F�brica DyD Java - RiesEmp,RecJud,Mora,Cont,Gt�as', 'DESA', 'MEDIO', '24', '24-09', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-20', 'F�brica DyD Java - HRies,Wkf,Vtas,Post-vta,EvMas', 'DESA', 'MEDIO', '24', '24-10', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-21', 'F�brica DyD Java - Sistemas Microinform�ticos', 'DESA', 'MEDIO', '24', '11-21', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-22', 'F�brica DyD Java - Seg.de Inciden. y C.de Mandos', 'DESA', 'MEDIO', '147', '13-06', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-23', 'F�brica DyD Java - Metodolog�a, CGM y Ent.Previos', 'DESA', 'MEDIO', '147', '24-11', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-24', 'F�brica DyD Java - Comercio Exterior y Extranjero', 'DESA', 'MEDIO', '148', '13-03', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-25', 'F�brica DyD Java - Empresas Vinculadas', 'DESA', 'MEDIO', '148', '12-06', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-26', 'F�brica DyD Java - Tarjetas Productos y Cobranzas', 'DESA', 'MEDIO', '148', '24-06', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-27', 'F�brica DyD Java - Productos Activos', 'DESA', 'MEDIO', '148', '12-07', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-28', 'F�brica DyD Java - Tarjetas Consumos y Comercios', 'DESA', 'MEDIO', '148', '24-12', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-29', 'F�brica DyD Java - Swift Regulatorios y Canales', 'DESA', 'MEDIO', '148', '24-13', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-30', 'F�brica DyD Java - Retiro, Internet y Cross', 'DESA', 'MEDIO', '251', '251-01', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-J-31', 'F�brica DyD Java - Seguros', 'DESA', 'MEDIO', '251', '251-02', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-01', 'Paquete de Horas - Canales Individuos', 'DESA', 'MEDIO', '11', '25-01', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-02', 'Paquete de Horas - Front-End Altamira', 'DESA', 'MEDIO', '11', '25-02', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-03', 'Paquete de Horas - Canales Empresas', 'DESA', 'MEDIO', '11', '25-03', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-04', 'Paquete de Horas - Canales Telef�nicos', 'DESA', 'MEDIO', '11', '25-09', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-05', 'Paquete de Horas - Arq. Aplicativa y T�c. HOST', 'DESA', 'MEDIO', '11', '11-19', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-06', 'Paquete de Horas - Arq. Aplicativa y T�c. no-HOST', 'DESA', 'MEDIO', '11', '11-20', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-07', 'Paquete de Horas - Contab., Imp. y R. Informativo', 'DESA', 'MEDIO', '12', '24-01', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-08', 'Paquete de Horas - PI, Hermes y Sist.Corp.de Inf.', 'DESA', 'MEDIO', '12', '24-02', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-09', 'Paquete de Horas - Business Intelligence', 'DESA', 'MEDIO', '12', '12-08', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-10', 'Paquete de Horas - Clientes, Agenda-Cartera Comer', 'DESA', 'MEDIO', '12', '13-09', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-11', 'Paquete de Horas - Efectos, Remesas y Cheques', 'DESA', 'MEDIO', '13', '13-01', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-12', 'Paquete de Horas - Paquetes, Cuentas, MEP', 'DESA', 'MEDIO', '13', '13-04', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-13', 'Paquete de Horas - Banelco y Domiciliaciones', 'DESA', 'MEDIO', '13', '13-08', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-14', 'Paquete de Horas - Cart. Pasivas, Recaud. y Pagos', 'DESA', 'MEDIO', '13', '13-10', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-15', 'Paquete de Horas - Cust.,Mesa Dinero y WallStreet', 'DESA', 'MEDIO', '13', '13-11', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-16', 'Paquete de Horas - PaP, Conv., Imp. y Pzo.Fij.', 'DESA', 'MEDIO', '13', '13-12', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-17', 'Paquete de Horas - RRHH y Legales', 'DESA', 'MEDIO', '24', '25-07', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-18', 'Paquete de Horas - Sistemas Centrales', 'DESA', 'MEDIO', '24', '24-08', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-19', 'Paquete de Horas - RiesEmp,RecJud,Mora,Cont,Gt�as', 'DESA', 'MEDIO', '24', '24-09', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-20', 'Paquete de Horas - HRies,Wkf,Vtas,Post-vta,EvMas', 'DESA', 'MEDIO', '24', '24-10', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-21', 'Paquete de Horas - Sistemas Microinform�ticos', 'DESA', 'MEDIO', '24', '11-21', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-22', 'Paquete de Horas - Seg.de Inciden. y C.de Mandos', 'DESA', 'MEDIO', '147', '13-06', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-23', 'Paquete de Horas - Metodolog�a, CGM y Ent.Previos', 'DESA', 'MEDIO', '147', '24-11', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-24', 'Paquete de Horas - Comercio Exterior y Extranjero', 'DESA', 'MEDIO', '148', '13-03', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-25', 'Paquete de Horas - Empresas Vinculadas', 'DESA', 'MEDIO', '148', '12-06', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-26', 'Paquete de Horas - Tarjetas Productos y Cobranzas', 'DESA', 'MEDIO', '148', '24-06', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-27', 'Paquete de Horas - Productos Activos', 'DESA', 'MEDIO', '148', '12-07', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-28', 'Paquete de Horas - Tarjetas Consumos y Comercios', 'DESA', 'MEDIO', '148', '24-12', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-29', 'Paquete de Horas - Swift Regulatorios y Canales', 'DESA', 'MEDIO', '148', '24-13', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-30', 'Paquete de Horas - Retiro, Internet y Cross', 'DESA', 'MEDIO', '251', '251-01', 'A')
GO
insert into GesPet..Fabrica values ('F-DYD-P-31', 'Paquete de Horas - Seguros', 'DESA', 'MEDIO', '251', '251-02', 'A')
GO
