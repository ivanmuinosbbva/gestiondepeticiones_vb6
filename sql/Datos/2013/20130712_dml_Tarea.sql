-- Agregado de nuevas tareas
insert into GesPet..Tarea values ('0026001','Licencia por ex�men','S')
go
insert into GesPet..Tarea values ('0026002','Licencia por maternidad','S')
go
insert into GesPet..Tarea values ('0026003','Licencia por excedencia maternidad','S')
go
insert into GesPet..Tarea values ('0026004','Licencia por vacaciones','S')
go
insert into GesPet..Tarea values ('0026005','Licencia por enfermedad','S')
go
insert into GesPet..Tarea values ('0026099','Otras licencias','S')
go

-- Se inhabilita la tarea gen�rica "Licencias"
update GesPet..Tarea
set flg_habil = 'N'
where cod_tarea = '00260'
go
