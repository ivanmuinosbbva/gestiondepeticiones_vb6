-- Versionamiento de implantación de CGM
print 'Versionamiento de implantación de CGM...'
go

update GesPet..Varios
set
	var_texto = 'v6.4.0',
	var_fecha = getdate()
where var_codigo = 'CGMVER'
go


update GesPet..Varios
set
	var_texto = 'v6.4.0',
	var_fecha = getdate()
where var_codigo = 'CFGVER'
go

print 'Listo.'
go
