-- Se elimina la tarea gen�rica "Licencias" y se reemplaza por las nuevas tareas
delete from GesPet..TareaSector where cod_sector ='100' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='103' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='11' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='12' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='13' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='2' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='20' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='21' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='22' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='23' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='24' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='25' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='26' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='27' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='28' and cod_tarea = '00260'
GO
delete from GesPet..TareaSector where cod_sector ='3' and cod_tarea = '00260'
GO

-- Agrego las nuevas tareas a los sectores que tenian antes la tarea "Licencias"
insert into GesPet..TareaSector select cod_tarea,'100' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'103' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'11' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'12' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'13' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'2' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'20' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'21' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'22' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'23' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'24' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'25' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'26' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'27' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'28' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
insert into GesPet..TareaSector select cod_tarea,'3' from GesPet..Tarea where cod_tarea like '0026%' and flg_habil = 'S'
GO
