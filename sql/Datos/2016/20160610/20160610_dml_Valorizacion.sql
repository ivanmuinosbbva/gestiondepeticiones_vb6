-- Cambio de los pesos en las opciones de valoraci�n

-- Impacto en resultados
sp_UpdateValoracion 11,'Hasta $500M x a�o',0.5,2,'S',3
go
sp_UpdateValoracion 13,'Hasta $6MM x a�o',2,4,'S',3
go

-- Impacto en productos m�s relevantes
sp_UpdateValoracion 18,'Seguros',1,3,'S',4
go
sp_UpdateValoracion 19,'Pr�stamos',2,4,'S',4
go
sp_UpdateValoracion 20,'Cuentas y paquetes',3,5,'S',4
go

-- Transaccionalidad
sp_UpdateValoracion 23,'Hasta 1M operaciones x mes',0.1,2,'S',5
go
sp_UpdateValoracion 24,'Hasta 50M operaciones x mes',0.5,3,'S',5
go
sp_UpdateValoracion 25,'Hasta 250M operaciones x mes',2,4,'S',5
go
