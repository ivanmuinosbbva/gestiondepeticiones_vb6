-- Cambio de pesos en los drivers
-- IMPACTO
sp_UpdateDrivers 1, 'Voz del cliente (IRENE)','SOLI','S',1,9,'Si el pedido mejora un punto evaluado en IRENE, indicar el valor obtenido en la �ltima encuesta.'
go
sp_UpdateDrivers 2,'Voz del cliente (Representatividad)','BPE','S',1,4,'Indicar el grado de representatividad por los Comit�s, Planes Estrat�gicos, Direcciones sponsor, Off Site que siguen el pedido.' 
go
sp_UpdateDrivers 3,'Impacto en Resultados','SOLI','S',1,8,'Indicar si el desarrollo generar� un incremento en ingresos netos o reducci�n de costos en $ mensuales.'  
go
sp_UpdateDrivers 4,'Impacto en los productos m�s relevantes','BPE','S',1,7,'Indicar si el desarrollo afecta alguno de los siguientes producto o servicios'  
go
sp_UpdateDrivers 5,'Transaccionalidad: n�mero de ocasiones en las cuales dicha transacci�n se ejecuta','SOLI','S',1,8,'Indicar el uso esperado del proceso afectado, cantidad de operaciones mensuales que afecta.'  
go
sp_UpdateDrivers 6,'Productividad','SOLI','S',1,7,'Indicar si libera tiempo de funcionarios.'  
go
sp_UpdateDrivers 7,'Control / Regulatorio','SOLI','S',1,5,'Indicar si el pedido se genera a partir de auditor�as, regulaciones o puntos de control interno.'  
go
sp_UpdateDrivers 8,'Grado de innovaci�n','SOLI','S',1,4,'Indicar si representa una innovaci�n para construir el Banco del futuro.'  
go
sp_UpdateDrivers 9,'Oportunidad a corto plazo','SOLI','S',1,1,'Indicar si la oportunidad del negocio o regulaci�n requiere una fecha espec�fica de implementaci�n en el corto plazo.'  
go
sp_UpdateDrivers 14,'Impacto en Segmentos','SOLI','S',1,7,'Indicar el segmento sobre el cual el pedido tiene mayor impacto.'  
go
sp_UpdateDrivers 15,'Empleados BBVA beneficiados','SOLI','S',1,6,'Indicar la cantidad de empleados afectados por el pedido.'  
go
-- FACILIDAD
sp_UpdateDrivers 10,'N�mero de �reas involucradas','BPE','S',2,1,'Indicar la cantidad de gerencias participantes en que colaboran en el proyecto.'  
go
sp_UpdateDrivers 11,'Costo estimado de desarrollo','BPE','S',2,5,'Indicar la estimaci�n de hs. de desarrollo.'  
go
sp_UpdateDrivers 12,'Tiempo estimado de ejecuci�n','BPE','S',2,2,'Indicar la estimaci�n de tiempo para la implementaci�n.'  
go
sp_UpdateDrivers 13,'Requiere aprovisionamiento','BPE','S',2,4,'Indicar el grado de participaci�n de Infraestructura Tecnolog�a'  
go