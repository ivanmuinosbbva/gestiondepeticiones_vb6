insert into InterfazTera values ('GesPet', 'TD_Agrup', 'Agrup', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_AgrupPetic', 'AgrupPetic', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Aplicativo', 'Aplicativo', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Clase', 'Clase', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Direccion', 'Direccion', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Documento', 'Documento', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Drivers', 'Drivers', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Empresa', 'Empresa', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Estados', 'Estados', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Fabrica', 'Fabrica', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Feriado', 'Feriado', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Gerencia', 'Gerencia', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Grupo', 'Grupo', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_GrupoAplicativo', 'GrupoAplicativo', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_HitoClase', 'HitoClase', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_HorasTrabajadas', 'HorasTrabajadas', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Importancia', 'Importancia', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Indicador', 'Indicador', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_IndicadorKPI', 'IndicadorKPI', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_IndicadorKPIUniMed', 'IndicadorKPIUniMed', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Justificativos', 'Justificativos', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Orientacion', 'Orientacion', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Perfil', 'Perfil', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_PeriodoEstado', 'PeriodoEstado', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Peticion', 'Peticion', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_PeticionAdjunto', 'PeticionAdjunto', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_PeticionBeneficios', 'PeticionBeneficios', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_PeticionConf', 'PeticionConf', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_PeticionGrupo', 'PeticionGrupo', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_PeticionKPI', 'PeticionKPI', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_PeticionRecurso', 'PeticionRecurso', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_PeticionSector', 'PeticionSector', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Prioridad', 'Prioridad', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_ProyectoCategoria', 'ProyectoCategoria', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_ProyectoClase', 'ProyectoClase', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_ProyectoIDM', 'ProyectoIDM', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_ProyectoIDMAdjuntos', 'ProyectoIDMAdjuntos', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_ProyectoIDMAlertas', 'ProyectoIDMAlertas', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_ProyectoIDMGrupos', 'ProyectoIDMGrupos', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_ProyectoIDMHitos', 'ProyectoIDMHitos', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_ProyectoIDMHitosEstados', 'ProyectoIDMHitosEstados', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_ProyectoIDMHoras', 'ProyectoIDMHoras', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_ProyectoIDMResponsables', 'ProyectoIDMResponsables', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Recurso', 'Recurso', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_RecursoPerfil', 'RecursoPerfil', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Sector', 'Sector', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Situaciones', 'Situaciones', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Tarea', 'Tarea', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_TareaSector', 'TareaSector', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_TipoFabrica', 'TipoFabrica', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_TipoPet', 'TipoPet', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_UnidadMedida', 'UnidadMedida', 'S')
GO
insert into InterfazTera values ('GesPet', 'TD_Valoracion', 'Valoracion', 'S')
GO
