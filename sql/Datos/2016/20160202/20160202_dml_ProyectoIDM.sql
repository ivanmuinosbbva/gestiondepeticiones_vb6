-- Banco
update ProyectoIDM
set codigoEmpresa = 1
where empresa = 'B'
go

-- BBVA Consolidar
update ProyectoIDM
set codigoEmpresa = 2
where empresa = 'C'
go

-- PSA
update ProyectoIDM
set codigoEmpresa = 3
where empresa = 'D'
go

-- RCF
update ProyectoIDM
set codigoEmpresa = 4
where empresa = 'E'
go

-- PSA / RCF
update ProyectoIDM
set codigoEmpresa = 20
where empresa = 'F'
go