INSERT INTO Empresa 
VALUES (1, 'BBVA Banco','S', 'BCO')
GO

INSERT INTO Empresa 
VALUES (2, 'BBVA Consolidar','S', 'CONSO')
GO

INSERT INTO Empresa 
VALUES (3, 'PSA','S', 'PSA')
GO

INSERT INTO Empresa 
VALUES (4, 'RCF','S', 'RCF')
GO

INSERT INTO Empresa 
VALUES (5, 'VWFS - Volkswagen Financial Services','S', 'VWFS')
GO

INSERT INTO Empresa 
VALUES (20, 'PSA y RCF','S', 'PSA/RCF')
GO

INSERT INTO Empresa 
VALUES (21, 'PSA, RCF y VWFS','S', 'PSA/RCF/VWFS')
GO
