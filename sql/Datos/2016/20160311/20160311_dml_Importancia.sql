UPDATE Importancia
SET nom_importancia = 'MA: Muy Alta (DIRE)'
WHERE cod_importancia = '10'
go

UPDATE Importancia
SET nom_importancia = 'A : Alta     (GERE)'
WHERE cod_importancia = '20'
go

UPDATE Importancia
SET nom_importancia = 'M : Media    (SECT)'
WHERE cod_importancia = '30'
go

UPDATE Importancia
SET nom_importancia = 'B : Baja     (GRUP)'
WHERE cod_importancia = '40'
go
