-- 1. IMPACTO

-- Voz del cliente (IRENE)
INSERT INTO Valoracion VALUES (1, 'No', 0, 1, 'S', 1)
go
INSERT INTO Valoracion VALUES (2, '76 a 100', 1, 2, 'S', 1)
go
INSERT INTO Valoracion VALUES (3, '51 a 75', 3, 3, 'S', 1)
go
INSERT INTO Valoracion VALUES (4, '26 a 50', 4, 4, 'S', 1)
go
INSERT INTO Valoracion VALUES (5, '0 a 25', 5, 5, 'S', 1)
go

-- Voz del cliente (Representatividad)
INSERT INTO Valoracion VALUES (6, 'No', 0, 1, 'S', 2)
go
INSERT INTO Valoracion VALUES (7, '1', 1, 2, 'S', 2)
go
INSERT INTO Valoracion VALUES (8, '2 o 3', 3, 3, 'S', 2)
go
INSERT INTO Valoracion VALUES (9, '+ de 3', 5, 4, 'S', 2)
go

-- Impacto en Resultados
INSERT INTO Valoracion VALUES (10, '$0 x a�o', 0, 1, 'S', 3)
go
INSERT INTO Valoracion VALUES (11, 'Hasta $500M x a�o', 1, 2, 'S', 3)
go
INSERT INTO Valoracion VALUES (12, 'Hasta $3MM x a�o', 2, 3, 'S', 3)
go
INSERT INTO Valoracion VALUES (13, 'Hasta $6MM x a�o', 3, 4, 'S', 3)
go
INSERT INTO Valoracion VALUES (14, 'Hasta $12MM x a�o', 4, 5, 'S', 3)
go
INSERT INTO Valoracion VALUES (15, 'M�s de $12MM x a�o', 5, 6, 'S', 3)
go

-- Impacto en los productos m�s relevantes
INSERT INTO Valoracion VALUES (16, 'Otros', 0, 1, 'S', 4)
go
INSERT INTO Valoracion VALUES (17, 'Comercio Exterior', 1, 2, 'S', 4)
go
INSERT INTO Valoracion VALUES (18, 'Seguros', 2, 3, 'S', 4)
go
INSERT INTO Valoracion VALUES (19, 'Pr�stamos', 3, 4, 'S', 4)
go
INSERT INTO Valoracion VALUES (20, 'Cuentas y paquetes', 4, 5, 'S', 4)
go
INSERT INTO Valoracion VALUES (21, 'Tarjetas de Cr�dito', 5, 6, 'S', 4)
go

-- Transaccionalidad: n�mero de ocasiones en las cuales dicha transacci�n se ejecuta
INSERT INTO Valoracion VALUES (22, '0 operaciones x mes', 0, 1, 'S', 5)
go
INSERT INTO Valoracion VALUES (23, 'Hasta 1M operaciones x mes', 1, 2, 'S', 5)
go
INSERT INTO Valoracion VALUES (24, 'Hasta 50M operaciones x mes', 2, 3, 'S', 5)
go
INSERT INTO Valoracion VALUES (25, 'Hasta 250M operaciones x mes', 3, 4, 'S', 5)
go
INSERT INTO Valoracion VALUES (26, 'Hasta 500M operaciones x mes', 4, 5, 'S', 5)
go
INSERT INTO Valoracion VALUES (27, 'M�s de 500M operaciones x mes', 5, 6, 'S', 5)
go

-- Productividad:
INSERT INTO Valoracion VALUES (28, 'No libera tiempo o reduce la carga operativa de forma significativa', 0, 1, 'S', 6)
go
INSERT INTO Valoracion VALUES (29, 'Libera tiempo, pero no se traduce en un alto ahorro', 1, 2, 'S', 6)
go
INSERT INTO Valoracion VALUES (30, 'Libera tiempo en AACC para enfocarlo en otros esfuerzos', 2, 3, 'S', 6)
go
INSERT INTO Valoracion VALUES (31, 'Libera tiempo operativo en sucursales', 3, 4, 'S', 6)
go
INSERT INTO Valoracion VALUES (32, 'Libera tiempo operativo en Caja de sucursales', 4, 5, 'S', 6)
go
INSERT INTO Valoracion VALUES (33, 'Libera tiempo del oficial para enfocarlo en esfuerzos comerciales', 5, 6, 'S', 6)
go

-- Control / Regulatorio
INSERT INTO Valoracion VALUES (34, 'NO', 0, 1, 'S', 7)
go
INSERT INTO Valoracion VALUES (35, 'Control Interno', 1, 2, 'S', 7)
go
INSERT INTO Valoracion VALUES (36, 'Auditor�a Interna', 2, 3, 'S', 7)
go
INSERT INTO Valoracion VALUES (37, 'Auditor�a Externa', 3, 4, 'S', 7)
go
INSERT INTO Valoracion VALUES (38, 'Auditor�a BCRA', 4, 5, 'S', 7)
go
INSERT INTO Valoracion VALUES (39, 'Regulatorio', 5, 6, 'S', 7)
go

-- Grado de innovaci�n
INSERT INTO Valoracion VALUES (40, 'No tiene', 0, 1, 'S', 8)
go
INSERT INTO Valoracion VALUES (41, 'Agrega atributos a un producto existente para alcanzar la competencia', 3, 2, 'S', 8)
go
INSERT INTO Valoracion VALUES (42, 'Agrega atributos novedosos en el mercado para un producto existente', 4, 3, 'S', 8)
go
INSERT INTO Valoracion VALUES (43, 'Genera un nuevo producto que no existe en el mercado', 5, 4, 'S', 8)
go

-- Oportunidad de corto plazo
INSERT INTO Valoracion VALUES (44, 'No es una oportunidad del negocio o regulaci�n que requiera una fecha espec�fica de implementaci�n en el corto plazo', 0, 1, 'S', 9)
go
INSERT INTO Valoracion VALUES (45, 'Es una oportunidad del negocio o regulaci�n que requiere una fecha espec�fica de implementaci�n en el corto plazo', 5, 2, 'S', 9)
go


-- 2. FACILIDAD

-- N�mero de �reas involucradas
INSERT INTO Valoracion VALUES (46, 'Implica la colaboraci�n de 1 area (fragmentaci�n baja)', 5, 1, 'S', 10)
go
INSERT INTO Valoracion VALUES (47, 'Implica la colaboraci�n de 2-3 �reas (fragmentaci�n media)', 3, 2, 'S', 10)
go
INSERT INTO Valoracion VALUES (48, 'Implica la colaboraci�n de >=4 �reas (fragmentaci�n alta)', 0, 3, 'S', 10)
go

-- Costo estimado de desarrollo
INSERT INTO Valoracion VALUES (49, 'Bajo costo de desarrollo (hasta 500 hs.)', 5, 1, 'S', 11)
go
INSERT INTO Valoracion VALUES (50, 'Moderado costo de desarrollo (hasta 2.000 hs.)', 3, 2, 'S', 11)
go
INSERT INTO Valoracion VALUES (51, 'Alto costo de desarrollo (hasta 5.000 hs.)', 2, 3, 'S', 11)
go
INSERT INTO Valoracion VALUES (52, 'Muy alto costo de desarrollo (m�s de 5.000 hs.)', 0, 4, 'S', 11)
go

-- Tiempo estimado de ejecuci�n
INSERT INTO Valoracion VALUES (53, 'Desarrollo inform�tico peque�o (<6m)', 5, 1, 'S', 12)
go
INSERT INTO Valoracion VALUES (54, 'Desarrollo inform�tico medio (6m-12m)', 3, 2, 'S', 12)
go
INSERT INTO Valoracion VALUES (55, 'Gran desarrollo inform�tico (>12m)', 0, 3, 'S', 12)
go

-- Requiere aprovisionamiento
INSERT INTO Valoracion VALUES (56, 'Nula', 5, 1, 'S', 13)
go
INSERT INTO Valoracion VALUES (57, 'Baja', 3, 2, 'S', 13)
go
INSERT INTO Valoracion VALUES (58, 'Media', 2, 3, 'S', 13)
go
INSERT INTO Valoracion VALUES (59, 'Alta', 0, 4, 'S', 13)
go
