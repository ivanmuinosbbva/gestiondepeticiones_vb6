INSERT INTO Drivers  
VALUES (1, 'Voz del cliente (IRENE)', 'Si el pedido mejora un punto evaluado en IRENE, indicar el valor obtenido en la �ltima encuesta.', 1, 'SOLI', 'S', 1)
go
INSERT INTO Drivers  
VALUES (2, 'Voz del cliente (Representatividad)','Indicar el grado de representatividad por los Comit�s, Planes Estrat�gicos, Direcciones sponsor, Off Site que siguen el pedido.', 1, 'BPE', 'S', 1)
go
INSERT INTO Drivers  
VALUES (3, 'Impacto en Resultados','Indicar si el desarrollo generar� un incremento en ingresos netos o reducci�n de costos en $ mensuales.',1, 'SOLI', 'S', 1)
go
INSERT INTO Drivers  
VALUES (4, 'Impacto en los productos m�s relevantes','Indicar si el desarrollo afecta alguno de los siguientes producto o servicios', 1, 'BPE', 'S', 1)
go
INSERT INTO Drivers  
VALUES (5, 'Transaccionalidad: n�mero de ocasiones en las cuales dicha transacci�n se ejecuta','Indicar el uso esperado del proceso afectado, cantidad de operaciones mensuales que afecta.', 1, 'SOLI', 'S', 1)
go
INSERT INTO Drivers  
VALUES (6, 'Productividad','Indicar si libera tiempo de funcionarios.', 1, 'SOLI', 'S', 1)
go
INSERT INTO Drivers  
VALUES (7, 'Control / Regulatorio', 'Indicar si el pedido se genera a partir de auditor�as, regulaciones o puntos de control interno.',1, 'SOLI', 'S', 1)
go
INSERT INTO Drivers  
VALUES (8, 'Grado de innovaci�n', 'Indicar si representa una innovaci�n para construir el Banco del futuro.',1, 'SOLI', 'S', 1)
go
INSERT INTO Drivers  
VALUES (9, 'Oportunidad a corto plazo','Indicar si la oportunidad del negocio o regulaci�n requiere una fecha espec�fica de implementaci�n en el corto plazo.',1, 'SOLI', 'S', 1)
go

INSERT INTO Drivers  
VALUES (10, 'N�mero de �reas involucradas','Indicar la cantidad de gerencias participantes en que colaboran en el proyecto.',1, 'BPE', 'S', 2)
go
INSERT INTO Drivers  
VALUES (11, 'Costo estimado de desarrollo','Indicar la estimaci�n de hs. de desarrollo.',1, 'BPE', 'S', 2)
go
INSERT INTO Drivers  
VALUES (12, 'Tiempo estimado de ejecuci�n','Indicar la estimaci�n de tiempo para la implementaci�n.',1, 'BPE', 'S', 2)
go
INSERT INTO Drivers  
VALUES (13, 'Requiere aprovisionamiento','Indicar el grado de participaci�n de Infraestructura Tecnolog�a',1, 'BPE', 'S', 2)
go
