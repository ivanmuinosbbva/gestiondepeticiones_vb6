/* 
	Configuraci�n de sectores para que cada uno de ellos, seg�n su direcci�n,
	quede apuntando al referente de BPE correspondiente.
	Habr�a cuatro grupos:
*/

-- Claudia Sapiz
print 'Actualizando sectores para Claudia Sapiz...'
go

UPDATE Sector
SET 
	cod_perfil = 'GBPE',
	cod_bpar = 'A39996'
WHERE  
	cod_sector in (
	select s.cod_sector
	from 
		Sector s inner join
		Gerencia g on (s.cod_gerencia = g.cod_gerencia) inner join
		Direccion d on (d.cod_direccion = g.cod_direccion)
	where d.cod_direccion in ('BMAYL','BMINO'))
go

-- Leonardo Rojas
print 'Actualizando sectores para Leonardo Rojas...'
go

UPDATE Sector
SET 
	cod_perfil = 'GBPE',
	cod_bpar = 'A115629'
WHERE  
	cod_sector in (
	select s.cod_sector
	from 
		Sector s inner join
		Gerencia g on (s.cod_gerencia = g.cod_gerencia) inner join
		Direccion d on (d.cod_direccion = g.cod_direccion)
	where d.cod_direccion in ('TRF','TRYD'))
go

-- Jorge Grandi
print 'Actualizando sectores para Jorge Grandi...'
go

print 'a. Actualizando sectores para BPE...'
go
-- a) La parte que va para BPE
UPDATE Sector
SET 
	cod_perfil = 'GBPE',
	cod_bpar = 'A91970'
WHERE  
	cod_sector in (
	select s.cod_sector
	from 
		Sector s inner join
		Gerencia g on (s.cod_gerencia = g.cod_gerencia) inner join
		Direccion d on (d.cod_direccion = g.cod_direccion)
	where d.cod_direccion in ('FINAN','RGOS'))
go

-- b) La parte que queda como est� hoy (gestionado por Sistemas)
-- La parte de Vinculadas que va para Rub�n Ricardo L�pez
print 'b. Actualizando sectores para Rub�n Ricardo L�pez...'
go

UPDATE Sector
SET 
	cod_perfil = 'BPAR',
	cod_bpar = 'A20895'
WHERE  
	cod_sector in (
	select s.cod_sector
	from 
		Sector s inner join
		Gerencia g on (s.cod_gerencia = g.cod_gerencia) inner join
		Direccion d on (d.cod_direccion = g.cod_direccion)
	where d.cod_direccion ='VINC')
go

-- La parte de Seguros que va para Alejandro Aboudara
print 'c. Actualizando sectores para Alejandro Aboudara...'
go

UPDATE Sector
SET 
	cod_perfil = 'BPAR',
	cod_bpar = 'A91372'
WHERE  
	cod_sector in (
	select s.cod_sector
	from 
		Sector s inner join
		Gerencia g on (s.cod_gerencia = g.cod_gerencia) inner join
		Direccion d on (d.cod_direccion = g.cod_direccion)
	where d.cod_direccion ='TECYCONT')
go

-- Sergio Arenas
print 'Actualizando sectores para Sergio Arenas...'
go

UPDATE Sector
SET 
	cod_perfil = 'GBPE',
	cod_bpar = 'A119430'
WHERE  
	cod_sector in (
	select s.cod_sector
	from 
		Sector s inner join
		Gerencia g on (s.cod_gerencia = g.cod_gerencia) inner join
		Direccion d on (d.cod_direccion = g.cod_direccion)
	where d.cod_direccion in ('AUDIT','CNORM','MEDIO','RELINST','RRHH','SERVJUR'))
go
