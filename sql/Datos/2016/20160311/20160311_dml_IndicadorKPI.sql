INSERT INTO IndicadorKPI  
VALUES (1, 'Horas hombre', 'S','S', 'HRSANU')
go

INSERT INTO IndicadorKPI  
VALUES (2, 'Ventas', 'S','S', 'UNIMEN')
go

INSERT INTO IndicadorKPI  
VALUES (3, 'Comisiones', 'S','S', 'PESMEN')
go

INSERT INTO IndicadorKPI  
VALUES (4, 'Operaciones / Transacciones', 'S','S', 'UNIMEN')
go

INSERT INTO IndicadorKPI  
VALUES (5, 'Tiempo de proceso', 'S','S', 'MIN')
go

INSERT INTO IndicadorKPI  
VALUES (6, 'Clientes', 'S','S', 'UNIMEN')
go

INSERT INTO IndicadorKPI  
VALUES (7, 'Disponibilidad', 'S','S', 'HS')
go

INSERT INTO IndicadorKPI  
VALUES (8, 'Reclamos / Consultas', 'S','S', 'UNIMEN')
go