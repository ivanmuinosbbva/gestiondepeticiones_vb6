print 'Cantidad total de acciones a actualizar: 70'
go

sp_UpdateAcciones 'ADJADD', 'Nuevo documento', 'S', 'ACC', 1, 'Adjuntar un documento', 160, 'S', 'S'
GO
sp_UpdateAcciones 'ADJDEL', 'Desadjuntar documento', 'S', 'ACC', 1, 'Desadjuntar un documento', 170, 'S', 'S'
GO
sp_UpdateAcciones 'ADJUPD', 'Modificaci�n documento', 'S', 'EVT', 0, 'Modificaci�n documento', 0, 'N', 'N'
GO
sp_UpdateAcciones 'AGCHGEST', 'Cambio estado de grupo', 'S', 'EVT', 0, 'Cambio estado de grupo', 0, 'N', 'N'
GO
sp_UpdateAcciones 'CONVERT', 'Conversi�n Datos', 'S', 'EVT', 0, 'Conversi�n de datos', 0, 'N', 'N'
GO
sp_UpdateAcciones 'GCHGEST', 'Cambio estado de grupo', 'S', 'ACC', 3, 'Cambiar el estado del grupo', 4, 'S', 'S'
GO
sp_UpdateAcciones 'GDEL000', 'Eliminar Grupo', 'S', 'ACC', 3, 'Eliminar f�sicamente un grupo', 3, 'S', 'S'
GO
sp_UpdateAcciones 'GINTERV', 'Intervencion Grupo', 'S', 'EVT', 3, 'Intervencion en grupo', 0, 'N', 'N'
GO
sp_UpdateAcciones 'GMOD000', 'Modificar Grupo', 'S', 'ACC', 3, 'Modificar un grupo', 2, 'S', 'S'
GO
sp_UpdateAcciones 'GNEW000', 'Nuevo grupo', 'S', 'ACC', 3, 'Dar de alta un grupo', 1, 'S', 'S'
GO
sp_UpdateAcciones 'GREASIG', 'Reasignaci�n de grupo', 'S', 'ACC', 3, 'Reasignar un grupo por otro', 5, 'S', 'S'
GO
sp_UpdateAcciones 'HABPROD', 'Hab. p/ pasaje a Producci�n', 'S', 'EVT', 0, 'Hab. p/ pasaje a Producci�n', 0, 'N', 'N'
GO
sp_UpdateAcciones 'HOMA.F', 'Nuevo HOMA definitivo', 'S', 'EVT', 0, 'Nuevo HOMA definitivo', 0, 'N', 'N'
GO
sp_UpdateAcciones 'HOMA.P', 'Nuevo HOMA parcial', 'S', 'EVT', 0, 'Nuevo HOMA parcial', 0, 'N', 'N'
GO
sp_UpdateAcciones 'HOME.F', 'Nuevo HOME definitivo', 'S', 'EVT', 0, 'Nuevo HOME definitivo', 0, 'N', 'N'
GO
sp_UpdateAcciones 'HOME.P', 'Nuevo HOME parcial', 'S', 'EVT', 0, 'Nuevo HOME parcial', 0, 'N', 'N'
GO
sp_UpdateAcciones 'HOMOLOGA', 'Para homologaci�n', 'S', 'EVT', 0, 'Para homologaci�n', 0, 'N', 'N'
GO
sp_UpdateAcciones 'HSOB.F', 'Nuevo HSOB definitivo', 'S', 'EVT', 0, 'Nuevo HSOB definitivo', 0, 'N', 'N'
GO
sp_UpdateAcciones 'HSOB.P', 'Nuevo HSOB parcial', 'S', 'EVT', 0, 'Nuevo HSOB parcial', 0, 'N', 'N'
GO
sp_UpdateAcciones 'HSTADD', 'Agregado manual al historial', 'S', 'ACC', 5, 'Agregar observaciones manuales en el historial', 1, 'S', 'S'
GO
sp_UpdateAcciones 'HSTREG', 'Regularizaci�n de modelo de control y circuito de homologaci�n asociado', 'S', 'EVT', 0, 'Regularizaci�n de modelo de control y circuito de homologaci�n asociado', 0, 'N', 'N'
GO
sp_UpdateAcciones 'INFOHP', 'Nuevo informe de Homologaci�n', 'S', 'EVT', 0, 'Nuevo informe de Homologaci�n', 0, 'N', 'N'
GO
sp_UpdateAcciones 'OKALCA', 'Conforme ALCA definitivo', 'S', 'EVT', 0, 'Conforme ALCA definitivo', 0, 'N', 'N'
GO
sp_UpdateAcciones 'OKALCP', 'Conforme ALCA parcial', 'S', 'EVT', 0, 'Conforme ALCA parcial', 0, 'N', 'N'
GO
sp_UpdateAcciones 'OKPF', 'Ok fin. Pje.Prod.s/ Conf.Homol', 'S', 'EVT', 0, 'Ok fin. Pje.Prod.s/ Conf.Homol', 0, 'N', 'N'
GO
sp_UpdateAcciones 'OKPP', 'Ok parc. Pje.Prod.s/Conf.Homol', 'S', 'EVT', 0, 'Ok parc. Pje.Prod.s/Conf.Homol', 0, 'N', 'N'
GO
sp_UpdateAcciones 'OKTESP', 'Conforme TEST parcial', 'S', 'EVT', 0, 'Conforme TEST parcial', 0, 'N', 'N'
GO
sp_UpdateAcciones 'OKTEST', 'Conforme TEST definitivo', 'S', 'EVT', 0, 'Conforme TEST definitivo', 0, 'N', 'N'
GO
sp_UpdateAcciones 'PASINRO', 'Asignaci�n de n�mero', 'S', 'ACC', 1, 'Asignar n�mero', 100, 'S', 'S'
GO
sp_UpdateAcciones 'PAUTHOM', 'Nuevo grupo (Homologaci�n)', 'S', 'EVT', 0, 'Nuevo grupo (Homologaci�n)', 0, 'N', 'N'
GO
sp_UpdateAcciones 'PCHGBP', 'Cambio de Referente de Sistema', 'S', 'ACC', 1, 'Cambiar el Referente de Sistemas', 90, 'S', 'S'
GO
sp_UpdateAcciones 'PCHGCLS', 'Cambio de clase', 'S', 'EVT', 0, 'Cambio de clase', 0, 'N', 'N'
GO
sp_UpdateAcciones 'PCHGEST', 'Cambio estado de peticion', 'S', 'ACC', 1, 'Cambiar el estado de la petici�n', 70, 'S', 'S'
GO
sp_UpdateAcciones 'PCHGIMP', 'Cambio de impacto tecnol�gico', 'S', 'EVT', 0, 'Cambio de impacto tecnol�gico', 0, 'N', 'N'
GO
sp_UpdateAcciones 'PCHGPRD', 'Fecha prevista pasaje a prod.', 'S', 'EVT', 0, 'Fecha prevista pasaje a prod.', 0, 'N', 'N'
GO
sp_UpdateAcciones 'PCHGPRIO', 'Cambio de prioridad', 'S', 'EVT', 0, 'Cambio de prioridad', 0, 'N', 'N'
GO
sp_UpdateAcciones 'PCHGREGU', 'Cambio en indicador de regulatorio', 'S', 'EVT', 0, 'Cambio en indicador de regulatorio', 0, 'N', 'N'
GO
sp_UpdateAcciones 'PCHGTXT', 'Modificaci�n Alcance Peticion', 'S', 'ACC', 1, 'Modificar el alcance de la petici�n', 120, 'S', 'S'
GO
sp_UpdateAcciones 'PCHGTYP2', 'Se notifica a SI la clasificaci�n de la petici�n', 'S', 'EVT', 0, 'Se notifica a SI la clasificaci�n de la petici�n', 0, 'N', 'N'
GO
sp_UpdateAcciones 'PCHGTYPE', 'Cambio de tipo', 'S', 'EVT', 0, 'Cambio de tipo', 0, 'N', 'N'
GO
sp_UpdateAcciones 'PCHGVISI', 'Cambio de visibilidad', 'S', 'EVT', 0, 'Cambio de visibilidad', 0, 'N', 'N'
GO
sp_UpdateAcciones 'PDEL000', 'Eliminar Petici�n', 'N', 'ACC', 1, 'Eliminar f�sicamente la petici�n', 80, 'S', 'S'
GO
sp_UpdateAcciones 'PIMPORT', 'Importacion Peticion', 'S', 'ACC', 5, 'Importar peticiones desde planilla MS Excel', 2, 'S', 'S'
GO
sp_UpdateAcciones 'PINTERV', 'Intervencion Petici�n', 'S', 'EVT', 1, 'Intervencion en petici�n', 0, 'N', 'N'
GO
sp_UpdateAcciones 'PMOD000', 'Modificar la Peticion', 'S', 'ACC', 1, 'Modificar los atributos de una petici�n', 60, 'S', 'S'
GO
sp_UpdateAcciones 'PMOD500', 'Asignar Nro Peticion', 'S', 'ACC', 1, 'Asignar un n�mero', 110, 'S', 'S'
GO
sp_UpdateAcciones 'PNEW000', 'Nueva petici�n', 'S', 'ACC', 1, 'Dar de alta una petici�n', 10, 'S', 'S'
GO
sp_UpdateAcciones 'PNEW001', 'Nueva petici�n N O R M A L', 'S', 'ACC', 1, 'Dar de alta una petici�n Normal', 20, 'S', 'S'
GO
sp_UpdateAcciones 'PNEW002', 'Nueva petici�n E S P E C I A L', 'S', 'ACC', 1, 'Dar de alta una petici�n Especial', 30, 'S', 'S'
GO
sp_UpdateAcciones 'PNEW003', 'Nueva petici�n P R O P I A', 'S', 'ACC', 1, 'Dar de alta una petici�n Propia', 40, 'S', 'S'
GO
sp_UpdateAcciones 'PNEW004', 'Nueva petici�n P R O Y E C T O', 'S', 'ACC', 1, 'Dar de alta una petici�n Proyecto', 50, 'S', 'S'
GO
sp_UpdateAcciones 'PNEWCLS', 'Asignaci�n de clase', 'S', 'EVT', 0, 'Asignaci�n de clase', 0, 'N', 'N'
GO
sp_UpdateAcciones 'SCHGEST', 'Cambio estado de sector', 'S', 'ACC', 2, 'Cambiar el estado del sector', 4, 'S', 'S'
GO
sp_UpdateAcciones 'SDEL000', 'Eliminar Sector', 'S', 'ACC', 2, 'Eliminar f�sicamente un sector', 3, 'S', 'S'
GO
sp_UpdateAcciones 'SFRZPLN', 'Forzar Planificacion', 'S', 'EVT', 0, 'Forzar Planificacion', 0, 'N', 'N'
GO
sp_UpdateAcciones 'SINTERV', 'Intervencion Sector', 'S', 'EVT', 2, 'Intervencion en sector', 0, 'N', 'N'
GO
sp_UpdateAcciones 'SMOD000', 'Modificar Sector', 'S', 'ACC', 2, 'Modificar un sector', 2, 'S', 'S'
GO
sp_UpdateAcciones 'SNEW000', 'Nuevo sector', 'S', 'ACC', 2, 'Dar de alta un sector', 1, 'S', 'S'
GO
sp_UpdateAcciones 'SREASIG', 'Reasignaci�n de sector', 'S', 'ACC', 2, 'Reasignar un sector por otro', 5, 'S', 'S'
GO
sp_UpdateAcciones 'TRASP', 'Traspaso de cartera de peticiones (e/Grupos)', 'S', 'ACC', 3, 'Traspasar cartera de peticiones entre grupos', 6, 'S', 'S'
GO
sp_UpdateAcciones 'V.HOMA.P', 'Venci� el conforme HOMA parcial', 'S', 'EVT', 0, 'Venci� el conforme HOMA parcial', 0, 'N', 'N'
GO
sp_UpdateAcciones 'V.HOME.P', 'Venci� el conforme HOME parcial', 'S', 'EVT', 0, 'Venci� el conforme HOME parcial', 0, 'N', 'N'
GO
sp_UpdateAcciones 'V.HSOB.P', 'Venci� el conforme HSOB parcial', 'S', 'EVT', 0, 'Venci� el conforme HSOB parcial', 0, 'N', 'N'
GO
sp_UpdateAcciones 'V.OKALCP', 'Venci� el conforme ALCA parcial', 'S', 'EVT', 0, 'Venci� el conforme ALCA parcial', 0, 'N', 'N'
GO
sp_UpdateAcciones 'V.OKPP', 'Venci� el conforme OKPP parcial', 'S', 'EVT', 0, 'Venci� el conforme OKPP parcial', 0, 'N', 'N'
GO
sp_UpdateAcciones 'V.OKTESP', 'Venci� el conforme TEST parcial', 'S', 'EVT', 0, 'Venci� el conforme TEST parcial', 0, 'N', 'N'
GO
sp_UpdateAcciones 'VALIDA', 'Venci� la validez de la petici�n', 'S', 'EVT', 0, 'Venci� la validez de la petici�n', 0, 'N', 'N'
GO
sp_UpdateAcciones 'VINCADD', 'Nuevo documento vinculado', 'S', 'ACC', 1, 'Vincular un documento', 130, 'S', 'S'
GO
sp_UpdateAcciones 'VINCDEL', 'Desvinculaci�n de documento', 'S', 'ACC', 1, 'Desvincular un documento', 150, 'S', 'S'
GO
sp_UpdateAcciones 'PCHGBPE', 'Cambio de Ref. de RGP', 'S', 'ACC', 1, 'Cambiar el Referente de RGP', 91, 'S', 'S'
GO

print 'Fin de actualizaci�n de acciones.'
go