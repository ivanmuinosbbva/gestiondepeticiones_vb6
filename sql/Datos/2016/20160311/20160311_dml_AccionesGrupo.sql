print 'Actualizando datos de AccionesGrupo...'
go

INSERT INTO AccionesGrupo 
VALUES (1, 'Petición', 'S')
go
INSERT INTO AccionesGrupo 
VALUES (2, 'Sectores en peticiones', 'S')
go
INSERT INTO AccionesGrupo 
VALUES (3, 'Grupos ejecutores en peticiones', 'S')
go
INSERT INTO AccionesGrupo 
VALUES (4, 'Recursos de grupos en peticiones', 'S')
go
INSERT INTO AccionesGrupo 
VALUES (5, 'Procesos', 'S')
go

print 'Fin de actualización de AccionesGrupo.'
go