/*
	Elimina de la tabla RecursoPerfil todos los perfiles Solicitantes de recursos
	pertenecientes a la gerencia de Sistemas.

	11.03.2016

	cod_recurso nom_recurso
	----------- -----------
	A59847      Patrignani, Carlos
	A91372      Aboudara, Alejandro
	A82242      Moros, Carlos Jos�
	A59638      Taboada, Javier
	A20895      L�pez, Rub�n Ricardo
	XA50640     Gimenez Cesar
	XA50894     Serapio, Jonathan
	A21918      Icaro, Claudia
	A50931      �lvarez, Leandro
	XA51005     Seminara, Roberto Mariano
	A126117     Gonzalez, Gladys B
	A78440      Cosentino, Graciela
	A53761      Maulella, Marcela
	A126728     Perot, Monica
	A126666     Brum, Raul

	(15 registros)
*/

print 'Eliminando perfiles SOLI para recursos de la gerencia de Sistemas...'
go

delete 
from RecursoPerfil
where 
	cod_perfil = 'SOLI' and 
	cod_recurso in (
	select r.cod_recurso 
	from Recurso r
	where 
		r.estado_recurso = 'A' and
		r.cod_gerencia = 'DESA')
go

/*
	Elimina de la tabla RecursoPerfil todos los perfiles Referente de recursos
	pertenecientes a la gerencia de Sistemas.

	11.03.2016

	cod_recurso nom_recurso
	----------- -----------
	A59847      Patrignani, Carlos
	A50931      �lvarez, Leandro
	A82895      Redlich, Ver�nica
	
	(3 registros)
*/

print 'Eliminando perfiles REFE para recursos de la gerencia de Sistemas...'
go

delete 
from RecursoPerfil
where 
	cod_perfil = 'REFE' and 
	cod_recurso in (
	select r.cod_recurso 
	from Recurso r
	where 
		r.estado_recurso = 'A' and
		r.cod_gerencia = 'DESA')
go

/*
	Elimina de la tabla RecursoPerfil todos los perfiles Autorizante de recursos
	pertenecientes a la gerencia de Sistemas.

	11.03.2016

	cod_recurso nom_recurso
	----------- -----------
	A59847      Patrignani, Carlos
	A50931      �lvarez, Leandro
	A82895      Redlich, Ver�nica

	(3 registros)
*/

print 'Eliminando perfiles AUTO para recursos de la gerencia de Sistemas...'
go

delete 
from RecursoPerfil
where 
	cod_perfil = 'AUTO' and 
	cod_recurso in (
	select r.cod_recurso 
	from Recurso r
	where 
		r.estado_recurso = 'A' and
		r.cod_gerencia = 'DESA')
go
