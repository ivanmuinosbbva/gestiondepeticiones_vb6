-- Inicializo los nuevos campos

print 'Actualizando los nuevos campos de la tabla Peticion...'
go

select 'Total peticiones: ', count(*)
from Peticion
go

UPDATE Peticion
SET 
	pet_driver = 'DESA', 
	cargaBeneficios = 'N'
go
