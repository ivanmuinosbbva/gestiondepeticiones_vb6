-- Nuevos (3)
INSERT INTO Orientacion 
VALUES ('AA', 'Experiencia del cliente', 'S', 10)
go

INSERT INTO Orientacion 
VALUES ('AB', 'Ajuste y depuración de datos', 'S', 90)
go

INSERT INTO Orientacion 
VALUES ('AC', 'Política de nivel directivo', 'S', 100)
go

-- Cambio de nombre (5)
UPDATE Orientacion 
SET nom_orientacion = 'Productividad y eficiencia', orden = 20
WHERE cod_orientacion = 'E' 
go

UPDATE Orientacion 
SET nom_orientacion = 'Gestión de comisiones y gastos', orden = 50
WHERE cod_orientacion = 'G' 
go

UPDATE Orientacion 
SET nom_orientacion = 'Gestión del riesgo', orden = 60
WHERE cod_orientacion = 'S' 
go

UPDATE Orientacion 
SET nom_orientacion = 'Contabilidad y finanzas', orden = 70
WHERE cod_orientacion = 'F' 
go

UPDATE Orientacion 
SET nom_orientacion = 'Infraestructura y soporte', orden = 80
WHERE cod_orientacion = 'I' 
go

-- Deshabilitación
UPDATE Orientacion 
SET flg_habil = 'N'
WHERE cod_orientacion in ('U','R','A','N','T','C') 
go

-- Unión de opciones anteriores
INSERT INTO Orientacion  
VALUES ('AD', 'Control y normas regulatorias', 'S', 30)
go

-- Ordenamiento
UPDATE Orientacion 
SET orden = 40
WHERE cod_orientacion = 'D' 
go
