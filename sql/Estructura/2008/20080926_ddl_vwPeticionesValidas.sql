/*
-000- a. FJS 26.09.2008 - Nueva vista para publicar los n�meros de peticiones v�lidas (Gesti�n de Calidad)
*/

use GesPet
go

print 'Creando/actualizando vista: vwPeticionesValidas'
go

if exists (select * from sysobjects where name = 'vwPeticionesValidas' and sysstat & 7 = 2)
	drop view dbo.vwPeticionesValidas
go

create view dbo.vwPeticionesValidas
as 
	select
		PET.pet_nroasignado
	from
		GesPet..PeticionEnviadas ENV inner join 
		GesPet..Peticion PET on (ENV.pet_nrointerno = PET.pet_nrointerno)
	group by
		PET.pet_nroasignado
	having
		PET.pet_nroasignado <> 0
go

grant select on dbo.vwPeticionesValidas to GesPetUsr
go

print 'Actualizaci�n realizada.'
