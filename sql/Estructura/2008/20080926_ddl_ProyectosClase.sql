print 'Creando / modificando la tabla: ProyectoClase... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'ProyectoClase' and type = 'U')
	drop table GesPet.dbo.ProyectoClase
go
	
create table GesPet.dbo.ProyectoClase (
	ProjCatId			int				not null,
	ProjClaseId			int				not null,
	ProjClaseNom		varchar(100)	null,
	constraint PK_ProjClase001 PRIMARY KEY CLUSTERED (ProjCatId, ProjClaseId )
	)
go

print 'Tabla modificada exitosamente.'