/*
*********************************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Prop�sito:	Modificaci�n de la tabla PeticionGrupo para soporte de funcionalidades de Homologaci�n
* Ambiente:		SYBASE
* Fecha:		09.05.2008
*********************************************************************************************************************
*/

print 'Modificando la tabla PeticionGrupo...'
go
use GesPet
go

print   'Borrando �ndice: idx_PetInterno'
drop index PeticionGrupo.idx_PetInterno
go

alter table GesPet.dbo.PeticionGrupo
	add 
		fe_produccion	smalldatetime	NULL,
		fe_suspension	smalldatetime	NULL,
		cod_motivo 		int				NULL
go

print   'Creando el �ndice: idx_PetInterno'
create unique nonclustered index idx_PetInterno
on GesPet.dbo.PeticionGrupo (pet_nrointerno, cod_grupo)
go

print 'Tabla modificada.'
