/*
*********************************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Prop�sito:	Modificaci�n de la tabla Peticion para indicar expl�citamente si es regulatorio o no
* Ambiente:		SYBASE
* Fecha:		19.08.2008
*********************************************************************************************************************
*/

print 'Modificando la tabla Peticion...'
go
use GesPet
go

print   'Borrando �ndice: idx_PetAnexada'
drop index Peticion.idx_PetAnexada
go

print   'Borrando �ndice: idx_PetInterno'
drop index Peticion.idx_PetInterno
go

alter table GesPet.dbo.Peticion
	add 
		pet_regulatorio		char(1)	null,
		pet_projid			int		null,
		pet_projsubid		int		null,
		pet_projsubsid		int		null
go

print   'Creando el �ndice: idx_PetAnexada'
create nonclustered index idx_PetAnexada
on GesPet.dbo.Peticion (pet_nroanexada)
go

print   'Creando el �ndice: idx_PetInterno'
create unique nonclustered index idx_PetInterno
on GesPet.dbo.Peticion (pet_nrointerno)
go

print 'Tabla modificada.'
