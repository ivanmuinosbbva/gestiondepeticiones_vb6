print 'Creando la tabla: tmpPetorg... aguarde por favor...'

if exists (select (1) from sysobjects where name = 'tmpPetOrg' and type = 'U')
	drop table GesPet.dbo.tmpPetOrg
go
	
create table GesPet.dbo.tmpPetOrg (
		pet_nroasignado		int				null,
		pet_tipo			char(3)			null,
		pet_clase			char(4)			null,
		pet_imptec			char(1)			null,
		pet_titulo			char(50)		null,
		pet_descripcion		char(255)		null,
		pet_prioridad		char(1)			null,
		pet_fe_comite		smalldatetime	null,
		pet_cod_estado		char(200)		null,
		pet_fe_estado		smalldatetime	null,
		pet_cod_sector		char(100)		null,
		pet_cod_bpar		char(100)		null,
		pet_cod_solicitante	char(100)		null,
		pet_cod_referente	char(100)		null,
		pet_cod_autorizante	char(100)		null,
		pet_horaspresup		int				null,
		pet_hs_resp			int				null,
		pet_hs_rec			int				null,
		pet_hs_resp_no		int				null,
		pet_hs_rec_no		int				null,
		pet_fe_ini_plan		smalldatetime	null,
		pet_fe_fin_plan		smalldatetime	null,
		pet_fe_ini_real		smalldatetime	null,
		pet_fe_fin_real		smalldatetime	null,
		pet_fe_ini_orig		smalldatetime	null,
		pet_fe_fin_orig		smalldatetime	null,
		pet_cant_planif		int				null,
		pet_corp_local		char(1)			null,
		pet_visibilidad		char(100)		null,
		pet_cod_orientacion	char(2)			null,
		pet_cod_grupo		char(100)		null,
		grp_cod_estado		char(100)		null,
		grp_horaspresup		int				null,
		grp_fe_ini_plan		smalldatetime	null,
		grp_fe_fin_plan		smalldatetime	null,
		grp_fe_ini_real		smalldatetime	null,
		grp_fe_fin_real		smalldatetime	null,
		grp_fe_ini_orig		smalldatetime	null,
		grp_fe_fin_orig		smalldatetime	null,
		grp_hs_resp			int				null,
		grp_hs_rec			int				null,
		pet_empresa			int				null)
go

print 'Tabla creada exitosamente.'