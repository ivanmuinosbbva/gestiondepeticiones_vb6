/*
-000- a. FJS 12.06.2008 - Nueva vista para que el sistema SGI pueda ver peticiones "del d�a"
*/

use GesPet
go

print 'Creando/actualizando View: vwPeticionesHoy'
go

if exists (select * from sysobjects where name = 'vwPeticionesHoy' and sysstat & 7 = 2)
	drop view dbo.vwPeticionesHoy
go

create view dbo.vwPeticionesHoy 
as
    select 
        a.pet_nrointerno as NroInterno, 
        a.pet_nroasignado as NroAsignado,
        a.cod_tipo_peticion as PeticionTipo, 
        a.cod_clase as PeticionClase,
        a.titulo as PeticionTitulo, 
        (select 
			z.mem_texto 
		 from 
			GesPet..PeticionMemo z 
         where 
			z.pet_nrointerno = a.pet_nrointerno and
			z.mem_campo = 'DESCRIPCIO' and z.mem_secuencia = 0) as PeticionDescripcion,
        a.audit_date as PeticionFechaAlta,
        a.fe_estado as FechaUltimoEstado,
        a.cod_estado as PeticionEstado
    from
        GesPet..Peticion a inner join GesPet..Historial c on
        a.pet_nrointerno = c.pet_nrointerno 
    where
        (
        datediff(day, convert(char(8),a.fe_estado,112), Convert(char(8),GetDate(),112)) < 4 and
        a.cod_estado = 'COMITE' and 
        a.pet_nroasignado <> 0 and 
        a.cod_tipo_peticion not in ('PRO', 'ESP')) or
        (
        datediff(day, convert(char(8),a.audit_date,112), Convert(char(8),GetDate(),112)) < 4 and
        a.pet_nroasignado <> 0 and 
        c.cod_evento in ('PNEW001','PNEW002', 'PNEW003', 'PNEW004', 'PMOD500', 'PASINRO') and
        a.cod_tipo_peticion in ('PRO','ESP')
        )

grant select on dbo.vwPeticionesHoy to cgm_gesinc 
go

grant select on dbo.vwPeticionesHoy to GesPetUsr 
go

print 'Actualizaci�n finalizada.'
go