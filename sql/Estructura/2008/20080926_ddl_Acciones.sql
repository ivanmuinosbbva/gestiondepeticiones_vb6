/*
*******************************************************************************************
* Propietario: 	Fernando J. Spitz
* Propósito:	Modificación de la tabla Acciones para ampliar el campo de la descripción
*				(se modifica la longitud de 30 a 100)
* Ambiente:		SyBase / SQL Server
* Fecha:		21.08.2008
*******************************************************************************************
*/

print 'Modificando la tabla GesPet.dbo.Acciones...'
go
use GesPet
go

alter table GesPet.dbo.Acciones
	modify nom_accion	varchar(100)	null
go

print 'Tabla modificada.'