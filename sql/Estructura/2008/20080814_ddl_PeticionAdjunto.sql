/*
*********************************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Prop�sito:	Agregar una columna que identifique el sector y grupo que adjunta un documento
* Ambiente:		SYBASE
* Fecha:		18.07.2008
*********************************************************************************************************************
*/

print 'Modificando la tabla PeticionAdjunto...'
go
use GesPet
go

print   'Borrando �ndice: idx_PeticionAdjunto'
drop index PeticionAdjunto.idx_PeticionAdjunto
go

alter table GesPet.dbo.PeticionAdjunto
	add 
		cod_sector		char(8)		null,
		cod_grupo		char(8)		null
go

print   'Creando el �ndice: idx_PeticionAdjunto'
create unique nonclustered index idx_PeticionAdjunto
on GesPet.dbo.PeticionAdjunto (pet_nrointerno, adj_tipo, adj_file)
go

grant Delete on PeticionAdjunto to GesPetUsr 
go

grant Insert on PeticionAdjunto to GesPetUsr 
go

grant Update on PeticionAdjunto to GesPetUsr 
go

grant Select on PeticionAdjunto to GesPetUsr 
go

print 'Tabla modificada.'
