print 'Creando / modificando la tabla: ProyectoCategoria... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'ProyectoCategoria' and type = 'U')
	drop table GesPet.dbo.ProyectoCategoria
go
	
create table GesPet.dbo.ProyectoCategoria (
	ProjCatId		int				not null,
	ProjCatNom		varchar(100)	null,
	constraint PK_ProjCat001 PRIMARY KEY CLUSTERED (ProjCatId )
	)
go

print 'Tabla modificada exitosamente.'