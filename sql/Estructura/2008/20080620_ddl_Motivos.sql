/*
*********************************************************************************************
* Propietario: 	Fernando J. Spitz
* Prop�sito:	Nueva tabla para motivos de suspensi�n de grupos DyD en peticiones
* Ambiente:		SyBase / SQL Server
* Fecha:		09.05.2008
*********************************************************************************************
*/

print 'Creando la tabla: Motivos'
go

create table dbo.Motivos (
	cod_motivo  int not			null,
	nom_motivo  varchar(120)    null,
	hab_motivo  char(1)			null,
	constraint PK_Motivos1 PRIMARY KEY CLUSTERED (cod_motivo)
)

print 'Tabla creada.'