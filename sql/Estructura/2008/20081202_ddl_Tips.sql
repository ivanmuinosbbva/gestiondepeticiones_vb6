/*
-000- a. FJS 24.11.2008 - Se agrega esta tabla para guardar los tips al usuario y las novedades del sistema.
*/

print 'Creando / modificando la tabla: Tips... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Tips' and type = 'U')
	drop table GesPet.dbo.Tips
go
	
create table GesPet.dbo.Tips (
	tips_version		char(10)		not null,
	tips_tipo			char(6)			not null,
	tips_nro			int				not null,
	tips_rng			int				not null,
	tips_txt			varchar(255)	null,
	tips_date			smalldatetime	null,
	tips_hab			char(1)			null,
	constraint PK_Tips PRIMARY KEY CLUSTERED (tips_version, tips_tipo, tips_nro, tips_rng))

go

print 'Tabla modificada exitosamente.'