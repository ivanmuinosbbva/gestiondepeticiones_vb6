/*
*******************************************************************************************
* Propietario: 	Fernando J. Spitz
* Prop�sito:	Modificaci�n de la tabla tmpPetOrg para incluir los datos de:
*				- Regulatorio
*				- Proyecto IDM (c�digos de Proyecto, Subproyecto, subsubproyecto y nombre
*				- Fecha deseada
*				- Beneficios esperados
*				- Relaci�n con otros requerimientos
* Ambiente:		SyBase / SQL Server
* Fecha:		27.10.2008
*******************************************************************************************
*/

print 'Modificando la tabla: tmpPetorg... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'tmpPetOrg' and type = 'U')
	drop table GesPet.dbo.tmpPetOrg
go
	
create table GesPet.dbo.tmpPetOrg (
		pet_nroasignado		int				null,
		pet_tipo			char(3)			null,
		pet_clase			char(4)			null,
		pet_imptec			char(1)			null,
		pet_titulo			char(50)		null,
		pet_descripcion		char(255)		null,
		pet_prioridad		char(1)			null,
		pet_fe_comite		char(10)		null,
		pet_cod_estado		char(30)		null,	-- 50
		pet_fe_estado		char(10)		null,
		pet_cod_sector		char(98)		null,	-- 80
		pet_cod_bpar		char(50)		null,	-- 80
		pet_cod_solicitante	char(50)		null,	-- 80
		pet_cod_referente	char(50)		null,	-- 80
		pet_cod_autorizante	char(50)		null,	-- 80
		pet_horaspresup		int				null,
		pet_hs_resp			int				null,
		pet_hs_rec			int				null,
		pet_hs_resp_no		int				null,
		pet_hs_rec_no		int				null,
		pet_fe_ini_plan		char(10)		null,
		pet_fe_fin_plan		char(10)		null,
		pet_fe_ini_real		char(10)		null,
		pet_fe_fin_real		char(10)		null,
		pet_fe_ini_orig		char(10)		null,
		pet_fe_fin_orig		char(10)		null,
		pet_cant_planif		int				null,
		pet_corp_local		char(1)			null,
		pet_visibilidad		char(30)		null,	-- 100
		pet_cod_orientacion	char(2)			null,
		pet_cod_grupo		char(64)		null,	-- 100
		grp_cod_estado		char(30)		null,	-- 50
		grp_horaspresup		int				null,
		grp_hs_resp			int				null,
		grp_hs_rec			int				null,
		grp_fe_ini_plan		char(10)		null,
		grp_fe_fin_plan		char(10)		null,
		grp_fe_ini_real		char(10)		null,
		grp_fe_fin_real		char(10)		null,
		grp_fe_ini_orig		char(10)		null,
		grp_fe_fin_orig		char(10)		null,
		pet_empresa			int				null,
		regulatorio			char(1)			null,
	    prjid				int				null,
	    prjsubid			int				null,
	    prjsubsid			int				null,
	    prjnom				varchar(255)	null,	-- 100
	    fec_deseada			char(10)		null,
	    beneficios			varchar(255)	null,	-- 100
	    relacion_otros		varchar(255)	null)	-- 100
go

print 'Tabla modificada exitosamente.'