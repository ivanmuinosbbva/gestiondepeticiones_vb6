print 'Creando / modificando la tabla: ProyectoIDM... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'ProyectoIDM' and type = 'U')
	drop table GesPet.dbo.ProyectoIDM
go
	
create table GesPet.dbo.ProyectoIDM (
	ProjId			int				not null,
	ProjSubId		int				not null,
	ProjSubSId		int				not null,
	ProjNom			varchar(100)	null,
	ProjFchAlta		smalldatetime	null,
	ProjCatId		int				not null,
	ProjClaseId		int				not null,
	constraint PK_Proj001 PRIMARY KEY CLUSTERED (ProjId, ProjSubId, ProjSubSId )
	)
go

print 'Tabla modificada exitosamente.'