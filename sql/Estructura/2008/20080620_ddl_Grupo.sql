/*
*******************************************************************************************
* Propietario: 	Fernando J. Spitz
* Prop�sito:	Modificaci�n de la tabla Grupo para la parte de Homologaci�n
* Ambiente:		SyBase / SQL Server
* Fecha:		09.05.2008
*******************************************************************************************
*/

print 'Modificando la tabla GesPet.dbo.Grupo...'
go
use GesPet
go

print   'Borrando �ndice: idx_Grupo'
drop index Grupo.idx_Grupo
go

alter table GesPet.dbo.Grupo
	add grupo_homologacion	char(1)		default		'N'    not null
go

print   'Creando el �ndice: idx_Grupo'
create unique nonclustered index idx_Grupo
on GesPet.dbo.Grupo (cod_grupo)
go

print 'Tabla modificada.'