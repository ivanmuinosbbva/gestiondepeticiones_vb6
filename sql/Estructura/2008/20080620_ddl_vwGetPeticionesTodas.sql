/*
-000- a. FJS 12.06.2008 - Nueva vista para que el sistema SGI pueda ver todas las peticiones "anteriores"
*/

use GesPet
go

print 'Creando/actualizando View: vwPeticionesTodas'
go

if exists (select * from sysobjects where name = 'vwPeticionesTodas' and sysstat & 7 = 2)
	drop view dbo.vwPeticionesTodas
go

create view dbo.vwPeticionesTodas 
as
    select 
        a.pet_nrointerno as NroInterno, 
        a.pet_nroasignado as NroAsignado,
        a.cod_tipo_peticion as PeticionTipo, 
        a.cod_clase as PeticionClase,
        a.titulo as PeticionTitulo, 
        c.mem_texto as PeticionDescripcion,
        a.audit_date as PeticionFechaAlta,
        a.cod_estado as PeticionEstado
    from
        GesPet..Peticion a left join GesPet..PeticionMemo c on
        a.pet_nrointerno = c.pet_nrointerno
    where 
        ((c.mem_campo = 'DESCRIPCIO' and c.mem_secuencia = 0) or c.mem_campo is null) and
        a.pet_nroasignado <> 0

grant select on dbo.vwPeticionesTodas to cgm_gesinc 
go

grant select on dbo.vwPeticionesTodas to GesPetUsr 
go

print 'Actualización finalizada.'
go