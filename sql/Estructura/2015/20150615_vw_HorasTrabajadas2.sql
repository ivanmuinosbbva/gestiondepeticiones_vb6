/*
-001- a. FJS 10.06.2015 - Nueva vista para Diego Carbone. Se agrega el nro. de petición asignado.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_HorasTrabajadas2'
go

if exists (select * from sysobjects where name = 'vw_HorasTrabajadas2' and sysstat & 7 = 2)
	drop view dbo.vw_HorasTrabajadas2
go

create view dbo.vw_HorasTrabajadas2
as 
	select
		a.cod_recurso,
		a.cod_tarea,
		a.pet_nrointerno,
		pet_nroasignado = (select x.pet_nroasignado from Peticion x where x.pet_nrointerno = a.pet_nrointerno),
		fe_desde = (convert(char(8), a.fe_desde, 112)),
		fe_hasta = (convert(char(8), a.fe_hasta, 112)),
		a.horas,
		a.trabsinasignar,
		a.audit_user,
		audit_date = (convert(char(8), a.audit_date, 112))
	from GesPet..HorasTrabajadas a
go

grant select on dbo.vw_HorasTrabajadas2 to GesPetUsr
go

print 'Actualización realizada.'
go


/*
-- Prueba real
select
	a.cod_recurso,
	a.pet_nrointerno,
	MIN(a.fe_desde)	as fe_desde,
	MAX(a.fe_hasta) as fe_hasta,
	SUM(a.horas/60)	as horas		
from GesPet..HorasTrabajadas a
where
	a.pet_nrointerno = 58004
group by 
	a.cod_recurso,
	a.pet_nrointerno



	select
		a.cod_recurso,
		a.pet_nrointerno,
		MIN(a.fe_desde)	as fe_desde,
		MAX(a.fe_hasta) as fe_hasta,
		SUM(a.horas/60)	as horas		
	from GesPet..HorasTrabajadas a
	group by 
		a.cod_recurso,
		a.pet_nrointerno


pet_nroasignado = (select x.pet_nroasignado from Peticion x where x.pet_nrointerno = a.pet_nrointerno),

		a.cod_tarea,
		a.trabsinasignar,
		--fe_desde = (convert(char(8), a.fe_desde, 112)),
		--fe_hasta = (convert(char(8), a.fe_hasta, 112)),
		a.audit_user,
		audit_date = (convert(char(8), a.audit_date, 112))


*/