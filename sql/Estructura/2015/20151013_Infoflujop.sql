print 'Creando / modificando la tabla: Infoflujop... aguarde por favor...'
go

-- ****************************************************************************************************
-- Infoflujop: parametrización (relación clase de petición, tipo de proceso de homologación)
-- ****************************************************************************************************
if exists (select (1) from sysobjects where name = 'Infoflujop' and type = 'U')
	drop table GesPet.dbo.Infoflujop
go

create table GesPet.dbo.Infoflujop (
	infoflujo_id		int			not null,		-- Id. de flujo
	infoflujo_clase		char(4)		not null,		-- Clase de petición
	CONSTRAINT PK_Infoflujop1 PRIMARY KEY NONCLUSTERED (infoflujo_id, infoflujo_clase))
go

print 'Creada / modificanda la tabla.'
go