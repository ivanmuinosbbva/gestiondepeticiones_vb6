print 'Creando / modificando la tabla: PeticionInfoht... aguarde por favor...'
go

-- ****************************************************************************************************
-- Peticiones: informes de homologación (observaciones adicionales)
-- ****************************************************************************************************
if exists (select (1) from sysobjects where name = 'PeticionInfoht' and type = 'U')
	drop table GesPet.dbo.PeticionInfoht
go

create table GesPet.dbo.PeticionInfoht (
	pet_nrointerno		int				not null,			-- Nro. interno de petición
	info_id				int				not null,			-- Id. de informe
	info_idver			int				not null,			-- Versión
	info_renglon		int				not null,			-- Renglón de la observación
	info_renglontexto	varchar(255)	null,				-- Texto del renglón
	CONSTRAINT PK_PetInfoht1 PRIMARY KEY NONCLUSTERED (pet_nrointerno, info_id, info_idver, info_renglon))
go

print 'Creada / modificanda la tabla.'
go
