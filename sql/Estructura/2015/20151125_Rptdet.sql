print 'Creando / modificando la tabla: Rptdet... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Rptdet' and type = 'U')
	drop table GesPet.dbo.Rptdet
go
	
create table GesPet.dbo.Rptdet (
	rptid		char(10)		not null,			-- Id.
	rptitem		int				not null,			-- Item
	rptnom		varchar(100)	null,				-- Nombre o descripci�n de la opci�n
	rpthab		char(1)			null,				-- Habilitado
	constraint PKRepo11 PRIMARY KEY NONCLUSTERED (rptid, rptitem))
go

print 'Tabla modificada exitosamente.'
go