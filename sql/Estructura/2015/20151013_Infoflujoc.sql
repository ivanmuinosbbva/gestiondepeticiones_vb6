print 'Creando / modificando la tabla: Infoflujoc... aguarde por favor...'
go

-- ****************************************************************************************************
-- Definici�n de flujo para informes de homologaci�n seg�n su tipo: Completo o Reducido
-- Infoflujoc: cabecera
-- ****************************************************************************************************
if exists (select (1) from sysobjects where name = 'Infoflujoc' and type = 'U')
	drop table GesPet.dbo.Infoflujoc
go

create table GesPet.dbo.Infoflujoc (
	infoflujo_id		int			not null,		-- Id. de flujo
	infoflujo_nom		char(50)		null,		-- Nombre descriptivo del flujo
	infoflujo_tipo		char(1)			null,		-- Tipo: (C)ompleto o (R)educido.
	infoflujo_vigencia	smalldatetime	null,		-- Fecha de vigencia
	CONSTRAINT PK_Infoflujoc1 PRIMARY KEY NONCLUSTERED (infoflujo_id))
go

print 'Creada / modificanda la tabla.'
go