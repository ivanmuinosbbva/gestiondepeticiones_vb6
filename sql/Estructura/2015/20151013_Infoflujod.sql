print 'Creando / modificando la tabla: Infoflujod... aguarde por favor...'
go

-- ****************************************************************************************************
-- Infoflujod: detalle
-- ****************************************************************************************************
if exists (select (1) from sysobjects where name = 'Infoflujod' and type = 'U')
	drop table GesPet.dbo.Infoflujod
go

create table GesPet.dbo.Infoflujod (
	infoflujo_id		int			not null,		-- Id. de flujo
	infoprot_id			int			not null,		-- Id. de prototipo de informe
	infoflujo_orden		int			null,			-- Orden de aparición
	CONSTRAINT PK_Infoflujod1 PRIMARY KEY NONCLUSTERED (infoflujo_id, infoprot_id))
go

print 'Creada / modificanda la tabla.'
go

-- FOREIGN KEY FK_Infoflujod1 (infoflujo_orden) REFERENCES Infoproc (infoprot_id)