print 'Creando / modificando la tabla: Rptcab... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Rptcab' and type = 'U')
	drop table GesPet.dbo.Rptcab
go
	
create table GesPet.dbo.Rptcab (
	rptid		char(10)		not null,			-- Id.
	rptnom		varchar(50)		null,				-- Nombre o descripci�n del reporte
	rpthab		char(1)			null,				-- Habilitado
	rptfch		smalldatetime	null,				-- Fecha de creaci�n
	constraint PKRepo1 PRIMARY KEY NONCLUSTERED (rptid))
go

print 'Tabla modificada exitosamente.'
go