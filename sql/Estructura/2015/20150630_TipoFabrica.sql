print 'Creando / modificando la tabla: TipoFabrica... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'TipoFabrica' and type = 'U')
	drop table GesPet.dbo.TipoFabrica
go
	
create table GesPet.dbo.TipoFabrica (
	TipoFabId		int				not null,
	TipoFabNom		varchar(50)		null,
	constraint PK_TipoFabrica1 PRIMARY KEY NONCLUSTERED (TipoFabId))
go

print 'Tabla modificada exitosamente.'
go