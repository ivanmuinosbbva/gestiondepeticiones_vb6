print 'Creando/modificando la tabla: Fabrica... aguarde por favor...'
go

use GesPet
go

alter table GesPet.dbo.Fabrica
	add tipo_fab 	int	default ''		NOT NULL
go

alter table GesPet.dbo.Fabrica
	add FOREIGN KEY (tipo_fab) REFERENCES TipoFabrica(TipoFabId)
go

print 'Tabla modificada exitosamente.'
go
