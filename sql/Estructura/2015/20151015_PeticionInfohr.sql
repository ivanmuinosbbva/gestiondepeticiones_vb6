print 'Creando / modificando la tabla: PeticionInfohr... aguarde por favor...'
go

-- ****************************************************************************************************
-- Peticiones: informes de homologaci�n (responsables)
-- ****************************************************************************************************
if exists (select (1) from sysobjects where name = 'PeticionInfohr' and type = 'U')
	drop table GesPet.dbo.PeticionInfohr
go
create table GesPet.dbo.PeticionInfohr (
	pet_nrointerno		int				not null,			-- Nro. interno de petici�n
	info_id				int				not null,			-- Id. de informe
	info_idver			int				not null,			-- Versi�n
	info_item			int				not null,			-- Item del informe
	cod_recurso			char(10)		not null,			-- C�digo de legajo del responsable
	cod_perfil			char(4)			null,				-- Perfil del responsable
	cod_nivel			char(4)			null,				-- Nivel
	cod_area			char(8)			null,				-- �rea
	info_valor			int				null,				-- 1:SOB / 2:OME / 3:OMA
	info_comen			varchar(255)	null,				-- Comentarios del item
	fecha_envio			smalldatetime	null,				-- Fecha y hora de envio de mail (original)
	fecha_reenvio		smalldatetime	null,				-- Fecha y hora de reenvio de mail (�ltimo reenvio)
	CONSTRAINT PK_PetInfohr1 PRIMARY KEY NONCLUSTERED (pet_nrointerno, info_id, info_idver, info_item, cod_recurso))
go

print 'Creada / modificanda la tabla.'
go