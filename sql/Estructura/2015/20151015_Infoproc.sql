print 'Creando / modificando la tabla: Infoproc... aguarde por favor...'
go

-- ****************************************************************************************************
-- Definición de informes para homologación (cabecera)
-- ****************************************************************************************************
if exists (select (1) from sysobjects where name = 'Infoproc' and type = 'U')
	drop table GesPet.dbo.Infoproc
go
create table GesPet.dbo.Infoproc (
	infoprot_id			int			not null,			-- Id. de prototipo de informe
	infoprot_nom		char(50)		null,			-- Nombre descriptivo del prototipo
	infoprot_tipo		char(1)			null,			-- Tipo: (C)ompleto o (R)educido.
	infoprot_punto		char(1)			null,			-- Punto de control: 1, 2, 3, etc.
	infoprot_estado		char(1)			null,			-- Estado (S: habilitado / N: deshabilitado)
	infoprot_vermaj		smallint		null,			-- Versión: mayor
	infoprot_vermin		smallint		null,			-- Versión: menor
	CONSTRAINT PK_Infoproc1 PRIMARY KEY NONCLUSTERED (infoprot_id))
go

print 'Creada / modificanda la tabla.'
go
