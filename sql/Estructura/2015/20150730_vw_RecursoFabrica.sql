/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_RecursoFabrica'
go

if exists (select * from sysobjects where name = 'vw_RecursoFabrica' and sysstat & 7 = 2)
	drop view dbo.vw_RecursoFabrica
go

create view dbo.vw_RecursoFabrica
as 
	select 
		r.cod_recurso,
		r.nom_recurso,
		r.cod_gerencia,
		r.cod_direccion,
		r.cod_sector,
		r.cod_grupo,
		'R'	as tipo
	from
		Recurso r
	union all
	select 
		f.cod_fab,
		f.nom_fab,
		f.cod_gerencia,
		f.cod_direccion,
		f.cod_sector,
		f.cod_grupo,
		'F'	as tipo
	from
		Fabrica f
go

grant select on dbo.vw_RecursoFabrica to GesPetUsr
go

print 'Actualización realizada.'
