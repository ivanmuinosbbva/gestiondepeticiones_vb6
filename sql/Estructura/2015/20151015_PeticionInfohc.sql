print 'Creando / modificando la tabla: PeticionInfohc... aguarde por favor...'
go

-- ****************************************************************************************************
-- Peticiones: informes de homologación (cabecera)
-- ****************************************************************************************************
if exists (select (1) from sysobjects where name = 'PeticionInfohc' and type = 'U')
	drop table GesPet.dbo.PeticionInfohc
go
create table GesPet.dbo.PeticionInfohc (
	pet_nrointerno		int			not null,			-- Nro. interno de petición
	info_id				int			not null,			-- Id. de informe
	info_idver			int			not null,			-- Versión
	info_tipo			char(1)			null,			-- Tipo: C:Completo/R:Reducido
	info_punto			int				null,			-- Punto de control del informe (1,2,3, etc.)
	info_fecha			smalldatetime	null,			-- Fecha de alta
	info_recurso		char(10)		null,			-- Recurso que adjunta el informe
	info_estado			char(6)			null,			-- Estado del informe
	infoprot_id			int				null,			-- Id. de prototipo de informe
	CONSTRAINT PK_PetInfohc1 PRIMARY KEY NONCLUSTERED (pet_nrointerno, info_id, info_idver))
go

print 'Creada / modificanda la tabla.'
go