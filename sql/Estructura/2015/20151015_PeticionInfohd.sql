print 'Creando / modificando la tabla: PeticionInfohd... aguarde por favor...'
go

-- ****************************************************************************************************
-- Peticiones: informes de homologación (detalle)
-- ****************************************************************************************************
if exists (select (1) from sysobjects where name = 'PeticionInfohd' and type = 'U')
	drop table GesPet.dbo.PeticionInfohd
go
create table GesPet.dbo.PeticionInfohd (
	pet_nrointerno		int			not null,			-- Nro. interno de petición
	info_id				int			not null,			-- Id. de informe
	info_idver			int			not null,			-- Versión
	info_item			int			not null,			-- Item del informe
	info_estado			char(1)			null,			-- Si/No
	info_valor			int				null,			-- 1:SOB / 2:OME / 3:OMA
	info_orden			int				null,			-- Orden en que debe aparecer en la grilla
	CONSTRAINT PK_PetInfohd1 PRIMARY KEY NONCLUSTERED (pet_nrointerno, info_id, info_idver, info_item))
go

print 'Creada / modificanda la tabla.'
go