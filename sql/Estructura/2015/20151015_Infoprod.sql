print 'Creando / modificando la tabla: Infoprod... aguarde por favor...'
go

-- ****************************************************************************************************
-- Definición de informes para homologación (detalle)
-- ****************************************************************************************************
if exists (select (1) from sysobjects where name = 'Infoprod' and type = 'U')
	drop table GesPet.dbo.Infoprod
go
create table GesPet.dbo.Infoprod (
	infoprot_id			int			not null,			-- Id. de prototipo de informe
	infoprot_item		int			not null,			-- Item
	infoprot_req		char(1)			null,			-- S:Obligatorio/R:Recomendado/N:Opcional
	infoprot_itemest	char(1)			null,			-- Estado (S:habilitado / N:no habilitado)
	infoprot_gru		int				null,			-- Grupo
	infoprot_orden		int				null,			-- Orden
	CONSTRAINT PK_Infoprod1 PRIMARY KEY NONCLUSTERED (infoprot_id, infoprot_item))
go

print 'Creada / modificanda la tabla.'
go