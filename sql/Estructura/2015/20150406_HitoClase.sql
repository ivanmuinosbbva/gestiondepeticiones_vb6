print 'Creando / modificando la tabla: HitoClase... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'HitoClase' and type = 'U')
	drop table GesPet.dbo.HitoClase
go
	
create table GesPet.dbo.HitoClase (
	hitoid			int				not null,
	hitodesc		char(60)		null,
	hitoexpos		char(1)			null,
	constraint PK_HitoClase1 PRIMARY KEY NONCLUSTERED (hitoid))
go

print 'Tabla modificada exitosamente.'
go