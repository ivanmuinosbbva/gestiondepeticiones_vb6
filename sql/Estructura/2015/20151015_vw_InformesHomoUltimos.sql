/*
-000- a. FJS 23.04.2015 - Nueva vista para ver la �ltima cabecera de los informes de homologaci�n por petici�n (el �ltimo informe generado).
						  Se usa para joinear con la tabla de peticiones y armar la vista de la bandeja de trabajo de los homologadores.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_InformesHomoUltimos'
go

if exists (select * from sysobjects where name = 'vw_InformesHomoUltimos' and sysstat & 7 = 2)
	drop view dbo.vw_InformesHomoUltimos
go

create view dbo.vw_InformesHomoUltimos
as 
	select 
		pc.pet_nrointerno	as pet_nrointerno,
		pc.info_id			as info_id,
		pc.info_idver		as info_idver,
		pc.info_fecha		as info_fecha,
		pc.info_tipo		as info_tipo,
		pc.info_punto		as info_punto,
		pc.info_recurso		as info_recurso,
		pc.info_estado		as info_estado,
		pc.infoprot_id		as infoprot_id
	from PeticionInfohc pc 
	where pc.info_id in (
			select MAX(p.info_id)
			from PeticionInfohc p
			group by p.pet_nrointerno)
go

grant select on dbo.vw_InformesHomoUltimos to GesPetUsr
go

print 'Actualizaci�n realizada.'
go

	/*
	select 
		pc.pet_nrointerno		as pet_nrointerno,
		MAX(pc.info_id)			as info_id, 
		MAX(pc.info_fecha)		as info_fecha,
		MAX(pc.info_tipo)		as info_tipo,
		MAX(pc.info_punto)		as info_punto,
		MAX(pc.info_recurso)	as info_recurso,
		MAX(pc.info_estado)		as info_estado,
		MAX(pc.infoprot_id)		as infoprot_id
	from PeticionInfohc pc 
	group by pc.pet_nrointerno
	*/