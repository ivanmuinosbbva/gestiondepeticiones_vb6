print 'Creando / modificando la tabla: Infoproi... aguarde por favor...'
go

-- ****************************************************************************************************
-- Definici�n de informes para homologaci�n (items)
-- ****************************************************************************************************
if exists (select (1) from sysobjects where name = 'Infoproi' and type = 'U')
	drop table GesPet.dbo.Infoproi
go

create table GesPet.dbo.Infoproi (
	infoprot_item		int			not null,			-- Id (item)
	infoprot_itemdsc	varchar(255)	null,			-- Texto descriptivo del item
	infoprot_itemest	char(1)			null,			-- Estado (S:habilitado / N:No habilitado)
	infoprot_itemtpo	char(1)			null,			-- Tipo de respuesta: A={A,S,D,N}		B={C,P}		C={V,R,U,L}
	infoprot_itemres	char(4)			null,			-- Responsabilidad: Solicitante / Referente / Autorizante / Referente de sistema / Responsable t�cnico / Responsable funcional
	infoprot_itemresp	varchar(255)	null,			-- Respuesta predefinida.
	CONSTRAINT PK_Infoproi1 PRIMARY KEY NONCLUSTERED (infoprot_item))
go

print 'Creada / modificanda la tabla.'
go

/*

Tipo de respuesta: 
******************
A={A,S,D,N}

A:		N/A
S:		Si
D,N:	No

B={C,P}

C:		C100
P:		P950

C={V,R,U,L}

R:		Referente
S:		Superv.
L:		L�der
U:		Usuario


Opciones:
*********
NULL	
SOLI	Solicitante
REFE	Referente
AUTO	Autorizante
BPAR	Referente de sistema
GRUT	Responsable t�cnico
GRUF	Responsable funcional
SECT	Subgerente t�cnico
SECF	Subgerente funcional
MULT	A elecci�n: hoy queda en USUARIO, REF. SIST. o SUPERV. (Usuario, Referente de sistema o Supervisor).
*/