print 'Creando/modificando la tabla: Sector... aguarde por favor...'
go

use GesPet
go

alter table GesPet.dbo.Sector
	add 
		soli_hab	char(1)		NULL		-- Indica (S/N) si el sector est� habilitado para ser utilizado como sector solicitante en peticiones especiales.
go

print 'Tabla modificada exitosamente.'
go
