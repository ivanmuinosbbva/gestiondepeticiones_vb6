print 'Creando / modificando la tabla: Infogru... aguarde por favor...'
go

-- ****************************************************************************************************
-- Definición grupos para informes para homologación
-- ****************************************************************************************************
if exists (select (1) from sysobjects where name = 'Infogru' and type = 'U')
	drop table GesPet.dbo.Infogru
go

create table GesPet.dbo.Infogru (
	info_gruid		int			not null,				-- Id. de grupo
	info_grunom		char(50)		null,				-- Nombre del grupo
	info_gruorden	int				null,				-- Orden
	info_gruhab		char(1)			null,				-- Estado (S:habilitado / N:no habilitado)
	CONSTRAINT PK_Infogru1 PRIMARY KEY NONCLUSTERED (info_gruid))
go

print 'Creada / modificanda la tabla.'
go
