Print 'Creando / modificando la tabla: HEDT012... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'HEDT012' and type = 'U')
	drop table GesPet.dbo.HEDT012
go
	
create table GesPet.dbo.HEDT012 (
	tipo_id			char(1)			not null,
	subtipo_id		char(1)			not null,
	subtipo_nom		varchar(50)		null,
	rutina			char(8)			null,
	constraint PK_HEDT0121 PRIMARY KEY CLUSTERED (tipo_id, subtipo_id )
	)
go

print 'Tabla modificada exitosamente.'