print 'Creando / modificando la tabla: hHorasTrabajadas... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'hHorasTrabajadas' and type = 'U')
	drop table GesPet.dbo.hHorasTrabajadas
go
	
create table GesPet.dbo.hHorasTrabajadas (
	cod_recurso		char(10)		not null,
	cod_tarea		char(8)			not null,
	pet_nrointerno  int				not null,
	fe_desde		smalldatetime   not null,
	fe_hasta		smalldatetime   null,
	horas			smallint		not null,
	trabsinasignar  char(1)			null,
	observaciones   varchar(255)    null,
	audit_user		char(10)		not null,
	audit_date		smalldatetime   not null,
	constraint PK_hHorasTrabajadas PRIMARY KEY CLUSTERED (cod_recurso, cod_tarea, pet_nrointerno, fe_desde)
	)
go

create nonclustered index idx_hHorasTrbPet
on GesPet.dbo.hHorasTrabajadas (pet_nrointerno, cod_recurso, fe_desde) on 'default'
go

create nonclustered index idx_hHorasTrbRec
on GesPet.dbo.hHorasTrabajadas (cod_recurso, pet_nrointerno, cod_tarea, fe_desde) on 'default'
go

create nonclustered index idx_hHorasTrbTar
on GesPet.dbo.hHorasTrabajadas (cod_tarea, cod_recurso, fe_desde) on 'default'
go

print 'Tabla modificada exitosamente.'