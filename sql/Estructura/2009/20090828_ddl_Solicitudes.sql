print 'Creando / modificando la tabla: Solicitudes... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Solicitudes' and type = 'U')
	drop table GesPet.dbo.Solicitudes
go
	
create table GesPet.dbo.Solicitudes (
	sol_nroasignado	int			not null,
	sol_tipo		char(1)		not null,
	sol_mask		char(1)		not null,
	sol_fecha		smalldatetime	null,
	sol_recurso		char(10)		null,
	pet_nrointerno	int			not null,
	sol_resp_ejec	char(10)		null,
	sol_resp_sect	char(10)		null,
	sol_seguridad	char(10)		null,
	jus_codigo		int				null,
	sol_texto		varchar(255)	null,
	sol_fe_auto1	smalldatetime	null,
	sol_fe_auto2	smalldatetime	null,
	sol_fe_auto3	smalldatetime	null,
	sol_file_prod	varchar(255)	null,
	sol_file_desa	varchar(255)	null,
	sol_file_out	varchar(255)	null,
	sol_lib_Sysin	varchar(255)	null,
	sol_mem_Sysin	varchar(255)	null,
	sol_join		char(1)			null,
	sol_nrotabla	char(4)			null,
	fe_formato		char(27)		null,
	sol_estado		char(1)			null,
	sol_trnm_fe		smalldatetime	null,
	sol_trnm_usr	char(10)		null,
	constraint PK_Solicitudes PRIMARY KEY CLUSTERED (sol_nroasignado )
	)
go

print 'Tabla creada / modificada exitosamente.'