print 'Modificando la tabla Peticion...'
go

use GesPet
go

alter table GesPet.dbo.Recurso
	add fe_seguridad 	smalldatetime		NULL
go


-- Recurso (creaci�n de �ndices)
create nonclustered index idx_Recurso1 on dbo.Recurso (cod_recurso, estado_recurso) on 'default'
go
create nonclustered index idx_Recurso2 on dbo.Recurso (nom_recurso, estado_recurso) on 'default'
go
create nonclustered index idx_Recurso3 on dbo.Recurso (cod_direccion, nom_recurso, estado_recurso) on 'default'
go
create nonclustered index idx_Recurso4 on dbo.Recurso (cod_gerencia, nom_recurso, estado_recurso) on 'default'
go
create nonclustered index idx_Recurso5 on dbo.Recurso (cod_sector, nom_recurso, estado_recurso) on 'default'
go
create nonclustered index idx_Recurso6 on dbo.Recurso (cod_grupo, nom_recurso, estado_recurso) on 'default'
go
