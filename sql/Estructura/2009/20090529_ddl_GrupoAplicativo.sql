print 'Creando / modificando la tabla: GrupoAplicativo... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'GrupoAplicativo' and type = 'U')
	drop table GesPet.dbo.GrupoAplicativo
go
	
create table GesPet.dbo.GrupoAplicativo (
	cod_grupo		char(8)		not null,
	app_id			char(30)	not null,
	constraint PK_GrupoAplicativo1 PRIMARY KEY CLUSTERED (cod_grupo, app_id )
	)
go

print 'Tabla modificada exitosamente.'