-- Historial (modificación de índices)
drop index Historial.idx_Historial
go

drop index Historial.idx_PetHistorial
go

drop index Historial.idx_Historial1
go

create unique clustered index idx_PetHistorial on GesPet..Historial (pet_nrointerno, hst_nrointerno desc) on 'default'
go

create nonclustered index idx_Historial on GesPet..Historial (hst_nrointerno) on 'default'
go

create nonclustered index idx_Historial1 on GesPet..Historial (pet_nrointerno, hst_fecha desc) on 'default'
go

