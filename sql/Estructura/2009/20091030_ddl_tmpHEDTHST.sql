print 'Creando / modificando la tabla: tmpHEDTHST... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'tmpHEDTHST' and type = 'U')
	drop table GesPet.dbo.HEDTHST
go
	
create table GesPet.dbo.tmpHEDTHST (
	HEDT0_table				char(8)			null,		-- Tabla de origen
	HEDT0_dsn_id			char(8)			null,		-- Id. de archivo
	HEDT0_cpy_id			char(8)			null,		-- Id. de copy
	HEDT0_cpo_id			char(18)		null,		-- Id. de campo
	HEDT1_dsn_nom			char(50)		null,		-- Nombre del archivo
	HEDT1_dsn_desc			char(50)		null,		-- Descripci�n del archivo
	HEDT1_dsn_enmas			char(1)			null,		-- Datos enmascarables (S/N)
	HEDT1_dsn_nomrut		char(8)			null,		-- Nombre de la rutina de enmascaramiento
	HEDT1_dsn_feult			char(8)			null,		-- Fecha de �ltima modificaci�n
	HEDT1_dsn_userid		char(8)			null,		-- Usuario que realiz� la �ltima modificaci�n
	HEDT1_dsn_vb			char(1)			null,		-- Visado por SSII
	HEDT1_dsn_vb_fe			char(8)			null,		-- Fecha de visado por SSII
	HEDT1_dsn_vb_userid		char(8)			null,		-- Usuario de SSII que vis�
	HEDT1_app_id			char(30)		null,		-- Aplicativo asociado al archivo
	HEDT2_cpy_dsc			char(50)		null,		-- Descripci�n del copy
	HEDT2_cpy_nomrut		char(8)			null,		-- Nombre de la rutina de enmascaramiento del copy
	HEDT2_cpy_feult			char(8)			null,		-- Fecha de de �ltima modificaci�n
	HEDT2_cpy_userid		char(10)		null,		-- Usuario que realiz� la �ltima modificaci�n
	HEDT3_cpo_nrobyte		char(4)			null,		-- Posici�n desde
	HEDT3_cpo_canbyte		char(4)			null,		-- Longitud del campo
	HEDT3_cpo_type			char(1)			null,		-- Tipo de dato
	HEDT3_cpo_decimals		char(4)			null,		-- Decimales
	HEDT3_cpo_dsc			char(20)		null,		-- Descripci�n del campo
	HEDT3_cpo_nomrut		char(8)			null,		-- Nombre de la rutina
	HEDT3_cpo_feult			char(8)			null,		-- Fecha de de �ltima modificaci�n
	HEDT3_cpo_userid		char(10)		null)		-- Usuario que realiz� la �ltima modificaci�n
go

print 'Tabla modificada exitosamente.'