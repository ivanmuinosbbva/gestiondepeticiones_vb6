Print 'Creando / modificando la tabla: HEDT001... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'HEDT001' and type = 'U')
	drop table GesPet.dbo.HEDT001
go
	
create table GesPet.dbo.HEDT001 (
	dsn_id			char(8)			not null,	-- Id. de archivo
	dsn_nom			char(50)		null,		-- Nombre del archivo
	dsn_desc		char(50)		null,		-- Descripci�n del archivo
	dsn_enmas		char(1)			null,		-- Datos enmascarables (S/N)
	dsn_nomrut		char(8)			null,		-- Nombre de la rutina de enmascaramiento
	dsn_feult		smalldatetime	null,		-- Fecha de �ltima modificaci�n
	dsn_userid		char(8)			null,		-- Usuario que realiz� la �ltima modificaci�n
	dsn_vb			char(1)			null,		-- Visado por SSII
	dsn_vb_fe		smalldatetime	null,		-- Fecha de visado por SSII
	dsn_vb_userid	char(8)			null,		-- Usuario de SSII que vis�
	app_id			char(30)		null,		-- Aplicativo asociado al archivo
	estado			char(1)			null,		-- Estado (Pendiente:A / En proceso de envio:B / Enviado a HOST:3)
	fe_hst_estado	smalldatetime	null,		-- Fecha y hora de envio a HOST 
	constraint PK_HEDT0011 PRIMARY KEY CLUSTERED (dsn_id )
	)
go

print 'Tabla modificada exitosamente.'
go

create nonclustered index idx_HEDT0011 on dbo.HEDT001 (dsn_nom) on 'default'
go

print 'Creaci�n del �ndice exitosa.'