print 'Creando / modificando la tabla: hPeticionMemo... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'hPeticionMemo' and type = 'U')
	drop table GesPet.dbo.hPeticionMemo
go
	
create table GesPet.dbo.hPeticionMemo (
	pet_nrointerno		int			not null,
	mem_campo			char(10)    not null,
	mem_secuencia		smallint    not null,
	mem_texto			char(255)   not null,
	constraint PK_hPeticionMemo PRIMARY KEY CLUSTERED (pet_nrointerno, mem_campo, mem_secuencia)
	)
go

print 'Tabla modificada exitosamente.'