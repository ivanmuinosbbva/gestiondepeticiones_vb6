print 'Creando / modificando la tabla: Justificativos... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Justificativos' and type = 'U')
	drop table GesPet.dbo.Justificativos
go
	
create table GesPet.dbo.Justificativos (
	jus_codigo		int				not null,
	jus_descripcion	varchar(255)	null,
	jus_hab			char(1)			null,
	constraint PK_Justificativos PRIMARY KEY CLUSTERED (jus_codigo )
	)
go

print 'Tabla creada / modificada exitosamente.'