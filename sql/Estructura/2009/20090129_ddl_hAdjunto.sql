print 'Creando / modificando la tabla: hAdjunto... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'hAdjunto' and type = 'U')
	drop table GesPet.dbo.hAdjunto
go
	
create table GesPet.dbo.hAdjunto (
	pet_nrointerno  int				not null,
	adj_file		char(50)		not null,
	adj_path		char(250)		not null,
	adj_texto		char(250)		not null,
	audit_user		char(10)		not null,
	audit_date		smalldatetime   not null,
	constraint PK_hAdjunto PRIMARY KEY CLUSTERED (pet_nrointerno, adj_file)
	)
go

print 'Tabla modificada exitosamente.'