print 'Creando / modificando la tabla: Aplicativo... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Aplicativo' and type = 'U')
	drop table GesPet.dbo.Aplicativo
go
	
create table GesPet.dbo.Aplicativo (
	app_id			char(30)		not null,
	app_nombre		char(100)		null,
	app_hab			char(1)			null,
	cod_direccion	char(8)			null,
	cod_gerencia	char(8)			null,
	cod_sector		char(8)			null,
	cod_grupo		char(8)			null,
	cod_r_direccion	char(10)		null,
	cod_r_gerencia	char(10)		null,
	cod_r_sector	char(10)		null,
	cod_r_grupo		char(10)		null,
	app_amb			char(1)			null,
	app_abrev		char(10)		null,
	app_fe_alta		smalldatetime	null,
	app_fe_lupd		smalldatetime	null,
	constraint PK_Aplicativo1 PRIMARY KEY CLUSTERED (app_id )
	)
go

print 'Tabla modificada exitosamente.'