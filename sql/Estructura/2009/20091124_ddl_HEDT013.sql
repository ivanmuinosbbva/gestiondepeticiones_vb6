Print 'Creando / modificando la tabla: HEDT013... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'HEDT013' and type = 'U')
	drop table GesPet.dbo.HEDT013
go
	
create table GesPet.dbo.HEDT013 (
	tipo_id			char(1)			not null,
	subtipo_id		char(1)			not null,
	item			int				not null,
	item_nom		varchar(50)		null,
	constraint PK_HEDT0131 PRIMARY KEY CLUSTERED (tipo_id, subtipo_id, item)
	)
go

print 'Tabla modificada exitosamente.'