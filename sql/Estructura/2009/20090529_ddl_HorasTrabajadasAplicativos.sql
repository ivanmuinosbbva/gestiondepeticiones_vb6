print 'Creando / modificando la tabla: HorasTrabajadasAplicativo... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'HorasTrabajadasAplicativo' and type = 'U')
	drop table GesPet.dbo.HorasTrabajadasAplicativo
go
	
create table GesPet.dbo.HorasTrabajadasAplicativo (
	cod_recurso		char(10)		not null,
	cod_tarea		char(8)			not null,
	pet_nrointerno  int				not null,
	fe_desde		smalldatetime	not null,
	fe_hasta		smalldatetime	not null,
	horas			smallint		not null,
	app_id			char(30)		not null,
	app_horas		smallint		null,
	hsapp_texto		varchar(255)	null,
	constraint PK_HsTrabApp1 PRIMARY KEY CLUSTERED (cod_recurso, cod_tarea, pet_nrointerno, fe_desde, fe_hasta, horas, app_id )
	)
go

print 'Tabla modificada exitosamente.'