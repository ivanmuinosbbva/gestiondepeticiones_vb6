print 'Creando / modificando la tabla: hPeticionRecurso... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'hPeticionRecurso' and type = 'U')
	drop table GesPet.dbo.hPeticionRecurso
go
	
create table GesPet.dbo.hPeticionRecurso (
	pet_nrointerno		int				not null,
	cod_recurso			char(10)		not null,
	cod_grupo			char(8)			not null,
	cod_sector			char(8)			not null,
	cod_gerencia		char(8)			not null,
	cod_direccion		char(8)			not null,
	audit_user			char(10)		null,
	audit_date			smalldatetime   not null,
	constraint PK_hPeticionRecurso PRIMARY KEY CLUSTERED (pet_nrointerno, cod_sector, cod_grupo, cod_recurso)
	)
go

print 'Tabla modificada exitosamente.'