print 'Creando / modificando la tabla: tmpSolicitudes... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'tmpSolicitudes' and type = 'U')
	drop table GesPet.dbo.tmpSolicitudes
go
	
create table GesPet.dbo.tmpSolicitudes (
	sol_nroasignado	int				not null,
	sol_tipo		char(1)			null,
	sol_mask		char(1)			null,
	sol_fecha		smalldatetime	null,
	sol_recurso		char(10)		null,
	nom_recurso		char(30)		null,
	pet_nrointerno	int				null,
	sol_resp_ejec	char(10)		null,
	nom_resp_ejec	char(30)		null,
	sol_resp_sect	char(10)		null,
	nom_resp_sect	char(30)		null,
	sol_seguridad	char(10)		null,
	nom_seguridad	char(30)		null,
	sol_fe_auto1	smalldatetime	null,
	sol_fe_auto2	smalldatetime	null,
	sol_fe_auto3	smalldatetime	null,
	sol_file_prod	varchar(255)	null,
	sol_file_desa	varchar(255)	null,
	sol_file_out	varchar(255)	null,
	sol_lib_Sysin	varchar(255)	null,
	sol_mem_Sysin	varchar(255)	null,
	sol_join		char(1)			null,
	sol_nrotabla	char(4)			null,
	fe_formato		char(27)		null,
	constraint PK_tmpSolicitudes PRIMARY KEY CLUSTERED (sol_nroasignado )
	)
go

print 'Tabla creada / modificada exitosamente.'