print 'Creando / modificando la tabla: HEDT002... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'HEDT002' and type = 'U')
	drop table GesPet.dbo.HEDT002
go
	
create table GesPet.dbo.HEDT002 (
	dsn_id			char(8)			not null,	-- Id. de archivo
	cpy_id			char(8)			not null,	-- Id. de copy
	cpy_dsc			varchar(50)		null,		-- Descripci�n del copy
	cpy_nomrut		char(8)			null,		-- Nombre de la rutina de enmascaramiento del copy
	cpy_feult		smalldatetime	null,		-- Fecha de de �ltima modificaci�n
	cpy_userid		char(10)		null,		-- Usuario que realiz� la �ltima modificaci�n
	estado			char(1)			null,		-- Estado (Pendiente:A / En proceso de envio:B / Enviado a HOST:3)
	fe_hst_estado	smalldatetime	null,		-- Fecha y hora de envio a HOST 
	constraint PK_HEDT0021 PRIMARY KEY CLUSTERED (dsn_id, cpy_id )
	)
go

print 'Tabla modificada exitosamente.'