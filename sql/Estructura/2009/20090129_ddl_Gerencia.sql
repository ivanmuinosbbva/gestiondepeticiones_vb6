-- Gerencia (creaci�n de �ndices)
create nonclustered index idx_Gerencia2 on dbo.Gerencia (cod_direccion, cod_gerencia) on 'default'
go
create nonclustered index idx_Gerencia3 on dbo.Gerencia (cod_gerencia, flg_habil) on 'default'
go
create nonclustered index idx_Gerencia4 on dbo.Gerencia (cod_direccion, cod_gerencia, flg_habil) on 'default'
go
