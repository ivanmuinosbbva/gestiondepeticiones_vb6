print 'Creando / modificando la tabla: hPeticion... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'hPeticion' and type = 'U')
	drop table GesPet.dbo.hPeticion
go
	
create table GesPet.dbo.hPeticion (
	pet_nrointerno		int				not null,
	pet_nroasignado		int				not null,
	titulo				char(50)		not null,
	cod_tipo_peticion   char(3)			not null,
	prioridad			char(1)			not null,
	pet_nroanexada		int				not null,
	fe_pedido			smalldatetime   null,
	fe_requerida		smalldatetime   null,
	fe_comite			smalldatetime   null,
	fe_ini_plan			smalldatetime   null,
	fe_fin_plan			smalldatetime   null,
	fe_ini_real			smalldatetime   null,
	fe_fin_real			smalldatetime   null,
	horaspresup			smallint		not null,
	cod_direccion		char(8)			not null,
	cod_gerencia		char(8)			not null,
	cod_sector			char(8)			not null,
	cod_usualta			char(10)		not null,
	cod_solicitante		char(10)		not null,
	cod_referente		char(10)		not null,
	cod_supervisor		char(10)		not null,
	cod_director		char(10)		not null,
	cod_estado			char(6)			not null,
	fe_estado			smalldatetime   null,
	cod_situacion		char(6)			not null,
	ult_accion			char(8)			not null,
	audit_user			char(10)		null,
	audit_date			smalldatetime   not null,
	corp_local			char(1)			null,
	cod_orientacion		char(2)			null,
	importancia_cod		char(2)			null,
	importancia_prf		char(4)			null,
	importancia_usr		char(10)		null,
	fe_fec_plan			smalldatetime   null,
	fe_ini_orig			smalldatetime   null,
	fe_fin_orig			smalldatetime   null,
	fe_fec_orig			smalldatetime   null,
	cant_planif			int				null,
	prj_nrointerno		int				not null,
	byp_comite			char(1) 		not null,
	cod_bpar			char(10)		not null,
	cod_clase			char(4)			not null,
	pet_imptech			char(1)			not null,
	pet_sox001			tinyint			not null,
	pet_regulatorio		char(1)			null,
	pet_projid			int				null,
	pet_projsubid		int				null,
	pet_projsubsid		int				null,
	constraint PK_hPeticion PRIMARY KEY CLUSTERED (pet_nrointerno )
	)
go

create nonclustered index idx_hEstFecPet
on GesPet.dbo.Peticion (cod_estado, fe_estado, pet_nrointerno ) on 'default'
go

create nonclustered index idx_hEstPedPet
on GesPet.dbo.Peticion (cod_estado, fe_pedido, pet_nrointerno ) on 'default'
go

print 'Tabla modificada exitosamente.'