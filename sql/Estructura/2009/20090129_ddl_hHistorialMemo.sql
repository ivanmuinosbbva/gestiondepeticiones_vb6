print 'Creando / modificando la tabla: hHistorialMemo... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'hHistorialMemo' and type = 'U')
	drop table GesPet.dbo.hHistorialMemo
go
	
create table GesPet.dbo.hHistorialMemo (
	hst_nrointerno  int			not null,
	hst_secuencia   smallint    not null,
	mem_texto		char(255)   null,
	constraint PK_hHistorialMemo PRIMARY KEY CLUSTERED (hst_nrointerno, hst_secuencia)
	)
go

print 'Tabla modificada exitosamente.'