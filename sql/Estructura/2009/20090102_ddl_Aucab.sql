print 'Creando la tabla: Aucab'
go

create table dbo.Aucab (
	cod_recurso		char(10)		not null,
	fe_ingreso		smalldatetime	not null,
	hr_ingreso		char(8)			not null,
	fe_egreso		smalldatetime   null,
	cod_equipo		char(20)		null,
	cod_usuario		char(10)		null,
	constraint PK_Aucab PRIMARY KEY CLUSTERED 
		(cod_recurso, fe_ingreso, hr_ingreso )
)

go

print 'Creaci�n de tabla finalizada.'
go