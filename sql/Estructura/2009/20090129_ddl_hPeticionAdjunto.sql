print 'Creando / modificando la tabla: hPeticionAdjunto... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'hPeticionAdjunto' and type = 'U')
	drop table GesPet.dbo.hPeticionAdjunto
go
	
create table GesPet.dbo.hPeticionAdjunto (
	pet_nrointerno  int				not null,
	adj_tipo		char(8)			not null,
	adj_file		char(100)		not null,
	adj_texto		char(250)		not null,
	adj_objeto		image			not null,
	audit_user		char(10)		not null,
	audit_date		smalldatetime   not null,
	cod_sector		char(8)			null,
	cod_grupo		char(8)			null, 
	constraint PK_hPeticionAdjunto PRIMARY KEY CLUSTERED (pet_nrointerno, adj_tipo, adj_file)
	)
go

grant delete on hPeticionAdjunto to GesPetUsr 
go

grant insert on hPeticionAdjunto to GesPetUsr 
go

grant select on hPeticionAdjunto to GesPetUsr 
go

grant update on hPeticionAdjunto to GesPetUsr 
go

print 'Tabla modificada exitosamente.'
