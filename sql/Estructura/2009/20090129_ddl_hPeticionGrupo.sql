print 'Creando / modificando la tabla: hPeticionGrupo... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'hPeticionGrupo' and type = 'U')
	drop table GesPet.dbo.hPeticionGrupo
go
	
create table GesPet.dbo.hPeticionGrupo (
	pet_nrointerno		int				not null,
	cod_grupo			char(8)			not null,
	cod_sector			char(8)			not null,
	cod_gerencia		char(8)			not null,
	cod_direccion		char(8)			not null,
	fe_ini_plan			smalldatetime   null,
	fe_fin_plan			smalldatetime   null,
	fe_ini_real			smalldatetime   null,
	fe_fin_real			smalldatetime   null,
	horaspresup			smallint		not null,
	cod_estado			char(6)			not null,
	fe_estado			smalldatetime   null,
	cod_situacion		char(6)			not null,
	ult_accion			char(8)			not null,
	hst_nrointerno_sol  int				not null,
	hst_nrointerno_rsp  int				not null,
	audit_user			char(10)		null,
	audit_date			smalldatetime   not null,
	fe_fec_plan			smalldatetime   null,
	fe_ini_orig			smalldatetime   null,
	fe_fin_orig			smalldatetime   null,
	fe_fec_orig			smalldatetime   null,
	cant_planif			int				null,
	prio_ejecuc			int				null,
	info_adicio			char(250)		null,
	fe_produccion		smalldatetime   null,
	fe_suspension		smalldatetime   null,
	cod_motivo			int				null,
	constraint PK_hPeticionGrupo PRIMARY KEY CLUSTERED (pet_nrointerno, cod_sector, cod_grupo)
	)
go

print 'Tabla modificada exitosamente.'