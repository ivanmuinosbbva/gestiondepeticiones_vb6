print 'Creando / modificando la tabla: hPeticionConf... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'hPeticionConf' and type = 'U')
	drop table GesPet.dbo.hPeticionConf
go
	
create table GesPet.dbo.hPeticionConf (
	pet_nrointerno  int				not null,
	ok_tipo			char(8)			not null,
	ok_modo			char(3)			not null,
	ok_texto		char(250)		not null,
	audit_user		char(10)		not null,
	audit_date		smalldatetime   not null,
	ok_secuencia    int				not null,
	constraint PK_hPeticionConf PRIMARY KEY CLUSTERED (pet_nrointerno, ok_tipo, ok_secuencia )
	)
go

print 'Tabla modificada exitosamente.'