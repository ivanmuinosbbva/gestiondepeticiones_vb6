print 'Creando / modificando la tabla: HEDT003... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'HEDT003' and type = 'U')
	drop table GesPet.dbo.HEDT003
go
	
create table GesPet.dbo.HEDT003 (
	dsn_id			char(8)			not null,	-- Id. de archivo
	cpy_id			char(8)			not null,	-- Id. de copy
	cpo_id			char(18)		not null,	-- Id. de campo
	cpo_nrobyte		char(4)			null,		-- Posici�n desde
	cpo_canbyte		char(4)			null,		-- Longitud del campo
	cpo_type		char(1)			null,		-- Tipo de dato
	cpo_decimals	char(4)			null,		-- Decimales
	cpo_dsc			varchar(50)		null,		-- Descripci�n del campo
	cpo_nomrut		char(8)			null,		-- Nombre de la rutina
	cpo_feult		smalldatetime	null,		-- Fecha de de �ltima modificaci�n
	cpo_userid		char(10)		null,		-- Usuario que realiz� la �ltima modificaci�n
	estado			char(1)			null,		-- Estado (Pendiente:A / En proceso de envio:B / Enviado a HOST:3)
	fe_hst_estado	smalldatetime	null,		-- Fecha y hora de envio a HOST 
	constraint PK_HEDT0031 PRIMARY KEY CLUSTERED (dsn_id, cpy_id, cpo_id)
	)
go

print 'Tabla modificada exitosamente.'