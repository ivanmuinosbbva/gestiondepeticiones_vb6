print 'Creando / modificando la tabla: hHistorial... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'hHistorial' and type = 'U')
	drop table GesPet.dbo.hHistorial
go
	
create table GesPet.dbo.hHistorial (
	pet_nrointerno  int				not null,
	hst_nrointerno  int				not null,
	hst_fecha		smalldatetime   not null,
	cod_evento		varchar(8)		null,
	pet_estado		varchar(6)		null,
	cod_sector		char(8)			null,
	sec_estado		varchar(6)		null,
	cod_grupo		char(8)			null,
	gru_estado		varchar(6)		null,
	replc_user		varchar(10)		null,
	audit_user		varchar(10)		null,
	constraint PK_hHistorial PRIMARY KEY CLUSTERED (pet_nrointerno, hst_nrointerno, hst_fecha)
	)
go

create nonclustered index idx_hHistorial1
on GesPet.dbo.Historial (pet_nrointerno, hst_fecha desc) on 'default'
go

print 'Tabla modificada exitosamente.'