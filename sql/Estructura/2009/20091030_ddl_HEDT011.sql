Print 'Creando / modificando la tabla: HEDT011... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'HEDT011' and type = 'U')
	drop table GesPet.dbo.HEDT011
go
	
create table GesPet.dbo.HEDT011 (
	tipo_id			char(1)			not null,
	tipo_nom		varchar(50)		null,
	constraint PK_HEDT0111 PRIMARY KEY CLUSTERED (tipo_id )
	)
go

print 'Tabla modificada exitosamente.'