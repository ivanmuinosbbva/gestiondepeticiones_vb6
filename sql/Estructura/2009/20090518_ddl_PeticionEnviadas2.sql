print 'Creando / modificando la tabla: PeticionEnviadas2... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'PeticionEnviadas2' and type = 'U')
	drop table GesPet.dbo.PeticionEnviadas2
go
	
create table GesPet.dbo.PeticionEnviadas2 (
	pet_nrointerno  int				not null,
	pet_sector		char(8)			not null,
	pet_grupo		char(8)			not null,
	pet_record		char(80)		null,
	pet_usrid		char(10)		null,
	pet_date		smalldatetime   null,
	pet_done		char(1)			null,
	constraint PK_PetEnv2 PRIMARY KEY CLUSTERED (pet_nrointerno, pet_sector, pet_grupo )
	)
go

print 'Tabla creada/modificada exitosamente.'
go
