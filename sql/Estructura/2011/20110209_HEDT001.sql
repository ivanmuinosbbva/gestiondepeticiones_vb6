print 'Modificando la tabla : HEDT001 (creando �ndice)'
go

if exists (select * from sysindexes where name = 'idx_HEDT0012')
	drop index HEDT001.idx_HEDT0012
go

create nonclustered index idx_HEDT0012 on GesPet.dbo.HEDT001 
	(dsn_userid asc)
go

print 'Listo.'
go
