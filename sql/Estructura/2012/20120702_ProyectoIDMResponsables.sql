-- �reas responsables por proyecto IDM
print 'Creando / modificando la tabla: ProyectoIDMResponsables... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'ProyectoIDMResponsables' and type = 'U')
	drop table GesPet.dbo.ProyectoIDMResponsables
go
	
create table GesPet.dbo.ProyectoIDMResponsables (
	ProjId			int				not null,
	ProjSubId		int				not null,
	ProjSubSId		int				not null,
	cod_nivel		char(4)			not null,
	cod_area		char(8)			not null,
	propietaria		char(1)				null,	-- S/N: �rea propietaria
	resp_user		char(10)			null,	-- Usuario que agreg� el �rea responsable
	resp_fecha		smalldatetime		null,	-- Fecha en que se agreg� el �rea responsable
	constraint PK_ProyIDMResponsables PRIMARY KEY NONCLUSTERED (ProjId,ProjSubId,ProjSubSId,cod_nivel,cod_area))
go

print 'Tabla modificada exitosamente.'
go