/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Gerencia'
go

if exists (select * from sysobjects where name = 'vw_Gerencia' and sysstat & 7 = 2)
	drop view dbo.vw_Gerencia
go

create view dbo.vw_Gerencia
as 
	select
		'1' as 'pet_empid',
		a.cod_gerencia,
		nom_gerencia = (str_replace(str_replace(str_replace(str_replace(str_replace(a.nom_gerencia, '|',''), char(13),''), char(10),''), char(9),''), char(7),'')),
		a.cod_direccion,
		a.flg_habil,
		a.es_ejecutor
	from
		GesPet..Gerencia a
go

grant select on dbo.vw_Gerencia to GesPetUsr
go

print 'Actualización realizada.'
go
