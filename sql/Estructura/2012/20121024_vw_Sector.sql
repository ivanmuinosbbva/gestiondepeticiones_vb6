/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Sector'
go

if exists (select * from sysobjects where name = 'vw_Sector' and sysstat & 7 = 2)
	drop view dbo.vw_Sector
go

create view dbo.vw_Sector
as 
	select
		'1' as 'pet_empid',
		a.cod_sector,
		nom_sector = (str_replace(str_replace(str_replace(str_replace(str_replace(a.nom_sector, '|',''), char(13),''), char(10),''), char(9),''), char(7),'')),
		a.cod_gerencia,
		a.flg_habil,
		a.cod_abrev,
		a.es_ejecutor,
		a.cod_bpar
	from
		GesPet..Sector a
go

grant select on dbo.vw_Sector to GesPetUsr
go

print 'Actualización realizada.'
