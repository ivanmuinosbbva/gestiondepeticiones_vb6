print 'Borrando constraint clustered: PKPetPlandet1...'
go
alter table GesPet..PeticionPlandet
	drop constraint PKPetPlandet1
go
print 'Creando �ndice non clustered: idx_PetPlandet1...'
go
create unique nonclustered index idx_PetPlandet1 on GesPet..PeticionPlandet (per_nrointerno, cod_grupo, pet_nrointerno) on 'default'
go
print 'Proceso finalizado.'
go
