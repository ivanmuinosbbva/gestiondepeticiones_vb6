/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Grupo'
go

if exists (select * from sysobjects where name = 'vw_Grupo' and sysstat & 7 = 2)
	drop view dbo.vw_Grupo
go

create view dbo.vw_Grupo
as 
	select
		'1' as 'pet_empid',
		a.cod_grupo,
		nom_grupo = (str_replace(str_replace(str_replace(str_replace(str_replace(a.nom_grupo, '|',''), char(13),''), char(10),''), char(9),''), char(7),'')),
		a.cod_sector,
		a.flg_habil,
		a.es_ejecutor,
		a.cod_bpar,
		a.grupo_homologacion
	from
		GesPet..Grupo a
go

grant select on dbo.vw_Grupo to GesPetUsr
go

print 'Actualización realizada.'
go
