print 'Borrando �ndice clustered: idx_PetHistorial...'
go
drop index Historial.idx_PetHistorial
go
print 'Creando �ndice non clustered: idx_PetHistorial...'
go
create unique nonclustered index idx_PetHistorial on GesPet..Historial (pet_nrointerno, hst_nrointerno) on 'default'
go
print 'Creando nuevo �ndice non clustered: idx_Historial2...'
go
create nonclustered index idx_Historial2 on GesPet..Historial (pet_nrointerno, hst_nrointerno desc) on 'default'
go
print 'Proceso finalizado.'
go