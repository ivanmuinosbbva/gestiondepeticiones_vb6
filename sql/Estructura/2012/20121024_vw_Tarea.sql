/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Tarea'
go

if exists (select * from sysobjects where name = 'vw_Tarea' and sysstat & 7 = 2)
	drop view dbo.vw_Tarea
go

create view dbo.vw_Tarea
as 
	select
		'1' as 'pet_empid',
		a.cod_tarea,
		nom_tarea = (str_replace(str_replace(str_replace(str_replace(str_replace(a.nom_tarea, '|',''), char(13),''), char(10),''), char(9),''), char(7),'')),
		a.flg_habil
	from
		GesPet..Tarea a
go

grant select on dbo.vw_Tarea to GesPetUsr
go

print 'Actualización realizada.'
