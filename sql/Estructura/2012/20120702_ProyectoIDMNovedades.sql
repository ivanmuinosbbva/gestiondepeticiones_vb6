print 'Creando / modificando la tabla: ProyectoIDMNovedades... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'ProyectoIDMNovedades' and type = 'U')
	drop table GesPet.dbo.ProyectoIDMNovedades
go
	
create table GesPet.dbo.ProyectoIDMNovedades (
	ProjId			int				not null,
	ProjSubId		int				not null,
	ProjSubSId		int				not null,
	mem_fecha		datetime		not null,
	mem_campo		char(10)		not null,
	mem_secuencia	smallint		not null,
	mem_texto		varchar(255)	null,
	cod_usuario		char(10)		null,
	mem_accion		char(3)			null,		-- Acci�n realizada (UPD: actualizaci�n; DEL: borrado)
	mem_fecha2		datetime		null,		-- Fecha en que se borr� o actualiz� el dato
	cod_usuario2	char(10)		null,		-- Usuario que realiz� este cambio
	constraint PK_ProyectoIDMNovedades PRIMARY KEY NONCLUSTERED (ProjId,ProjSubId,ProjSubSId,mem_fecha,mem_campo,mem_secuencia))
go

print 'Tabla modificada exitosamente.'
go