print 'Creando / modificando la tabla: ProyectoIDMMemo... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'ProyectoIDMMemo' and type = 'U')
	drop table GesPet.dbo.ProyectoIDMMemo
go
	
create table GesPet.dbo.ProyectoIDMMemo (
	ProjId			int			not null,
	ProjSubId		int			not null,
	ProjSubSId		int			not null,
	mem_campo		char(10)	not null,
	mem_secuencia	smallint	not null,
	mem_texto		varchar(255)	null,
	constraint PK_ProyectoIDMMemo PRIMARY KEY NONCLUSTERED (ProjId,ProjSubId,ProjSubSId,mem_campo,mem_secuencia))
go

print 'Tabla modificada exitosamente.'
go