print 'Creando / modificando la tabla: ProyectoIDMHitos... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'ProyectoIDMHitos' and type = 'U')
	drop table GesPet.dbo.ProyectoIDMHitos
go
	
create table GesPet.dbo.ProyectoIDMHitos (
	ProjId				int				not null,
	ProjSubId			int				not null,
	ProjSubSId			int				not null,
	hito_nombre			char(100)		not null,
	hito_fe_ini			smalldatetime	not null,
	hito_fe_fin			smalldatetime	null,
	hito_descripcion	char(255)		null,
	hito_estado			char(6)			null,
	cod_nivel			char(4)			null,	-- �rea responsable del hito
	cod_area			char(8)			null,
	hito_fecha			smalldatetime	null,
	hito_usuario		char(10)		null,
	constraint PK_ProyectoIDMHitos PRIMARY KEY NONCLUSTERED (ProjId,ProjSubId,ProjSubSId,hito_nombre,hito_fe_ini))
go

print 'Tabla modificada exitosamente.'
go

