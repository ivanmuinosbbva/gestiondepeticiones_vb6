print 'Borrando constraint clustered: PKPetPlancab1...'
go
alter table GesPet..PeticionPlancab 
	drop constraint PKPetPlancab1
go
print 'Creando nuevo �ndice non clustered: idx_PetPlancab1...'
go
create unique nonclustered index idx_PetPlancab1 on GesPet..PeticionPlancab (per_nrointerno, pet_nrointerno) on 'default'
go
print 'Creando nuevo �ndice non clustered: idx_PetPlancab2...'
go
create nonclustered index idx_PetPlancab2 on GesPet..PeticionPlancab (per_nrointerno, cod_bpar, prioridad, pet_nrointerno) on 'default'
go
print 'Proceso finalizado.'
go