print 'Creando / modificando la tabla: ProyectoIDMHitosEstados... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'ProyectoIDMHitosEstados' and type = 'U')
	drop table GesPet.dbo.ProyectoIDMHitosEstados
go
	
create table GesPet.dbo.ProyectoIDMHitosEstados (
	hito_estado		char(6)			not null,
	descripcion		varchar(255)	null,
	constraint PK_ProyectoIDMHitosEstados PRIMARY KEY NONCLUSTERED (hito_estado))
go

print 'Tabla modificada exitosamente.'
go