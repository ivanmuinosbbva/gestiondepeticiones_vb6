/* ESTE SP DEBE SER CONVERTIDO PARA CONSOLIDAR */
/*
-001- a. FJS 26.05.2010 - Nueva vista para
-002- a. FJS 10.05.2012 - Se agrega el campo empresa.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Peticion'
go

if exists (select * from sysobjects where name = 'vw_Peticion' and sysstat & 7 = 2)
	drop view dbo.vw_Peticion
go

create view dbo.vw_Peticion
as 
	select
		'1' as 'pet_empid',
		a.pet_nrointerno, 
		a.pet_nroasignado, 
		titulo = (str_replace(str_replace(str_replace(str_replace(str_replace(a.titulo, '|',''), char(13),''), char(10),''), char(9),''), char(7),'')),
		a.cod_tipo_peticion, 
		a.prioridad,
		prior_dsc = 
		case
			when a.prioridad = '2' then 'Muy alta'
			when a.prioridad = '3' then 'Alta'
			when a.prioridad = '4' then 'Media'
			when a.prioridad = '5' then 'Baja'
			when a.prioridad = '6' then 'Muy baja'
			else ''
		end,
		a.pet_nroanexada, 
		fe_pedido = (convert(char(8), a.fe_pedido, 112)),
		fe_requerida = (convert(char(8), a.fe_requerida, 112)),
		fe_comite = (convert(char(8), a.fe_comite, 112)),
		fe_ini_plan = (convert(char(8), a.fe_ini_plan, 112)),
		fe_fin_plan = (convert(char(8), a.fe_fin_plan, 112)),
		fe_ini_real = (convert(char(8), a.fe_ini_real, 112)),
		fe_fin_real = (convert(char(8), a.fe_fin_real, 112)),
		a.horaspresup, 
		a.cod_direccion, 
		a.cod_gerencia, 
		a.cod_sector, 
		a.cod_usualta, 
		a.cod_solicitante, 
		a.cod_referente, 
		a.cod_supervisor, 
		a.cod_director, 
		a.cod_estado, 
		fe_estado = (convert(char(8), a.fe_estado, 112)),
		a.cod_situacion, 
		nom_situacion = (select x.nom_situacion from GesPet..Situaciones x where x.cod_situacion = a.cod_situacion),
		a.ult_accion, 
		a.audit_user, 
		audit_date = (convert(char(8), a.audit_date, 112)),
		a.corp_local, 
		a.cod_orientacion, 
		nom_orientacion = (select x.nom_orientacion from GesPet..Orientacion x where x.cod_orientacion = a.cod_orientacion),
		a.importancia_cod, 
		a.importancia_prf, 
		a.importancia_usr, 
		fe_fec_plan = (convert(char(8), a.fe_fec_plan, 112)),
		fe_ini_orig = (convert(char(8), a.fe_ini_orig, 112)),
		fe_fin_orig = (convert(char(8), a.fe_fin_orig, 112)),
		fe_fec_orig = (convert(char(8), a.fe_fec_orig, 112)),
		a.cant_planif, 
		a.prj_nrointerno, 
		a.byp_comite, 
		a.cod_bpar, 
		a.cod_clase, 
		a.pet_imptech,
		a.pet_sox001, 
		a.pet_regulatorio, 
		a.pet_projid, 
		a.pet_projsubid, 
		a.pet_projsubsid,
		a.pet_emp								-- add -002- a.
	from 
		GesPet..Peticion a
go

grant select on dbo.vw_Peticion to GesPetUsr
go

print 'Actualización realizada.'
go
