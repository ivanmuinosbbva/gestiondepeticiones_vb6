print 'Creando / modificando la tabla: ProyectoIDMAdjuntos... aguarde por favor...'
go

if exists (select (1) from sysobjects where name = 'ProyectoIDMAdjuntos' and type = 'U')
	drop table GesPet.dbo.ProyectoIDMAdjuntos
go
	
create table GesPet.dbo.ProyectoIDMAdjuntos (
	ProjId			int				not null,
	ProjSubId		int				not null,
	ProjSubSId		int				not null,
	adj_tipo		char(8)			not null,
	adj_file		char(100)		not null,
	adj_texto		char(250)		NULL,
	adj_objeto		image			NULL,
	audit_user		char(10)		NULL,
	audit_date		smalldatetime	NULL,
	cod_nivel		char(4)			null,
	cod_area		char(8)			null,
	constraint PK_ProyIDMAdj PRIMARY KEY NONCLUSTERED (ProjId,ProjSubId,ProjSubSId,adj_tipo,adj_file))
go

grant select on dbo.ProyectoIDMAdjuntos to GesPetUsr
go 

grant insert on dbo.ProyectoIDMAdjuntos to GesPetUsr
go 

grant delete on dbo.ProyectoIDMAdjuntos to GesPetUsr
go 

print 'Tabla modificada exitosamente.'
go
