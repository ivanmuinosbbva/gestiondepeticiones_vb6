print 'Creando / modificando la tabla: ProyectoIDM... aguarde por favor...'
go

alter table GesPet..ProyectoIDM
	add 
		plan_tyo		char(1)			null,	-- Plan TYO
		empresa			char(1)			null,	-- Empresa: Banco o Consolidar
		area			char(1)			null,	-- Corporativo, local, regional, etc.
		clas3			char(1)			null,	-- Customer centric, Modelo de distribuci�n, Lean, etc.
		semaphore		char(1)			null,	-- Sem�foro (autom�tico)
		cod_direccion	char(8)			null,	-- Solicitante (Direcci�n)
		cod_gerencia	char(8)			null,	-- Solicitante (Gerencia)
		cod_sector		char(8)			null,	-- Solicitante (Sector)
		cod_direccion_p	char(8)			null,	-- Propietario (Direcci�n)
		cod_gerencia_p	char(8)			null,	-- Propietario (Gerencia)
		cod_sector_p	char(8)			null,	-- Propietario (Sector)
		cod_grupo_p		char(8)			null,	-- Propietario (Grupo)
		avance			int				null,	-- % de avance
		cod_estado2		char(6)			null,	-- Estado final
		fe_modif		smalldatetime	null,	-- Fecha de �ltima modificaci�n
		fe_inicio		smalldatetime	null,	-- Fecha de inicio
		fe_fin			smalldatetime	null,	-- Fecha de fin
		fe_finreprog	smalldatetime	null,	-- Fecha de reprogramada (�ltima)
		fe_ult_nove		smalldatetime	null,	-- Fecha de la �ltima novedad cargada
		semaphore2		char(1)			null,	-- Sem�foro (cambio manual)
		sema_fe_modif	smalldatetime	null,	-- Fecha de modificaci�n manual del sem�foro
		sema_usuario	char(10)		null	-- Usuario que modific� manualmente el sem�foro
go

print 'Tabla modificada exitosamente.'
go

print 'Borrando �ndice clustered unique: PK_Proj001...'
go
alter table GesPet..ProyectoIDM 
	drop constraint PK_Proj001
go
print 'Creando nuevo �ndice unique nonclustered index: PK_Proj001...'
go
create unique nonclustered index PK_Proj001 on GesPet..ProyectoIDM (ProjId, ProjSubId, ProjSubSId) on 'default'
go
print 'Proceso finalizado.'
go