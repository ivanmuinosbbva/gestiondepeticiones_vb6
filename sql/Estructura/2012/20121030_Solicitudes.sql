print 'Eliminando unique clustered index: PK_Solicitudes...'
go
alter table GesPet..Solicitudes 
	drop constraint PK_Solicitudes
go

print 'Creando nuevo índice unique nonclustered index: PK_Solicitudes1...'
go

create unique nonclustered index PK_Solicitudes1 on GesPet..Solicitudes (sol_nroasignado)
go

print 'Proceso finalizado.'
go

print 'Modificando la tabla: Solicitudes...'
go

alter table GesPet..Solicitudes
	add
		pet_nroasignado	int					null,	-- Nro. asignado de petición
		sol_prod_cpybbl	char(50)			null,	-- Biblioteca de copy del archivo productivo
		sol_prod_cpynom char(50)			null,	-- Nombre de copy del archivo productivo
		dsn_id			char(8)				null	-- ID del dsn usado
go

print 'Modificación finalizada.'
go
