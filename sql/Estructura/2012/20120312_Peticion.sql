/*
*********************************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Propósito:	Modificación de la tabla Peticion para indicar la empresa (1:Banco/2:Seguros)
* Ambiente:		SYBASE
* Fecha:		12.03.2012
*********************************************************************************************************************
*/

print 'Modificando la tabla Peticion...'
go
use GesPet
go
alter table GesPet.dbo.Peticion
	add pet_emp		int	null
go
print 'Tabla modificada.'
go
