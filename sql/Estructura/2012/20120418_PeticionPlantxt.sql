print 'Borrando constraint clustered: PKPetPlantxt1...'
go
alter table GesPet..PeticionPlantxt
	drop constraint PKPetPlantxt1
go
print 'Creando �ndice non clustered: idx_PetPlantxt1...'
go
create unique nonclustered index idx_PetPlantxt1 on GesPet..PeticionPlantxt (per_nrointerno, cod_grupo, pet_nrointerno, mem_campo, mem_secuencia) on 'default'
go
print 'Proceso finalizado.'
go
