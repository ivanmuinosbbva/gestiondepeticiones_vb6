print 'Eliminando unique clustered index: PK_HEDT0011...'
go
alter table GesPet..HEDT001 
	drop constraint PK_HEDT0011
go

print 'Creando nuevo �ndice unique nonclustered index: PK_HEDT001...'
go

create unique nonclustered index PK_HEDT001 on GesPet..HEDT001 (dsn_id)
go

print 'Creando nuevo �ndice nonclustered index: idx_HEDT0013...'
go

create nonclustered index idx_HEDT0013 on GesPet..HEDT001 (dsn_vb)
go

print 'Proceso finalizado.'
go

print 'Modificando la tabla: HEDT001...'
go

alter table GesPet..HEDT001
	add
		dsn_cpy			char(1)				null,	-- Indica si tiene COPY
		dsn_cpybbl		char(50)			null,	-- Biblioteca de copy del archivo productivo
		dsn_cpynom		char(50)			null,	-- Nombre de copy del archivo productivo
		dsn_cantuso		int					null	-- Cantidad de veces utilizado en las solicitudes
go
print 'Modificaci�n finalizada.'
go

