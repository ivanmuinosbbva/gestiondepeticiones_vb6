print 'Creando / modificando la tabla: ProyectoIDMAlertas... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'ProyectoIDMAlertas' and type = 'U')
	drop table GesPet.dbo.ProyectoIDMAlertas
go
	
create table GesPet.dbo.ProyectoIDMAlertas (
	ProjId			int				not null,
	ProjSubId		int				not null,
	ProjSubSId		int				not null,
	alert_itm		int				not null,
	alert_tipo		char(1)			null,
	alert_desc		varchar(180)	null,
	accion_id		varchar(180)	null,
	alert_fe_ini	smalldatetime	null,
	alert_fe_fin	smalldatetime	null,
	responsables	varchar(180)	null,
	observaciones	varchar(180)	null,
	alert_status	char(1)			null,		-- Estado del alerta: S: Resuelta / N: No resuelta
	alert_fecres	smalldatetime	null,		-- Fecha de resolución
	alert_fecha		smalldatetime	null,
	alert_usuario	char(10)		null,
	constraint PK_ProyectoIDMAlertas PRIMARY KEY NONCLUSTERED (ProjId,ProjSubId,ProjSubSId,alert_itm))
go

print 'Tabla modificada exitosamente.'
go