/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Direccion'
go

if exists (select * from sysobjects where name = 'vw_Direccion' and sysstat & 7 = 2)
	drop view dbo.vw_Direccion
go

create view dbo.vw_Direccion
as 
	select
		'1' as 'pet_empid',
		a.cod_direccion,
		nom_direccion = (str_replace(str_replace(str_replace(str_replace(str_replace(a.nom_direccion, '|',''), char(13),''), char(10),''), char(9),''), char(7),'')),
		a.flg_habil,
		a.es_ejecutor
	from
		GesPet..Direccion a
go

grant select on dbo.vw_Direccion to GesPetUsr
go

print 'Actualización realizada.'
