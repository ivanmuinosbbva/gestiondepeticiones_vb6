/*
*********************************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Propósito:	Modificación de la tabla Peticion para identificar las peticiones vinculadas a la mitigación de factores
*				de riesgo operacional (RO) y la prioridad del factor.
* Ambiente:		SYBASE
* Fecha:		11.07.2012
*********************************************************************************************************************
*/

print 'Modificando la tabla Peticion...'
go
use GesPet
go
alter table GesPet.dbo.Peticion
	add pet_ro		char(1)		null
go
print 'Tabla modificada.'
go
