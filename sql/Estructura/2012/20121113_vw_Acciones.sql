/*
-001- a. FJS 13.11.2012 - Nueva vista para listar las acciones o eventos del sistema.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Acciones'
go

if exists (select * from sysobjects where name = 'vw_Acciones' and sysstat & 7 = 2)
	drop view dbo.vw_Acciones
go

create view dbo.vw_Acciones
as 
	select
		'1' as 'pet_empid',
		a.cod_accion,
		a.nom_accion,
		a.IGM_hab
	from GesPet..Acciones a
go

grant select on dbo.vw_Acciones to GesPetUsr
go

print 'Actualización realizada.'
go
