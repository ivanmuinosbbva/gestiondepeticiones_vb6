print 'Creando / modificando la tabla: AccionesIDM... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'AccionesIDM' and type = 'U')
	drop table GesPet.dbo.AccionesIDM
go
	
create table GesPet.dbo.AccionesIDM (
	cod_accion  char(8)	not NULL,
	nom_accion  varchar(100) NULL,
	constraint PKAccIDM1 PRIMARY KEY NONCLUSTERED (cod_accion))
go

print 'Tabla modificada exitosamente.'
go
