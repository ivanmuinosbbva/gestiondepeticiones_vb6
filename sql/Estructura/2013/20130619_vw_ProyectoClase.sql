/*
-001- a. FJS 18.06.2013 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_ProyectoClase'
go

if exists (select * from sysobjects where name = 'vw_ProyectoClase' and sysstat & 7 = 2)
	drop view dbo.vw_ProyectoClase
go

create view dbo.vw_ProyectoClase
as 
	select
		'1' as 'pet_empid',
		a.ProjCatId,
		a.ProjClaseId,
		a.ProjClaseNom
	from
		GesPet..ProyectoClase a
go

grant select on dbo.vw_ProyectoClase to GesPetUsr
go

print 'Actualización realizada.'
go
