/*
-001- a. FJS 26.05.2010 - Nueva vista para visualizar proyectos IDM.
-002- a. FJS 12.10.2011 - Se actualizan los datos de la vista para ver todos los campos de Proyectos IDM.

*/

use GesPet
go

print 'Creando/actualizando vista: vw_ProyectoIDM'
go

if exists (select * from sysobjects where name = 'vw_ProyectoIDM' and sysstat & 7 = 2)
	drop view dbo.vw_ProyectoIDM
go

create view dbo.vw_ProyectoIDM
as 
	select
		'1' as 'pet_empid',
		a.ProjId,
		a.ProjSubId,
		a.ProjSubSId,
		a.ProjNom,
		ProjFchAlta = (convert(char(8), a.ProjFchAlta, 112)),
		a.ProjCatId,
		ProjCatNom = (select x.ProjCatNom from GesPet..ProyectoCategoria x where x.ProjCatId = a.ProjCatId),
		a.ProjClaseId,
		ProjClaseNom = (select x.ProjClaseNom from GesPet..ProyectoClase x where x.ProjCatId = a.ProjCatId and x.ProjClaseId = a.ProjClaseId),
		a.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..EstadosIDM x where x.cod_estado = a.cod_estado),
		a.fch_estado,
		a.usr_estado,
		a.marca,
		a.plan_tyo,
		plan_TYO_nom = case
			when a.plan_tyo = 'S' then 'Si'
			when a.plan_tyo = 'N' then 'No'
			else ''
		end,
		a.empresa,
		nom_empresa = case
			when a.empresa = 'B' then 'Banco'
			when a.empresa = 'C' then 'BBVA Seguros'
			when a.empresa = 'D' then 'RCF'
			when a.empresa = 'E' then 'PSA'
			when a.empresa = 'F' then 'RCF/PSA'
			else ''
		end,
		a.area,
		area_nom = case
			when a.area = 'C' then 'Corporativo'
			when a.area = 'R' then 'Regional'
			when a.area = 'L' then 'Local'
			else ''
		end,
		a.clas3,
		clas3_nom = case
			when a.clas3 = '1' then '2013'
			when a.clas3 = '2' then '2014'
			when a.clas3 = '3' then '2015'
			when a.clas3 = '4' then '2013 a 2014'
			when a.clas3 = '5' then '2013 a 2015'
			when a.clas3 = '6' then '2014 a 2015'
			else ''
			/*
			when a.clas3 = 'C' then 'Customer Centric'
			when a.clas3 = 'M' then 'Modelo de distribución'
			when a.clas3 = 'L' then 'LEAN'
			else ''
			*/
		end,
		a.semaphore
		,a.cod_direccion
		,a.cod_gerencia
		,a.cod_sector
		,nom_areaprop = (
			RTRIM((select d.nom_direccion from GesPet..Direccion d where d.cod_direccion = a.cod_direccion)) +
			' » ' + RTRIM((select g.nom_gerencia from GesPet..Gerencia g where g.cod_gerencia = a.cod_gerencia)) + 
			' » ' + RTRIM((select s.nom_sector from GesPet..Sector s where s.cod_sector = a.cod_sector))
		),
		a.cod_direccion_p,
		a.cod_gerencia_p,
		a.cod_sector_p,
		a.cod_grupo_p,
		a.avance,
		a.cod_estado2,
		a.fe_modif,
		a.fe_inicio,
		a.fe_fin,
		a.fe_finreprog,
		a.fe_ult_nove,
		a.semaphore2,
		a.sema_fe_modif,
		a.sema_usuario,
		orden = (a.ProjId * 10000 + a.ProjSubId * 10 + a.ProjSubSId),			-- Para ordenar
		a.tipo_proyecto,
		a.totalhs_gerencia,
		a.totalreplan,
		a.totalhs_sector,
		a.fe_aprob,
		a.cambio_alcance,
		a.codigo_gps
	from
		GesPet..ProyectoIDM a
go

grant select on dbo.vw_ProyectoIDM to GesPetUsr
go

print 'Actualización realizada.'
go
