/*
-001- a. FJS 10.07.2013 - Nueva vista para
*/

use GesPet
go

print 'Creando/actualizando vista: vw_ProyectoIDMNovedades'
go

if exists (select * from sysobjects where name = 'vw_ProyectoIDMNovedades' and sysstat & 7 = 2)
	drop view dbo.vw_ProyectoIDMNovedades
go

create view dbo.vw_ProyectoIDMNovedades
as 
	select
		'1' as 'pet_empid',
		a.ProjId,
		a.ProjSubId,
		a.ProjSubSId,
		a.mem_fecha,
		a.mem_campo,
		a.mem_secuencia,
		mem_texto = rtrim((str_replace(str_replace(str_replace(str_replace(str_replace(a.mem_texto, '|', ''),char(13),''),char(10),''),char(9),''),char(7),'')))
		/*
		a.cod_usuario,
		a.mem_accion,
		a.mem_fecha2,
		a.cod_usuario2
		*/
	from GesPet..ProyectoIDMNovedades a
	where a.mem_accion <> 'DEL'
go

grant select on dbo.vw_ProyectoIDMNovedades to GesPetUsr
go

print 'Actualización realizada.'
go
