/*
-001- a. FJS 08.07.2013 - Nueva vista para
*/

use GesPet
go

print 'Creando/actualizando vista: vw_ProyectoIDMMemo'
go

if exists (select * from sysobjects where name = 'vw_ProyectoIDMMemo' and sysstat & 7 = 2)
	drop view dbo.vw_ProyectoIDMMemo
go

create view dbo.vw_ProyectoIDMMemo
as 
	select
		'1' as 'pet_empid',
		a.ProjId,
		a.ProjSubId,
		a.ProjSubSId,
		a.mem_campo,
		a.mem_secuencia,
		mem_texto = rtrim((str_replace(str_replace(str_replace(str_replace(str_replace(a.mem_texto, '|', ''),char(13),''),char(10),''),char(9),''),char(7),'')))
	from GesPet..ProyectoIDMMemo a
go

grant select on dbo.vw_ProyectoIDMMemo to GesPetUsr
go

print 'Actualización realizada.'
go
