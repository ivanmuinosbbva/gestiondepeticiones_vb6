/*
-001- a. FJS 26.05.2010 - Nueva vista para...
-002- a. FJS 20.05.2013 - Se agrega join para vincular las fábricas de software.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Recurso'
go

if exists (select * from sysobjects where name = 'vw_Recurso' and sysstat & 7 = 2)
	drop view dbo.vw_Recurso
go

create view dbo.vw_Recurso
as 
	select
		'1' as 'pet_empid',
		a.cod_recurso,
		nom_recurso = (str_replace(str_replace(str_replace(str_replace(str_replace(a.nom_recurso, '|',''), char(13),''), char(10),''), char(9),''), char(7),'')),
		a.vinculo,
		a.cod_gerencia,
		a.cod_direccion,
		a.cod_sector,
		a.cod_grupo,
		a.flg_cargoarea,
		a.estado_recurso,
		a.horasdiarias,
		observaciones = (str_replace(str_replace(str_replace(str_replace(str_replace(a.observaciones, '|',''), char(13),''), char(10),''), char(9),''), char(7),'')),
		a.dlg_recurso,
		dlg_desde = (convert(char(8), a.dlg_desde, 112)),
		dlg_hasta = (convert(char(8), a.dlg_hasta, 112)),
		a.audit_user,
		audit_date = (convert(char(8), a.audit_date, 112)),
		a.email,
		a.euser
	from GesPet..Recurso a
	union 
	select
		'1' as 'pet_empid',
		f.cod_fab,
		nom_recurso = (str_replace(str_replace(str_replace(str_replace(str_replace(f.nom_fab, '|',''), char(13),''), char(10),''), char(9),''), char(7),'')),
		'F',
		f.cod_gerencia,
		f.cod_direccion,
		f.cod_sector,
		f.cod_grupo,
		null,
		f.estado_fab,
		0,
		null,
		null,
		null,
		null,
		null,
		null,
		null,
		'N'
	from GesPet..Fabrica f
go

grant select on dbo.vw_Recurso to GesPetUsr
go

print 'Actualización realizada.'
go
