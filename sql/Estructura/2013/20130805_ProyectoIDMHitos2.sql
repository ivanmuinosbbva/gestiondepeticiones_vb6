print 'Creando / modificando la tabla: ProyectoIDMHitos2... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'ProyectoIDMHitos2' and type = 'U')
	drop table GesPet.dbo.ProyectoIDMHitos2
go
	
create table GesPet.dbo.ProyectoIDMHitos2 (
	ProjId				int				null,
	ProjSubId			int				null,
	ProjSubSId			int				null,
	hito_nombre			char(100)		null,
	hito_fe_ini			smalldatetime	null,
	hito_fe_fin			smalldatetime	null,
	hito_descripcion	char(255)		null,
	hito_estado			char(6)			null,
	cod_nivel			char(4)			null,
	cod_area			char(8)			null,
	hito_fecha			smalldatetime	null,
	hito_usuario		char(10)		null,
	hst_nrointerno		int				NULL,
	hit_nrointerno		int             NULL)
go

print 'Tabla modificada exitosamente.'
go

