print 'Creando / modificando la tabla: AccionesPerfilIDM... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'AccionesPerfilIDM' and type = 'U')
	drop table GesPet.dbo.AccionesPerfilIDM
go
	
create table GesPet.dbo.AccionesPerfilIDM (
	cod_accion	char(8) NOT NULL,
	cod_perfil	char(4) NOT NULL,
	cod_estado	char(6)	NOT NULL,
	cod_estnew	char(6)	NOT NULL,
	flg_hereda	char(1)	NULL,
	constraint PKAccPrfIDM1 PRIMARY KEY NONCLUSTERED (cod_accion, cod_perfil, cod_estado, cod_estnew))
go

print 'Tabla modificada exitosamente.'
go
