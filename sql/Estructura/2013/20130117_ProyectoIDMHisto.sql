print 'Creando / modificando la tabla: ProyectoIDMHisto... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'ProyectoIDMHisto' and type = 'U')
	drop table GesPet.dbo.ProyectoIDMHisto
go
	
create table GesPet.dbo.ProyectoIDMHisto (
	ProjId          int			not null,
	ProjSubId       int			not null,
	ProjSubSId      int			not null,
	hst_nrointerno	int			not null,
	hst_fecha		smalldatetime	null,
	cod_evento		char(8)			null,
	proj_estado		char(6)			null,
	replc_user		char(10)		null,
	audit_user		char(10)		null,
	constraint PK_ProyectoIDMHisto1 PRIMARY KEY NONCLUSTERED (ProjId, ProjSubId, ProjSubSId, hst_nrointerno )
	)
go

print 'Tabla modificada exitosamente.'
go