print 'Creando / modificando la tabla: EstadosIDM... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'EstadosIDM' and type = 'U')
	drop table GesPet.dbo.EstadosIDM
go
	
create table GesPet.dbo.EstadosIDM (
	cod_estado  char(6)		NOT NULL,
	nom_estado  char(30)	NULL,
	flg_rnkup   char(2)		NULL,
	flg_herdlt1 char(1)		NULL,
	flg_petici  char(1)		NULL,
	flg_secgru  char(1)		NULL,
	constraint PKEstadosIDM1 PRIMARY KEY NONCLUSTERED (cod_estado))
go

print 'Tabla modificada exitosamente.'
go
