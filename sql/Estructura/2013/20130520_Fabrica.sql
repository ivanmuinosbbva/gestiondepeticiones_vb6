print 'Creando / modificando la tabla: Fabrica... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Fabrica' and type = 'U')
	drop table GesPet.dbo.Fabrica
go
	
create table GesPet.dbo.Fabrica (
	cod_fab			char(10)	NOT NULL,
	nom_fab			char(50)	NULL,
	cod_gerencia	char(8)		NULL,
	cod_direccion	char(8)		NULL,
	cod_sector		char(8)		NULL,
	cod_grupo		char(8)		NULL,
	estado_fab		char(1)		NULL,
	constraint PKFab1 PRIMARY KEY NONCLUSTERED (cod_fab))
go

print 'Tabla modificada exitosamente.'
go
