print 'Creando / modificando la tabla: ProyectoIDM... aguarde por favor...'
go

alter table GesPet..ProyectoIDM
	add 
		fe_aprob		smalldatetime	null,	-- Fecha de aprobación (cuando pasa de Iniciativa a Proyecto)
		cambio_alcance	char(1)			null	-- Indica con S o N si hubo cambio de alcance
go

print 'Tabla modificada exitosamente.'
go
