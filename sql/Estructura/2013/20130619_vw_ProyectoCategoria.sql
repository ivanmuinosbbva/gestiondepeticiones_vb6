/*
-001- a. FJS 18.06.2013 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_ProyectoCategoria'
go

if exists (select * from sysobjects where name = 'vw_ProyectoCategoria' and sysstat & 7 = 2)
	drop view dbo.vw_ProyectoCategoria
go

create view dbo.vw_ProyectoCategoria
as 
	select
		'1' as 'pet_empid',
		a.ProjCatId,
		a.ProjCatNom
	from
		GesPet..ProyectoCategoria a
go

grant select on dbo.vw_ProyectoCategoria to GesPetUsr
go

print 'Actualización realizada.'
