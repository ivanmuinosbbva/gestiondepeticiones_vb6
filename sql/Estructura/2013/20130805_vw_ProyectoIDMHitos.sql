/*
-001- a. FJS 05.08.2013 - Nueva vista para listar los hitos de proyectos IDM.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_ProyectoIDMHitos'
go

if exists (select * from sysobjects where name = 'vw_ProyectoIDMHitos' and sysstat & 7 = 2)
	drop view dbo.vw_ProyectoIDMHitos
go

create view dbo.vw_ProyectoIDMHitos
as 
	select

		a.ProjId,
		a.ProjSubId,
		a.ProjSubSId,
		hito_nombre = rtrim((str_replace(str_replace(str_replace(str_replace(str_replace(a.hito_nombre, '|', ''),char(13),''),char(10),''),char(9),''),char(7),''))),
		a.hito_fe_ini,
		a.hito_fe_fin,
		hito_descripcion = rtrim((str_replace(str_replace(str_replace(str_replace(str_replace(a.hito_descripcion, '|', ''),char(13),''),char(10),''),char(9),''),char(7),''))),
		a.hito_estado,
		a.cod_nivel,
		a.cod_area,
		a.hito_fecha,
		a.hito_usuario
	from GesPet..ProyectoIDMHitos a
go

grant select on dbo.vw_ProyectoIDMHitos to GesPetUsr
go

print 'Actualización realizada.'
go
