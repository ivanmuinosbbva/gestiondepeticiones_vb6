print 'Creando / modificando la tabla: ProyectoIDMHitos... aguarde por favor...'
go

alter table GesPet..ProyectoIDMHitos
	add 
		hst_nrointerno	int		null,	-- Nro. de interno en el historial
		hit_nrointerno	int		null,	-- Nro. interno de hito
		hit_orden		int		null
go

print 'Tabla modificada exitosamente.'
go
