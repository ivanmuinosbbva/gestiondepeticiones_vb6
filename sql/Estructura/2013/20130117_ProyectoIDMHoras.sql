print 'Creando / modificando la tabla: ProyectoIDMHoras... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'ProyectoIDMHoras' and type = 'U')
	drop table GesPet.dbo.ProyectoIDMHoras
go
	
create table GesPet.dbo.ProyectoIDMHoras (
	ProjId          int			not null,
	ProjSubId       int			not null,
	ProjSubSId      int			not null,
	cod_nivel		char(3)		not null,
	cod_area		char(8)		not null,
	cant_horas		int			null,
	constraint PKProyectoIDMHoras1 PRIMARY KEY NONCLUSTERED (ProjId, ProjSubId, ProjSubSId, cod_nivel, cod_area))
go

print 'Tabla modificada exitosamente.'
go
