print 'Creando / modificando la tabla: ProyectoIDM... aguarde por favor...'
go

alter table GesPet..ProyectoIDM
	add 
		tipo_proyecto	char(1)			null,	-- Tipo de proyecto (I:Iniciativa / P:Proyecto)
		totalhs_gerencia	int			null,	-- Total de horas planificadas por gerencia
		totalhs_sector		int			null,	-- Total de horas estimadas por sector
		totalreplan			int			null	-- Total de replanificaciones
go

print 'Tabla modificada exitosamente.'
go
