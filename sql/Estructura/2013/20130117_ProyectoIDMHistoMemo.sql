print 'Creando / modificando la tabla: ProyectoIDMHistoMemo... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'ProyectoIDMHistoMemo' and type = 'U')
	drop table GesPet.dbo.ProyectoIDMHistoMemo
go
	
create table GesPet.dbo.ProyectoIDMHistoMemo (
	hst_nrointerno	int			not null,
	mem_campo		char(10)	not null,
	mem_secuencia	smallint	not null,
	mem_texto		varchar(255)	null,
	constraint PK_ProyIDMHistoMemo1 PRIMARY KEY NONCLUSTERED (hst_nrointerno,mem_campo,mem_secuencia))
go

print 'Tabla creada/modificada exitosamente.'
go
