use GesPet
go 

print 'Creando / modificando la tabla: PeticionPlancab... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'PeticionPlancab' and type = 'U')
	drop table GesPet.dbo.PeticionPlancab
go
	
create table GesPet.dbo.PeticionPlancab (
	per_nrointerno	int not null,			-- Nro. interno de per�odo
	pet_nrointerno  int not null,			-- Nro. interno de petici�n
	prioridad       int null,				-- Prioridad asignada por el ref. de RGyP
	plan_actfch     smalldatetime null,		-- Fecha de vinculaci�n de la petici�n al per�odo
	plan_actusr     char(10) null,			-- Usuario que realiz� la vinculaci�n de la petici�n al per�odo
	cod_bpe         char(10) null,			-- Referente de RGyP que asign� la prioridad
	plan_origen     int null,				-- Origen de la vinculaci�n de la petici�n al per�odo de planificaci�n (1: proceso, 2: manual)
	hst_nrointerno  int	null,				-- Nro. de historial de la petici�n del momento de asignaci�n
	constraint PKPetPlancab1 PRIMARY KEY NONCLUSTERED (per_nrointerno, pet_nrointerno))
go

print 'Tabla modificada exitosamente.'
go
