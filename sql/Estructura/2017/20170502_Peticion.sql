/*
*********************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Prop�sito:	Nuevos campos para la parte de BPE.
* Ambiente:		SyBase
* Fecha:		01.11.2016
*********************************************************************************************************
*/

use GesPet
go

print 'Creando/modificando la tabla: Peticion... aguarde por favor...'
go

alter table GesPet.dbo.Peticion
	add
		prioridadBPE		int				null,		-- Es el n�mero de prioridad (orden) con que BPE ordena la lista de peticiones valorizadas.
		puntuacion			real			null
go

print 'Tabla modificada exitosamente.'
go
