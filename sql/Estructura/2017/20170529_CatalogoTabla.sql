print 'Creando / modificando la tabla: CatalogoTabla... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'CatalogoTabla' and type = 'U')
	drop table GesPet.dbo.CatalogoTabla
go
	
create table GesPet.dbo.CatalogoTabla (
	id				int				not null,
	nombreTabla		varchar(40)		not null,
	baseDeDatos		varchar(40)		not null,
	esquema			varchar(40)		not null,
	DBMS			int				not null,
	servidorNombre	varchar(40)		not null,
	servidorPuerto	int				null,
	app_id			char(30)		null,
	fe_alta			smalldatetime	null,
	usr_alta		char(10)		null,
	datosSensibles	char(1)			null,
	visado			char(1)			null,
	visadoFecha		smalldatetime	null,
	visadoUsuario	char(10)		null,
	constraint PK_Catcab1 PRIMARY KEY NONCLUSTERED (id))
go

print 'Tabla modificada exitosamente.'
go
