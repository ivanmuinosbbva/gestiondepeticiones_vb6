/*
	23.05.2017 - FJS

	Esta tabla es una especializaci�n de la tabla original AccionesPerfil.

	La tabla original, AccionesPerfil, contiene dos tipos de relaciones entre perfiles y acciones:
	- Acci�n por perfil habilitada seg�n un estado determinado.
	- Acci�n por perfil habilitada(s) seg�n un estado, que permite pasar a otro estado.

	Ejemplos de �stas �ltimas, son las acciones PCHGEST, SCHGEST y GCHGEST, cambio de estado de la petici�n, 
	cambio de estado de sector vinculado a la petici�n y, por �ltimo, cambio de estado de grupo vinculado a la 
	petici�n.

	A partir de la incorporaci�n de esta nueva tabla, lo que se pretende es dejar la tabla original solo con 
	informaci�n relevante a la autorizaci�n de acciones para un perfil dado, basado en un estado definido.

	La nueva tabla, en cambio, servir� para identificar estas acciones que permiten realizar un pasaje de 
	estado, ya sea de la petici�n, el sector o grupo, basado en un estado de origen y definiendo un estado
	de destino.

*/

print 'Creando / modificando la tabla: AccionesPerfilEstados... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'AccionesPerfilEstados' and type = 'U')
	drop table GesPet.dbo.AccionesPerfilEstados
go
	
create table GesPet.dbo.AccionesPerfilEstados (
	cod_version		int			not null,				-- Nro que se utiliza para poder llevar diferentes versiones de estas acciones por perfil para cambios de estado.
	cod_accion		char(8)		not null,
	cod_perfil		char(4)		not null,
	cod_estado		char(6)		not null,
	cod_estnew		char(6)		not null,
	flg_hereda		char(1)			null,
	constraint PK_AccPerfEst1 PRIMARY KEY NONCLUSTERED (cod_version, cod_accion, cod_perfil, cod_estado, cod_estnew))
go

print 'Tabla creada / modificada exitosamente.'
go
