use GesPet
go

print 'Eliminando el �ndice: idx_PeticionGrupo2...'
drop index PeticionGrupo.idx_PeticionGrupo2
print 'Hecho.'
go

print 'Creando el �ndice: idx_PeticionGrupo1...'
create nonclustered index idx_PeticionGrupo1
on GesPet.dbo.PeticionGrupo (cod_grupo, pet_nrointerno)
print 'Hecho.'
go

