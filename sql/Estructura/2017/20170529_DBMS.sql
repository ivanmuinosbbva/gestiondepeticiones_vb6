print 'Creando / modificando la tabla: DBMS... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'DBMS' and type = 'U')
	drop table GesPet.dbo.DBMS
go
	
create table GesPet.dbo.DBMS (
	dbmsId			int				not null,
	dbmsNom			varchar(50)		null,
	constraint PK_DBMS1 PRIMARY KEY NONCLUSTERED (dbmsId))
go

print 'Tabla modificada exitosamente.'
go