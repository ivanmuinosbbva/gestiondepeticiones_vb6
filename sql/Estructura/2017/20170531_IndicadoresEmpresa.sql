print 'Creando / modificando la tabla: IndicadoresEmpresa... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'IndicadoresEmpresa' and type = 'U')
	drop table GesPet.dbo.IndicadoresEmpresa
go
	
create table GesPet.dbo.IndicadoresEmpresa (
	empid_peticion	int		not null,
	empid_indicador	int		not null,
	constraint PK_IndicadorEmpid PRIMARY KEY NONCLUSTERED (empid_peticion, empid_indicador))
go

print 'Tabla modificada exitosamente.'
go