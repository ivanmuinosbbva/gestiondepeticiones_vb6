/*
*********************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Prop�sito:	Se agrega el campo driverReq para indicar que si el valor del driver (peso de la opci�n) 
*				es mayor a cero, entonces debe indicar un KPI.
* Ambiente:		SyBase
* Fecha:		04.11.2016
*********************************************************************************************************
*/

use GesPet
go

print 'Creando/modificando la tabla: Drivers... aguarde por favor...'
go

alter table GesPet.dbo.Drivers 
	add 
		driverReq	char(1)		null		-- Indica que si el valor del driver (peso de la opci�n) es mayor a cero, entonces debe indicar un KPI.
go

print 'Tabla modificada exitosamente.'
go
