use GesPet
go

if exists (select * from sysobjects where name = 'TD_Periodo' and sysstat & 7 = 2)
	drop view dbo.TD_Periodo
go

create view dbo.TD_Periodo
as
	select
		p.per_nrointerno,
		p.per_nombre,
		p.per_tipo,
		p.fe_desde,
		p.fe_hasta,
		p.per_estado,
		p.per_ultfchest,
		p.per_ultusrid,
		p.per_abrev
	from 
		GesPet..Periodo p
go

grant select on dbo.PeriodoEstado to GesPetUsr
go

print 'Actualización realizada.'
go
