print 'Creando / modificando la tabla: Validacion... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Validacion' and type = 'U')
	drop table GesPet.dbo.Validacion
go
	
create table GesPet.dbo.Validacion (
	valid		int				not null,
	valitem		int				not null,
	valdesc		varchar(255)	not	null,
	valorden	int				null,
	valhab		char(1)			null,
	audituser	char(10)		null,
	auditfecha	smalldatetime	null,
	constraint PK_Validacion1 PRIMARY KEY NONCLUSTERED (valid, valitem))
go

print 'Tabla modificada exitosamente.'
go