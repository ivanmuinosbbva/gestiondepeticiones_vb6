use GesPet
go

print 'Creando/modificando la tabla: Peticion... aguarde por favor...'
go

alter table GesPet.dbo.Peticion 
	add 
		sda_agr_nrointerno	int			null,		-- El agrupamiento que indica que pertenece a SDA.
		sgi_incidencia		varchar(20)	null		-- Nro. de incidencia vinculada del aplicativo SGI.
go

print 'Tabla modificada exitosamente.'
go
