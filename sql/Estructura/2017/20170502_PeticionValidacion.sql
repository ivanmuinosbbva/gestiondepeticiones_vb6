print 'Creando / modificando la tabla: PeticionValidacion... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'PeticionValidacion' and type = 'U')
	drop table GesPet.dbo.PeticionValidacion
go
	
create table GesPet.dbo.PeticionValidacion (
	pet_nrointerno	int		not null,
	valid			int		not null,
	valitem			int		not null,
	valitemvalor	char(1)			null,
	audituser		char(10)		null,
	auditfecha		smalldatetime	null,
	constraint PK_PeticionValidacion1 PRIMARY KEY NONCLUSTERED (pet_nrointerno, valid, valitem))
go

print 'Tabla modificada exitosamente.'
go