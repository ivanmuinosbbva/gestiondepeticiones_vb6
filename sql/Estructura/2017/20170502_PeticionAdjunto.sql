print 'Creando/modificando la tabla: PeticionAdjunto... aguarde por favor...'
go

use GesPet
go

alter table GesPet.dbo.PeticionAdjunto
	add 
		adj_unc				varchar(255)	null,			-- Universal Naming Convention: is the naming system commonly used in Microsoft Windows for accessing shared network folders.
		adj_filename		varchar(255)	null,			-- Nombre del archivo guardado en el UNC.
		adj_filesystem		char(1)			null,			-- Indica con S o N si el objeto se encuentra en el filesystem (caso contrario, est� en la base de datos).
		adj_CRC32			int				null			-- El c�digo CRC32 del archivo al bajar de la base de datos al filesystem.
	modify 
		adj_file			varchar(255)	not null,		-- Se cambia el tipo de datos de char a varchar.
		adj_texto			varchar(255)	null,			-- Idem.
		adj_objeto			image			null			-- Se acepta valor NULL para el campo.
go

print 'Tabla modificada exitosamente.'
go
