/*
*********************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Prop�sito:	Se agregan cuatro campos.
*				- grupo_tipopet: indica que tipo de petici�n (NOR, ESP, PRJ, AUX, etc.) puede dar de alta 
*				  determinado grupo.
*				- grupo_inchoras: indica si el grupo tiene incidencia en las horas del sector y/o peticion 
* 
* Ambiente:		SyBase
* Fecha:		17.04.2017
*********************************************************************************************************
*/

use GesPet
go

print 'Creando/modificando la tabla: Grupo... aguarde por favor...'
go

alter table GesPet.dbo.Grupo
	add
		grupo_tipopet      char(1)	NULL,
		grupo_inchoras     char(1)	NULL,
		grupo_incfechas    char(1)	NULL,
		grupo_incestado    char(1)	NULL
go

print 'Tabla modificada exitosamente.'
go
