print 'Creando / modificando la tabla: RecursoPerfilAudit... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'RecursoPerfilAudit' and type = 'U')
	drop table GesPet.dbo.RecursoPerfilAudit
go
	
create table GesPet.dbo.RecursoPerfilAudit (
	cod_recurso char(10)		NOT NULL,
	cod_perfil  char(4)			NOT NULL,
	cod_ope		int				NOT NULL,
	cod_nivel	char(4)			NOT NULL,
	cod_area	char(8)			NOT NULL,
	auditfch	smalldatetime	NOT NULL,
	auditusr	char(10)		NULL,
	constraint PK_RecPrfAudit1 PRIMARY KEY NONCLUSTERED (cod_recurso, cod_perfil, cod_ope, auditfch))
go

print 'Tabla modificada exitosamente.'
go
