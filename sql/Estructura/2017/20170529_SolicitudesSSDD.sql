print 'Creando / modificando la tabla: SolicitudesSSDD... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'SolicitudesSSDD' and type = 'U')
	drop table GesPet.dbo.SolicitudesSSDD
go
	
create table GesPet.dbo.SolicitudesSSDD (
	sol_nroasignado		int			not null,
	sol_mask			char(1)			NULL,
	sol_fecha			smalldatetime	NULL,
	sol_recurso			char(10)		NULL,
	jus_codigo			int             NULL,
	sol_texto			varchar(255)	NULL,
	sol_trnm_fe			smalldatetime	NULL,			-- Fecha de proceso del ENM
	sol_trnm_usr		char(10)		NULL,			-- Usuario de proceso del ENM
	pet_nroasignado		int				NULL,
	pet_nrointerno		int				NULL,
	tablaId				int				null,			-- Opcional, una tabla asociada, para ya traer los datos de nombre de tabla, esquema, DBMS, servidor, puerto, etc.
	sol_archivo			varchar(80)	not null,			-- Nmbre del archivo a generar a partir de una tabla
	sol_eme				char(1)			null,
	sol_diferida		char(1)			null,
	sol_estado			char(1)			null,			-- Estado del pedido
	fechaAprobacion		smalldatetime	null,
	aprobador			char(10)		null,
	constraint PK_SOLSSDD1 PRIMARY KEY NONCLUSTERED (sol_nroasignado))
go

print 'Tabla modificada exitosamente.'
go
