print 'Creando / modificando la tabla: Estructura... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Estructura' and type = 'U')
	drop table GesPet.dbo.Estructura
go
	
create table GesPet.dbo.Estructura (
	fecha			datetime		NOT NULL,
	cod_recurso		char(10)		NOT NULL,
	cod_direccion	char(8)			NULL,
	cod_gerencia	char(8)			NULL,
	cod_sector		char(8)			NULL,
	cod_grupo		char(8)			NULL,
	audit_user		char(10)		null,
	audit_date		smalldatetime	null,
	constraint PK_Estructura1 PRIMARY KEY NONCLUSTERED (fecha, cod_recurso))
go

print 'Tabla modificada exitosamente.'
go