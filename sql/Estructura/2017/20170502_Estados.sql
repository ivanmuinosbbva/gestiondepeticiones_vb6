/*
*********************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Propůsito:	Nuevos campos para la tabla Estados.
* Ambiente:		SyBase
* Fecha:		13.03.2017
*********************************************************************************************************
*/

use GesPet
go

print 'Creando/modificando la tabla: Estados... aguarde por favor...'
go

alter table GesPet.dbo.Estados
	add
		terminal	char(1)	null,	-- Indica si el estado es terminal (S/N).
		imagen		int		null,	-- Nro. de imagen para uso futuro con ImageComboBox o controles similares.
		default_est	char(6)	null	-- Siguiente estado natural por defecto para el estado actual
go

print 'Tabla modificada exitosamente.'
go
