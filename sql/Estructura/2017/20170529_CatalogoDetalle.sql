print 'Creando / modificando la tabla: CatalogoDetalle... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'CatalogoDetalle' and type = 'U')
	drop table GesPet.dbo.CatalogoDetalle
go
	
create table GesPet.dbo.CatalogoDetalle (
	id				int				not null,
	campo			varchar(50)		not null,
	pos_desde		int				null,
	longitud		int				null,
	tipo_id			char(1)			null,
	subtipo_id		char(1)			null,
	rutina			char(8)			null, 
	constraint PK_Catdet1 PRIMARY KEY NONCLUSTERED (id, campo))
go

print 'Tabla modificada exitosamente.'
go