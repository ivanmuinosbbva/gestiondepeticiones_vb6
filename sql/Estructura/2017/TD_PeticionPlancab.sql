use GesPet
go

if exists (select * from sysobjects where name = 'TD_PeticionPlancab' and sysstat & 7 = 2)
	drop view dbo.TD_PeticionPlancab
go

create view dbo.TD_PeticionPlancab
as
	select
		per_nrointerno,
		pet_nrointerno,
		prioridad,
		plan_actfch,
		plan_actusr,
		cod_bpe,
		plan_origen,
		hst_nrointerno
	from PeticionPlancab
go

grant select on dbo.PeticionPlancab to GesPetUsr
go

print 'Actualización realizada.'
go
