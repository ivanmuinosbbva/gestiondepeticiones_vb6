print 'Creando/modificando la tabla: HorasTrabajadas... aguarde por favor...'
go

use GesPet
go

alter table GesPet..HorasTrabajadas
	add tipo	int	default 0
go

print 'Tabla modificada exitosamente.'
go

print 'Fin de proceso.'
go