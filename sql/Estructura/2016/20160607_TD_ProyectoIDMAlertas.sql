use GesPet
go
if exists (select * from sysobjects where name = 'TD_ProyectoIDMAlertas' and sysstat & 7 = 2)
	drop view dbo.TD_ProyectoIDMAlertas
go
create view dbo.TD_ProyectoIDMAlertas
as
select
	ProjId, 
	ProjSubId, 
	ProjSubSId, 
	alert_itm, 
	alert_tipo, 
	alert_desc, 
	accion_id, 
	alert_fe_ini, 
	alert_fe_fin, 
	responsables, 
	observaciones, 
	alert_status, 
	alert_fecres, 
	alert_fecha, 
	alert_usuario
from ProyectoIDMAlertas
go
grant select on dbo.ProyectoIDMAlertas to GesPetUsr
go
print 'Actualización realizada.'
go
