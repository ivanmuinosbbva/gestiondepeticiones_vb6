print 'Creando / modificando la tabla: Valoracion... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Valoracion' and type = 'U')
	drop table GesPet.dbo.Valoracion
go
	
create table GesPet.dbo.Valoracion (
	valorId			int				not null,
	valorTexto		varchar(255)	null,
	valorNum		int				null,
	valorOrden		int				null,
	valorHab		char(1)			null,
	valorDriver		int				null,
	constraint PK_Valoracion1 PRIMARY KEY NONCLUSTERED (valorId ))
go

print 'Tabla modificada exitosamente.'
go