use GesPet
go
if exists (select * from sysobjects where name = 'TD_Prioridad' and sysstat & 7 = 2)
	drop view dbo.TD_Prioridad
go
create view dbo.TD_Prioridad
as
select
	prior_id, 
	prior_dsc, 
	prior_ord, 
	prior_hab
from Prioridad
go
grant select on dbo.Prioridad to GesPetUsr
go
print 'Actualización realizada.'
go
