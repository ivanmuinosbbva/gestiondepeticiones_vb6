/*
-000- a. FJS 21.03.2016 - Nueva vista para interfase BI.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Valoracion'
go

if exists (select * from sysobjects where name = 'vw_Valoracion' and sysstat & 7 = 2)
	drop view dbo.vw_Valoracion
go

create view dbo.vw_Valoracion
as 
	SELECT
		v.valorId,
		v.valorTexto,
		v.valorNum,
		v.valorOrden,
		v.valorHab,
		v.valorDriver
	FROM Valoracion v 
go

grant select on dbo.vw_Valoracion to GesPetUsr
go

print 'Actualización realizada.'
go
