use GesPet
go
if exists (select * from sysobjects where name = 'TD_RecursoPerfil' and sysstat & 7 = 2)
	drop view dbo.TD_RecursoPerfil
go
create view dbo.TD_RecursoPerfil
as
select
	cod_recurso, 
	cod_perfil, 
	cod_nivel, 
	cod_area
from RecursoPerfil
go
grant select on dbo.RecursoPerfil to GesPetUsr
go
print 'Actualización realizada.'
go
