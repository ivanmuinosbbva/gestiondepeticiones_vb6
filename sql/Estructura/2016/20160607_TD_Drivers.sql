use GesPet
go
if exists (select * from sysobjects where name = 'TD_Drivers' and sysstat & 7 = 2)
	drop view dbo.TD_Drivers
go
create view dbo.TD_Drivers
as
select
	driverId, 
	driverNom, 
	driverAyuda, 
	driverValor, 
	driverResp, 
	driverHab, 
	indicadorId
from Drivers
go
grant select on dbo.Drivers to GesPetUsr
go
print 'Actualización realizada.'
go
