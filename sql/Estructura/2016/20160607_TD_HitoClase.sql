use GesPet
go
if exists (select * from sysobjects where name = 'TD_HitoClase' and sysstat & 7 = 2)
	drop view dbo.TD_HitoClase
go
create view dbo.TD_HitoClase
as
select
	hitoid, 
	hitodesc, 
	hitoexpos
from HitoClase
go
grant select on dbo.HitoClase to GesPetUsr
go
print 'Actualización realizada.'
go
