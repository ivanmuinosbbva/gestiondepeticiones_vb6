print 'Creando / modificando la tabla: InterfazTeraDet... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'InterfazTeraDet' and type = 'U')
	drop table GesPet.dbo.InterfazTeraDet
go
	
create table GesPet.dbo.InterfazTeraDet (
	dbName			varchar(30)		not null,
	objname			varchar(50)		not null,
	objfield		varchar(50)		not null,
	objfieldhab		char(1)			null,
	objfielddesc	varchar(255)	null,
	objultact		smalldatetime	null,
	constraint PKITeraDet1 PRIMARY KEY NONCLUSTERED (dbName, objname, objfield))
go

print 'Tabla modificada exitosamente.'
go