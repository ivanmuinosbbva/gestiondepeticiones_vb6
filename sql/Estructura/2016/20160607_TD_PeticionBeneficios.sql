use GesPet
go
if exists (select * from sysobjects where name = 'TD_PeticionBeneficios' and sysstat & 7 = 2)
	drop view dbo.TD_PeticionBeneficios
go
create view dbo.TD_PeticionBeneficios
as
select
	pet_nrointerno, 
	indicadorId, 
	subindicadorId, 
	valor, 
	audit_fe, 
	audit_usr
from PeticionBeneficios
go
grant select on dbo.PeticionBeneficios to GesPetUsr
go
print 'Actualización realizada.'
go
