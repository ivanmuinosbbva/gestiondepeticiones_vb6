use GesPet
go

if exists (select * from sysobjects where name = 'TD_ProyectoIDMAdjuntos' and sysstat & 7 = 2)
	drop view dbo.TD_ProyectoIDMAdjuntos
go

create view dbo.TD_ProyectoIDMAdjuntos
as
	select
		ProjId, 
		ProjSubId, 
		ProjSubSId, 
		adj_tipo, 
		adj_file, 
		audit_user, 
		audit_date, 
		cod_nivel, 
		cod_area
	from ProyectoIDMAdjuntos
go

grant select on dbo.ProyectoIDMAdjuntos to GesPetUsr
go

print 'Actualización realizada.'
go
