print 'Modificando la tabla : Gerencia'
go

print '1. Agregando campo: abrev_gerencia...'
go

alter table GesPet..Gerencia
	add abrev_gerencia		varchar(30)	null
go

print '2. Modificando campo: nom_gerencia...'
go

alter table GesPet..Gerencia
	modify nom_gerencia		char(80)	null
go

print 'Listo.'
go
