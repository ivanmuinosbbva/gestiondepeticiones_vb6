use GesPet
go
if exists (select * from sysobjects where name = 'TD_Agrup' and sysstat & 7 = 2)
	drop view dbo.TD_Agrup
go
create view dbo.TD_Agrup
as
select
	agr_nrointerno, 
	agr_nropadre, 
	agr_titulo, 
	agr_vigente, 
	cod_direccion, 
	cod_gerencia, 
	cod_sector, 
	cod_grupo, 
	cod_usualta, 
	agr_visibilidad, 
	agr_actualiza, 
	audit_user, 
	audit_date, 
	agr_admpet
from Agrup
go
grant select on dbo.Agrup to GesPetUsr
go
print 'Actualización realizada.'
go
