use GesPet
go
if exists (select * from sysobjects where name = 'TD_PeticionRecurso' and sysstat & 7 = 2)
	drop view dbo.TD_PeticionRecurso
go
create view dbo.TD_PeticionRecurso
as
select
	pet_nrointerno, 
	cod_recurso, 
	cod_grupo, 
	cod_sector, 
	cod_gerencia, 
	cod_direccion, 
	audit_user, 
	audit_date
from PeticionRecurso
go
grant select on dbo.PeticionRecurso to GesPetUsr
go
print 'Actualización realizada.'
go
