use GesPet
go
if exists (select * from sysobjects where name = 'TD_ProyectoIDM' and sysstat & 7 = 2)
	drop view dbo.TD_ProyectoIDM
go
create view dbo.TD_ProyectoIDM
as
select
	ProjId, 
	ProjSubId, 
	ProjSubSId, 
	ProjNom, 
	ProjFchAlta, 
	ProjCatId, 
	ProjClaseId, 
	cod_estado, 
	fch_estado, 
	usr_estado, 
	marca, 
	plan_tyo, 
	empresa, 
	area, 
	clas3, 
	semaphore, 
	avance, 
	cod_estado2, 
	fe_modif, 
	fe_inicio, 
	fe_fin, 
	fe_finreprog, 
	fe_ult_nove, 
	semaphore2, 
	sema_fe_modif, 
	sema_usuario, 
	cod_direccion, 
	cod_gerencia, 
	cod_sector, 
	cod_direccion_p, 
	cod_gerencia_p, 
	cod_sector_p, 
	cod_grupo_p, 
	tipo_proyecto, 
	totalhs_gerencia, 
	totalreplan, 
	totalhs_sector, 
	fe_aprob, 
	cambio_alcance, 
	codigo_gps, 
	codigoEmpresa
from ProyectoIDM
go
grant select on dbo.ProyectoIDM to GesPetUsr
go
print 'Actualización realizada.'
go
