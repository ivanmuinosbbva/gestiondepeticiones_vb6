/*
-001- a. FJS 26.05.2010 - Nueva vista para...
-002- a. FJS 11.03.2016 - Se adapta para BPE.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Sector'
go

if exists (select * from sysobjects where name = 'vw_Sector' and sysstat & 7 = 2)
	drop view dbo.vw_Sector
go

create view dbo.vw_Sector
as 
	select
		'1' as 'pet_empid',
		a.cod_sector,
		a.nom_sector,
		a.cod_gerencia,
		a.flg_habil,
		a.cod_abrev,
		a.es_ejecutor,
		a.cod_bpar,
		a.cod_perfil			-- add -002- a.
	from
		GesPet..Sector a
go

grant select on dbo.vw_Sector to GesPetUsr
go

print 'Actualización realizada.'
go
