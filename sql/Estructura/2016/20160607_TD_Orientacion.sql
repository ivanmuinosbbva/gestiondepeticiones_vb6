use GesPet
go
if exists (select * from sysobjects where name = 'TD_Orientacion' and sysstat & 7 = 2)
	drop view dbo.TD_Orientacion
go
create view dbo.TD_Orientacion
as
select
	cod_orientacion, 
	nom_orientacion, 
	flg_habil, 
	orden
from Orientacion
go
grant select on dbo.Orientacion to GesPetUsr
go
print 'Actualización realizada.'
go
