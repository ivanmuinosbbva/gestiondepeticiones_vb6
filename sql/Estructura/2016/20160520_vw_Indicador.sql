/*
-000- a. FJS 21.03.2016 - Nueva vista para interfase BI.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Indicador'
go

if exists (select * from sysobjects where name = 'vw_Indicador' and sysstat & 7 = 2)
	drop view dbo.vw_Indicador
go

create view dbo.vw_Indicador
as 
	SELECT
		i.indicadorId,
		i.indicadorNom,
		i.indicadorHab
	FROM Indicador i
go

grant select on dbo.vw_Indicador to GesPetUsr
go

print 'Actualización realizada.'
go
