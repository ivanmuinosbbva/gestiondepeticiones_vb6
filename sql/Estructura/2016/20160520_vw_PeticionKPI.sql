/*
-000- a. FJS 21.03.2016 - Nueva vista para interfase BI.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_PeticionKPI'
go

if exists (select * from sysobjects where name = 'vw_PeticionKPI' and sysstat & 7 = 2)
	drop view dbo.vw_PeticionKPI
go

create view dbo.vw_PeticionKPI
as 
	SELECT
		pk.pet_nrointerno,
		pk.pet_kpiid,
		pk.kpi_unimedId,
		pk.pet_kpidesc,
		pk.valorEsperado,
		pk.valorPartida,
		pk.tiempoEsperado,
		pk.unimedTiempo,
		pk.audit_fecha,
		pk.audit_usuario
	FROM PeticionKPI pk 
go

grant select on dbo.vw_PeticionKPI to GesPetUsr
go

print 'Actualización realizada.'
go
