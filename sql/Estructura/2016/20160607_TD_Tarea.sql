use GesPet
go
if exists (select * from sysobjects where name = 'TD_Tarea' and sysstat & 7 = 2)
	drop view dbo.TD_Tarea
go
create view dbo.TD_Tarea
as
select
	cod_tarea, 
	nom_tarea, 
	flg_habil
from Tarea
go
grant select on dbo.Tarea to GesPetUsr
go
print 'Actualización realizada.'
go
