/*
*********************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Propůsito:	Nuevo campo para indicar el perfil del responsable.
* Ambiente:		SyBase
* Fecha:		01.02.2016
*********************************************************************************************************
*/

print 'Creando/modificando la tabla: Sector... aguarde por favor...'
go

use GesPet
go

alter table GesPet.dbo.Sector
	add
		cod_perfil	char(4)		null		-- Perfil, para indicar si es el Ref. de sistemas o el Ref. de RGP.
go

print 'Tabla modificada exitosamente.'
go
