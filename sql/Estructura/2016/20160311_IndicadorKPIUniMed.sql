print 'Creando / modificando la tabla: IndicadorKPIUniMed... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'IndicadorKPIUniMed' and type = 'U')
	drop table GesPet.dbo.IndicadorKPIUniMed
go
	
create table GesPet.dbo.IndicadorKPIUniMed (
	kpiid		int			not null,
	unimedId	char(10)	not	null,
	constraint PK_IndiKPIUM1 PRIMARY KEY NONCLUSTERED (kpiid, unimedId))
go

print 'Tabla modificada exitosamente.'
go