use GesPet
go
if exists (select * from sysobjects where name = 'TD_Peticion' and sysstat & 7 = 2)
	drop view dbo.TD_Peticion
go
create view dbo.TD_Peticion
as
select
	pet_nrointerno, 
	pet_nroasignado, 
	titulo, 
	cod_tipo_peticion, 
	prioridad, 
	pet_nroanexada, 
	fe_pedido, 
	fe_requerida, 
	fe_comite, 
	fe_ini_plan, 
	fe_fin_plan, 
	fe_ini_real, 
	fe_fin_real, 
	horaspresup, 
	cod_direccion, 
	cod_gerencia, 
	cod_sector, 
	cod_usualta, 
	cod_solicitante, 
	cod_referente, 
	cod_supervisor, 
	cod_director, 
	cod_estado, 
	fe_estado, 
	cod_situacion, 
	ult_accion, 
	audit_user, 
	audit_date, 
	corp_local, 
	cod_orientacion, 
	importancia_cod, 
	importancia_prf, 
	importancia_usr, 
	fe_fec_plan, 
	fe_ini_orig, 
	fe_fin_orig, 
	fe_fec_orig, 
	cant_planif, 
	prj_nrointerno, 
	byp_comite, 
	cod_bpar, 
	cod_clase, 
	pet_imptech, 
	pet_sox001, 
	pet_regulatorio, 
	pet_projid, 
	pet_projsubid, 
	pet_projsubsid, 
	pet_emp, 
	pet_ro, 
	cod_BPE, 
	fecha_BPE, 
	pet_driver, 
	valor_impacto, 
	valor_facilidad, 
	cargaBeneficios
from Peticion
go
grant select on dbo.Peticion to GesPetUsr
go
print 'Actualización realizada.'
go
