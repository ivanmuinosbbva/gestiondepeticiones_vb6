/*
*********************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Prop�sito:	Nuevos campos para la parte de BPE.
* Ambiente:		SyBase
* Fecha:		28.01.2016
*********************************************************************************************************
*/

print 'Creando/modificando la tabla: Peticion... aguarde por favor...'
go

use GesPet
go

print 'Creando/modificando la tabla: agregando los nuevos campos.'
go

alter table GesPet.dbo.Peticion
	add
		cod_BPE				char(10)		null,		-- Ref. de RGP
		fecha_BPE			smalldatetime	null,		-- Fecha en que queda al estado "Al referente RGP"
		pet_driver			char(8)			null,		-- Qui�n administra la petici�n: Sistemas o BPE
		valor_impacto		int				null,		-- Valor de impacto
		valor_facilidad		int				null,		-- Valor de facilidad
		cargaBeneficios		char(1)			null		-- indica si al dar de alta una petici�n el solicitante carga los beneficios (impacto, etc. para valoraci�n)
go

print 'Creando/modificando la tabla: pasando cod_situacion a NULLEABLE.'
go

alter table GesPet.dbo.Peticion
	modify 
		cod_situacion		null
go

print 'Creando/modificando la tabla: pasando titulo de CHAR(50) a VARCHAR(120).'
go

alter table GesPet.dbo.Peticion
	modify 
		titulo	varchar(120)	null					-- Se extiende el tama�o de 50 a 120 y se cambia el tipo CHAR por VARCHAR
go

print 'Tabla modificada exitosamente.'
go
