/*
*********************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Propůsito:	Cambio de tipo de dato de campo.
* Ambiente:		SyBase
* Fecha:		03.05.2016
*********************************************************************************************************
*/

print 'Creando/modificando la tabla: Valoracion... aguarde por favor...'
go

use GesPet
go

print 'Creando/modificando la tabla: pasando valorNum a REAL.'
go

alter table GesPet.dbo.Valoracion 
	modify 
		valorNum	REAL	null
go

print 'Tabla modificada exitosamente.'
go
