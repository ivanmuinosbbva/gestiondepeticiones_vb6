use GesPet
go
if exists (select * from sysobjects where name = 'TD_Documento' and sysstat & 7 = 2)
	drop view dbo.TD_Documento
go
create view dbo.TD_Documento
as
select
	cod_doc, 
	nom_doc, 
	hab_doc
from Documento
go
grant select on dbo.Documento to GesPetUsr
go
print 'Actualización realizada.'
go
