use GesPet
go
if exists (select * from sysobjects where name = 'TD_Fabrica' and sysstat & 7 = 2)
	drop view dbo.TD_Fabrica
go
create view dbo.TD_Fabrica
as
select
	cod_fab, 
	nom_fab, 
	cod_gerencia, 
	cod_direccion, 
	cod_sector, 
	cod_grupo, 
	estado_fab, 
	tipo_fab
from Fabrica
go
grant select on dbo.Fabrica to GesPetUsr
go
print 'Actualización realizada.'
go
