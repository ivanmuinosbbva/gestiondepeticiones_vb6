use GesPet
go
if exists (select * from sysobjects where name = 'TD_ProyectoIDMHitos' and sysstat & 7 = 2)
	drop view dbo.TD_ProyectoIDMHitos
go
create view dbo.TD_ProyectoIDMHitos
as
select
	ProjId, 
	ProjSubId, 
	ProjSubSId, 
	hito_nombre, 
	hito_fe_ini, 
	hito_fe_fin, 
	hito_estado, 
	cod_nivel, 
	cod_area, 
	hito_fecha, 
	hito_usuario, 
	hst_nrointerno, 
	hit_nrointerno, 
	hit_orden, 
	hito_expos, 
	hito_id
from ProyectoIDMHitos
go
grant select on dbo.ProyectoIDMHitos to GesPetUsr
go
print 'Actualización realizada.'
go
