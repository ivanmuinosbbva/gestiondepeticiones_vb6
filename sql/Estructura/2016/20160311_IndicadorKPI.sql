print 'Creando / modificando la tabla: IndicadorKPI... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'IndicadorKPI' and type = 'U')
	drop table GesPet.dbo.IndicadorKPI
go
	
create table GesPet.dbo.IndicadorKPI (
	kpiid		int			not null,
	kpinom		varchar(60)		null,
	kpireq		char(1)			null,
	kpihab		char(1)			null,
	unimedId	char(10)		null,
	kpiayuda	varchar(255)	null,
	constraint PK_IndicadorKPI1 PRIMARY KEY NONCLUSTERED (kpiid))
go

print 'Tabla modificada exitosamente.'
go