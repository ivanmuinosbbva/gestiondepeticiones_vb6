use GesPet
go
if exists (select * from sysobjects where name = 'TD_TipoFabrica' and sysstat & 7 = 2)
	drop view dbo.TD_TipoFabrica
go
create view dbo.TD_TipoFabrica
as
select
	TipoFabId, 
	TipoFabNom
from TipoFabrica
go
grant select on dbo.TipoFabrica to GesPetUsr
go
print 'Actualización realizada.'
go
