use GesPet
go
if exists (select * from sysobjects where name = 'TD_Valoracion' and sysstat & 7 = 2)
	drop view dbo.TD_Valoracion
go
create view dbo.TD_Valoracion
as
select
	valorId, 
	valorTexto, 
	valorNum, 
	valorOrden, 
	valorHab, 
	valorDriver
from Valoracion
go
grant select on dbo.Valoracion to GesPetUsr
go
print 'Actualización realizada.'
go
