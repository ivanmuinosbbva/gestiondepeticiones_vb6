/*
-000- a. FJS 21.03.2016 - Nueva vista para interfase BI.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Drivers'
go

if exists (select * from sysobjects where name = 'vw_Drivers' and sysstat & 7 = 2)
	drop view dbo.vw_Drivers
go

create view dbo.vw_Drivers
as 
	SELECT
		d.driverId,
		d.driverNom,
		d.driverAyuda,
		d.driverValor,
		d.driverResp,
		d.driverHab,
		d.indicadorId
	FROM Drivers d
go

grant select on dbo.vw_Drivers to GesPetUsr
go

print 'Actualización realizada.'
go
