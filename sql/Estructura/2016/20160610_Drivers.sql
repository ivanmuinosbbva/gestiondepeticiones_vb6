/*
*********************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Propůsito:	Cambio de tipo de dato de campo.
* Ambiente:		SyBase
* Fecha:		03.05.2016
*********************************************************************************************************
*/

print 'Creando/modificando la tabla: Drivers... aguarde por favor...'
go

use GesPet
go

print 'Creando/modificando la tabla: pasando driverValor a REAL.'
go

alter table GesPet.dbo.Drivers 
	modify 
		driverValor	REAL	null
go

print 'Tabla modificada exitosamente.'
go
