/*
-000- a. FJS 13.05.2016 - Nueva vista para interfase BI.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Agrup'
go

if exists (select * from sysobjects where name = 'vw_Agrup' and sysstat & 7 = 2)
	drop view dbo.vw_Agrup
go

create view dbo.vw_Agrup
as 
	SELECT
		a.agr_nrointerno,
		a.agr_nropadre,
		a.agr_titulo,
		a.agr_vigente,
		a.agr_admpet,
		a.cod_direccion,
		a.cod_gerencia,
		a.cod_sector,
		a.cod_grupo,
		a.cod_usualta,
		a.agr_visibilidad,
		a.agr_actualiza,
		a.audit_user,
		audit_date = (convert(char(8), a.audit_date, 112))
	FROM Agrup a
go

grant select on dbo.vw_Agrup to GesPetUsr
go

print 'Actualización realizada.'
go
