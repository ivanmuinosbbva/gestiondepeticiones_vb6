/*
-000- a. FJS 21.03.2016 - Nueva vista para interfase BI.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_PeticionBeneficios'
go

if exists (select * from sysobjects where name = 'vw_PeticionBeneficios' and sysstat & 7 = 2)
	drop view dbo.vw_PeticionBeneficios
go

create view dbo.vw_PeticionBeneficios
as 
	SELECT
		pb.pet_nrointerno,
		pb.indicadorId,
		pb.subindicadorId,
		pb.valor,
		pb.audit_fe,
		pb.audit_usr
	FROM PeticionBeneficios pb
go

grant select on dbo.vw_PeticionBeneficios to GesPetUsr
go

print 'Actualización realizada.'
go
