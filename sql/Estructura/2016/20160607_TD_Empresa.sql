use GesPet
go
if exists (select * from sysobjects where name = 'TD_Empresa' and sysstat & 7 = 2)
	drop view dbo.TD_Empresa
go
create view dbo.TD_Empresa
as
select
	empid, 
	empnom, 
	emphab, 
	empabrev
from Empresa
go
grant select on dbo.Empresa to GesPetUsr
go
print 'Actualización realizada.'
go
