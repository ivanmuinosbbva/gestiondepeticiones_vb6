/*
-000- a. FJS 21.03.2016 - Nueva vista para interfase BI.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_IndicadorKPI'
go

if exists (select * from sysobjects where name = 'vw_IndicadorKPI' and sysstat & 7 = 2)
	drop view dbo.vw_IndicadorKPI
go

create view dbo.vw_IndicadorKPI
as 
	SELECT
		ik.kpiid,
		ik.kpinom,
		ik.kpireq,
		ik.kpihab,
		ik.unimedId,
		unimedDesc = (select um.unimedDesc from UnidadMedida um where um.unimedId = ik.unimedId),
		ik.kpiayuda
	FROM IndicadorKPI ik 
go

grant select on dbo.vw_IndicadorKPI to GesPetUsr
go

print 'Actualización realizada.'
go
