use GesPet
go
if exists (select * from sysobjects where name = 'TD_Situaciones' and sysstat & 7 = 2)
	drop view dbo.TD_Situaciones
go
create view dbo.TD_Situaciones
as
select
	cod_situacion, 
	nom_situacion
from Situaciones
go
grant select on dbo.Situaciones to GesPetUsr
go
print 'Actualización realizada.'
go
