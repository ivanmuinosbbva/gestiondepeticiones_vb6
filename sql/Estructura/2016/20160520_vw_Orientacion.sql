/*
-000- a. FJS 21.03.2016 - Nueva vista para interfase BI.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Orientacion'
go

if exists (select * from sysobjects where name = 'vw_Orientacion' and sysstat & 7 = 2)
	drop view dbo.vw_Orientacion
go

create view dbo.vw_Orientacion
as 
	SELECT
		o.cod_orientacion, 
		o.nom_orientacion,
		o.flg_habil,
		o.orden
	FROM Orientacion o
go

grant select on dbo.vw_Orientacion to GesPetUsr
go

print 'Actualización realizada.'
go
