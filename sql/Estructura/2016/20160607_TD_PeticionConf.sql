use GesPet
go
if exists (select * from sysobjects where name = 'TD_PeticionConf' and sysstat & 7 = 2)
	drop view dbo.TD_PeticionConf
go
create view dbo.TD_PeticionConf
as
select
	pet_nrointerno, 
	ok_tipo, 
	ok_modo, 
	audit_user, 
	audit_date, 
	ok_secuencia
from PeticionConf
go
grant select on dbo.PeticionConf to GesPetUsr
go
print 'Actualización realizada.'
go
