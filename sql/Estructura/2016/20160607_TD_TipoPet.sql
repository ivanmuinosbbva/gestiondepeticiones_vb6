use GesPet
go
if exists (select * from sysobjects where name = 'TD_TipoPet' and sysstat & 7 = 2)
	drop view dbo.TD_TipoPet
go
create view dbo.TD_TipoPet
as
select
	cod_tipopet, 
	nom_tipopet
from TipoPet
go
grant select on dbo.TipoPet to GesPetUsr
go
print 'Actualización realizada.'
go
