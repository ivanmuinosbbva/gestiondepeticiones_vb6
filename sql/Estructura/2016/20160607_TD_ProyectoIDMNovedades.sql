use GesPet
go
if exists (select * from sysobjects where name = 'TD_ProyectoIDMNovedades' and sysstat & 7 = 2)
	drop view dbo.TD_ProyectoIDMNovedades
go
create view dbo.TD_ProyectoIDMNovedades
as
select
	ProjId, 
	ProjSubId, 
	ProjSubSId, 
	mem_fecha, 
	mem_campo, 
	mem_secuencia, 
	mem_texto, 
	cod_usuario, 
	mem_accion, 
	mem_fecha2, 
	cod_usuario2
from ProyectoIDMNovedades
go
grant select on dbo.ProyectoIDMNovedades to GesPetUsr
go
print 'Actualización realizada.'
go
