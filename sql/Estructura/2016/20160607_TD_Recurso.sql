use GesPet
go
if exists (select * from sysobjects where name = 'TD_Recurso' and sysstat & 7 = 2)
	drop view dbo.TD_Recurso
go
create view dbo.TD_Recurso
as
select
	cod_recurso, 
	nom_recurso, 
	vinculo, 
	cod_gerencia, 
	cod_direccion, 
	cod_sector, 
	cod_grupo, 
	flg_cargoarea, 
	estado_recurso, 
	horasdiarias, 
	dlg_recurso, 
	dlg_desde, 
	dlg_hasta, 
	audit_user, 
	audit_date, 
	email, 
	euser
from Recurso
go
grant select on dbo.Recurso to GesPetUsr
go
print 'Actualización realizada.'
go
