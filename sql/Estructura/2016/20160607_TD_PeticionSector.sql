use GesPet
go
if exists (select * from sysobjects where name = 'TD_PeticionSector' and sysstat & 7 = 2)
	drop view dbo.TD_PeticionSector
go
create view dbo.TD_PeticionSector
as
select
	pet_nrointerno, 
	cod_sector, 
	cod_gerencia, 
	cod_direccion, 
	fe_ini_plan, 
	fe_fin_plan, 
	fe_ini_real, 
	fe_fin_real, 
	horaspresup, 
	cod_estado, 
	fe_estado, 
	cod_situacion, 
	ult_accion, 
	hst_nrointerno_sol, 
	hst_nrointerno_rsp, 
	audit_user, 
	audit_date, 
	fe_fec_plan, 
	fe_ini_orig, 
	fe_fin_orig, 
	fe_fec_orig, 
	cant_planif
from PeticionSector
go
grant select on dbo.PeticionSector to GesPetUsr
go
print 'Actualización realizada.'
go
