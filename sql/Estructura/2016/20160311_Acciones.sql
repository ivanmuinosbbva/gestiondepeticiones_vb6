print 'Creando/modificando la tabla: Acciones... aguarde por favor...'
go

use GesPet
go

alter table GesPet.dbo.Acciones
	add  
		tipo_accion			char(6)			null,
		agrupador			int				null,
		leyenda				varchar(100)	null,
		orden				int				null,
		porEstado			char(1)			null,
		multiplesEstados	char(1)			null
go

print 'Tabla modificada exitosamente.'
go
