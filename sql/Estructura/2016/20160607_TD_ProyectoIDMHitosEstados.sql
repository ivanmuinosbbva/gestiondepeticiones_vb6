use GesPet
go
if exists (select * from sysobjects where name = 'TD_ProyectoIDMHitosEstados' and sysstat & 7 = 2)
	drop view dbo.TD_ProyectoIDMHitosEstados
go
create view dbo.TD_ProyectoIDMHitosEstados
as
select
	hito_estado, 
	descripcion
from ProyectoIDMHitosEstados
go
grant select on dbo.ProyectoIDMHitosEstados to GesPetUsr
go
print 'Actualización realizada.'
go
