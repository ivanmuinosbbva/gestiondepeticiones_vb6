use GesPet
go
if exists (select * from sysobjects where name = 'TD_AgrupPetic' and sysstat & 7 = 2)
	drop view dbo.TD_AgrupPetic
go
create view dbo.TD_AgrupPetic
as
select
	agr_nrointerno, 
	pet_nrointerno, 
	audit_user, 
	audit_date
from AgrupPetic
go
grant select on dbo.AgrupPetic to GesPetUsr
go
print 'Actualización realizada.'
go
