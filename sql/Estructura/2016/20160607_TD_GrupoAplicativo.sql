use GesPet
go
if exists (select * from sysobjects where name = 'TD_GrupoAplicativo' and sysstat & 7 = 2)
	drop view dbo.TD_GrupoAplicativo
go
create view dbo.TD_GrupoAplicativo
as
select
	cod_grupo, 
	app_id
from GrupoAplicativo
go
grant select on dbo.GrupoAplicativo to GesPetUsr
go
print 'Actualización realizada.'
go
