print 'Creando / modificando la tabla: PeticionBeneficios... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'PeticionBeneficios' and type = 'U')
	drop table GesPet.dbo.PeticionBeneficios
go
	
create table GesPet.dbo.PeticionBeneficios (
	pet_nrointerno		int				not null,
	indicadorId			int				not null,
	subindicadorId		int				not null,
	valor				int				null,
	audit_fe			smalldatetime	null,
	audit_usr			char(10)		null,
	constraint PK_PeticionBeneficios1 PRIMARY KEY NONCLUSTERED (pet_nrointerno, indicadorId, subindicadorId))
go

print 'Tabla modificada exitosamente.'
go