use GesPet
go
if exists (select * from sysobjects where name = 'TD_Feriado' and sysstat & 7 = 2)
	drop view dbo.TD_Feriado
go
create view dbo.TD_Feriado
as
select
	fecha
from Feriado
go
grant select on dbo.Feriado to GesPetUsr
go
print 'Actualización realizada.'
go
