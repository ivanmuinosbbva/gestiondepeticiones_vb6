use GesPet
go
if exists (select * from sysobjects where name = 'TD_ProyectoIDMGrupos' and sysstat & 7 = 2)
	drop view dbo.TD_ProyectoIDMGrupos
go
create view dbo.TD_ProyectoIDMGrupos
as
select
	ProjId, 
	ProjSubId, 
	ProjSubSId, 
	cod_nivel, 
	cod_area, 
	grupo_user, 
	grupo_fecha
from ProyectoIDMGrupos
go
grant select on dbo.ProyectoIDMGrupos to GesPetUsr
go
print 'Actualización realizada.'
go
