print 'Creando / modificando la tabla: UnidadMedida... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'UnidadMedida' and type = 'U')
	drop table GesPet.dbo.UnidadMedida
go
	
create table GesPet.dbo.UnidadMedida (
	unimedId		char(10)		not null,
	unimedDesc		varchar(60)		null,
	unimedSmb		varchar(4)		null,
	constraint PK_UniMed1 PRIMARY KEY NONCLUSTERED (unimedId))
go

print 'Tabla modificada exitosamente.'
go
