use GesPet
go
if exists (select * from sysobjects where name = 'TD_Justificativos' and sysstat & 7 = 2)
	drop view dbo.TD_Justificativos
go
create view dbo.TD_Justificativos
as
select
	jus_codigo, 
	jus_descripcion, 
	jus_hab
from Justificativos
go
grant select on dbo.Justificativos to GesPetUsr
go
print 'Actualización realizada.'
go
