/*
-000- a. FJS 13.05.2016 - Nueva vista para interfase BI.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_AgrupPetic'
go

if exists (select * from sysobjects where name = 'vw_AgrupPetic' and sysstat & 7 = 2)
	drop view dbo.vw_AgrupPetic
go

create view dbo.vw_AgrupPetic
as 
	SELECT
		ap.agr_nrointerno,
		ap.pet_nrointerno,
		ap.audit_user,
		audit_date = (convert(char(8), ap.audit_date, 112))
	FROM AgrupPetic ap
go

grant select on dbo.vw_AgrupPetic to GesPetUsr
go

print 'Actualización realizada.'
go
