print 'Creando / modificando la tabla: Empresa... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Empresa' and type = 'U')
	drop table GesPet.dbo.Empresa
go
	
create table GesPet.dbo.Empresa (
	empid		int				not null,
	empnom		varchar(50)		null,
	emphab		char(1)			null,
	empabrev	varchar(30)		null,
	constraint PK_Empresa1 PRIMARY KEY NONCLUSTERED (empid))
go

print 'Tabla modificada exitosamente.'
go