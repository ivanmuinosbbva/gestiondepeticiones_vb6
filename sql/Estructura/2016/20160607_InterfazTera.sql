print 'Creando / modificando la tabla: InterfazTera... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'InterfazTera' and type = 'U')
	drop table GesPet.dbo.InterfazTera
go
	
create table GesPet.dbo.InterfazTera (
	dbName			varchar(30)		not null,
	objname			varchar(50)		not null,
	objlabel		varchar(50)		null,
	objhab			char(1)			null,
	constraint PK_Interfaztera1 PRIMARY KEY NONCLUSTERED (dbName, objname))
go

print 'Tabla modificada exitosamente.'
go