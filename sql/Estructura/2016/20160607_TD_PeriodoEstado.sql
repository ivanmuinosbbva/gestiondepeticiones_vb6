use GesPet
go
if exists (select * from sysobjects where name = 'TD_PeriodoEstado' and sysstat & 7 = 2)
	drop view dbo.TD_PeriodoEstado
go
create view dbo.TD_PeriodoEstado
as
select
	cod_estado_per, 
	nom_estado_per
from PeriodoEstado
go
grant select on dbo.PeriodoEstado to GesPetUsr
go
print 'Actualización realizada.'
go
