use GesPet
go
if exists (select * from sysobjects where name = 'TD_IndicadorKPI' and sysstat & 7 = 2)
	drop view dbo.TD_IndicadorKPI
go
create view dbo.TD_IndicadorKPI
as
select
	kpiid, 
	kpinom, 
	kpireq, 
	kpihab, 
	unimedId, 
	kpiayuda
from IndicadorKPI
go
grant select on dbo.IndicadorKPI to GesPetUsr
go
print 'Actualización realizada.'
go
