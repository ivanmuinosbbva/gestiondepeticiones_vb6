use GesPet
go
if exists (select * from sysobjects where name = 'TD_Importancia' and sysstat & 7 = 2)
	drop view dbo.TD_Importancia
go
create view dbo.TD_Importancia
as
select
	cod_importancia, 
	nom_importancia, 
	flg_habil
from Importancia
go
grant select on dbo.Importancia to GesPetUsr
go
print 'Actualización realizada.'
go
