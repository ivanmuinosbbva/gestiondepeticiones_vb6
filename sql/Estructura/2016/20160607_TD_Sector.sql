use GesPet
go
if exists (select * from sysobjects where name = 'TD_Sector' and sysstat & 7 = 2)
	drop view dbo.TD_Sector
go
create view dbo.TD_Sector
as
select
	cod_sector, 
	nom_sector, 
	cod_gerencia, 
	flg_habil, 
	cod_abrev, 
	es_ejecutor, 
	cod_bpar, 
	soli_hab, 
	cod_perfil
from Sector
go
grant select on dbo.Sector to GesPetUsr
go
print 'Actualización realizada.'
go
