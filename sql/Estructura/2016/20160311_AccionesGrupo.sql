print 'Creando / modificando la tabla: AccionesGrupo... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'AccionesGrupo' and type = 'U')
	drop table GesPet.dbo.AccionesGrupo
go
	
create table GesPet.dbo.AccionesGrupo (
	accionGrupoId		int				not null,
	accionGrupoNom		varchar(50)		null,
	accionGrupoHab		char(1)			null,
	constraint PK_AccionesGrupo1 PRIMARY KEY NONCLUSTERED (accionGrupoId))
go

print 'Tabla modificada exitosamente.'
go