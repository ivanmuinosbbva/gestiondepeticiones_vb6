use GesPet
go
if exists (select * from sysobjects where name = 'TD_ProyectoCategoria' and sysstat & 7 = 2)
	drop view dbo.TD_ProyectoCategoria
go
create view dbo.TD_ProyectoCategoria
as
select
	ProjCatId, 
	ProjCatNom
from ProyectoCategoria
go
grant select on dbo.ProyectoCategoria to GesPetUsr
go
print 'Actualización realizada.'
go
