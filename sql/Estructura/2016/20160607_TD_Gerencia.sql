use GesPet
go
if exists (select * from sysobjects where name = 'TD_Gerencia' and sysstat & 7 = 2)
	drop view dbo.TD_Gerencia
go
create view dbo.TD_Gerencia
as
select
	cod_gerencia, 
	nom_gerencia, 
	cod_direccion, 
	flg_habil, 
	es_ejecutor, 
	IGM_hab, 
	abrev_gerencia
from Gerencia
go
grant select on dbo.Gerencia to GesPetUsr
go
print 'Actualización realizada.'
go
