use GesPet
go
if exists (select * from sysobjects where name = 'TD_Direccion' and sysstat & 7 = 2)
	drop view dbo.TD_Direccion
go
create view dbo.TD_Direccion
as
select
	cod_direccion, 
	nom_direccion, 
	flg_habil, 
	es_ejecutor, 
	IGM_hab, 
	abrev_direccion
from Direccion
go
grant select on dbo.Direccion to GesPetUsr
go
print 'Actualización realizada.'
go
