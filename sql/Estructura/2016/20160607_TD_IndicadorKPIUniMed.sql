use GesPet
go
if exists (select * from sysobjects where name = 'TD_IndicadorKPIUniMed' and sysstat & 7 = 2)
	drop view dbo.TD_IndicadorKPIUniMed
go
create view dbo.TD_IndicadorKPIUniMed
as
select
	kpiid, 
	unimedId
from IndicadorKPIUniMed
go
grant select on dbo.IndicadorKPIUniMed to GesPetUsr
go
print 'Actualización realizada.'
go
