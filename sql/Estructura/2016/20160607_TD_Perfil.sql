use GesPet
go
if exists (select * from sysobjects where name = 'TD_Perfil' and sysstat & 7 = 2)
	drop view dbo.TD_Perfil
go
create view dbo.TD_Perfil
as
select
	cod_perfil, 
	nom_perfil, 
	visible, 
	habilitado, 
	orden
from Perfil
go
grant select on dbo.Perfil to GesPetUsr
go
print 'Actualización realizada.'
go
