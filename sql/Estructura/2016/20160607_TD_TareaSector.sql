use GesPet
go
if exists (select * from sysobjects where name = 'TD_TareaSector' and sysstat & 7 = 2)
	drop view dbo.TD_TareaSector
go
create view dbo.TD_TareaSector
as
select
	cod_tarea, 
	cod_sector
from TareaSector
go
grant select on dbo.TareaSector to GesPetUsr
go
print 'Actualización realizada.'
go
