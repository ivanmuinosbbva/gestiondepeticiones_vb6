use GesPet
go

if exists (select * from sysobjects where name = 'TD_Historial' and sysstat & 7 = 2)
	drop view dbo.TD_Historial
go

create view dbo.TD_Historial
as
	select
		pet_nrointerno,
		hst_nrointerno,
		hst_fecha,
		cod_evento,
		pet_estado,
		cod_sector,
		sec_estado,
		cod_grupo,
		gru_estado,
		replc_user,
		audit_user
	from Historial
go

grant select on dbo.HitoClase to GesPetUsr
go

print 'Actualización realizada.'
go
