use GesPet
go
if exists (select * from sysobjects where name = 'TD_PeticionAdjunto' and sysstat & 7 = 2)
	drop view dbo.TD_PeticionAdjunto
go
create view dbo.TD_PeticionAdjunto
as
select
	pet_nrointerno, 
	adj_tipo, 
	adj_file, 
	audit_user, 
	audit_date, 
	cod_sector, 
	cod_grupo
from PeticionAdjunto
go
grant select on dbo.PeticionAdjunto to GesPetUsr
go
print 'Actualización realizada.'
go
