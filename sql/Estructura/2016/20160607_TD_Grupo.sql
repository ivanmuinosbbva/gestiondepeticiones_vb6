use GesPet
go
if exists (select * from sysobjects where name = 'TD_Grupo' and sysstat & 7 = 2)
	drop view dbo.TD_Grupo
go
create view dbo.TD_Grupo
as
select
	cod_grupo, 
	nom_grupo, 
	cod_sector, 
	flg_habil, 
	es_ejecutor, 
	cod_bpar, 
	grupo_homologacion, 
	abrev_grupo
from Grupo
go
grant select on dbo.Grupo to GesPetUsr
go
print 'Actualización realizada.'
go
