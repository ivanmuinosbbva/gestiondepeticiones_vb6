print 'Creando / modificando la tabla: PeticionKPI... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'PeticionKPI' and type = 'U')
	drop table GesPet.dbo.PeticionKPI
go
	
create table GesPet.dbo.PeticionKPI (
	pet_nrointerno	int			not null,
	pet_kpiid		int			not null,
	kpi_unimedId	char(10)		null,
	pet_kpidesc		varchar(150)	null,
	valorEsperado	numeric(10,2)	null,
	valorPartida	numeric(10,2)	null,
	tiempoEsperado	numeric(10,2)	null,
	unimedTiempo	char(10)		null,
	audit_fecha		smalldatetime	null,
	audit_usuario	char(10)		null,
	constraint PK_PeticionKPI1 PRIMARY KEY NONCLUSTERED (pet_nrointerno, pet_kpiid))
go

print 'Tabla modificada exitosamente.'
go
