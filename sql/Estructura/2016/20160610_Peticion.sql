/*
*********************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Propůsito:	Cambio de tipo de datos de campos.
* Ambiente:		SyBase
* Fecha:		03.05.2016
*********************************************************************************************************
*/

print 'Creando/modificando la tabla: Peticion... aguarde por favor...'
go

use GesPet
go

print '1. Modificando campo: valor_impacto...'
go

alter table GesPet.dbo.Peticion
	modify valor_impacto	REAL	null
go

print '2. Modificando campo: valor_facilidad...'
go

alter table GesPet.dbo.Peticion
	modify valor_facilidad	REAL	null
go

print 'Tabla modificada exitosamente.'
go
