use GesPet
go
if exists (select * from sysobjects where name = 'TD_PeticionKPI' and sysstat & 7 = 2)
	drop view dbo.TD_PeticionKPI
go
create view dbo.TD_PeticionKPI
as
select
	pet_nrointerno, 
	pet_kpiid, 
	kpi_unimedId, 
	valorEsperado, 
	valorPartida, 
	tiempoEsperado, 
	unimedTiempo, 
	audit_fecha, 
	audit_usuario, 
	pet_kpidesc
from PeticionKPI
go
grant select on dbo.PeticionKPI to GesPetUsr
go
print 'Actualización realizada.'
go
