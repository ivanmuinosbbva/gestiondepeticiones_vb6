use GesPet
go
if exists (select * from sysobjects where name = 'TD_Clase' and sysstat & 7 = 2)
	drop view dbo.TD_Clase
go
create view dbo.TD_Clase
as
select
	cod_clase, 
	nom_clase
from Clase
go
grant select on dbo.Clase to GesPetUsr
go
print 'Actualización realizada.'
go
