use GesPet
go
if exists (select * from sysobjects where name = 'TD_Estados' and sysstat & 7 = 2)
	drop view dbo.TD_Estados
go
create view dbo.TD_Estados
as
select
	cod_estado, 
	nom_estado, 
	flg_rnkup, 
	flg_herdlt1, 
	flg_petici, 
	flg_secgru, 
	IGM_hab
from Estados
go
grant select on dbo.Estados to GesPetUsr
go
print 'Actualización realizada.'
go
