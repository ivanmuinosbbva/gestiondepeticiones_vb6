use GesPet
go
if exists (select * from sysobjects where name = 'TD_ProyectoIDMHoras' and sysstat & 7 = 2)
	drop view dbo.TD_ProyectoIDMHoras
go
create view dbo.TD_ProyectoIDMHoras
as
select
	ProjId, 
	ProjSubId, 
	ProjSubSId, 
	cod_nivel, 
	cod_area, 
	cant_horas
from ProyectoIDMHoras
go
grant select on dbo.ProyectoIDMHoras to GesPetUsr
go
print 'Actualización realizada.'
go
