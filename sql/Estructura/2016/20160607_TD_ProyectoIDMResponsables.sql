use GesPet
go
if exists (select * from sysobjects where name = 'TD_ProyectoIDMResponsables' and sysstat & 7 = 2)
	drop view dbo.TD_ProyectoIDMResponsables
go
create view dbo.TD_ProyectoIDMResponsables
as
select
	ProjId, 
	ProjSubId, 
	ProjSubSId, 
	cod_nivel, 
	cod_area, 
	propietaria, 
	resp_user, 
	resp_fecha
from ProyectoIDMResponsables
go
grant select on dbo.ProyectoIDMResponsables to GesPetUsr
go
print 'Actualización realizada.'
go
