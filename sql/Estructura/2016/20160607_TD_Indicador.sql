use GesPet
go
if exists (select * from sysobjects where name = 'TD_Indicador' and sysstat & 7 = 2)
	drop view dbo.TD_Indicador
go
create view dbo.TD_Indicador
as
select
	indicadorId, 
	indicadorNom, 
	indicadorHab
from Indicador
go
grant select on dbo.Indicador to GesPetUsr
go
print 'Actualización realizada.'
go
