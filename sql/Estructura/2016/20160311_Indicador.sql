print 'Creando / modificando la tabla: Indicador... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Indicador' and type = 'U')
	drop table GesPet.dbo.Indicador
go
	
create table GesPet.dbo.Indicador (
	indicadorId		int				not null,
	indicadorNom		varchar(60)		null,
	indicadorHab		char(1)			null,
	constraint PK_Indicador1 PRIMARY KEY NONCLUSTERED (indicadorId))
go

print 'Tabla modificada exitosamente.'
go