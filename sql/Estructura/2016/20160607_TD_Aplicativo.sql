use GesPet
go
if exists (select * from sysobjects where name = 'TD_Aplicativo' and sysstat & 7 = 2)
	drop view dbo.TD_Aplicativo
go
create view dbo.TD_Aplicativo
as
select
	app_id, 
	app_nombre, 
	app_hab, 
	cod_direccion, 
	cod_gerencia, 
	cod_sector, 
	cod_grupo, 
	cod_r_direccion, 
	cod_r_gerencia, 
	cod_r_sector, 
	cod_r_grupo, 
	app_amb
from Aplicativo
go
grant select on dbo.Aplicativo to GesPetUsr
go
print 'Actualización realizada.'
go
