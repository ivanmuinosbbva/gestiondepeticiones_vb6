use GesPet
go
if exists (select * from sysobjects where name = 'TD_ProyectoClase' and sysstat & 7 = 2)
	drop view dbo.TD_ProyectoClase
go
create view dbo.TD_ProyectoClase
as
select
	ProjCatId, 
	ProjClaseId, 
	ProjClaseNom
from ProyectoClase
go
grant select on dbo.ProyectoClase to GesPetUsr
go
print 'Actualización realizada.'
go
