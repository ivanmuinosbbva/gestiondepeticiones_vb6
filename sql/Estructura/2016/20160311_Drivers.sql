print 'Creando / modificando la tabla: Drivers... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Drivers' and type = 'U')
	drop table GesPet.dbo.Drivers
go
	
create table GesPet.dbo.Drivers (
	driverId		int			not null,
	driverNom		varchar(255)	null,
	driverAyuda		varchar(255)	null,
	driverValor		int				null,
	driverResp		char(4)			null,
	driverHab		char(1)			null,
	indicadorId		int				null,
	constraint PK_Drivers1 PRIMARY KEY NONCLUSTERED (driverId))
go

print 'Tabla modificada exitosamente.'
go