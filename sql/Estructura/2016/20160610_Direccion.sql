print 'Modificando la tabla : Direccion'
go

print '1. Agregando campo: abrev_direccion...'
go

alter table GesPet..Direccion
	add abrev_direccion		varchar(30)		null
go

print '2. Modificando campo: nom_direccion...'
go

alter table GesPet..Direccion
	MODIFY nom_direccion	char(80)	null
go

print 'Listo.'
go
