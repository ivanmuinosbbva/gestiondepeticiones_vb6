print 'Creando/modificando la tabla: Perfil... aguarde por favor...'
go

use GesPet
go

alter table GesPet.dbo.Perfil
	add  
		visible		char(1)			null,
		habilitado	char(1)			null,
		orden		int				null
go

print 'Tabla modificada exitosamente.'
go
