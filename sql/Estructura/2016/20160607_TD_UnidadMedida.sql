use GesPet
go
if exists (select * from sysobjects where name = 'TD_UnidadMedida' and sysstat & 7 = 2)
	drop view dbo.TD_UnidadMedida
go
create view dbo.TD_UnidadMedida
as
select
	unimedId, 
	unimedDesc, 
	unimedSmb
from UnidadMedida
go
grant select on dbo.UnidadMedida to GesPetUsr
go
print 'Actualización realizada.'
go
