use GesPet
go

if exists (select * from sysobjects where name = 'TD_HorasTrabajadas' and sysstat & 7 = 2)
	drop view dbo.TD_HorasTrabajadas
go

create view dbo.TD_HorasTrabajadas
as
	select
		cod_recurso, 
		cod_tarea, 
		pet_nrointerno, 
		fe_desde, 
		fe_hasta, 
		horas, 
		trabsinasignar, 
		audit_user, 
		audit_date,
		tipo
	from HorasTrabajadas
go

grant select on dbo.HorasTrabajadas to GesPetUsr
go

print 'Actualización realizada.'
go

print 'Fin de proceso.'
go