print 'Creando / modificando la tabla: MotivosNoPlan... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'MotivosNoPlan' and type = 'U')
	drop table GesPet.dbo.MotivosNoPlan
go
	
create table GesPet.dbo.MotivosNoPlan (
	cod_motivo_noplan		int			not null,
	nom_motivo_noplan		char(50)		null,
	estado_hab				char(1)			null,
	constraint PKMotivosNoPlan1 PRIMARY KEY CLUSTERED (cod_motivo_noplan)
	)
go

print 'Tabla modificada exitosamente.'