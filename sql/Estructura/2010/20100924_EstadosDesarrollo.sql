print 'Creando / modificando la tabla: EstadosDesarrollo... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'EstadosDesarrollo' and type = 'U')
	drop table GesPet.dbo.EstadosDesarrollo
go
	
create table GesPet.dbo.EstadosDesarrollo (
	cod_estado_desa		int			not null,
	nom_estado_desa		char(50)		null,
	estado_hab			char(1)			null,
	constraint PKEstadosDesarrollo1 PRIMARY KEY CLUSTERED (cod_estado_desa)
	)
go

print 'Tabla modificada exitosamente.'