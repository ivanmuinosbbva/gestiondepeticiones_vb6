/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Recurso'
go

if exists (select * from sysobjects where name = 'vw_Recurso' and sysstat & 7 = 2)
	drop view dbo.vw_Recurso
go

create view dbo.vw_Recurso
as 
	select
		'1' as 'pet_empid',
		a.cod_recurso,
		a.nom_recurso,
		a.vinculo,
		a.cod_gerencia,
		a.cod_direccion,
		a.cod_sector,
		a.cod_grupo,
		a.flg_cargoarea,
		a.estado_recurso,
		a.horasdiarias,
		a.observaciones,
		a.dlg_recurso,
		dlg_desde = (convert(char(8), a.dlg_desde, 112)),
		dlg_hasta = (convert(char(8), a.dlg_hasta, 112)),
		a.audit_user,
		audit_date = (convert(char(8), a.audit_date, 112)),
		a.email,
		a.euser
	from
		GesPet..Recurso a
go

grant select on dbo.vw_Recurso to GesPetUsr
go

print 'Actualización realizada.'
