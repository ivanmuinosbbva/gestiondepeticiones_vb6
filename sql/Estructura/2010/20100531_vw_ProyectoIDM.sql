/*
-001- a. FJS 26.05.2010 - Nueva vista para visualizar proyectos IDM.
-002- a. FJS 12.10.2011 - Se actualizan los datos de la vista para ver todos los campos de Proyectos IDM.

*/

use GesPet
go

print 'Creando/actualizando vista: vw_ProyectoIDM'
go

if exists (select * from sysobjects where name = 'vw_ProyectoIDM' and sysstat & 7 = 2)
	drop view dbo.vw_ProyectoIDM
go

create view dbo.vw_ProyectoIDM
as 
	select
		'1' as 'pet_empid',
		a.ProjId,
		a.ProjSubId,
		a.ProjSubSId,
		a.ProjNom,
		ProjFchAlta = (convert(char(8), a.ProjFchAlta, 112)),
		a.ProjCatId,
		ProjCatNom = (select x.ProjCatNom from GesPet..ProyectoCategoria x where x.ProjCatId = a.ProjCatId),
		a.ProjClaseId,
		ProjClaseNom = (select x.ProjClaseNom from GesPet..ProyectoClase x where x.ProjCatId = a.ProjCatId and x.ProjClaseId = a.ProjClaseId),
		a.cod_estado,
		nom_estado = (select x.nom_estado from GesPet..Estados x where x.cod_estado = a.cod_estado),
		a.fch_estado,
		a.usr_estado,
		a.marca,
		a.plan_tyo,
		plan_TYO_nom = case
			when a.plan_tyo = 'S' then 'Si'
			when a.plan_tyo = 'N' then 'No'
			else ''
		end,
		a.empresa,
		nom_empresa = case
			when a.empresa = 'B' then 'Banco'
			when a.empresa = 'C' then 'Consolidar'
			else ''
		end,
		a.area,
		area_nom = case
			when a.area = 'C' then 'Corporativo'
			when a.area = 'R' then 'Regional'
			when a.area = 'L' then 'Local'
			else ''
		end,
		a.clas3,
		clas3_nom = case
			when a.clas3 = 'C' then 'Customer Centric'
			when a.clas3 = 'M' then 'Modelo de distribución'
			when a.clas3 = 'L' then 'LEAN'
			else ''
		end,
		a.semaphore,
		a.owner,
		a.cod_bpar,
		nom_bpar = (select x.nom_recurso from GesPet..Recurso x where x.cod_recurso = a.cod_bpar),
		a.avance,
		a.cod_estado2,
		a.fe_modif,
		a.fe_inicio,
		a.fe_fin,
		a.fe_finreprog,
		a.fe_ult_nove,
		a.semaphore2,
		a.sema_fe_modif,
		a.sema_usuario,
		orden = (a.ProjId * 100000 + a.ProjSubId * 100 + a.ProjSubSId)			-- Para ordenar
	from
		GesPet..ProyectoIDM a
	return(0)
go

grant select on dbo.vw_ProyectoIDM to GesPetUsr
go

print 'Actualización realizada.'
go
