print 'Creando / modificando la tabla: MotivosReplan... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'MotivosReplan' and type = 'U')
	drop table GesPet.dbo.MotivosReplan
go
	
create table GesPet.dbo.MotivosReplan (
	cod_motivo_replan		int			not null,
	nom_motivo_replan		char(50)		null,
	estado_hab				char(1)			null,
	constraint PKMotivosReplan1 PRIMARY KEY CLUSTERED (cod_motivo_replan)
	)
go

print 'Tabla modificada exitosamente.'