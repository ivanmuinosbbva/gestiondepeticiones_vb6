print 'Creando / modificando la tabla: Documento... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Documento' and type = 'U')
	drop table GesPet.dbo.Documento
go
	
create table GesPet.dbo.Documento (
	cod_doc			char(6)		not null,
	nom_doc			char(80)		null,
	hab_doc			char(1)			null,
	constraint PK_Documento1 PRIMARY KEY CLUSTERED (cod_doc))
go

print 'Tabla modificada exitosamente.'
go