print 'Creando / modificando la tabla: PeriodoEstado... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'PeriodoEstado' and type = 'U')
	drop table GesPet.dbo.PeriodoEstado
go
	
create table GesPet.dbo.PeriodoEstado (
	cod_estado_per	char(8)		not null,
	nom_estado_per	char(100)	null,
	constraint PKPeriodoEstado1 PRIMARY KEY CLUSTERED (cod_estado_per))
go

print 'Tabla modificada exitosamente.'