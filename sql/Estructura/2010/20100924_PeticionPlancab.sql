print 'Creando / modificando la tabla: PeticionPlancab... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'PeticionPlancab' and type = 'U')
	drop table GesPet.dbo.PeticionPlancab
go
	
create table GesPet.dbo.PeticionPlancab (
	per_nrointerno		int				not null,
	pet_nrointerno		int				not null,
	prioridad			char(1)			null,		-- 1 o 2 para indicar la prioridad
	cod_bpar			char(10)		null,		-- C�digo de legajo del BP que prioriz� la petici�n.
	plan_stage			char(1)			null,		-- Indica en que momento de la planificaci�n fu� ingresada esta petici�n (inicial, agregada luego, etc.)
	plan_actfch			smalldatetime	null,		-- Fecha de �ltima actualizaci�n
	plan_actusr			char(10)		null,		-- Legajo de usuario de �ltima actualizaci�n
	plan_estado			char(6)			null,		-- Estado del registro
	plan_act2fch		smalldatetime	null,
	plan_act2usr		char(10)		null,
	constraint PKPetPlancab1 PRIMARY KEY CLUSTERED (per_nrointerno,pet_nrointerno)
	)
go

print 'Tabla modificada exitosamente.'