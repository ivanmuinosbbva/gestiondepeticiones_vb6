print 'Creando / modificando la tabla: Periodo... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Periodo' and type = 'U')
	drop table GesPet.dbo.Periodo
go
	
create table GesPet.dbo.Periodo (
	per_nrointerno	int				not null,
	per_nombre		char(50)		null,
	per_tipo		char(1)			null,
	fe_desde		smalldatetime	null,
	fe_hasta		smalldatetime	null,
	per_estado		char(6)			null,
	per_ultfchest	smalldatetime	null,
	per_ultusrid	char(10)		null,
	constraint PKPeriodo1 PRIMARY KEY CLUSTERED (per_nrointerno)
	)
go

print 'Tabla modificada exitosamente.'