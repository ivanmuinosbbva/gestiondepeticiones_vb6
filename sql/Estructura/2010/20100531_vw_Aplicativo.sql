/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Aplicativo'
go

if exists (select * from sysobjects where name = 'vw_Aplicativo' and sysstat & 7 = 2)
	drop view dbo.vw_Aplicativo
go

create view dbo.vw_Aplicativo
as 
	select
		'1' as 'pet_empid',
		a.app_id,
		a.app_nombre,
		a.app_hab
	from
		GesPet..Aplicativo a
go

grant select on dbo.vw_Aplicativo to GesPetUsr
go

print 'Actualización realizada.'
