print 'Creando / modificando la tabla: Prioridad... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Prioridad' and type = 'U')
	drop table GesPet.dbo.Prioridad
go
	
create table GesPet.dbo.Prioridad (
	prior_id		char(1)		not null,
	prior_dsc		varchar(50)	null,
	prior_ord		int			null,
	prior_hab		char(1)		null,
	constraint PK_Prioridad1 PRIMARY KEY CLUSTERED (prior_id )
	)
go

print 'Tabla modificada exitosamente.'