/*
-000- a. FJS 02.11.2010 - Nueva vista para enumerar los legajos de recursos con perfiles Administrador, Business Partner, Responsable de Ejecución, 
						  Responsable de Gerencia y Responsable de Sector para ser tomados por Seguridad Informática para armar el acceso por WebSEAL 
						  para IGM. Es decir, todas las personas que tengan estos perfiles en CGM tendrán acceso a IGM.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_IGM_Recursos'
go

if exists (select * from sysobjects where name = 'vw_IGM_Recursos' and sysstat & 7 = 2)
	drop view dbo.vw_IGM_Recursos
go

create view dbo.vw_IGM_Recursos
as 
	select
		distinct r.cod_recurso as 'legajo'
	from
		GesPet..Recurso r
	where
		r.cod_recurso in (
			select x.cod_recurso
			from GesPet..RecursoPerfil x
			where charindex(x.cod_perfil,'ADMI|BPAR|CGER|CSEC|CGRU|')>0)
go

grant select on dbo.vw_IGM_Recursos to GesPetUsr
go
grant select on dbo.vw_IGM_Recursos to GrpTrnIGM
go

print 'Actualización realizada.'
go