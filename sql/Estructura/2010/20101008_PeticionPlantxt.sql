print 'Creando / modificando la tabla: PeticionPlantxt... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'PeticionPlantxt' and type = 'U')
	drop table GesPet.dbo.PeticionPlantxt
go
	
create table GesPet.dbo.PeticionPlantxt (
	per_nrointerno		int				not null,
	pet_nrointerno		int				not null,
	cod_grupo			char(8)			not null,
	mem_campo			char(10)		not null,
	mem_secuencia		smallint		not null,
	mem_texto			char(255)		null,
	constraint PKPetPlantxt1 PRIMARY KEY CLUSTERED (per_nrointerno,cod_grupo,pet_nrointerno,mem_campo,mem_secuencia))
go

print 'Tabla modificada exitosamente.'