/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_HorasTrabajadasAplicativo'
go

if exists (select * from sysobjects where name = 'vw_HorasTrabajadasAplicativo' and sysstat & 7 = 2)
	drop view dbo.vw_HorasTrabajadasAplicativo
go

create view dbo.vw_HorasTrabajadasAplicativo
as 
	select
		'1' as 'pet_empid',
		a.cod_recurso,
		a.cod_tarea,
		a.pet_nrointerno,
		fe_desde = (convert(char(8), a.fe_desde, 112)),
		fe_hasta = (convert(char(8), a.fe_hasta, 112)),
		a.horas,
		a.app_id,
		a.app_horas,
		a.hsapp_texto
	from 
		GesPet..HorasTrabajadasAplicativo a
go

grant select on dbo.vw_HorasTrabajadasAplicativo to GesPetUsr
go

print 'Actualización realizada.'
