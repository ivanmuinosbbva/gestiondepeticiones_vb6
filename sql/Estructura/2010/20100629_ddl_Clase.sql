print 'Creando / modificando la tabla: Clase... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'Clase' and type = 'U')
	drop table GesPet.dbo.Clase
go
	
create table GesPet.dbo.Clase (
	cod_clase		char(4)	not null,
	nom_clase		char(40)	null,
	constraint PK_Clase001 PRIMARY KEY CLUSTERED (cod_clase)
	)
go

print 'Tabla modificada exitosamente.'