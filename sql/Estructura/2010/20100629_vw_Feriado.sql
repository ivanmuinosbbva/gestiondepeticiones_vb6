/*
-001- a. FJS 26.05.2010 - Nueva vista para feriados (s�bados, domingos y no h�biles)
-002- a. FJS 14.10.2014 - Se agrega permiso de selecci�n para el usuario TrnCGM (SGI).
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Feriado'
go

if exists (select * from sysobjects where name = 'vw_Feriado' and sysstat & 7 = 2)
	drop view dbo.vw_Feriado
go

create view dbo.vw_Feriado
as 
	select
		'1' as 'pet_empid',
		fecha = (convert(char(8), a.fecha, 112))
	from
		GesPet..Feriado a
go

grant select on dbo.vw_Feriado to GesPetUsr
go

grant select on dbo.vw_Feriado to TrnCGM
go

print 'Actualizaci�n realizada.'
go
