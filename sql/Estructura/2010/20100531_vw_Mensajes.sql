/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Mensajes'
go

if exists (select * from sysobjects where name = 'vw_Mensajes' and sysstat & 7 = 2)
	drop view dbo.vw_Mensajes
go

create view dbo.vw_Mensajes
as 
	select
		'1' as 'pet_empid',
		a.msg_nrointerno,
		a.pet_nrointerno,
		fe_mensaje = (convert(char(8), a.fe_mensaje, 112)),
		a.cod_perfil,
		a.cod_nivel,
		a.cod_area,
		a.cod_txtmsg,
		a.cod_estado,
		a.cod_situacion,
		txt_txtmsg = rtrim((str_replace(str_replace(str_replace(str_replace(str_replace(a.txt_txtmsg, '|',''), char(13),''), char(10),''), char(9),''), char(7),'')))
	from 
		GesPet..Mensajes a
go

grant select on dbo.vw_Mensajes to GesPetUsr
go

print 'Actualización realizada.'
