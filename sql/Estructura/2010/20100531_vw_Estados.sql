/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Estados'
go

if exists (select * from sysobjects where name = 'vw_Estados' and sysstat & 7 = 2)
	drop view dbo.vw_Estados
go

create view dbo.vw_Estados
as 
	select
		'1' as 'pet_empid',
		a.cod_estado,
		a.nom_estado
	from
		GesPet..Estados a
go

grant select on dbo.vw_Estados to GesPetUsr
go

print 'Actualización realizada.'
