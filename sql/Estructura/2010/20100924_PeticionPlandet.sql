print 'Creando / modificando la tabla: PeticionPlandet... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'PeticionPlandet' and type = 'U')
	drop table GesPet.dbo.PeticionPlandet
go
	
create table GesPet.dbo.PeticionPlandet (
	per_nrointerno		int				not null,
	pet_nrointerno		int				not null,
	cod_grupo			char(8)			not null,
	plan_orden			int				null,
	plan_ordenfin		int				null,
	plan_estado			char(6)			null,
	plan_actfch			smalldatetime	null,
	plan_actusr			char(10)		null,
	plan_act2fch		smalldatetime	null,
	plan_act2usr		char(10)		null,
	plan_ori			int				null,		-- Inicial: Origen de planificación
	plan_estadoini		int				null,		-- Inicial: Estado de planificación
	plan_motnoplan		int				null,		-- Inicial: Motivo de no planificación (no se puede planificar)
	plan_fealcadef		smalldatetime	null,		-- Inicial: Fecha de definición de alcance
	plan_fefunctst		smalldatetime	null,		-- Inicial: Fecha de fin de pruebas funcionales
	plan_horasper		int				null,		-- Inicial: Cantidad de horas planificadas para el presente período
	plan_desaestado		int				null,		-- Seguimiento: Estado actual del desarrollo
	plan_motivsusp		int				null,		-- Seguimiento: Motivo de Susp./Desplan./Canc./Rech.
	plan_motivrepl		int				null,		-- Replanificar: Motivo de replanificación
	plan_feplanori		smalldatetime	null,		-- Replanificar: Fecha original de planificación
	plan_feplannue		smalldatetime	null,		-- Replanificar: Nueva fecha de planificación
	plan_cantreplan		int				null,		-- Replanificar: Cantidad de replanificaciones (contador)
	plan_horasreplan	int				null,		-- Replanificar: Cantidad de horas totales replanificadas
	plan_fchreplan		smalldatetime	null,		-- Replanificar: fecha de la última replanificación
	constraint PKPetPlandet1 PRIMARY KEY CLUSTERED (per_nrointerno,cod_grupo,pet_nrointerno))
go

print 'Tabla modificada exitosamente.'