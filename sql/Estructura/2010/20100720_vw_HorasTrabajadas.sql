/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_HorasTrabajadas'
go

if exists (select * from sysobjects where name = 'vw_HorasTrabajadas' and sysstat & 7 = 2)
	drop view dbo.vw_HorasTrabajadas
go

create view dbo.vw_HorasTrabajadas
as 
	select
		'1' as 'pet_empid',
		a.cod_recurso,
		a.cod_tarea,
		a.pet_nrointerno,
		fe_desde = (convert(char(8), a.fe_desde, 112)),
		fe_hasta = (convert(char(8), a.fe_hasta, 112)),
		a.horas,
		a.trabsinasignar,
		'' as observaciones,
		a.audit_user,
		audit_date = (convert(char(8), a.audit_date, 112))
	from
		GesPet..HorasTrabajadas a
go

grant select on dbo.vw_HorasTrabajadas to GesPetUsr
go

print 'Actualización realizada.'








