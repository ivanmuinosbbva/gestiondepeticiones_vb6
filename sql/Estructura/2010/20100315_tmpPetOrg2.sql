/*
*********************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Prop�sito:	Creaci�n de la tabla tmpPetOrg2 para exportar datos semanalmente para W. Salvi
* Ambiente:		SyBase / SQL Server
* Fecha:		15.03.2010
*********************************************************************************************************
*/

print 'Modificando la tabla: tmpPetOrg2... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'tmpPetOrg2' and type = 'U')
	drop table GesPet.dbo.tmpPetOrg2
go
	
create table GesPet.dbo.tmpPetOrg2 (
	pet_empresa			int				null,
	pet_nroasignado		int				null,
	pet_tipo			char(3)			null,
	pet_titulo			char(50)		null,
	pet_descripcion		char(255)		null,
	pet_caracteristicas	char(255)		null,
	pet_motivos			char(255)		null,
	pet_beneficios		char(255)		null,
	pet_relaciones		char(255)		null,
	pet_nom_estado		char(30)		null,
	pet_fe_ini_plan		char(10)		null,
	pet_fe_fin_plan		char(10)		null,
	pet_fe_ini_real		char(10)		null,
	pet_fe_fin_real		char(10)		null,
	pet_cod_agrup		char(30)		null)
go

print 'Tabla modificada exitosamente.'