/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_HistorialMemo'
go

if exists (select * from sysobjects where name = 'vw_HistorialMemo' and sysstat & 7 = 2)
	drop view dbo.vw_HistorialMemo
go

create view dbo.vw_HistorialMemo
as 
	select
		'1' as 'pet_empid',
		a.hst_nrointerno,
		a.hst_secuencia,
		mem_texto = rtrim((str_replace(str_replace(str_replace(str_replace(str_replace(a.mem_texto, '|', ''),char(13),''),char(10),''),char(9),''),char(7),'')))
	from
		GesPet..HistorialMemo a
go

grant select on dbo.vw_HistorialMemo to GesPetUsr
go

print 'Actualización realizada.'
