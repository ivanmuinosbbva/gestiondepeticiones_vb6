/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Historial'
go

if exists (select * from sysobjects where name = 'vw_Historial' and sysstat & 7 = 2)
	drop view dbo.vw_Historial
go

create view dbo.vw_Historial
as 
	select
		'1' as 'pet_empid',
		a.pet_nrointerno,
		a.hst_nrointerno,
		hst_fecha = (convert(char(10), a.hst_fecha, 112)),
		a.cod_evento,
		a.pet_estado,
		a.cod_sector,
		a.sec_estado,
		a.cod_grupo,
		a.gru_estado,
		a.replc_user,
		a.audit_user
	from
		GesPet..Historial a
go

grant select on dbo.vw_Historial to GesPetUsr
go

print 'Actualización realizada.'
