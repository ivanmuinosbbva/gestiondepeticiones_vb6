print 'Creando / modificando la tabla: IGM_Estados... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'IGM_Estados' and type = 'U')
	drop table GesPet.dbo.IGM_Estados
go
	
create table GesPet.dbo.IGM_Estados (
	cod_IGM_estado		char(6)	not null,
	cod_CGM_estado		char(6)	not	null,
	constraint PK_IGM_Estados001 PRIMARY KEY CLUSTERED (cod_IGM_estado, cod_CGM_estado)
	)
go

print 'Tabla creada / modificada exitosamente.'
go