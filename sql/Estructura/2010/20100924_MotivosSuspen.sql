print 'Creando / modificando la tabla: MotivosSuspen... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'MotivosSuspen' and type = 'U')
	drop table GesPet.dbo.MotivosSuspen
go
	
create table GesPet.dbo.MotivosSuspen (
	cod_motivo_suspen		int			not null,
	nom_motivo_suspen		char(50)		null,
	estado_hab				char(1)			null,
	constraint PKMotivosSuspen1 PRIMARY KEY CLUSTERED (cod_motivo_suspen)
	)
go

print 'Tabla modificada exitosamente.'