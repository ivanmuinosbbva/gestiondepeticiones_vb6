/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_Motivos'
go

if exists (select * from sysobjects where name = 'vw_Motivos' and sysstat & 7 = 2)
	drop view dbo.vw_Motivos
go

create view dbo.vw_Motivos
as 
	select
		'1' as 'pet_empid',
		a.cod_motivo,
		a.nom_motivo,
		a.hab_motivo
	from
		GesPet..Motivos a
go

grant select on dbo.vw_Motivos to GesPetUsr
go

print 'Actualización realizada.'
