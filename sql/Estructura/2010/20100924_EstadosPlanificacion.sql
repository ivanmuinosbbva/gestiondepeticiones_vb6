print 'Creando / modificando la tabla: EstadosPlanificacion... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'EstadosPlanificacion' and type = 'U')
	drop table GesPet.dbo.EstadosPlanificacion
go
	
create table GesPet.dbo.EstadosPlanificacion (
	cod_estado_plan		char(6)		not null,
	nom_estado_plan		char(50)	null,
	estado_hab			char(1)		null,
	constraint PKEstadosPlanificacion1 PRIMARY KEY CLUSTERED (cod_estado_plan)
	)
go

print 'Tabla modificada exitosamente.'