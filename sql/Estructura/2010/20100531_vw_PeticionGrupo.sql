/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_PeticionGrupo'
go

if exists (select * from sysobjects where name = 'vw_PeticionGrupo' and sysstat & 7 = 2)
	drop view dbo.vw_PeticionGrupo
go

create view dbo.vw_PeticionGrupo
as 
	select
		'1' as 'pet_empid',
		a.pet_nrointerno,
		a.cod_grupo,
		a.cod_sector,
		a.cod_gerencia,
		a.cod_direccion,
		fe_ini_plan = (convert(char(8), a.fe_ini_plan, 112)),
		fe_fin_plan = (convert(char(8), a.fe_fin_plan, 112)),
		fe_ini_real = (convert(char(8), a.fe_ini_real, 112)),
		fe_fin_real = (convert(char(8), a.fe_fin_real, 112)),
		a.horaspresup,
		a.cod_estado,
		fe_estado = (convert(char(8), a.fe_estado, 112)),
		a.cod_situacion,
		a.ult_accion,
		a.hst_nrointerno_sol,
		a.hst_nrointerno_rsp,
		a.audit_user,
		audit_date = (convert(char(8), a.audit_date, 112)),
		fe_fec_plan = (convert(char(8), a.fe_fec_plan, 112)),
		fe_ini_orig = (convert(char(8), a.fe_ini_orig, 112)),
		fe_fin_orig = (convert(char(8), a.fe_fin_orig, 112)),
		fe_fec_orig = (convert(char(8), a.fe_fec_orig, 112)),
		a.cant_planif,
		a.prio_ejecuc,
		a.info_adicio,
		fe_produccion = (convert(char(8), a.fe_produccion, 112)),
		fe_suspension = (convert(char(8), a.fe_suspension, 112)),
		a.cod_motivo
	from
		GesPet..PeticionGrupo a
go

grant select on dbo.vw_PeticionGrupo to GesPetUsr
go

print 'Actualización realizada.'
