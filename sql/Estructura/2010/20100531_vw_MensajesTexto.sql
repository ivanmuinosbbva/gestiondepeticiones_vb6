/*
-001- a. FJS 26.05.2010 - Nueva vista para...
*/

use GesPet
go

print 'Creando/actualizando vista: vw_MensajesTexto'
go

if exists (select * from sysobjects where name = 'vw_MensajesTexto' and sysstat & 7 = 2)
	drop view dbo.vw_MensajesTexto
go

create view dbo.vw_MensajesTexto
as 
	select
		'1' as 'pet_empid',
		a.cod_txtmsg,
		a.msg_texto
	from
		GesPet..MensajesTexto a
go

grant select on dbo.vw_MensajesTexto to GesPetUsr
go

print 'Actualización realizada.'
