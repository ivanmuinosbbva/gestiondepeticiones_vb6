/*
*********************************************************************
* Propietario: 	Fernando J. Spitz
* Prop�sito:	Modificaci�n de la tabla Peticion
* Ambiente:		SyBase / SQL Server
* Fecha:		25.06.2007
*********************************************************************
*/

print 'Modificando la tabla Peticion...'
go
use GesPet
go

print   'Borrando �ndice: idx_PetAnexada'
drop index Peticion.idx_PetAnexada
go

print   'Borrando �ndice: idx_PetInterno'
drop index Peticion.idx_PetInterno
go

alter table GesPet.dbo.Peticion 
	add cod_clase 	CHAR(4)	default 'EVOL'	NOT NULL
	add pet_imptech CHAR(1) default 'N'		NOT NULL
go

print   'Creando �ndice: idx_PetAnexada'
create nonclustered index idx_PetAnexada
on dbo.Peticion (pet_nroanexada)
on 'default'
go

print   'Creando �ndice: idx_PetInterno'
create unique nonclustered index idx_PetInterno
on dbo.Peticion (pet_nrointerno)
on 'default'
go
