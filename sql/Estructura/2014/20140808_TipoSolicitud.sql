print 'Creando / modificando la tabla: TipoSolicitud... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'TipoSolicitud' and type = 'U')
	drop table GesPet.dbo.TipoSolicitud
go
	
create table GesPet.dbo.TipoSolicitud (
	cod_tipo		char(1)		not null,
	nom_tipo		char(20)	null,
	dsc_tipo		char(50)	null,
	constraint PKTipoSolicitud PRIMARY KEY NONCLUSTERED (cod_tipo))
go

print 'Tabla modificada exitosamente.'
go