print 'Creando / modificando la tabla: TipoPet... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'TipoPet' and type = 'U')
	drop table GesPet.dbo.TipoPet
go
	
create table GesPet.dbo.TipoPet (
	cod_tipopet		char(3)		not null,
	nom_tipopet		char(50)	null,
	constraint PKTipoPet1 PRIMARY KEY NONCLUSTERED (cod_tipopet)
	)
go

print 'Tabla creada / modificada exitosamente.'
go