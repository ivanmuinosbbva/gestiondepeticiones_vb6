print 'Creando / modificando la tabla: EstadoSolicitud... aguarde por favor...'
go
	
if exists (select (1) from sysobjects where name = 'EstadoSolicitud' and type = 'U')
	drop table GesPet.dbo.EstadoSolicitud
go
	
create table GesPet.dbo.EstadoSolicitud (
	cod_estado		char(1)		not null,
	nom_estado		char(50)	null,
	constraint PKEstadoSolicitud PRIMARY KEY NONCLUSTERED (cod_estado))
go

print 'Tabla modificada exitosamente.'
go