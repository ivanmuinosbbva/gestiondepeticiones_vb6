/*
-000- a. FJS 06.06.2014 - Nueva vista para realizar un BCPOUT y enviar el archivo a Carga de M�quina con los correos de los usuarios activos de CGM.
*/

use GesPet
go

print 'Creando/actualizando vista: vw_RecursosMail'
go

if exists (select * from sysobjects where name = 'vw_RecursosMail' and sysstat & 7 = 2)
	drop view dbo.vw_RecursosMail
go

create view dbo.vw_RecursosMail
as 
	select '%%' + 
		LTRIM(RTRIM(UPPER(cod_recurso))) + '=' + LTRIM(RTRIM(UPPER(email))) +
		SPACE(80 - DATALENGTH('%%' + LTRIM(RTRIM(UPPER(cod_recurso))) + '=' + LTRIM(RTRIM(UPPER(email)))))
		as recurso
	from Recurso
	where 
		estado_recurso = 'A' and																-- Recurso activo
		LEN(LTRIM(RTRIM(email)))>0 and															-- El campo de email no esta vacio
		CHARINDEX('@',LTRIM(RTRIM(email)))>0 and												-- Existe el @
		LEN(SUBSTRING(LTRIM(RTRIM(email)),1,charindex('@',LTRIM(RTRIM(email)))-1))>0 and		-- La subcadena que antecede al @ es una subcadena > 0
		LEN(SUBSTRING(LTRIM(RTRIM(email)),charindex('@',LTRIM(RTRIM(email))),999))>0 and		-- La subcadena que precede al @ es una subcadena > 0
		CHARINDEX('.',SUBSTRING(LTRIM(RTRIM(email)),charindex('@',LTRIM(RTRIM(email))),999))>0	-- Y adem�s en la parte precedente hay al menos un punto
go

grant select on dbo.vw_RecursosMail to GesPetUsr
go

print 'Actualizaci�n realizada.'
go