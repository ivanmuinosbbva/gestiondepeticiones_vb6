/*
*********************************************************************************************************
* Propietario: 	Fernando J. Spitz
* Prop�sito:	Ampliaci�n del campo nombre de 30 a 80 caracteres y agregado de campo "abrev_grupo"
* Ambiente:		SyBase / SQL Server
* Fecha:		14.11.2014
*********************************************************************************************************
*/

print 'Modificando la tabla GesPet.dbo.Grupo...'
go
use GesPet
go

print   'Borrando �ndice: idx_Grupo'
drop index Grupo.idx_Grupo
go

alter table GesPet.dbo.Grupo
	add 
		abrev_grupo			char(30)	null 
go

alter table GesPet.dbo.Grupo
	modify
		nom_grupo			char(80)	null
go

print   'Creando el �ndice: idx_Grupo'
create unique nonclustered index idx_Grupo
on GesPet.dbo.Grupo (cod_grupo)
go

print 'Tabla modificada.'