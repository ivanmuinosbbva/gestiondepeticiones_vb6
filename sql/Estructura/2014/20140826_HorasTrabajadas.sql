
/* Indices a eliminar

idx_HorasTrbRec    cod_recurso, pet_nrointerno, cod_tarea, fe_desde nonclustered                            0                0                    0 Jan 12 2013  6:19PM Global Index 
idx_HorasTrbFec    fe_desde, fe_hasta                               nonclustered                            
idx_HorasTrbRecFF  cod_recurso, fe_desde, fe_hasta                  nonclustered                            0                0                    0 Jan 12 2013  6:19PM Global Index 
*/

print 'Creando / modificando la tabla: HorasTrabajadas... aguarde por favor...'
go

use GesPet
go

drop index HorasTrabajadas.idx_HorasTrbRec
go

drop index HorasTrabajadas.idx_HorasTrbFec
go

drop index HorasTrabajadas.idx_HorasTrbRecFF
go

alter table HorasTrabajadas
	modify horas	int		NOT NULL
go

print 'Tabla modificada exitosamente.'
go
