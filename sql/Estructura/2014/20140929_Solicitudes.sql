print 'Creando / modificando la tabla: Solicitudes... aguarde por favor...'
go

alter table GesPet..Solicitudes
	add 
		sol_file_inter	char(50)	null,
		sol_bckuptabla	char(50)	null,
		sol_bckupjob	char(50)	null,
		sol_diferida	char(1)		null
go

alter table GesPet..Solicitudes
	modify
		sol_file_prod   char(50)	null,
		sol_file_desa   char(50)	null,
		sol_file_out    char(50)	null,
		sol_lib_Sysin	char(50)	null,
		sol_mem_Sysin	char(50)	null
go

print 'Tabla modificada exitosamente.'
go